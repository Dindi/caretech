<!--Bread-crumb div starts here --> <div>
    <ul class = "breadcrumb">
        <li>
            <a href = "#">Home</a>
        </li>
        <li>
            <a href = "#">Payments</a>
        </li>
    </ul>
</div>
<!--Bread-crumb div ends here -->

<div class = " row">
    <div class = "col-md-4 col-sm-3 col-xs-6">
        <a data-toggle = "tooltip" title = "Total Regular Payment Today" class = "well top-block" href = "#">
            <i class = "glyphicon glyphicon-user blue"></i>

            <div>Today Regular Patient Payments </div>
            <div><span id="total_regular_payments" class="total_regular_payments"></span></div>

        </a>
    </div>
    <div class = "col-md-4 col-sm-3 col-xs-6">
        <a data-toggle = "tooltip" title = "Total Package  Payment Today" class = "well top-block" href = "#">
            <i class = "glyphicon glyphicon-user green"></i>

            <div>Total Package  Payment Today</div>
            <div><span id="total_package_payments"class="total_package_payments"></span></div>

        </a>
    </div>
    <div class = "col-md-4 col-sm-3 col-xs-6">
        <a data-toggle = "tooltip" title = "Total Walk-in Patient Payment Today" class = "well top-block" href = "#">
            <i class = "glyphicon glyphicon-user green"></i>

            <div>Total Walk-in Patient Payment Today </div>
            <div><span id="total_walkin_patient_payments"class="total_walkin_patient_payments"></span></div>

        </a>
    </div>

    <div class = "col-md-4 col-sm-3 col-xs-6">
        <a data-toggle = "tooltip" title = "Total Lab Service Payment Today" class = "well top-block" href = "#">
            <i class = "glyphicon glyphicon-user blue"></i>

            <div>Total Lab Service Payment Today </div>
            <div><span id="total_lab_service_payments" class="total_lab_service_payments"></span></div>

        </a>
    </div>

    <div class = "col-md-4 col-sm-3 col-xs-6">
        <a data-toggle = "tooltip" title = "Total Procedure Service Payment Today" class = "well top-block" href = "#">
            <i class = "glyphicon glyphicon-user green"></i>

            <div>Total Procedure Service Payment Today </div>
            <div><span id="total_procedure_service_payments"class="total_procedure_service_payments"></span></div>

        </a>
    </div>

    <div class = "col-md-4 col-sm-3 col-xs-6">
        <a data-toggle = "tooltip" title = "Total Pharmacy Payment Today" class = "well top-block" href = "#">
            <i class = "glyphicon glyphicon-user blue"></i>

            <div>Total Pharmacy Payment Today </div>
            <div><span id="total_pharmacy_payments" class="total_pharmacy_payments"></span></div>

        </a>
    </div>










</div>




<div class = "row">
    <div class = "box col-md-12">
        <div class = "box-inner">
            <div class = "box-header well">
                <h2><i class = "glyphicon glyphicon-info-sign"></i> Cashier </h2>

                <div class = "box-icon">

                    <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                            class = "glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class = "box-content row">


                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Regular Patient Payments</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list regular_patient_payment" id="regular_patient_payment">

                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->





                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "Walk-in Pharmacy Patient Payments">
                            <h2><i class = "glyphicon glyphicon-list"></i> Walk-in Pharmacy Patient Payments</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list walkin_pharmacy_patient_payment" id="walkin_pharmacy_patient_payment">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->


                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "Walk-in Laboratory Patient Payments">
                            <h2><i class = "glyphicon glyphicon-list"></i> Walk-in Laboratory Patient Payments</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list walkin_lab_patient_payment" id="walkin_lab_patient_payment">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->




                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "Walk-in Nurse Patient Payments">
                            <h2><i class = "glyphicon glyphicon-list"></i> Walk-in Nurse Patient Payments</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list walkin_nurse_patient_payment" id="walkin_nurse_patient_payment">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->



                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Lab Service Payments</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list lab_service_payments" id="lab_service_payments">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->

                <!--Active Laboratories -->
                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Pharmacy Payment List </h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list pharmacy_service_payments" id="pharmacy_service_payments">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->

                <!--Active Laboratories end -->

                <!--Active Doctors -->

                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Procedure Payment List</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list" id="procedure_payment_list">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->






                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i>  Pay at the End Payment List(Express Service)</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list pay_at_the_end_payment_list" id="pay_at_the_end_payment_list">

                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->




                <!--Active Doctors end -->



                <!--Active pharmacists end -->


            </div>
        </div>
    </div>
</div>

<div class = "row">

    <!--/span-->


    <!--/span-->

    <!--/span-->
</div><!--/row-->

<div class = "row">

    <!--/span-->

    <!--/span-->


    <!--/span-->
</div><!--/row-->
<!--content ends -->
</div><!--/#content.col-md-0-->
</div><!--/fluid-row-->




<div class = "visit_records_div" id = "visit_records_div" style = "display: none;">

    <fieldset>
        <form class = "form-inline" id = "record_visit_filter_form" method = "post" action = "/visit_records.html">

        </form>
    </fieldset>
</div>










</div><!--/row-->