<?php $this->load->view('reports/header'); ?>
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Reports</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Patients Report</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>
                        <tr>
                            <th>No .</th>
                            <th>Date registered</th>
                            <th>Patient Name</th>
                            <th>DoB</th>
                            <th>Gender</th>
                            <th>Phone No</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $i = 1;
                            foreach ($patients as $value) {
                                ?>
                                <td class="center"><?php echo $i; ?></td>
                                <td class="center">
                                    <?php
                                    echo $value['date_added'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['f_name'] . $value['s_name'] . $value['other_name'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['dob'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['gender']; ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['phone_no'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['email'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    $status = $value['status'];
                                    if ($status === 'Active') {
                                        ?>
                                        <span class="label-success label label-default"><?php echo $status; ?></span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="label-default label"><?php echo $status; ?></span>
                                        <?php
                                    }
                                    ?>
                                </td>


                                <td class="center">
                                    <input type="hidden" name="view_patient_id" class="view_patient_id" id="view_patient_id" value="<?php echo $value['patient_id']; ?>"/>
                                    <a  id="view_patient_link" class="view_patient_link" href="#view_patients_form">
                                        <i class="glyphicon glyphicon-zoom-in icon-white"></i>

                                    </a>|
                                    <a  id="edit_patient_link" class="edit_patient_link" href="#edit_patients_form">
                                        <i class="glyphicon glyphicon-edit icon-white"></i>

                                    </a>|
                                    <a  id="delete_patient_link" class="delete_patient_link" href="#delete_patients_form">
                                        <i class="glyphicon glyphicon-trash icon-white"></i>

                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>




                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->




<div id="view_patients_form" class="view_patients_form" style = "display: none;">






    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Information Form</h2>

                    <div class = "box-icon">
                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                class = "glyphicon glyphicon-chevron-up"></i></a>

                    </div>
                </div>
                <div class = "box-content">

                    <div class = "bs-example">


                        <form class = "view_paitent_form " autocomplete="off" method = "post"  id = "view_paitent_form" role = "form">



                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "Title"> Title :</label>

                                    <input type = "text" class = "form-control view_title" id = "view_title" name="view_title" placeholder = "Title">

                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputFirstName">First Name : </label>
                                    <input type = "text" class = "form-control view_inputFirstName" id = "view_inputFirstName" name="fname" placeholder = "First Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputSurName">Sur Name : </label>
                                    <input type = "text" class = "form-control view_inputSurName" id = "view_inputSurName" name="sname" placeholder = "Sur Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputOtherName">Other Name : </label>
                                    <input type = "text" class = "form-control view_inputOtherName" id = "view_inputOtherName" name="lname" placeholder = "Other Name">
                                </div>

                            </div>

                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputDateofBirth">Date of Birth</label>
                                    <input type = "text" class = "form-control view_dob" name="dob" id = "view_dob" placeholder = "D.O.B">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputSurName">National/Military/Alien ID or Passport No</label>
                                    <input type = "text" class = "form-control view_nationalid" name="nationalid" id = "view_nationalid" placeholder = "National ID ">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputPostalAddress">Postal Address</label>
                                    <input type = "email" class = "form-control view_address" name="address" id = "view_address" placeholder = "Postal Address">
                                </div>
                            </div>


                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputResidence">Residence</label>
                                    <input type = "text" class = "form-control view_residence" name="residence" id = "view_residence" placeholder = "Residence">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputCity">City</label>
                                    <input type = "text" class = "form-control view_city" name="city" id = "view_city" placeholder = "City">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputPhoneNo">Phone No</label>
                                    <input type = "text" class = "form-control view_phone_no" name="phone_no" id = "view_phone_no" placeholder = "Phone No">
                                </div>
                            </div>


                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputEmail">Email : </label>
                                    <input type = "email" class = "form-control view_email" name="email" id ="view_email"  placeholder = "Email">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputEmail">Employment Status : </label>

                                    <input type = "text"  class = "form-control view_employement_status" name="view_employement_status" id ="view_employement_status"  placeholder = "Employment Status" >

                                </div>

                                <div class = "form-group employers_name_div" id="employers_name_div_1" >
                                    <label class = "sr-only" for = "inputEmail">Employer's Name : </label>
                                    <input type = "text"  class = "form-control view_employers_name" name="view_employers_name" id ="view_employers_name"  placeholder = "Employmer's Name" >

                                </div>


                            </div>


                            <div class = "form-inline">
                                <div class = "control-group">
                                    <label class = "control-label" for = "inputGender">Gender</label>
                                    <div class = "controls">


                                        <input type = "text"  class = "form-control view_gender" name="view_gender" id ="view_gender"  placeholder = "Gender" >

                                    </div>
                                </div>
                                <div class = "control-group">
                                    <label class = "control-label" for = "inputMaritalStatus">Marital Status</label>
                                    <div class = "controls">


                                        <input type = "text"  class = "form-control view_maritalstatus" name="view_maritalstatus" id ="view_maritalstatus"  placeholder = "Marital Status" >

                                    </div>
                                </div>


                            </div>



                            <label>Next of Kin Details </label>
                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputFirstName">First Name</label>
                                    <input type = "text" class = "form-control view_kinname" id = "view_kinname" name="kinname" placeholder = "First Name">
                                </div><div class = "form-group">
                                    <label class = "sr-only" for = "inputLastName">Last Name</label>
                                    <input type = "text" class = "form-control view_kinsname" id = "view_kinsname" name="kinsname" placeholder = "Last Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputMiddleName">Middle Name</label>
                                    <input type = "text" class = "form-control view_middlename" id = "view_middlename" placeholder = "Middle Name">
                                </div>
                            </div>


                            <div class = "form-inline">

                                <div class = "control-group">
                                    <label class = "control-label" for = "inputkinrelation"> Relation Type : </label>
                                    <div class = "controls">


                                        <input type = "text" class = "form-control view_kinrelation" id = "view_kinrelation" placeholder = "Kin Relation">

                                    </div>
                                </div>


                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputPhone">Kin's Phone No</label>
                                    <input type="text" placeholder="Kin's Phone"  class = "form-control view_kinphone"  id="view_kinphone" name="view_kinphone" size="30" />
                                </div>

                            </div>
                            <hr>


                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!--/span-->

    </div><!--/row-->





</div>




















<div id="edit_patients_form" class="edit_patients_form">

    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Information Form</h2>

                    <div class = "box-icon">
                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                class = "glyphicon glyphicon-chevron-up"></i></a>

                    </div>
                </div>
                <div class = "box-content">

                    <div class = "bs-example">


                        <form class = "edit_patient_information_form " autocomplete="off" method = "post"  id = "edit_patient_information_form" role = "form">



                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "family_number"> Title :</label>
                                    <select name="title"  required="" class="selector" id="title" >
                                        <option value="">Please select title </option>
                                        <?php foreach ($title as $value) {
                                            ?>
                                            <option value="<?php echo $value['title_name']; ?>"><?php echo $value['title_name']; ?></option>
                                        <?php }
                                        ?>
                                    </select>   
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputFirstName">First Name : </label>
                                    <input type = "text" class = "form-control edit_inputFirstName" id = "edit_inputFirstName" name="fname" placeholder = "First Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputSurName">Sur Name : </label>
                                    <input type = "text" class = "form-control edit_inputSurName" id = "edit_inputSurName" name="sname" placeholder = "Sur Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputOtherName">Other Name : </label>
                                    <input type = "text" class = "form-control edit_inputOtherName" id = "edit_inputOtherName" name="lname" placeholder = "Other Name">
                                </div>

                            </div>

                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputDateofBirth">Date of Birth</label>
                                    <input type = "text" class = "form-control edit_dob" name="dob" id = "edit_dob" placeholder = "D.O.B">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputSurName">National/Military/Alien ID or Passport No</label>
                                    <input type = "text" class = "form-control edit_nationalid" name="nationalid" id = "edit_nationalid" placeholder = "National ID ">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputPostalAddress">Postal Address</label>
                                    <input type = "email" class = "form-control edit_address" name="address" id = "edit_address" placeholder = "Postal Address">
                                </div>
                            </div>


                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputResidence">Residence</label>
                                    <input type = "text" class = "form-control edit_residence" name="residence" id = "edit_residence" placeholder = "Residence">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputCity">City</label>
                                    <input type = "text" class = "form-control edit_city" name="city" id = "edit_city" placeholder = "City">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputPhoneNo">Phone No</label>
                                    <input type = "text" class = "form-control edit_phone_no" name="phone_no" id = "edit_phone_no" placeholder = "Phone No">
                                </div>
                            </div>


                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputEmail">Email : </label>
                                    <input type = "email" class = "form-control edit_email" name="email" id ="edit_email"  placeholder = "Email">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputEmail">Employment Status : </label>

                                    <input type = "text"  class = "form-control edit_employement_status" name="edit_employement_status" id ="edit_employement_status"  placeholder = "Employment Status" >

                                </div>

                                <div class = "form-group employers_name_div" id="employers_name_div_1" >
                                    <label class = "sr-only" for = "inputEmail">Employer's Name : </label>
                                    <input type = "text"  class = "form-control edit_employers_name" name="edit_employers_name" id ="edit_employers_name"  placeholder = "Employmer's Name" >

                                </div>


                            </div>


                            <div class = "form-inline">
                                <div class = "control-group">
                                    <label class = "control-label" for = "inputGender">Gender</label>
                                    <div class = "controls">
                                        <select class = "selectError edit_gender" id="edit_gender" name="edit_gender" data-rel = "" >
                                            <option value = "Male">Male</option>
                                            <option value = "Female">Female</option>
                                            <option value = "Trans-gender">Trans-gender</option>
                                        </select>
                                    </div>
                                </div>
                                <div class = "control-group">
                                    <label class = "control-label" for = "inputMaritalStatus">Marital Status</label>
                                    <div class = "controls">
                                        <select class = "selectError edit_maritalstatus" id="edit_maritalstatus" name="edit_maritalstatus" data-rel = "">
                                            <option value = "Single">Single</option>
                                            <option value = "Married">Married</option>
                                            <option value = "Divorced">Divorced</option>
                                            <option value = "Widowed">Widowed</option>
                                        </select>
                                    </div>
                                </div>


                            </div>



                            <label>Next of Kin Details </label>
                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputFirstName">First Name</label>
                                    <input type = "text" class = "form-control edit_kinname" id = "edit_kinname" name="kinname" placeholder = "First Name">
                                </div><div class = "form-group">
                                    <label class = "sr-only" for = "inputLastName">Last Name</label>
                                    <input type = "text" class = "form-control edit_kinsname" id = "edit_kinsname" name="kinsname" placeholder = "Last Name">
                                </div>

                            </div>


                            <div class = "form-inline">

                                <div class = "control-group">
                                    <label class = "control-label" for = "inputkinrelation"> Relation Type : </label>
                                    <div class = "controls">
                                        <select class = "selectError edit_kinrelation"  id="edit_kinrelation" name="edit_kinrelation" data-rel = "">
                                            <option value = "Sibling">Sibling</option>
                                            <option value = "Father">Father</option>
                                            <option value = "Mother">Mother</option>
                                            <option value = "Husband">Husband</option>
                                            <option value = "Wife">Wife</option>
                                            <option value = "Aunt">Aunt</option>
                                            <option value="Uncle">Uncle</option>
                                        </select>
                                    </div>
                                </div>


                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputPhone">Kin's Phone No</label>
                                    <input type="text" placeholder="Kin's Phone"  class = "form-control edit_kinphone"  id="edit_kinphone" name="edit_kinphone" size="30" />
                                </div>

                            </div>
                            <hr>

                            <div class = "form-group">

                                <input type="submit"   class = "form-control edit_kinphone"  id="edit_kinphone" name="edit_kinphone" size="30" />
                                <input type="reset" class="btn btn-close"/>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!--/span-->

    </div><!--/row-->


</div>








<div id="delete_patients_form" class="delete_patients_form">

</div>

<?php $this->load->view('reports/footer'); ?>