<?php $this->load->view('header'); ?>
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Reports</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Walk-In Report</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>
                        <tr>

                            <th>Date </th>
                            <th>Patient Name</th>
                            <th>Phone No</th>
                            <th>Department</th>
                            <th>Payment Status</th>

                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            foreach ($walkins as $value) {
                                ?>
                                <td class="center">
                                    <?php
                                    echo $value['walkin_date'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['walkin_patient_name'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['walkin_phone_no'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['walkin_department']; ?>
                                </td>
                                <td class="center">
                                    <?php
                                    $paid = $value['paid'];
                                    if ($paid === 'paid') {
                                        ?>
                                        <label class="label label-success"><?php echo$paid; ?></label>
                                        <?php
                                    } elseif ($paid === 'not paid') {
                                        ?>
                                        <label class="label label-danger"><?php echo$paid; ?></label>
                                        <?php
                                    }
                                    ?>
                                </td>




                                <td class="center">
                                    <a class="btn btn-success" href="<?php echo base_url();?>">
                                        <i class="glyphicon glyphicon-zoom-in icon-white"></i>
                                        View
                                    </a>
                                    <a class="btn btn-info" href="<?php echo base_url();?>">
                                        <i class="glyphicon glyphicon-edit icon-white"></i>
                                        Edit
                                    </a>
                                    <a class="btn btn-danger" href="#delete_walkin_patient">
                                        <i class="glyphicon glyphicon-trash icon-white"></i>
                                        Delete
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>




                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<?php $this->load->view('footer'); ?>