<?php if (!isset($no_visible_elements) || !$no_visible_elements) { ?>
    <!-- content ends -->
    </div><!--/#content.col-md-0-->
<?php } ?>
</div><!--/fluid-row-->
<?php if (!isset($no_visible_elements) || !$no_visible_elements) { ?>


    <hr>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>

    <footer class="row">
        <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://healthtechkenya.com" target="_blank">Health Tech Kenya
            </a> 2012 - <?php echo date('Y') ?></p>


    </footer>
<?php } ?>

</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="<?php echo base_url(); ?>assets/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='<?php echo base_url(); ?>assets/bower_components/moment/min/moment.min.js'></script>
<script src='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="<?php echo base_url(); ?>assets/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="<?php echo base_url(); ?>assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<?php echo base_url(); ?>assets/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<?php echo base_url(); ?>assets/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<?php echo base_url(); ?>assets/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<?php echo base_url(); ?>assets/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<?php echo base_url(); ?>assets/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<?php echo base_url(); ?>assets/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<?php echo base_url(); ?>assets/js/jquery.history.js"></script>
<!-- validate.js for form and user input validation -->
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
<!-- application script for Charisma demo -->
<script src="<?php echo base_url(); ?>assets/js/charisma.js"></script>



<link href='http://datatables.net/release-datatables/media/css/jquery.dataTables.css' rel='stylesheet'>
<link href='http://datatables.net/release-datatables/extensions/FixedColumns/css/dataTables.fixedColumns.css' rel='stylesheet'>
<script type="text/css">
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 800px;
        margin: 0 auto;
    }
</script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.pack.js?v=2.1.4"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>


<script type="text/javascript" href="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" href="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.js"></script>

<script src="<?php echo base_url(); ?>assets/js/notify.js"></script>


<div style="display: none !important;">
    <div class="box-content ajax_loader" id="ajax_loader">
        <ul class="ajax-loaders">

            <img src="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" title="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" >
            </br>
            <br/>

            <span class=" loader_notify clearfix" id="loader_notify">
            </span>  

        </ul>


    </div>

</div>

<script type="text/javascript">
    $(function () {
        var $sfield = $('#procedure_name_1').autocomplete({
            source: function (request, response) {
                var url = "<?php echo site_url('reception/procedures'); ?>";
                $.post(url, {data: request.term}, function (data) {
                    response($.map(data, function (procedures) {
                        return {
                            value: procedures.procedure_name
                        };
                    }));
                }, "json");
            },
            minLength: 2,
            autofocus: true
        });
    });
    $(document).ready(function () {

        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reception/reception/get_active_doctors/",
                dataType: "JSON",
                success: function (active_doctors) {
                    active_doctor = $('#active_doctor_list_1').empty();
                    if (active_doctor === null) {
                        active_doctor.append("<option>No  Active Doctor</opton>");
                    }
                    $.each(active_doctors, function (i, active_doctorss) {
                        active_doctor.append('<option value="' + active_doctorss.employee_id + '">' + active_doctorss.employee_name + '</option>');
                    });
                },
                error: function (data) {
                    //
                    //  alert('An error occured, kindly try later');
                }
            });
        }, 10000);


        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reception/reception/total_visits",
                dataType: "JSON",
                success: function (total_visits) {
                    total_visit = $('#total_regular_visits').empty();
                    if (total_visit === null) {
                        total_visit.append("<option>No Visits</opton>");
                    } else {
                        $.each(total_visits, function (i, total_visitss) {
                            total_visit.append('<p>' + total_visitss.total_patient_visits + '</p>');
                        });
                    }



                },
                error: function (data) {
                    //
                    //  alert('An error occured, kindly try later');
                }
            });
        }, 2000);


        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reception/reception/total_walkin_patients",
                dataType: "JSON",
                success: function (walkin_visits) {
                    walkin_visit = $('#total_walkin_patient_visits').empty();
                    if (walkin_visit === null) {
                        walkin_visit.append("<p>No Walkin Visits</p>");
                    } else {
                        $.each(walkin_visits, function (i, walkin_visitss) {
                            walkin_visit.append('<p>' + walkin_visitss.total_walkin_patient + '</p>');
                        });
                    }


                },
                error: function (data) {
                    //
                    //  alert('An error occured, kindly try later');
                }
            });
        }, 2000);


        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reception/reception/seeappointments",
                dataType: "JSON",
                success: function (appointed) {
                    appointment_list = $('#patient_appointments').empty();
                    if (appointed === null) {


                    }
                    else {

                        $.each(appointed, function (i, appointment) {

                            if (appointment.urgency == "urgent") {
                                appointment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + appointment.f_name + " " + appointment.s_name + " " + appointment.other_name + '</a></li><span style="color:red !important;">' + appointment.urgency + '</span>');
                            }
                            else if (appointment_list.urgency != "urgent")
                            {
                                appointment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + appointment.f_name + " " + appointment.s_name + " " + appointment.other_name + '</li>');
                            }
                            else {
                                appointment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                            }


                        });
                    }

                },
                error: function (data) {

                }
            });
        }, 2000);
    });
    $(document).ready(function () {
        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reception/reception/to_paywalkin",
                dataType: "JSON",
                success: function (to_paywalkin) {
                    to_paywalkin_list = $('#walk_in_patient_payments').empty();
                    if (to_paywalkin === null) {

                    } else {
                        $.each(to_paywalkin, function (i, to_paywalkin) {

                            if (to_paywalkin.urgency == "urgent") {
                                to_paywalkin_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + to_paywalkin.fname + " " + to_paywalkin.lname + " " + to_paywalkin.sname + '</a><span style="color:red !important;">' + to_paywalkin.urgency + '</span></li>');
                            }
                            else if (to_paywalkin.urgency != "urgent")
                            {
                                to_paywalkin_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + to_paywalkin.walkin_patient_name + " " + to_paywalkin.faculty + " " + to_paywalkin.walkin_payments_total + '</a></li>');
                            }
                            else {
                                to_paywalkin_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                            }


                        });
                    }


                },
                error: function (data) {
                    //error do something
                }
            });
        }, 2000);
    });
    $(document).ready(function () {
        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reception/reception/pat_payment",
                dataType: "JSON",
                success: function (payment_list) {
                    pat_payment_list = $('#regular_patient_payments').empty();
                    if (payment_list === null) {

                    } else {
                        $.each(payment_list, function (i, payment_list) {

                            if (payment_list.urgency == "urgent") {
                                pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.urgency + '</span><span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');
                            }
                            else if (payment_list.urgency != "urgent")
                            {
                                pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');
                            }
                            else {

                                pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                            }

                        });
                    }


                },
                error: function (data) {
                    //error do something
                }
            });
        }, 2000);
    });
    $(document).ready(function () {

        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reception/reception/active_doctors",
                dataType: "JSON",
                success: function (doctor_list) {
                    active_doctor_list = $('#active_doctor_list').empty();
                    if (doctor_list === null) {
                        active_doctor_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Doctor</a> </li>");
                    } else {
                        $.each(doctor_list, function (i, doctor_list) {


                            active_doctor_list.append('<li><a class="doctor" href="#active_patients_in_doctor"><i class = "glyphicon glyphicon-user"></i>' + doctor_list.user_name + '</a></li>');
                            var doctor_id = doctor_list.employee_id;
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/d_active/" + doctor_id,
                                dataType: "JSON",
                                success: function (people) {
                                    waiting_list = $('#waiting_list').empty();
                                    if (people === null) {


                                        waiting_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                    } else {
                                        $.each(people, function (i, waiting) {

                                            if (waiting.urgency == "urgent") {
                                                waiting_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + waiting.f_name + " " + waiting.other_name + " " + waiting.s_name + '<span style="color:red !important;">' + waiting.urgency + '</span></li>');
                                            }
                                            else if (waiting_list.urgency != "urgent") {
                                                waiting_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + waiting.f_name + " " + waiting.other_name + " " + waiting.s_name + '</a></li>');
                                            }


                                        });
                                    }

                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        });
                    }


                },
                error: function (data) {
                    //error do something
                }
            });
        }, 2000);
    });
    $(document).ready(function () {

        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reception/reception/active_patient_doctor",
                dataType: "JSON",
                success: function (patient_doctor_list) {
                    active_patient_doctor_list = $('#active_patient_visit_doctor').empty();
                    $.each(patient_doctor_list, function (i, patient_doctor_list) {

                        if (patient_doctor_list.urgency == "urgent") {
                            active_patient_doctor_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + patient_doctor_list.f_name + " " + patient_doctor_list.other_name + " " + patient_doctor_list.s_name + '</a><span style="color:red !important;">' + patient_doctor_list.urgency + '</span></li>');
                        }
                        else if (patient_doctor_list.urgency != "urgent")
                        {
                            active_patient_doctor_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + patient_doctor_list.f_name + " " + patient_doctor_list.other_name + " " + patient_doctor_list.s_name + '</a></li>');
                        }
                        else {
                            active_patient_doctor_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                        }

                    });
                },
                error: function (data) {
                    //error do something
                }
            });
        }, 2000);
    });
    $(document).ready(function () {

        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reception/reception/active_nurses",
                dataType: "JSON",
                success: function (nurse_list) {
                    active_nurse_list = $('#active_nurses').empty();
                    if (nurse_list === null) {
                        active_nurse_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Nurse</a> </li>");
                    } else {
                        $.each(nurse_list, function (i, nurse_list) {


                            active_nurse_list.append('<li><i class = "glyphicon glyphicon-user"></i><a class="nurse" href="#active_patients_in_nurse">' + nurse_list.user_name + '</a></li>');
                            var nurse_id = nurse_list.employee_id;
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/load_data/" + nurse_id,
                                dataType: "JSON",
                                success: function (nurse) {
                                    nurse_list = $('#nurse_list').empty();
                                    if (nurse === null) {

                                        nurse_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                    }
                                    else {
                                        $.each(nurse, function (i, nurse) {

                                            if (nurse.urgency == "urgent") {
                                                nurse_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + nurse.f_name + " " + nurse.other_name + " " + nurse.s_name + '<span style="color:red !important;">' + nurse.urgency + '</span></li>');
                                            }
                                            else if (nurse.urgency != "urgent")
                                            {
                                                nurse_list.append('<li><i class = "glyphicon glyphicon-user"></i>' + nurse.f_name + " " + nurse.other_name + " " + nurse.s_name + '</li>');
                                            }
                                            else {
                                                nurse_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                            }
                                        });
                                    }

                                },
                                error: function (data) {
                                    //error do something                          
                                }
                            });
                        });
                    }
                },
                error: function (data) {
                    //error do something            
                }
            });
        }, 2000);
    });
    $(document).ready(function () {

        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reception/reception/active_laboratories",
                dataType: "JSON",
                success: function (laboratory_list) {
                    active_laboratory_list = $('#active_laboratories').empty();
                    if (laboratory_list === null) {
                        active_laboratory_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Lab Tech</a> </li>");
                    } else {
                        $.each(laboratory_list, function (i, laboratory_list) {

                            active_laboratory_list.append('<li><a class="laboratorist" href=""><i class="glyphicon glyphicon-user"></i>' + laboratory_list.user_name + '</a></li>');
                            var laboratorist_id = laboratory_list.employee_id;
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/l_active/" + laboratorist_id,
                                dataType: "JSON",
                                success: function (laboratory) {
                                    laboratory_list = $('#lab_list').empty();
                                    if (laboratory === null) {
                                        laboratory_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                    } else {
                                        $.each(laboratory, function (i, laboratory) {
                                            if (laboratory.urgency == "urgent") {
                                                laboratory_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + laboratory.f_name + " " + laboratory.other_name + " " + laboratory.s_name + '</ul></a><span style="color:red !important;">' + pharm.urgency + '</span></br></ul>');
                                            }
                                            else if (laboratory.urgency != "urgent")
                                            {
                                                laboratory_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + laboratory.f_name + " " + laboratory.other_name + " " + laboratory.s_name + '</br></ul>');
                                            }
                                            else {
                                                laboratory_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                            }
                                        });
                                    }

                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        });
                    }

                },
                error: function (data) {
                    //error do something
                }
            });
        }, 2000);
        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reception/reception/active_pharmacist",
                dataType: "JSON",
                success: function (pharm_list) {
                    active_pharm_list = $('#active_pharm_list').empty();
                    if (pharm_list === null) {
                        active_pharm_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Pharm Tech</a> </li>");
                    } else {
                        $.each(pharm_list, function (i, pharm_list) {

                            active_pharm_list.append('<a class="pharmacist"  href=""><i class="glyphicon glyphicon-user"></i>' + pharm_list.user_name + '</a></br>');
                            var pharmacist_id = pharm_list.employee_id;
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/p_active/" + pharmacist_id,
                                dataType: "JSON",
                                success: function (pharm) {
                                    pharm_list = $('#pharm_list').empty();
                                    if (pharm === null) {
                                        pharm_list.append("<ul>No Active Patient</ul>");
                                    } else {
                                        $.each(pharm, function (i, pharm) {
                                            if (pharm.urgency == "urgent") {
                                                pharm_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + pharm.f_name + " " + pharm.other_name + " " + pharm.s_name + '</ul></a></br></ul>');
                                            }
                                            else if (pharm.urgency != "urgent")
                                            {
                                                pharm_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + pharm.f_name + " " + pharm.other_name + " " + pharm.s_name + '</br></ul>');
                                            }
                                            else {
                                                pharm_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                            }
                                        });
                                    }

                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        });
                    }

                },
                error: function (data) {
                    //error do something
                }
            });
        }, 2000);
    });
    $(document).ready(function () {

        //delegated submit handlers for the forms inside the table
        $('#new_family_registration_button').on('click', function (e) {
            e.preventDefault();
            $.fancybox.open([
                {
                    href: '#ajax_loader',
                    title: 'Please wait information being updated...'
                }
            ], {
                padding: 0
            });
            //read the form data ans submit it to someurl
            $.post('<?php echo base_url(); ?>index.php/reception/reception/new_family_tree', $('#new_family_registration_form').serialize(), function () {
                //success do something
                $(".loader_notify").notify(
                        "New Patient Added Successfully",
                        "success",
                        {position: "left"}
                );
                setInterval(function () {
                    var url = "<?php echo base_url(); ?>index.php/reception/reception/newpatvisit";
                    $(location).attr('href', url);
                }, 200);
            }).fail(function () {
                //error do something
                $(".loader_notify").notify(
                        "There was an error please try again later or  contact the system support desk  for assistance",
                        "error",
                        {position: "left"}
                );
            })
        })

    });
    $(document).ready(function () {

        $('.employmentstatus_yes').click(function () {

            // $("#txtAge").toggle(this.checked);
            $('.employers_name').css('display', 'inline');
        });
        $('.employmentstatus_no').click(function () {

            // $("#txtAge").toggle(this.checked);
            $('.employers_name').val('');
            $('.employers_name').css('display', 'none');
        });


        $('.employmentstatus_yes_1').click(function () {
            // $("#txtAge").toggle(this.checked);
            $('.employers_name_1').css('display', 'inline');
        });
        $('.employmentstatus_no_1').click(function () {
            // $("#txtAge").toggle(this.checked);
            $('.employers_name_1').val('');
            $('.employers_name_1').css('display', 'none');
        });

        //delegated submit handlers for the forms inside the table
        $('#edit_patient_information_button').on('click', function (e) {
            e.preventDefault();
            $.fancybox.open([
                {
                    href: '#ajax_loader',
                    title: 'Please wait information being updated...'
                }
            ], {
                padding: 0
            });
            //read the form data ans submit it to someurl
            $.post('<?php echo base_url(); ?>index.php/reception/reception/add', $('#register_new_patient_form').serialize(), function () {
                //success do something
                $(".loader_notify").notify(
                        "New Patient Added Successfully",
                        "success",
                        {position: "center"},
                {autoHideDelay: 2000}
                );
                setInterval(function () {
                    var url = "<?php echo base_url(); ?>index.php/reception/reception/newpatvisit";
                    $(location).attr('href', url);
                }, 2000);
            }).fail(function () {

                //error do something

                $(".loader_notify").notify(
                        "An error occured, please try again or contact the system support desk for assistance",
                        {autoHideDelay: 2000},
                "error",
                        {position: "center"}
                );
            })
        })

        //delegated submit handlers for the forms inside the table
        $('#add_register_new_patient').on('click', function (e) {
            e.preventDefault();
            $.fancybox.open([
                {
                    href: '#ajax_loader',
                    title: 'Please wait information being updated...'
                }
            ], {
                padding: 0
            });
            //read the form data ans submit it to someurl
            $.post('<?php echo base_url(); ?>index.php/reports/reports/edit_patient_info', $('#edit_patient_information_form').serialize(), function () {
                //success do something
                $(".loader_notify").notify(
                        "New Patient Added Successfully",
                        "success",
                        {position: "center"},
                {autoHideDelay: 2000}
                );
                setInterval(function () {
                    var url = window.pathname.location;
                    $(location).attr('href', url);
                }, 2000);
            }).fail(function () {

                //error do something

                $(".loader_notify").notify(
                        "An error occured, please try again or contact the system support desk for assistance",
                        {autoHideDelay: 2000},
                "error",
                        {position: "center"}
                );
            })
        })
    });
    $(document).ready(function () {





        $('#book_visit_button').on('click', function (e) {
            e.preventDefault();

            $.fancybox.open([
                {
                    href: '#ajax_loader',
                    title: 'Please wait information being updated...'
                }
            ], {
                padding: 0
            });
            //read the form data ans submit it to someurl
            $.post('<?php echo base_url() ?>index.php/reception/reception/new_visit', $('#add_visit_form').serialize(), function () {

                $(".loader_notify").notify(
                        "New Visit Added Successfully!!!",
                        "success",
                        {position: "left"}
                );
                setInterval(function () {
                    //var url = "<?php echo base_url() ?>index.php/home";
                    var url = window.location.href;
                    $(location).attr('href', url);
                }, 3000);
            }).fail(function () {
                //error do something
                $(".loader_notify").notify(
                        "There was an error please try again later or  contact the system support desk  for assistance",
                        "error",
                        {position: "left"}
                );
            })
        })




        $('#inputShwariFamilyNumber').keyup(function () {
            //get

            member_search = $('#inputShwariFamilyNumber').val();
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reception/reception/check_shari_member_existence/" + member_search,
                dataType: "json",
                success: function (response) {
                    shwari_family_number_result = $('#shwari_family_number_result').empty();
                    $.each(response, function (i, response) {

                        var str = response.family_base_number;
                        var res = str.substring(0, 14);
                        shwari_family_number_result.append('<span>' + res + '</span></br>');
                    });
                },
                error: function (data) {

                }
            })

        });
        $('#employmentstatus_yes').click(function () {
            // $("#txtAge").toggle(this.checked);
            $('#employer_1').css('display', 'inline');
        });
        $('#employmentstatus_no').click(function () {
            // $("#txtAge").toggle(this.checked);
            $('#employer_1').css('display', 'none');
        });


        //delegated submit handlers for the forms inside the table
        $('#add_walkin_patient_button').on('click', function (e) {
            e.preventDefault();
            $.fancybox.open([
                {
                    href: '#ajax_loader',
                    title: 'Please wait information being updated...'
                }
            ], {
                padding: 0
            });
            //read the form data ans submit it to someurl
            $.post('<?php echo base_url() ?>index.php/reception/reception/add_walkin', $('#add_walkin_patient_form').serialize(), function () {
                //success do something
                //alert("Success New Patient registered Successfully");
                // $.notify("New Patient Added Succesfully", "success",{ position:"left" });
                $(".loader_notify").notify(
                        "New Walkin Patient Added Successfully",
                        "success",
                        {position: "center"}
                );
                setInterval(function () {
                    var url = window.location.href;
                    $(location).attr('href', url);
                }, 3000);


            }).fail(function () {
                //error do something
                $(".loader_notify").notify(
                        "There was an error please try again later or  contact the system support desk  for assistance",
                        "error",
                        {position: "center"}
                );

            })
        })


        //delegated submit handlers for the forms inside the table
        $('#book_patient_procedure_button').on('click', function (e) {
            e.preventDefault();
            $.fancybox.open([
                {
                    href: '#ajax_loader',
                    title: 'Please wait information being updated...'
                }
            ], {
                padding: 0
            });
            //read the form data ans submit it to someurl
            $.post('<?php echo base_url() ?>index.php/reception/add_procedure', $('#book_patient_procedure_form').serialize(), function () {
                //success do something
                //alert("Success New Patient registered Successfully");
                // $.notify("New Patient Added Succesfully", "success",{ position:"left" });
                $(".loader_notify").notify(
                        "Patient Booked for Procedure ",
                        "success",
                        {position: "center"}
                );
                setInterval(function () {
                    var url = window.location.href;
                    $(location).attr('href', url);
                }, 3000);


            }).fail(function () {
                //error do something
                $(".loader_notify").notify(
                        "There was an error please try again later or  contact the system support desk  for assistance",
                        "error",
                        {position: "center"}
                );

            })
        })

        //delegated submit handlers for the forms inside the table
        $('#shwari_patient_book_procedure_button').on('click', function (e) {
            e.preventDefault();
            $.fancybox.open([
                {
                    href: '#ajax_loader',
                    title: 'Please wait information being updated...'
                }
            ], {
                padding: 0
            });
            //read the form data ans submit it to someurl
            $.post('<?php echo base_url() ?>index.php/reception/reception/add_procedure', $('#shwari_patient_book_procedure_form').serialize(), function () {
                //success do something
                //alert("Success New Patient registered Successfully");
                // $.notify("New Patient Added Succesfully", "success",{ position:"left" });
                $(".loader_notify").notify(
                        "Patient Booked for Procedure ",
                        "success",
                        {position: "center"}
                );
                setInterval(function () {
                    var url = window.location.href;
                    $(location).attr('href', url);
                }, 3000);


            }).fail(function () {
                //error do something
                $(".loader_notify").notify(
                        "There was an error please try again later or  contact the system support desk  for assistance",
                        "error",
                        {position: "center"}
                );

            })
        })




        $('.edit_patient_link').click(function () {


            //get data
            var patient_id = $(this).closest('tr').find('input[name="view_patient_id"]').val();

            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/reports/get_patient_reports_details/" + patient_id,
                dataType: "json",
                success: function (response) {
                    $('#edit_patient_id').val(response[0].patient_id);
                    $('#edit_title').val(response[0].title);
                    $('#edit_phone_no').val(response[0].phone_no);
                    $('#edit_inputFirstName').val(response[0].f_name);
                    $('#edit_inputSurName').val(response[0].s_name);
                    $('#edit_inputOtherName').val(response[0].other_name);
                    $('#edit_dob').val(response[0].dob);
                    $('#edit_gender').val(response[0].gender);
                    $('#edit_maritalstatus').val(response[0].marital_status);
                    $('#edit_nationalid').val(response[0].identification_number);
                    $('#edit_email').val(response[0].email);
                    $('#edit_residence').val(response[0].residence);
                    $('#edit_employement_status').val(response[0].employment_status);
                    $('#edit_employers_name').val(response[0].employer);
                    $('#edit_kinname').val(response[0].next_of_kin_fname);
                    $('#edit_kinsname').val(response[0].next_of_kin_lname);
                    $('#edit_kinrelation').val(response[0].next_of_kin_relation);
                    $('#edit_kinphone').val(response[0].next_of_kin_phone);
                    $('#edit_family_number').val(response[0].family_base_number);
                },
                error: function (data) {

                }
            })

        });



        $('.view_patient_link').click(function () {


            //get data
            var patient_id = $(this).closest('tr').find('input[name="view_patient_id"]').val();

            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/reports/get_patient_reports_details/" + patient_id,
                dataType: "json",
                success: function (response) {
                    $('#view_patient_id').val(response[0].patient_id);
                    $('#view_title').val(response[0].title);
                    $('#view_phone_no').val(response[0].phone_no);
                    $('#view_inputFirstName').val(response[0].f_name);
                    $('#view_inputSurName').val(response[0].s_name);
                    $('#view_inputOtherName').val(response[0].other_name);
                    $('#view_dob').val(response[0].dob);
                    $('#view_gender').val(response[0].gender);
                    $('#view_maritalstatus').val(response[0].marital_status);
                    $('#view_nationalid').val(response[0].identification_number);
                    $('#view_email').val(response[0].email);
                    $('#view_residence').val(response[0].residence);
                    $('#view_employement_status').val(response[0].employment_status);
                    $('#view_employers_name').val(response[0].employer);
                    $('#view_kinname').val(response[0].next_of_kin_fname);
                    $('#view_kinsname').val(response[0].next_of_kin_lname);
                    $('#view_kinrelation').val(response[0].next_of_kin_relation);
                    $('#view_kinphone').val(response[0].next_of_kin_phone);
                    $('#view_family_number').val(response[0].family_base_number);
                },
                error: function (data) {

                }
            })

        });



    });</script>





<!--<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>-->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#datepicker").datepicker({
            minDate: '-100Y',
            maxDate: '+0m +0w',
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true
        });
        $("#datepcker").datepicker({minDate: '-100Y',
            maxDate: '+0m +0w',
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true});
        $("#date_to").datepicker({minDate: '-100Y',
            maxDate: '+0m +0w',
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true});
        $("#date_from").datepicker({minDate: '-100Y',
            maxDate: '+0m +0w',
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true});
        $("#visitation_date_to").datepicker({minDate: '-100Y',
            maxDate: '+0m +0w',
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true});
        $("#visitation_date_from").datepicker({minDate: '-100Y',
            maxDate: '+0m +0w',
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true});
        $("#walkin_date_to").datepicker({minDate: '-100Y',
            maxDate: '+0m +0w',
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true});
        $("#walkin_date_from").datepicker({minDate: '-100Y',
            maxDate: '+0m +0w',
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true});
    });</script>




<script type="text/javascript">
    $(function () {
        $("#datepicker_1").datepicker({
            minDate: '-100Y',
            maxDate: '+0m +0w',
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true
        });
        $("#datepcker_1").datepicker({dateFormat: 'yy-mm-dd'});
    });
</script>



</body>
</html>
