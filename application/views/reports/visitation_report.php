<?php $this->load->view('header'); ?>
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Reports</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Visitation Report</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered visitation_report  responsive">
                    <thead>
                        <tr>

                            <th>Visit Date</th>
                            <th>Patient Name</th>
                            <th>DoB</th>
                            <th>Gender</th>
                            <th>Phone No</th>
                            <th>Email</th>
                            <th>Service Offered</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            foreach ($visitations as $value) {
                                ?>
                                <td class="center">
                                    <?php
                                    echo $value['date_added'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['f_name'] . $value['s_name'] . $value['other_name'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['dob'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['gender']; ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['phone_no'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['email'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    $status = $value['package_name'];
                                    echo $status;
                                 
                                    ?>
                                </td>


                                <td class="center">
                                    <a class="" href="<?php echo base_url(); ?>reports/view_visitation/<?php echo $value['visit_id']; ?>">
                                        <i class="glyphicon glyphicon-zoom-in icon-white"></i>
                                        View
                                    </a>
                                    <a class="" href="#">
                                        <i class="glyphicon glyphicon-edit icon-white"></i>
                                        Edit
                                    </a>
                                    <a class="" href="#">
                                        <i class="glyphicon glyphicon-trash icon-white"></i>
                                        Delete
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>




                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<?php $this->load->view('footer'); ?>