<!--Bread-crumb div starts here --> <div>
    <ul class = "breadcrumb">
        <li>
            <a href = "<?php echo base_url(); ?>">Home</a>
        </li>
        <li>
            <a href = "<?php echo base_url(); ?>">Nurse</a>
        </li>
    </ul>
</div>
<!--Bread-crumb div ends here -->

<div class = " row">
    <div class = "col-md-2 col-sm-4 col-xs-6">
        <a data-toggle = "tooltip" title = "Total Regular Payment Today" class = "well top-block" href = "#">
            <i class = "glyphicon glyphicon-user blue"></i>

            <div>Total Regular Patients In-Tray Today </div>
            <div><span id="total_regular_patients_in_tray_today" class="total_regular_patients_in_tray_today"></span></div>

        </a>
    </div>

    <div class = "col-md-2 col-sm-3 col-xs-6">
        <a data-toggle = "tooltip" title = "Total Walk-in Patient Payment Today" class = "well top-block" href = "#">
            <i class = "glyphicon glyphicon-user green"></i>

            <div>Today Walk-in Patients In-Tray </div>
            <div><span id="total_walkin_patients_in_tray_today"class="total_walkin_patients_in_tray_today"></span></div>

        </a>
    </div>

    <div class = "col-md-2 col-sm-3 col-xs-6">
        <a data-toggle = "tooltip" title = "Total Appointments Today" class = "well top-block" href = "#">
            <i class = "glyphicon glyphicon-user green"></i>

            <div>Today Appointments To-day </div>
            <div><span id="total_appointments_today"class="total_appointments_today"></span></div>

        </a>
    </div>








</div>




<div class = "row">
    <div class = "box col-md-12">
        <div class = "box-inner">
            <div class = "box-header well">
                <h2><i class = "glyphicon glyphicon-info-sign"></i> Nurse </h2>

                <div class = "box-icon">

                    <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                            class = "glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class = "box-content row">


                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Regular Patients In Tray</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list regular_patients_in_tray" id="regular_patients_in_tray"></ul>
                               <ul class = "dashboard-list regular_patients_in_tray_express" id="regular_patients_in_tray_express">    
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->





                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Walk-in Patients In Tray</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list walkin_patients_in_tray" id="walkin_patients_in_tray">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->


                <!--Appointments Start-->
                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Appointments Today</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list appointments_today" id="appointments_today">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--Appointments End-->



            </div>
        </div>
    </div>
</div>

<div class = "row">


</div><!--/row-->

<div class = "row">


</div><!--/row-->
<!--content ends -->
</div><!--/#content.col-md-0-->
</div><!--/fluid-row-->













</div><!--/row-->