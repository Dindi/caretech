<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?php echo base_url(); ?>">Home</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>">Patient Profile </a>
        </li>
    </ul>
</div>




<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-user"></i> Patient Bio Data </h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-7 col-md-12">

                    <table>
                        <tbody>
                            <?php
                            foreach ($patient_bio as $patient_bio_data) {
                                ?>
                                <tr>`
                                    <td> <h5>Name : <?php echo $patient_bio_data['patient_name']; ?> </h5></td>
                            <input type="hidden" name="hidden_patient_name" class="hidden_patient_name" id="hidden_patient_name" value="<?php echo $patient_bio_data['patient_name']; ?>"/>
                            <td><h6>EMR No : <?php echo $patient_bio_data['family_number']; ?> </h6></td>
                            <td> <h6 class="">Age : <?php
                                    $dob = $patient_bio_data['dob'];
                                    $age = date_diff(date_create($dob), date_create('now'))->y;

                                    if ($age <= 0) {
                                        $bage = date_diff(date_create($dob), date_create('now'))->d;
                                        $nage = $bage . " Days Old";
                                    } elseif ($age > 0) {
                                        $nage = $age . " Years Old";
                                    }
                                    echo $nage;
                                    ?> </h6><td>

                            <td> <h6>Sex : <?php
                                    echo $patient_bio_data['gender'];
                                    ?>
                                </h6> </td>
                            <td> <h6>Residence : <?php
                                    echo $patient_bio_data['residence'];
                                    ?>
                                </h6> </td>
                            <td><span class="label">Allergies : </span><h6 id="patient_allergies_list" class="patient_allergies_list"></h6></td>
                            <td> <a href="#send_to_doctor" class=" send_to_doctor_link btn btn-xs btn-info" id="send_to_doctor_link">
                                    <i class="glyphicon  icon-white"></i>
                                    Send To Doctor : 
                                </a></td>
                            <td> 
                                <form class="send_to_pharmacy_form" id="send_to_pharmacy_form">
                                    <input type="hidden" name="send_to_pharm_visit_id" id="send_to_pharm_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="send_to_pharm_visit_id"/>
                                    <input type="submit" name="send_to_pharmacy_link" value=" Send To Pharmacy : " id="send_to_pharmacy_link" class=" send_to_pharmacy_link btn btn-xs btn-info"/>
                                </form>
                            </td>
                            <td> 
                                <a id="release_patient_link" href="#notification_release_patient" class="release_patient_link btn btn-xs btn-danger">Release Patient</a>

                            </td>
                            </tr>
                            <?php
                        }
                        ?> 

                        </tbody>
                    </table>

                </div>

                <div id="notification_release_patient" class="notification_release_patient" style="display: none;">




                    <div class = "box col-md-12">
                        <div class = "box-inner">
                            <div class = "box-header well" data-original-title = "">
                                <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                <div class = "box-icon">

                                </div>
                            </div>

                            <div class = "box-content">
                                <label class = "">
                                    Are you sure you want to release the  Patient ? 
                                </label><br>
                                <table>
                                    <tr>
                                        <td>
                                            <form class="release_patient_form" id="release_patient_form">
                                                <input type="hidden" name="release_patient_visit_id" id="release_patient_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="relaease_patient_visit_id"/>
                                                <input type="submit" name="release_patient_link" value="Yes " id="yes_release_patient" class=" yes_release_patient btn btn-xs btn-danger"/>
                                            </form>
                                        </td>
                                        <td>

                                            <a id = "relase_patient_no" href = "#relase_patient_no" class = " relase_patient_no btn btn-default btn-xs ">
                                                No
                                            </a>
                                        </td>
                                    </tr>
                                </table>




                            </div>
                        </div>
                    </div>
                    <!--/span-->



                </div>


            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-th"></i> Patient Records</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                </div>
            </div>
            <div class="box-content">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"> <a href="#reason_for_visit">Reason for Visit</a></li>
                    <li > <a href="#vitals">Vitals</a></li>
                    <li> <a href="#allergies">Allergies</a></li>
                    <li> <a href="#tobacco_social">Tobacco/Social History</a></li>
                    <li><a href="#all_records_tab">All History</a></li>
                    <li><a href="#immunization">Immunization</a></li>
                    <li ><a href="#triage_tab">Triage</a></li>
                    <li><a href="#lab_tab">Lab</a></li>
                    <li><a href="#consultation_tab">Consultation</a></li>
                    <li><a href="#prescription_tab">Prescription</a></li>
                    <!--<li><a href="#appointment_form">Appointment</a></li>-->
                </ul>

                <div id="myTabContent" class="tab-content">
                    <!--Reason for visit tab start -->
                    <div class="tab-pane active" id="reason_for_visit">
                        <form class=" reason_for_visit_form" id="reason_for_visit_form" role="form">

                            <input type="hidden" name="checking_patient_id" class="checking_patient_id" id="checking_patient_id" value="<?php echo $this->uri->segment(3); ?>"/>
                            <input type="hidden" name="checking_visit_id" class="checking_visit_id" id="checking_visit_id" value="<?php echo $this->uri->segment(4); ?>"/>


                            <input type="hidden" name="reason_for_visit_patient_id_1" class="patient_id_1" id="patient_id_1" value="<?php echo $this->uri->segment(3); ?>"/>
                            <input type="hidden" name="reason_for_visit_visit_id" class="visit_id" id="visit_id" value="<?php echo $this->uri->segment(4); ?>"/>
                            <hr>
                            <div class="form-inline">
                                <div class="form-group">
                                    <span class="label label-default sr-only"> Reason For Visit :  </span>
                                </div>
                                <div class="form-group">
                                    <textarea class=" form-control textarea reason_for_visit_txt_area" placeholder="Reason for Visit : " rows="2" cols="100" name="reason_for_visit_txt_area" id="reason_for_visit_txt_area"></textarea>
                                </div>
                            </div>
                            <hr>
                            <div class="form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "medication">Medication : </label>
                                    <textarea  class = "form-control medication_txt_area" name="medication_txt_area"  rows="2" cols="100" id="medication_txt_area"  placeholder = "Medication"></textarea>
                                </div>
                            </div>
                            <hr>
                            <div class="form-inline">
                                <div class = "form-group">
                                    <label>
                                        <input type="checkbox" name="urgency" value="urgent" id="Emergency_0" class="checkbox Emergency_0">
                                        Urgent</label>
                                </div> 
                            </div>









                        </form>

                    </div>
                    <!-- Reason for visit end -->
                    <!-- Vital signs start -->
                    <div class="tab-pane" id="vitals">


                        <div class = "row">
                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Add Patient Triage Form</h2>

                                        <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                    class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                    </div>
                                    <div class = "box-content">

                                        <div class = "bs-example">


                                            <form class = "add_patient_triage_form " autocomplete="off" method = "post"  id = "add_patient_triage_form" role = "form">

                                                <input type="hidden" class="form-control add_triage_patient_id" id="add_triage_patient_id" value="<?php echo $this->uri->segment(3); ?>" name="add_triage_patient_id" />
                                                <input type="hidden" class="form-control add_triage_visit_id" id="add_triage_visit_id" value="<?php echo $this->uri->segment(4); ?>" name="add_triage_visit_id"/>

                                                <hr>
                                                <div class = "form-inline">
                                                    <div class = "form-group">
                                                        <label class = "sr-only" for = "weight">Weight(Kgs) : </label>
                                                        <input type = "number" min="1" class = "form-control" id = "weight" name="weight" placeholder = "Weight(Kgs) :">
                                                    </div>

                                                    <div class = "form-group">
                                                        <label class = "sr-only" for = "systolic_diastolic">Systolic/Diastolic : </label>
                                                        <input type = "number" class = "form-control" id = "systolic" name="systolic" placeholder = "Systolic">/
                                                        <input type = "number" class = "form-control" id = "diastolic" name="diastolic" placeholder = "Diastolic">
                                                    </div>


                                                    <div class = "form-group">
                                                        <label class = "sr-only" for = "temperature">Temperature(°C)</label>
                                                        <input type = "number" class = "form-control temperature" name="temperature" id = "temperature" placeholder = "Temperature(°C)">
                                                    </div>

                                                    <div class = "form-group">
                                                        <label class = "sr-only" for = "height">Height (cm)</label>
                                                        <input type = "number" class = "form-control" name="height" id = "height" placeholder = "Height (cm) ">
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class = "form-inline">
                                                    <div class = "form-group">
                                                        <label class = "sr-only" for = "respiratory">Respiratory (No/Min)</label>
                                                        <input type = "number" class = "form-control" name="respiratory" id = "respiratory" placeholder = "Respiratory (No/Min)">
                                                    </div>

                                                    <div class = "form-group">
                                                        <label class = "sr-only" for = "blood_sugar">Blood Sugar (Mmol/Ltr):</label>
                                                        <input type = "number" class = "form-control" name="blood_sugar" id = "blood_sugar" placeholder = "Blood Sugar (Mmol/Ltr)">
                                                    </div>

                                                    <div class = "form-group">
                                                        <label class = "sr-only" for = "pulse_rate">Pulse Rate (Bpm):</label>
                                                        <input type = "number" class = "form-control" name="pulse_rate" id = "pulse_rate" placeholder = "Pulse Rate (Bpm)">
                                                    </div>

                                                    <?php
                                                    foreach ($patient_bio as $patient_bio_data) {
                                                        $gender = $patient_bio_data['gender'];
                                                        if ($gender === "Female") {
                                                            ?>
                                                    <div class = "form-group" id="LMP_div">
                                                                <label class = "sr-only" for = "LMP">LMP Date(YYYY-MM-DD):</label>
                                                                <input type = "text" class = "form-control" name="LMP" id = "LMP" placeholder = "LMP Date(YYYY-MM-DD)">
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>

                                                            <?php
                                                        }
                                                        ?>

                                                        <?php
                                                    }
                                                    ?>

                                                </div>








                                            </form>




                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->

                        </div><!--/row-->
                    </div>
                    <!-- Vital signs end -->
                    <!-- Allergies start -->
                    <div class="tab-pane" id="allergies">

                        <form class="add_allergy_form" id="add_allergy_form">
                            <input type="hidden" name="add_allergy_patient_id" id="" value="<?php echo $this->uri->segment(3); ?>"/>
                            <input type="hidden" name="add_allergy_visit_id" id="" value="<?php echo $this->uri->segment(4); ?>"/>
                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "allergy">Allergy : </label>
                                    <textarea  class = "form-control" name="allergy" id ="allergy"  placeholder = "Allergy"></textarea>
                                </div>
                            </div>
                            <hr>

                        </form>

                    </div>
                    <!-- Allergies end -->
                    <!-- Tobbacco Social Tab start-->

                    <div class="tab-pane" id="tobacco_social">
                        <form class="add_family_social_form" id="add_family_social_form">
                            <input type="hidden" name="add_family_social_patient_id" id="" value="<?php echo $this->uri->segment(3); ?>"/>
                            <input type="hidden" name="add_family_social_visit_id" id="" value="<?php echo $this->uri->segment(4); ?>"/>

                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "family_social">Family Social History  : </label>
                                    <textarea  class = "form-control" name="family_social" id ="family_social"  placeholder = "Family Social History"></textarea>
                                </div>
                            </div>
                            <hr>


                        </form>
                    </div>

                    <!-- Tobbacco Social Tab end --> 

                    <!-- Family Social History form tab start -->

                    <div class="tab-pane" id="family_social_history_form_tab">
                        <form class="family_social_history_form" id="family_social_history_form">
                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "allergy">Family Social History  : </label>
                                    <textarea  class = "form-control" name="family_social" id ="family_social"  placeholder = "Family Social History"></textarea>
                                </div>
                            </div>
                            <hr>
                            <div class="form-inline">
                                <div class="form-group">
                                    <input type="submit" name="add_family_social_button" class="add_family_social_button btn btn-success btn-xs" id="add_allergy_button" value="Save"/>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- Family Social History  form tab  end -->

                    <!--All records tab start -->

                    <div class="tab-pane" id="all_records_tab">

                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseTriage"><i class="glyphicon glyphicon-plus"></i> 
                                        Triage Records : 
                                    </a>
                                </div>

                                <div id="collapseTriage" class="accordion-body collapse">

                                    <?php
                                    foreach ($patient_triage_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo 'triage' . $value['triage_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i> <span> <?php
                                                                $weight = $value['weight'];
                                                                $height = $value['height'];
                                                                $one_hundred = 100;
                                                                $height = $height / $one_hundred;
                                                                $new_height = $height * $height;


                                                                if ($new_height == 0) {
                                                                    ?>
                                                                    <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                                                    <?php
                                                                } else {
                                                                    $BMI = $weight / $new_height;
                                                                    if ($BMI <= 20) {
                                                                        ?>
                                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                    <?php } ?>

                                                                    <?php if ($BMI >= 25) { ?>
                                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                    <?php } ?>

                                                                    <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                                        <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>

                                                            </span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo 'triage' . $value['triage_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">


                                                            <ul class="list-inline">
                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Visit Date :</label>  <?php
                                                            echo $value['visit_date'];
                                                                ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                echo $value['employee_name'];
                                                                ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Respiratory Rate :(No/Min)</label>  <?php
                                                                echo $value['respiratory_rate'];
                                                                ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Pulse Rate :(No/Min)</label> <?php
                                                                echo $value['pulse_rate'];
                                                                ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Blood Pressure :(No/Min)</label> <?php
                                                                echo $value['diastolic'] . " / " . $value['systolic'];
                                                                ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Temperature :(°C)</label> <?php
                                                                echo $value['temperature'];
                                                                ?></li>
                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Height :(Cm)</label> <?php
                                                                echo $value['height'];
                                                                ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Weight :(Kg)</label> <?php
                                                                echo $value['weight'];
                                                                ?></li>

                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                        <hr>


                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseLab"><i class="glyphicon glyphicon-plus"></i> 
                                        Lab Records : 
                                    </a>
                                </div>

                                <div id="collapseLab" class="accordion-body collapse">

                                    <?php
                                    foreach ($patient_lab_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['lab_test_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php
                                    echo $value['date_added'] . '   Test Name : ' . $value['test_name'];
                                        ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['lab_test_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                            echo $value['employee_name'];
                                        ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Test Name</label>  <?php
                                                                echo $value['test_name'];
                                        ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Test Results : </label> <?php
                                                                echo $value['test_results'];
                                        ?></li>


                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <hr>

                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseConsultation"><i class="glyphicon glyphicon-plus"></i> 
                                        Consultation Records : 
                                    </a>
                                </div>

                                <div id="collapseConsultation" class="accordion-body collapse">

                                    <?php
                                    foreach ($patient_consultation_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['consultation_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php echo $value['date']; ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['consultation_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                    echo $value['employee_name'];
                                        ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Chief Complaint : </label>  <?php
                                                                echo $value['complaints'];
                                        ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Working Diagnosis :</label> <?php
                                                                echo $value['working_diagnosis'];
                                        ?></li>




                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                        <hr>


                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapsePrescription"><i class="glyphicon glyphicon-plus"></i> 
                                        Prescription Records : 
                                    </a>
                                </div>

                                <div id="collapsePrescription" class="accordion-body collapse">

                                    <?php
                                    foreach ($patient_prescription_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['prescription_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php echo $value['date']; ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['prescription_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                    echo $value['employee_name'];
                                        ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Commodity Name : </label>  <?php
                                                                echo $value['commodity_name'];
                                        ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Strength :</label> <?php
                                                                echo $value['strength'];
                                        ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Route : </label> <?php
                                                                echo $value['route'];
                                        ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Frequency : </label> <?php
                                                                echo $value['frequency'];
                                        ?></li>
                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Duration : </label> <?php
                                                                echo $value['duration'];
                                        ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">No of Days : </label> <?php
                                                                echo $value['no_of_days'];
                                        ?></li>



                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                    </div>

                    <!--All records tab end -->

                    <div class="tab-pane" id="immunization">
                        <form class="add_immunization_form" id="add_immunization_form">
                            <input type="hidden" name="add_immunization_patient_id" id="" value="<?php echo $this->uri->segment(3); ?>"/>
                            <input type="hidden" name="add_immunization_visit_id" id="" value="<?php echo $this->uri->segment(4); ?>"/>

                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "immunization_txt_area">Immunization   : </label>
                                    <textarea  class = "form-control immunization_txt_area" name="immunization" id ="immunization_txt_area"  placeholder = "Immunization"></textarea>
                                </div>
                            </div>
                            <hr>
                            <div class="form-inline">
                                <div class="form-group">
                                    <input type="submit" name="add_immunization_button" class="add_immunization_button btn btn-success btn-xs" id="add_immunization_button" value="Save"/>
                                </div>
                            </div>

                        </form>
                    </div>

                    <!--Book Appointment Tab -->
                    <!--                    <div class="tab-pane" id="appointment_form">
                    
                                            <div class = "row">
                                                <div class = "box col-md-12">
                                                    <div class = "box-inner">
                                                        <div class = "box-header well" data-original-title = "">
                                                            <h2><i class = "glyphicon glyphicon-edit"></i> Book Appointment</h2>
                    
                                                            <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                                        class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                                        </div>
                                                        <div class = "box-content">
                    
                                                            <div class = "bs-example">
                    
                    
                                                                <form class = "appointment_form_book" autocomplete="off" method = "post"  id = "appointment_form_book" role = "form">
                    
                                                                    <input type="hidden" class="form-control appointment_patient_id" id="appointment_patient_id" value="<?php echo $this->uri->segment(3); ?>" name="appointment_patient_id" />
                                                                    <input type="hidden" class="form-control appointment_visit_id" id="appointment_visit_id" value="<?php echo $this->uri->segment(4); ?>" name="appointment_visit_id"/>
                    
                                                                    <hr>
                                                                    <div class = "form-inline">
                                                                        <div class = "form-group">
                                                                            <label class = "sr-only" for = "appointment_type">Appointment Type : </label>
                                                                            <input type="text" class="form-control appointment_type_1" id="appointment_type_1"placeholder="Appointment Type : " name="appointment_type"/>
                                                                            <input type = "text"  class = "form-control appointment_type"  id = "appointment_type" name="appointment_type" placeholder = "Appointment Type :">
                                                                        </div>
                                                                    </div>
                                                                    <div class = "form-inline">
                                                                        <div class = "form-group">
                                                                            <label class = "sr-only" for = "person_to_see">Person to See : </label>
                                                                            <input type = "text" class = "form-control person_to_see" id = "person_to_see" name="person_to_see" placeholder = "Person to See : ">
                                                                        </div>
                                                                    </div>
                                                                    <div class = "form-inline">
                    
                    
                    
                                                                        <div class="form-group">
                                                                            <div class='input-group date' id='datetimepicker1'>
                                                                                <input type='text' placeholder="Date From : " required="" name="date_from" id="date_from" class="form-control" />
                                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                    
                                                                        <div class="form-group">
                                                                            <div class='input-group date' id='datetimepicker2'>
                                                                                <input type='text' placeholder="Date To : " required="" name="date_to" id="date_to" class="form-control" />
                                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                    
                    
                                                                    </div>
                                                                    <div class = "form-inline">
                                                                        <div class = "form-group">
                                                                            <label class = "sr-only" for = "reason_for_appointment">Reason for Appointment </label>
                                                                            <textarea class=" form-control" rows="5" cols="100" placeholder="Reason for Appointment: " name="reason_for_appointment" id="reason_for_appointment"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                    
                    
                    
                                                                    <div class = "form-inline">
                                                                        <div class = "form-group">
                                                                            <input type = "submit" value = "Save" class = "btn btn-success btn-xs book_appointment_form_button" id = "book_appointment_form_button"/>
                                                                            <input type = "reset" value = "Reset" class = "btn btn-danger btn-xs" id = "save_reset"/>
                                                                        </div>
                                                                    </div>
                    
                                                                </form>
                    
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                /span
                    
                                            </div>/row
                                        </div>-->
                    <!--Book Appointment Tab end -->
                    <!---Triage Tab --->

                    <div class="tab-pane" id="triage_tab">

                        <a  id="export_triage_patient_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Walkin Patient Report" class="export_triage_patient_report_filter_link" href="#export_triage_report_filter_form">
                            <i class="glyphicon glyphicon-download-alt"></i>
                        </a>
                        |



                        <table class="table table-striped table-bordered triage_report  responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Date </th>
                                    <th>Nurse Name</th>
                                    <th>Respiratory (No/Min)</th>
                                    <th>Pulse Rate (No/Min)</th>
                                    <th>Blood pressure (mmHg)</th>
                                    <th>Temperature (°C)</th>
                                    <th>Height (Cm)</th>
                                    <th>Weight (KGs)</th>
                                    <th>BMI</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php
                                    $i = 1;
                                    foreach ($patient_triage_records as $value) {
                                        ?>
                                        <td class="center"><?php echo $i; ?></td>
                                        <td class="center">
                                            <?php
                                            echo $value['visit_date'];
                                            ?>
                                        </td>
                                        <td class="center">
                                            <?php
                                            echo $value['employee_name'];
                                            ?>
                                        </td>
                                        <td class="center">
                                            <?php
                                            echo $value['respiratory_rate'];
                                            ?>
                                        </td>
                                        <td class="center">
                                            <?php echo $value['pulse_rate']; ?>
                                        </td>
                                        <td class="center">
                                            <?php echo $value['diastolic'] . "/" . $value['systolic']; ?>
                                        </td>
                                        <td class="center">
                                            <?php echo $value['temperature']; ?>
                                        </td>
                                        <td class="center">
                                            <?php echo $value['height']; ?>
                                        </td>
                                        <td class="center">
                                            <?php echo $value['weight']; ?>
                                        </td>

                                        <td class="center" ><?php
                                            $weight = $value['weight'];
                                            $height = $value['height'];
                                            $one_hundred = 100;
                                            $height = $height / $one_hundred;
                                            $new_height = $height * $height;


                                            if ($new_height == 0) {
                                                ?>
                                                <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                                <?php
                                            } else {
                                                $BMI = $weight / $new_height;
                                                if ($BMI <= 20) {
                                                    ?>
                                                    <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                <?php } ?>

                                                <?php if ($BMI >= 25) { ?>
                                                    <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                <?php } ?>

                                                <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                    <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                    <?php
                                                }
                                            }
                                            ?>



                                        </td> 



                                        <td class="center">
                                            <input type="hidden" name="view_triage_patient_id" class="view_triage_patient_id" id="view_triage_patient_id" value="<?php echo $value['patient_id']; ?>"/>
                                            <input type="hidden" name="view_triage_id" class="view_triage_id" id="view_triage_id" value="<?php echo $value['triage_id']; ?>"/>
                                            <input type="hidden" name="view_triage_visit_id" class="view_triage_visit_id" id="view_triage_visit_id" value="<?php echo $value['visit_id']; ?>"/>
                                            <a  id="edit_triage_patient_link" class="edit_triage_patient_link" href="#edit_triage_form">
                                                <i class="glyphicon glyphicon-edit icon-white"></i>

                                            </a>|
                                            <a  id="delete_triage_patient_link" class="delete_triage_patient_link" href="#delete_triage_form">
                                                <i class="glyphicon glyphicon-trash icon-white"></i>

                                            </a>
                                        </td>
                                    </tr>

    <?php
    $i++;
}
?>




                            </tbody>
                        </table>


                    </div>
                    <div class="tab-pane" id="lab_tab">

                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                            <thead>
                                <tr>
                                    <th>Test Date</th>
                                    <th>Doctor Name</th>
                                    <th>Test Name</th>
                                    <th>Test Results </th>

                                </tr>
                            </thead>
                            <tbody>
<?php foreach ($patient_lab_records as $value) {
    ?>
                                    <tr>
                                        <td class="center"><?php echo $value['date_added']; ?></td>
                                        <td class="center"><?php echo $value['employee_name']; ?></td>
                                        <td class="center"><?php echo $value['test_name']; ?></td>
                                        <td class="center">
    <?php echo $value['test_results']; ?>
                                        </td>

                                    </tr>
<?php }
?>


                            </tbody>
                        </table>

                    </div>

                    <div class="tab-pane" id="consultation_tab">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                            <thead>
                                <tr>
                                    <th>Visit Date</th>
                                    <th> Chief Complaints</th>
                                    <th>Diagnosis</th>
                                    <th>Employee Name</th>

                                </tr>
                            </thead>
                            <tbody>
<?php foreach ($patient_consultation_records as $value) {
    ?>
                                    <tr>
                                        <td><?php echo $value['date']; ?></td>
                                        <td class="center"><?php echo $value['complaints']; ?></td>
                                        <td class="center"> <?php echo $value['working_diagnosis']; ?></td>
                                        <td class="center"<?php echo $value['employee_name']; ?>></td>

                                    </tr>
    <?php
}
?>


                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="prescription_tab">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                            <thead>
                                <tr>
                                    <th>Visit Date</th>
                                    <th>Commodity Name</th>
                                    <th>Strength</th>
                                    <th>Route </th>
                                    <th>Frequency</th>
                                    <th>Duration</th>
                                    <th>Employee Name</th>

                                </tr>
                            </thead>
                            <tbody>
<?php foreach ($patient_prescription_records as $value) {
    ?>
                                    <tr>
                                        <td><?php echo $value['date']; ?></td>
                                        <td class="center"><?php echo $value['commodity_name']; ?></td>
                                        <td class="center"><?php echo $value['strength']; ?></td>
                                        <td class="center"><?php echo $value['route']; ?> </td>
                                        <td class="center"><?php echo $value['frequency']; ?></td>
                                        <td class="center"> <?php echo $value['duration'] . $value['no_of_days']; ?></td>
                                        <td class="center"> <?php echo $value['employee_name']; ?></td>

                                    </tr>
<?php }
?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/span-->


</div><!--/row-->













































<div class="all" style="display: none;">



    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2>Patient Bio Data</h2>
                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <div class="well">
<?php
foreach ($patient_bio as $patient_bio_data) {
    
}
?>
                    <h5>Patient Name : <?php echo $patient_bio_data['patient_name']; ?> </h5>
                    <input type="hidden" name="hidden_patient_name" class="hidden_patient_name" id="hidden_patient_name" value="<?php echo $patient_bio_data['patient_name']; ?>"/>
                    <hr>
                    <h6 class="">Age : <?php
                    $dob = $patient_bio_data['dob'];
                    $age = date_diff(date_create($dob), date_create('now'))->y;

                    if ($age <= 0) {
                        $bage = date_diff(date_create($dob), date_create('now'))->d;
                        $nage = $bage . " Days Old";
                    } elseif ($age > 0) {
                        $nage = $age . " Years Old";
                    }
                    echo $nage;
?> </h6>
                    <hr>
                    <h6>Residence : <?php echo $patient_bio_data['residence']; ?> </h6>

                    <a class="btn btn-info btn-xs" href="#send_to_doctor">Send to Doctor</a>
                </div>
            </div>
        </div>
    </div>

    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2>Patient Allergies </h2>
                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <div class="well">
                    <p id="patient_allergies_list" class="patient_allergies_list"></p>
                </div>
            </div>
        </div>
    </div>


    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2>Patient </h2>
                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <div class="well">
                    <a href="#nurse_patient_list_div" id="nurse_view_patient_list" class="nurse_view_patient_list btn btn-xs btn-success "><i class="glyphicon glyphicon-zoom-in"></i>View Patients List</a>


                    <hr>
                    <a class="btn btn-info btn-xs patient_medical_records_link" href="#patient_medical_records" id="patient_medical_records_link"><i class="glyphicon glyphicon-align-justify"> Medical Records :</i></a>

                </div>
            </div>
        </div>
    </div>



    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2>Patient </h2>
                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <div class="well">
                    <a href="#book_appointment_form" class=" book_appointment_form_link btn btn-xs btn-info" id="book_appointment_form_link">
                        <i class="glyphicon  icon-white"></i>
                        Book an Appointment : 
                    </a>
                    <hr>
                    <a href="#send_to_doctor" class=" send_to_doctor_link btn btn-xs btn-info" id="send_to_doctor_link">
                        <i class="glyphicon  icon-white"></i>
                        Send To Doctor : 
                    </a>
                    <hr>
                    <form class="send_to_pharmacy_form" id="send_to_pharmacy_form">
                        <input type="hidden" name="send_to_pharm_visit_id" id="send_to_pharm_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="send_to_pharm_visit_id"/>
                        <input type="submit" name="send_to_pharmacy_link" value=" Send To Pharmacy : " id="send_to_pharmacy_link" class=" send_to_pharmacy_link btn btn-xs btn-info"/>
                    </form>

                    <hr>

                </div>
            </div>
        </div>
    </div>









    <div class="box-content patient_medical_records" id="patient_medical_records" style="display: none">
        <div class="well">
            <a href="<?php echo base_url() ?>reports/patient_lab_history/<?php echo $this->uri->segment(3); ?>" id="" class=" btn btn-info "><i class="glyphicon icon-white glyphicon-align-justify"></i> Lab Records</a>
            <hr>
            <a href="<?php echo base_url() ?>reports/patient_consultation_history/<?php echo $this->uri->segment(3); ?>" class="  btn btn-info" id="send_to_doctor_link">
                <i class="glyphicon glyphicon-align-justify icon-white"></i>
                Consultation Records 
            </a>
            <hr>
            <a href="<?php echo base_url() ?>reports/patient_prescription_history/<?php echo $this->uri->segment(3); ?>" class="btn btn-info"><i class="glyphicon icon-white glyphicon-align-justify"> Prescription Records </i></a>

        </div>
    </div>



    <!--/span-->
    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-edit"></i> Patient Triage Report</h2>

                    <div class="box-icon">

                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                class="glyphicon glyphicon-chevron-up"></i></a>

                    </div>
                </div>
                <div class="box-content">

                    <!--/span-->

                    <a  id="export_triage_patient_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Walkin Patient Report" class="export_triage_patient_report_filter_link" href="#export_triage_report_filter_form">
                        <i class="glyphicon glyphicon-download-alt"></i>
                    </a>
                    |
                    <a  id="add_triage_link" data-toggle="tooltip" data-placement="top" data-original-title="Add Triage " class="add_triage_link" href="#add_triage_form">
                        Add Triage <i class="glyphicon glyphicon-plus"></i>
                    </a>


                    <table class="table table-striped table-bordered triage_report_1  responsive">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Date </th>
                                <th>Nurse Name</th>
                                <th>Respiratory (No/Min)</th>
                                <th>Pulse Rate (No/Min)</th>
                                <th>Blood pressure (mmHg)</th>
                                <th>Temperature (°C)</th>
                                <th>Height (Cm)</th>
                                <th>Weight (KGs)</th>
                                <th>BMI</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
<?php
$i = 1;
foreach ($patient_triage_report as $value) {
    ?>
                                    <td class="center"><?php echo $i; ?></td>
                                    <td class="center">
    <?php
    echo $value['visit_date'];
    ?>
                                    </td>
                                    <td class="center">
    <?php
    echo $value['employee_name'];
    ?>
                                    </td>
                                    <td class="center">
    <?php
    echo $value['respiratory_rate'];
    ?>
                                    </td>
                                    <td class="center">
    <?php echo $value['pulse_rate']; ?>
                                    </td>
                                    <td class="center">
    <?php echo $value['diastolic'] . "/" . $value['systolic']; ?>
                                    </td>
                                    <td class="center">
    <?php echo $value['temperature']; ?>
                                    </td>
                                    <td class="center">
    <?php echo $value['height']; ?>
                                    </td>
                                    <td class="center">
    <?php echo $value['weight']; ?>
                                    </td>

                                    <td class="center" ><?php
    $weight = $value['weight'];
    $height = $value['height'];
    $one_hundred = 100;
    $height = $height / $one_hundred;
    $new_height = $height * $height;


    if ($new_height == 0) {
        ?>
                                            <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                            <?php
                                        } else {
                                            $BMI = $weight / $new_height;
                                            if ($BMI <= 20) {
                                                ?>
                                                <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                            <?php } ?>

                                            <?php if ($BMI >= 25) { ?>
                                                <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                            <?php } ?>

                                            <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                <?php
                                            }
                                        }
                                        ?>

                                    </td> 



                                    <td class="center">
                                        <input type="hidden" name="view_triage_patient_id" class="view_triage_patient_id" id="view_triage_patient_id" value="<?php echo $value['patient_id']; ?>"/>
                                        <input type="hidden" name="view_triage_id" class="view_triage_id" id="view_triage_id" value="<?php echo $value['triage_id']; ?>"/>
                                        <input type="hidden" name="view_triage_visit_id" class="view_triage_visit_id" id="view_triage_visit_id" value="<?php echo $value['visit_id']; ?>"/>
                                        <a  id="edit_triage_patient_link" class="edit_triage_patient_link" href="#edit_triage_form">
                                            <i class="glyphicon glyphicon-edit icon-white"></i>

                                        </a>|
                                        <a  id="delete_triage_patient_link" class="delete_triage_patient_link" href="#delete_triage_form">
                                            <i class="glyphicon glyphicon-trash icon-white"></i>

                                        </a>
                                    </td>
                                </tr>

    <?php
    $i++;
}
?>




                        </tbody>
                    </table>



                </div>
            </div>
        </div>
        <!--/span-->



    </div>







    <div class="send_to_doctor" id="send_to_doctor" style="display: none;">



        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class="box-header well">


                        <div class="box-icon">

                        </div>
                    </div>
                    <div class = "box-content">

                        <div class = "bs-example">


                            <form class = "send_to_doctor_form " autocomplete="off"   id = "send_to_doctor_form" role = "form">

                                <input type="hidden" id="send_to_doctor_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="send_to_doctor_visit_id" name="send_to_doctor_visit_id"/>
                                <input type="hidden" id="send_to_doctor_patient_id" value="<?php echo $this->uri->segment(3); ?>" class="send_to_doctor_patient_id" name="send_to_doctor_patient_id"/>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <span class="label label-default">Patient Name : </span>
                                        <input type="text" readonly="" id="send_to_doctor_patient_name" class="send_to_doctor_patient_name uneditable-input form-control "/>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="label label-default">Active Doctors :</label>
                                        <select name="send_to_doctor_name" required="" class="send_to_doctor_name form-control" id="send_to_doctor_name">
                                            <option>Please select Doctor's Name :</option>
                                        </select>
                                    </div>

                                    <hr>
                                    <div class = "form-group">

                                        <input type="submit" class="btn btn-small btn-success btn-xs send_to_doctor_button" id="send_to_doctor_button" value="Send tO Doctor"/>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>







    <div class="delete_triage_form" id="delete_triage_form" style="display: none;">



        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class="box-header well">


                        <div class="box-icon">

                        </div>
                    </div>
                    <div class = "box-content">

                        <div class = "bs-example">


                            <form class = "delete_triage_information_form " autocomplete="off"   id = "delete_tri_information_form" role = "form">


                                <input type="hidden" id="input_triage_id_delete" name="input_triage_id_delete" class="input_triage_id_delete"/>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <h5 class = "" for = "">Are you sure you want to Delete the Triage details? </h5>

                                    </div>

                                    <br>
                                    <div class = "form-group">

                                        <input type="submit" class="btn btn-small btn-success delete_triage_record_yes" value="Yes"/>
                                        <input type="reset" class="btn btn-close delete_triage_record_no" value="No"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>





    <!--/span-->




    <div class="nurse_patient_list_div" id="nurse_patient_list_div" style="display: none;">


        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class = "box-header well">
                        <h2><i class = "glyphicon glyphicon-info-sign"></i> Nurse </h2>

                        <div class = "box-icon">

                            <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                    class = "glyphicon glyphicon-chevron-up"></i></a>

                        </div>
                    </div>
                    <div class = "box-content row">


                        <div class = "box col-md-4">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "">
                                    <h2><i class = "glyphicon glyphicon-list"></i> Regular Patients In Tray</h2>

                                    <div class = "box-icon">

                                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                class = "glyphicon glyphicon-chevron-up"></i></a>

                                    </div>
                                </div>
                                <div class = "box-content">
                                    <ul class = "dashboard-list regular_patients_in_tray" id="regular_patients_in_tray">

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--/span-->





                        <div class = "box col-md-4">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "">
                                    <h2><i class = "glyphicon glyphicon-list"></i> Walk-in Patients In Tray</h2>

                                    <div class = "box-icon">

                                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                class = "glyphicon glyphicon-chevron-up"></i></a>

                                    </div>
                                </div>
                                <div class = "box-content">
                                    <ul class = "dashboard-list walkin_patients_in_tray" id="walkin_patients_in_tray">


                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--/span-->


                        <!--Appointments Start-->
                        <div class = "box col-md-4">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "">
                                    <h2><i class = "glyphicon glyphicon-list"></i> Appointments Today</h2>

                                    <div class = "box-icon">

                                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                class = "glyphicon glyphicon-chevron-up"></i></a>

                                    </div>
                                </div>
                                <div class = "box-content">
                                    <ul class = "dashboard-list appointments_today" id="appointments_today">


                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--Appointments End-->



                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

