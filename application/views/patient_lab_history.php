<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Patient Lab History</a>
        </li>
    </ul>
</div>
<button class="btn btn-default" onclick="goBack()"><i class="glyphicon glyphicon-backward" ></i>Go Back </button>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Patient Laboratory Records </h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>
                        <tr>
                            <th>Test Date</th>
                            <th>Doctor Name</th>
                            <th>Test Name</th>
                            <th>Test Results </th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($patient_lab_history as $value) {
                            ?>
                            <tr>
                                <td class="center"><?php echo $value['date_added']; ?></td>
                                <td class="center"><?php echo $value['employee_name']; ?></td>
                                <td class="center"><?php echo $value['test']; ?></td>
                                <td class="center">
                                    <?php echo $value['test_results']; ?>
                                </td>

                            </tr>
                        <?php }
                        ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->
