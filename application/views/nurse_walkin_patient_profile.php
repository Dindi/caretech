<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?php echo base_url(); ?>">Home</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>">Patient Profile </a>
        </li>
    </ul>
</div>




<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-user"></i> Patient Bio Data </h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-7 col-md-12">

                    <table>
                        <tbody>
                            <?php
                            foreach ($patient_bio as $patient_bio_data) {
                                ?>
                                <tr>`
                                    <td> <h5>Name : <?php echo $patient_bio_data['patient_name']; ?> </h5></td>
                            <input type="hidden" name="hidden_patient_name" class="hidden_patient_name" id="hidden_patient_name" value="<?php echo $patient_bio_data['patient_name']; ?>"/>
                            <td><h6>EMR No : <?php echo $patient_bio_data['family_number']; ?> </h6></td>
                            <td> <h6 class="">Age : <?php
                                    $dob = $patient_bio_data['dob'];
                                    $age = date_diff(date_create($dob), date_create('now'))->y;

                                    if ($age <= 0) {
                                        $bage = date_diff(date_create($dob), date_create('now'))->d;
                                        $nage = $bage . " Days Old";
                                    } elseif ($age > 0) {
                                        $nage = $age . " Years Old";
                                    }
                                    echo $nage;
                                    ?> </h6><td>

                            <td> <h6>Sex : <?php
                                    echo $patient_bio_data['gender'];
                                    ?>
                                </h6> </td>
                            <td> <h6>Residence : <?php
                                    echo $patient_bio_data['residence'];
                                    ?>
                                </h6> </td>
                            <td><span class="label">Allergies : </span><h6 id="patient_allergies_list" class="patient_allergies_list"></h6></td>
                            <td> <a href="#send_to_doctor" class=" send_to_doctor_link btn btn-xs btn-info" id="send_to_doctor_link">
                                    <i class="glyphicon  icon-white"></i>
                                    Send To Doctor : 
                                </a></td>
                            <td> 
                                <form class="send_to_pharmacy_form" id="send_to_pharmacy_form">
                                    <input type="hidden" name="send_to_pharm_visit_id" id="send_to_pharm_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="send_to_pharm_visit_id"/>
                                    <input type="submit" name="send_to_pharmacy_link" value=" Send To Pharmacy : " id="send_to_pharmacy_link" class=" send_to_pharmacy_link btn btn-xs btn-info"/>
                                </form>
                            </td>
                            <td> 
                                <a id="release_patient_link" href="#notification_release_patient" class="release_patient_link btn btn-xs btn-danger">Release Patient</a>

                            </td>
                            </tr>
                            <?php
                        }
                        ?> 

                        </tbody>
                    </table>

                </div>

                <div id="notification_release_patient" class="notification_release_patient" style="display: none;">




                    <div class = "box col-md-12">
                        <div class = "box-inner">
                            <div class = "box-header well" data-original-title = "">
                                <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                <div class = "box-icon">

                                </div>
                            </div>

                            <div class = "box-content">
                                <label class = "">
                                    Are you sure you want to release the  Patient ? 
                                </label><br>
                                <table>
                                    <tr>
                                        <td>
                                            <form class="release_patient_form" id="release_patient_form">
                                                <input type="hidden" name="release_patient_visit_id" id="release_patient_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="relaease_patient_visit_id"/>
                                                <input type="submit" name="release_patient_link" value="Yes " id="yes_release_patient" class=" yes_release_patient btn btn-xs btn-danger"/>
                                            </form>
                                        </td>
                                        <td>

                                            <a id = "relase_patient_no" href = "#relase_patient_no" class = " relase_patient_no btn btn-default btn-xs ">
                                                No
                                            </a>
                                        </td>
                                    </tr>
                                </table>




                            </div>
                        </div>
                    </div>
                    <!--/span-->



                </div>


            </div>
        </div>
    </div>
</div>



<div style="display: block;">


    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well">
                    <h2><i class="glyphicon glyphicon-th"></i> Patient Records</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                class="glyphicon glyphicon-chevron-up"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active"> <a href="#reason_for_visit">Reason for Visit</a></li>
                        <li><a href="#procedure_notes">Procedure Notes</a></li>
                        <li > <a href="#vitals">Vitals</a></li>
                        <li> <a href="#allergies">Allergies</a></li>
                        <li> <a href="#tobacco_social">Tobacco/Social History</a></li>
                        <li><a href="#all_records_tab">All History</a></li>
                        <li><a href="#immunization">Immunization</a></li>
                        <li ><a href="#triage_tab">Triage</a></li>
                        <li><a href="#lab_tab">Lab</a></li>
                        <li><a href="#consultation_tab">Consultation</a></li>
                        <li><a href="#prescription_tab">Prescription</a></li>

                    </ul>

                    <div id="myTabContent" class="tab-content">


                        <!--Reason for visit tab start -->
                        <div class="tab-pane active" id="reason_for_visit">

                            <table class="table table-bordered table-striped">

                                <tbody>
                                    <tr>
                                        <td>



                                            <form id="add_walkin_procedure_form" class="add_walkin_procedure_form" name="add_walkin_procedure_form" method="POST">
                                                <input type="hidden" name="visit_id" value="<?php echo $this->uri->segment(4); ?>" class="procedure_patient_id hidden" id="procedure_patient_id"/>

                                                <input type="hidden" name="patient_id" value="<?php echo $this->uri->segment(3); ?>" class="procedure_patient_id hidden" id="procedure_patient_id"/>
                                                <div class="alert alert-info" style="height: 50px; width: 250px;">
                                                    (hold "Ctrl" key to select multiple):
                                                </div>
                                                <select class="form-control" multiple="" size="10"  id="walkin_procedure" name="walkin_procedure[]">
                                                    <option>Please select at least one Procedure : </option>
                                                    <?php
                                                    foreach ($procedure_list as $value) {
                                                        ?>
                                                        <option value="<?php echo $value['package_id']; ?>"><?php echo $value['package_name'] . ':' . $value['cost']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>

                                                <hr>
                                                <input type="submit" name="bill_walkin_procedure" id="bill_walkin_procedure" value="Book Procedure" class="bill_walkin_procedure btn btn-xs btn-success" />
                                                <input type="reset" name="reset_bill_walkin_procedure" value="Cancel" class="reset_bill_walkin_procedure btn btn-danger btn-xs" />
                                            </form>



                                        </td>
                                        <td>
                                            <form class=" reason_for_visit_form" id="reason_for_visit_form" role="form">
                                                <div class="alert alert-info" style="height: 50px; width: 250px;">
                                                    (Information saved real-time):
                                                </div>
                                                <input type="hidden" name="checking_patient_id" class="checking_patient_id" id="checking_patient_id" value="<?php echo $this->uri->segment(3); ?>"/>
                                                <input type="hidden" name="checking_visit_id" class="checking_visit_id" id="checking_visit_id" value="<?php echo $this->uri->segment(4); ?>"/>


                                                <input type="hidden" name="reason_for_visit_patient_id_1" class="patient_id_1" id="patient_id_1" value="<?php echo $this->uri->segment(3); ?>"/>
                                                <input type="hidden" name="reason_for_visit_visit_id" class="visit_id" id="visit_id" value="<?php echo $this->uri->segment(4); ?>"/>
                                                <hr>


                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <span class="label label-default sr-only"> Reason For Visit :  </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea class=" form-control textarea reason_for_visit_txt_area" placeholder="Reason for Visit : " rows="2" cols="100" name="reason_for_visit_txt_area" id="reason_for_visit_txt_area"></textarea>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-inline">
                                                    <div class = "form-group">
                                                        <label class = "sr-only" for = "medication">Medication : </label>
                                                        <textarea  class = "form-control medication_txt_area" name="medication_txt_area"  rows="2" cols="100" id="medication_txt_area"  placeholder = "Medication"></textarea>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-inline">
                                                    <div class = "form-group">
                                                        <label>
                                                            <input type="checkbox" name="urgency" value="urgent" id="Emergency_0" class="checkbox Emergency_0">
                                                            Urgent</label>
                                                    </div> 
                                                </div>









                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>




                        </div>
                        <!-- Reason for visit end -->


                        <!--Procedure results start -->
                        <div class="tab-pane active" id="procedure_notes">



                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Procedure Name</th>
                                        <th>Procedure Notes</th> 
                                        <th>Action</th>
                                    </tr>

                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Procedure Name </th>
                                        <th>Procedure Notes</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php
                                    foreach ($procedure_results as $value) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $value['procedure_name']; ?>
                                            </td><td>
                                                <?php
                                                $procedure_notes = $value['procedure_result'];
                                                if (!empty($procedure_notes)) {
                                                    echo $procedure_notes;
                                                } else {
                                                    $no_notes = "No Notes ";
                                                    echo $no_notes;
                                                }
                                                ?>


                                            </td>
                                            <td>
                                                <input type="hidden" name="procedure_result_id" class="procedure_result_id hidden" id="procedure_result_id" value="<?php echo $value['id']; ?>"/>
                                                <a  id="edit_procedure_result_link" class="edit_procedure_result_link" href="#add_procedure_option_div">
                                                    <i class="glyphicon glyphicon-edit icon-white"></i>

                                                </a>



                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>




                        </div>
                        <!-- Procedure Results end -->



                        <!--- Shwari procedure notes  form  start -->

                        <div id = "add_procedure_option_div" class="add_procedure_option_div" style = "display: none;">


                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                        <div class = "box-icon">

                                        </div>
                                    </div>

                                    <div class = "box-content">
                                        <form id="add_walkin_procedure_notes_form" class="add_walkin_procedure_notes_form " >
                                            <input type="hidden" name="add_procedure_result_id" class="add_procedure_result_id hidden form-control" id="add_procedure_result_id" />
                                                <div class="alert alert-info" style="height: 50px; width: 250px;">
                                                    (Information saved real-time):
                                                </div>
                                            <div class="form-inline">
                                               
                                                <div class="form-group">

                                                    <textarea id="add_procedure_notes" class="add_procedure_notes textarea form-control" name="add_procedure_notes"></textarea>  
                                                </div>
                                            </div>
                                        </form>


                                    </div>
                                </div>
                            </div>
                            <!--/span-->


                        </div>
                        <!--- Shwari procedure notes  form  end -->








                        <!-- Vital signs start -->
                        <div class="tab-pane" id="vitals">


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Add Patient Triage Form</h2>

                                            <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">


                                                <form class = "add_patient_triage_form " autocomplete="off" method = "post"  id = "add_patient_triage_form" role = "form">

                                                    <input type="hidden" class="form-control add_triage_patient_id" id="add_triage_patient_id" value="<?php echo $this->uri->segment(3); ?>" name="add_triage_patient_id" />
                                                    <input type="hidden" class="form-control add_triage_visit_id" id="add_triage_visit_id" value="<?php echo $this->uri->segment(4); ?>" name="add_triage_visit_id"/>

                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "weight">Weight(Kgs) : </label>
                                                            <input type = "number" min="1" class = "form-control" id = "weight" name="weight" placeholder = "Weight(Kgs) :">
                                                        </div>

                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "systolic_diastolic">Systolic/Diastolic : </label>
                                                            <input type = "number" class = "form-control" id = "systolic" name="systolic" placeholder = "Systolic">/
                                                            <input type = "number" class = "form-control" id = "diastolic" name="diastolic" placeholder = "Diastolic">
                                                        </div>


                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "temperature">Temperature(°C)</label>
                                                            <input type = "number" class = "form-control temperature" name="temperature" id = "temperature" placeholder = "Temperature(°C)">
                                                        </div>

                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "height">Height (cm)</label>
                                                            <input type = "number" class = "form-control" name="height" id = "height" placeholder = "Height (cm) ">
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "respiratory">Respiratory (No/Min)</label>
                                                            <input type = "number" class = "form-control" name="respiratory" id = "respiratory" placeholder = "Respiratory (No/Min)">
                                                        </div>

                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "blood_sugar">Blood Sugar (Mmol/Ltr):</label>
                                                            <input type = "number" class = "form-control" name="blood_sugar" id = "blood_sugar" placeholder = "Blood Sugar (Mmol/Ltr)">
                                                        </div>

                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "pulse_rate">Pulse Rate (Bpm):</label>
                                                            <input type = "number" class = "form-control" name="pulse_rate" id = "pulse_rate" placeholder = "Pulse Rate (Bpm)">
                                                        </div>

                                                        <?php
                                                        foreach ($patient_bio as $patient_bio_data) {
                                                            $gender = $patient_bio_data['gender'];
                                                            if ($gender === "Female") {
                                                                ?>
                                                                <div class = "form-group">
                                                                    <label class = "sr-only" for = "LMP">LMP Date(YYYY-MM-DD):</label>
                                                                    <input type = "text" class = "form-control" name="LMP" id = "LMP" placeholder = "LMP Date(YYYY-MM-DD)">
                                                                </div>
                                                                <?php
                                                            } else {
                                                                ?>

                                                                <?php
                                                            }
                                                            ?>

                                                            <?php
                                                        }
                                                        ?>

                                                    </div>








                                                </form>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div><!--/row-->
                        </div>
                        <!-- Vital signs end -->
                        <!-- Allergies start -->
                        <div class="tab-pane" id="allergies">

                            <form class="add_allergy_form" id="add_allergy_form">
                                <input type="hidden" name="add_allergy_patient_id" id="" value="<?php echo $this->uri->segment(3); ?>"/>
                                <input type="hidden" name="add_allergy_visit_id" id="" value="<?php echo $this->uri->segment(4); ?>"/>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "allergy">Allergy : </label>
                                        <textarea  class = "form-control" name="allergy" id ="allergy"  placeholder = "Allergy"></textarea>
                                    </div>
                                </div>
                                <hr>

                            </form>

                        </div>
                        <!-- Allergies end -->
                        <!-- Tobbacco Social Tab start-->

                        <div class="tab-pane" id="tobacco_social">
                            <form class="add_family_social_form" id="add_family_social_form">
                                <input type="hidden" name="add_family_social_patient_id" id="" value="<?php echo $this->uri->segment(3); ?>"/>
                                <input type="hidden" name="add_family_social_visit_id" id="" value="<?php echo $this->uri->segment(4); ?>"/>

                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "family_social">Family Social History  : </label>
                                        <textarea  class = "form-control" name="family_social" id ="family_social"  placeholder = "Family Social History"></textarea>
                                    </div>
                                </div>
                                <hr>


                            </form>
                        </div>

                        <!-- Tobbacco Social Tab end --> 

                        <!-- Family Social History form tab start -->

                        <div class="tab-pane" id="family_social_history_form_tab">
                            <form class="family_social_history_form" id="family_social_history_form">
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "allergy">Family Social History  : </label>
                                        <textarea  class = "form-control" name="family_social" id ="family_social"  placeholder = "Family Social History"></textarea>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="submit" name="add_family_social_button" class="add_family_social_button btn btn-success btn-xs" id="add_allergy_button" value="Save"/>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <!-- Family Social History  form tab  end -->

                        <!--All records tab start -->

                        <div class="tab-pane" id="all_records_tab">

                            <div class="accordion" id="accordion1">

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseTriage"><i class="glyphicon glyphicon-plus"></i> 
                                            Triage Records : 
                                        </a>
                                    </div>

                                    <div id="collapseTriage" class="accordion-body collapse">

                                        <?php
                                        foreach ($patient_triage_records as $value) {
                                            ?>
                                            <div class="accordion-inner">

                                                <!-- Here we insert another nested accordion -->

                                                <div class="accordion" id="accordion2">
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo 'triage' . $value['triage_id']; ?>">
                                                                <i class="glyphicon glyphicon-plus"></i> <span> <?php
                                                                    $weight = $value['weight'];
                                                                    $height = $value['height'];
                                                                    $one_hundred = 100;
                                                                    $height = $height / $one_hundred;
                                                                    $new_height = $height * $height;


                                                                    if ($new_height == 0) {
                                                                        ?>
                                                                        <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                                                        <?php
                                                                    } else {
                                                                        $BMI = $weight / $new_height;
                                                                        if ($BMI <= 20) {
                                                                            ?>
                                                                            <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                        <?php } ?>

                                                                        <?php if ($BMI >= 25) { ?>
                                                                            <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                        <?php } ?>

                                                                        <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                                            <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>

                                                                </span>
                                                            </a>
                                                        </div>
                                                        <div id="<?php echo 'triage' . $value['triage_id']; ?>" class="accordion-body collapse in">
                                                            <div class="accordion-inner">


                                                                <ul class="list-inline">
                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Visit Date :</label>  <?php
                                                                        echo $value['visit_date'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Posted by : </label>  <?php
                                                                        echo $value['employee_name'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Respiratory Rate :(No/Min)</label>  <?php
                                                                        echo $value['respiratory_rate'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Pulse Rate :(No/Min)</label> <?php
                                                                        echo $value['pulse_rate'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Blood Pressure :(No/Min)</label> <?php
                                                                        echo $value['diastolic'] . " / " . $value['systolic'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Temperature :(°C)</label> <?php
                                                                        echo $value['temperature'];
                                                                        ?></li>
                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Height :(Cm)</label> <?php
                                                                        echo $value['height'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Weight :(Kg)</label> <?php
                                                                        echo $value['weight'];
                                                                        ?></li>

                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>          

                                                <!-- Inner accordion ends here -->

                                            </div>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>

                            <hr>


                            <div class="accordion" id="accordion1">

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseLab"><i class="glyphicon glyphicon-plus"></i> 
                                            Lab Records : 
                                        </a>
                                    </div>

                                    <div id="collapseLab" class="accordion-body collapse">

                                        <?php
                                        foreach ($patient_lab_records as $value) {
                                            ?>
                                            <div class="accordion-inner">

                                                <!-- Here we insert another nested accordion -->

                                                <div class="accordion" id="accordion2">
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['lab_test_id']; ?>">
                                                                <i class="glyphicon glyphicon-plus"></i><span> <?php
                                                                    echo $value['date_added'] . '   Test Name : ' . $value['test_name'];
                                                                    ?></span>
                                                            </a>
                                                        </div>
                                                        <div id="<?php echo $value['lab_test_id']; ?>" class="accordion-body collapse in">
                                                            <div class="accordion-inner">

                                                                <ul class="list-inline">


                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Posted by : </label>  <?php
                                                                        echo $value['employee_name'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Test Name</label>  <?php
                                                                        echo $value['test_name'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Test Results : </label> <?php
                                                                        echo $value['test_results'];
                                                                        ?></li>


                                                                </ul>


                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>          

                                                <!-- Inner accordion ends here -->

                                            </div>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>
                            <hr>

                            <div class="accordion" id="accordion1">

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseConsultation"><i class="glyphicon glyphicon-plus"></i> 
                                            Consultation Records : 
                                        </a>
                                    </div>

                                    <div id="collapseConsultation" class="accordion-body collapse">

                                        <?php
                                        foreach ($patient_consultation_records as $value) {
                                            ?>
                                            <div class="accordion-inner">

                                                <!-- Here we insert another nested accordion -->

                                                <div class="accordion" id="accordion2">
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['consultation_id']; ?>">
                                                                <i class="glyphicon glyphicon-plus"></i><span> <?php echo $value['date']; ?></span>
                                                            </a>
                                                        </div>
                                                        <div id="<?php echo $value['consultation_id']; ?>" class="accordion-body collapse in">
                                                            <div class="accordion-inner">

                                                                <ul class="list-inline">


                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Posted by : </label>  <?php
                                                                        echo $value['employee_name'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Chief Complaint : </label>  <?php
                                                                        echo $value['complaints'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Working Diagnosis :</label> <?php
                                                                        echo $value['working_diagnosis'];
                                                                        ?></li>




                                                                </ul>


                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>          

                                                <!-- Inner accordion ends here -->

                                            </div>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>

                            <hr>


                            <div class="accordion" id="accordion1">

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapsePrescription"><i class="glyphicon glyphicon-plus"></i> 
                                            Prescription Records : 
                                        </a>
                                    </div>

                                    <div id="collapsePrescription" class="accordion-body collapse">

                                        <?php
                                        foreach ($patient_prescription_records as $value) {
                                            ?>
                                            <div class="accordion-inner">

                                                <!-- Here we insert another nested accordion -->

                                                <div class="accordion" id="accordion2">
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['prescription_id']; ?>">
                                                                <i class="glyphicon glyphicon-plus"></i><span> <?php echo $value['date']; ?></span>
                                                            </a>
                                                        </div>
                                                        <div id="<?php echo $value['prescription_id']; ?>" class="accordion-body collapse in">
                                                            <div class="accordion-inner">

                                                                <ul class="list-inline">


                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Posted by : </label>  <?php
                                                                        echo $value['employee_name'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Commodity Name : </label>  <?php
                                                                        echo $value['commodity_name'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Strength :</label> <?php
                                                                        echo $value['strength'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Route : </label> <?php
                                                                        echo $value['route'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Frequency : </label> <?php
                                                                        echo $value['frequency'];
                                                                        ?></li>
                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Duration : </label> <?php
                                                                        echo $value['duration'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">No of Days : </label> <?php
                                                                        echo $value['no_of_days'];
                                                                        ?></li>



                                                                </ul>


                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>          

                                                <!-- Inner accordion ends here -->

                                            </div>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <!--All records tab end -->

                        <div class="tab-pane" id="immunization">
                            <form class="add_immunization_form" id="add_immunization_form">
                                <input type="hidden" name="add_immunization_patient_id" id="" value="<?php echo $this->uri->segment(3); ?>"/>
                                <input type="hidden" name="add_immunization_visit_id" id="" value="<?php echo $this->uri->segment(4); ?>"/>

                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "immunization_txt_area">Immunization   : </label>
                                        <textarea  class = "form-control immunization_txt_area" name="immunization" id ="immunization_txt_area"  placeholder = "Immunization"></textarea>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="submit" name="add_immunization_button" class="add_immunization_button btn btn-success btn-xs" id="add_immunization_button" value="Save"/>
                                    </div>
                                </div>

                            </form>
                        </div>


                        <!---Triage Tab --->

                        <div class="tab-pane" id="triage_tab">

                            <a  id="export_triage_patient_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Walkin Patient Report" class="export_triage_patient_report_filter_link" href="#export_triage_report_filter_form">
                                <i class="glyphicon glyphicon-download-alt"></i>
                            </a>
                            |



                            <table class="table table-striped table-bordered triage_report  responsive">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Date </th>
                                        <th>Nurse Name</th>
                                        <th>Respiratory (No/Min)</th>
                                        <th>Pulse Rate (No/Min)</th>
                                        <th>Blood pressure (mmHg)</th>
                                        <th>Temperature (°C)</th>
                                        <th>Height (Cm)</th>
                                        <th>Weight (KGs)</th>
                                        <th>BMI</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php
                                        $i = 1;
                                        foreach ($patient_triage_records as $value) {
                                            ?>
                                            <td class="center"><?php echo $i; ?></td>
                                            <td class="center">
                                                <?php
                                                echo $value['visit_date'];
                                                ?>
                                            </td>
                                            <td class="center">
                                                <?php
                                                echo $value['employee_name'];
                                                ?>
                                            </td>
                                            <td class="center">
                                                <?php
                                                echo $value['respiratory_rate'];
                                                ?>
                                            </td>
                                            <td class="center">
                                                <?php echo $value['pulse_rate']; ?>
                                            </td>
                                            <td class="center">
                                                <?php echo $value['diastolic'] . "/" . $value['systolic']; ?>
                                            </td>
                                            <td class="center">
                                                <?php echo $value['temperature']; ?>
                                            </td>
                                            <td class="center">
                                                <?php echo $value['height']; ?>
                                            </td>
                                            <td class="center">
                                                <?php echo $value['weight']; ?>
                                            </td>

                                            <td class="center" ><?php
                                                $weight = $value['weight'];
                                                $height = $value['height'];
                                                $one_hundred = 100;
                                                $height = $height / $one_hundred;
                                                $new_height = $height * $height;


                                                if ($new_height == 0) {
                                                    ?>
                                                    <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                                    <?php
                                                } else {
                                                    $BMI = $weight / $new_height;
                                                    if ($BMI <= 20) {
                                                        ?>
                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                    <?php } ?>

                                                    <?php if ($BMI >= 25) { ?>
                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                    <?php } ?>

                                                    <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                        <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                        <?php
                                                    }
                                                }
                                                ?>



                                            </td> 



                                            <td class="center">
                                                <input type="hidden" name="view_triage_patient_id" class="view_triage_patient_id" id="view_triage_patient_id" value="<?php echo $value['patient_id']; ?>"/>
                                                <input type="hidden" name="view_triage_id" class="view_triage_id" id="view_triage_id" value="<?php echo $value['triage_id']; ?>"/>
                                                <input type="hidden" name="view_triage_visit_id" class="view_triage_visit_id" id="view_triage_visit_id" value="<?php echo $value['visit_id']; ?>"/>
                                                <a  id="edit_triage_patient_link" class="edit_triage_patient_link" href="#edit_triage_form">
                                                    <i class="glyphicon glyphicon-edit icon-white"></i>

                                                </a>|
                                                <a  id="delete_triage_patient_link" class="delete_triage_patient_link" href="#delete_triage_form">
                                                    <i class="glyphicon glyphicon-trash icon-white"></i>

                                                </a>
                                            </td>
                                        </tr>

                                        <?php
                                        $i++;
                                    }
                                    ?>




                                </tbody>
                            </table>


                        </div>
                        <div class="tab-pane" id="lab_tab">

                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                <thead>
                                    <tr>
                                        <th>Test Date</th>
                                        <th>Doctor Name</th>
                                        <th>Test Name</th>
                                        <th>Test Results </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($patient_lab_records as $value) {
                                        ?>
                                        <tr>
                                            <td class="center"><?php echo $value['date_added']; ?></td>
                                            <td class="center"><?php echo $value['employee_name']; ?></td>
                                            <td class="center"><?php echo $value['test_name']; ?></td>
                                            <td class="center">
                                                <?php echo $value['test_results']; ?>
                                            </td>

                                        </tr>
                                    <?php }
                                    ?>


                                </tbody>
                            </table>

                        </div>

                        <div class="tab-pane" id="consultation_tab">
                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                <thead>
                                    <tr>
                                        <th>Visit Date</th>
                                        <th> Chief Complaints</th>
                                        <th>Diagnosis</th>
                                        <th>Employee Name</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($patient_consultation_records as $value) {
                                        ?>
                                        <tr>
                                            <td><?php echo $value['date']; ?></td>
                                            <td class="center"><?php echo $value['complaints']; ?></td>
                                            <td class="center"> <?php echo $value['working_diagnosis']; ?></td>
                                            <td class="center"<?php echo $value['employee_name']; ?>></td>

                                        </tr>
                                        <?php
                                    }
                                    ?>


                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="prescription_tab">
                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                <thead>
                                    <tr>
                                        <th>Visit Date</th>
                                        <th>Commodity Name</th>
                                        <th>Strength</th>
                                        <th>Route </th>
                                        <th>Frequency</th>
                                        <th>Duration</th>
                                        <th>Employee Name</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($patient_prescription_records as $value) {
                                        ?>
                                        <tr>
                                            <td><?php echo $value['date']; ?></td>
                                            <td class="center"><?php echo $value['commodity_name']; ?></td>
                                            <td class="center"><?php echo $value['strength']; ?></td>
                                            <td class="center"><?php echo $value['route']; ?> </td>
                                            <td class="center"><?php echo $value['frequency']; ?></td>
                                            <td class="center"> <?php echo $value['duration'] . $value['no_of_days']; ?></td>
                                            <td class="center"> <?php echo $value['employee_name']; ?></td>

                                        </tr>
                                    <?php }
                                    ?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->


    </div><!--/row-->

</div>




