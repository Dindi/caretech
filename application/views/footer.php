<?php if (!isset($no_visible_elements) || !$no_visible_elements) { ?>
    <!-- content ends -->
    </div><!--/#content.col-md-0-->
<?php } ?>
</div><!--/fluid-row-->
<?php if (!isset($no_visible_elements) || !$no_visible_elements) { ?>


    <hr>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>date
        </div>
    </div>

    <footer class="row">
        <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://healthtechkenya.com" target="_blank">Health Tech Kenya
            </a> 2012 - <?php echo date('Y') ?></p>


    </footer>
<?php } ?>

</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="<?php echo base_url(); ?>assets/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='<?php echo base_url(); ?>assets/bower_components/moment/min/moment.min.js'></script>
<script src='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="<?php echo base_url(); ?>assets/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="<?php echo base_url(); ?>assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<?php echo base_url(); ?>assets/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<?php echo base_url(); ?>assets/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<?php echo base_url(); ?>assets/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<?php echo base_url(); ?>assets/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<?php echo base_url(); ?>assets/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<?php echo base_url(); ?>assets/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<?php echo base_url(); ?>assets/js/jquery.history.js"></script>
<!-- validate.js for form and user input validation -->
<!--<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>-->
<!-- application script for Charisma demo -->
<script src="<?php echo base_url(); ?>assets/js/charisma.js"></script>
<!-- application script for Time-picker --->
<script src="<?php echo base_url(); ?>assets/js/jquery.timepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
<script src="<?php echo base_url(); ?>assets/js/transition.js"></script>
<script src="<?php echo base_url(); ?>assets/js/collapse.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js"></script>



<!--
<script src="<?php echo base_url(); ?>assets/jquery-tokeninput/src/jquery.tokeninput.js"></script>
-->


<!--<link href='http://datatables.net/release-datatables/media/css/jquery.dataTables.css' rel='stylesheet'>
<link href='http://datatables.net/release-datatables/extensions/FixedColumns/css/dataTables.fixedColumns.css' rel='stylesheet'>-->
<link href='<?php echo base_url(); ?>assets/datatables-fixedcolumn/jquery.dataTables.css' rel='stylesheet'>
<link href='<?php echo base_url(); ?>assets/datatables-fixedcolumn/dataTables.fixedColumns.css' rel='stylesheet'>
<script type="text/css">
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 800px;
        margin: 0 auto;
    }
</script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.pack.js?v=2.1.4"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>


<script type="text/javascript" href="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" href="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/notify.js"></script>



<script type="text/javascript" src="<?php echo base_url(); ?>assets/chosen/chosen.jquery.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/chosen/chosen.jquery.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/chosen/chosen.proto.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/chosen/chosen.proto.min.js"></script>





<link href='<?php echo base_url(); ?>assets/chosen/chosen.css' rel="stylesheet">
<link href='<?php echo base_url(); ?>assets/chosen/chosen.min.css' rel="stylesheet">

<link href='<?php echo base_url(); ?>assets/chosen/docsupport/prism.css' rel="stylesheet">







<div style="display: none !important;">
    <div class="box-content ajax_loader" id="ajax_loader">
        <ul class="ajax-loaders">

            <img src="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" title="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" >
            </br>
            <br/>

            <span class=" loader_notify clearfix" id="loader_notify">
            </span>  

        </ul>


    </div>

</div>

<script type="text/javascript">








    function add_vital_signs() {

        dataString = $("#add_patient_triage_form").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>index.php/nurse/add_triage",
            data: dataString,
            success: function (data) {

            }

        });
        event.preventDefault();
        return false;


    }


    function order_tests_form() {

        alert('Order made sucessfully');
        dataString = $("#order_tests_form").serialize();
        $.fancybox.open([
            {
                href: '#ajax_loader',
                title: 'Please wait information being updated...'
            }
        ], {
            padding: 0
        });
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>index.php/doctor/order_lab_tests",
            data: dataString,
            success: function (data) {
                $(".loader_notify").notify(
                        "Lab Orders Successfull",
                        "success",
                        {position: "center"}
                );

                setInterval(function () {
                    var url = "<?php echo base_url() ?>home";
                    $(location).attr('href', url);
                }, 300000);

                alert('Order made sucessfully');
            }

        });
        event.preventDefault();
        return false;

    }



    function reason_for_visit() {

        dataString = $("#reason_for_visit_form").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>index.php/nurse/add_reason_for_visit",
            data: dataString,
            success: function (data) {

            }

        });
        event.preventDefault();
        return false;


    }


    function add_lab_test_results() {

        dataString = $("#patient_test_results_form").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>index.php/lab/add_lab_test_results",
            data: dataString,
            success: function (data) {

            }

        });
        event.preventDefault();
        return false;


    }



    function add_allergy() {




        dataString = $("#add_allergy_form").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>nurse/add_allergy",
            data: dataString,
            success: function (data) {

            }

        });
        event.preventDefault();
        return false;


    }



    function add_social_history() {




        dataString = $("#add_family_social_form").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>nurse/add_social_history",
            data: dataString,
            success: function (data) {

            }

        });
        event.preventDefault();
        return false;


    }

    function add_immunization() {

        dataString = $("#add_immunization_form").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>nurse/add_immunization",
            data: dataString,
            success: function (data) {

            }

        });
        event.preventDefault();
        return false;


    }



    function book_appointment() {

        dataString = $("#appointment_form_book").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>index.php/nurse/book_appointment",
            data: dataString,
            success: function (data) {

            }

        });
        event.preventDefault();
        return false;


    }


    function chief_complaints() {

        dataString = $("#chief_complaints_form").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>index.php/doctor/chief_complaints",
            data: dataString,
            success: function (data) {

            }

        });
        event.preventDefault();
        return false;


    }




    function review_of_systems() {

        dataString = $("#review_of_systems_form").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>index.php/doctor/review_of_systems",
            data: dataString,
            success: function (data) {

            }

        });
        event.preventDefault();
        return false;


    }

    function working_diagnosis() {

        dataString = $("#working_diagnosis_form").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>index.php/doctor/working_diagnosis",
            data: dataString,
            success: function (data) {

            }

        });
        event.preventDefault();
        return false;


    }



    $(function () {
        $('#datetimepicker1').datetimepicker();
        $('#datetimepicker2').datetimepicker();
    });




    $(document).ready(function () {

        $("#inputquantity_billed").keyup(function () {

            var inputQuantityavailable = $('#inputQuantityavailable').val();
            var inputsellingprice = $('#inputsellingprice').val();
            var inputquantity_billed = $('#inputquantity_billed').val();
            var inputQuantityavailable = parseInt(inputQuantityavailable);
            var inputsellingprice = parseInt(inputsellingprice);
            var inputquantity_billed = parseInt(inputquantity_billed);
            var total_cost = inputsellingprice * inputquantity_billed;
            $('#input_total_cost').val(total_cost);

            if (inputquantity_billed > inputQuantityavailable) {
                alert("You cannot bill a patient for more than what is available in Stock.");
                $(this).val('');
                $(this).focus();
            }
        });


        $('#bill_commodity_name').on('change', function () {

            var commodity_id = this.value;


            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>pharmacy/get_available_commodity/" + commodity_id,
                dataType: "json",
                success: function (response) {

                    $('#inputstrength').val(response[0].strength);
                    $('#inputtransaction_id').val(response[0].transaction_id);
                    $('#transaction_total_quantiy').val(response[0].transaction_total_quantity);
                    $('#inputQuantityavailable').val(response[0].available_quantity);
                    $('#inputsellingprice').val(response[0].selling_price);
                },
                error: function (data) {

                }
            });



        });




        $('#bill_patient_form').submit(function (event) {
            dataString = $("#bill_patient_form").serialize();
            $.fancybox.open([
                {
                    href: '#ajax_loader',
                    title: 'Please wait information being updated...'
                }
            ], {
                padding: 0
            });
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/pharmacy/bill_patient",
                data: dataString,
                success: function (data) {
                    $(".loader_notify").notify(
                            "Patient Billed Successfully",
                            "success",
                            {position: "center"}
                    );
                    setInterval(function () {
                        var url = window.location.href;
                        $(location).attr('href', url);
                    }, 3000);
                }

            });
            event.preventDefault();
            return false;
        });




        $('#release_patient_form').submit(function (event) {
            dataString = $("#release_patient_form").serialize();
            $.fancybox.open([
                {
                    href: '#ajax_loader',
                    title: 'Please wait information being updated...'
                }
            ], {
                padding: 0
            });
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/doctor/release_patient",
                data: dataString,
                success: function (data) {
                    $(".loader_notify").notify(
                            "Patient Released from the Queue Successfully",
                            "success",
                            {position: "center"}
                    );
                    setInterval(function () {
                        var url = "<?php echo base_url() ?>home";
                        $(location).attr('href', url);
                    }, 3000);
                }

            });
            event.preventDefault();
            return false;
        });







        $('#send_to_current_doctor_form').submit(function (event) {
            dataString = $("#send_to_current_doctor_form").serialize();
            $.fancybox.open([
                {
                    href: '#ajax_loader',
                    title: 'Please wait information being updated...'
                }
            ], {
                padding: 0
            });
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/nurse/send_to_doctor",
                data: dataString,
                success: function (data) {
                    $(".loader_notify").notify(
                            "Patient Sent to Doctor Successfully",
                            "success",
                            {position: "center"}
                    );
                    setInterval(function () {
                        var url = "<?php echo base_url() ?>home";
                        $(location).attr('href', url);
                    }, 3000);
                }

            });
            event.preventDefault();
            return false;
        });





        $('#send_to_doctor_form').submit(function (event) {
            dataString = $("#send_to_doctor_form").serialize();
            $.fancybox.open([
                {
                    href: '#ajax_loader',
                    title: 'Please wait information being updated...'
                }
            ], {
                padding: 0
            });
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/nurse/send_to_doctor",
                data: dataString,
                success: function (data) {
                    $(".loader_notify").notify(
                            "Patient Sent to Doctor Successfully",
                            "success",
                            {position: "center"}
                    );
                    setInterval(function () {
                        var url = "<?php echo base_url() ?>home";
                        $(location).attr('href', url);
                    }, 3000);
                }

            });
            event.preventDefault();
            return false;
        });










        $('#send_to_pharmacy_form').submit(function (event) {
            dataString = $("#send_to_pharmacy_form").serialize();
            $.fancybox.open([
                {
                    href: '#ajax_loader',
                    title: 'Please wait information being updated...'
                }
            ], {
                padding: 0
            });
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/nurse/send_to_pharmacy",
                data: dataString,
                success: function (data) {
                    $(".loader_notify").notify(
                            "Patient Sent to Pharmacy Successfully",
                            "success",
                            {position: "center"}
                    );
                    setInterval(function () {
                        var url = "<?php echo base_url() ?>home";
                        $(location).attr('href', url);
                    }, 300000);
                }

            });
            event.preventDefault();
            return false;
        });




        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/patient_allergies/<?php echo $this->uri->segment(3); ?>",
                                dataType: "JSON",
                                success: function (patient_allergies) {
                                    patient_allergies_list = $('#patient_allergies_list').empty();
                                    if (patient_allergies_list === null) {
                                        patient_allergies_list.append("<option> No Active Doctors in the  System</option>");
                                    } else {
                                        $.each(patient_allergies, function (i, patient_allergiess) {
                                            if (patient_allergiess.allergies === null) {
                                                patient_allergies_list.append("<h6>No Allergies</h6>");
                                            } else {
                                                if (patient_allergiess.allergies === null) {
                                                    patient_allergies_list.append("<h6>No Allergies</h6>");
                                                } else {
                                                    patient_allergies_list.append("<h6 class='label-danger'>'" + patient_allergiess.allergies + "'</h6>");
                                                }
                                            }

                                        });
                                    }



                                },
                                error: function (data) {
                                    //
                                    //  alert('An error occured, kindly try later');
                                }
                            });
                        }, 3000);




                        var reception_url = window.location.href.indexOf("reception") > -1;
                        var nurse_url = window.location.href.indexOf("nurse") > -1;




                        if (reception_url) {

                        } else if (nurse_url) {


                            var nurse_patient_profile = window.location.href.indexOf("nurse_patient_profile") > -1;


                            if (nurse_patient_profile) {



                                var visit_id = $('#checking_visit_id').val();

                                var patient_id = $('#checking_patient_id').val();




                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>nurse/get_allergy/" + visit_id + "/" + patient_id,
                                    dataType: "json",
                                    success: function (response) {

                                        $('#allergy_id').val(response[0].allergy_id);
                                        $('#patient_').val(response[0].patient_id);
                                        $('#allergy').val(response[0].allergies);
                                        $('#medicatinons').val(response[0].medication);
                                        $('#family_social').val(response[0].social_history);
                                        $('#immunization_txt_area').val(response[0].immunization);
                                    },
                                    error: function (data) {

                                    }
                                });




                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>nurse/get_triage/" + visit_id + "/" + patient_id,
                                    dataType: "json",
                                    success: function (response) {

                                        $('#allergy_id').val(response[0].triage_id);
                                        $('#patient_id').val(response[0].patient_id);
                                        $('#medication_txt_area').val(response[0].medication);
                                        $('#weight').val(response[0].weight);
                                        $('#diastolic').val(response[0].diastolic);
                                        $('#systolic').val(response[0].systolic);
                                        $('#temperature').val(response[0].temperature);
                                        $('#height').val(response[0].height);
                                        $('#respiratory').val(response[0].respiratory_rate);
                                        $('#pulse_rate').val(response[0].pulse_rate);
                                        $('#LMP').val(response[0].lmp);
                                        $('#blood_sugar').val(response[0].blood_sugar);

                                        $('#reason_for_visit_txt_area').val(response[0].visit_reason);
                                        $('#Emergency_0').val(response[0].urgency);
                                        $('#viist_id').val(response[0].OCS);

                                    },
                                    error: function (data) {

                                    }
                                });



                            }



                            $(".add_triage_link").click(function () {
                                var patient_name = $(".hidden_patient_name").val();
                                $(".add_triage_patient_name").val(patient_name);
                            });

                            $(".edit_triage_patient_link").click(function () {
                                var patient_name = $(".hidden_patient_name").val();
                                $(".edit_triage_patient_name").val(patient_name);
                            });

                            $(".delete_triage_patient_link").click(function () {
                                var triage_id = $(this).closest('tr').find('input[name="view_triage_id"]').val();
                                $(".input_triage_id_delete").val(triage_id);
                            });



                            $(".send_to_doctor_link").click(function () {


                                var patient_name = $('input[name="hidden_patient_name"]').val();
                                $(".send_to_doctor_patient_name").val(patient_name);
                            });

                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>nurse/active_doctors_list",
                                    dataType: "JSON",
                                    success: function (active_doctors_list) {
                                        send_to_doctor_name = $('#send_to_doctor_name').empty();
                                        if (send_to_doctor_name === null) {
                                            send_to_doctor_name.append("<option> No Active Doctors in the  System</option>");
                                        } else {
                                            $.each(active_doctors_list, function (i, active_doctors_lists) {
                                                if (active_doctors_lists.employee_id === null) {
                                                    send_to_doctor_name.append("<option> No Active Doctors in the  System</option>");
                                                } else {
                                                    if (active_doctors_lists.employee_id === null) {
                                                        send_to_doctor_name.append("<option> No Active Doctors in the  System</option>");
                                                    } else {
                                                        send_to_doctor_name.append('<option value="' + active_doctors_lists.employee_id + '">' + active_doctors_lists.employee_name + '</option>');
                                                    }
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);






                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>nurse/nurse_patient_list",
                                    dataType: "JSON",
                                    success: function (nurse_patient_list) {
                                        patient_intray = $('#regular_patients_in_tray').empty();
                                        if (patient_intray === null) {
                                            patient_intray.append("<p>No Patients In Tray</p>");
                                        } else {
                                            $.each(nurse_patient_list, function (i, nurse_patient_lists) {
                                                if (patient_intray.total_patient_visits === null) {
                                                    patient_intray.append("<p>No Patients In Tray</p>");
                                                } else {
                                                    if (nurse_patient_lists.patient_id === null) {
                                                        patient_intray.append("<p>No Patients In Tray</p>");
                                                    } else {
                                                        if (nurse_patient_lists.urgency !== "urgent") {
                                                            patient_intray.append('<li><a href="<?php echo base_url() ?>nurse/nurse_patient_profile/' + nurse_patient_lists.patient_id + '/' + nurse_patient_lists.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + nurse_patient_lists.title + " : " + nurse_patient_lists.f_name + " " + nurse_patient_lists.s_name + " " + nurse_patient_lists.other_name + '</a></li></br>');

                                                        } else {
                                                            patient_intray.append('<li><a href="<?php echo base_url() ?>nurse/nurse_patient_profile/' + nurse_patient_lists.patient_id + '/' + nurse_patient_lists.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + nurse_patient_lists.title + " : " + nurse_patient_lists.f_name + " " + nurse_patient_lists.s_name + " " + nurse_patient_lists.other_name + '</a><span style="color:red !important;"> Urgent</span></li></br>');

                                                        }
                                                    }
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);


                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>nurse/nurse_walkin_patient_list",
                                    dataType: "JSON",
                                    success: function (nurse_patient_list) {
                                        patient_intray = $('#walkin_patients_in_tray').empty();
                                        if (patient_intray === null) {
                                            patient_intray.append("<p>No Patients In Tray</p>");
                                        } else {
                                            $.each(nurse_patient_list, function (i, nurse_patient_lists) {
                                                if (patient_intray.total_patient_visits === null) {
                                                    patient_intray.append("<p>No Patients In Tray</p>");
                                                } else {
                                                    if (nurse_patient_lists.patient_id === null) {
                                                        patient_intray.append("<p>No Patients In Tray</p>");
                                                    } else {
                                                        patient_intray.append('<li><a href="<?php echo base_url() ?>nurse/profile/' + nurse_patient_lists.walkin_id + '/"><i class = "glyphicon glyphicon-user"></i>' + nurse_patient_lists.walkin_patient_name + '</a></li></br>');
                                                    }
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);




                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>nurse/total_regular_patients_in_tray",
                                    dataType: "JSON",
                                    success: function (total_regular_patients) {
                                        total_visit = $('#total_regular_patients_in_tray_today').empty();
                                        if (total_visit === null) {
                                            total_visit.append("<p>No Patients In Tray</p>");
                                        } else {
                                            $.each(total_regular_patients, function (i, total_regular_patients) {
                                                if (total_regular_patients.total_patient_visits === null) {
                                                    total_visit.append("<p>No Patients In Tray</p>");
                                                } else {
                                                    total_visit.append('<p>' + total_regular_patients.total_regular_patients_in_tray + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);



                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>nurse/total_walkin_patients_in_tray",
                                    dataType: "JSON",
                                    success: function (total_walkin_patients) {
                                        total_walkin_patientsa = $('#total_walkin_patients_in_tray_today').empty();
                                        if (total_walkin_patients === null) {
                                            total_walkin_patientsa.append("<p>No Patients In-Tray</p>");
                                        } else {
                                            $.each(total_walkin_patients, function (i, total_walkin_patientss) {
                                                if (total_walkin_patientss.total_walkin_patients_in_tray === 0) {
                                                    total_walkin_patientsa.append("<p>No Patients In-Tray</p>");
                                                } else {
                                                    total_walkin_patientsa.append('<p>' + total_walkin_patientss.total_walkin_patients_in_tray + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);


                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>nurse/total_appointments_to_date",
                                    dataType: "JSON",
                                    success: function (total_appointments) {
                                        total_appointmets = $('#total_appointments_today').empty();
                                        if (total_appointments === null) {
                                            total_appointmets.append("<p>No Appointmets Today</p>");
                                        } else {
                                            $.each(total_appointments, function (i, total_appointmentss) {
                                                if (total_appointmentss.amount_owed === null) {
                                                    total_appointmets.append("<p>No Appointments Today</p>");
                                                } else {
                                                    total_appointmets.append('<p>' + total_appointmentss.amount_owed + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);

                            $('#send_to_doctor_form').submit(function (event) {
                                dataString = $("#send_to_doctor_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/nurse/send_to_doctor",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Sent to Doctor Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = "<?php echo base_url() ?>home";
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });





                            $('#appointment_form_book').submit(function (event) {
                                dataString = $("#appointment_form_book").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>nurse/book_appointment",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Appointment booked Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = "<?php echo base_url() ?>home";
                                            $(location).attr('href', url);
                                        }, 300000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });




                            $('#add_patient_triage_form').submit(function (event) {
                                dataString = $("#add_patient_triage_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>nurse/add_triage",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Triage Updated Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = "<?php echo base_url() ?>home";
                                            $(location).attr('href', url);
                                        }, 300000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });




                            $('#weight').keyup(function () {

                                add_vital_signs();
                            });

                            $('#systolic').keyup(function () {
                                add_vital_signs();
                            });

                            $('#diastolic').keyup(function () {
                                add_vital_signs();
                            });

                            $('#temperature').keyup(function () {
                                add_vital_signs();
                            });

                            $('#height').keyup(function () {
                                add_vital_signs();
                            });

                            $('#respiratory').keyup(function () {
                                add_vital_signs();
                            });

                            $('#blood_sugar').keyup(function () {
                                add_vital_signs();
                            });

                            $('#pulse_rate').keyup(function () {
                                add_vital_signs();
                            });

                            $('#LMP').keyup(function () {
                                add_vital_signs();
                            });




                            $('#allergy').keyup(function () {
                                add_allergy();
                            });

                            $('#add_family_social_form').keyup(function () {
                                add_social_history();
                            });

                            $('#immunization_txt_area').keyup(function () {
                                add_immunization();
                            });



                            $('#appointment_type_1').keyup(function () {
                                book_appointment();
                            });
                            $('#person_to_see').keyup(function () {
                                book_appointment();
                            });
                            $('#date_from').keyup(function () {
                                book_appointment();
                            });
                            $('#date_to').keyup(function () {
                                book_appointment();
                            });
                            $('#reason_for_appointment').keyup(function () {
                                book_appointment();
                            });


                            $("#reason_for_visit_txt_area").keyup(function () {

                                reason_for_visit();
                            });


                            $("#medication_txt_area").keyup(function () {
                                reason_for_visit();
                            });


                            $("#Emergency_0").change(function () {
                                reason_for_visit();
                            });



                            $('#edit_patient_triage_form').submit(function (event) {
                                dataString = $("#edit_patient_triage_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/nurse/update_triage",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Triage Updated Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.pathname;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });


                            $('#delete_triage_information_form').submit(function (event) {
                                dataString = $("#delete_triage_information_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/nurse/delete_triage",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Triage Deleted Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.pathname;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });



                        }
                    });
                    $(document).ready(function () {
                        var reception_url = window.location.href.indexOf("reception") > -1;
                        var cashier_url = window.location.href.indexOf("cashier") > -1;
                        var reports_url = window.location.href.indexOf("reports") > -1;
                        var nurse_url = window.location.href.indexOf("nurse") > -1;
                        var doctor_url = window.location.href.indexOf("doctor") > -1;
                        var lab_url = window.location.href.indexOf("lab") > -1;
                        var pharmacy_url = window.location.href.indexOf("pharmacy") > -1;
                        if (reception_url) {

                            $(function () {
                                var $sfield = $('#procedure_name_1').autocomplete({
                                    source: function (request, response) {
                                        var url = "<?php echo site_url('reception/procedures'); ?>";
                                        $.post(url, {data: request.term}, function (data) {
                                            response($.map(data, function (procedures) {
                                                return {
                                                    value: procedures.procedure_name
                                                };
                                            }));
                                        }, "json");
                                    },
                                    minLength: 2,
                                    autofocus: true
                                });
                            });
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reception/get_active_doctors/",
                                    dataType: "JSON",
                                    success: function (active_doctors) {
                                        active_doctor = $('#active_doctor_list_1').empty();
                                        if (active_doctor === null) {
                                            active_doctor.append("<option>No  Active Doctor</opton>");
                                        }
                                        $.each(active_doctors, function (i, active_doctorss) {
                                            active_doctor.append('<option value="' + active_doctorss.employee_id + '">' + active_doctorss.employee_name + '</option>');
                                        });
                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 2000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reception/total_visits",
                                    dataType: "JSON",
                                    success: function (total_visits) {
                                        total_visit = $('#total_regular_visits').empty();
                                        if (total_visit === null) {
                                            total_visit.append("<option>No Visits</opton>");
                                        } else {
                                            $.each(total_visits, function (i, total_visitss) {
                                                if (total_visitss.total_patient_visits === null) {
                                                    total_visit.append("<p>No Visits</p>");
                                                } else {
                                                    total_visit.append('<p>' + total_visitss.total_patient_visits + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);
                            //reception
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reception/total_walkin_patients",
                                    dataType: "JSON",
                                    success: function (walkin_visits) {
                                        walkin_visit = $('#total_walkin_patient_visits').empty();
                                        if (walkin_visit === null) {
                                            walkin_visit.append("<p>No Walkin Visits</p>");
                                        } else {
                                            $.each(walkin_visits, function (i, walkin_visitss) {

                                                if (walkin_visitss.total_walkin_patient === null) {
                                                    walkin_visit.append("<p>No Walkin Visits</p>");
                                                } else {
                                                    walkin_visit.append('<p>' + walkin_visitss.total_walkin_patient + '</p>');
                                                }

                                            });
                                        }


                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reception/seeappointments",
                                    dataType: "JSON",
                                    success: function (appointed) {
                                        appointment_list = $('#patient_appointments').empty();
                                        if (appointed === null) {


                                        }
                                        else {

                                            $.each(appointed, function (i, appointment) {

                                                if (appointment.urgency === "urgent") {
                                                    appointment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + appointment.f_name + " " + appointment.s_name + " " + appointment.other_name + '</a></li><span style="color:red !important;">' + appointment.urgency + '</span>');
                                                }
                                                else if (appointment_list.urgency !== "urgent")
                                                {
                                                    appointment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + appointment.f_name + " " + appointment.s_name + " " + appointment.other_name + '</li>');
                                                }
                                                else {
                                                    appointment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                }


                                            });
                                        }

                                    },
                                    error: function (data) {

                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reception/to_paywalkin",
                                    dataType: "JSON",
                                    success: function (to_paywalkin) {
                                        to_paywalkin_list = $('#walk_in_patient_payments').empty();
                                        if (to_paywalkin === null) {

                                        } else {
                                            $.each(to_paywalkin, function (i, to_paywalkin) {

                                                if (to_paywalkin.urgency === "urgent") {
                                                    to_paywalkin_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + to_paywalkin.fname + " " + to_paywalkin.lname + " " + to_paywalkin.sname + '</a><span style="color:red !important;">' + to_paywalkin.urgency + '</span></li>');
                                                }
                                                else if (to_paywalkin.urgency !== "urgent")
                                                {
                                                    to_paywalkin_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + to_paywalkin.walkin_patient_name + " " + to_paywalkin.faculty + " " + to_paywalkin.walkin_payments_total + '</a></li>');
                                                }
                                                else {
                                                    to_paywalkin_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                }


                                            });
                                        }


                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reception/pat_payment",
                                    dataType: "JSON",
                                    success: function (payment_list) {
                                        pat_payment_list = $('#regular_patients_in_traypayments').empty();
                                        if (payment_list === null) {

                                        } else {
                                            $.each(payment_list, function (i, payment_list) {

                                                if (payment_list.urgency === "urgent") {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.urgency + '</span> Kshs :<span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');
                                                }
                                                else if (payment_list.urgency !== "urgent")
                                                {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a> Kshs :<span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');
                                                }
                                                else {

                                                    pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                }

                                            });
                                        }


                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reception/active_doctors",
                                    dataType: "JSON",
                                    success: function (doctor_list) {
                                        active_doctor_list = $('#active_doctor_list').empty();
                                        if (doctor_list === null) {
                                            active_doctor_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Doctor</a> </li>");
                                        } else {
                                            $.each(doctor_list, function (i, doctor_list) {


                                                active_doctor_list.append('<li><a class="doctor" href="#active_patients_in_doctor"><i class = "glyphicon glyphicon-user"></i>' + doctor_list.user_name + '</a></li>');
                                                var doctor_id = doctor_list.employee_id;
                                                $.ajax({
                                                    type: "GET",
                                                    url: "<?php echo base_url(); ?>reception/d_active/" + doctor_id,
                                                    dataType: "JSON",
                                                    success: function (people) {
                                                        waiting_list = $('#waiting_list').empty();
                                                        if (people === null) {


                                                            waiting_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                        } else {
                                                            $.each(people, function (i, waiting) {

                                                                if (waiting.urgency === "urgent") {
                                                                    waiting_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + waiting.f_name + " " + waiting.other_name + " " + waiting.s_name + '<span style="color:red !important;">' + waiting.urgency + '</span></li>');
                                                                }
                                                                else if (waiting_list.urgency !== "urgent") {
                                                                    waiting_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + waiting.f_name + " " + waiting.other_name + " " + waiting.s_name + '</a></li>');
                                                                }


                                                            });
                                                        }

                                                    },
                                                    error: function (data) {
                                                        //error do something
                                                    }
                                                });
                                            });
                                        }


                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reception/active_patient_doctor",
                                    dataType: "JSON",
                                    success: function (patient_doctor_list) {
                                        active_patient_doctor_list = $('#active_patient_visit_doctor').empty();
                                        $.each(patient_doctor_list, function (i, patient_doctor_list) {

                                            if (patient_doctor_list.urgency === "urgent") {
                                                active_patient_doctor_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + patient_doctor_list.f_name + " " + patient_doctor_list.other_name + " " + patient_doctor_list.s_name + '</a><span style="color:red !important;">' + patient_doctor_list.urgency + '</span></li>');
                                            }
                                            else if (patient_doctor_list.urgency !== "urgent")
                                            {
                                                active_patient_doctor_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + patient_doctor_list.f_name + " " + patient_doctor_list.other_name + " " + patient_doctor_list.s_name + '</a></li>');
                                            }
                                            else {
                                                active_patient_doctor_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                            }

                                        });
                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reception/active_nurses",
                                    dataType: "JSON",
                                    success: function (nurse_list) {
                                        active_nurse_list = $('#active_nurses').empty();
                                        if (nurse_list === null) {
                                            active_nurse_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Nurse</a> </li>");
                                        } else {
                                            $.each(nurse_list, function (i, nurse_list) {


                                                active_nurse_list.append('<li><i class = "glyphicon glyphicon-user"></i><a class="nurse" href="#active_patients_in_nurse">' + nurse_list.user_name + '</a></li>');
                                                var nurse_id = nurse_list.employee_id;
                                                $.ajax({
                                                    type: "GET",
                                                    url: "<?php echo base_url(); ?>reception/load_data/" + nurse_id,
                                                    dataType: "JSON",
                                                    success: function (nurse) {
                                                        nurse_list = $('#nurse_list').empty();
                                                        if (nurse === null) {

                                                            nurse_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                        }
                                                        else {
                                                            $.each(nurse, function (i, nurse) {

                                                                if (nurse.urgency === "urgent") {
                                                                    nurse_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + nurse.f_name + " " + nurse.other_name + " " + nurse.s_name + '<span style="color:red !important;">' + nurse.urgency + '</span></li>');
                                                                }
                                                                else if (nurse.urgency !== "urgent")
                                                                {
                                                                    nurse_list.append('<li><i class = "glyphicon glyphicon-user"></i>' + nurse.f_name + " " + nurse.other_name + " " + nurse.s_name + '</li>');
                                                                }
                                                                else {
                                                                    nurse_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                                }
                                                            });
                                                        }

                                                    },
                                                    error: function (data) {
                                                        //error do something                          
                                                    }
                                                });
                                            });
                                        }
                                    },
                                    error: function (data) {
                                        //error do something            
                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reception/active_laboratories",
                                    dataType: "JSON",
                                    success: function (laboratory_list) {
                                        active_laboratory_list = $('#active_laboratories').empty();
                                        if (laboratory_list === null) {
                                            active_laboratory_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Lab Tech</a> </li>");
                                        } else {
                                            $.each(laboratory_list, function (i, laboratory_list) {

                                                active_laboratory_list.append('<li><a class="laboratorist" href=""><i class="glyphicon glyphicon-user"></i>' + laboratory_list.user_name + '</a></li>');
                                                var laboratorist_id = laboratory_list.employee_id;
                                                $.ajax({
                                                    type: "GET",
                                                    url: "<?php echo base_url(); ?>reception/l_active/" + laboratorist_id,
                                                    dataType: "JSON",
                                                    success: function (laboratory) {
                                                        laboratory_list = $('#lab_list').empty();
                                                        if (laboratory === null) {
                                                            laboratory_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                        } else {
                                                            $.each(laboratory, function (i, laboratory) {
                                                                if (laboratory.urgency === "urgent") {
                                                                    laboratory_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + laboratory.f_name + " " + laboratory.other_name + " " + laboratory.s_name + '</ul></a><span style="color:red !important;">' + pharm.urgency + '</span></br></ul>');
                                                                }
                                                                else if (laboratory.urgency !== "urgent")
                                                                {
                                                                    laboratory_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + laboratory.f_name + " " + laboratory.other_name + " " + laboratory.s_name + '</br></ul>');
                                                                }
                                                                else {
                                                                    laboratory_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                                }
                                                            });
                                                        }

                                                    },
                                                    error: function (data) {
                                                        //error do something
                                                    }
                                                });
                                            });
                                        }

                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reception/active_pharmacist",
                                    dataType: "JSON",
                                    success: function (pharm_list) {
                                        active_pharm_list = $('#active_pharm_list').empty();
                                        if (pharm_list === null) {
                                            active_pharm_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Pharm Tech</a> </li>");
                                        } else {
                                            $.each(pharm_list, function (i, pharm_list) {

                                                active_pharm_list.append('<a class="pharmacist"  href=""><i class="glyphicon glyphicon-user"></i>' + pharm_list.user_name + '</a></br>');
                                                var pharmacist_id = pharm_list.employee_id;
                                                $.ajax({
                                                    type: "GET",
                                                    url: "<?php echo base_url(); ?>reception/p_active/" + pharmacist_id,
                                                    dataType: "JSON",
                                                    success: function (pharm) {
                                                        pharm_list = $('#pharm_list').empty();
                                                        if (pharm === null) {
                                                            pharm_list.append("<ul>No Active Patient</ul>");
                                                        } else {
                                                            $.each(pharm, function (i, pharm) {
                                                                if (pharm.urgency === "urgent") {
                                                                    pharm_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + pharm.f_name + " " + pharm.other_name + " " + pharm.s_name + '</ul></a></br></ul>');
                                                                }
                                                                else if (pharm.urgency !== "urgent")
                                                                {
                                                                    pharm_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + pharm.f_name + " " + pharm.other_name + " " + pharm.s_name + '</br></ul>');
                                                                }
                                                                else {
                                                                    pharm_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                                }
                                                            });
                                                        }

                                                    },
                                                    error: function (data) {
                                                        //error do something
                                                    }
                                                });
                                            });
                                        }

                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                            //reception
                            $('#new_family_registration_form').submit(function (event) {
                                dataString = $("#new_family_registration_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/reception/new_family_tree",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Details saved Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = "<?php echo base_url(); ?>index.php/reception/reception/newpatvisit";
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });


                            $('.employmentstatus_yes').click(function () {

                                // $("#txtAge").toggle(this.checked);
                                $('.employers_name').css('display', 'inline');
                            });
                            $('.employmentstatus_no').click(function () {

                                // $("#txtAge").toggle(this.checked);
                                $('.employers_name').val('');
                                $('.employers_name').css('display', 'none');
                            });
                            $('.employmentstatus_yes_1').click(function () {
                                // $("#txtAge").toggle(this.checked);
                                $('.employers_name_1').css('display', 'inline');
                            });
                            $('.employmentstatus_no_1').click(function () {
                                // $("#txtAge").toggle(this.checked);
                                $('.employers_name_1').val('');
                                $('.employers_name_1').css('display', 'none');
                            });
                            $('#register_new_patient_form').submit(function (event) {
                                dataString = $("#register_new_patient_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/reception/add",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Details saved Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = "<?php echo base_url(); ?>index.php/reception/reception/newpatvisit";
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            //reception
                            $('#add_visit_form').submit(function (event) {
                                dataString = $("#add_visit_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/reception/new_visit",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "New Visit Added Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            $('#inputShwariFamilyNumber').keyup(function () {
                                //get

                                member_search = $('#inputShwariFamilyNumber').val();
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reception/check_shari_member_existence/" + member_search,
                                    dataType: "json",
                                    success: function (response) {
                                        shwari_family_number_result = $('#shwari_family_number_result').empty();
                                        $.each(response, function (i, response) {

                                            var str = response.family_base_number;
                                            var res = str.substring(0, 14);
                                            shwari_family_number_result.append('<span>' + res + '</span></br>');
                                        });
                                    },
                                    error: function (data) {

                                    }
                                });
                            });
                            $('#employmentstatus_yes').click(function () {
                                // $("#txtAge").toggle(this.checked);
                                $('#employer_1').css('display', 'inline');
                            });
                            $('#employmentstatus_no').click(function () {
                                // $("#txtAge").toggle(this.checked);
                                $('#employer_1').css('display', 'none');
                            });
                            $('#add_walkin_patient_form').submit(function (event) {
                                dataString = $("#add_walkin_patient_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/reception/add_walkin",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "New Walkin Patient Added Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            $('#book_patient_procedure_form').submit(function (event) {
                                dataString = $("#book_patient_procedure_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/reception/add_patient_procedure",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Booked for Procedure",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            $('#shwari_patient_book_procedure_form').submit(function (event) {
                                dataString = $("#shwari_patient_book_procedure_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/reception/add_procedure",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Booked for Procedure",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                        } else if (reports_url) {

                            //reports
                            $('#edit_patient_information_form').submit(function (event) {
                                dataString = $("#edit_patient_information_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/reports/edit_patient_info",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Details saved Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            $('#edit_visitation_info').submit(function (event) {
                                dataString = $("#edit_visitation_info").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/reports/edit_visitation_info",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Visitation Details updated Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            $('#delete_visit_information_form').submit(function (event) {
                                dataString = $("#delete_visit_information_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/reports/delete_visitation_info",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Vistation Details updated Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            //reports
                            $('.delete_patient_information_form').submit(function (event) {
                                dataString = $(".delete_patient_information_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>reports/delete_patient",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Record Deleted Succesfully ",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            $('.edit_patient_information_form').submit(function (event) {
                                dataString = $(".edit_patient_information_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>reports/edit_patient",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Record Edited Succesfully ",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 30000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            $('.edit_walkin_patient_info').submit(function (event) {
                                dataString = $(".edit_walkin_patient_info").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>reports/edit_walkin_patient",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Walkin Patient Details Updated Succesfully ",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 30000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            $('.delete_walkin_patient_information_form').submit(function (event) {
                                dataString = $(".delete_walkin_patient_information_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>reports/delete_walkin_patient",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Walkin Patient Details Updated Succesfully ",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 30000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            $('.edit_procedure_visit_info').submit(function (event) {
                                dataString = $(".edit_procedure_visit_info").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>reports/edit_procedure_visit_data",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                " Patient Procedure Details Updated Succesfully ",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 30000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            $('.delete_procedure_information_form').submit(function (event) {
                                dataString = $(".delete_procedure_information_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>reports/delete_procedure_visit_data",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                " Patient Procedure Details Updated Succesfully ",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.href;
                                            $(location).attr('href', url);
                                        }, 30000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                        } else if (cashier_url) {

                            //cashier
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/total_regular_payments",
                                    dataType: "JSON",
                                    success: function (total_visits) {
                                        total_visit = $('#total_regular_payments').empty();
                                        if (total_visit === null) {
                                            total_visit.append("<p>No Payments</p>");
                                        } else {
                                            $.each(total_visits, function (i, total_visitss) {
                                                if (total_visitss.amount_owed === null) {
                                                    total_visit.append("<p>No Payments</p>");
                                                } else {
                                                    total_visit.append('<p>' + total_visitss.amount_owed + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/total_package_payments",
                                    dataType: "JSON",
                                    success: function (total_visits) {
                                        total_visit = $('#total_package_payments').empty();
                                        if (total_visit === null) {
                                            total_visit.append("<p>No Payments</p>");
                                        } else {
                                            $.each(total_visits, function (i, total_visitss) {
                                                if (total_visitss.amount_owed === null) {
                                                    total_visit.append("<p>No Payments</p>");
                                                } else {
                                                    total_visit.append('<p>' + total_visitss.amount_owed + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);
                            setInterval(function () {

                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/total_lab_service_payments",
                                    dataType: "JSON",
                                    success: function (total_visits) {

                                        total_visit = $('#total_lab_service_payments').empty();
                                        if (total_visits === null) {

                                            total_visit.append("<p>No Payments</p>");
                                        } else {
                                            $.each(total_visits, function (i, total_visitss) {
                                                if (total_visitss.amount_owed === null) {

                                                    total_visit.append("<p>No Payments</p>");
                                                } else {

                                                    total_visit.append('<p>' + total_visitss.amount_owed + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/total_procedure_service_payments",
                                    dataType: "JSON",
                                    success: function (total_visits) {
                                        total_visit = $('#total_procedure_service_payments').empty();
                                        if (total_visit === null) {
                                            total_visit.append("<p>No Payments</p>");
                                        } else {
                                            $.each(total_visits, function (i, total_visitss) {
                                                if (total_visitss.amount_owed === null) {
                                                    total_visit.append("<p>No Payments</p>");
                                                } else {
                                                    total_visit.append('<p>' + total_visitss.amount_owed + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/total_pharmacy_payments",
                                    dataType: "JSON",
                                    success: function (total_visits) {
                                        total_visit = $('#total_pharmacy_payments').empty();
                                        if (total_visit === null) {
                                            total_visit.append("<p>No Payments</p>");
                                        } else {
                                            $.each(total_visits, function (i, total_visitss) {
                                                if (total_visitss.amount_owed === null) {
                                                    total_visit.append("<p>No Payments</p>");
                                                } else {
                                                    total_visit.append('<p>' + total_visitss.amount_owed + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);
                            //cashier
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/total_walkin_payments",
                                    dataType: "JSON",
                                    success: function (total_visits) {
                                        total_visit = $('#total_walkin_patient_payments').empty();
                                        if (total_visits === null) {
                                            total_visit.append("<option>No Payments</option>");
                                        } else {
                                            $.each(total_visits, function (i, total_visitss) {
                                                if (total_visitss.amount_owed === null) {
                                                    total_visit.append("<p>No Payments</p>");
                                                } else {
                                                    total_visit.append('<p>' + total_visitss.amount_owed + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);
                            //cashier




                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/regular_patient_payments",
                                    dataType: "JSON",
                                    success: function (payment_list) {
                                        pat_payment_list = $('#regular_patient_payment').empty();
                                        $.each(payment_list, function (i, payment_list) {
                                            pat_payment_list.append('<li><a href="#regular_patient_form" id="regular_payment_link" class="regular_payment_link"><i class = "glyphicon glyphicon-user"> </i>\n\
                ' + payment_list.patient_name + '</a>\n\
      <input type="hidden" id="patient_id" name="patient_id_list" class="patient_id" value="' + payment_list.patient_id + '"/>\n\
<input type="hidden" id="visit_id" class="visit_id" name="visit_id_list" value="' + payment_list.visit_id + '" /></br></li>');
                                        });
                                    },
                                    error: function (data) {
                                        //error do something
                                        // alert(data);
                                    }
                                });
                            }, 3000);
                            $('.regular_patient_payment').on('click', '.regular_payment_link', function () {
                                // alert('Alert!!');
                                //get
                                var visit_id = $(this).closest('li').find('input[name="visit_id_list"]').val();
                                // alert(visit_id);
                                var patient_id = $(this).closest('li').find('input[name="patient_id_list"]').val();
                                // alert(patient_id);
                                html1 = '';
                                htmlhead1 = '';
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/regular_patient_payments_details/" + visit_id + "/" + patient_id,
                                    dataType: "JSON",
                                    success: function (data) {
                                        for (i = 0; i < data.length; i++) {

                                            // alert(data[i].stock_id);
                                            html1 += '<div>\n\
            <div class = "form-group">\n\
\n\
            <input type = "text" required=""  class = "form-control" style="width:80px" readonly="" name="description[]" id = "description" value="' + data[i].description + '" placeholder = "Description">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" required="" class = "form-control" name="amount_owed[]" readonly="" style="width:80px" id = "amount_owed" value="' + data[i].amount_owed + '" placeholder = "Amount Paid">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" required="" class = "form-control" name="amount_paid[]"  style="width:80px" id = "amount_paid"  placeholder = "Amount Paid">\n\
</div>\n\
 <div class = "form-group">\n\
\n\
<input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
<input type="hidden" required="" readonly="" name="patient_visit_statement_id[]" id="patient_visit_statement_id" value="' + data[i].patient_visit_statement_id + '">\n\
<input type="hidden" required="" readonly="" name="patient_id[]" id="patient_id" value="' + data[i].patient_id + '">\n\
<input type="hidden" required="" readonly="" name="visit_id[]" id="visit_id" value="' + data[i].visit_id + '">\n\
<input type="hidden" required="" readonly="" name="patient_payment_id[]" id="patient_payment_id" value="' + data[i].patient_payment_id + '">\n\
<input type="hidden" required="" readonly="" name="amount_paid_td[]" id="amount_paid_td" value="' + data[i].amount_paid + '">\n\
\n\
</div>\n\
<div class = "form-group">\n\
\n\
\n\
<select required="" name="payment_method[]" class="" id="payment_method">\n\
<option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
<option value="Cash">Cash</option>\n\
<option value="M-Pesa">M-Pesa</option>\n\
<option value="Airtel Money">Airtel Money</option>\n\
<option value="Orange Money">Orange Money</option>\n\
<option value="PDQ">PDQ </option></select>\n\
</div>\n\
<div class = "form-group">\n\
\n\
<textarea type = "text" style="width:80px" required="" class = "form-control" name="payment_code[]" id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
</div>\n\
            </div>';
                                        }

                                        htmlhead1 += '<div>\n\
            <div class = "form-group">\n\
<label class = "label label-info" for = "description">Description</label>\n\
</div>\n\
<div class = "form-group">\n\
 <label class = "label label-info" for = "amount_charged">Amount Charged</label>\n\
</div>\n\
\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "balance_remaining">Balance Remaining</label>\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "amount_paid">Amount Paid</label>\n\
</div>\n\
 <div class = "form-group">\n\
<label class = "label label-info" for = "quantity">Quantity</label>\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "payment_method">Payment Method </label>\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "payment_code">Payment Code </label>\n\
</div>\n\
            </div>';
                                        $('#regular_patient_payment_div').empty();
                                        $('#regular_patient_payment_div').append(htmlhead1);
                                        $('#regular_patient_payment_div').append(html1);
                                    },
                                    error: function (data) {

                                    }
                                });
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/get_patient_name/" + visit_id + "/" + patient_id,
                                    dataType: "JSON",
                                    success: function (response) {
                                        $('#regular_patient_name').val(response[0].patient_name);
                                    }
                                });
                            });
                            $('#regular_patient_payment_form').submit(function (event) {
                                dataString = $("#regular_patient_payment_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/cashier/regular_patient_make_payment",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Payments saved Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.pathname;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });











                            //Lab Service Payments


                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/lab_service_payments",
                                    dataType: "JSON",
                                    success: function (payment_list) {
                                        pat_payment_list = $('#lab_service_payments').empty();
                                        $.each(payment_list, function (i, payment_list) {
                                            pat_payment_list.append('<li><a href="#lab_service_payment_div" id="lab_service_payment_link" class="lab_service_payment_link"><i class = "glyphicon glyphicon-user"> </i>\n\
                ' + payment_list.patient_name + '</a>\n\
      <input type="hidden" id="patient_id" name="patient_id_list" class="patient_id" value="' + payment_list.patient_id + '"/>\n\
<input type="hidden" id="visit_id" class="visit_id" name="visit_id_list" value="' + payment_list.visit_id + '" /></br></li>');
                                        });
                                    },
                                    error: function (data) {
                                        //error do something
                                        // alert(data);
                                    }
                                });
                            }, 3000);
                            $('.lab_service_payments').on('click', '.lab_service_payment_link', function () {
                                //alert('Alert!!');
                                //get
                                var visit_id = $(this).closest('li').find('input[name="visit_id_list"]').val();
                                //alert(visit_id);
                                var patient_id = $(this).closest('li').find('input[name="patient_id_list"]').val();
                                //alert(patient_id);
                                html1 = '';
                                htmlhead1 = '';
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/lab_service_payments_details/" + visit_id + "/" + patient_id,
                                    dataType: "JSON",
                                    success: function (data) {
                                        for (i = 0; i < data.length; i++) {

                                            // alert(data[i].stock_id);
                                            html1 += '<div>\n\
            <div class = "form-group">\n\
\n\
            <input type = "text" required=""  class = "form-control" style="width:80px" readonly="" name="description[]" id = "description" value="' + data[i].description + '" placeholder = "Description">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" required="" class = "form-control" name="amount_owed[]" readonly="" style="width:80px" id = "amount_owed" value="' + data[i].amount_owed + '" placeholder = "Amount Paid">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" required="" class = "form-control" name="amount_paid[]"  style="width:80px" id = "amount_paid"  placeholder = "Amount Paid">\n\
</div>\n\
 <div class = "form-group">\n\
\n\
<input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
<input type="hidden" required="" readonly="" name="patient_visit_statement_id[]" id="patient_visit_statement_id" value="' + data[i].patient_visit_statement_id + '">\n\
<input type="hidden" required="" readonly="" name="patient_id[]" id="patient_id" value="' + data[i].patient_id + '">\n\
<input type="hidden" required="" readonly="" name="visit_id[]" id="visit_id" value="' + data[i].visit_id + '">\n\
<input type="hidden" required="" readonly="" name="patient_payment_id[]" id="patient_payment_id" value="' + data[i].patient_payment_id + '">\n\
<input type="hidden" required="" readonly="" name="amount_paid_td[]" id="amount_paid_td" value="' + data[i].amount_paid + '">\n\
\n\
</div>\n\
<div class = "form-group">\n\
\n\
\n\
<select required="" name="payment_method[]" class="" id="payment_method">\n\
<option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
<option value="Cash">Cash</option>\n\
<option value="M-Pesa">M-Pesa</option>\n\
<option value="Airtel Money">Airtel Money</option>\n\
<option value="Orange Money">Orange Money</option>\n\
<option value="PDQ">PDQ </option></select>\n\
</div>\n\
<div class = "form-group">\n\
\n\
<textarea type = "text" style="width:80px" required="" class = "form-control" name="payment_code[]" id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
</div>\n\
            </div>';
                                        }

                                        htmlhead1 += '<div>\n\
            <div class = "form-group">\n\
<label class = "label label-info" for = "description">Description</label>\n\
</div>\n\
<div class = "form-group">\n\
 <label class = "label label-info" for = "amount_charged">Amount Charged</label>\n\
</div>\n\
\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "balance_remaining">Balance Remaining</label>\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "amount_paid">Amount Paid</label>\n\
</div>\n\
 <div class = "form-group">\n\
<label class = "label label-info" for = "quantity">Quantity</label>\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "payment_method">Payment Method </label>\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "payment_code">Payment Code </label>\n\
</div>\n\
            </div>';
                                        $('#patient_lab_service_payment_div').empty();
                                        $('#patient_lab_service_payment_div').append(htmlhead1);
                                        $('#patient_lab_service_payment_div').append(html1);
                                    },
                                    error: function (data) {

                                    }
                                });
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/get_patient_name/" + visit_id + "/" + patient_id,
                                    dataType: "JSON",
                                    success: function (response) {
                                        $('#lab_service_patient_name').val(response[0].patient_name);
                                    }
                                });
                            });
                            $('#lab_service_payment_form').submit(function (event) {
                                dataString = $("#lab_service_payment_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/cashier/lab_service_make_payment",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Payments saved Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.pathname;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });

















                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/pay_at_the_end",
                                    dataType: "JSON",
                                    success: function (payment_list) {
                                        pat_payment_list = $('#pay_at_the_end_payment_list').empty();
                                        if (payment_list === null) {

                                        } else {
                                            $.each(payment_list, function (i, payment_list) {


                                                pat_payment_list.append('<li><a href="#pay_at_the_end_patient_form" id="pay_at_the_end_patient_payment_link" class="pay_at_the_end_patient_payment_link"><i class = "glyphicon glyphicon-user"> </i>' + payment_list.patient_name + '</a>\n\
      <input type="hidden" id="patient_id" name="patient_id" class="patient_id" value="' + payment_list.patient_id + '"/>\n\
<input type="hidden" id="visit_id" class="visit_id" name="visit_id" value="' + payment_list.visit_id + '" /></br></li>');
                                            });
                                        }


                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                            $('.pay_at_the_end_payment_list').on('click', '.pay_at_the_end_patient_payment_link', function () {
                                // alert('Alert!!');
                                //get
                                var visit_id = $(this).closest('li').find('input[name="visit_id"]').val();
                                // alert(visit_id);
                                var patient_id = $(this).closest('li').find('input[name="patient_id"]').val();
                                // alert(patient_id);
                                html1 = '';
                                htmlhead1 = '';
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/pay_at_the_end_details/" + visit_id + "/" + patient_id,
                                    dataType: "JSON",
                                    success: function (data) {
                                        for (i = 0; i < data.length; i++) {

                                            // alert(data[i].stock_id);
                                            html1 += '<div>\n\
            <div class = "form-group">\n\
\n\
            <input type = "text" required=""  class = "form-control" style="width:80px" readonly="" name="description[]" id = "description" value="' + data[i].description + '" placeholder = "Description">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" required="" class = "form-control" name="amount_owed[]" readonly="" style="width:80px" id = "amount_owed" value="' + data[i].amount_owed + '" placeholder = "Amount Paid">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" required="" class = "form-control" name="amount_paid[]"  style="width:80px" id = "amount_paid"  placeholder = "Amount Paid">\n\
</div>\n\
 <div class = "form-group">\n\
\n\
<input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
<input type="hidden" required="" readonly="" name="patient_visit_statement_id[]" id="patient_visit_statement_id" value="' + data[i].patient_visit_statement_id + '">\n\
<input type="hidden" required="" readonly="" name="patient_id[]" id="patient_id" value="' + data[i].patient_id + '">\n\
<input type="hidden" required="" readonly="" name="visit_id[]" id="visit_id" value="' + data[i].visit_id + '">\n\
<input type="hidden" required="" readonly="" name="patient_payment_id[]" id="patient_payment_id" value="' + data[i].patient_payment_id + '">\n\
<input type="hidden" required="" readonly="" name="amount_paid_td[]" id="amount_paid_td" value="' + data[i].amount_paid + '">\n\
\n\
</div>\n\
<div class = "form-group">\n\
\n\
\n\
<select required="" name="payment_method[]" class="" id="payment_method">\n\
<option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
<option value="Cash">Cash</option>\n\
<option value="M-Pesa">M-Pesa</option>\n\
<option value="Airtel Money">Airtel Money</option>\n\
<option value="Orange Money">Orange Money</option>\n\
<option value="PDQ">PDQ </option></select>\n\
</div>\n\
<div class = "form-group">\n\
\n\
<textarea type = "text" style="width:80px" required="" class = "form-control" name="payment_code[]" id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
</div>\n\
            </div>';
                                        }

                                        htmlhead1 += '<div>\n\
            <div class = "form-group">\n\
<label class = "label label-info" for = "description">Description</label>\n\
</div>\n\
<div class = "form-group">\n\
 <label class = "label label-info" for = "amount_charged">Amount Charged</label>\n\
</div>\n\
\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "balance_remaining">Balance Remaining</label>\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "amount_paid">Amount Paid</label>\n\
</div>\n\
 <div class = "form-group">\n\
<label class = "label label-info" for = "quantity">Quantity</label>\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "payment_method">Payment Method </label>\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "payment_code">Payment Code </label>\n\
</div>\n\
            </div>';
                                        $('#pay_at_the_end_patient_payment_div').empty();
                                        $('#pay_at_the_end_patient_payment_div').append(htmlhead1);
                                        $('#pay_at_the_end_patient_payment_div').append(html1);
                                    },
                                    error: function (data) {

                                    }
                                });
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/get_patient_name/" + visit_id + "/" + patient_id,
                                    dataType: "JSON",
                                    success: function (response) {
                                        $('#pay_at_the_end_patient_name').val(response[0].patient_name);
                                    }
                                });
                            });
                            $('#pay_at_the_end_patient_payment_form').submit(function (event) {
                                dataString = $("#pay_at_the_end_patient_payment_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/cashier/pay_at_the_end_make_payment",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Payments saved Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.pathname;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/walkin_patient_nurse_payments",
                                    dataType: "JSON",
                                    success: function (payment_list) {
                                        pat_payment_list = $('#walkin_nurse_patient_payment').empty();
                                        if (payment_list === null) {

                                        } else {
                                            $.each(payment_list, function (i, payment_list) {


                                                pat_payment_list.append('<li><a href="#walkin_patient_nurse_form" id="walkin_nurse_payment_link" class="walkin_nurse_payment_link"><i class = "glyphicon glyphicon-user"> </i>' + payment_list.walkin_patient_name + '</a>\n\
      <input type="hidden" id="walkin_id_list" name="walkin_id_list" class="walkin_id_list" value="' + payment_list.walkin_id + '"/>\n\
</br></li>');
                                            });
                                        }


                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                            $('.walkin_nurse_patient_payment').on('click', '.walkin_nurse_payment_link', function () {
                                // alert('Alert!!');
                                //get

                                // var walkin_id = $(this).closest('ul').find('input[name="walkin_id_list"]').val();
                                var walkin_id = $(this).closest('li').find('input[name="walkin_id_list"]').val();
                                //alert(walkin_id);
                                html1 = '';
                                htmlhead1 = '';
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/walkin_patient_nurse_payments_details/" + walkin_id + "/",
                                    dataType: "JSON",
                                    success: function (data) {
                                        for (i = 0; i < data.length; i++) {

                                            // alert(data[i].stock_id);
                                            html1 += '<div>\n\
            <div class = "form-group">\n\
\n\
      <input type="hidden" class="form-control" name="walkin_id[]" id="walkin_id" class="walkin_id" value="' + data[i].walkin_id + '"> \n\
<input type="hidden" class="form-control" name="walkin_patient_visit_statement_id[]" id="walkin_patient_visit_statement_id" class="walkin_patient_visit_statement_id" value="' + data[i].walkin_patient_visit_statement_id + '">  \n\
    <textarea type = "text" class = "form-control" readonly="" name="description[]" id = "description"  placeholder = "Description">' + data[i].description + '</textarea>\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" class = "form-control" readonly="" style="width:80px" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" class = "form-control"  name="amount_paid[]" style="width:80px" id = "amount_paid" value="' + data[i].amount_paid + '" placeholder = "Amount Paid">\n\
</div>\n\
 <div class = "form-group">\n\
\n\
<input type = "text" class = "form-control" readonly="" style="width:80px" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
</div>\n\
<div class = "form-group">\n\
\n\
\n\
<select required="" name="payment_method[]" class="" id="payment_method">\n\
<option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
<option value="Cash">Cash</option>\n\
<option value="M-Pesa">M-Pesa</option>\n\
<option value="Airtel Money">Airtel Money</option>\n\
<option value="Orange Money">Orange Money</option>\n\
<option value="PDQ">PDQ </option></select>\n\
</div>\n\
<div class = "form-group">\n\
\n\
<textarea type = "text" style="width:80px" class = "form-control" name="payment_code[] id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
</div>\n\
            </div>';
                                        }

                                        htmlhead1 += '<div>\n\
            <div class = "form-group">\n\
<label class = "label label-info" for = "description">Description</label>\n\
\n\
</div>\n\
<div class = "form-group">\n\
 <label class = "label label-info" for = "amount_charged">Amount Charged</label>\n\
\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "amount_paid">Amount Paid</label>\n\
\n\
</div>\n\
 <div class = "form-group">\n\
<label class = "label label-info" for = "quantity">Quantity</label>\n\
\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "payment_method">Payment Method </label>\n\
\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "payment_code">Payment Code </label>\n\
\n\
</div>\n\
            </div>';
                                        $('#walkin_patient_nurse_payment_div').empty();
                                        $('#walkin_patient_nurse_payment_div').append(htmlhead1);
                                        $('#walkin_patient_nurse_payment_div').append(html1);
                                    },
                                    error: function (data) {

                                    }
                                });
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/get_walkin_patient_name/" + walkin_id + "/",
                                    dataType: "JSON",
                                    success: function (response) {
                                        $('#walkin_nurse_patient_name').val(response[0].patient_name);
                                    }
                                });
                            });
                            $('#walkin_patient_nurse_payment_form').submit(function (event) {
                                dataString = $("#walkin_patient_nurse_payment_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/cashier/walkin_patient_nurse_make_payment",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Payments saved Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.pathname;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/walkin_patient_lab_payments",
                                    dataType: "JSON",
                                    success: function (payment_list) {
                                        pat_payment_list = $('#walkin_lab_patient_payment').empty();
                                        if (payment_list === null) {

                                        } else {
                                            $.each(payment_list, function (i, payment_list) {


                                                pat_payment_list.append('<li><a href="#walkin_patient_lab_form" id="walkin_lab_payment_link" class="walkin_lab_payment_link"><i class = "glyphicon glyphicon-user"> </i>' + payment_list.walkin_patient_name + '</a>\n\
      <input type="hidden" id="walkin_id_list" name="walkin_id_list" class="walkin_id_list" value="' + payment_list.walkin_id + '"/>\n\
</br></li>');
                                            });
                                        }


                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                            $('.walkin_lab_patient_payment').on('click', '.walkin_lab_payment_link', function () {
                                // alert('Alert!!');
                                //get

                                // var walkin_id = $(this).closest('ul').find('input[name="walkin_id_list"]').val();
                                var walkin_id = $(this).closest('li').find('input[name="walkin_id_list"]').val();
                                //alert(walkin_id);
                                html1 = '';
                                htmlhead1 = '';
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/walkin_patient_lab_payments_details/" + walkin_id + "/",
                                    dataType: "JSON",
                                    success: function (data) {
                                        for (i = 0; i < data.length; i++) {

                                            // alert(data[i].stock_id);
                                            html1 += '<div>\n\
            <div class = "form-group">\n\
\n\
      <input type="hidden" class="form-control" name="walkin_id[]" id="walkin_id" class="walkin_id" value="' + data[i].walkin_id + '"> \n\
<input type="hidden" class="form-control" name="walkin_patient_visit_statement_id[]" id="walkin_patient_visit_statement_id" class="walkin_patient_visit_statement_id" value="' + data[i].walkin_patient_visit_statement_id + '">  \n\
    <textarea type = "text" class = "form-control" readonly="" name="description[]" id = "description"  placeholder = "Description">' + data[i].description + '</textarea>\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" class = "form-control" readonly="" style="width:80px" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" class = "form-control"  name="amount_paid[]" style="width:80px" id = "amount_paid" value="' + data[i].amount_paid + '" placeholder = "Amount Paid">\n\
</div>\n\
 <div class = "form-group">\n\
\n\
<input type = "text" class = "form-control" readonly="" style="width:80px" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
</div>\n\
<div class = "form-group">\n\
\n\
\n\
<select required="" name="payment_method[]" class="" id="payment_method">\n\
<option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
<option value="Cash">Cash</option>\n\
<option value="M-Pesa">M-Pesa</option>\n\
<option value="Airtel Money">Airtel Money</option>\n\
<option value="Orange Money">Orange Money</option>\n\
<option value="PDQ">PDQ </option></select>\n\
</div>\n\
<div class = "form-group">\n\
\n\
<textarea type = "text" style="width:80px" class = "form-control" name="payment_code[] id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
</div>\n\
            </div>';
                                        }

                                        htmlhead1 += '<div>\n\
            <div class = "form-group">\n\
<label class = "label label-info" for = "description">Description</label>\n\
\n\
</div>\n\
<div class = "form-group">\n\
 <label class = "label label-info" for = "amount_charged">Amount Charged</label>\n\
\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "amount_paid">Amount Paid</label>\n\
\n\
</div>\n\
 <div class = "form-group">\n\
<label class = "label label-info" for = "quantity">Quantity</label>\n\
\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "payment_method">Payment Method </label>\n\
\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" for = "payment_code">Payment Code </label>\n\
\n\
</div>\n\
            </div>';
                                        $('#walkin_patient_lab_payment_div').empty();
                                        $('#walkin_patient_lab_payment_div').append(htmlhead1);
                                        $('#walkin_patient_lab_payment_div').append(html1);
                                    },
                                    error: function (data) {

                                    }
                                });
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/get_walkin_patient_name/" + walkin_id + "/",
                                    dataType: "JSON",
                                    success: function (response) {
                                        $('#walkin_lab_patient_name').val(response[0].patient_name);
                                    }
                                });
                            });
                            $('#walkin_patient_lab_payment_form').submit(function (event) {
                                dataString = $("#walkin_patient_lab_payment_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/cashier/walkin_patient_lab_make_payment",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Payments saved Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.pathname;
                                            $(location).attr('href', url);
                                        }, 3000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/walkin_patient_pharmacy_payments",
                                    dataType: "JSON",
                                    success: function (payment_list) {
                                        pat_payment_list = $('#walkin_pharmacy_patient_payment').empty();
                                        if (payment_list === null) {

                                        } else {
                                            $.each(payment_list, function (i, payment_list) {


                                                pat_payment_list.append('<li><a href="#walkin_patient_pharmacy_form" id="walkin_pharmacy_payment_link" class="walkin_pharmacy_payment_link"><i class = "glyphicon glyphicon-user"> </i>' + payment_list.walkin_patient_name + '</a>\n\
      <input type="hidden" id="walkin_id_list" name="walkin_id_list" class="walkin_id_list" value="' + payment_list.walkin_id + '"/>\n\
</br></li>');
                                            });
                                        }


                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                            $('.walkin_pharmacy_patient_payment').on('click', '.walkin_pharmacy_payment_link', function () {
                                // alert('Alert!!');
                                //get

                                // var walkin_id = $(this).closest('ul').find('input[name="walkin_id_list"]').val();
                                var walkin_id = $(this).closest('li').find('input[name="walkin_id_list"]').val();
                                //alert(walkin_id);
                                html1 = '';
                                htmlhead1 = '';
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/walkin_patient_pharmacy_payments_details/" + walkin_id + "/",
                                    dataType: "JSON",
                                    success: function (data) {
                                        for (i = 0; i < data.length; i++) {

                                            // alert(data[i].stock_id);
                                            html1 += '<div>\n\
            <div class = "form-group">\n\
\n\
      <input type="hidden" class="form-control" name="walkin_id[]" id="walkin_id" class="walkin_id" value="' + data[i].walkin_id + '"> \n\
<input type="hidden" class="form-control" name="walkin_statement_id[]" id="walkin_statement_id" class="walkin_statement_id" value="' + data[i].walkin_patient_visit_statement_id + '">  \n\
    <textarea type = "text" class = "form-control" readonly="" name="description[]" id = "description"  placeholder = "Description">' + data[i].description + '</textarea>\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" class = "form-control" readonly="" style="width:80px" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<input type = "text" class = "form-control"  name="amount_paid[]" style="width:80px" id = "amount_paid" value="' + data[i].amount_paid + '" placeholder = "Amount Paid">\n\
</div>\n\
\n\
<div class = "form-group">\n\
\n\
<input type = "text" class = "form-control" readonly="" name="amount_owed[]" style="width:80px" id = "amount_owed" value="' + data[i].amount_owed + '" placeholder = "Balance Remaining">\n\
</div>\n\
\n\
 <div class = "form-group">\n\
\n\
<input type = "text" class = "form-control" readonly="" style="width:80px" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
</div>\n\
<div class = "form-group">\n\
\n\
<select required="" name="payment_method[]" class="" id="payment_method">\n\
<option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
<option value="Cash">Cash</option>\n\
<option value="M-Pesa">M-Pesa</option>\n\
<option value="Airtel Money">Airtel Money</option>\n\
<option value="Orange Money">Orange Money</option>\n\
<option value="PDQ">PDQ </option></select>\n\
</div>\n\
<div class = "form-group">\n\
\n\
<textarea type = "text" style="width:80px" class = "form-control" name="payment_code[] id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
</div>\n\
            </div>';
                                        }

                                        htmlhead1 += '<div>\n\
            <div class = "form-group">\n\
<label class = "label label-info" style="-webkit-column-gap: 50px; /* Chrome, Safari, Opera */-moz-column-gap: 50px; /* Firefox */column-gap: 50px;" for = "description">Description</pharmacyel>\n\
\n\
</div>\n\
<div class = "form-group">\n\
 <label class = "label label-info" style="-webkit-column-gap: 50px; /* Chrome, Safari, Opera */-moz-column-gap: 50px; /* Firefox */column-gap: 50px;" for = "amount_charged">Amount Charged</label>\n\
\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" style="-webkit-column-gap: 50px; /* Chrome, Safari, Opera */-moz-column-gap: 50px; /* Firefox */column-gap: 50px;"  for = "amount_paid">Amount Paid</label>\n\
\n\
</div>\n\
 <div class = "form-group">\n\
<label class = "label label-info" style="-webkit-column-gap: 50px; /* Chrome, Safari, Opera */-moz-column-gap: 50px; /* Firefox */column-gap: 50px;" for = "quantity">Quantity</label>\n\
\n\
</div>\n\
<div class = "form-group">\n\
<label class = "label label-info" style="-webkit-column-gap: 50px; /* Chrome, Safari, Opera */-moz-column-gap: 50px; /* Firefox */column-gap: 50px;" for = "payment_method">Payment Method </label>\n\
\n\
</div>\n\
<div class = "form-group">\n\
<pharmacyel class = "label label-info" style="-webkit-column-gap: 50px; /* Chrome, Safari, Opera */-moz-column-gap: 50px; /* Firefox */column-gap: 50px;" for = "payment_code">Payment Code </pharmacyel>\n\
\n\
</div>\n\
            </div>';
                                        $('#walkin_patient_pharmacy_payment_div').empty();
                                        $('#walkin_patient_pharmacy_payment_div').append(htmlhead1);
                                        $('#walkin_patient_pharmacy_payment_div').append(html1);
                                    },
                                    error: function (data) {

                                    }
                                });
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/get_walkin_patient_name/" + walkin_id + "/",
                                    dataType: "JSON",
                                    success: function (response) {
                                        $('#walkin_pharmacy_patient_name').val(response[0].patient_name);
                                    }
                                });
                            });
                            $('#walkin_patient_pharmacy_payment_form').submit(function (event) {
                                dataString = $("#walkin_patient_pharmacy_payment_form").serialize();
                                $.fancybox.open([
                                    {
                                        href: '#ajax_loader',
                                        title: 'Please wait information being updated...'
                                    }
                                ], {
                                    padding: 0
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>index.php/cashier/walkin_patient_pharmacy_make_payment",
                                    data: dataString,
                                    success: function (data) {
                                        $(".loader_notify").notify(
                                                "Patient Payments saved Successfully",
                                                "success",
                                                {position: "center"}
                                        );
                                        setInterval(function () {
                                            var url = window.location.pathname;
                                            $(location).attr('href', url);
                                        }, 300000);
                                    }

                                });
                                event.preventDefault();
                                return false;
                            });
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/procedure_payments",
                                    dataType: "JSON",
                                    success: function (payment_list) {
                                        pat_payment_list = $('#procedure_patient_payments').empty();
                                        if (payment_list === null) {

                                        } else {
                                            $.each(payment_list, function (i, payment_list) {

                                                if (payment_list.urgency === "urgent") {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.urgency + '</span><span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');
                                                }
                                                else if (payment_list.urgency !== "urgent")
                                                {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');
                                                }
                                                else {

                                                    pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                }

                                            });
                                        }


                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/lab_payments",
                                    dataType: "JSON",
                                    success: function (payment_list) {
                                        pat_payment_list = $('#lab_patient_payments').empty();
                                        if (payment_list === null) {

                                        } else {
                                            $.each(payment_list, function (i, payment_list) {

                                                if (payment_list.urgency === "urgent") {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.urgency + '</span><span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');
                                                }
                                                else if (payment_list.urgency !== "urgent")
                                                {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');
                                                }
                                                else {

                                                    pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                }

                                            });
                                        }


                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>cashier/pharmacy_payments",
                                    dataType: "JSON",
                                    success: function (pharm_payment_list) {
                                        pat_payment_list = $('#pharmacy_patient_payments').empty();
                                        if (pharm_payment_list === null) {

                                        } else {
                                            $.each(pharm_payment_list, function (i, pharm_payment_list) {

                                                if (pharm_payment_list.urgency === "urgent") {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + pharm_payment_list.f_name + " " + pharm_payment_list.s_name + " " + pharm_payment_list.other_name + '</a><span style="color:red !important;">' + pharm_payment_list.urgency + '</span><span style="color:red !important;">' + pharm_payment_list.total_cost + '</span></li></br>');
                                                }
                                                else if (pharm_payment_list.urgency !== "urgent")
                                                {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + pharm_payment_list.f_name + " " + pharm_payment_list.s_name + " " + pharm_payment_list.other_name + '</a><span style="color:red !important;">' + pharm_payment_list.total_cost + '</span></li></br>');
                                                }
                                                else {

                                                    pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                }

                                            });
                                        }


                                    },
                                    error: function (data) {
                                        //error do something
                                    }
                                });
                            }, 3000);
                        } else if (nurse_url) {


                        } else if (doctor_url) {

                            var doctor_patient_profile_url = window.location.href.indexOf("doctor_patient_profile") > -1;


                            if (doctor_patient_profile_url) {











                                var visit_id = $('#checking_visit_id').val();
                                var patient_id = $('#checking_patient_id').val();



                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>doctor/get_review_of_systems/" + visit_id + "/" + patient_id,
                                    dataType: "json",
                                    success: function (response) {

                                        $('#constitutional').val(response[0].constitutional);
                                        $('#ent').val(response[0].ent);
                                        $('#cardio_vascular').val(response[0].cardiovascular);
                                        $('#eye').val(response[0].eye);
                                        $('#respiratory').val(response[0].respiratory);
                                        $('#gastro_intestinal').val(response[0].gastrointestinal);
                                        $('#genito_urinary').val(response[0].genitourinary);
                                        $('#masculo_skeletal').val(response[0].masculoskeletal);
                                        $('#skin').val(response[0].skin);
                                        $('#neuro_logic').val(response[0].neurologic);
                                        $('#other_systems').val(response[0].other);
                                        $('#viist_id').val(response[0].visit_id);
                                        $('#patient_id').val(response[0].patient_id);
                                    },
                                    error: function (data) {

                                    }
                                });




                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>doctor/get_chief_complaints/" + visit_id + "/" + patient_id,
                                    dataType: "json",
                                    success: function (response) {
                                        ;
                                        $('#chief_complaints_txt_area').val(response[0].complaints);
                                        $('#working_diagnosis_txt_area').val(response[0].working_diagnosis);
                                        $('#consultation_id').val(response[0].consultation_id);
                                        $('#visit_id').val(response[0].visit_id);
                                    },
                                    error: function (data) {

                                    }
                                });






                                $('#send_to_pharmacy_form').submit(function (event) {
                                    dataString = $("#send_to_pharmacy_form").serialize();
                                    $.fancybox.open([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ], {
                                        padding: 0
                                    });
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url() ?>index.php/nurse/send_to_pharmacy",
                                        data: dataString,
                                        success: function (data) {
                                            $(".loader_notify").notify(
                                                    "Patient Sent to Pharmacy Successfully",
                                                    "success",
                                                    {position: "center"}
                                            );
                                            setInterval(function () {
                                                var url = "<?php echo base_url() ?>home";
                                                $(location).attr('href', url);
                                            }, 300000);
                                        }

                                    });
                                    event.preventDefault();
                                    return false;
                                });



                                $('#order_tests_form').submit(function (event) {


                                    // order_tests_form();

                                    dataString = $("#order_tests_form").serialize();
                                    $.fancybox.open([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ], {
                                        padding: 0
                                    });
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url() ?>index.php/doctor/order_lab_tests",
                                        data: dataString,
                                        success: function (data) {
                                            $(".loader_notify").notify(
                                                    "Lab Orders  made Successfully",
                                                    "success",
                                                    {position: "center"}
                                            );

                                            setInterval(function () {
                                                var url = "<?php echo base_url() ?>home";
                                                $(location).attr('href', url);
                                            }, 3000);


                                        }

                                    });
                                    event.preventDefault();
                                    return false;





                                });


                                $('#order_commodity_form').submit(function (event) {


                                    // order_tests_form();

                                    dataString = $("#order_commodity_form").serialize();
                                    $.fancybox.open([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ], {
                                        padding: 0
                                    });
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url() ?>index.php/doctor/order_commodity",
                                        data: dataString,
                                        success: function (data) {
                                            $(".loader_notify").notify(
                                                    " Order made Successfully",
                                                    "success",
                                                    {position: "center"}
                                            );

                                            setInterval(function () {
                                                var url = window.location.path;
                                                $(location).attr('href', url);
                                            }, 300000);


                                        }

                                    });
                                    event.preventDefault();
                                    return false;





                                });




                                $('#working_diagnosis').keyup(function () {
                                    working_diagnosis();
                                });



                                $(".icd10_selector").change(function () {
                                    working_diagnosis();
                                });



                                $("#constitutional").keyup(function () {

                                    review_of_systems();
                                });


                                $("#ent").keyup(function () {
                                    review_of_systems();
                                });


                                $("#cardio_vascular").keyup(function () {
                                    review_of_systems();
                                });


                                $("#eye").keyup(function () {
                                    review_of_systems();
                                });


                                $("#respiratory").keyup(function () {
                                    review_of_systems();
                                });


                                $("#gastro_intestinal").keyup(function () {
                                    review_of_systems();
                                });


                                $("#genito_urinary").keyup(function () {
                                    review_of_systems();
                                });


                                $("#masculo_skeletal").keyup(function () {
                                    review_of_systems();
                                });


                                $("#skin").keyup(function () {
                                    review_of_systems();
                                });


                                $("#neuro_logic").keyup(function () {
                                    review_of_systems();
                                });


                                $("#other_systems").keyup(function () {
                                    review_of_systems();
                                });

                                $("#chief_complaints").keyup(function () {

                                    chief_complaints();
                                });

                                $("#reason_for_visit").keyup(function () {
                                    reason_for_visit();
                                });

                                $("#Emergency_0").change(function () {
                                    reason_for_visit();
                                });







                                $(function () {
                                    var $sfield = $('#icd_10').autocomplete({
                                        source: function (request, response) {
                                            var icd_10_value = $("#icd_10").val();

                                            var url = "<?php echo site_url('doctor/auto_icd_10'); ?>/" + icd_10_value;
                                            $.post(url, {data: request.term}, function (data) {
                                                response($.map(data, function (consultation_icd10) {
                                                    return {
                                                        value: consultation_icd10.icd_description
                                                    };
                                                }));
                                            }, "json");
                                        },
                                        minLength: 2,
                                        autofocus: true
                                    });
                                });




















                            } else {



                                setInterval(function () {
                                    $.ajax({
                                        type: "GET",
                                        url: "<?php echo base_url(); ?>doctor/doctor_patient_list",
                                        dataType: "JSON",
                                        success: function (doctor_patient_list) {
                                            patient_intray = $('#regular_patients_in_tray').empty();
                                            if (patient_intray === null) {
                                                patient_intray.append("<p>No Patients In Tray</p>");
                                            } else {
                                                $.each(doctor_patient_list, function (i, doctor_patient_list) {
                                                    if (patient_intray.total_patient_visits === null) {
                                                        patient_intray.append("<li>No Patients In Tray</li>");
                                                    } else {
                                                        if (doctor_patient_list.patient_id === null) {
                                                            patient_intray.append("<li>No Patients In Tray</li>");
                                                        } else {
                                                            if (doctor_patient_list.urgency !== "urgent") {
                                                                patient_intray.append('<li><a href="<?php echo base_url() ?>doctor/doctor_patient_profile/' + doctor_patient_list.patient_id + '/' + doctor_patient_list.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + doctor_patient_list.title + " : " + doctor_patient_list.f_name + " " + doctor_patient_list.s_name + " " + doctor_patient_list.other_name + '</a></li></br>');

                                                            } else {
                                                                patient_intray.append('<li><a href="<?php echo base_url() ?>doctor/doctor_patient_profile/' + doctor_patient_list.patient_id + '/' + doctor_patient_list.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + doctor_patient_list.title + " : " + doctor_patient_list.f_name + " " + doctor_patient_list.s_name + " " + doctor_patient_list.other_name + '</a><span style="color:red !important;"> Urgent</span></li></br>');

                                                            }
                                                        }
                                                    }

                                                });
                                            }



                                        },
                                        error: function (data) {
                                            //
                                            //  alert('An error occured, kindly try later');
                                        }
                                    });
                                }, 3000);




                            }



















                        } else if (lab_url) {


                            $("#test_results_text_area").keyup(function () {
                                add_lab_test_results();
                            });

                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>lab/total_regular_patients_in_tray",
                                    dataType: "JSON",
                                    success: function (total_regular_patients) {
                                        total_visit = $('#total_regular_patients_in_tray_today').empty();
                                        if (total_visit === null) {
                                            total_visit.append("<p>No Patients In Tray</p>");
                                        } else {
                                            $.each(total_regular_patients, function (i, total_regular_patients) {
                                                if (total_regular_patients.total_patient_visits === null) {
                                                    total_visit.append("<p>No Patients In Tray</p>");
                                                } else {
                                                    total_visit.append('<p>' + total_regular_patients.total_regular_patients_in_tray + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);





                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>lab/lab_patient_list",
                                    dataType: "JSON",
                                    success: function (doctor_patient_list) {
                                        patient_intray = $('#regular_patients_in_tray').empty();
                                        if (patient_intray === null) {
                                            patient_intray.append("<p>No Patients In Tray</p>");
                                        } else {
                                            $.each(doctor_patient_list, function (i, doctor_patient_list) {
                                                if (patient_intray.total_patient_visits === null) {
                                                    patient_intray.append("<li>No Patients In Tray</li>");
                                                } else {
                                                    if (doctor_patient_list.patient_id === null) {
                                                        patient_intray.append("<li>No Patients In Tray</li>");
                                                    } else {
                                                        if (doctor_patient_list.urgency !== "urgent") {
                                                            patient_intray.append('<li><a href="<?php echo base_url() ?>lab/lab_patient_profile/' + doctor_patient_list.patient_id + '/' + doctor_patient_list.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + doctor_patient_list.title + " : " + doctor_patient_list.f_name + " " + doctor_patient_list.s_name + " " + doctor_patient_list.other_name + '</a></li></br>');

                                                        } else {
                                                            patient_intray.append('<li><a href="<?php echo base_url() ?>lab/lab_patient_profile/' + doctor_patient_list.patient_id + '/' + doctor_patient_list.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + doctor_patient_list.title + " : " + doctor_patient_list.f_name + " " + doctor_patient_list.s_name + " " + doctor_patient_list.other_name + '</a><span style="color:red !important;"> Urgent</span></li></br>');

                                                        }
                                                    }
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);





                        } else if (pharmacy_url) {




                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>pharmacy/pharmacy_patient_list",
                                    dataType: "JSON",
                                    success: function (pharmacy_patient_list) {
                                        patient_intray = $('#regular_patients_in_tray').empty();
                                        if (patient_intray === null) {
                                            patient_intray.append("<p>No Patients In Tray</p>");
                                        } else {
                                            $.each(pharmacy_patient_list, function (i, pharmacy_patient_lists) {
                                                if (patient_intray.total_patient_visits === null) {
                                                    patient_intray.append("<p>No Patients In Tray</p>");
                                                } else {
                                                    if (pharmacy_patient_lists.patient_id === null) {
                                                        patient_intray.append("<p>No Patients In Tray</p>");
                                                    } else {
                                                        if (pharmacy_patient_lists.urgency !== "urgent") {
                                                            patient_intray.append('<li><a href="<?php echo base_url() ?>pharmacy/pharmacy_patient_profile/' + pharmacy_patient_lists.patient_id + '/' + pharmacy_patient_lists.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + pharmacy_patient_lists.title + " : " + pharmacy_patient_lists.f_name + " " + pharmacy_patient_lists.s_name + " " + pharmacy_patient_lists.other_name + '</a></li></br>');

                                                        } else {
                                                            patient_intray.append('<li><a href="<?php echo base_url() ?>pharmacy/pharmacy_patient_profile/' + pharmacy_patient_lists.patient_id + '/' + pharmacy_patient_lists.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + pharmacy_patient_lists.title + " : " + pharmacy_patient_lists.f_name + " " + pharmacy_patient_lists.s_name + " " + pharmacy_patient_lists.other_name + '</a><span style="color:red !important;"> Urgent</span></li></br>');

                                                        }
                                                    }
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);


                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>pharmacy/pharmacy_walkin_patient_list",
                                    dataType: "JSON",
                                    success: function (pharmacy_patient_list) {
                                        patient_intray = $('#walkin_patients_in_tray').empty();
                                        if (patient_intray === null) {
                                            patient_intray.append("<p>No Patients In Tray</p>");
                                        } else {
                                            $.each(pharmacy_patient_list, function (i, pharmacy_patient_lists) {
                                                if (patient_intray.total_patient_visits === null) {
                                                    patient_intray.append("<p>No Patients In Tray</p>");
                                                } else {
                                                    if (pharmacy_patient_lists.patient_id === null) {
                                                        patient_intray.append("<p>No Patients In Tray</p>");
                                                    } else {
                                                        patient_intray.append('<li><a href="<?php echo base_url() ?>pharmacy/profile/' + pharmacy_patient_lists.walkin_id + '/"><i class = "glyphicon glyphicon-user"></i>' + pharmacy_patient_lists.walkin_patient_name + '</a></li></br>');
                                                    }
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);




                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>pharmacy/total_regular_patients_in_tray",
                                    dataType: "JSON",
                                    success: function (total_regular_patients) {
                                        total_visit = $('#total_regular_patients_in_tray_today').empty();
                                        if (total_visit === null) {
                                            total_visit.append("<p>No Patients In Tray</p>");
                                        } else {
                                            $.each(total_regular_patients, function (i, total_regular_patients) {
                                                if (total_regular_patients.total_patient_visits === null) {
                                                    total_visit.append("<p>No Patients In Tray</p>");
                                                } else {
                                                    total_visit.append('<p>' + total_regular_patients.total_regular_patients_in_tray + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);



                            setInterval(function () {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>pharmacy/total_walkin_patients_in_tray",
                                    dataType: "JSON",
                                    success: function (total_walkin_patients) {
                                        total_walkin_patientsa = $('#total_walkin_patients_in_tray_today').empty();
                                        if (total_walkin_patients === null) {
                                            total_walkin_patientsa.append("<p>No Patients In-Tray</p>");
                                        } else {
                                            $.each(total_walkin_patients, function (i, total_walkin_patientss) {
                                                if (total_walkin_patientss.total_walkin_patients_in_tray === 0) {
                                                    total_walkin_patientsa.append("<p>No Patients In-Tray</p>");
                                                } else {
                                                    total_walkin_patientsa.append('<p>' + total_walkin_patientss.total_walkin_patients_in_tray + '</p>');
                                                }

                                            });
                                        }



                                    },
                                    error: function (data) {
                                        //
                                        //  alert('An error occured, kindly try later');
                                    }
                                });
                            }, 3000);











                        }




                    });</script>





<link href='<?php echo base_url(); ?>assets/css/jquery-ui.css' rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>-->


<script type="text/javascript">
                    $(function () {
                        $("#sick_off_date_from").datepicker({
                            minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true
                        });
                        $("#sick_off_date_to").datepicker({
                            minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true
                        });


                        $("#previous_visit_date").datepicker({
                            minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true
                        });
                        $("#datepicker").datepicker({
                            minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true
                        });

                        $("#datepcker").datepicker({minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true});
                        $("#date_to").datepicker({minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true});
                        $("#date_from").datepicker({minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true});
                        $("#visitation_date_to").datepicker({minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true});
                        $("#visitation_date_from").datepicker({minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true});
                        $("#walkin_date_to").datepicker({minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true});
                        $("#walkin_date_from").datepicker({minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true});

                        $("#LMP").datepicker({minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true});

                        $("#LMP").datepicker({minDate: '-100Y',
                            maxDate: '+0m +0w',
                            changeMonth: true,
                            dateFormat: 'yy-mm-dd',
                            changeYear: true});
                    });</script>




<script type="text/javascript">
    $(function () {
        $("#datepicker_1").datepicker({
            minDate: '-100Y',
            maxDate: '+0m +0w',
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true
        });
        $("#datepcker_1").datepicker({dateFormat: 'yy-mm-dd'});
    });
    $(document).ready(function () {
        $('#example').dataTable({
            "scrollY": 200,
            "scrollX": true
        });
    });</script>




<script  src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>

<script  src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<link href='<?php echo base_url(); ?>assets/css/jquery.dataTables.css' rel="stylesheet">
<!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>-->
<!--<script type="text/javascript" src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>-->

<!--<link href='http://cdn.datatables.net/1.10.5/css/jquery.dataTables.css' rel='stylesheet'>-->



<script type="text/javascript">
    var j = jQuery.noConflict();
    j(document).ready(function () {
        j('.patient_test_table').dataTable({
            "scrollY": "200px",
            "scrollCollapse": true,
            "paging": false
        });
        j('.patients_report').dataTable({
            "scrollY": "200px",
            "scrollCollapse": true,
            "paging": false
        });

        j('#bill_patient_prescription_table').dataTable({
            "scrollY": "400px",
            "scrollCollapse": true,
            "paging": false
        });
        j('.procedure_reportss').dataTable({
            "scrollY": "200px",
            "scrollCollapse": true,
            "paging": false
        });
        j('.visitation_report').dataTable({
            "scrollY": "200px",
            "scrollCollapse": true,
            "paging": false
        });
        j('.walkin_report').dataTable({
            "scrollY": "400px",
            "scrollCollapse": true,
            "paging": false
        });
        j('.commodity_report').dataTable({
            "scrollY": "400px",
            "scrollCollapse": true,
            "paging": false
        });
        j('.triage_report_1').dataTable({
            "scrollY": "200px",
            "scrollCollapse": true,
            "paging": false
        });
        j('.triage_report').dataTable({
            "scrollY": "200px",
            "scrollCollapse": true,
            "paging": false
        });

        j('.procedure_report').dataTable({
            "scrollY": "200px",
            "scrollCollapse": true,
            "paging": false
        });
        j('.appointment_history_table').dataTable({
            "scrollY": "200px",
            "scrollCollapse": true,
            "paging": false
        });
    });
    j(document).ready(function () {

        j('.bill_patient_prescription_link').click(function () {


            //get data
            var prescription_id = j(this).closest('tr').find('input[name="prescription_id"]').val();

            j.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>pharmacy/get_presciprion_details/" + prescription_id,
                dataType: "json",
                success: function (response) {

                    j('#patient_lab_test_idsend_to_doctor_form').val(response[0].prescription_id);
                    j('#presc_prescription_tracker').val(response[0].prescription_tracker);
                    j('#presc_route_name').val(response[0].route);
                    j('#presc_frequency').val(response[0].frequency);
                    
                    j('#presc_strength').val(response[0].strength);
                    j('#presc_commodity_name').val(response[0].commodity_name);
                    j('#presc_is_dispensed').val(response[0].is_dispensed);
                    j('#presc_quantity_issued').val(response[0].quantity_issued);
                    j('#test_results_lab_test_id').val(response[0].consultation_id);
                },
                error: function (data) {

                }
            });
        });

        j('.add_test_results_link').click(function () {


            //get data
            var lab_test_id = j(this).closest('tr').find('input[name="lab_test_id"]').val();

            j.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>lab/get_lab_test_results/" + lab_test_id,
                dataType: "json",
                success: function (response) {

                    j('#patient_lab_test_idsend_to_doctor_form').val(response[0].lab_test_id);
                    j('#patient_test_name').val(response[0].test_name);
                    j('#test_results_text_area').val(response[0].test_results);
                    j('#test_results_visit_id').val(response[0].visit_id);
                    j('#test_results_lab_test_id').val(response[0].lab_test_id);
                },
                error: function (data) {

                }
            });
        });









        j('.edit_triage_patient_link').click(function () {


            //get data
            var triage_id = j(this).closest('tr').find('input[name="view_triage_id"]').val();
            j.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>nurse/get_triage_details/" + triage_id,
                dataType: "json",
                success: function (response) {
                    j('#edit_weight').val(response[0].weight);
                    j('#edit_systolic').val(response[0].systolic);
                    j('#edit_diastolic').val(response[0].diastolic);
                    j('#edit_temperature').val(response[0].temperature);
                    j('#edit_height').val(response[0].height);
                    j('#edit_respiratory').val(response[0].respiratory_rate);
                    j('#edit_blood_sugar').val(response[0].blood_sugar);
                    j('#edit_pulse_rate').val(response[0].pulse_rate);
                    j('#edit_LMP').val(response[0].lmp);
                    j('#edit_general_complaints').val(response[0].OCS);
                    j('#edit_allergy').val(response[0].allergy);
                    j('#edit_urgency').val(response[0].urgency);
                    j('#edit_triage_id').val(response[0].triage_id);
                },
                error: function (data) {

                }
            });
        });


        j('.edit_patient_link').click(function () {


            //get data
            var patient_id = j(this).closest('tr').find('input[name="view_patient_id"]').val();
            j.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/get_patient_reports_details/" + patient_id,
                dataType: "json",
                success: function (response) {
                    j('#edit_patient_id').val(response[0].patient_id);
                    j('#edit_title').val(response[0].title);
                    j('#edit_phone_no').val(response[0].phone_no);
                    j('#edit_inputFirstName').val(response[0].f_name);
                    j('#edit_inputSurName').val(response[0].s_name);
                    j('#edit_inputOtherName').val(response[0].other_name);
                    j('#edit_dob').val(response[0].dob);
                    j('#edit_gender').val(response[0].gender);
                    j('#edit_maritalstatus').val(response[0].marital_status);
                    j('#edit_nationalid').val(response[0].identification_number);
                    j('#edit_email').val(response[0].email);
                    j('#edit_address').val(response[0].address);
                    j('#edit_residence').val(response[0].residence);
                    j('#edit_employement_status').val(response[0].employment_status);
                    j('#edit_employers_name').val(response[0].employer);
                    j('#edit_kinname').val(response[0].next_of_kin_fname);
                    j('#edit_kinsname').val(response[0].next_of_kin_lname);
                    j('#edit_kinrelation').val(response[0].next_of_kin_relation);
                    j('#edit_kinphone').val(response[0].next_of_kin_phone);
                    j('#edit_family_number').val(response[0].family_base_number);
                },
                error: function (data) {

                }
            });
        });


        j('.view_patient_link').click(function () {


            //get data
            var patient_id = j(this).closest('tr').find('input[name="view_patient_id"]').val();
            j.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/get_patient_reports_details/" + patient_id,
                dataType: "json",
                success: function (response) {
                    j('#view_patient_id').val(response[0].patient_id);
                    j('#view_title').val(response[0].title);
                    j('#view_phone_no').val(response[0].phone_no);
                    j('#view_inputFirstName').val(response[0].f_name);
                    j('#view_inputSurName').val(response[0].s_name);
                    j('#view_inputOtherName').val(response[0].other_name);
                    j('#view_dob').val(response[0].dob);
                    j('#view_gender').val(response[0].gender);
                    j('#view_maritalstatus').val(response[0].marital_status);
                    j('#view_nationalid').val(response[0].identification_number);
                    j('#view_email').val(response[0].email);
                    j('#view_residence').val(response[0].residence);
                    j('#view_employement_status').val(response[0].employment_status);
                    j('#view_employers_name').val(response[0].employer);
                    j('#view_kinname').val(response[0].next_of_kin_fname);
                    j('#view_kinsname').val(response[0].next_of_kin_lname);
                    j('#view_kinrelation').val(response[0].next_of_kin_relation);
                    j('#view_kinphone').val(response[0].next_of_kin_phone);
                    j('#view_family_number').val(response[0].family_base_number);
                },
                error: function (data) {

                }
            });
        });
        j('.delete_patient_link').click(function () {


            //get data
            var patient_id = j(this).closest('tr').find('input[name="view_patient_id"]').val();
            j('.input_patient_id_delete').val(patient_id);
        });
        j('.edit_visit_link').click(function () {


            //get data
            var visit_id = j(this).closest('tr').find('input[name="view_visit_id"]').val();
            j.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/get_visit_reports_details/" + visit_id,
                dataType: "json",
                success: function (response) {
                    var title = response[0].title;
                    var f_name = response[0].f_name;
                    var s_name = response[0].s_name;
                    var other_name = response[0].other_name;
                    var space = ' ';
                    var patient_name = title + space + f_name + space + s_name + space + other_name;
                    j('#edit_visit_id').val(response[0].visit_id);
                    j('#edit_visitation_status').val(response[0].visit_status);
                    j('#edit_phone_no').val(response[0].phone_no);
                    j('#edit_inputPatientName').val(patient_name);
                    j('#edit_inputPatientFamilyNumber').val(response[0].family_number);
                    j('#edit_visit_date').val(response[0].visit_date);
                    j('#edit_nurse_queue').val(response[0].nurse_queue);
                    j('#edit_doctor_queue').val(response[0].doctor_queue);
                    j('#edit_doctor_name').val(response[0].doctor_name);
                    j('#edit_lab_queue').val(response[0].lab_queue);
                    j('#edit_pharm_queue').val(response[0].pharm_queue);
                    j('#edit_urgency').val(response[0].urgency);
                    j('#edit_package_name').val(response[0].package_name);
                    j('#edit_pay_at_the_end').val(response[0].pay_at_the_end);
                },
                error: function (data) {

                }
            });
        });
        j('.view_visit_link').click(function () {


            //get data
            var visit_id = j(this).closest('tr').find('input[name="view_visit_id"]').val();
            j.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/get_visit_reports_details/" + visit_id,
                dataType: "json",
                success: function (response) {
                    var title = response[0].title;
                    var f_name = response[0].f_name;
                    var s_name = response[0].s_name;
                    var other_name = response[0].other_name;
                    var space = ' ';
                    var patient_name = title + space + f_name + space + s_name + space + other_name;
                    j('#view_visit_id').val(response[0].visit_id);
                    j('#view_visitation_status').val(response[0].visit_status);
                    j('#view_phone_no').val(response[0].phone_no);
                    j('#view_inputPatientName').val(patient_name);
                    j('#view_inputPatientFamilyNumber').val(response[0].family_number);
                    j('#view_visit_date').val(response[0].visit_date);
                    j('#view_nurse_queue').val(response[0].nurse_queue);
                    j('#view_doctor_queue').val(response[0].doctor_queue);
                    j('#view_doctor_name').val(response[0].doctor_name);
                    j('#view_lab_queue').val(response[0].lab_queue);
                    j('#view_pharm_queue').val(response[0].pharm_queue);
                    j('#view_urgency').val(response[0].urgency);
                    j('#view_package_name').val(response[0].package_name);
                    j('#view_pay_at_the_end').val(response[0].pay_at_the_end);
                },
                error: function (data) {

                }
            });
        });
        j('.delete_visit_link').click(function () {


            //get data
            var visit_id = j(this).closest('tr').find('input[name="view_visit_id"]').val();
            j('.input_visit_id_delete').val(visit_id);
        });
        j('.view_walkin_patient_link').click(function () {


            //get data
            var walkin_patient_id = j(this).closest('tr').find('input[name="view_walkin_patient_id"]').val();
            j.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/get_walkinpatient_reports_details/" + walkin_patient_id,
                dataType: "json",
                success: function (response) {

                    j('#view_walkin_id').val(response[0].walkin_id);
                    j('#view_inputwalkinPatientName').val(response[0].walkin_patient_name);
                    j('#view_inputPhoneNumber').val(response[0].walkin_phone_no);
                    j('#view_department_name').val(response[0].walkin_department);
                    j('#view_payment_status').val(response[0].paid);
                    j('#view_walkin_visit_date').val(response[0].walkin_date);
                },
                error: function (data) {

                }
            });
        });
        j('.edit_walkin_patient_link').click(function () {


            //get data
            var walkin_patient_id = j(this).closest('tr').find('input[name="view_walkin_patient_id"]').val();
            j.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/get_walkinpatient_reports_details/" + walkin_patient_id,
                dataType: "json",
                success: function (response) {

                    j('#edit_walkin_id').val(response[0].walkin_id);
                    j('#edit_inputwalkinPatientName').val(response[0].walkin_patient_name);
                    j('#edit_inputPhoneNumber').val(response[0].walkin_phone_no);
                    j('#edit_department_name').val(response[0].walkin_department);
                    j('#edit_payment_status').val(response[0].paid);
                    j('#edit_walkin_status').val(response[0].walkin_status);
                    j('#edit_walkin_visit_date').val(response[0].walkin_date);
                },
                error: function (data) {

                }
            });
        });
        j('.delete_walkin_patient_link').click(function () {


            //get data
            var visit_id = j(this).closest('tr').find('input[name="view_walkin_patient_id"]').val();
            j('.input_walkin_patient_id_delete').val(visit_id);
        });
        j('.edit_procedure_link').click(function () {


            //get data
            var procedure_visit_id = j(this).closest('tr').find('input[name="view_procedure_id"]').val();
            j.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/get_procedure_visit_reports_details/" + procedure_visit_id,
                dataType: "json",
                success: function (response) {

                    j('#edit_procedure_visit_id').val(response[0].procedure_visit_id);
                    j('#edit_inputPatientName').val(response[0].patient_name);
                    j('#edit_inputPhoneNumber').val(response[0].patient_phone);
                    j('#edit_patient_type').val(response[0].patient_type);
                    j('#edit_procedure_name').val(response[0].procedure_name);
                    j('#edit_visit_date').val(response[0].date_added);
                    j('#edit_visit_status').val(response[0].status);
                },
                error: function (data) {

                }
            });
        });
        j('.view_procedure_link').click(function () {


            //get data
            var procedure_visit_id = j(this).closest('tr').find('input[name="view_procedure_id"]').val();
            j.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/get_procedure_visit_reports_details/" + procedure_visit_id,
                dataType: "json",
                success: function (response) {

                    j('#view_procedure_visit_id').val(response[0].procedure_visit_id);
                    j('#view_inputPatientName').val(response[0].patient_name);
                    j('#view_inputPhoneNumber').val(response[0].patient_phone);
                    j('#view_procedure_name').val(response[0].procedure_name);
                    j('#view_visit_date').val(response[0].date_added);
                    j('#view_visit_status').val(response[0].status);
                },
                error: function (data) {

                }
            });
        });
        j('.delete_procedure_link').click(function () {


            //get data
            var visit_id = j(this).closest('tr').find('input[name="view_procedure_id"]').val();
            j('.input_procedure_patient_id_delete').val(visit_id);
        });
    });
</script>




</body>
</html>
