<!--Bread-crumb div starts here --> <div>
    <ul class = "breadcrumb">
        <li>
            <a href = "#">Home</a>
        </li>
        <li>
            <a href = "#">Dashboard</a>
        </li>
    </ul>
</div>
<!--Bread-crumb div ends here -->

<div class = " row">
    <div class = "col-md-3 col-sm-3 col-xs-6">
        <a data-toggle = "tooltip" title = "Total Regular Visits Today" class = "well top-block" href = "#">
            <i class = "glyphicon glyphicon-user blue"></i>

            <div>Today Regular Patient Visits </div>
            <div><span id="total_regular_visits" class="total_regular_visits"></span></div>

        </a>
    </div>

    <div class = "col-md-3 col-sm-3 col-xs-6">
        <a data-toggle = "tooltip" title = "Total Walk-in Patient Visits Today" class = "well top-block" href = "#">
            <i class = "glyphicon glyphicon-user green"></i>

            <div>Today Walk-in Patient Visits </div>
            <div><span id="total_walkin_patient_visits"class="total_walkin_patient_visits"></span></div>

        </a>
    </div>








</div>




<div class = "row">
    <div class = "box col-md-12">
        <div class = "box-inner">
            <div class = "box-header well">
                <h2><i class = "glyphicon glyphicon-info-sign"></i> Reception </h2>

                <div class = "box-icon">

                    <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                            class = "glyphicon glyphicon-chevron-up"></i></a>
                   
                </div>
            </div>
            <div class = "box-content row">


                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Regular Patient Payments</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>
                                
                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list" id="regular_patient_payments">

                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->


                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Appointments </h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>
                                
                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list" id="patient_appointments">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->




                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Walk-in Patient Payments</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>
                                
                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list" id="walk_in_patient_payments">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->



                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Active Nurses</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>
                               
                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list" id="active_nurses">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->

                <!--Active Laboratories -->
                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Active Laboratories</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>
                               
                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list" id="active_laboratories">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->

                <!--Active Laboratories end -->

                <!--Active Doctors -->

                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Active Doctors</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>
                                
                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list" id="active_doctor_list">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <!--Active Doctors end -->

                <!--- Active Pharmacists start -->
                <div class = "box col-md-4">
                    <div class = "box-inner">
                        <div class = "box-header well" data-original-title = "">
                            <h2><i class = "glyphicon glyphicon-list"></i> Appointments</h2>

                            <div class = "box-icon">

                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                        class = "glyphicon glyphicon-chevron-up"></i></a>
                              
                            </div>
                        </div>
                        <div class = "box-content">
                            <ul class = "dashboard-list" id="appointment_list">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--/span-->


                <!--Active pharmacists end -->


            </div>
        </div>
    </div>
</div>

<div class = "row">

    <!--/span-->


    <!--/span-->

    <!--/span-->
</div><!--/row-->

<div class = "row">

    <!--/span-->

    <!--/span-->


    <!--/span-->
</div><!--/row-->
<!--content ends -->
</div><!--/#content.col-md-0-->
</div><!--/fluid-row-->




<div class = "visit_records_div" id = "visit_records_div" style = "display: none;">

    <fieldset>
        <form class = "form-inline" id = "record_visit_filter_form" method = "post" action = "/visit_records.html">

        </form>
    </fieldset>
</div>










</div><!--/row-->