<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Patient Prescription History</a>
        </li>
    </ul>
</div>

<button class="btn btn-default" onclick="goBack()"><i class="glyphicon glyphicon-backward" ></i>Go Back </button>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Patient Prescription Records </h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>
                        <tr>
                            <th>Visit Date</th>
                            <th>Commodity Name</th>
                            <th>Strength</th>
                            <th>Route </th>
                            <th>Frequency</th>
                            <th>Duration</th>
                            <th>Employee Name</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($patient_prescription_history as $value) {
                            ?>
                            <tr>
                                <td><?php echo $value['date']; ?></td>
                                <td class="center"><?php echo $value['commodity_name']; ?></td>
                                <td class="center"><?php echo $value['strength']; ?></td>
                                <td class="center"><?php echo $value['route']; ?> </td>
                                <td class="center"><?php echo $value['frequency']; ?></td>
                                <td class="center"> <?php echo $value['duration'] . $value['no_of_days']; ?></td>
                                <td class="center"> <?php echo $value['employee_name']; ?></td>

                            </tr>
                        <?php }
                        ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->
