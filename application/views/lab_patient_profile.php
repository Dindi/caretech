<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?php echo base_url(); ?>">Home</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>">Patient Profile </a>
        </li>
    </ul>
</div>




<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-user"></i> Patient Bio Data </h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-7 col-md-12">

                    <table>
                        <tbody>
                            <?php
                            foreach ($patient_bio as $patient_bio_data) {
                                ?>
                                <tr>`
                                    <td> <h5>Name : <?php echo $patient_bio_data['patient_name']; ?> </h5></td>
                            <input type="hidden" name="hidden_patient_name" class="hidden_patient_name" id="hidden_patient_name" value="<?php echo $patient_bio_data['patient_name']; ?>"/>
                            <td><h6>EMR No : <?php echo $patient_bio_data['family_number']; ?> </h6></td>
                            <td> <h6 class="">Age : <?php
                                    $dob = $patient_bio_data['dob'];
                                    $age = date_diff(date_create($dob), date_create('now'))->y;

                                    if ($age <= 0) {
                                        $bage = date_diff(date_create($dob), date_create('now'))->d;
                                        $nage = $bage . " Days Old";
                                    } elseif ($age > 0) {
                                        $nage = $age . " Years Old";
                                    }
                                    echo $nage;
                                    ?> </h6><td>

                            <td> <h6>Sex : <?php
                                    echo $patient_bio_data['gender'];
                                    ?>
                                </h6> </td>
                            <td> <h6>Residence : <?php
                                    echo $patient_bio_data['residence'];
                                    ?>
                                </h6> </td>
                            <td><span class="label">Allergies : </span><h6 id="patient_allergies_list" class="patient_allergies_list"></h6></td>
                            <td> <a href="#send_to_doctor" class=" lab_send_to_doctor_link btn btn-xs btn-info" id="lab_send_to_doctor_link">
                                    <i class="glyphicon  icon-white"></i>
                                    Send To Doctor : 
                                </a></td>
                            <td> 
                                <form class="send_to_pharmacy_form" id="send_to_pharmacy_form">
                                    <input type="hidden" name="send_to_pharm_visit_id" id="send_to_pharm_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="send_to_pharm_visit_id"/>
                                    <input type="submit" name="send_to_pharmacy_link" value=" Send To Pharmacy : " id="send_to_pharmacy_link" class=" send_to_pharmacy_link btn btn-xs btn-info"/>
                                </form>
                            </td>
                            <td> 
                                <a id="release_patient_link" href="#notification_release_patient" class="release_patient_link btn btn-xs btn-danger">Release Patient</a>

                            </td>
                            </tr>
                            <?php
                        }
                        ?> 

                        </tbody>
                    </table>

                    <div id="notification_release_patient" class="notification_release_patient" style="display: none;">




                        <div class = "box col-md-12">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "">
                                    <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                    <div class = "box-icon">

                                    </div>
                                </div>

                                <div class = "box-content">
                                    <label class = "">
                                        Are you sure you want to release the  Patient ? 
                                    </label><br>
                                    <table>
                                        <tr>
                                            <td>
                                                <form class="release_patient_form" id="release_patient_form">
                                                    <input type="hidden" name="release_patient_visit_id" id="release_patient_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="relaease_patient_visit_id"/>
                                                    <input type="submit" name="release_patient_link" value="Yes " id="yes_release_patient" class=" yes_release_patient btn btn-xs btn-danger"/>
                                                </form>
                                            </td>
                                            <td>

                                                <a id = "relase_patient_no" href = "#relase_patient_no" class = " relase_patient_no btn btn-default btn-xs ">
                                                    No
                                                </a>
                                            </td>
                                        </tr>
                                    </table>




                                </div>
                            </div>
                        </div>
                        <!--/span-->



                    </div>


                </div>


            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-th"></i> Patient Records</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                </div>
            </div>
            <div class="box-content">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"> <a href="#nurse_notes">Nurse Notes : </a></li>
                    <li > <a href="#lab_test_results">Lab Test Results </a></li>

                    <li> <a href="#tests_referrals">Tests/Referrals </a></li>
                    <li><a href="#all_records_tab">All History</a></li>

                    <li ><a href="#triage_tab">Triage</a></li>
                    <li><a href="#lab_tab">Patient Lab Records </a></li>
                    <li><a href="#consultation_tab">Consultation</a></li>
                    <li><a href="#prescription_tab">Prescription</a></li>
                    <!--<li><a href="#appointment_form">Appointment</a></li>-->
                    <li><a href="#others">Others</a></li>
                </ul>

                <div id="myTabContent" class="tab-content">
                    <!--Nurse notes tab start -->
                    <div class="tab-pane active" id="nurse_notes">
                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapsevitalsigns"><i class="glyphicon glyphicon-plus"></i> 
                                        Vital Signs Records : 
                                    </a>
                                </div>

                                <div id="collapsevitalsigns" class="accordion-body collapse">

                                    <?php
                                    foreach ($vital_signs as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo 'triage' . $value['triage_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i> <span> <?php
                                                                $weight = $value['weight'];
                                                                $height = $value['height'];
                                                                $one_hundred = 100;
                                                                $height = $height / $one_hundred;
                                                                $new_height = $height * $height;


                                                                if ($new_height == 0) {
                                                                    ?>
                                                                    <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                                                    <?php
                                                                } else {
                                                                    $BMI = $weight / $new_height;
                                                                    if ($BMI <= 20) {
                                                                        ?>
                                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                    <?php } ?>

                                                                    <?php if ($BMI >= 25) { ?>
                                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                    <?php } ?>

                                                                    <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                                        <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>


                                                            </span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo 'triage' . $value['triage_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">


                                                            <ul class="list-inline">
                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Visit Date :</label>  <?php
                                                                    echo $value['visit_date'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Respiratory Rate :(No/Min)</label>  <?php
                                                                    echo $value['respiratory_rate'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Pulse Rate :(No/Min)</label> <?php
                                                                    echo $value['pulse_rate'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Blood Pressure :(No/Min)</label> <?php
                                                                    echo $value['diastolic'] / $value['systolic'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Temperature :(°C)</label> <?php
                                                                    echo $value['temperature'];
                                                                    ?></li>
                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Height :(Cm)</label> <?php
                                                                    echo $value['height'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Weight :(Kg)</label> <?php
                                                                    echo $value['weight'];
                                                                    ?></li>

                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                        <hr>


                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapsevisitreason"><i class="glyphicon glyphicon-plus"></i> 
                                        Reason for Visit : 
                                    </a>
                                </div>

                                <div id="collapsevisitreason" class="accordion-body collapse">

                                    <?php
                                    foreach ($reason_for_visit as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#">
                                                            <i class="glyphicon glyphicon-hand-right"></i><span> <?php
                                                                echo'   Reason for visit : ' . $value['visit_reason'];
                                                                ?></span>
                                                        </a>
                                                    </div>

                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <hr>

                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapsetobaccosocial"><i class="glyphicon glyphicon-plus"></i> 
                                        Tobacco/Social History : 
                                    </a>
                                </div>

                                <div id="collapsetobaccosocial" class="accordion-body collapse">

                                    <?php
                                    foreach ($allergy_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['allergy_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php echo $value['date_added']; ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['allergy_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Tobacco/Social History  : </label>  <?php
                                                                    echo $value['social_history'];
                                                                    ?></li>







                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                        <hr>


                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseimmunization"><i class="glyphicon glyphicon-plus"></i> 
                                        Immunization History : 
                                    </a>
                                </div>

                                <div id="collapseimmunization" class="accordion-body collapse">

                                    <?php
                                    foreach ($allergy_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['allergy_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php $value['date_added']; ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['allergy_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">



                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default"> Immunization : </label>  <?php
                                                                    echo $value['social_history'];
                                                                    ?></li>




                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>


                    </div>
                    <!-- Nurse notes tab end -->
                    <!-- hematology start -->
                    <div class="tab-pane" id="lab_test_results">
                        <div class = "row">
                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> Lab Test Results</h2>

                                        <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                    class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                    </div>
                                    <div class = "box-content">


                                        <div class="row">

                                            <table class="patient_test_table" id="patient_test_table">
                                                <thead>
                                                    <tr>
                                                        <th>Test Name </th>

                                                        <th>Date Added</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    foreach ($patient_tests as $value) {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $value['test_name']; ?></td>

                                                            <td><?php echo $value['date_added']; ?></td>
                                                            <td>
                                                                <?php
                                                                $test_result = $value['test_results'];
                                                                if (empty($test_result)) {
                                                                    ?>
                                                                    <input id="lab_test_result_visit_id" class="lab_test_result_visit_id hidden "  name="lab_test_result_visit_id"type="hidden" value="<?php echo $value['visit_id']; ?>"/>

                                                                    <input id="lab_test_id" class="lab_test_id hidden " name="lab_test_id" type="hidden" value="<?php echo $value['lab_test_id']; ?>"/>
                                                                    <a class=" btn btn-xs btn-info add_test_results_link" id="add_test_results_link" href="#add_test_results_form" >Add Test Results</a></td>

                                                                <?php
                                                            } else {
                                                                ?>

                                                        <span class="alert alert-info">Patient results posted. </span>
                                                        <br>
                                                        <input id="lab_test_result_visit_id" class="lab_test_result_visit_id hidden "  name="lab_test_result_visit_id"type="hidden" value="<?php echo $value['visit_id']; ?>"/>

                                                        <input id="lab_test_id" class="lab_test_id hidden " name="lab_test_id" type="hidden" value="<?php echo $value['lab_test_id']; ?>"/>
                                                        <a class=" btn btn-xs btn-info add_test_results_link" id="add_test_results_link" href="#add_test_results_form" >Edit Test Results</a></td>

                                                        <?php
                                                    }
                                                    ?>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>


                                                </tbody>
                                            </table>










                                        </div><!--/row-->


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Chief complaints  end -->
                    <!-- Allergies start -->
                    <div class="tab-pane" id="allergies">



                    </div>
                    <!-- Allergies end -->



                    <!-- Tests/Referrals start -->
                    <div class="tab-pane" id="tests_referrals">
                        <div class="">
                            <h6>Please select the  type of test you would like to perform : </h6>
                            <hr>
                            <a href="#test_refferal" class="btn btn-xs test_referral_link" id="test_referral_link"> Order Test</a>
                            <hr>
                            <a href="#imaging_referral" class="btn btn-xs imaging_referral_link" id="imaging_referral_link">Request for Imaging</a> |
                            <a href="#other_referral" class="btn btn-xs other_referral_link" id="other_referral_link">Refer Patient </a> |
                            <a href="#sick_off" class="btn btn-xs sick_off_link" id="sick_off_link">Sick Off </a>

                        </div>

                        <hr>



                    </div>
                    <!-- Tests/Referrals end -->



                    <!--Prescription tab start --> 
                    <div class="tab-pane" id="prescription_tab">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                            <thead>
                                <tr>
                                    <th>Visit Date</th>
                                    <th>Commodity Name</th>
                                    <th>Strength</th>
                                    <th>Route </th>
                                    <th>Frequency</th>
                                    <th>Duration</th>
                                    <th>Employee Name</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($patient_prescription_records as $value) {
                                    ?>
                                    <tr>
                                        <td><?php echo $value['date']; ?></td>
                                        <td class="center"><?php echo $value['commodity_name']; ?></td>
                                        <td class="center"><?php echo $value['strength']; ?></td>
                                        <td class="center"><?php echo $value['route']; ?> </td>
                                        <td class="center"><?php echo $value['frequency']; ?></td>
                                        <td class="center"> <?php echo $value['duration'] . $value['no_of_days']; ?></td>
                                        <td class="center"> <?php echo $value['employee_name']; ?></td>

                                    </tr>
                                <?php }
                                ?>


                            </tbody>
                        </table>
                    </div>
                    <!-- Prescription tab end -->




                    <!-- Patient instructions tab start -->
                    <div class="tab-pane" id="patient_instruction_tab">

                    </div>
                    <!-- Patient instructions tab end -->



                    <!--All records tab start -->

                    <div class="tab-pane" id="all_records_tab">



                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseTriage"><i class="glyphicon glyphicon-plus"></i> 
                                        Triage Records : 
                                    </a>
                                </div>

                                <div id="collapseTriage" class="accordion-body collapse">

                                    <?php
                                    foreach ($patient_triage_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo 'triage' . $value['triage_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i> <span> <?php
                                                                $weight = $value['weight'];
                                                                $height = $value['height'];
                                                                $one_hundred = 100;
                                                                $height = $height / $one_hundred;
                                                                $new_height = $height * $height;


                                                                if ($new_height == 0) {
                                                                    ?>
                                                                    <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                                                    <?php
                                                                } else {
                                                                    $BMI = $weight / $new_height;
                                                                    if ($BMI <= 20) {
                                                                        ?>
                                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                    <?php } ?>

                                                                    <?php if ($BMI >= 25) { ?>
                                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                    <?php } ?>

                                                                    <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                                        <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>


                                                            </span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo 'triage' . $value['triage_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">


                                                            <ul class="list-inline">
                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Visit Date :</label>  <?php
                                                                    echo $value['visit_date'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Respiratory Rate :(No/Min)</label>  <?php
                                                                    echo $value['respiratory_rate'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Pulse Rate :(No/Min)</label> <?php
                                                                    echo $value['pulse_rate'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Blood Pressure :(No/Min)</label> <?php
                                                                    echo $value['diastolic'] . "/" . $value['systolic'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Temperature :(°C)</label> <?php
                                                                    echo $value['temperature'];
                                                                    ?></li>
                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Height :(Cm)</label> <?php
                                                                    echo $value['height'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Weight :(Kg)</label> <?php
                                                                    echo $value['weight'];
                                                                    ?></li>

                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                        <hr>


                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseLab"><i class="glyphicon glyphicon-plus"></i> 
                                        Lab Records : 
                                    </a>
                                </div>

                                <div id="collapseLab" class="accordion-body collapse">

                                    <?php
                                    foreach ($patient_lab_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['lab_test_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php
                                                                echo $value['date_added'] . '   Test Name : ' . $value['test_name'];
                                                                ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['lab_test_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Test Name</label>  <?php
                                                                    echo $value['test_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Test Results : </label> <?php
                                                                    echo $value['test_results'];
                                                                    ?></li>


                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <hr>

                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseConsultation"><i class="glyphicon glyphicon-plus"></i> 
                                        Consultation Records : 
                                    </a>
                                </div>

                                <div id="collapseConsultation" class="accordion-body collapse">

                                    <?php
                                    foreach ($patient_consultation_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['consultation_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php $value['date']; ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['consultation_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Chief Complaint  : </label>  <?php
                                                                    echo $value['complaints'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">  Diagnosis :</label> <?php
                                                                    echo $value['working_diagnosis'];
                                                                    ?></li>




                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                        <hr>


                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapsePrescription"><i class="glyphicon glyphicon-plus"></i> 
                                        Prescription Records : 
                                    </a>
                                </div>

                                <div id="collapsePrescription" class="accordion-body collapse">

                                    <?php
                                    foreach ($patient_prescription_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['prescription_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php $value['date']; ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['prescription_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Commodity name : </label>  <?php
                                                                    echo $value['commodity_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Strength :</label> <?php
                                                                    echo $value['strength'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Route : </label> <?php
                                                                    echo $value['route'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Frequency : </label> <?php
                                                                    echo $value['frequency'];
                                                                    ?></li>
                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Duration : </label> <?php
                                                                    echo $value['duration'];
                                                                    ?></li>



                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>



                    </div>

                    <!--All records tab end -->



                    <!--Book Appointment Tab -->
                    <!--                    <div class="tab-pane" id="appointment_form">
                    
                                            <div class = "row">
                                                <div class = "box col-md-12">
                                                    <div class = "box-inner">
                                                        <div class = "box-header well" data-original-title = "">
                                                            <h2><i class = "glyphicon glyphicon-edit"></i> Book Appointment</h2>
                    
                                                            <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                                        class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                                        </div>
                                                        <div class = "box-content">
                    
                                                            <div class = "bs-example">
                    
                    
                                                                <form class = "appointment_form_book" autocomplete="off" method = "post"  id = "appointment_form_book" role = "form">
                    
                                                                    <input type="hidden" class="form-control appointment_patient_id" id="appointment_patient_id" value="<?php echo $this->uri->segment(3); ?>" name="appointment_patient_id" />
                                                                    <input type="hidden" class="form-control appointment_visit_id" id="appointment_visit_id" value="<?php echo $this->uri->segment(4); ?>" name="appointment_visit_id"/>
                    
                                                                    <hr>
                                                                    <div class = "form-inline">
                                                                        <div class = "form-group">
                                                                            <label class = "sr-only" for = "appointment_type">Appointment Type : </label>
                                                                            <input type="text" class="form-control appointment_type_1" id="appointment_type_1"placeholder="Appointment Type : " name="appointment_type"/>
                                                                            <input type = "text"  class = "form-control appointment_type"  id = "appointment_type" name="appointment_type" placeholder = "Appointment Type :">
                                                                        </div>
                                                                    </div>
                                                                    <div class = "form-inline">
                                                                        <div class = "form-group">
                                                                            <label class = "sr-only" for = "person_to_see">Person to See : </label>
                                                                            <input type = "text" class = "form-control person_to_see" id = "person_to_see" name="person_to_see" placeholder = "Person to See : ">
                                                                        </div>
                                                                    </div>
                                                                    <div class = "form-inline">
                    
                    
                    
                                                                        <div class="form-group">
                                                                            <div class='input-group date' id='datetimepicker1'>
                                                                                <input type='text' placeholder="Date From : " required="" name="date_from" id="date_from" class="form-control" />
                                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                    
                                                                        <div class="form-group">
                                                                            <div class='input-group date' id='datetimepicker2'>
                                                                                <input type='text' placeholder="Date To : " required="" name="date_to" id="date_to" class="form-control" />
                                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                    
                    
                                                                    </div>
                                                                    <div class = "form-inline">
                                                                        <div class = "form-group">
                                                                            <label class = "sr-only" for = "reason_for_appointment">Reason for Appointment </label>
                                                                            <textarea class=" form-control" rows="5" cols="100" placeholder="Reason for Appointment: " name="reason_for_appointment" id="reason_for_appointment"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                    
                    
                    
                                                                    <div class = "form-inline">
                                                                        <div class = "form-group">
                                                                            <input type = "submit" value = "Save" class = "btn btn-success btn-xs book_appointment_form_button" id = "book_appointment_form_button"/>
                                                                            <input type = "reset" value = "Reset" class = "btn btn-danger btn-xs" id = "save_reset"/>
                                                                            <hr>
                                                                            <a href="#appointment_history" class="btn btn-info btn-xs" id="patient_appointments">View Patient Appointments </a>
                    
                                                                        </div>
                                                                    </div>
                    
                                                                </form>
                    
                    
                    
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                /span
                    
                                            </div>/row
                                        </div>-->
                    <!--Book Appointment Tab end -->
                    <!---Triage Tab start --->

                    <div class="tab-pane" id="triage_tab">

                        <a  id="export_triage_patient_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Walkin Patient Report" class="export_triage_patient_report_filter_link" href="#export_triage_report_filter_form">
                            <i class="glyphicon glyphicon-download-alt"></i>
                        </a>
                        |



                        <table class="table table-striped table-bordered triage_report  responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Date </th>
                                    <th>Nurse Name</th>
                                    <th>Respiratory (No/Min)</th>
                                    <th>Pulse Rate (No/Min)</th>
                                    <th>Blood pressure (mmHg)</th>
                                    <th>Temperature (°C)</th>
                                    <th>Height (Cm)</th>
                                    <th>Weight (KGs)</th>
                                    <th>BMI</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php
                                    $i = 1;
                                    foreach ($patient_triage_records as $value) {
                                        ?>
                                        <td class="center"><?php echo $i; ?></td>
                                        <td class="center">
                                            <?php
                                            echo $value['visit_date'];
                                            ?>
                                        </td>
                                        <td class="center">
                                            <?php
                                            echo $value['employee_name'];
                                            ?>
                                        </td>
                                        <td class="center">
                                            <?php
                                            echo $value['respiratory_rate'];
                                            ?>
                                        </td>
                                        <td class="center">
                                            <?php echo $value['pulse_rate']; ?>
                                        </td>
                                        <td class="center">
                                            <?php echo $value['diastolic'] . "/" . $value['systolic']; ?>
                                        </td>
                                        <td class="center">
                                            <?php echo $value['temperature']; ?>
                                        </td>
                                        <td class="center">
                                            <?php echo $value['height']; ?>
                                        </td>
                                        <td class="center">
                                            <?php echo $value['weight']; ?>
                                        </td>

                                        <td class="center" ><?php
                                            $weight = $value['weight'];
                                            $height = $value['height'];
                                            $one_hundred = 100;
                                            $height = $height / $one_hundred;
                                            $new_height = $height * $height;


                                            if ($new_height == 0) {
                                                ?>
                                                <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                                <?php
                                            } else {
                                                $BMI = $weight / $new_height;
                                                if ($BMI <= 20) {
                                                    ?>
                                                    <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                <?php } ?>

                                                <?php if ($BMI >= 25) { ?>
                                                    <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                <?php } ?>

                                                <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                    <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                    <?php
                                                }
                                            }
                                            ?>

                                        </td> 



                                        <td class="center">
                                            <input type="hidden" name="view_triage_patient_id" class="view_triage_patient_id" id="view_triage_patient_id" value="<?php echo $value['patient_id']; ?>"/>
                                            <input type="hidden" name="view_triage_id" class="view_triage_id" id="view_triage_id" value="<?php echo $value['triage_id']; ?>"/>
                                            <input type="hidden" name="view_triage_visit_id" class="view_triage_visit_id" id="view_triage_visit_id" value="<?php echo $value['visit_id']; ?>"/>
                                            <a  id="edit_triage_patient_link" class="edit_triage_patient_link" href="#edit_triage_form">
                                                <i class="glyphicon glyphicon-edit icon-white"></i>

                                            </a>|
                                            <a  id="delete_triage_patient_link" class="delete_triage_patient_link" href="#delete_triage_form">
                                                <i class="glyphicon glyphicon-trash icon-white"></i>

                                            </a>
                                        </td>
                                    </tr>

                                    <?php
                                    $i++;
                                }
                                ?>




                            </tbody>
                        </table>


                    </div>

                    <!-- Triage tab end -->
                    <!-- Laboratory Tab Start -->

                    <div class="tab-pane" id="lab_tab">

                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                            <thead>
                                <tr>
                                    <th>Test Date</th>
                                    <th>Doctor Name</th>
                                    <th>Test Name</th>
                                    <th>Test Results </th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($patient_lab_records as $value) {
                                    ?>
                                    <tr>
                                        <td class="center"><?php echo $value['date_added']; ?></td>
                                        <td class="center"><?php echo $value['employee_name']; ?></td>
                                        <td class="center"><?php echo $value['test_name']; ?></td>
                                        <td class="center">
                                            <?php echo $value['test_results']; ?>
                                        </td>

                                    </tr>
                                <?php }
                                ?>


                            </tbody>
                        </table>

                    </div>

                    <!-- Laboratory tab end -->



                    <!-- Consultation tab start -->
                    <div class="tab-pane" id="consultation_tab">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                            <thead>
                                <tr>
                                    <th>Visit Date</th>
                                    <th>Chief Complaints</th>
                                    <th>Diagnosis</th>
                                    <th>Employee Name</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($patient_consultation_records as $value) {
                                    ?>
                                    <tr>
                                        <td><?php echo $value['date']; ?></td>
                                        <td class="center"><?php echo $value['complaints']; ?></td>
                                        <td class="center"> <?php echo $value['working_diagnosis']; ?></td>
                                        <td class="center"<?php echo $value['employee_name']; ?>></td>

                                    </tr>
                                    <?php
                                }
                                ?>


                            </tbody>
                        </table>
                    </div>

                    <!-- Consultation tab end -->





                    <!-- Prescription tab start -->
                    <div class="tab-pane" id="prescription_tab">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                            <thead>
                                <tr>
                                    <th>Visit Date</th>
                                    <th>Commodity Name</th>
                                    <th>Strength</th>
                                    <th>Route </th>
                                    <th>Frequency</th>
                                    <th>Duration</th>
                                    <th>Employee Name</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($patient_prescription_records as $value) {
                                    ?>
                                    <tr>
                                        <td><?php echo $value['date']; ?></td>
                                        <td class="center"><?php echo $value['commodity_name']; ?></td>
                                        <td class="center"><?php echo $value['strength']; ?></td>
                                        <td class="center"><?php echo $value['route']; ?> </td>
                                        <td class="center"><?php echo $value['frequency']; ?></td>
                                        <td class="center"> <?php echo $value['duration'] . $value['no_of_days']; ?></td>
                                        <td class="center"> <?php echo $value['employee_name']; ?></td>

                                    </tr>
                                <?php }
                                ?>


                            </tbody>
                        </table>
                    </div>

                    <!-- Prescription tab end -->

                    <!--Others tab start -->

                    <div class="tab-pane" id="prescription_tab">

                        <div>
                            <a href="#book_referral" id="book_referral_link" class="btn btn-xs btn-info book_referral_link">Refer Patient </a>

                            <hr>
                            <a href="#sick_off_sheet" id="sick_off_sheet_link" class="btn btn-xs btn-info sick_off_sheet_link">Sick Off Sheet </a>
                        </div>
                    </div>
                    <!--Others tab end -->
                </div>
            </div>
        </div>
    </div>
    <!--/span-->









</div><!--/row-->



<div id="icd_code_form" class="icd_code_form" style="display: none;">



    <div class="control-group">
        <label class="control-label" for="selectError1">Multiple Select / Tags</label>

        <div class="controls">
            <select id="selectError1" multiple class="form-control" data-rel="chosen" placeholder="Please select atleast one ..."   >

                <?php foreach ($cache_data as $value) {
                    ?>
                    <option value="<?php echo $value['id']; ?>">
                        <?php echo $value['icd_description']; ?>
                    </option>
                <?php }
                ?>


            </select>
        </div>
    </div>

</div>









































<div class="all" style="display: none;">



    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2>Patient Bio Data</h2>
                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <div class="well">
                    <?php
                    foreach ($patient_bio as $patient_bio_data) {
                        
                    }
                    ?>
                    <h5>Patient Name : <?php echo $patient_bio_data['patient_name']; ?> </h5>
                    <input type="hidden" name="hidden_patient_name" class="hidden_patient_name" id="hidden_patient_name" value="<?php echo $patient_bio_data['patient_name']; ?>"/>
                    <hr>
                    <h6 class="">Age : <?php
                        $dob = $patient_bio_data['dob'];
                        $age = date_diff(date_create($dob), date_create('now'))->y;

                        if ($age <= 0) {
                            $bage = date_diff(date_create($dob), date_create('now'))->d;
                            $nage = $bage . " Days Old";
                        } elseif ($age > 0) {
                            $nage = $age . " Years Old";
                        }
                        echo $nage;
                        ?> </h6>
                    <hr>
                    <h6>Residence : <?php echo $patient_bio_data['residence']; ?> </h6>

                    <a class="btn btn-info btn-xs" href="#send_to_doctor">Send to Doctor</a>
                </div>
            </div>
        </div>
    </div>

    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2>Patient Allergies </h2>
                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <div class="well">
                    <p id="patient_allergies_list" class="patient_allergies_list"></p>
                </div>
            </div>
        </div>
    </div>


    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2>Patient </h2>
                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <div class="well">
                    <a href="#nurse_patient_list_div" id="nurse_view_patient_list" class="nurse_view_patient_list btn btn-xs btn-success "><i class="glyphicon glyphicon-zoom-in"></i>View Patients List</a>


                    <hr>
                    <a class="btn btn-info btn-xs patient_medical_records_link" href="#patient_medical_records" id="patient_medical_records_link"><i class="glyphicon glyphicon-align-justify"> Medical Records :</i></a>

                </div>
            </div>
        </div>
    </div>



    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2>Patient </h2>
                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <div class="well">
                    <a href="#book_appointment_form" class=" book_appointment_form_link btn btn-xs btn-info" id="book_appointment_form_link">
                        <i class="glyphicon  icon-white"></i>
                        Book an Appointment : 
                    </a>
                    <hr>
                    <a href="#send_to_doctor" class=" lab_send_to_doctor_link btn btn-xs btn-info" id="lab_send_to_doctor_link">
                        <i class="glyphicon  icon-white"></i>
                        Send To Doctor : 
                    </a>
                    <hr>
                    <form class="send_to_pharmacy_form" id="send_to_pharmacy_form">
                        <input type="hidden" name="send_to_pharm_visit_id" id="send_to_pharm_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="send_to_pharm_visit_id"/>
                        <input type="submit" name="send_to_pharmacy_link" value=" Send To Pharmacy : " id="send_to_pharmacy_link" class=" send_to_pharmacy_link btn btn-xs btn-info"/>
                    </form>

                    <hr>

                </div>
            </div>
        </div>
    </div>


    <div class="box-content patient_medical_records" id="patient_medical_records" style="display: none">
        <div class="well">
            <a href="<?php echo base_url() ?>reports/patient_lab_history/<?php echo $this->uri->segment(3); ?>" id="" class=" btn btn-info "><i class="glyphicon icon-white glyphicon-align-justify"></i> Lab Records</a>
            <hr>
            <a href="<?php echo base_url() ?>reports/patient_consultation_history/<?php echo $this->uri->segment(3); ?>" class="  btn btn-info" id="send_to_doctor_link">
                <i class="glyphicon glyphicon-align-justify icon-white"></i>
                Consultation Records 
            </a>
            <hr>
            <a href="<?php echo base_url() ?>reports/patient_prescription_history/<?php echo $this->uri->segment(3); ?>" class="btn btn-info"><i class="glyphicon icon-white glyphicon-align-justify"> Prescription Records </i></a>

        </div>
    </div>



    <!--/span-->
    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-edit"></i> Patient Triage Report</h2>

                    <div class="box-icon">

                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                class="glyphicon glyphicon-chevron-up"></i></a>

                    </div>
                </div>
                <div class="box-content">

                    <!--/span-->

                    <a  id="export_triage_patient_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Walkin Patient Report" class="export_triage_patient_report_filter_link" href="#export_triage_report_filter_form">
                        <i class="glyphicon glyphicon-download-alt"></i>
                    </a>
                    |
                    <a  id="add_triage_link" data-toggle="tooltip" data-placement="top" data-original-title="Add Triage " class="add_triage_link" href="#add_triage_form">
                        Add Triage <i class="glyphicon glyphicon-plus"></i>
                    </a>


                    <table class="table table-striped table-bordered triage_report_1  responsive">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Date </th>
                                <th>Nurse Name</th>
                                <th>Respiratory (No/Min)</th>
                                <th>Pulse Rate (No/Min)</th>
                                <th>Blood pressure (mmHg)</th>
                                <th>Temperature (°C)</th>
                                <th>Height (Cm)</th>
                                <th>Weight (KGs)</th>
                                <th>BMI</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                $i = 1;
                                foreach ($patient_triage_report as $value) {
                                    ?>
                                    <td class="center"><?php echo $i; ?></td>
                                    <td class="center">
                                        <?php
                                        echo $value['visit_date'];
                                        ?>
                                    </td>
                                    <td class="center">
                                        <?php
                                        echo $value['employee_name'];
                                        ?>
                                    </td>
                                    <td class="center">
                                        <?php
                                        echo $value['respiratory_rate'];
                                        ?>
                                    </td>
                                    <td class="center">
                                        <?php echo $value['pulse_rate']; ?>
                                    </td>
                                    <td class="center">
                                        <?php echo $value['diastolic'] . "/" . $value['systolic']; ?>
                                    </td>
                                    <td class="center">
                                        <?php echo $value['temperature']; ?>
                                    </td>
                                    <td class="center">
                                        <?php echo $value['height']; ?>
                                    </td>
                                    <td class="center">
                                        <?php echo $value['weight']; ?>
                                    </td>

                                    <td class="center" ><?php
                                        $weight = $value['weight'];
                                        $height = $value['height'];
                                        $one_hundred = 100;
                                        $height = $height / $one_hundred;
                                        $new_height = $height * $height;


                                        if ($new_height == 0) {
                                            ?>
                                            <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                            <?php
                                        } else {
                                            $BMI = $weight / $new_height;
                                            if ($BMI <= 20) {
                                                ?>
                                                <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                            <?php } ?>

                                            <?php if ($BMI >= 25) { ?>
                                                <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                            <?php } ?>

                                            <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                <?php
                                            }
                                        }
                                        ?>

                                    </td> 



                                    <td class="center">
                                        <input type="hidden" name="view_triage_patient_id" class="view_triage_patient_id" id="view_triage_patient_id" value="<?php echo $value['patient_id']; ?>"/>
                                        <input type="hidden" name="view_triage_id" class="view_triage_id" id="view_triage_id" value="<?php echo $value['triage_id']; ?>"/>
                                        <input type="hidden" name="view_triage_visit_id" class="view_triage_visit_id" id="view_triage_visit_id" value="<?php echo $value['visit_id']; ?>"/>
                                        <a  id="edit_triage_patient_link" class="edit_triage_patient_link" href="#edit_triage_form">
                                            <i class="glyphicon glyphicon-edit icon-white"></i>

                                        </a>|
                                        <a  id="delete_triage_patient_link" class="delete_triage_patient_link" href="#delete_triage_form">
                                            <i class="glyphicon glyphicon-trash icon-white"></i>

                                        </a>
                                    </td>
                                </tr>

                                <?php
                                $i++;
                            }
                            ?>




                        </tbody>
                    </table>



                </div>
            </div>
        </div>
        <!--/span-->



    </div>







    <div class="send_to_doctor" id="send_to_doctor" style="display: none;">



        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class="box-header well">


                        <div class="box-icon">

                        </div>
                    </div>
                    <div class = "box-content">

                        <div class = "bs-example">

                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <form class = "send_to_current_doctor_form " autocomplete="off"   id = "send_to_current_doctor_form" role = "form">
                                                <input type="text" name="current_doctor" id="current_doctor" class="current_doctor hidden" value="current_doctor"/>
                                                <input type="text" id="send_to_doctor_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="send_to_doctor_visit_id" name="send_to_doctor_visit_id"/>
                                                <input type="text" id="send_to_doctor_patient_id" value="<?php echo $this->uri->segment(3); ?>" class="send_to_doctor_patient_id" name="send_to_doctor_patient_id"/>
                                                <div class = "form-inline">

                                                    <div class = "form-group">

                                                        <input type="submit" class="btn btn-small btn-success btn-xs send_to_current_doctor_button" id="send_to_current_doctor_button" value="Send To Previous Doctor"/>

                                                    </div>
                                                </div>
                                            </form> 
                                        </td>
                                        <td>
                                            <form class = "send_to_doctor_form " autocomplete="off"   id = "send_to_doctor_form" role = "form">

                                                <input type="hidden" id="send_to_doctor_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="send_to_doctor_visit_id" name="send_to_doctor_visit_id"/>
                                                <input type="hidden" id="send_to_doctor_patient_id" value="<?php echo $this->uri->segment(3); ?>" class="send_to_doctor_patient_id" name="send_to_doctor_patient_id"/>
                                                <div class = "form-inline">
                                                    <div class = "form-group">
                                                        <span class="label label-default">Patient Name : </span>
                                                        <input type="text" readonly="" id="send_to_doctor_patient_name" class="send_to_doctor_patient_name uneditable-input form-control "/>
                                                    </div>
                                                    <hr>
                                                    <div class="form-group">
                                                        <label class="label label-default">Active Doctors :</label>
                                                        <select name="send_to_doctor_name" required="" class="send_to_doctor_name form-control" id="send_to_doctor_name">
                                                            <option>Please select Doctor's Name :</option>
                                                        </select>
                                                    </div>

                                                    <hr>
                                                    <div class = "form-group">

                                                        <input type="submit" class="btn btn-small btn-success btn-xs send_to_doctor_button" id="send_to_doctor_button" value="Send To Another Doctor"/>

                                                    </div>
                                                </div>
                                            </form>  
                                        </td>
                                    </tr>
                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>







    <div class="delete_triage_form" id="delete_triage_form" style="display: none;">



        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class="box-header well">


                        <div class="box-icon">

                        </div>
                    </div>
                    <div class = "box-content">

                        <div class = "bs-example">


                            <form class = "delete_triage_information_form " autocomplete="off"   id = "delete_tri_information_form" role = "form">


                                <input type="hidden" id="input_triage_id_delete" name="input_triage_id_delete" class="input_triage_id_delete"/>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <h5 class = "" for = "">Are you sure you want to Delete the Triage details? </h5>

                                    </div>

                                    <br>
                                    <div class = "form-group">

                                        <input type="submit" class="btn btn-small btn-success delete_triage_record_yes" value="Yes"/>
                                        <input type="reset" class="btn btn-close delete_triage_record_no" value="No"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>





    <!--/span-->




    <div class="nurse_patient_list_div" id="nurse_patient_list_div" style="display: none;">


        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class = "box-header well">
                        <h2><i class = "glyphicon glyphicon-info-sign"></i> Nurse </h2>

                        <div class = "box-icon">

                            <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                    class = "glyphicon glyphicon-chevron-up"></i></a>

                        </div>
                    </div>
                    <div class = "box-content row">


                        <div class = "box col-md-4">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "">
                                    <h2><i class = "glyphicon glyphicon-list"></i> Regular Patients In Tray</h2>

                                    <div class = "box-icon">

                                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                class = "glyphicon glyphicon-chevron-up"></i></a>

                                    </div>
                                </div>
                                <div class = "box-content">
                                    <ul class = "dashboard-list regular_patients_in_tray" id="regular_patients_in_tray">

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--/span-->





                        <div class = "box col-md-4">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "">
                                    <h2><i class = "glyphicon glyphicon-list"></i> Walk-in Patients In Tray</h2>

                                    <div class = "box-icon">

                                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                class = "glyphicon glyphicon-chevron-up"></i></a>

                                    </div>
                                </div>
                                <div class = "box-content">
                                    <ul class = "dashboard-list walkin_patients_in_tray" id="walkin_patients_in_tray">


                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--/span-->


                        <!--Appointments Start-->
                        <div class = "box col-md-4">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "">
                                    <h2><i class = "glyphicon glyphicon-list"></i> Appointments Today</h2>

                                    <div class = "box-icon">

                                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                class = "glyphicon glyphicon-chevron-up"></i></a>

                                    </div>
                                </div>
                                <div class = "box-content">
                                    <ul class = "dashboard-list appointments_today" id="appointments_today">


                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--Appointments End-->



                    </div>
                </div>
            </div>
        </div>

    </div>

</div>




<div id="add_test_results_form" class="add_test_results_form" style="display: none;">

    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-th"></i> Post Lab Test Results</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-setting btn-round btn-default"><i
                                class="glyphicon glyphicon-cog"></i></a>
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="#" class="btn btn-close btn-round btn-default"><i
                                class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <div class="row">

                        <form id="patient_test_results_form" class="patient_test_results_form" method="post">
                            <div align="center">
                                <input type="hidden" class="form-control" name="lab_patient_id" id="lab_patient_id" value="<?php echo $this->uri->segment(3); ?>" class="lab_patient_id"/>
                                <input type="hidden" class="form-control" name="lab_patient_visit_id" id="lab_patient_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="lab_patient_visit_id"/>
                                <input type="hidden" class="form-control" name="lab_test_id" id="patient_lab_test_id" class="patient_lab_test_id"/>
                                <label>Patient Name : </label>
                                <input type="text" id="name_of_patient"  readonly=""  class="name_of_patient form-control input" placeholder="Patient's Name" />
                            </div>
                            <hr>
                            <div align="center">
                                <label>Test Name : </label>
                                <input type="text" id="patient_test_name" name="patient_test_name" readonly="" class="patient_test_name form-control input" placeholder="Test's Name" />
                            </div>
                            <hr>
                            <div align="center">
                                <label>Results : </label>
                                <textarea class="textarea form-control test_results_text_area" id="test_results_text_area" name="tests_results" ></textarea>
                            </div>
                            <hr>

                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!--/span-->


        <!--/span-->
    </div><!--/row-->
</div>

