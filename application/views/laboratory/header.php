<!DOCTYPE html>
<html lang="en">
    <head>
        <!--
          
        -->
        <meta charset="utf-8">
        <title>Care-tech System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
        <meta name="author" content="Muhammad Usman">

        <!-- The styles -->
        <link id="bs-css" href="<?php echo base_url(); ?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/charisma-app.css" rel="stylesheet">
        <link href='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
        <link href='<?php echo base_url(); ?>assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/jquery.noty.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/noty_theme_default.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/elfinder.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/elfinder.theme.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/uploadify.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/animate.min.css' rel='stylesheet'>

        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/jquery.min.js"></script>

        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->


        <!--
        Scripting starts here ...-->

        <script type="text/javascript">
            $(document).ready(function () {

                $('.visit_type').on('change', function () {

                    $visit_type = (this.value);
                    if ($visit_type === "follow_up") {
                        // code to be executed if condition is true

                        $('#previous_visit_div').css('display', 'inline-block');
                        $('#queue_to_join_div').css('display', 'inline-block');

                        $('.queue_to_join').on('change', function () {
                            var doctor = this.value;
                            if (doctor === 'Doctor') {
                                $('#active_doctor_list_div').css('display', 'inline-block');
                            }
                        });
                    }
                    else {
                        // code to be executed if condition is false
                        $('#previous_visit_div').css('display', 'none');
                        $('#queue_to_join_div').css('display', 'none');
                        $('.queue_to_join').val("");
                        $('.previous_visit_div').val("");
                    }

                });
            })
            $(document).ready(function () {
                $('#register_patient').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#not_joining').fancybox({
                    paddding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });


                $('#shwari_relation_yes').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#shwari_relation_no').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#visit_records').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });



                $('#patient_procedure_option').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#regular_patient_yes').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#regular_patient_no').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#patient_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#walkin_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#visit_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#procedure_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

            });


            $('.insurance_exists').on('change', function () {
                alert(this.value);
            });
            $("#save_submit").click(function () {

            });
            $("#save_submit_1").click(function () {

            });

            //delegated submit handlers for the forms inside the table
            $('#save_submit_1').on('click', function (e) {
                e.preventDefault();




                //read the form data ans submit it to someurl
                $.post('visit.html', $('#patient_registration_form_1').serialize(), function () {

                    generateAll();
                    setInterval(function () {
                        var url = "<?php echo base_url() ?>index.php/home";
                        $(location).attr('href', url);
                    }, 30000);
                }).fail(function () {
                    //error do something
                    //alert("Failed please try again later or contact the system administrator");
                    // $.notify("There was an error please try again later or  contact the system support desk  for assistance  ", "error",{ position:"left" });
                    $(".save_timesheet_notify").notify(
                            "There was an error please try again later or  contact the system support desk  for assistance",
                            "error",
                            {position: "left"}
                    );
                });
            });



            function generate(layout) {
                var n = noty({
                    text: 'Do you want to continue?',
                    type: 'alert',
                    dismissQueue: true,
                    layout: layout,
                    theme: 'defaultTheme',
                    buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                                $noty.close();
                                noty({dismissQueue: true, force: true, layout: layout, theme: 'defaultTheme', text: 'You clicked "Ok" button', type: 'success'});
                            }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                                $noty.close();
                                noty({dismissQueue: true, force: true, layout: layout, theme: 'defaultTheme', text: 'You clicked "Cancel" button', type: 'error'});
                            }
                        }
                    ]
                });
                console.log('html: ' + n.options.id);
            }

            function generateAll() {
                generate('center');
            }



        </script>

        <!--
        Scripting ends here
        -->

        <!-- The fav icon -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.ico">

    </head>

    <body>
        <?php
        if (!isset($no_visible_elements) || !$no_visible_elements) {
            if ($this->session->userdata('id')) {
                ?>
                <!-- topbar starts -->
                <!--                <div class="navbar navbar-default" role="navigation">
                
                                    <div class="navbar-inner">
                                        <button type="button" class="navbar-toggle pull-left animated flip">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <a class="navbar-brand" href="index.html"> <img alt="Charisma Logo" src="<?php echo base_url(); ?>assets/img/logo20.png" class="hidden-xs"/>
                                            <span>Charisma</span></a>
                
                                         user dropdown starts 
                                        <div class="btn-group pull-right">
                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Profile</a></li>
                                                <li class="divider"></li>
                                                <li><a href="login.html">Logout</a></li>
                                            </ul>
                                        </div>
                                         user dropdown ends 
                
                                         theme selector starts 
                                        <div class="btn-group pull-right theme-container animated tada">
                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <i class="glyphicon glyphicon-tint"></i><span
                                                    class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" id="themes">
                                                <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                                                <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                                                <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                                                <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                                                <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                                                <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                                                <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                                                <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                                                <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                                            </ul>
                                        </div>
                                         theme selector ends 
                
                                        <ul class="collapse navbar-collapse nav navbar-nav top-menu">
                                            <li><a href="#"><i class="glyphicon glyphicon-globe"></i> Visit Site</a></li>
                                            <li class="dropdown">
                                                <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> Dropdown <span
                                                        class="caret"></span></a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">One more separated link</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <form class="navbar-search pull-left">
                                                    <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                                                           type="text">
                                                </form>
                                            </li>
                                        </ul>
                
                                    </div>
                                </div>-->


                <div class="navbar navbar-default" role="navigation">

                    <div class="navbar-inner">
                        <button type="button" class="navbar-toggle pull-left animated flip">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url(); ?>home"> <img alt="Shwari Healthcare" src="<?php echo base_url(); ?>assets/img/shwari.jpg" class="hidden-xs"/>
                            <span>Shwari</span></a>

                        <!-- user dropdown starts -->
                        <div class="btn-group pull-right">
                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Reception</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>home/do_logout">Logout</a></li>
                            </ul>
                        </div>



                    </div>
                </div>
                <!-- topbar ends -->
                <?php
            }
        } else {
            
        }
        ?>
        <div class="ch-container">
            <div class="row">
                <?php
                if (!isset($no_visible_elements) || !$no_visible_elements) {


                    if ($this->session->userdata('id')) {
                        ?>

                        <!-- left menu starts -->
                        <!--                        <div class="col-sm-2 col-lg-2">
                                                    <div class="sidebar-nav">
                                                        <div class="nav-canvas">
                                                            <div class="nav-sm nav nav-stacked">
                        
                                                            </div>
                                                            <ul class="nav nav-pills nav-stacked main-menu">
                                                                <li class="nav-header">Main</li>
                                                                <li><a class="ajax-link" href="index.html"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                                                                </li>
                                                                <li><a class="ajax-link" href="ui.html"><i class="glyphicon glyphicon-eye-open"></i><span> UI Features</span></a>
                                                                </li>
                                                                <li><a class="ajax-link" href="form.html"><i
                                                                            class="glyphicon glyphicon-edit"></i><span> Forms</span></a></li>
                                                                <li><a class="ajax-link" href="chart.html"><i class="glyphicon glyphicon-list-alt"></i><span> Charts</span></a>
                                                                </li>
                                                                <li><a class="ajax-link" href="typography.html"><i class="glyphicon glyphicon-font"></i><span> Typography</span></a>
                                                                </li>
                                                                <li><a class="ajax-link" href="gallery.html"><i class="glyphicon glyphicon-picture"></i><span> Gallery</span></a>
                                                                </li>
                                                                <li class="nav-header hidden-md">Sample Section</li>
                                                                <li><a class="ajax-link" href="table.html"><i
                                                                            class="glyphicon glyphicon-align-justify"></i><span> Tables</span></a></li>
                                                                <li class="accordion">
                                                                    <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Accordion Menu</span></a>
                                                                    <ul class="nav nav-pills nav-stacked">
                                                                        <li><a href="#">Child Menu 1</a></li>
                                                                        <li><a href="#">Child Menu 2</a></li>
                                                                    </ul>
                                                                </li>
                                                                <li><a class="ajax-link" href="calendar.html"><i class="glyphicon glyphicon-calendar"></i><span> Calendar</span></a>
                                                                </li>
                                                                <li><a class="ajax-link" href="grid.html"><i
                                                                            class="glyphicon glyphicon-th"></i><span> Grid</span></a></li>
                                                                <li><a href="tour.html"><i class="glyphicon glyphicon-globe"></i><span> Tour</span></a></li>
                                                                <li><a class="ajax-link" href="icon.html"><i
                                                                            class="glyphicon glyphicon-star"></i><span> Icons</span></a></li>
                                                                <li><a href="error.html"><i class="glyphicon glyphicon-ban-circle"></i><span> Error Page</span></a>
                                                                </li>
                                                                <li><a href="login.html"><i class="glyphicon glyphicon-lock"></i><span> Login Page</span></a>
                                                                </li>
                                                            </ul>
                                                            <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                                        </div>
                                                    </div>
                                                </div>-->

                        <div class="col-sm-2 col-lg-2">
                            <div class="sidebar-nav">
                                <div class="nav-canvas">
                                    <div class="nav-sm nav nav-stacked">

                                    </div>

                                    <ul class="nav nav-pills nav-stacked main-menu">
                                        <li class="nav-header">Main</li>
                                        <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                        </li>
                                        <li><a class="ajax-link" id="register_patient" href="#patient_option"><i class="glyphicon glyphicon-edit"></i><span> Register Patient</span></a>
                                        </li>
                                        <li><a class="ajax-link" href="<?php echo base_url(); ?>reception/visit"><i class="glyphicon glyphicon-book"></i><span> Book Visit</span></a></li>
                                        <li><a class="ajax-link" href="<?php echo base_url(); ?>reception/walkin_patient"><i class="glyphicon glyphicon-plus-sign"></i><span> Add Walkin</span></a></li>

                                        <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier"><i class="glyphicon glyphicon-check"></i><span> Payments</span></a>
                                        </li>
                                        <li><a class="ajax-link" href="<?php echo base_url(); ?>reports/visitation_report"><i class="glyphicon glyphicon-check"></i><span> Edit visitation </span></a>
                                        </li>
                                        
                                        <li><a class="ajax-link" id="patient_procedure_option" href="#procedure_option"><i class="glyphicon glyphicon-check"></i><span> Book patient for Procedure </span></a>
                                        </li>
                                        
                                        <li class="accordion">
                                            <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                            <ul class="nav nav-pills nav-stacked">
                                                <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                <li><a href="#procedure_report_filter_form" id="procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                            </ul>
                                        </li>

                                        <li class="nav-header hidden-md"></li>
                                        <!--<li><a class="ajax-link" href="visit_records.html"><i
                                                    class="glyphicon glyphicon-align-justify"></i><span> Visit Records</span></a></li>
                                        -->


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                        </li>
                                    </ul>




                                    <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                </div>
                            </div>
                        </div>

                        <!--/span-->
                        <!-- left menu ends -->

                        <noscript>
                        <div class="alert alert-block col-md-12">
                            <h4 class="alert-heading">Warning!</h4>

                            <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                                enabled to use this site.</p>
                        </div>
                        </noscript>






                        <div id = "procedure_option" style = "display: none;">


                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                        <div class = "box-icon">

                                        </div>
                                    </div>

                                    <div class = "box-content">
                                        <label class = "">
                                            Are you a regular patient to any of our Clinics ?
                                        </label><br>

                                        <a id = "regular_patient_yes" class = "btn btn-info btn-sm regular_patient_yes" href ="#shwari_regular_client">
                                            Yes
                                        </a>

                                        <a id = "regular_patient_no" href = "#not_regular_patient" class = "btn btn-info btn-sm regular_patient_no">
                                            No
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->


                        </div>




                        <div id = "not_interested_in_joining" style = "display: none;">

                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Book Procedure</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" id="book_patient_procedure_form" class="form-inline add_visit_form book_patient_procedure_form">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Patient Name (required) </label>

                                                    <div class="controls">
                                                        <input type="text" name="patientname" id="patientname" class="patientname form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Phone Number :  (required)</label>

                                                    <div class="controls">
                                                        <input type="text" name="patient_phone" class="form-control input-sm"  id="patient_phone"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Department  (required)</label>

                                                    <div class="controls">
                                                        <select id="selectError" name="procedure_name" required="" data-rel="">
                                                            <option value="">Please select  Procedure : </option>

                                                            <?php foreach ($procedure_name as $proceudre_type) { ?>
                                                                <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                                            <?php } ?>

                                                        </select>
                                                    </div>



                                                </div>


                                                <hr>



                                                <hr>

                                                <input type="submit" class=" btn btn-info book_patient_procedure_button" id="book_patient_procedure_button" value="Book Patient"/>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>





                        </div>




                        <div id = "shwari_regular_client" style = "display: none;">

                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Book Procedure</h2>


                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">
                                                <form class = "shwari_patient_book_procedure_form" method = "post"  id = "shwari_patient_book_procedure_form" role = "form">
                                                    <div class="form-inline">

                                                        <div class = "form-inline">
                                                            <div class="control-group">
                                                                <label class="control-label" for="selectError">Patient Name (required) </label>

                                                                <div class="controls">
                                                                    <select id="selectError" name="patientname" required="" data-rel="">
                                                                        <option value="">Please select Patient Name : </option>
                                                                        <?php foreach ($name as $name_types) { ?>
                                                                            <option  value="<?php echo $name_types['patient_id'] ?>" id="<?php echo $name_types['f_name'] ?>" ><?php echo $name_types['f_name'] ?>   <?php echo $name_types['s_name'] ?>   <?php echo $name_types['other_name'] ?>(<?php echo $name_types['family_number'] ?>)</option>
                                                                        <?php } ?>

                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <hr>

                                                        </div>


                                                        <div class="form-inline">
                                                            <div class="controls">
                                                                <select id="selectError" name="procedure_name" required="" data-rel="">
                                                                    <option value="">Please select  Procedure : </option>

                                                                    <?php foreach ($procedure_name as $proceudre_type) { ?>
                                                                        <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                                                    <?php } ?>

                                                                </select>
                                                            </div>
                                                        </div>





                                                        <div class = "form-inline">
                                                            <div class = "form-group">
                                                                <input type = "submit" value = "Book for Procedure" class = "btn btn-success shwari_patient_book_procedure_button" id = "shwari_patient_book_procedure_button"/>
                                                                <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                            </div>
                                                        </div>

                                                    </div>


                                                </form>
                                            </div>
                                        </div>






                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>



                        <div id = "not_regular_patient" style = "display: none;">


                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                        <div class = "box-icon">

                                        </div>
                                    </div>

                                    <div class = "box-content">
                                        <label class = "">
                                            Would you like to Join our Clinic network ?
                                        </label><br>

                                        <a id = "shwari_relation_no" href = "#shwari_relation_no_form" class = "btn btn-info btn-sm shwari_relation_no">
                                            Yes
                                        </a>

                                        <a id = "not_joining" href = "#not_interested_in_joining" class = "btn btn-info btn-sm not_joining">
                                            No
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->


                        </div>






                        <div id = "patient_option" style = "display: none;">


                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                        <div class = "box-icon">

                                        </div>
                                    </div>

                                    <div class = "box-content">
                                        <label class = "">
                                            Are you related to any Shwari Member ?
                                        </label><br>

                                        <a id = "shwari_relation_yes" class = "btn btn-info btn-sm shwari_relation_yes" href = "#shwari_relation_yes_form">
                                            Yes
                                        </a>

                                        <a id = "shwari_relation_no" href = "#shwari_relation_no_form" class = "btn btn-info btn-sm shwari_relation_no">
                                            No
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->


                        </div>




                        <div class = "form-control" id = "shwari_relation_yes_form" style = "display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Registration</h2>


                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">
                                                <form class = "register_new_patient_form " autocomplete="off" method = "post"  id = "register_new_patient_form" role = "form">
                                                    <div class="form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputShwariFamilyNumber">Shwari Family Number :</label>
                                                            <input type = "text" class = "form-control" id ="inputShwariFamilyNumber" placeholder = "Shwari Family Number">
                                                        </div>

                                                        <div class = "form-group">
                                                            <div><label>Family Number Result : </label></div>
                                                            <div id="shwari_family_number_result" name="shwari_family_number_result" class="shwari_family_number_result">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class = "form-group">
                                                        <label class = "sr-only" for = "family_number"> Family Number :</label>
                                                        <input type = "text" class = "form-control" name="family_number" id ="family_number" placeholder = "Please paste the  family number : ">
                                                    </div>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "family_number"> Family Number :</label>
                                                            <select name="title" class="selector" id="title" data-bind="">
                                                                <?php foreach ($title as $value) {
                                                                    ?>
                                                                    <option value="<?php echo $value['title_name']; ?>"><?php echo $value['title_name']; ?></option>
                                                                <?php }
                                                                ?>
                                                            </select>   
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputFirstName">First Name : </label>
                                                            <input type = "text" class = "form-control" id = "inputFirstName" name="fname" placeholder = "First Name">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputSurName">Sur Name : </label>
                                                            <input type = "text" class = "form-control" id = "inputSurName" name="sname" placeholder = "Sur Name">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputOtherName">Other Name : </label>
                                                            <input type = "text" class = "form-control" id = "inputOtherName" name="lname" placeholder = "Other Name">
                                                        </div>

                                                    </div>

                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputDateofBirth">Date of Birth</label>
                                                            <input type = "text" class = "form-control" name="dob" id = "datepicker" placeholder = "D.O.B">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputSurName">National/Military/Alien ID or Passport No</label>
                                                            <input type = "text" class = "form-control" name="nationalid" id = "inputSurName" placeholder = "National ID ">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputPostalAddress">Postal Address</label>
                                                            <input type = "email" class = "form-control" name="address" id = "inputPostalAddress" placeholder = "Postal Address">
                                                        </div>
                                                    </div>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputResidence">Residence</label>
                                                            <input type = "text" class = "form-control" name="residence" id = "inputResidence" placeholder = "Residence">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputCity">City</label>
                                                            <input type = "text" class = "form-control" name="city" id = "inputCity" placeholder = "City">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputPhoneNo">Phone No</label>
                                                            <input type = "text" class = "form-control" name="phone_no" id = "inputPhoneNo" placeholder = "Phone No">
                                                        </div>
                                                    </div>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputEmail">Email : </label>
                                                            <input type = "email" class = "form-control" name="email" id ="inputEmail"  placeholder = "Email">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputEmail">Employment Status : </label>
                                                            <span class="radio"> <input type = "radio" class = "form-control radio checkbox employmentstatus_yes" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Employed">
                                                                Employed</span>
                                                            <span class="radio">  <input type = "radio" class = "form-control radio checkbox employmentstatus_no" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Not Employed">
                                                                Not Employed</span>

                                                        </div>

                                                        <div class = "form-group employers_name_div" id="employers_name_div" >
                                                            <label class = "sr-only" for = "inputEmail">Employer's Name : </label>
                                                            <input type = "text" style="display:none ;" class = "form-control employers_name" name="employers_name" id ="employers_name"  placeholder = "Employmer's Name" >

                                                        </div>


                                                    </div>


                                                    <div class = "form-inline">
                                                        <div class = "control-group">
                                                            <label class = "control-label" for = "inputGender">Gender</label>
                                                            <div class = "controls">
                                                                <select class = "selectError" name="sex" data-rel = "" >
                                                                    <option value = "Male">Male</option>
                                                                    <option value = "Female">Female</option>
                                                                    <option value = "Trans-gender">Trans-gender</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class = "control-group">
                                                            <label class = "control-label" for = "inputMaritalStatus">Marital Status</label>
                                                            <div class = "controls">
                                                                <select class = "selectError" name="maritalstatus" data-rel = "">
                                                                    <option value = "Single">Single</option>
                                                                    <option value = "Married">Married</option>
                                                                    <option value = "Divorced">Divorced</option>
                                                                    <option value = "Widowed">Widowed</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                    </div>



                                                    <label>Next of Kin Details </label>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputFirstName">First Name</label>
                                                            <input type = "text" class = "form-control" id = "inputFirstName" name="kinname" placeholder = "First Name">
                                                        </div><div class = "form-group">
                                                            <label class = "sr-only" for = "inputLastName">Last Name</label>
                                                            <input type = "text" class = "form-control" id = "inputLastName" name="kinsname" placeholder = "Last Name">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputMiddleName">Middle Name</label>
                                                            <input type = "text" class = "form-control" id = "inputMiddleName" placeholder = "Middle Name">
                                                        </div>
                                                    </div>


                                                    <div class = "form-inline">

                                                        <div class = "control-group">
                                                            <label class = "control-label" for = "inputkinrelation"> Relation Type : </label>
                                                            <div class = "controls">
                                                                <select class = "selectError" name="kinrelation" data-rel = "">
                                                                    <option value = "Sibling">Sibling</option>
                                                                    <option value = "Father">Father</option>
                                                                    <option value = "Mother">Mother</option>
                                                                    <option value = "Husband">Husband</option>
                                                                    <option value = "Wife">Wife</option>
                                                                    <option value = "Aunt">Aunt</option>
                                                                    <option value="Uncle">Uncle</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputPhone">Kin's Phone No</label>
                                                            <input type="text" placeholder="Kin's Phone"  class = "form-control"  id="kinphone" name="kinphone" size="30" />
                                                        </div>

                                                    </div>
                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <input type = "submit" value = "Register Patient" class = "btn btn-success add_register_new_patient" id = "add_register_new_patient"/>
                                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                        </div>
                                                    </div>




                                                </form>



                                            </div>
                                        </div>






                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>









                        <div class = "form-control" id = "shwari_relation_no_form" style = "display: none;">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Registration Form</h2>

                                            <div class = "box-icon">
                                                <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-cog"></i></a>
                                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>
                                                <a href = "#" class = "btn btn-close btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-remove"></i></a>
                                            </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">


                                                <form class = "new_family_registration_form " autocomplete="off" method = "post"  id = "new_family_registration_form" role = "form">



                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "family_number"> Title :</label>
                                                            <select name="title"  required="" class="selector" id="title" >
                                                                <option value="">Please select title </option>
                                                                <?php foreach ($title as $value) {
                                                                    ?>
                                                                    <option value="<?php echo $value['title_name']; ?>"><?php echo $value['title_name']; ?></option>
                                                                <?php }
                                                                ?>
                                                            </select>   
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputFirstName">First Name : </label>
                                                            <input type = "text" class = "form-control" id = "inputFirstName" name="fname" placeholder = "First Name">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputSurName">Sur Name : </label>
                                                            <input type = "text" class = "form-control" id = "inputSurName" name="sname" placeholder = "Sur Name">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputOtherName">Other Name : </label>
                                                            <input type = "text" class = "form-control" id = "inputOtherName" name="lname" placeholder = "Other Name">
                                                        </div>

                                                    </div>

                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputDateofBirth">Date of Birth</label>
                                                            <input type = "text" class = "form-control" name="dob" id = "datepicker_1" placeholder = "D.O.B">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputSurName">National/Military/Alien ID or Passport No</label>
                                                            <input type = "text" class = "form-control" name="nationalid" id = "inputSurName" placeholder = "National ID ">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputPostalAddress">Postal Address</label>
                                                            <input type = "email" class = "form-control" name="address" id = "inputPostalAddress" placeholder = "Postal Address">
                                                        </div>
                                                    </div>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputResidence">Residence</label>
                                                            <input type = "text" class = "form-control" name="residence" id = "inputResidence" placeholder = "Residence">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputCity">City</label>
                                                            <input type = "text" class = "form-control" name="city" id = "inputCity" placeholder = "City">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputPhoneNo">Phone No</label>
                                                            <input type = "text" class = "form-control" name="phone_no" id = "inputPhoneNo" placeholder = "Phone No">
                                                        </div>
                                                    </div>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputEmail">Email : </label>
                                                            <input type = "email" class = "form-control" name="email" id ="inputEmail"  placeholder = "Email">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputEmail">Employment Status : </label>
                                                            <span class="radio"> <input type = "radio" class = "form-control radio checkbox employmentstatus_yes_1" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Employed">
                                                                Employed</span>
                                                            <span class="radio">  <input type = "radio" class = "form-control radio checkbox employmentstatus_no_1" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Not Employed">
                                                                Not Employed</span>

                                                        </div>

                                                        <div class = "form-group employers_name_div" id="employers_name_div_1" >
                                                            <label class = "sr-only" for = "inputEmail">Employer's Name : </label>
                                                            <input type = "text" style="display:none ;" class = "form-control employers_name_1" name="employers_name" id ="employers_name_1"  placeholder = "Employmer's Name" >

                                                        </div>


                                                    </div>


                                                    <div class = "form-inline">
                                                        <div class = "control-group">
                                                            <label class = "control-label" for = "inputGender">Gender</label>
                                                            <div class = "controls">
                                                                <select class = "selectError" name="sex" data-rel = "" >
                                                                    <option value = "Male">Male</option>
                                                                    <option value = "Female">Female</option>
                                                                    <option value = "Trans-gender">Trans-gender</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class = "control-group">
                                                            <label class = "control-label" for = "inputMaritalStatus">Marital Status</label>
                                                            <div class = "controls">
                                                                <select class = "selectError" name="maritalstatus" data-rel = "">
                                                                    <option value = "Single">Single</option>
                                                                    <option value = "Married">Married</option>
                                                                    <option value = "Divorced">Divorced</option>
                                                                    <option value = "Widowed">Widowed</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                    </div>



                                                    <label>Next of Kin Details </label>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputFirstName">First Name</label>
                                                            <input type = "text" class = "form-control" id = "inputFirstName" name="kinname" placeholder = "First Name">
                                                        </div><div class = "form-group">
                                                            <label class = "sr-only" for = "inputLastName">Last Name</label>
                                                            <input type = "text" class = "form-control" id = "inputLastName" name="kinsname" placeholder = "Last Name">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputMiddleName">Middle Name</label>
                                                            <input type = "text" class = "form-control" id = "inputMiddleName" placeholder = "Middle Name">
                                                        </div>
                                                    </div>


                                                    <div class = "form-inline">

                                                        <div class = "control-group">
                                                            <label class = "control-label" for = "inputkinrelation"> Relation Type : </label>
                                                            <div class = "controls">
                                                                <select class = "selectError" name="kinrelation" data-rel = "">
                                                                    <option value = "Sibling">Sibling</option>
                                                                    <option value = "Father">Father</option>
                                                                    <option value = "Mother">Mother</option>
                                                                    <option value = "Husband">Husband</option>
                                                                    <option value = "Wife">Wife</option>
                                                                    <option value = "Aunt">Aunt</option>
                                                                    <option value="Uncle">Uncle</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputPhone">Kin's Phone No</label>
                                                            <input type="text" placeholder="Kin's Phone"  class = "form-control"  id="kinphone" name="kinphone" size="30" />
                                                        </div>

                                                    </div>
                                                    <hr>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <input type = "submit" value = "Register Patient" class = "btn btn-success new_family_registration_button" id = "new_family_registration_button"/>
                                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                        </div>
                                                    </div>




                                                </form>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div><!--/row-->

                        </div>






                        <div id="walkin_report_filter_form" style="display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Care-tech Walk-in Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/walkin_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="walkin_date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="walkin_date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Department Name : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="department_name" class="department_name" required="" data-rel="">
                                                            <option value="">Please select  Department : </option>
                                                            <option value="All">All</option>
                                                            <option value="Nursing">Nursing</option>
                                                            <option value="Laboratory">Laboratory</option>
                                                            <option value="Pharmacy">Pharmacy</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Payment Status  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="payment_status" id="package_type" required="" data-rel="">
                                                            <option value="">Please select  Payment Status : </option>
                                                            <option value="All">All</option>
                                                            <option value="Paid">Paid</option>
                                                            <option value="Not Paid">Not Paid</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                        </div>

                        <div id="visit_report_filter_form" style="display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Visitation Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/visitation_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="visitation_date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="visitation_date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Patient Name : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="patient_name" class="patient_name" required="" data-rel="">
                                                            <option value="">Please select  Status : </option>
                                                            <?php foreach ($name as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['patient_id']; ?>"><?php echo $value['title'] . ':' . $value['f_name'] . $value['s_name'] . $value['other_name']; ?></option><?php }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Package Type  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="package_type" id="package_type" required="" data-rel="">
                                                            <option value="">Please select  Package Type : </option>
                                                            <?php foreach ($pack as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['package_id']; ?>"><?php echo $value['package_name']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                        </div>

                        <div id = "patient_report_filter_form" style = "display: none;">

                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Patient Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/patient_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Patient Status : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="patient_status" class="patient_status" required="" data-rel="">
                                                            <option value="">Please select  Status : </option>
                                                            <option value="All">All</option>
                                                            <option value="Active">Active</option>
                                                            <option value="In Active">In Active</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Gender  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="gender" id="gender" required="" data-rel="">
                                                            <option value="">Please select  Gender : </option>
                                                            <option value="All">All</option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                            <option value="transgender">Trans-Gender</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>


                        </div>

                        <div id="content" class="col-lg-10 col-sm-10">
                            <!-- content starts -->
                            <?php
                        }
                    }
                    ?>