<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Book Visit</a>
        </li>
    </ul>
</div>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Book Patient Visit</h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">


                <!--
                
                
                
                                <div class="appointment_calendar" id="appointment_calendar" style="display: inline;">
                                    <div class = "row">
                                        <div class = "box col-md-12">
                                            <div class = "box-inner">
                                                <div class = "box-header well" data-original-title = "">
                                                    <h2><i class = "glyphicon glyphicon-edit"></i> Visit Appointment Calendar</h2>
                
                                                    <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                                class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                                </div>
                                                <div class = "box-content">
                
                                                    <div class = "bs-example">
                                                        <button class="show_calendar btn btn-info" id="show_calendar">Show Calendar</button>
                                                        <button class="show_book_visit btn btn-info" id="show_book_visit" style="display: none;">Book Visit</button>
                                                        <a href="#book_visit_form" id="book_visit_link" class="book_visit_link btn btn-info ">Book Visit </a>
                
                
                                                        <div class="appointment_calendar_form" id="appointment_calendar_form" >
                
                                                            <link href='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.css' rel='stylesheet' />
                                                            <link href='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
                                                            <script src='<?php echo base_url(); ?>assets/fullcalendar/lib/moment.min.js'></script>
                                                            <script src='<?php echo base_url(); ?>assets/fullcalendar/lib/jquery.min.js'></script>
                                                            <script src='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.min.js'></script>
                
                
                
                
                
                
                                                            <script>
                
                                                                $(document).ready(function () {
                                                                    var date = new Date();
                                                                    var d = date.getDate();
                                                                    var m = date.getMonth();
                                                                    var y = date.getFullYear();
                
                                                                    var calendar = $('#calendar').fullCalendar({
                                                                        editable: true,
                                                                        header: {
                                                                            left: 'prev,next today',
                                                                            center: 'title',
                                                                            right: 'month,agendaWeek,agendaDay'
                                                                        },
                                                                        events: "<?php echo base_url(); ?>appointments/get_appointmentss",
                                                                        // Convert the allDay from string to boolean
                                                                        eventRender: function (event, element, view) {
                                                                            if (event.allDay === 'true') {
                                                                                event.allDay = true;
                                                                            } else {
                                                                                event.allDay = false;
                                                                            }
                                                                        },
                                                                        selectable: true,
                                                                        selectHelper: true
                
                                                                    });
                                                                    
                                                                    
                                                                    
                
                                                                });
                
                                                            </script>
                
                
                
                
                
                
                                                            <div id='calendar'></div>
                
                
                                                        </div>
                
                
                
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                -->



                <div class="book_visit_form" id="book_visit_form" >





                    <div class = "row">
                        <div class = "box col-md-12">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "">
                                    <h2><i class = "glyphicon glyphicon-edit"></i> Book Visit </h2>

                                    <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                </div>
                                <div class = "box-content">

                                    <div class = "bs-example">

                                        <form role="form"  id="add_visit_form" autocomplete="off" class="form-inline  add_visit_form">
                                            <div class="control-group">
                                                <label class="control-label" for="selectError">Patient Name (required) </label>

                                                <div class="controls">
                                                    <select id="selectError" name="visit_id" class="patient_visit_name" required="" data-rel="chosen">
                                                        <option value="">Please select Patient Name : </option>
                                                        <?php foreach ($name as $name_types) { ?>
                                                            <option  value="<?php echo $name_types['patient_id'] ?>" id="<?php echo $name_types['f_name'] ?>" ><?php echo $name_types['f_name'] ?>   <?php echo $name_types['s_name'] ?>   <?php echo $name_types['other_name'] ?>(<?php echo $name_types['family_number'] ?>)</option>
                                                        <?php } ?>

                                                    </select>
                                                </div>
                                                <hr>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="selectError">Visit Type (required)</label>

                                                <div class="controls">
                                                    <select id="selectError form-control" class="visit_type" required="" name="visit_type" >
                                                        <option value="">Please select visit type : </option>
                                                        <option value="new_visit_nurse">New Visit Nurse</option>
                                                        <option value="new_visit_lab">New Visit Lab</option>
                                                        <option value="new_visit_doctor">New Visit Doctor</option>
                                                        <option value="follow_up_nurse">Follow Up Nurse</option>
                                                        <option value="follow_up_lab">Follow Up Lab</option>
                                                        <option value="follow_up_doctor">Follow Up Doctor </option>
                                                        <option value="walk_in_nurse">Walk-in Nurse</option>
                                                        <option value="walk_in_lab">Walk-in Laboratory</option>
                                                        <option value="walk_in_pharmacy">Walk-in Pharmacy</option>
                                                    </select>
                                                </div>


                                                <hr>

                                            </div>


                                            <div class="control-group" id="pay_at_end_div">
                                                <label>Pay at the  End ? (Express Service ) : </label>
                                                <input type="checkbox" name="pay_at_the_end" value="yes" class="pay_at_the_end" id="pay_at_the_end"/>

                                                <hr>
                                            </div>
                                            <br>




                                            <div class="control-group charge_follow_up_div" id="charge_follow_up_div" style="display: none;">
                                                <label>Charge for Follow Up ?  : </label>
                                                <input type="checkbox" name="charge_follow_up" value="yes" class="charge_follow_up" id="charge_follow_up"/>
                                                <hr>
                                            </div>



                                            <div class="control-group queue_to_join_div" id="queue_to_join_div" style="display: none;">
                                                <label class="control-label" for="selectError">Queue to join?  (required)</label>

                                                <div class="controls">
                                                    <select id="" name="queue_to_join" class="queue_to_join"  data-rel="">
                                                        <option value="">Please select  Queue to join : </option>

                                                        <?php foreach ($queue_name as $name_types) { ?>
                                                            <option  value="<?php echo $name_types['queue_name'] ?>" id="<?php echo $name_types['queue_name'] ?>" ><?php echo $name_types['queue_name'] ?>  </option>
                                                        <?php } ?>

                                                    </select>
                                                </div>
                                                <hr>
                                            </div>



                                            <div class="control-group active_doctor_list_div" id="active_doctor_list_div" style="display:none ;">
                                                <label class="control-label" for="selectError">Select Doctor </label>

                                                <div class="controls">

                                                    <SELECT data-placeholder="Select Doctor's Name ..." class="active_doctor_list_1" id="active_doctor_list_1" name="doctor_id">

                                                    </SELECT>
                                                </div>
                                                <hr>
                                            </div>


                                            <div class="control-group">
                                                <label>Visit Date/ Time : </label>
                                                <div class="controls">
                                                    <div class="form-group">
                                                        <div class='input-group date' id='datetimepicker1'>
                                                            <input type='text' placeholder=" From : " required="" name="date_from" id="visit_date_start" class="form-control visit_date_start" />
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class='input-group date' id='datetimepicker2'>
                                                            <input type='text' placeholder="To : " required="" name="date_to" id="visit_date_end" class="form-control visit_date_end" />
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>                                            
                                            </div>




                                            <div class="control-group" id="package_type_visit_div">
                                                <label class="control-label" for="selectError">Package Type (required)</label>

                                                <div class="controls">
                                                    <select id="selectError package" name="package_id"  class="visit_package_type" data-rel="chosen">
                                                        <option value="">Please select package type : </option>
                                                        <?php foreach ($pack as $pack_types) { ?>
                                                            <option  value="<?php echo $pack_types['package_id']; ?>" id="<?php echo $pack_types['cost'] ?>" ><?php echo $pack_types['package_name']; ?>(Kes :<?php echo $pack_types['cost']; ?>)<?php echo $pack_types['type']; ?> </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <hr>
                                            </div>
                                            
                                            <br>


                                            <input type="submit" class=" btn btn-info book_visit_button" id="book_visit_button" value="Book Visit"/>
                                            <input type="reset" class="btn btn-danger reset_visit_button" id="reset_visit_button" value="Cancel"/>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>





                </div>



            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<!-- content ends -->
</div><!--/#content.col-md-0-->
</div><!--/fluid-row-->