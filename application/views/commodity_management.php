<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Reports</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-shopping-cart"></i> Stock Management</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">

                <!--                
                                <a  id="export_stock_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Stock  Report" class="export_stock_report_filter_link" href="#export_stock_report_filter_form">
                                    <i class="glyphicon glyphicon-download-alt"></i> </a>
                -->

                <a href="#add_commodity_form" class="btn btn-default btn-sm add_commodity_link" id="add_commodity_link"><i class="glyphicon glyphicon-plus-sign"></i> Add Stock</a>
                <a href="#export_commodity_report_filter_form" data-original-title="Export Stock  Report" class="btn btn-default btn-sm export_commodity_report_filter_link" id="export_commodity_report_filter_link" ><i class="glyphicon glyphicon-download-alt"></i> Export as Excel</a>


                <table id="commodity_management_table" class=" dataTable table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Date </th>
                            <th>Commodity Name</th>
                            <th>Commodity Code</th>
                            <th>Strength</th>
                            <th>Unit</th>
                            <th>Max Stock</th>
                            <th>Min Stock</th>
                            <th>Status</th>
                            <th>Posted by :</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Date </th>
                            <th>Commodity Name</th>
                            <th>Commodity Code</th>
                            <th>Strength</th>
                            <th>Unit</th>
                            <th>Max Stock</th>
                            <th>Min Stock</th>
                            <th>Status</th>
                            <th>Posted by :</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        <tr>
                            <?php
                            $i = 1;
                            foreach ($commodity_management as $value) {
                                ?>
                                <td class="center"><?php echo $i; ?></td>
                                <td class="center">
                                    <?php
                                    echo $value['date_added'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['commodity_name'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['commodity_type'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['strength']; ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['commodity_unit']; ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['max_stock']; ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['min_stock']; ?>
                                </td>
                                <td class="center">
                                    <?php
                                    $has_expired = $value['status'];
                                    if ($has_expired === 'Active') {
                                        ?>
                                        <label class="label label-success"><?php echo $has_expired; ?></label>
                                        <?php
                                    } elseif ($has_expired === 'In Active') {
                                        ?>
                                        <label class="label label-danger"><?php echo $has_expired; ?></label>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['employee_name'];?>
                                </td>



                                <td class="center">
                                    <input type="hidden" name="view_commodity_id" class="view_commodity_id" id="view_commodity_id" value="<?php echo $value['commodity_id']; ?>"/>
                                    <a  id="view_commodity_link" class="view_commodity_link" href="#view_commodity_info_form">
                                        <i class="glyphicon glyphicon-zoom-in icon-white"></i>

                                    </a>|
                                    <a  id="edit_commodity_link" class="edit_commodity_link" href="#edit_commodity_info_form">
                                        <i class="glyphicon glyphicon-edit icon-white"></i>

                                    </a>|
                                    <a  id="delete_commodity_link" class="delete_commodity_link" href="#delete_commodity_info_form">
                                        <i class="glyphicon glyphicon-trash icon-white"></i>

                                    </a>
                                </td>

                            </tr>
                            <?php
                            $i++;
                        }
                        ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->


</div><!--/row-->


<div id="export_commodity_report_filter_form" style="display: none;">


    <style type = "text/css">
        .bs-example{
            margin: 20px;
        }
    </style>


    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i>  Stock Report Filter</h2>


                </div>
                <div class="box-content">
                    <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/export_commodity_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                        <div class="control-group">
                            <label class="control-label" for="selectError">Date From :  </label>

                            <div class="controls">
                                <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="commodity_date_from" class="commodity_date_from form-control input-sm"/>
                            </div>
                        </div>

                        <hr>
                        <div class="control-group">
                            <label class="control-label" for="selectError">Date To : </label>

                            <div class="controls">
                                <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" commodity_date_to form-control input-sm"  id="commodity_date_to"/>
                            </div>
                        </div>
                        <hr>


                        <div class="control-group">
                            <label class="control-label" for="selectError">Commodity Name : </label>

                            <div class="controls">
                                <select id="selectError" name="commmodity_name" class="commodity_name_filter form-control" required="" data-rel="">
                                    <option>Please select commodity :</option>
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($commodities as $value) {
                                        ?>

                                        <option value="<?php echo $value['commodity_name']; ?>"><?php echo $value['commodity_name']; ?></option>
                                        <?php
                                    }
                                    ?> 
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="control-group">
                            <label class="control-label" for="selectError">Commodity Type  : </label>

                            <div class="controls">
                                <select id="selectError" name="commodity_type" class="commodity_type_filter form-control" required="" data-rel="">
                                    <option>Please select commodity :</option>
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($commodity_type as $value) {
                                        ?>

                                        <option value="<?php echo $value['commodity_type']; ?>"><?php echo $value['commodity_type']; ?></option>
                                        <?php
                                    }
                                    ?> 
                                </select>
                            </div>
                        </div>

                        <hr>

                        <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                        <hr>
                    </form>

                </div>
            </div>
        </div>
        <!--/span-->
    </div>

</div>






<div class = "form-control view_commodity_info_form" id = "view_commodity_info_form" style = "display: none;">


    <style type = "text/css">
        .bs-example{
            margin: 20px;
        }
    </style>


    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-view"></i> View Stock Information</h2>


                </div>
                <div class = "box-content">

                    <div class = "bs-example">
                        <div class="view_outer_info_loader" id="view_outer_info_loader">
                            <div class="box-content " id="">
                                <ul class="ajax-loaders">

                                    <img src="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" title="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" >
                                    </br>
                                    <br/>


                                </ul>


                            </div>

                        </div>
                        <div class="view_outer_form" id="view_outer_form" style="display: none;">
                            <form id="view_commodity_form" class="view_commodity_form" >


                                <div class="form-inline">
                                    <div class="form-group">
                                        <textarea name="view_commodity_name" id="view_commodity_name" class="view_commodity_name form-control" readonly="">
                                        
                                        </textarea>

                                    </div>
                                    <div class="div_1_hide" id="div_1_hide" style="display: inline;">


                                        <div class="form-group" >
                                            <input class="form-control view_commodity_type" readonly="" placeholder="Commodity Type" id="view_commodity_type" name="commodity_type"/>
                                            <input class="form-control view_commodity_code" readonly="" placeholder="Commodity Code" id="view_commodity_code" name="commodity_code"/>
                                            <input class="form-control view_batch_no" readonly="" placeholder="Batch No" id="view_batch_no" name="batch_no"/>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <input type="text" readonly="" name="view_supplier_name" id="view_supplier_name" class="view_supplier_name form-control"/>

                                        </div>
                                        <div class="form-group">
                                            <input type="text" readonly="" name="expiry_date" placeholder="Expiry Date" id="view_expiry_date" class="form-control view_expiry_date"/>
                                        </div>
                                        <hr>
                                    </div>  
                                </div>

                                <div class="div_2_hide" id="div_2_hide" style="display: inline;">

                                    <div class="form-inline">


                                        <div class="form-group">
                                            <input type="number" readonly="" name="no_of_packs" placeholder="No of Packs" id="view_no_of_packs" class="form-control view_no_of_packs"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" readonly="" name="unit_per_pack" placeholder="Unit per Pack" class="view_unit_per_pack form-control" id="view_unit_per_pack"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" readonly="" name="total_quantity" placeholder="Total Quantity" class="view_total_quantity form-control" id="view_total_quantity"/>
                                        </div>
                                    </div>
                                    <div class="form-inline">

                                        <div class="form-group">
                                            <input type="number" readonly="" name="buying_price" placeholder="Buying Price" class="view_buying_price form-control" id="view_buying_price"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" readonly="" name="selling_price" placeholder="Selling Price" class="view_selling_price form-control" id="view_selling_price"/>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="textarea view_remarks form-control placeholder" readonly="" name="remarks" placeholder="Remarks" id="view_remarks">
                                    
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="view_has_expired form-control" id="view_has_expired" readonly=""/>

                                        </div>

                                        <hr>


                                    </div>




                                </div>

                            </form>
                        </div>





                    </div>
                </div>






            </div>
        </div>
    </div>
    <!--/span-->
</div>




<div class = "form-control edit_commodity_info_form" id = "edit_commodity_info_form" style = "display: none;">


    <style type = "text/css">
        .bs-example{
            margin: 20px;
        }
    </style>


    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i>Edit Stock Information</h2>


                </div>
                <div class = "box-content">

                    <div class = "bs-example">

                        <div class="outer_info_loader" id="outer_info_loader">
                            <div class="box-content " id="">
                                <ul class="ajax-loaders">

                                    <img src="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" title="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" >
                                    </br>
                                    <br/>


                                </ul>


                            </div>

                        </div>

                        <div class="form_info" id="form_info" style="display: none;">

                            <form id="edit_commodity_form" class="edit_commodity_form" >








                                <div class="form-inline">
                                    <div class="form-group">
                                        <select id="selectError" name="commmodity_name" class="edit_commodity_name form-control" required="" data-rel="">
                                            <option></option>
                                            <option>Please select commodity :</option>

                                            <?php
                                            foreach ($commodities as $value) {
                                                ?>

                                                <option value="<?php echo $value['commodity_id']; ?>"><?php echo $value['commodity_name']; ?></option>
                                                <?php
                                            }
                                            ?> 
                                        </select>
                                    </div>


                                    <div style="display: none !important;">
                                        <div class="box-content info_loader" id="info_loader">
                                            <ul class="ajax-loaders">

                                                <img src="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" title="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" >
                                                </br>
                                                <br/>


                                            </ul>


                                        </div>

                                    </div>


                                    <div class="div_1_hide" id="div_1_hide" >
                                        <input type="hidden" name="edit_commodity_id" class="edit_commodity_id" id="edit_commodity_id"/>

                                        <div class="form-group" >
                                            <input class="form-control edit_commodity_type" placeholder="Commodity Type" id="edit_commodity_type" name="commodity_type"/>
                                            <input class="form-control edit_commodity_code" placeholder="Commodity Code" id="edit_commodity_code" name="commodity_code"/>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <select id="selectError" name="supplier_name" class="edit_supplier_name form-control" required="" data-rel="">
                                                <option></option>
                                                <option>Please select Supplier :</option>

                                                <?php
                                                foreach ($suppliers as $value) {
                                                    ?>

                                                    <option value="<?php echo $value['supplier_name']; ?>"><?php echo $value['supplier_name'] . ' : ' . $value['supplier_code']; ?></option>
                                                    <?php
                                                }
                                                ?> 
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="expiry_date" placeholder="Expiry Date" id="edit_commodity_expiry_date" class="form-control edit_commodity_expiry_date"/>
                                        </div>
                                        <hr>
                                    </div>  
                                </div>

                                <div class="div_2_hide" id="div_2_hide" >

                                    <div class="form-inline">


                                        <div class="form-group">
                                            <input type="number" name="no_of_packs" placeholder="No of Packs" id="edit_no_of_packs" class="form-control edit_no_of_packs"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" name="unit_per_pack" placeholder="Unit per Pack" class="edit_unit_per_pack form-control" id="edit_unit_per_pack"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" name="total_quantity" placeholder="Total Quantity" class="edit_total_quantity form-control" id="edit_total_quantity"/>
                                        </div>
                                    </div>
                                    <div class="form-inline">

                                        <div class="form-group">
                                            <input type="number" name="buying_price" placeholder="Buying Price" class="edit_buying_price form-control" id="edit_buying_price"/>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="textarea edit_remarks form-control placeholder"  name="remarks" placeholder="Remarks" id="edit_remarks">
                                    
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <select name="has_expired" class="edit_has_expired form-control" id="edit_has_expired">
                                                <option></option>
                                                <option>Please select one :</option>
                                                <option class="yes" id="yes" value="Yes">Yes</option>
                                                <option class="no" id="no" value="No">No</option>
                                            </select>
                                        </div>

                                        <hr>


                                    </div>

                                    <div class="form-inline">
                                        <div class="form-group">
                                            <input type="submit" name="edit_commodity" value="Add Stock" class="btn btn-success btn-xs"/>
                                            <input type="reset" name="cancel_edit_commodity" value="Cancel" class="btn btn-danger btn-xs"/>
                                        </div>
                                    </div>


                                </div>

                            </form>


                        </div>



                    </div>
                </div>






            </div>
        </div>
    </div>
    <!--/span-->
</div>









<div class = "form-control delete_commodity_info_form" id = "delete_commodity_info_form" style = "display: none;">


    <style type = "text/css">
        .bs-example{
            margin: 20px;
        }
    </style>


    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i>Delete Stock Information</h2>


                </div>
                <div class = "box-content">

                    <div class = "bs-example">

                        <div class="outer_info_loader" id="outer_info_loader">
                            <div class="box-content " id="">
                                <ul class="ajax-loaders">

                                    <img src="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" title="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" >
                                    </br>
                                    <br/>


                                </ul>


                            </div>

                        </div>

                        <div class="form_info" id="form_info" style="display: none;">

                            <form id="delete_commodity_form" class="delete_commodity_form" >








                                <div class="form-inline">



                                    <div style="display: none !important;">
                                        <div class="box-content info_loader" id="info_loader">
                                            <ul class="ajax-loaders">

                                                <img src="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" title="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" >
                                                </br>
                                                <br/>


                                            </ul>


                                        </div>

                                    </div>


                                    <div class="div_1_hide" id="div_1_hide" >
                                        <input type="hidden" name="delete_commodity_id" class="delete_commodity_id" id="delete_commodity_id"/>

                                        <div class="form-inline">
                                            <p>Are you sure you want to delete <textarea  readonly="" id="delete_commodity_name" class="delete_commodity_name form-control"></textarea> with Batch Number :<input type="text" readonly="" class="delete_batch_no form-control" id="delete_batch_no" />from the  list of Stock ? </p>
                                        </div>
                                        <hr>

                                        <div class="form-group">
                                            <input type="submit" value="Yes" name="yes_delete_commodity" id="yes_delete_commodity" class="btn btn-danger btn-xs yes_delete_commodity"/>   
                                            <input type="reset" value="No" name="no_delete_commodity" id="no_delete_commodity" class="no_delete_commodity btn btn-xs btn-success no_delete_commodity"/>
                                        </div>
                                        <hr>
                                    </div>  
                                </div>


                            </form>


                        </div>



                    </div>
                </div>






            </div>
        </div>
    </div>
    <!--/span-->
</div>









<div class = "form-control add_commodity_form" id = "add_commodity_form" style = "display: none;">


    <style type = "text/css">
        .bs-example{
            margin: 20px;
        }
    </style>


    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Stock Information</h2>


                </div>
                <div class = "box-content">

                    <div class = "bs-example">


                        <form id="add_new_commodity_form" class="add_new_commodity_form" >


                            <div class="form-inline">
                                 <div class="form-group">
                                        <select id="selectError" name="supplier_name" class="add_supplier_name form-control" required="" data-rel="">
                                            <option>Please select Supplier :</option>

                                            <?php
                                            foreach ($suppliers as $value) {
                                                ?>

                                                <option value="<?php echo $value['supplier_name']; ?>"><?php echo $value['supplier_name'] . ' : ' . $value['supplier_code']; ?></option>
                                                <?php
                                            }
                                            ?> 
                                        </select>
                                    </div>
                                <div class="div_1_hide" id="div_1_hide" style="display: none;">


                                    <div class="form-group" >
                                        <input class="form-control add_commodity_type" placeholder="Commodity Type" id="add_commodity_type" name="commodity_type"/>
                                        <input class="form-control add_commodity_code" placeholder="Commodity Code" id="add_commodity_code" name="commodity_code"/>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <select id="selectError" name="commodity_type" class="add_commodity_type form-control" required="" data-rel="">
                                            <option>Please select Commodity Type :</option>

                                            <?php
                                            foreach ($commodity_type as $value) {
                                                ?>

                                                <option value="<?php echo $value['supplier_name']; ?>"><?php echo $value['supplier_name'] . ' : ' . $value['supplier_code']; ?></option>
                                                <?php
                                            }
                                            ?> 
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="expiry_date" placeholder="Expiry Date" id="add_expiry_date" class="form-control add_expiry_date"/>
                                    </div>
                                    <hr>
                                </div>  
                            </div>

                            <div class="div_2_hide" id="div_2_hide" style="display: none;">

                                <div class="form-inline">


                                    <div class="form-group">
                                        <input type="number" name="no_of_packs" placeholder="No of Packs" id="add_no_of_packs" class="form-control add_no_of_packs"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="unit_per_pack" placeholder="Unit per Pack" class="add_unit_per_pack form-control" id="add_unit_per_pack"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="total_quantity" placeholder="Total Quantity" class="add_total_quantity form-control" id="add_total_quantity"/>
                                    </div>
                                </div>
                                <div class="form-inline">

                                    <div class="form-group">
                                        <input type="number" name="buying_price" placeholder="Buying Price" class="add_buying_price form-control" id="add_buying_price"/>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="textarea add_remarks form-control placeholder"  name="remarks" placeholder="Remarks" id="add_remarks">
                                    
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <select name="has_expired" class="add_has_expired form-control" id="add_has_expired">
                                            <option>Please select one :</option>
                                            <option class="yes" id="yes" value="Yes">Yes</option>
                                            <option class="no" id="no" value="No">No</option>
                                        </select>
                                    </div>

                                    <hr>


                                </div>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="submit" name="add_commodity" value="Add Commodity" class="btn btn-success btn-xs"/>
                                        <input type="reset" name="cancel_add_commodity" value="Cancel" class="btn btn-danger btn-xs"/>
                                    </div>
                                </div>


                            </div>

                        </form>




                    </div>
                </div>






            </div>
        </div>
    </div>
    <!--/span-->
</div>



