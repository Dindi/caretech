<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Patient Consultation History</a>
        </li>
    </ul>
</div>
<button class="btn btn-default" onclick="goBack()"><i class="glyphicon glyphicon-backward" ></i>Go Back </button>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Patient Consultation Records </h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>
                        <tr>
                            <th>Visit Date</th>
                            <th>Complaints</th>
                            <th>History of Presenting Illness</th>
                            <th>Medical History</th>
                            <th>Obstetrics and Gynaecology History</th>
                            <th>Family and Social History</th>
                            <th>Systemic Inquiry </th>
                            <th>Examination Findings</th>
                            <th>Diagnosis</th>
                            <th>Employee Name</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($patient_consultation_history as $value) {
                            ?>
                            <tr>
                                <td><?php echo $value['date']; ?></td>
                                <td class="center"><?php echo $value['complaints']; ?></td>
                                <td class="center"><?php echo $value['history_of_presenting_illness']; ?></td>
                                <td class="center"><?php echo $value['medical_history']; ?></td>
                                <td class="center"><?php echo $value['obstetrics_gynaecology']; ?></td>
                                <td class="center"><?php echo $value['family_social_history']; ?></td>
                                <td class="center">  <?php echo $value['systemic_inquiry']; ?>  </td>
                                <td class="center"> <?php echo $value['examination_findings']; ?></td>
                                <td class="center"> <?php echo $value['diagnosis']; ?></td>
                                <td class="center"<?php echo $value['employee_name']; ?>></td>

                            </tr>
                            <?php
                        }
                        ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->
