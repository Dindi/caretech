<!DOCTYPE html>
<html lang="en">
    <head>
        <!--
          
        -->
        <meta charset="utf-8">
        <title>Care-tech System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Out patient Hospital Management System.">
        <meta name="author" content="Harris Samuel Dindi">

        <!-- The styles -->
        <link id="bs-css" href="<?php echo base_url(); ?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/charisma-app.css" rel="stylesheet">

        <?php
        $function_name = $this->uri->segment(2);

        $url_array = array("visit");
        if (in_array($function_name, $url_array)) {
            ?>
            <link href='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
            <link href='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
            <link href='<?php echo base_url(); ?>assets/css/jquery.timepicker.css' rel="stylesheet">
            <link href='<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css' rel="stylesheet">



            <?php
        }
        ?>


        <link href='<?php echo base_url(); ?>assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/jquery.noty.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/noty_theme_default.css' rel='stylesheet'>



        <?php
        $function_name = $this->uri->segment(2);

        $url_array = array("error");
        if (in_array($function_name, $url_array)) {
            ?>
            <link href='<?php echo base_url(); ?>assets/css/elfinder.min.css' rel='stylesheet'>
            <link href='<?php echo base_url(); ?>assets/css/elfinder.theme.css' rel='stylesheet'>
            <link href='<?php echo base_url(); ?>assets/css/uploadify.css' rel='stylesheet'>
            <?php
        }
        ?>
        <link href='<?php echo base_url(); ?>assets/css/jquery.iphone.toggle.css' rel='stylesheet'>

        <link href='<?php echo base_url(); ?>assets/css/animate.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/chosen/chosen.css' rel="stylesheet">
        <link href='<?php echo base_url(); ?>assets/chosen/chosen.min.css' rel="stylesheet">

        <link href='<?php echo base_url(); ?>assets/chosen/docsupport/prism.css' rel="stylesheet">

        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/jquery.min.js"></script>

        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->


        <!--
        Scripting starts here ...-->
        <?php
        $function_name = $this->uri->segment(2);

        $url_array = array("visit");
        $url_profile_array = array("doctor_patient_profile", "pharmacy_patient_profile", "nurse_patient_profile", "lab_patient_profile");
        if (in_array($function_name, $url_profile_array)) {
            ?>
            <script type="text/javascript">
                $(document).ready(function () {


                    var visit_id = $('#checking_visit_id').val();
                    var patient_id = $('#checking_patient_id').val();
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>doctor/get_review_of_systems/" + visit_id + "/" + patient_id,
                        dataType: "json",
                        success: function (response) {

                            $('#constitutional').val(response[0].constitutional);
                            $('#ent').val(response[0].ent);
                            $('#cardio_vascular').val(response[0].cardiovascular);
                            $('#eye').val(response[0].eye);
                            $('#respiratory').val(response[0].respiratory);
                            $('#gastro_intestinal').val(response[0].gastrointestinal);
                            $('#genito_urinary').val(response[0].genitourinary);
                            $('#masculo_skeletal').val(response[0].masculoskeletal);
                            $('#skin').val(response[0].skin);
                            $('#neuro_logic').val(response[0].neurologic);
                            $('#other_systems').val(response[0].other);
                            $('#viist_id').val(response[0].visit_id);
                            $('#patient_id').val(response[0].patient_id);
                        },
                        error: function (data) {

                        }
                    });
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>doctor/get_chief_complaints/" + visit_id + "/" + patient_id,
                        dataType: "json",
                        success: function (response) {

                            $('#chief_complaints_txt_area').val(response[0].complaints);
                            $('#working_diagnosis_txt_area').val(response[0].working_diagnosis);
                            $('#consultation_id').val(response[0].consultation_id);
                            $('#visit_id').val(response[0].visit_id);
                        },
                        error: function (data) {

                        }
                    });
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/patient_allergies/<?php echo $this->uri->segment(3); ?>",
                                            dataType: "JSON",
                                            success: function (patient_allergies) {
                                                patient_allergies_list = $('#patient_allergies_list').empty();
                                                if (patient_allergies_list === null) {
                                                    patient_allergies_list.append("<option> No Active Doctors in the  System</option>");
                                                } else {
                                                    $.each(patient_allergies, function (i, patient_allergiess) {
                                                        if (patient_allergiess.allergies === null) {
                                                            patient_allergies_list.append("<h6>No Allergies</h6>");
                                                        } else {
                                                            if (patient_allergiess.allergies === null) {
                                                                patient_allergies_list.append("<h6>No Allergies</h6>");
                                                            } else {
                                                                patient_allergies_list.append("<h6 class='label-danger'>'" + patient_allergiess.allergies + "'</h6>");
                                                            }
                                                        }

                                                    });
                                                }



                                            },
                                            error: function (data) {
                                                //
                                                //  alert('An error occured, kindly try later');
                                            }
                                        });
                                    }, 3000);
                                });</script>
            <?php
        }
        ?>
        <script type="text/javascript">

            $(document).ready(function () {

                $('#patient_appointments').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#register_patient').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#yes_related_shwari').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 800,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#edit_test_result_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 800,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });


                $('#download_patient_lab_results_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 800,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });


                $('#not_joining').fancybox({
                    paddding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#shwari_relation_yes').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#add_test_results_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 450,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });
                $('#bill_patient_prescription_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 940,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });
                $('#dipsense_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 940,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });
                $('#order_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 940,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });
                $('#lab_send_to_doctor_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 940,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });
                $('#shwari_relation_no').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#visit_records').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#patient_procedure_option').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#regular_patient_yes').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#regular_patient_no').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#patient_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#book_visit_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#export_stock_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 540,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#export_commodity_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 540,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#add_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 800,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#view_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 800,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#edit_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 800,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#delete_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 800,
                    'height': 300,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#export_patient_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#export_vistation_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#export_patient_procedure_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#export_walkin_patient_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#walkin_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#visit_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#regular_procedure_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#walkin_procedure_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#view_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#edit_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#delete_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#patient_medical_records_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                })


                $('#view_procedure_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#edit_procedure_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#delete_procedure_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#view_visit_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#edit_visit_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#delete_visit_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#view_walkin_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#edit_walkin_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#delete_walkin_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                //regular payment link
                $('#regular_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                //lab service payment link
                $('#lab_service_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                //pharmacy service payment link
                $('#pharmacy_service_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                //pay at the ned link 
                $('#pay_at_the_end_patient_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#walkin_nurse_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#walkin_lab_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#release_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#walkin_pharmacy_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#add_triage_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#edit_triage_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#delete_triage_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#nurse_view_patient_list').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#send_to_doctor_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#book_appointment_form_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#test_referral_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#imaging_referral_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#other_referral_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#sick_off_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
            });
            $('.insurance_exists').on('change', function () {
                alert(this.value);
            });
            $("#save_submit").click(function () {

            });
            $("#save_submit_1").click(function () {

            });
            //delegated submit handlers for the forms inside the table
            $('#save_submit_1').on('click', function (e) {
                e.preventDefault();
                //read the form data ans submit it to someurl
                $.post('visit.html', $('#patient_registration_form_1').serialize(), function () {

                    generateAll();
                    setInterval(function () {
                        var url = "<?php echo base_url() ?>index.php/home";
                        $(location).attr('href', url);
                    }, 30000);
                }).fail(function () {
                    //error do something
                    $(".save_timesheet_notify").notify(
                            "There was an error please try again later or  contact the system support desk  for assistance",
                            "error",
                            {position: "left"}
                    );
                });
            });
            function generate(layout) {
                var n = noty({
                    text: 'Do you want to continue?',
                    type: 'alert',
                    dismissQueue: true,
                    layout: layout,
                    theme: 'defaultTheme',
                    buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                                $noty.close();
                                noty({dismissQueue: true, force: true, layout: layout, theme: 'defaultTheme', text: 'You clicked "Ok" button', type: 'success'});
                            }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                                $noty.close();
                                noty({dismissQueue: true, force: true, layout: layout, theme: 'defaultTheme', text: 'You clicked "Cancel" button', type: 'error'});
                            }
                        }
                    ]
                });
                console.log('html: ' + n.options.id);
            }

            function generateAll() {
                generate('center');
            }



        </script>

        <!--
        Scripting ends here
        -->

        <!-- The fav icon -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.ico">

    </head>

    <body>
        <?php
        if (!isset($no_visible_elements) || !$no_visible_elements) {
            if ($this->session->userdata('id')) {
                ?>
                <!-- topbar starts -->



                <div class="navbar navbar-default" role="navigation">

                    <div class="navbar-inner">
                        <button type="button" class="navbar-toggle pull-left animated flip">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url(); ?>home"> <img alt="Shwari Healthcare" src="<?php echo base_url(); ?>assets/img/shwari.jpg" class="hidden-xs"/>
                            <span>Shwari</span></a>

                        <!-- user dropdown starts -->
                        <div class="btn-group pull-right">
                            <?php
                            $type = $this->session->userdata('type');
                            if ($type === "Support") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Reception</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                            } elseif ($type === "Nurse") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Nurse</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                                ?>  <?php
                            } elseif ($type === "Laboratory") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Lab </span>
                                    <span class="caret"></span>
                                </button>
                                <?php ?><?php ?>  <?php
                            } elseif ($type === "Doctor") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Doctor</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                            } elseif ($type === "Pharmacy") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Pharmacy</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                            }
                            ?>
                            <ul class="dropdown-menu">
                                <li><a href="#">Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>home/do_logout">Logout</a></li>
                            </ul>
                        </div>



                    </div>
                </div>
                <!-- topbar ends -->
                <?php
            }
        } else {
            
        }
        ?>
        <div class="ch-container">
            <div class="row">
                <?php
                if (!isset($no_visible_elements) || !$no_visible_elements) {


                    if ($this->session->userdata('id')) {
                        if ($type === "Support") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>
                                            <li><a class="ajax-link" id="register_patient" href="<?php echo base_url(); ?>reception/register_patient"><i class="glyphicon glyphicon-edit"></i><span> Register Patient</span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reception/visit"><i class="glyphicon glyphicon-book"></i><span> Book Visit</span></a></li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reception/walkin_patient"><i class="glyphicon glyphicon-plus-sign"></i><span> Add Walkin</span></a></li>

                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier"><i class="glyphicon glyphicon-check"></i><span> Payments</span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reports/visitation_report"><i class="glyphicon glyphicon-check"></i><span> Edit visitation </span></a>
                                            </li>

                                            <li><a class="ajax-link" id="patient_procedure_option" href="#procedure_option"><i class="glyphicon glyphicon-check"></i><span> Book patient for Procedure </span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier/regular_patient_visit_statement"><i class="glyphicon glyphicon-check"></i><span> Regular Patient Visit Statements </span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier/walkin_patient_visit_statement"><i class="glyphicon glyphicon-check"></i><span> Walk-in Patient Visit Statements  </span></a>
                                            </li>

                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        } elseif ($type === "Nurse") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>



                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>stock/manage_stock"><i class="glyphicon glyphicon-check"></i><span> Stock Management </span></a>
                                            </li>

                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        } elseif ($type === "Laboratory") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>
                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        } elseif ($type === "Doctor") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>


                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        } elseif ($type === "Pharmacy") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>

                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>pharmacy/commodity_management"><i class="glyphicon glyphicon-shopping-cart"></i><span>Commodity Management</span></a></li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>pharmacy/stock_management"><i class="glyphicon glyphicon-shopping-cart"></i><span>Stock Management </span></a></li>


                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        }
                        ?>

                        <!-- left menu starts -->



                        <!--/span-->
                        <!-- left menu ends -->

                        <noscript>
                        <div class="alert alert-block col-md-12">
                            <h4 class="alert-heading">Warning!</h4>

                            <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                                enabled to use this site.</p>
                        </div>
                        </noscript>


























                        <div style="display: none;" class="imaging_referral" id="imaging_referral">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Imaging Request  Form </h2>

                                            <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                        </div>
                                        <div class = "box-content">



                                            <form class="imaging_request_form" id="imaging_request_form" method="post" action="<?php echo base_url(); ?>doctor/imaging_request_form">

                                                <input type="hidden" name="patient_id_imaging" id="patient_id_imaging" class="patient_id_imaging" value="<?php echo $this->uri->segment(3); ?>"/>
                                                <input type="hidden" name="visit_id_imaging" id="visit_id_imaging" class="visit_id_imaging" value="<?php echo $this->uri->segment(4); ?>"/>


                                                <label class="label label-info">Imaging Test : </label>
                                                <textarea name="imaging_test" class="autogrow" id="imaging_test"></textarea>
                                                <hr>
                                                <label class="label label-info">Major Complaints : </label>
                                                <textarea name="major_complaint" id="major_complaint" class="autogrow"></textarea>
                                                <hr>
                                                <label class="label label-info">Diagnosis :</label>
                                                <textarea name="diagnosis" id="diagnosis" class="autogrow"></textarea>
                                                <hr>
                                                <label class="label label-info">Special Instructions : </label>
                                                <textarea name="special_instrations" class="autogrow" id="special_instructions"></textarea>
                                                <hr>
                                                <label class="label label-info" >Referring Doctor's Comments :</label>
                                                <textarea name="referring_doctor_comments" class="autogrow" id=""></textarea>

                                                <hr>
                                                <input type="submit" name="save_request_for_imaging" class="save_request_for_imaging btn btn-xs btn-success" id="save_request_for_imaging" value="Save"/>
                                                <input type="reset" class="btn btn-xs btn-close" value="Cancel"/>


                                            </form>




                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>


















                        <div style="display: none;" class="other_referral" id="other_referral">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i>  Referral  Form </h2>

                                            <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                        </div>
                                        <div class = "box-content">



                                            <form class="referral_request_form" id="referral_request_form" method="post" action="<?php echo base_url(); ?>doctor/patient_referral_form">

                                                <input type="hidden" name="patient_id_other" id="patient_id_imaging" class="patient_id_imaging" value="<?php echo $this->uri->segment(3); ?>"/>
                                                <input type="hidden" name="visit_id_other" id="visit_id_imaging" class="visit_id_imaging" value="<?php echo $this->uri->segment(4); ?>"/>


                                                <label class="label label-info">Referral To : </label>
                                                <textarea name="referral_to" class="autogrow" id="referral_to"></textarea>
                                                <hr>
                                                <label class="label label-info">Major Complaints : </label>
                                                <textarea name="major_complaint" id="major_complaint" class="autogrow"></textarea>
                                                <hr>
                                                <label class="label label-info">Diagnosis :</label>
                                                <textarea name="diagnosis" id="diagnosis" class="autogrow"></textarea>
                                                <hr>
                                                <label class="label label-info">Special Instructions : </label>
                                                <textarea name="special_instrations" class="autogrow" id="special_instructions"></textarea>
                                                <hr>
                                                <label class="label label-info" >Referring Doctor's Comments :</label>
                                                <textarea name="referring_doctor_comments" class="autogrow" id=""></textarea>

                                                <hr>
                                                <input type="submit" name="save_request_for_referral" class="save_request_for_referral btn btn-xs btn-success" id="save_request_for_referral" value="Save"/>
                                                <input type="reset" class="btn btn-xs btn-close" value="Cancel"/>


                                            </form>




                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>


                        <!-- Visitation report filter form start -->

                        <div id="visit_report_filter_form" style="display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Visitation Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/visitation_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="visitation_date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="visitation_date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Patient Name : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="patient_name" class="patient_name" required="" data-rel="">
                                                            <option value="">Please select  Status : </option>
                                                            <option value="All" >All</option>
                                                            <?php foreach ($name as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['patient_id']; ?>"><?php echo $value['title'] . ':' . $value['f_name'] . $value['s_name'] . $value['other_name']; ?></option><?php }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Package Type  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="package_type" id="package_type" required="" data-rel="">
                                                            <option value="">Please select  Package Type : </option>
                                                            <option value="All" >All</option>
                                                            <?php foreach ($pack as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['package_id']; ?>"><?php echo $value['package_name']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                        </div>
                        <!-- Visitation report filter form end -->

                        <!-- Patient report filter form start -->

                        <div id = "patient_report_filter_form" style = "display: none;">

                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Patient Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/patient_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Patient Status : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="patient_status" class="patient_status" required="" data-rel="">
                                                            <option value="">Please select  Status : </option>
                                                            <option value="All">All</option>
                                                            <option value="Active">Active</option>
                                                            <option value="In Active">In Active</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Gender  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="gender" id="gender" required="" data-rel="">
                                                            <option value="">Please select  Gender : </option>
                                                            <option value="All">All</option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                            <option value="transgender">Trans-Gender</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>


                        </div>

                        <!-- Patient report filter form end -->





                        <div style="display: none;" class="sick_off" id="sick_off">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i>  Referral  Form </h2>

                                            <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                        </div>
                                        <div class = "box-content">



                                            <form class="sick_off_form" id="sick_off_form" method="post" action="<?php echo base_url(); ?>doctor/sick_off_form">

                                                <input type="hidden" name="patient_id_sick_off" id="patient_id_imaging" class="patient_id_imaging" value="<?php echo $this->uri->segment(3); ?>"/>
                                                <input type="hidden" name="visit_id_sick_off" id="visit_id_imaging" class="visit_id_imaging" value="<?php echo $this->uri->segment(4); ?>"/>


                                                <label class="label label-info">Date from  : </label>
                                                <input type="text" name="sick_off_date_from" id="sick_off_date_from" class="sick_off_date_from" placeholder="YYYY-MM-DD"/>
                                                <hr>
                                                <label class="label label-info">Date To  : </label>
                                                <input type="text" name="sick_off_date_to" id="sick_off_date_to" class="sick_off_date_to" placeholder="YYYY-MM-DD"/>
                                                <hr>

                                                <input type="submit" name="save_sick_off" class="save_sick_off btn btn-xs btn-success" id="save_sick_off" value="Save Sick Off"/>
                                                <input type="reset" class="btn btn-xs btn-close" value="Cancel"/>


                                            </form>




                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>

























                        <?php
                        $segment_1 = $this->uri->segment(1);

                        $profile_array = array("doctor", "nurse", "lab", "pharmacy");
                        if (in_array($segment_1, $profile_array)) {
                            ?>

                            <div style="display: none;" class="test_refferal" id="test_refferal" >


                                <div class = "box-content">



                                    <form class="order_tests_form" id="order_tests_form" method="post" >



                                        <label class="control-label" for="selectError1">Tests</label>
                                        <div class="alert alert-info">
                                            (hold "Ctrl" key to select multiple):
                                        </div>


                                        <div class="controls">
                                            <select name="tests[]" class="select test_order" id="test_order" required="" multiple="multiple" size=10 style='height: 50%;'>

                                                <?php foreach ($tests as $test_data) {
                                                    ?>
                                                    <option value="<?php echo $test_data['test_id']; ?>">
                                                        <?php
                                                        echo $test_data['test_name'];
                                                        ?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>

                                            </select>
                                        </div>
                                        <input type="hidden" name="patient_id_lab" id="patient_id_lab" class="patient_id_lab" value="<?php echo $this->uri->segment(3); ?>"/>
                                        <input type="hidden" name="visit_id_lab" id="visit_id_lab" class="visit_id_lab" value="<?php echo $this->uri->segment(4); ?>"/>

                                        <hr>
                                        <input type="submit" value="Order Lab Tests" class="btn btn-xs btn-info order_tests" id="order_tests"/>
                                    </form>
                                </div>
                            </div>


                            <?php
                        }
                        ?>








                        <div id="content" class="col-lg-10 col-sm-10">
                            <!-- content starts -->
                            <?php
                        }
                    }
                    ?>


                    <?php $this->load->view($contents); ?>





                    <?php if (!isset($no_visible_elements) || !$no_visible_elements) { ?>
                        <!-- content ends -->
                    </div><!--/#content.col-md-0-->
                <?php } ?>
            </div><!--/fluid-row-->
            <?php if (!isset($no_visible_elements) || !$no_visible_elements) { ?>


                <hr>

                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">

                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h3>Settings</h3>
                            </div>
                            <div class="modal-body">
                                <p>Here settings can be configured...</p>
                            </div>
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                                <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                            </div>
                        </div>date
                    </div>
                </div>

                <footer class="row">
                    <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://healthtechkenya.com" target="_blank">Health Tech Kenya
                        </a> 2012 - <?php echo date('Y') ?></p>


                </footer>
            <?php } ?>

        </div><!--/.fluid-container-->

        <!-- external javascript -->

        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- library for cookie management -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.cookie.js"></script>



        <!-- calender plugin -->
        <script src='<?php echo base_url(); ?>assets/bower_components/moment/min/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>



        <!-- star rating plugin -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.raty.min.js"></script>
        <!-- data table plugin -->
        <script src='<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js'></script>

        <!-- select or dropdown enhancer -->
        <script src="<?php echo base_url(); ?>assets/bower_components/chosen/chosen.jquery.min.js"></script>
        <!-- plugin for gallery image view -->
        <script src="<?php echo base_url(); ?>assets/bower_components/colorbox/jquery.colorbox-min.js"></script>

        <!-- library for making tables responsive -->
        <script src="<?php echo base_url(); ?>assets/bower_components/responsive-tables/responsive-tables.js"></script>
        <!-- tour plugin -->
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>

        <!-- for iOS style toggle switch -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.iphone.toggle.js"></script>
        <!-- autogrowing textarea plugin -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.autogrow-textarea.js"></script>
        <!-- multiple file upload plugin -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.uploadify-3.1.min.js"></script>
        <!-- history.js for cross-browser state change on ajax -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.history.js"></script>

        <!-- application script for Charisma demo -->
        <script src="<?php echo base_url(); ?>assets/js/charisma.js"></script>






        <!-- Add mousewheel plugin (this is optional) -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/lib/jquery.mousewheel-3.0.6.pack.js"></script>

        <!-- Add fancyBox -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.pack.js?v=2.1.4"></script>

        <!-- Optionally add helpers - button, thumbnail and/or media -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>


        <script type="text/javascript" href="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" href="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/notify.js"></script>







        <div style="display: none !important;">
            <div class="box-content ajax_loader" id="ajax_loader">
                <ul class="ajax-loaders">

                    <img src="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" title="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" >
                    </br>
                    <br/>

                    <span class=" loader_notify clearfix" id="loader_notify">
                    </span>  

                </ul>


            </div>

        </div>

        <script type="text/javascript">


            function add_vital_signs() {

                dataString = $("#add_patient_triage_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/nurse/add_triage",
                    data: dataString,
                    success: function (data) {

                    }

                });
                event.preventDefault();
                return false;
            }


            function order_tests_form() {

                alert('Order made sucessfully');
                dataString = $("#order_tests_form").serialize();
                $.fancybox.open([
                    {
                        href: '#ajax_loader',
                        title: 'Please wait information being updated...'
                    }
                ], {
                    padding: 0
                });
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/doctor/order_lab_tests",
                    data: dataString,
                    success: function (data) {
                        $(".loader_notify").notify(
                                "Lab Orders Successfull",
                                "success",
                                {position: "center"}
                        );
                        setInterval(function () {
                            var url = "<?php echo base_url() ?>home";
                            $(location).attr('href', url);
                        }, 300000);
                        alert('Order made sucessfully');
                    }

                });
                event.preventDefault();
                return false;
            }

            function reason_for_visit() {

                dataString = $("#reason_for_visit_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/nurse/add_reason_for_visit",
                    data: dataString,
                    success: function (data) {

                    }

                });
                event.preventDefault();
                return false;
            }


            function add_lab_test_results() {

                dataString = $("#patient_test_results_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/lab/add_lab_test_results",
                    data: dataString,
                    success: function (data) {

                    }

                });
                event.preventDefault();
                return false;
            }

            function add_allergy() {




                dataString = $("#add_allergy_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>nurse/add_allergy",
                    data: dataString,
                    success: function (data) {

                    }

                });
                event.preventDefault();
                return false;
            }

            function add_social_history() {




                dataString = $("#add_family_social_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>nurse/add_social_history",
                    data: dataString,
                    success: function (data) {

                    }

                });
                event.preventDefault();
                return false;
            }

            function add_immunization() {

                dataString = $("#add_immunization_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>nurse/add_immunization",
                    data: dataString,
                    success: function (data) {

                    }

                });
                event.preventDefault();
                return false;
            }

            function book_appointment() {

                dataString = $("#appointment_form_book").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/nurse/book_appointment",
                    data: dataString,
                    success: function (data) {

                    }

                });
                event.preventDefault();
                return false;
            }
            function chief_complaints() {

                dataString = $("#chief_complaints_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/doctor/chief_complaints",
                    data: dataString,
                    success: function (data) {

                    }

                });
                event.preventDefault();
                return false;
            }
            function review_of_systems() {

                dataString = $("#review_of_systems_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/doctor/review_of_systems",
                    data: dataString,
                    success: function (data) {

                    }

                });
                event.preventDefault();
                return false;
            }

            function working_diagnosis() {

                dataString = $("#working_diagnosis_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/doctor/working_diagnosis",
                    data: dataString,
                    success: function (data) {

                    }

                });
                event.preventDefault();
                return false;
            }

            function icd_10_code() {

                dataString = $("#patient_icd_10_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/doctor/patient_icd_10",
                    data: dataString,
                    success: function (data) {

                    }

                });
                event.preventDefault();
                return false;
            }




            function add_walkin_lab_results_notes() {

                dataString = $("#add_walkin_test_notes_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/lab/add_walkin_lab_results_notes",
                    data: dataString,
                    success: function (data) {

                    }

                });
                event.preventDefault();
                return false;
            }




        </script>
        <script type="text/javascript">
            $(document).ready(function () {



                var stock_management = window.location.href.indexOf("stock_management") > -1;
                if (stock_management) {

                    $('.add_commodity_name').on('change', function () {

                        var commodity_id = this.value;
                        $(".div_1_hide").hide('slow');
                        $(".div_2_hide").hide('slow');
                        $('.add_commodity_type').val("");
                        $('.add_commodity_code').val("");
                        $('.add_supplier_name').val("");
                        $('.add_no_of_packs').val("");
                        $('.add_unit_per_pack').val("");
                        $('.add_total_quantity').val("");
                        $('.add_buying_price').val("");
                        $('.add_has_expired').val("");
                        $('.add_remarks').val("");
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_commodity_info/" + commodity_id,
                            dataType: "json",
                            success: function (response) {

                                $('.add_commodity_code').val(response[0].code);
                                $('.add_commodity_type').val(response[0].commodity_type);
                                $(".div_1_hide").show('slow');
                                $(".div_2_hide").show('slow');
                            },
                            error: function (data) {
                                $(".add_new_stock_form").blur();
                            }
                        });
                    });
                    $(".add_no_of_packs").keyup(function () {


                        //Get
                        var no_of_packs = $('#add_no_of_packs').val();
                        var no_of_packs = parseFloat(no_of_packs);
                        var unit_per_pack = $('#add_unit_per_pack').val();
                        var unit_per_pack = parseFloat(unit_per_pack);
                        var total_quantity = no_of_packs * unit_per_pack;
                        //Set
                        $('.add_total_quantity').val(total_quantity);
                    });
                    $(".add_unit_per_pack").keyup(function () {

                        //Get
                        var no_of_packs = $('#add_no_of_packs').val();
                        var no_of_packs = parseFloat(no_of_packs);
                        var unit_per_pack = $('#add_unit_per_pack').val();
                        var unit_per_pack = parseFloat(unit_per_pack);
                        var total_quantity = no_of_packs * unit_per_pack;
                        //Set
                        $('.add_total_quantity').val(total_quantity);
                    });
                    $('.edit_commodity_name').on('change', function () {

                        var commodity_id = this.value;
                        $('.info_loader').show('slow');
                        $(".div_1_hide").hide('slow');
                        $(".div_2_hide").hide('slow');
                        $('.edit_commodity_type').val("");
                        $('.edit_commodity_code').val("");
                        $('.edit_supplier_name').val("");
                        $('.edit_no_of_packs').val("");
                        $('.edit_unit_per_pack').val("");
                        $('.edit_total_quantity').val("");
                        $('.edit_buying_price').val("");
                        $('.edit_has_expired').val("");
                        $('.edit_remarks').val("");
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_commodity_info/" + commodity_id,
                            dataType: "json",
                            success: function (response) {

                                $('.edit_commodity_code').val(response[0].code);
                                $('.edit_commodity_type').val(response[0].commodity_type);
                                $('.info_loader').hide('slow');
                                $(".div_1_hide").show('slow');
                                $(".div_2_hide").show('slow');
                            },
                            error: function (data) {
                                $(".edit_new_stock_form").blur();
                            }
                        });
                    });
                    $(".edit_no_of_packs").keyup(function () {


                        //Get
                        var no_of_packs = $('#edit_no_of_packs').val();
                        var no_of_packs = parseFloat(no_of_packs);
                        var unit_per_pack = $('#edit_unit_per_pack').val();
                        var unit_per_pack = parseFloat(unit_per_pack);
                        var total_quantity = no_of_packs * unit_per_pack;
                        //Set
                        $('.edit_total_quantity').val(total_quantity);
                    });
                    $(".edit_unit_per_pack").keyup(function () {

                        //Get
                        var no_of_packs = $('#edit_no_of_packs').val();
                        var no_of_packs = parseFloat(no_of_packs);
                        var unit_per_pack = $('#edit_unit_per_pack').val();
                        var unit_per_pack = parseFloat(unit_per_pack);
                        var total_quantity = no_of_packs * unit_per_pack;
                        //Set
                        $('.edit_total_quantity').val(total_quantity);
                    });
                    $('#add_new_stock_form').submit(function (event) {
                        dataString = $("#add_new_stock_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reports/add_new_stock",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "New Stock Added Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#edit_stock_form').submit(function (event) {
                        dataString = $("#edit_stock_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please $ait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reports/edit_stock_info",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        " Stock Updated Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#delete_stock_form').submit(function (event) {
                        dataString = $("#delete_stock_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please $ait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reports/delete_stock_info",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        " Stock Deleted Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                }


            });</script>

        <script type="text/javascript">
            $(document).ready(function () {

                $('#send_to_pharmacy_form').submit(function (event) {
                    dataString = $("#send_to_pharmacy_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/nurse/send_to_pharmacy",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Patient Sent to Pharmacy Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = "<?php echo base_url() ?>home";
                                $(location).attr('href', url);
                            }, 300000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
                $('#order_tests_form').submit(function (event) {


                    // order_tests_form();

                    dataString = $("#order_tests_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/doctor/order_lab_tests",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Lab Orders  made Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = "<?php echo base_url() ?>home";
                                $(location).attr('href', url);
                            }, 3000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
                $('#order_commodity_form').submit(function (event) {


                    // order_tests_form();

                    dataString = $("#order_commodity_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/doctor/order_commodity",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    " Order made Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = window.location.path;
                                $(location).attr('href', url);
                            }, 300000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
            });</script>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#inputquantity_billed").keyup(function () {

                    var inputQuantityavailable = $('#inputQuantityavailable').val();
                    var inputsellingprice = $('#inputsellingprice').val();
                    var inputquantity_billed = $('#inputquantity_billed').val();
                    var inputQuantityavailable = parseInt(inputQuantityavailable);
                    var inputsellingprice = parseInt(inputsellingprice);
                    var inputquantity_billed = parseInt(inputquantity_billed);
                    var total_cost = inputsellingprice * inputquantity_billed;
                    $('#input_total_cost').val(total_cost);
                    if (inputquantity_billed > inputQuantityavailable) {
                        alert("You cannot bill a patient for more than what is available in Stock.");
                        $(this).val('');
                        $(this).focus();
                    }
                });
                $('#bill_commodity_name').on('change', function () {

                    var commodity_id = this.value;
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>pharmacy/get_available_commodity/" + commodity_id,
                        dataType: "json",
                        success: function (response) {

                            $('#inputstrength').val(response[0].strength);
                            $('#inputtransaction_id').val(response[0].transaction_id);
                            $('#transaction_total_quantiy').val(response[0].transaction_total_quantity);
                            $('#inputQuantityavailable').val(response[0].available_quantity);
                            $('#inputsellingprice').val(response[0].selling_price);
                            var available_quantity_in_stock = response[0].available_quantity;
                            if (available_quantity_in_stock !== null) {
                                $("#bill_commodity_info").show("slow");
                            } else {
                                alert("You cannot issue a commodity with empty quantity. ");
                                $("#bill_commodity_info").hide("slow");
                            }



                        },
                        error: function (data) {

                        }
                    });
                });
                $('#bill_patient_form').submit(function (event) {
                    dataString = $("#bill_patient_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/pharmacy/bill_patient",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Patient Billed Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = window.location.href;
                                $(location).attr('href', url);
                            }, 3000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
                $('#release_patient_form').submit(function (event) {
                    dataString = $("#release_patient_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/doctor/release_patient",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Patient Released from the Queue Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = "<?php echo base_url() ?>home";
                                $(location).attr('href', url);
                            }, 3000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
                $('#send_to_current_doctor_form').submit(function (event) {
                    dataString = $("#send_to_current_doctor_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/nurse/send_to_doctor",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Patient Sent to Doctor Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = "<?php echo base_url() ?>home";
                                $(location).attr('href', url);
                            }, 3000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
                $('#send_to_doctor_form').submit(function (event) {
                    dataString = $("#send_to_doctor_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/nurse/send_to_doctor",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Patient Sent to Doctor Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = "<?php echo base_url() ?>home";
                                $(location).attr('href', url);
                            }, 3000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
                $('#send_to_pharmacy_form').submit(function (event) {
                    dataString = $("#send_to_pharmacy_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/nurse/send_to_pharmacy",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Patient Sent to Pharmacy Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = "<?php echo base_url() ?>home";
                                $(location).attr('href', url);
                            }, 300000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
            });</script>
        <script type="text/javascript">
            $(document).ready(function () {

                $('#working_diagnosis').keyup(function () {
                    working_diagnosis();
                });
                $(".icd10_selector").change(function () {
                    icd_10_code();
                });
                $("#constitutional").keyup(function () {

                    review_of_systems();
                });
                $("#ent").keyup(function () {
                    review_of_systems();
                });
                $("#cardio_vascular").keyup(function () {
                    review_of_systems();
                });
                $("#eye").keyup(function () {
                    review_of_systems();
                });
                $("#respiratory").keyup(function () {
                    review_of_systems();
                });
                $("#gastro_intestinal").keyup(function () {
                    review_of_systems();
                });
                $("#genito_urinary").keyup(function () {
                    review_of_systems();
                });
                $("#masculo_skeletal").keyup(function () {
                    review_of_systems();
                });
                $("#skin").keyup(function () {
                    review_of_systems();
                });
                $("#neuro_logic").keyup(function () {
                    review_of_systems();
                });
                $("#other_systems").keyup(function () {
                    review_of_systems();
                });
                $("#chief_complaints").keyup(function () {

                    chief_complaints();
                });
                $("#reason_for_visit").keyup(function () {
                    reason_for_visit();
                });
                $("#Emergency_0").change(function () {
                    reason_for_visit();
                });
            });</script>
        <script type="text/javascript">
            $(document).ready(function () {
                var reception_url = window.location.href.indexOf("reception") > -1;
                if (reception_url) {






                    var reception_index_url = window.location.href.indexOf("reception") > 1;
                    if (reception_index_url) {


                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>nurse/active_doctors_list",
                                dataType: "JSON",
                                success: function (active_doctors_list) {
                                    send_to_doctor_name = $('#send_to_doctor_name').empty();
                                    if (send_to_doctor_name === null) {
                                        send_to_doctor_name.append("<option> No Active Doctors in the  System</option>");
                                    } else {
                                        $.each(active_doctors_list, function (i, active_doctors_lists) {
                                            if (active_doctors_lists.employee_id === null) {
                                                send_to_doctor_name.append("<option> No Active Doctors in the  System</option>");
                                            } else {
                                                if (active_doctors_lists.employee_id === null) {
                                                    send_to_doctor_name.append("<option> No Active Doctors in the  System</option>");
                                                } else {
                                                    send_to_doctor_name.append('<option value="' + active_doctors_lists.employee_id + '">' + active_doctors_lists.employee_name + '</option>');
                                                }
                                            }

                                        });
                                    }



                                },
                                error: function (data) {
                                    //
                                    //  alert('An error occured, kindly try later');
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/get_active_doctors/",
                                dataType: "JSON",
                                success: function (active_doctors) {
                                    active_doctor = $('#active_doctor_list_1').empty();
                                    if (active_doctor === null) {
                                        active_doctor.append("<option>No  Active Doctor</opton>");
                                    }
                                    $.each(active_doctors, function (i, active_doctorss) {
                                        active_doctor.append('<option value="' + active_doctorss.employee_id + '">' + active_doctorss.employee_name + '</option>');
                                    });
                                },
                                error: function (data) {
                                    //
                                    //  alert('An error occured, kindly try later');
                                }
                            });
                        }, 2000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/total_visits",
                                dataType: "JSON",
                                success: function (total_visits) {
                                    total_visit = $('#total_regular_visits').empty();
                                    if (total_visit === null) {
                                        total_visit.append("<option>No Visits</opton>");
                                    } else {
                                        $.each(total_visits, function (i, total_visitss) {
                                            if (total_visitss.total_patient_visits === null) {
                                                total_visit.append("<p>No Visits</p>");
                                            } else {
                                                total_visit.append('<p>' + total_visitss.total_patient_visits + '</p>');
                                            }

                                        });
                                    }



                                },
                                error: function (data) {
                                    //
                                    //  alert('An error occured, kindly try later');
                                }
                            });
                        }, 3000);
                        //reception
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/total_walkin_patients",
                                dataType: "JSON",
                                success: function (walkin_visits) {
                                    walkin_visit = $('#total_walkin_patient_visits').empty();
                                    if (walkin_visit === null) {
                                        walkin_visit.append("<p>No Walkin Visits</p>");
                                    } else {
                                        $.each(walkin_visits, function (i, walkin_visitss) {

                                            if (walkin_visitss.total_walkin_patient === null) {
                                                walkin_visit.append("<p>No Walkin Visits</p>");
                                            } else {
                                                walkin_visit.append('<p>' + walkin_visitss.total_walkin_patient + '</p>');
                                            }

                                        });
                                    }


                                },
                                error: function (data) {
                                    //
                                    //  alert('An error occured, kindly try later');
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/seeappointments",
                                dataType: "JSON",
                                success: function (appointed) {
                                    appointment_list = $('#patient_appointments').empty();
                                    if (appointed === null) {


                                    }
                                    else {

                                        $.each(appointed, function (i, appointment) {

                                            if (appointment.urgency === "urgent") {
                                                appointment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + appointment.f_name + " " + appointment.s_name + " " + appointment.other_name + '</a></li><span style="color:red !important;">' + appointment.urgency + '</span>');
                                            }
                                            else if (appointment_list.urgency !== "urgent")
                                            {
                                                appointment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + appointment.f_name + " " + appointment.s_name + " " + appointment.other_name + '</li>');
                                            }
                                            else {
                                                appointment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                            }


                                        });
                                    }

                                },
                                error: function (data) {

                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/to_paywalkin",
                                dataType: "JSON",
                                success: function (to_paywalkin) {
                                    to_paywalkin_list = $('#walk_in_patient_payments').empty();
                                    if (to_paywalkin === null) {

                                    } else {
                                        $.each(to_paywalkin, function (i, to_paywalkin) {

                                            if (to_paywalkin.urgency === "urgent") {
                                                to_paywalkin_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + to_paywalkin.fname + " " + to_paywalkin.lname + " " + to_paywalkin.sname + '</a><span style="color:red !important;">' + to_paywalkin.urgency + '</span></li>');
                                            }
                                            else if (to_paywalkin.urgency !== "urgent")
                                            {
                                                to_paywalkin_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + to_paywalkin.walkin_patient_name + " " + to_paywalkin.faculty + " " + to_paywalkin.walkin_payments_total + '</a></li>');
                                            }
                                            else {
                                                to_paywalkin_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                            }


                                        });
                                    }


                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/pat_payment",
                                dataType: "JSON",
                                success: function (payment_list) {
                                    pat_payment_list = $('#regular_patients_in_traypayments').empty();
                                    if (payment_list === null) {

                                    } else {
                                        $.each(payment_list, function (i, payment_list) {

                                            if (payment_list.urgency === "urgent") {
                                                if (payment_list.charge_followup === "Yes") {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.urgency + '</span> Kshs :<span style="color:red !important;">' + payment_list.total_cost + '</span>(Follow Up)</li></br>');

                                                } else {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.urgency + '</span> Kshs :<span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');

                                                }
                                            }
                                            else if (payment_list.urgency !== "urgent")
                                            {
                                                if (payment_list.charge_followup === "Yes") {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a> Kshs :<span style="color:red !important;">' + payment_list.total_cost + '</span>(Follow Up)</li></br>');

                                                } else {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a> Kshs :<span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');

                                                }
                                            }
                                            else {
                                                if (payment_list.charge_followup === "Yes") {
                                                    pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");

                                                } else {
                                                    pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");

                                                }
                                                pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                            }

                                        });
                                    }


                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/active_doctors",
                                dataType: "JSON",
                                success: function (doctor_list) {
                                    active_doctor_list = $('#active_doctor_list').empty();
                                    if (doctor_list === null) {
                                        active_doctor_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Doctor</a> </li>");
                                    } else {
                                        $.each(doctor_list, function (i, doctor_list) {


                                            active_doctor_list.append('<li><a class="doctor" href="#active_patients_in_doctor"><i class = "glyphicon glyphicon-user"></i>' + doctor_list.user_name + '</a></li>');
                                            var doctor_id = doctor_list.employee_id;
                                            $.ajax({
                                                type: "GET",
                                                url: "<?php echo base_url(); ?>reception/d_active/" + doctor_id,
                                                dataType: "JSON",
                                                success: function (people) {
                                                    waiting_list = $('#waiting_list').empty();
                                                    if (people === null) {


                                                        waiting_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                    } else {
                                                        $.each(people, function (i, waiting) {

                                                            if (waiting.urgency === "urgent") {
                                                                waiting_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + waiting.f_name + " " + waiting.other_name + " " + waiting.s_name + '<span style="color:red !important;">' + waiting.urgency + '</span></li>');
                                                            }
                                                            else if (waiting_list.urgency !== "urgent") {
                                                                waiting_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + waiting.f_name + " " + waiting.other_name + " " + waiting.s_name + '</a></li>');
                                                            }


                                                        });
                                                    }

                                                },
                                                error: function (data) {
                                                    //error do something
                                                }
                                            });
                                        });
                                    }


                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/active_patient_doctor",
                                dataType: "JSON",
                                success: function (patient_doctor_list) {
                                    active_patient_doctor_list = $('#active_patient_visit_doctor').empty();
                                    $.each(patient_doctor_list, function (i, patient_doctor_list) {

                                        if (patient_doctor_list.urgency === "urgent") {
                                            active_patient_doctor_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + patient_doctor_list.f_name + " " + patient_doctor_list.other_name + " " + patient_doctor_list.s_name + '</a><span style="color:red !important;">' + patient_doctor_list.urgency + '</span></li>');
                                        }
                                        else if (patient_doctor_list.urgency !== "urgent")
                                        {
                                            active_patient_doctor_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + patient_doctor_list.f_name + " " + patient_doctor_list.other_name + " " + patient_doctor_list.s_name + '</a></li>');
                                        }
                                        else {
                                            active_patient_doctor_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                        }

                                    });
                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/active_nurses",
                                dataType: "JSON",
                                success: function (nurse_list) {
                                    active_nurse_list = $('#active_nurses').empty();
                                    if (nurse_list === null) {
                                        active_nurse_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Nurse</a> </li>");
                                    } else {
                                        $.each(nurse_list, function (i, nurse_list) {


                                            active_nurse_list.append('<li><i class = "glyphicon glyphicon-user"></i><a class="nurse" href="#active_patients_in_nurse">' + nurse_list.user_name + '</a></li>');
                                            var nurse_id = nurse_list.employee_id;
                                            $.ajax({
                                                type: "GET",
                                                url: "<?php echo base_url(); ?>reception/load_data/" + nurse_id,
                                                dataType: "JSON",
                                                success: function (nurse) {
                                                    nurse_list = $('#nurse_list').empty();
                                                    if (nurse === null) {

                                                        nurse_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                    }
                                                    else {
                                                        $.each(nurse, function (i, nurse) {

                                                            if (nurse.urgency === "urgent") {
                                                                nurse_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + nurse.f_name + " " + nurse.other_name + " " + nurse.s_name + '<span style="color:red !important;">' + nurse.urgency + '</span></li>');
                                                            }
                                                            else if (nurse.urgency !== "urgent")
                                                            {
                                                                nurse_list.append('<li><i class = "glyphicon glyphicon-user"></i>' + nurse.f_name + " " + nurse.other_name + " " + nurse.s_name + '</li>');
                                                            }
                                                            else {
                                                                nurse_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                            }
                                                        });
                                                    }

                                                },
                                                error: function (data) {
                                                    //error do something                          
                                                }
                                            });
                                        });
                                    }
                                },
                                error: function (data) {
                                    //error do something            
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/active_laboratories",
                                dataType: "JSON",
                                success: function (laboratory_list) {
                                    active_laboratory_list = $('#active_laboratories').empty();
                                    if (laboratory_list === null) {
                                        active_laboratory_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Lab Tech</a> </li>");
                                    } else {
                                        $.each(laboratory_list, function (i, laboratory_list) {

                                            active_laboratory_list.append('<li><a class="laboratorist" href=""><i class="glyphicon glyphicon-user"></i>' + laboratory_list.user_name + '</a></li>');
                                            var laboratorist_id = laboratory_list.employee_id;
                                            $.ajax({
                                                type: "GET",
                                                url: "<?php echo base_url(); ?>reception/l_active/" + laboratorist_id,
                                                dataType: "JSON",
                                                success: function (laboratory) {
                                                    laboratory_list = $('#lab_list').empty();
                                                    if (laboratory === null) {
                                                        laboratory_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                    } else {
                                                        $.each(laboratory, function (i, laboratory) {
                                                            if (laboratory.urgency === "urgent") {
                                                                laboratory_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + laboratory.f_name + " " + laboratory.other_name + " " + laboratory.s_name + '</ul></a><span style="color:red !important;">' + pharm.urgency + '</span></br></ul>');
                                                            }
                                                            else if (laboratory.urgency !== "urgent")
                                                            {
                                                                laboratory_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + laboratory.f_name + " " + laboratory.other_name + " " + laboratory.s_name + '</br></ul>');
                                                            }
                                                            else {
                                                                laboratory_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                            }
                                                        });
                                                    }

                                                },
                                                error: function (data) {
                                                    //error do something
                                                }
                                            });
                                        });
                                    }

                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/active_pharmacist",
                                dataType: "JSON",
                                success: function (pharm_list) {
                                    active_pharm_list = $('#active_pharm_list').empty();
                                    if (pharm_list === null) {
                                        active_pharm_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Pharm Tech</a> </li>");
                                    } else {
                                        $.each(pharm_list, function (i, pharm_list) {

                                            active_pharm_list.append('<a class="pharmacist"  href=""><i class="glyphicon glyphicon-user"></i>' + pharm_list.user_name + '</a></br>');
                                            var pharmacist_id = pharm_list.employee_id;
                                            $.ajax({
                                                type: "GET",
                                                url: "<?php echo base_url(); ?>reception/p_active/" + pharmacist_id,
                                                dataType: "JSON",
                                                success: function (pharm) {
                                                    pharm_list = $('#pharm_list').empty();
                                                    if (pharm === null) {
                                                        pharm_list.append("<ul>No Active Patient</ul>");
                                                    } else {
                                                        $.each(pharm, function (i, pharm) {
                                                            if (pharm.urgency === "urgent") {
                                                                pharm_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + pharm.f_name + " " + pharm.other_name + " " + pharm.s_name + '</ul></a></br></ul>');
                                                            }
                                                            else if (pharm.urgency !== "urgent")
                                                            {
                                                                pharm_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + pharm.f_name + " " + pharm.other_name + " " + pharm.s_name + '</br></ul>');
                                                            }
                                                            else {
                                                                pharm_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                            }
                                                        });
                                                    }

                                                },
                                                error: function (data) {
                                                    //error do something
                                                }
                                            });
                                        });
                                    }

                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        }, 3000);



                    } else {
                        alert('Not found .....');
                    }


























                    //reception
                    $('#new_family_registration_form').submit(function (event) {
                        dataString = $("#new_family_registration_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reception/new_family_tree",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Details saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = "<?php echo base_url(); ?>index.php/reception/reception/newpatvisit";
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('.employmentstatus_yes').click(function () {

                        // $("#txtAge").toggle(this.checked);
                        $('.employers_name').css('display', 'inline');
                    });
                    $('.employmentstatus_no').click(function () {

                        // $("#txtAge").toggle(this.checked);
                        $('.employers_name').val('');
                        $('.employers_name').css('display', 'none');
                    });
                    $('.employmentstatus_yes_1').click(function () {
                        // $("#txtAge").toggle(this.checked);
                        $('.employers_name_1').css('display', 'inline');
                    });
                    $('.employmentstatus_no_1').click(function () {
                        // $("#txtAge").toggle(this.checked);
                        $('.employers_name_1').val('');
                        $('.employers_name_1').css('display', 'none');
                    });
                    $('#register_new_patient_form').submit(function (event) {
                        dataString = $("#register_new_patient_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reception/add",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Details saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = "<?php echo base_url(); ?>index.php/reception/reception/newpatvisit";
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    //reception
                    $('#add_visit_form').submit(function (event) {
                        dataString = $("#add_visit_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reception/new_visit",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "New Visit Added Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                $('.patient_visit_name').prop('selectedIndex', 0);
                                $('.pay_at_the_end').val("");
                                $('.queue_to_join').empty();
                                $('.visit_date_start').val("");
                                $('.visit_date_end').val("");
                                $('.active_doctor_list_1').val("");
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#inputShwariFamilyNumber').keyup(function () {
                        //get

                        member_search = $('#inputShwariFamilyNumber').val();
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reception/check_shari_member_existence/" + member_search,
                            dataType: "json",
                            success: function (response) {
                                shwari_family_number_result = $('#shwari_family_number_result').empty();
                                $.each(response, function (i, response) {

                                    var str = response.family_base_number;
                                    var res = str.substring(0, 14);
                                    shwari_family_number_result.append('<span>' + res + '</span></br>');
                                });
                            },
                            error: function (data) {

                            }
                        });
                    });
                    $('#employmentstatus_yes').click(function () {
                        // $("#txtAge").toggle(this.checked);
                        $('#employer_1').css('display', 'inline');
                    });
                    $('#employmentstatus_no').click(function () {
                        // $("#txtAge").toggle(this.checked);
                        $('#employer_1').css('display', 'none');
                    });
                    $('#add_walkin_patient_form').submit(function (event) {
                        dataString = $("#add_walkin_patient_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reception/add_walkin",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "New Walkin Patient Added Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#book_patient_procedure_form').submit(function (event) {
                        dataString = $("#book_patient_procedure_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reception/add_patient_procedure",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Booked for Procedure",
                                        "success",
                                        {position: "center"}
                                );
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#shwari_patient_book_procedure_form').submit(function (event) {
                        dataString = $("#shwari_patient_book_procedure_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reception/add_procedure",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Booked for Procedure",
                                        "success",
                                        {position: "center"}
                                );
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                }
            });</script>



        <!---RePORTS PROFILE START  -->
        <script type="text/javascript">

            $(document).ready(function () {
                var reports_url = window.location.href.indexOf("reports") > -1;
                if (reports_url) {

                    //reports
                    $('#edit_patient_information_form').submit(function (event) {
                        dataString = $("#edit_patient_information_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reports/edit_patient_info",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Details saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#edit_visitation_info').submit(function (event) {
                        dataString = $("#edit_visitation_info").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reports/edit_visitation_info",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Visitation Details updated Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#delete_visit_information_form').submit(function (event) {
                        dataString = $("#delete_visit_information_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reports/delete_visitation_info",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Vistation Details updated Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    //reports
                    $('.delete_patient_information_form').submit(function (event) {
                        dataString = $(".delete_patient_information_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>reports/delete_patient",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Record Deleted Succesfully ",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('.edit_patient_information_form').submit(function (event) {
                        dataString = $(".edit_patient_information_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>reports/edit_patient",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Record Edited Succesfully ",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('.edit_walkin_patient_info').submit(function (event) {
                        dataString = $(".edit_walkin_patient_info").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>reports/edit_walkin_patient",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Walkin Patient Details Updated Succesfully ",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('.delete_walkin_patient_information_form').submit(function (event) {
                        dataString = $(".delete_walkin_patient_information_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>reports/delete_walkin_patient",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Walkin Patient Details Updated Succesfully ",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('.edit_procedure_visit_info').submit(function (event) {
                        dataString = $(".edit_procedure_visit_info").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>reports/edit_procedure_visit_data",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        " Patient Procedure Details Updated Succesfully ",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('.delete_procedure_information_form').submit(function (event) {
                        dataString = $(".delete_procedure_information_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>reports/delete_procedure_visit_data",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        " Patient Procedure Details Updated Succesfully ",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                }
            });</script>
        <!--- Reports profile end -->






        <!--  Lab profile start--> 
        <script type="text/javascript">
            $(document).ready(function () {




                $('#add_walkin_test_form').submit(function (event) {
                    dataString = $("#add_walkin_test_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/lab/add_walkin_tests",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Test booked Successfully",
                                    "success",
                                    {position: "center"}
                            );

                        }

                    });
                    event.preventDefault();
                    return false;
                });





                var lab_url = window.location.href.indexOf("lab") > -1;
                if (lab_url) {

                    $("#add_test_notes").keyup(function () {
                        add_walkin_lab_results_notes();
                    });



                    $("#test_results_text_area").keyup(function () {
                        add_lab_test_results();
                    });
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>lab/total_regular_patients_in_tray",
                            dataType: "JSON",
                            success: function (total_regular_patients) {
                                total_visit = $('#total_regular_patients_in_tray_today').empty();
                                if (total_visit === null) {
                                    total_visit.append("<p>No Patients In Tray</p>");
                                } else {
                                    $.each(total_regular_patients, function (i, total_regular_patients) {
                                        if (total_regular_patients.total_patient_visits === null) {
                                            total_visit.append("<p>No Patients In Tray</p>");
                                        } else {
                                            total_visit.append('<p>' + total_regular_patients.total_regular_patients_in_tray + '</p>');
                                        }

                                    });
                                }



                            },
                            error: function (data) {
                                //
                                //  alert('An error occured, kindly try later');
                            }
                        });
                    }, 3000);
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>lab/lab_patient_lists",
                            dataType: "JSON",
                            success: function (lab_patient_lists) {
                                patient_intray = $('#regular_patients_in_tray').empty();
                                if (patient_intray === null) {
                                    patient_intray.append("<p>No Patients In Tray</p>");
                                } else {
                                    $.each(lab_patient_lists, function (i, lab_patient_lists) {
                                        if (patient_intray.total_patient_visits === null) {
                                            patient_intray.append("<li>No Patients In Tray</li>");
                                        } else {
                                            if (lab_patient_lists.patient_id === null) {
                                                patient_intray.append("<li>No Patients In Tray</li>");
                                            } else {
                                                if (lab_patient_lists.urgency !== "urgent") {
                                                    if (lab_patient_lists.charge_followup !== "Yes") {
                                                        patient_intray.append('<li><a href="<?php echo base_url() ?>lab/lab_patient_profile/' + lab_patient_lists.patient_id + '/' + lab_patient_lists.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + lab_patient_lists.title + " : " + lab_patient_lists.f_name + " " + lab_patient_lists.s_name + " " + lab_patient_lists.other_name + '</a></li></br>');

                                                    } else {
                                                        patient_intray.append('<li><a href="<?php echo base_url() ?>lab/lab_patient_profile/' + lab_patient_lists.patient_id + '/' + lab_patient_lists.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + lab_patient_lists.title + " : " + lab_patient_lists.f_name + " " + lab_patient_lists.s_name + " " + lab_patient_lists.other_name + '</a>(Follow Up)</li></br>');

                                                    }
                                                } else {

                                                    if (lab_patient_lists.charge_followup !== "Yes") {
                                                        patient_intray.append('<li><a href="<?php echo base_url() ?>lab/lab_patient_profile/' + lab_patient_lists.patient_id + '/' + lab_patient_lists.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + lab_patient_lists.title + " : " + lab_patient_lists.f_name + " " + lab_patient_lists.s_name + " " + lab_patient_lists.other_name + '</a><span style="color:red !important;"> Urgent</span></li></br>');

                                                    } else {
                                                        patient_intray.append('<li><a href="<?php echo base_url() ?>lab/lab_patient_profile/' + lab_patient_lists.patient_id + '/' + lab_patient_lists.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + lab_patient_lists.title + " : " + lab_patient_lists.f_name + " " + lab_patient_lists.s_name + " " + lab_patient_lists.other_name + '</a><span style="color:red !important;"> Urgent (Follow Up)</span></li></br>');

                                                    }
                                                }
                                            }
                                        }

                                    });
                                }



                            },
                            error: function (data) {
                                //
                                //  alert('An error occured, kindly try later');
                            }
                        });
                    }, 3000);
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>lab/lab_patient_lists_express",
                            dataType: "JSON",
                            success: function (lab_patient_lists) {
                                patient_intray = $('#regular_patients_in_tray_express').empty();
                                if (patient_intray === null) {
                                    patient_intray.append("<p>No Patients In Tray</p>");
                                } else {
                                    $.each(lab_patient_lists, function (i, lab_patient_lists) {
                                        if (patient_intray.total_patient_visits === null) {
                                            patient_intray.append("<li>No Patients In Tray</li>");
                                        } else {
                                            if (lab_patient_lists.patient_id === null) {
                                                patient_intray.append("<li>No Patients In Tray</li>");
                                            } else {
                                                if (lab_patient_lists.urgency !== "urgent") {
                                                    if (lab_patient_lists.charge_followup !== "Yes") {
                                                        patient_intray.append('<li><a href="<?php echo base_url() ?>lab/lab_patient_profile/' + lab_patient_lists.patient_id + '/' + lab_patient_lists.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + lab_patient_lists.title + " : " + lab_patient_lists.f_name + " " + lab_patient_lists.s_name + " " + lab_patient_lists.other_name + '</a>(Express)</li></br>');

                                                    } else {
                                                        patient_intray.append('<li><a href="<?php echo base_url() ?>lab/lab_patient_profile/' + lab_patient_lists.patient_id + '/' + lab_patient_lists.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + lab_patient_lists.title + " : " + lab_patient_lists.f_name + " " + lab_patient_lists.s_name + " " + lab_patient_lists.other_name + '</a>(Express):(Follow Up)</li></br>');

                                                    }
                                                } else {

                                                    if (lab_patient_lists.charge_followup !== "Yes") {
                                                        patient_intray.append('<li><a href="<?php echo base_url() ?>lab/lab_patient_profile/' + lab_patient_lists.patient_id + '/' + lab_patient_lists.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + lab_patient_lists.title + " : " + lab_patient_lists.f_name + " " + lab_patient_lists.s_name + " " + lab_patient_lists.other_name + '</a>(Express)<span style="color:red !important;"> Urgent</span></li></br>');

                                                    } else {
                                                        patient_intray.append('<li><a href="<?php echo base_url() ?>lab/lab_patient_profile/' + lab_patient_lists.patient_id + '/' + lab_patient_lists.visit_id + '"><i class = "glyphicon glyphicon-user"></i>' + lab_patient_lists.title + " : " + lab_patient_lists.f_name + " " + lab_patient_lists.s_name + " " + lab_patient_lists.other_name + '</a>(Express)(Follow Up) <span style="color:red !important;"> Urgent</span></li></br>');

                                                    }
                                                }
                                            }
                                        }

                                    });
                                }



                            },
                            error: function (data) {
                                //
                                //  alert('An error occured, kindly try later');
                            }
                        });
                    }, 3000);



                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>lab/lab_walkin_patient_list",
                            dataType: "JSON",
                            success: function (lab_patient_list) {
                                patient_intray = $('#walkin_patients_in_tray').empty();
                                if (patient_intray === null) {
                                    patient_intray.append("<p>No Patients In Tray</p>");
                                } else {
                                    $.each(lab_patient_list, function (i, lab_patient_lists) {
                                        if (patient_intray.total_patient_visits === null) {
                                            patient_intray.append("<p>No Patients In Tray</p>");
                                        } else {
                                            if (lab_patient_lists.patient_id === null) {
                                                patient_intray.append("<p>No Patients In Tray</p>");
                                            } else {
                                                patient_intray.append('<li><a href="<?php echo base_url() ?>lab/lab_walkin_profile/' + lab_patient_lists.patient_id + '/' + lab_patient_lists.walkin_visit_id + '/"><i class = "glyphicon glyphicon-user"></i>' + lab_patient_lists.patient_name + '</a></li></br>');
                                            }
                                        }

                                    });
                                }



                            },
                            error: function (data) {
                                //
                                //  alert('An error occured, kindly try later');
                            }
                        });
                    }, 3000);





                }
            });</script>
        <!-- Lab Profile End -->








        <link href='<?php echo base_url(); ?>assets/css/jquery-ui.css' rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>-->





        <script type="text/javascript">
            $(function () {
                $(".lab_results_date_from").datepicker({
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $(".lab_results_date_to").datepicker({
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });

                $(".add_expiry_date").datepicker({
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $(".edit_stock_expiry_date").datepicker({
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $(".stock_date_from").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $(".stock_date_to").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $("#sick_off_date_from").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $("#sick_off_date_to").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $("#previous_visit_date").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $("#datepicker").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $("#datepcker").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#date_to").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#date_from").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#visitation_date_to").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#visitation_date_from").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#walkin_date_to").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#walkin_date_from").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#LMP").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#LMP").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
            });</script>




        <script type="text/javascript">
            $(function () {
                $("#datepicker_1").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $("#datepcker_1").datepicker({dateFormat: 'yy-mm-dd'});
            });
            $(document).ready(function () {
                $('#example').dataTable({
                    "scrollY": 200,
                    "scrollX": true
                });
            });</script>







        <?php
        $segment_1 = $this->uri->segment(1);

        $profile_array = array("reports", "lab");
        if (in_array($segment_1, $profile_array)) {
            ?>



            <link href='<?php echo base_url(); ?>assets/datatables-fixedcolumn/jquery.dataTables.css' rel='stylesheet'>
            <link href='<?php echo base_url(); ?>assets/datatables-fixedcolumn/dataTables.fixedColumns.css' rel='stylesheet'>
            <script type="text/css">
                /* Ensure that the demo table scrolls */
                th, td { white-space: nowrap; }
                div.dataTables_wrapper {
                    width: 800px;
                    margin: 0 auto;
                }
            </script>

            <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
            <script type="text/javascript" src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>

            <link href='http://cdn.datatables.net/1.10.5/css/jquery.dataTables.css' rel='stylesheet'>



            <script type="text/javascript">
                var j = jQuery.noConflict();
                j(document).ready(function () {
                    j('.patient_test_table').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.patients_report').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('#bill_patient_prescription_table').dataTable({
                        "scrollY": "400px",
                        "scrollCollapse": true,
                        "paging": false,
                        "pageLength": 20
                    });
                    j('.procedure_reportss').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.visitation_report').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.walkin_report').dataTable({
                        "scrollY": "400px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.commodity_report').dataTable({
                        "scrollY": "400px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.triage_report_1').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.triage_report').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.procedure_report').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.appointment_history_table').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                });
                j(document).ready(function () {

                    j('.bill_patient_prescription_link').click(function () {


                        //get data
                        var prescription_id = j(this).closest('tr').find('input[name="prescription_id"]').val();
                        var prescription_tracker = j(this).closest('tr').find('input[name="hidden_prescription_tracker"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>pharmacy/get_presciprion_details/" + prescription_id,
                            dataType: "json",
                            success: function (response) {

                                j('#patient_lab_test_idsend_to_doctor_form').val(response[0].prescription_id);
                                j('#presc_prescription_tracker').val(response[0].prescription_tracker);
                                j('#presc_route_name').val(response[0].route);
                                j('#presc_frequency').val(response[0].frequency);
                                j('#presc_prescription_tracker_2').val(response[0].prescription_tracker);
                                j('#presc_strength').val(response[0].strength);
                                j('#presc_commodity_name').val(response[0].commodity_name);
                                j('#presc_is_dispensed').val(response[0].is_dispensed);
                                j('#presc_quantity_issued').val(response[0].quantity_issued);
                                j('#test_results_lab_test_id').val(response[0].consultation_id);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.dipsense_commodity_link').click(function () {


                        //get data
                        var dispensing_transaction_id = j(this).closest('tr').find('input[name="dispensing_transaction_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>pharmacy/get_available_commodities/" + dispensing_transaction_id,
                            dataType: "json",
                            success: function (response) {

                                j('#dispensing_form_patient_visit_statement_id').val(response[0].patient_visit_statement_id);
                                j('#dispensing_form_prescription_tracker').val(response[0].prescription_tracker);
                                j('#dispensing_form_prescription_id').val(response[0].prescription_id);
                                j('#dispensing_form_patient_id').val(response[0].patient_id);
                                j('#dispensing_form_visit_id').val(response[0].visit_id);
                                j('#dispensing_form_transaction_id').val(response[0].transaction_id);
                                j('#dispensing_form_commodity_name').val(response[0].commodity_name);
                                j('#dispensinf_form_stock_id').val(response[0].stock_id);
                                j('#dispensing_form_available_quantity').val(response[0].available_quantity);
                                j('#dispensing_form_batch_no').val(response[0].batch_no);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.add_test_results_link').click(function () {


                        //get data
                        var lab_test_id = j(this).closest('tr').find('input[name="lab_test_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>lab/get_lab_test_results/" + lab_test_id,
                            dataType: "json",
                            success: function (response) {

                                j('#patient_lab_test_id').val(response[0].lab_test_id);
                                j('#patient_test_name').val(response[0].test_name);
                                j('#test_results_text_area').val(response[0].test_results);
                                j('#test_results_visit_id').val(response[0].visit_id);
                                j('#test_results_lab_test_id').val(response[0].lab_test_id);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.edit_triage_patient_link').click(function () {


                        //get data
                        var triage_id = j(this).closest('tr').find('input[name="view_triage_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>nurse/get_triage_details/" + triage_id,
                            dataType: "json",
                            success: function (response) {
                                j('#edit_weight').val(response[0].weight);
                                j('#edit_systolic').val(response[0].systolic);
                                j('#edit_diastolic').val(response[0].diastolic);
                                j('#edit_temperature').val(response[0].temperature);
                                j('#edit_height').val(response[0].height);
                                j('#edit_respiratory').val(response[0].respiratory_rate);
                                j('#edit_blood_sugar').val(response[0].blood_sugar);
                                j('#edit_pulse_rate').val(response[0].pulse_rate);
                                j('#edit_LMP').val(response[0].lmp);
                                j('#edit_general_complaints').val(response[0].OCS);
                                j('#edit_allergy').val(response[0].allergy);
                                j('#edit_urgency').val(response[0].urgency);
                                j('#edit_triage_id').val(response[0].triage_id);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.edit_patient_link').click(function () {


                        //get data
                        var patient_id = j(this).closest('tr').find('input[name="view_patient_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_patient_reports_details/" + patient_id,
                            dataType: "json",
                            success: function (response) {
                                j('#edit_patient_id').val(response[0].patient_id);
                                j('#edit_title').val(response[0].title);
                                j('#edit_phone_no').val(response[0].phone_no);
                                j('#edit_inputFirstName').val(response[0].f_name);
                                j('#edit_inputSurName').val(response[0].s_name);
                                j('#edit_inputOtherName').val(response[0].other_name);
                                j('#edit_dob').val(response[0].dob);
                                j('#edit_gender').val(response[0].gender);
                                j('#edit_maritalstatus').val(response[0].marital_status);
                                j('#edit_nationalid').val(response[0].identification_number);
                                j('#edit_email').val(response[0].email);
                                j('#edit_address').val(response[0].address);
                                j('#edit_residence').val(response[0].residence);
                                j('#edit_employement_status').val(response[0].employment_status);
                                j('#edit_employers_name').val(response[0].employer);
                                j('#edit_kinname').val(response[0].next_of_kin_fname);
                                j('#edit_kinsname').val(response[0].next_of_kin_lname);
                                j('#edit_kinrelation').val(response[0].next_of_kin_relation);
                                j('#edit_kinphone').val(response[0].next_of_kin_phone);
                                j('#edit_family_number').val(response[0].family_base_number);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.view_patient_link').click(function () {


                        //get data
                        var patient_id = j(this).closest('tr').find('input[name="view_patient_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_patient_reports_details/" + patient_id,
                            dataType: "json",
                            success: function (response) {
                                j('#view_patient_id').val(response[0].patient_id);
                                j('#view_title').val(response[0].title);
                                j('#view_phone_no').val(response[0].phone_no);
                                j('#view_inputFirstName').val(response[0].f_name);
                                j('#view_inputSurName').val(response[0].s_name);
                                j('#view_inputOtherName').val(response[0].other_name);
                                j('#view_dob').val(response[0].dob);
                                j('#view_gender').val(response[0].gender);
                                j('#view_maritalstatus').val(response[0].marital_status);
                                j('#view_nationalid').val(response[0].identification_number);
                                j('#view_email').val(response[0].email);
                                j('#view_residence').val(response[0].residence);
                                j('#view_employement_status').val(response[0].employment_status);
                                j('#view_employers_name').val(response[0].employer);
                                j('#view_kinname').val(response[0].next_of_kin_fname);
                                j('#view_kinsname').val(response[0].next_of_kin_lname);
                                j('#view_kinrelation').val(response[0].next_of_kin_relation);
                                j('#view_kinphone').val(response[0].next_of_kin_phone);
                                j('#view_family_number').val(response[0].family_base_number);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.delete_patient_link').click(function () {


                        //get data
                        var patient_id = j(this).closest('tr').find('input[name="view_patient_id"]').val();
                        j('.input_patient_id_delete').val(patient_id);
                    });
                    j('.edit_visit_link').click(function () {


                        //get data
                        var visit_id = j(this).closest('tr').find('input[name="view_visit_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_visit_reports_details/" + visit_id,
                            dataType: "json",
                            success: function (response) {
                                var title = response[0].title;
                                var f_name = response[0].f_name;
                                var s_name = response[0].s_name;
                                var other_name = response[0].other_name;
                                var space = ' ';
                                var patient_name = title + space + f_name + space + s_name + space + other_name;
                                j('#edit_visit_id').val(response[0].visit_id);
                                j('#edit_visitation_status').val(response[0].visit_status);
                                j('#edit_phone_no').val(response[0].phone_no);
                                j('#edit_inputPatientName').val(patient_name);
                                j('#edit_inputPatientFamilyNumber').val(response[0].family_number);
                                j('#edit_visit_date').val(response[0].visit_date);
                                j('#edit_nurse_queue').val(response[0].nurse_queue);
                                j('#edit_doctor_queue').val(response[0].doctor_queue);
                                j('#edit_doctor_name').val(response[0].doctor_name);
                                j('#edit_lab_queue').val(response[0].lab_queue);
                                j('#edit_pharm_queue').val(response[0].pharm_queue);
                                j('#edit_urgency').val(response[0].urgency);
                                j('#edit_package_name').val(response[0].package_name);
                                j('#edit_pay_at_the_end').val(response[0].pay_at_the_end);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.view_visit_link').click(function () {


                        //get data
                        var visit_id = j(this).closest('tr').find('input[name="view_visit_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_visit_reports_details/" + visit_id,
                            dataType: "json",
                            success: function (response) {
                                var title = response[0].title;
                                var f_name = response[0].f_name;
                                var s_name = response[0].s_name;
                                var other_name = response[0].other_name;
                                var space = ' ';
                                var patient_name = title + space + f_name + space + s_name + space + other_name;
                                j('#view_visit_id').val(response[0].visit_id);
                                j('#view_visitation_status').val(response[0].visit_status);
                                j('#view_phone_no').val(response[0].phone_no);
                                j('#view_inputPatientName').val(patient_name);
                                j('#view_inputPatientFamilyNumber').val(response[0].family_number);
                                j('#view_visit_date').val(response[0].visit_date);
                                j('#view_nurse_queue').val(response[0].nurse_queue);
                                j('#view_doctor_queue').val(response[0].doctor_queue);
                                j('#view_doctor_name').val(response[0].doctor_name);
                                j('#view_lab_queue').val(response[0].lab_queue);
                                j('#view_pharm_queue').val(response[0].pharm_queue);
                                j('#view_urgency').val(response[0].urgency);
                                j('#view_package_name').val(response[0].package_name);
                                j('#view_pay_at_the_end').val(response[0].pay_at_the_end);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.delete_visit_link').click(function () {


                        //get data
                        var visit_id = j(this).closest('tr').find('input[name="view_visit_id"]').val();
                        j('.input_visit_id_delete').val(visit_id);
                    });
                    j('.view_walkin_patient_link').click(function () {


                        //get data
                        var walkin_patient_id = j(this).closest('tr').find('input[name="view_walkin_patient_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_walkinpatient_reports_details/" + walkin_patient_id,
                            dataType: "json",
                            success: function (response) {

                                j('#view_walkin_id').val(response[0].walkin_id);
                                j('#view_inputwalkinPatientName').val(response[0].walkin_patient_name);
                                j('#view_inputPhoneNumber').val(response[0].walkin_phone_no);
                                j('#view_department_name').val(response[0].walkin_department);
                                j('#view_payment_status').val(response[0].paid);
                                j('#view_walkin_visit_date').val(response[0].walkin_date);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.edit_walkin_patient_link').click(function () {


                        //get data
                        var walkin_patient_id = j(this).closest('tr').find('input[name="view_walkin_patient_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_walkinpatient_reports_details/" + walkin_patient_id,
                            dataType: "json",
                            success: function (response) {

                                j('#edit_walkin_id').val(response[0].walkin_id);
                                j('#edit_inputwalkinPatientName').val(response[0].walkin_patient_name);
                                j('#edit_inputPhoneNumber').val(response[0].walkin_phone_no);
                                j('#edit_department_name').val(response[0].walkin_department);
                                j('#edit_payment_status').val(response[0].paid);
                                j('#edit_walkin_status').val(response[0].walkin_status);
                                j('#edit_walkin_visit_date').val(response[0].walkin_date);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.delete_walkin_patient_link').click(function () {


                        //get data
                        var visit_id = j(this).closest('tr').find('input[name="view_walkin_patient_id"]').val();
                        j('.input_walkin_patient_id_delete').val(visit_id);
                    });
                    j('.edit_procedure_link').click(function () {


                        //get data
                        var procedure_visit_id = j(this).closest('tr').find('input[name="view_procedure_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_procedure_visit_reports_details/" + procedure_visit_id,
                            dataType: "json",
                            success: function (response) {

                                j('#edit_procedure_visit_id').val(response[0].procedure_visit_id);
                                j('#edit_inputPatientName').val(response[0].patient_name);
                                j('#edit_inputPhoneNumber').val(response[0].patient_phone);
                                j('#edit_patient_type').val(response[0].patient_type);
                                j('#edit_procedure_name').val(response[0].procedure_name);
                                j('#edit_visit_date').val(response[0].date_added);
                                j('#edit_visit_status').val(response[0].status);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.view_procedure_link').click(function () {


                        //get data
                        var procedure_visit_id = j(this).closest('tr').find('input[name="view_procedure_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_procedure_visit_reports_details/" + procedure_visit_id,
                            dataType: "json",
                            success: function (response) {

                                j('#view_procedure_visit_id').val(response[0].procedure_visit_id);
                                j('#view_inputPatientName').val(response[0].patient_name);
                                j('#view_inputPhoneNumber').val(response[0].patient_phone);
                                j('#view_procedure_name').val(response[0].procedure_name);
                                j('#view_visit_date').val(response[0].date_added);
                                j('#view_visit_status').val(response[0].status);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.delete_procedure_link').click(function () {


                        //get data
                        var visit_id = j(this).closest('tr').find('input[name="view_procedure_id"]').val();
                        j('.input_procedure_patient_id_delete').val(visit_id);
                    });
                });</script>


            <?php
        }
        ?>










        <script src="<?php echo base_url(); ?>assets/hotkeys/jquery-1.4.2.js"></script>
        <script src="<?php echo base_url(); ?>assets/hotkeys/jquery.hotkeys.js"></script>
        <script type="text/javascript">
            var y = jQuery.noConflict();
            function activaTab(tab) {
                $('.nav-tabs a[href="#' + tab + '"]').tab('show');
            }
            ;
            function fancypopup(link_id) {

                $.fancybox({
                    href: link_id,
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
            }
            ;
            y(document).ready(function () {

                $('#patient_procedure_option').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                y(document).bind('keydown', 'shift+1', function () {
                    activaTab('nurse_notes');
                })
                        .bind('keydown', 'shift+2', function () {
                            activaTab('chief_complaints');
                        })
                        .bind('keydown', 'shift+3', function () {
                            activaTab('review_of_systems');
                        })
                        .bind('keydown', 'shift+4', function () {
                            activaTab('working_diagnosis');
                        })
                        .bind('keydown', 'shift+5', function () {
                            activaTab('tests_referrals');
                        })
                        .bind('keydown', 'shift+6', function () {
                            activaTab('all_records_tab');
                        })
                        .bind('keydown', 'shift+7', function () {
                            activaTab('triage_tab');
                        })
                        .bind('keydown', 'shift+8', function () {
                            activaTab('lab_tab');
                        })
                        .bind('keydown', 'shift+9', function () {
                            activaTab('consultation_tab');
                        })
                        .bind('keydown', 'shift+q', function () {
                            activaTab('prescription_tab');
                        })
                        .bind('keydown', 'shift+w', function () {
                            activaTab('order_commodity_tab');
                        })
                        .bind('keydown', 'shift+e', function () {
                            activaTab('bill_patient_tab');
                        })
                        .bind('keydown', 'shift+r', function () {
                            activaTab('dispense_commodity_tab');
                        })
                        .bind('keydown', 'shift+h', function () {
                            var url = "<?php echo base_url(); ?>home";
                            window.location = url;
                        })
                        .bind('keydown', 'shift+r', function () {
                            fancypopup('#patient_option');
                        })
                        .bind('keydown', 'shift+b', function () {
                            var url = "<?php echo base_url(); ?>reception/visit";
                            window.location = url;
                        })
                        .bind('keydown', 'shift+a', function () {
                            var url = "<?php echo base_url(); ?>reception/walkin_patient";
                            window.location = url;
                        })
                        .bind('keydown', 'shift+p', function () {
                            var url = "<?php echo base_url(); ?>cashier";
                            window.location = url;
                        })


                        .bind('keydown', 'shift+l', function () {
                            var url = "<?php echo base_url(); ?>home/do_logout";
                            window.location = url;
                        })
                        .bind('keydown', 'shift+z', function () {
                            fancypopup('#edit_triage_form');
                        })
                        .bind('keydown', 'shift+x', function () {
                            fancypopup('#add_walkin_form');
                        })

                        .bind('keydown', 'shift+e', function () {
                            var url = "<?php echo base_url(); ?>reports/visitation_report";
                            window.location = url;
                        })

                        .bind('keydown', 'shift+d', function () {
                            fancypopup('#procedure_option');
                        })
                        .bind('keydown', 'alt+r', function () {
                            var url = "<?php echo base_url(); ?>cashier/regular_patient_visit_statement";
                            window.location = url;
                        })

                        .bind('keydown', 'ctrl+alt+w', function () {
                            var url = "<?php echo base_url(); ?>cashier/walkin_patient_visit_statement";
                            window.location = url;
                        })

                        .bind('keydown', 'alt+p', function () {
                            fancypopup('#patient_report_filter_form');
                        })


                        .bind('keydown', 'alt+v', function () {
                            fancypopup('#visit_report_filter_form');
                        })


                        .bind('keydown', 'alt+i', function () {
                            fancypopup('#walkin_report_filter_form');
                        })

                        .bind('keydown', 'alt+d', function () {
                            fancypopup('#regular_procedure_report_filter_form');
                        })



                setInterval(function () {
                    window.location.reload();
                }, 300000);



            });

        </script>
        <script>

        </script>

    </body>
</html>
