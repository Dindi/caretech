<!DOCTYPE html> 
<html>
    <head>
        <title>jquery add remove table row dynamically with checkbox functionality</title>
        <meta charset='utf-8'>
        <style>
            @font-face{font-family: Lobster;src: url('Lobster.otf');}
            h1{font-family: Lobster;text-align:center;}
            table{border-collapse:collapse;border-radius:25px;width:880px;}
            table, td, th{border:1px solid #00BB64;}
            tr,input{height:30px;border:1px solid #fff;}
            input{text-align:center;}
            input:focus{border:1px solid yellow;} 
            .space{margin-bottom: 2px;}
            #container{margin-left:210px;}
            .but{width:270px;background:#00BB64;border:1px solid #00BB64;height:40px;border-radius:3px;color:white;margin-top:10px;margin:0px 0px 0px 290px;}
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                var i = 2;
                $(".addmore").on('click', function () {
                    var data = "<tr><td><input type='checkbox' class='case'/></td><td>" + i + ".</td>";
                    data += "<td><input type='text' id='list_name" + i + "' name='list_name[]'/></td> <td><input type='text' id='icd_10_name" + i + "' name='icd_10_name[]'/></td></tr>";
                    $('table').append(data);
                    i++;
                });

                $(".delete").on('click', function () {
                    $('.case:checkbox:checked').parents("tr").remove();

                });


            });

        </script>

        <script type="text/javascript">
            function select_all() {
                $('input[class=case]:checkbox').each(function () {
                    if ($('input[class=check_all]:checkbox:checked').length == 0) {
                        $(this).prop("checked", false);
                    } else {
                        $(this).prop("checked", true);
                    }
                });
            }

        </script>
    </head>
    <body>


        <form id='icd_10_lists' method='post' name='icd_10_lists' >

            <table border="1" cellspacing="0">
                <tr>
                    <th><input class='check_all' type='checkbox' onclick="select_all()"/></th>
                    <th>S. No</th>
                    <th>List Name</th>
                    <th>ICD 10 Name</th>

                </tr>
                <tr>
                    <td><input type='checkbox' class='case'/></td>
                    <td>1.</td>
                    <td><input type='text' id='list_name' name='list_name[]'/></td>
                    <td><input type='text' id='icd_10_name' name='icd_10_name[]'/></td>

                </tr>
            </table>

            <button type="button" class='delete'>- Delete</button>
            <button type="button" class='addmore'>+ Add More</button>
            <p>
                <input type='submit' name='submit' value='submit' class='but'/></p>
        </form>
    </body>
</html>