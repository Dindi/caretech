<!DOCTYPE html>
<html lang="en">
    <head>
        <!--
    
        -->
        <meta charset="utf-8">
        <title>Care-tech System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Out patient Hospital Management System.">
        <meta name="author" content="Harris Samuel Dindi">
    </head>

    <script type="text/javascript" src="http://localhost/caretech_new/assets/datatables_serverside/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="http://localhost/caretech_new/assets/datatables_serverside/js/jquery.dataTables.js"></script>

    <link href='http://localhost/caretech_new/assets/datatables_serverside/css/jquery.dataTables.css' rel='stylesheet'>



    <style>
        p{
            text-align: center;
            font-weight: bolder;
        }
    </style>
    <div class="row">

    </div>
    <!-- /.row -->
    <div class="row " style="margin-top: 20px;">
        <p>JOB REQUEST(S) APPROVED BY  VOTEHOLDER</p>

    </div>
    <div class="row">
        <div id="time_selector">
            <span id="error" style="background: red; color:white;">Start and end Date cannot be the same.</span><br>
            <input type="text" id="start"  style="width:100px;"/>
            <input type="text" id="end"  style="width:100px;"/>
            <input type="button" value="Filter" id="filter" style="width: 80px;"/>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <div class="bak"><button class="btn  btn-success back-button" title="Go back"><span class="glyphicon glyphicon-backward"> <strong>Back</strong></span></i> &nbsp; </button><p id='thehistoryname' style="font-weight: bolder; color:blue; text-decoration: underline; text-transform: uppercase;"></p></div>
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>                                       
                    </tr>
                </thead>
                <tbody>
                    <tr class="odd gradeX">                                          
                    </tr>                                       
                </tbody>
            </table>
        </div>
    </div>



    <script>
        $(document).ready(function () {



            $('#filter').click(function () {
                start = $('#start').val();
                end = $('#end').val();
                $("#dataTables-example").empty();
                $('#dataTables-example').dataTable().fnDestroy();

                $('.table-striped').dataTable({
                    "bautoWidth": false,
                    "aoColumns": [
                        {"sTitle": "Commodity No", "mData": "commodity_id"},
                        {"sTitle": "Commodity Name.", "mData": "commodity_name"

                        },
                        {"sTitle": "Strength", "mData": "strength"},
                        {"sTitle": "Commodity code", "mData": "commodity_code"},
                        {"sTitle": "Type.", "mData": "commodity_type"},
                        {"sTitle": "Commodity unit", "mData": "commodity_unit"},
                        {"sTitle": "Max Stock", "mData": "max_stock"},
                        {"sTitle": "Min sTOCK", "mData": "min_stock"},
                        {"sTitle": "Remarks", "mData": "remarks"},
                    ],
                    "bDeferRender": true,
                    "bProcessing": true,
                    "bDestroy": true,
                    "bLengthChange": true,
                    "iDisplayLength": 200,
                    "sAjaxDataProp": "",
                    "sAjaxSource": '<?php echo site_url() . "pharmacy/commodity_managements/"; ?>' + start + '/' + end,
                    "aaSorting": [[5, "asc"]],
                });
                $('#dataTables-example').css('width', '100%');

            });



            oTable = '';
            Draw();

            $('.back-button').hide();
            function Draw() {
                oTable = $("#dataTables-example").DataTable({
                    "dom": 'T<"clear">lfrtip',
                    
                    "tableTools": {
                        "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
                    },
                    stateSave: true,
                    "bautoWidth": false,
                    "aoColumns": [
                        {"sTitle": "Commodity No", "mData": "commodity_id"},
                        {"sTitle": "Commodity Name.", "mData": "commodity_name"

                        },
                        {"sTitle": "Strength", "mData": "strength"},
                        {"sTitle": "Commodity code", "mData": "commodity_code"},
                        {"sTitle": "Type.", "mData": "commodity_type"},
                        {"sTitle": "Commodity unit", "mData": "commodity_unit"},
                        {"sTitle": "Max Stock", "mData": "max_stock"},
                        {"sTitle": "Min sTOCK", "mData": "min_stock"},
                        {"sTitle": "Remarks", "mData": "remarks"},
                    ],
                    "bDeferRender": true,
                    "bProcessing": true,
                    "bDestroy": true,
                    "bLengthChange": true,
                    "iDisplayLength": 200,
                    "sAjaxDataProp": "",
                    "sAjaxSource": '<?php echo base_url() . "pharmacy/commodity_managements"; ?>',
                    "aaSorting": [[8, "desc"]],
                });

            }

            function DrawHistor(i) {

                oTable = $("#dataTables-example").dataTable({
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                       "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
                    },
                    "bautoWidth": false,
                    "aoColumns": [
                        {"sTitle": "Project/Unit", "mData": "Project_unit"},
                        {"sTitle": "Surname.", "mData": "Surname"},
                        {"sTitle": "Other Names", "mData": "Other_Names"},
                        {"sTitle": "Start Date", "mData": "Position_DO"},
                        {"sTitle": "End Date", "mData": "Position_DC"},
                        {"sTitle": "Employment Type", "mData": "Employment_type"},
                        {"sTitle": "Contract", "mData": "ID",
                            "mRender": function (data, type, row) {
                                if (row.Days_Left <= '0') {
                                    return '<div class="alert alert-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"> <sup>Expired!</sup> </div>';
                                } else if (row.Days_Left > 0 && row.Days_Left < 14) {
                                    return '<div class="alert alert-warning"><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"><sup> Almost Expiring!</sup></div>';
                                } else if (row.Days_Left > 14) {
                                    return '<div class="alert alert-info"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"> <sup>Valid</sup></span></div>';

                                }
                            }
                        },
                        {"sTitle": "Action", "mData": "ID",
                            "mRender": function (data, type, full) {
                                return '<button  title="Full info " class="show-history btn btn-small btn-primary popitover" data-placement="bottom" id = ' + data + ' > <span  class="glyphicon glyphicon-edit"></span></button>';

                            }
                        },
                    ],
                    "bDeferRender": true,
                    "bProcessing": true,
                    "bDestroy": true,
                    "bLengthChange": true,
                    "iDisplayLength": 200,
                    "sAjaxDataProp": "",
                    "sAjaxSource": '<?php echo site_url() . "service_request/getAllHistoryDetails/"; ?>' + i,
                    "aaSorting": [[5, "asc"]]



                });
            }



        });



    </script>