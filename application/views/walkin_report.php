<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Reports</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Walk-In Report</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <a  id="export_walkin_patient_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Walkin Patient Report" class="export_walkin_patient_report_filter_link" href="#export_walkin_report_filter_form">
                    <i class="glyphicon glyphicon-download-alt"></i>

                </a>
                <table class="table table-striped table-bordered walkin_report  responsive">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Date </th>
                            <th>Patient Name</th>
                            <th>Phone No</th>
                            <th>Department</th>
                            <th>Payment Status</th>

                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $i = 1;
                            foreach ($walkins as $value) {
                                ?>
                                <td class="center"><?php echo $i; ?></td>
                                <td class="center">
                                    <?php
                                    echo $value['walkin_date'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['walkin_patient_name'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['walkin_phone_no'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['walkin_department']; ?>
                                </td>
                                <td class="center">
                                    <?php
                                    $paid = $value['paid'];
                                    if ($paid === 'paid') {
                                        ?>
                                        <label class="label label-success"><?php echo$paid; ?></label>
                                        <?php
                                    } elseif ($paid === 'not paid') {
                                        ?>
                                        <label class="label label-danger"><?php echo$paid; ?></label>
                                        <?php
                                    }
                                    ?>
                                </td>




                                <td class="center">
                                    <input type="hidden" name="view_walkin_patient_id" class="view_walkin_patient_id" id="view_walkin_patient_id" value="<?php echo $value['walkin_id']; ?>"/>
                                    <a  id="view_walkin_patient_link" class="view_walkin_patient_link" href="#view_walkin_patient_form">
                                        <i class="glyphicon glyphicon-zoom-in icon-white"></i>

                                    </a>|
                                    <a  id="edit_walkin_patient_link" class="edit_walkin_patient_link" href="#edit_walkin_patient_form">
                                        <i class="glyphicon glyphicon-edit icon-white"></i>

                                    </a>|
                                    <a  id="delete_walkin_patient_link" class="delete_walkin_patient_link" href="#delete_walkin_patient_form">
                                        <i class="glyphicon glyphicon-trash icon-white"></i>

                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>




                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->


</div><!--/row-->


<div style="display: none;" class="view_walkin_patient_form" id="view_walkin_patient_form">




    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Walk-in Patient Form</h2>

                    <div class = "box-icon">
                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                class = "glyphicon glyphicon-chevron-up"></i></a>

                    </div>
                </div>



                <div class = "box-content">

                    <div class = "bs-example">


                        <form class = "view_visitation_info " autocomplete="off" method = "post"  id = "view_visitation_info" role = "form">

                            <input type="hidden" name="view_walkin_id" class="view_walkin_id" id="view_walkin_id" />

                            <div class = "form-inline">

                                <div class = "form-group">
                                    <label class = "sr-only" for = "view_inputwalkinPatientName">Patient Name : </label>
                                    <input type = "text" readonly="" class = "form-control view_inputwalkinPatientName" id = "view_inputwalkinPatientName" name="view_inputwalkinPatientName" placeholder = "Patient Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "view_inputPhoneNumber"> Phone Number : </label>
                                    <input type = "text" readonly="" class = "form-control view_inputPhoneNumber" id = "view_inputPhoneNumber" name="view_inputPhoneNumber" placeholder = "Patient Phone Number">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "view_department_name">Department Name : </label>
                                    <input type = "text" readonly="" class = "form-control view_department_name" id = "view_department_name" name="view_department_name" placeholder = "Department Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "view_visit_date">Visit Date : </label>
                                    <input type = "text" readonly="" class = "form-control view_walkin_visit_date" id = "view_walkin_visit_date" name="view_walkin_visit_date" placeholder = "Visit Date">
                                </div>
                                <hr>

                            </div>

                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "view_payment_status"> Payment Status :</label>
                                    <input name="view_payment_status"  readonly="" type="text" class="form-control view_payment_status" id="view_payment_status" />

                                </div>


                                <hr>

                            </div>


                        </form>


                    </div>


                </div>
            </div>
            <!--/span-->

        </div><!--/row-->


    </div>





</div>


<div style="display: none;" class="edit_walkin_patient_form" id="edit_walkin_patient_form">


    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Walk-in Patient Form</h2>

                    <div class = "box-icon">
                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                class = "glyphicon glyphicon-chevron-up"></i></a>

                    </div>
                </div>



                <div class = "box-content">

                    <div class = "bs-example">


                        <form class = "edit_walkin_patient_info " autocomplete="off" method = "post"  id = "edit_walkin_patient_info" role = "form">

                            <input type="hidden" name="edit_walkin_id" class="edit_walkin_id" id="edit_walkin_id" />

                            <div class = "form-inline">

                                <div class = "form-group">
                                    <label class = "sr-only" for = "edit_inputwalkinPatientName">Patient Name : </label>
                                    <input type = "text"  class = "form-control edit_inputwalkinPatientName" id = "edit_inputwalkinPatientName" name="edit_inputwalkinPatientName" placeholder = "Patient Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "edit_inputPhoneNumber"> Phone Number : </label>
                                    <input type = "text"  class = "form-control edit_inputPhoneNumber" id = "edit_inputPhoneNumber" name="edit_inputPhoneNumber" placeholder = "Patient Phone Number">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "edit_department_name">Department Name : </label>
                                    <input type = "text"  class = "form-control edit_department_name" id = "edit_department_name" name="edit_department_name" placeholder = "Department Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "edit_visit_date">Visit Date : </label>
                                    <input type = "text" class = "form-control edit_walkin_visit_date" id = "edit_walkin_visit_date" name="edit_walkin_visit_date" placeholder = "Visit Date">
                                </div>
                                <hr>

                            </div>

                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "control-label" for = "edit_payment_status"> Payment Status :</label>
                                    <select name="edit_payment_status"  required="" class="selector edit_payment_status" id="edit_payment_status" >
                                        <option value="">Please select status </option>
                                        <option value="paid">Paid</option>
                                        <option value="not paid">Not Paid </option>
                                    </select>   
                                </div>
                                <div class = "form-group">
                                    <label class = "control-label" for = "edit_walkin_status"> Walk-in Status :</label>
                                    <select name="edit_walkin_status"  required="" class="selector edit_walkin_status" id="edit_walkin_status" >
                                        <option value="">Please select status </option>
                                        <option value="Active">Active</option>
                                        <option value="In Active">In Active </option>
                                    </select>   
                                </div>

                                <hr>

                            </div>


                            <div class = "form-group">

                                <input type="submit" class="btn btn-small btn-success" value="Save Edit"/>
                                <input type="reset" class="btn btn-close btn-danger"/>
                            </div>
                        </form>


                    </div>


                </div>
            </div>
            <!--/span-->

        </div><!--/row-->


    </div>

</div>

<div style="display: none;" class="delete_walkin_patient_form" id="delete_walkin_patient_form">
    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class="box-header well">


                    <div class="box-icon">

                    </div>
                </div>




                <div class = "box-content">

                    <div class = "bs-example">


                        <form class = "delete_walkin_patient_information_form " autocomplete="off" method = "post"  id = "delete_walkin_patient_information_form" role = "form">


                            <input type="hidden" id="input_walkin_patient_id_delete" name="input_walkin_patient_id_delete" class="input_walkin_patient_id_delete"/>
                            <div class = "form-inline">
                                <div class = "form-group">
                                    <h5 class = "" for = "">Are you sure you want to Delete the details? </h5>

                                </div>

                                <br>
                                <div class = "form-group">

                                    <input type="submit" class="btn btn-small btn-success delete_patient_record_yes" value="Yes"/>
                                    <input type="reset" class="btn btn-close delete_patient_record_no" value="No"/>
                                </div>
                            </div>



                        </form>

                    </div>
                </div>


            </div>
        </div>
    </div>

</div>


<div id="export_walkin_report_filter_form" style="display: none;">


    <style type = "text/css">
        .bs-example{
            margin: 20px;
        }
    </style>


    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Care-tech Walk-in Report Filter</h2>


                </div>
                <div class="box-content">
                    <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/export_walkin_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                        <div class="control-group">
                            <label class="control-label" for="selectError">Date From :  </label>

                            <div class="controls">
                                <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="walkin_date_from" class="date_from form-control input-sm"/>
                            </div>
                        </div>

                        <hr>
                        <div class="control-group">
                            <label class="control-label" for="selectError">Date To : </label>

                            <div class="controls">
                                <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="walkin_date_to"/>
                            </div>
                        </div>
                        <hr>


                        <div class="control-group">
                            <label class="control-label" for="selectError">Department Name : </label>

                            <div class="controls">
                                <select id="selectError" name="department_name" class="department_name" required="" data-rel="">
                                    <option value="">Please select  Department : </option>
                                    <option value="All">All</option>
                                    <option value="Nursing">Nursing</option>
                                    <option value="Laboratory">Laboratory</option>
                                    <option value="Pharmacy">Pharmacy</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="control-group">
                            <label class="control-label" for="selectError">Payment Status  : </label>

                            <div class="controls">
                                <select id="selectError" name="payment_status" id="package_type" required="" data-rel="">
                                    <option value="">Please select  Payment Status : </option>
                                    <option value="All">All</option>
                                    <option value="Paid">Paid</option>
                                    <option value="Not Paid">Not Paid</option>
                                </select>
                            </div>
                        </div>

                        <hr>

                        <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                        <hr>
                    </form>

                </div>
            </div>
        </div>
        <!--/span-->
    </div>

</div>