<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?php echo base_url(); ?>">Home</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>">Patient Profile </a>
        </li>
    </ul>
</div>




<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-user"></i> Patient Bio Data </h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-7 col-md-12">

                    <table>
                        <tbody>
                            <?php
                            foreach ($patient_bio as $patient_bio_data) {
                                ?>
                                <tr>`
                                    <td> <h5>Name : <?php echo $patient_bio_data['patient_name']; ?> </h5></td>
                            <input type="hidden" name="hidden_patient_name" class="hidden_patient_name" id="hidden_patient_name" value="<?php echo $patient_bio_data['patient_name']; ?>"/>
                            <td><h6>EMR No : <?php echo $patient_bio_data['family_number']; ?> </h6></td>
                            <td> <h6 class="">Age : <?php
                                    $dob = $patient_bio_data['dob'];
                                    $age = date_diff(date_create($dob), date_create('now'))->y;

                                    if ($age <= 0) {
                                        $bage = date_diff(date_create($dob), date_create('now'))->d;
                                        $nage = $bage . " Days Old";
                                    } elseif ($age > 0) {
                                        $nage = $age . " Years Old";
                                    }
                                    echo $nage;
                                    ?> </h6><td>

                            <td> <h6>Sex : <?php
                                    echo $patient_bio_data['gender'];
                                    ?>
                                </h6> </td>
                            <td> <h6>Residence : <?php
                                    echo $patient_bio_data['residence'];
                                    ?>
                                </h6> </td>

                            <td><span class="label">Allergies : </span><h6 id="patient_allergies_list" class="patient_allergies_list"></h6></td>
                            <td> <a href="#download_lab_results" class=" download_patient_lab_results_link btn btn-xs btn-info" id="download_patient_lab_results_link">
                                    <i class="glyphicon  icon-white glyphicon-download-alt"></i>
                                    Download Lab Results : 
                                </a></td>
                            <td> <a href="#send_to_doctor" class=" send_to_doctor_link btn btn-xs btn-info" id="send_to_doctor_link">
                                    <i class="glyphicon  icon-white"></i>
                                    Send To Doctor : 
                                </a></td>
                            <td> 
                                <form class="send_to_pharmacy_form" id="send_to_pharmacy_form">
                                    <input type="hidden" name="send_to_pharm_visit_id" id="send_to_pharm_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="send_to_pharm_visit_id"/>
                                    <input type="submit" name="send_to_pharmacy_link" value=" Send To Pharmacy : " id="send_to_pharmacy_link" class=" send_to_pharmacy_link btn btn-xs btn-info"/>
                                </form>
                            </td>
                            <td> 
                                <a id="release_patient_link" href="#notification_release_patient" class="release_patient_link btn btn-xs btn-danger">Release Patient</a>

                            </td>
                            </tr>
                            <?php
                        }
                        ?> 

                        </tbody>
                    </table>

                </div>


                <!-- Download patient lab results start -->

                <div id = "download_lab_results" style = "display: none; margin: 20px;">



                    <div class = "row">
                        <div class = "box col-md-9">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "Shwari Download Patient Lab Results">
                                    <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Download Patient Lab Results</h2>
                                    <div class="box-icon">

                                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                                class="glyphicon glyphicon-chevron-up"></i></a>

                                    </div>

                                </div>
                                <div class="box-content">
                                    <?php
                                    $patient_id = $this->uri->segment(3);
                                    $visit_id = $this->uri->segment(4);
                                    ?>
                                    <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>lab/download_walkin_lab_results/<?php echo $patient_id ?>/<?php echo $visit_id ?>" id="patient_report_filter" class="form-inline  patient_report_filter">
                                        <div class="control-group">
                                            <label class="control-label" for="selectError">Date From :  </label>

                                            <div class="controls">
                                                <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="lab_results_date_from" class="lab_results_date_from form-control input-sm"/>
                                            </div>
                                        </div>

                                        <hr>
                                        <div class="control-group">
                                            <label class="control-label" for="selectError">Date To : </label>

                                            <div class="controls">
                                                <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" lab_results_date_to form-control input-sm"  id="lab_results_date_to"/>
                                            </div>
                                        </div>
                                        <hr>


                                        <div class="control-group">
                                            <label class="control-label" for="selectError">Visit Type : </label>

                                            <div class="controls">
                                                <select id="selectError" name="visit_type" class="visit_type" required="" data-rel="">
                                                    <option value="">Please select  Status : </option>
                                                    <option value="Walk-in">Walk-In Visit</option>
                                                    <option value="Regular">Regular Visit</option>
                                                    <option value="All_Visits">All Visit</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>


                                        <hr>

                                        <input type="submit" class=" btn btn-info download_patient_lab_results_button " id="download_patient_lab_results_button" value="Download Lab Results"/>
                                        <hr>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>


                </div>

                <!-- Download patient lab results -->






                <div id="notification_release_patient" class="notification_release_patient" style="display: none;">




                    <div class = "box col-md-12">
                        <div class = "box-inner">
                            <div class = "box-header well" data-original-title = "">
                                <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                <div class = "box-icon">

                                </div>
                            </div>

                            <div class = "box-content">
                                <label class = "">
                                    Are you sure you want to release the  Patient ? 
                                </label><br>
                                <table>
                                    <tr>
                                        <td>
                                            <form class="release_patient_form" id="release_patient_form">
                                                <input type="hidden" name="release_patient_visit_id" id="release_patient_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="relaease_patient_visit_id"/>
                                                <input type="submit" name="release_patient_link" value="Yes " id="yes_release_patient" class=" yes_release_patient btn btn-xs btn-danger"/>
                                            </form>
                                        </td>
                                        <td>

                                            <a id = "relase_patient_no" href = "#relase_patient_no" class = " relase_patient_no btn btn-default btn-xs ">
                                                No
                                            </a>
                                        </td>
                                    </tr>
                                </table>




                            </div>
                        </div>
                    </div>
                    <!--/span-->



                </div>


            </div>
        </div>
    </div>
</div>



<div style="display: block;">


    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well">
                    <h2><i class="glyphicon glyphicon-th"></i> Patient Records</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                class="glyphicon glyphicon-chevron-up"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active"> <a href="#prescribe_medicine">Prescribe Medicine</a></li>
                        <li><a href="#dispense_medicine">Bill Patient</a></li>
                        <li><a href="#dispense_commodity_tab">Dispense Medicine</a></li>
                        <li> <a href="#allergies">Allergies</a></li>
                        <li> <a href="#tobacco_social">Tobacco/Social History</a></li>
                        <li><a href="#all_records_tab">All History</a></li>
                        <li><a href="#immunization">Immunization</a></li>
                        <li ><a href="#triage_tab">Triage</a></li>
                        <li><a href="#lab_tab">Lab</a></li>
                        <li><a href="#consultation_tab">Consultation</a></li>
                        <li><a href="#prescription_tab">Prescription</a></li>

                    </ul>

                    <div id="myTabContent" class="tab-content">










                        <!--Dispense Commodity tab start -->

                        <div class="tab-pane" id="dispense_commodity_tab">






                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Dispense Commodity </h2>

                                            <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">

                                                <div class="patient_dispense_div" id="patient_dispense_div">

                                                    <table class="table table-striped table-bordered  patient_dispense_table" id="patient_dispense_table">
                                                        <thead>
                                                            <tr>
                                                                <th>Commodity Name : </th>
                                                                <th>Visit Date : </th>
                                                                <th>Quantity Available : </th>
                                                                <th>Quantity Billed : </th>
                                                                <th>Batch No : </th>
                                                                <th>Is Dispensed ? :</th>
                                                                <th>Action : </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($dispensing_data as $value) {
                                                                ?>
                                                                <tr>
                                                                    <td class="center"><?php echo $value['commodity_name']; ?></td>
                                                                    <td><?php echo $value['date_added']; ?></td>
                                                                    <td class="center"> <?php echo $value['available_quantity']; ?></td>
                                                                    <td class="center"> <?php echo $value['quantity_billed']; ?></td>
                                                                    <td class="center"><?php echo $value['batch_no']; ?></td>
                                                                    <td class="center" ><?php echo $value['is_dispensed']; ?></td>
                                                                    <td>

                                                                        <input type="hidden" name="dispensing_patient_visit_statement_id" class="dispensing_patient_visit_statement_id hidden" id="dispensing_patient_visit_statement_id" value="<?php echo $value['patient_visit_statement_id']; ?>"/>
                                                                        <input type="hidden" name="dispensing_visit_id" class="dispensing_visit_id hidden" id="dispensing_visit_id" value="<?php echo $value['visit_id']; ?>"/>
                                                                        <input type="hidden" name="dipsensing_patient_id" class="dipsensing_patient_id hidden" id="dipsensing_patient_id" value="<?php echo $value['patient_id']; ?>"/>
                                                                        <input type="hidden" name="dispensing_prescription_id" class="dispensing_prescription_id hidden" id="dispensing_prescription_id" value="<?php echo $value['prescription_id']; ?>"/>
                                                                        <input type="hidden" name="dispensing_transaction_id" class="dispensing_transaction_id hidden" id="dispensing_transaction_id" value="<?php echo $value['transaction_id']; ?>"/>
                                                                        <input type="hidden" name="dispensing_stock_id" class="dispensing_stock_id hidden" id="dispensing_stock_id" value="<?php echo $value['stock_id']; ?>"/>
                                                                        <a id="dipsense_commodity_link" class="dipsense_commodity_link btn btn-default" href="#walkin_dipsense_commodity_form">Dispense Commodity</a>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>


                                                        </tbody>
                                                    </table>


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div><!--/row-->








                        </div>
                        <!--Dispense Commodity tab end -->












































                        <!--Tests List start -->
                        <div class="tab-pane active" id="prescribe_medicine">



                            <form class="form-inline add_walkin_commodity_form" id="add_walkin_commodity_form" name="add_walkin_commodity_form" method="post">

                                <input type="hidden" name="visit_id" value="<?php echo $this->uri->segment(4); ?>" class="test_patient_id hidden" id="test_patient_id"/>

                                <input type="hidden" name="patient_id" value="<?php echo $this->uri->segment(3); ?>" class="test_patient_id hidden" id="test_patient_id"/>

                                <table class="table table-striped table-bordered" style="width: 50%;">
                                    <tr>
                                        <td style="font-size:14px;">Commodity Name</td>
                                        <td style="font-size:14px;">Strength</td>
                                        <td style="font-size:14px;">Route</td>
                                        <td style="font-size:14px;">Frequency</td>
                                        <td style="font-size:14px;">Occurence</td>
                                        <td style="font-size:14px;">Duration</td>

                                    </tr>
                                    <tr>
                                        <td>      <div class="form-group">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Commodity Name : </label>
                                                    <div class="controls">
                                                        <select id="selectError" required="" name="commodity_name" data-rel="chosen">

                                                            <option>Please select commodity :</option>
                                                            <?php
                                                            foreach ($commodities as $value) {
                                                                ?>

                                                                <option value="<?php echo $value['commodity_name']; ?>"><?php echo $value['commodity_name']; ?></option>
                                                                <?php
                                                            }
                                                            ?>  
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </td>
                                        <td><div class="form-group">
                                                <label class="" for="inputStrength">Strength : </label><br>
                                                <input id="inputStrength" required="" class="inputStrength form-control" placeholder="e.g 500Mg" name="Strength" />
                                            </div></td>
                                        <td>
                                            <div class="form-group">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Route : </label>
                                                    <div class="controls">
                                                        <select id="selectError" required="" name="route" data-rel="chosen">
                                                            <option> Please select route : </option>
                                                            <?php
                                                            foreach ($route as $value) {
                                                                ?>

                                                                <option value="<?php echo $value['route_name']; ?>"><?php echo $value['route_name']; ?></option>
                                                                <?php
                                                            }
                                                            ?>  
                                                        </select>
                                                    </div>
                                                </div>
                                            </div></td>
                                        <td>
                                            <div class="form-group">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Frequency : </label>
                                                    <div class="controls">
                                                        <select id="selectError" required="" name="frequency" data-rel="chosen">
                                                            <option>Please select frequency : </option>
                                                            <?php
                                                            foreach ($frequency as $value) {
                                                                ?>

                                                                <option value="<?php echo $value['frequency_name']; ?>"><?php echo $value['frequency_name']; ?></option>
                                                                <?php
                                                            }
                                                            ?>  
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError"> Occurrence : </label>
                                                    <div class="controls">
                                                        <select id="selectError" required="" name="no_of_occurence" data-rel="chosen">
                                                            <option>Please select occurence: </option>
                                                            <?php
                                                            foreach ($occurence as $value) {
                                                                ?>

                                                                <option value="<?php echo $value['name']; ?>"><?php echo $value['name']; ?></option>
                                                                <?php
                                                            }
                                                            ?>  
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>

                                        <td><div class="form-group">
                                                <label class="" for="inputDuration">Duration : </label><br>
                                                <input id="inputDuration" required=""  class="inputDuration form-control" placeholder="e.g 5 Days" name="Duration" />
                                            </div></td>
                                    </tr>
                                </table>




                                <input type="hidden" name="order_commodity_patient_id" value="<?php echo $this->uri->segment(3); ?>"/>
                                <input type="hidden" name="order_commodiy_visit_id" value="<?php echo $this->uri->segment(4); ?>"/>
                                <hr>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="submit" value="Order Commodity" name="order_commodity_button" class="btn btn-xs btn-success" id="order_commodity_button"/>

                                    </div>
                                    <div class="form-group">
                                        <input type="reset" value="Cancel"  name="reset_order_commodity" class="btn btn-xs btn-danger" id="reset_order_commodity"/>

                                    </div>

                                </div>

                            </form>







                        </div>
                        <!-- Test List end -->


                        <!--Dispense Commodity tab start -->

<!--                        <div class="tab-pane" id="dispense_commodity_tab">






                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Dispense Commodity </h2>

                                            <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">

                                                <div class="patient_dispense_div" id="patient_dispense_div">

                                                    <table class="table table-striped table-bordered  patient_dispense_table" id="patient_dispense_table">
                                                        <thead>
                                                            <tr>
                                                                <th>Commodity Name : </th>
                                                                <th>Visit Date : </th>
                                                                <th>Quantity Available : </th>
                                                                <th>Quantity Billed : </th>
                                                                <th>Batch No : </th>
                                                                <th>Is Dispensed ? :</th>
                                                                <th>Action : </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($dispensing_data as $value) {
                                                                ?>
                                                                <tr>
                                                                    <td class="center"><?php echo $value['commodity_name']; ?></td>
                                                                    <td><?php echo $value['date_added']; ?></td>
                                                                    <td class="center"> <?php echo $value['available_quantity']; ?></td>
                                                                    <td class="center"> <?php echo $value['quantity_billed']; ?></td>
                                                                    <td class="center"><?php echo $value['batch_no']; ?></td>
                                                                    <td class="center" ><?php echo $value['is_dispensed']; ?></td>
                                                                    <td>

                                                                        <input type="hidden" name="dispensing_patient_visit_statement_id" class="dispensing_patient_visit_statement_id hidden" id="dispensing_patient_visit_statement_id" value="<?php echo $value['patient_visit_statement_id']; ?>"/>
                                                                        <input type="hidden" name="dispensing_visit_id" class="dispensing_visit_id hidden" id="dispensing_visit_id" value="<?php echo $value['visit_id']; ?>"/>
                                                                        <input type="hidden" name="dipsensing_patient_id" class="dipsensing_patient_id hidden" id="dipsensing_patient_id" value="<?php echo $value['patient_id']; ?>"/>
                                                                        <input type="hidden" name="dispensing_prescription_id" class="dispensing_prescription_id hidden" id="dispensing_prescription_id" value="<?php echo $value['prescription_id']; ?>"/>
                                                                        <input type="hidden" name="dispensing_transaction_id" class="dispensing_transaction_id hidden" id="dispensing_transaction_id" value="<?php echo $value['transaction_id']; ?>"/>
                                                                        <input type="hidden" name="dispensing_stock_id" class="dispensing_stock_id hidden" id="dispensing_stock_id" value="<?php echo $value['stock_id']; ?>"/>
                                                                        <a id="dipsense_commodity_link" class="dipsense_commodity_link btn btn-default" href="#dipsense_commodity_form">Dispense Commodity</a>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>


                                                        </tbody>
                                                    </table>


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                /span

                            </div>/row








                        </div>-->
                        <!--Dispense Commodity tab end -->




                        <!--Test results start--> 
                        <div class="tab-pane " id="dispense_medicine">



                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Bill Patient</h2>

                                            <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">

                                                <table class="table table-striped table-bordered bill_patient_prescription_table " id="bill_patient_prescription_table">
                                                    <thead>
                                                        <tr>
                                                            <th>No . </th>
                                                            <th>Visit Date</th>
                                                            <th>Commodity Name</th>
                                                            <th>Strength</th>
                                                            <th>Route </th>
                                                            <th>Frequency</th>
                                                            <th>Duration</th>
                                                            <th>Employee Name</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <th>No . </th>
                                                            <th>Visit Date</th>
                                                            <th>Commodity Name</th>
                                                            <th>Strength</th>
                                                            <th>Route </th>
                                                            <th>Frequency</th>
                                                            <th>Duration</th>
                                                            <th>Employee Name</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </tfoot>
                                                    <tbody>
                                                        <?php
                                                        $i = 1;
                                                        foreach ($bill_patient_records as $value) {
                                                            ?>
                                                            <tr>
                                                                <td class="center"><?php echo $i; ?></td>
                                                                <td class="center"><?php echo $value['date']; ?></td>
                                                                <td class="center"><?php echo $value['commodity_name']; ?></td>
                                                                <td class="center"><?php echo $value['strength']; ?></td>
                                                                <td class="center"><?php echo $value['route']; ?> </td>
                                                                <td class="center"><?php echo $value['frequency']; ?></td>
                                                                <td class="center"> <?php echo $value['duration'] . $value['frequency']; ?></td>
                                                                <td class="center"> <?php echo $value['employee_name']; ?></td>
                                                                <td><input type="hidden" name="prescription_id" id="bill_patient_prescription_id" value="<?php echo $value['prescription_id']; ?>" class="bill_patient_prescription_id"/>
                                                                    <input type="hidden" name="hidden_prescription_tracker" id="hidden_prescription_tracker" value="<?php echo $value['prescription_tracker']; ?>" class="hidden_prescription_tracker hidden"/>
                                                                    <?php
                                                                    $billed = $value['billed'];

                                                                    if ($billed === 'Yes') {
                                                                        ?>
                                                                        <span class=" alert-info ">Patient Billed</span>
                                                                        <a class="btn btn-xs btn-info bill_patient_prescription_link" id="bill_patient_prescription_link" href="#bill_patient_prescription_form">Edit Bill </a>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <a class="btn btn-xs btn-info bill_patient_prescription_link" id="bill_patient_prescription_link" href="#bill_patient_prescription_form">Bill Patient</a>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $i++;
                                                        }
                                                        ?>


                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div><!--/row-->




                        </div>
                        <!--                         Test Results end -->



                        <!--- Shwari test notes  form  start -->

                        <div id = "add_test_option_div" class="add_test_option_div" style = "display: none;">


                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                        <div class = "box-icon">

                                        </div>
                                    </div>

                                    <div class = "box-content">
                                        <form id="add_walkin_test_notes_form" class="add_walkin_test_notes_form " >
                                            <input type="hidden" name="add_test_result_id" class="add_test_result_id hidden form-control" id="add_test_result_id" />
                                            <div class="alert alert-info" style="height: 50px; width: 250px;">
                                                (Information saved real-time):
                                            </div>
                                            <div class="form-inline">

                                                <div class="form-group">

                                                    <textarea id="add_test_notes" class="add_test_notes textarea form-control" name="add_test_notes"></textarea>  
                                                </div>
                                            </div>
                                        </form>


                                    </div>
                                </div>
                            </div>
                            <!--/span-->


                        </div>
                        <!--- Shwari test notes  form  end -->














                        <!-- Allergies start -->
                        <div class="tab-pane" id="allergies">

                            <form class="add_allergy_form" id="add_allergy_form">
                                <input type="hidden" name="add_allergy_patient_id" id="" value="<?php echo $this->uri->segment(3); ?>"/>
                                <input type="hidden" name="add_allergy_visit_id" id="" value="<?php echo $this->uri->segment(4); ?>"/>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "allergy">Allergy : </label>
                                        <textarea  class = "form-control" name="allergy" id ="allergy"  placeholder = "Allergy"></textarea>
                                    </div>
                                </div>
                                <hr>

                            </form>

                        </div>
                        <!-- Allergies end -->
                        <!-- Tobbacco Social Tab start-->

                        <div class="tab-pane" id="tobacco_social">
                            <form class="add_family_social_form" id="add_family_social_form">
                                <input type="hidden" name="add_family_social_patient_id" id="" value="<?php echo $this->uri->segment(3); ?>"/>
                                <input type="hidden" name="add_family_social_visit_id" id="" value="<?php echo $this->uri->segment(4); ?>"/>

                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "family_social">Family Social History  : </label>
                                        <textarea  class = "form-control" name="family_social" id ="family_social"  placeholder = "Family Social History"></textarea>
                                    </div>
                                </div>
                                <hr>


                            </form>
                        </div>

                        <!-- Tobbacco Social Tab end --> 

                        <!-- Family Social History form tab start -->

                        <div class="tab-pane" id="family_social_history_form_tab">
                            <form class="family_social_history_form" id="family_social_history_form">
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "allergy">Family Social History  : </label>
                                        <textarea  class = "form-control" name="family_social" id ="family_social"  placeholder = "Family Social History"></textarea>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="submit" name="add_family_social_button" class="add_family_social_button btn btn-success btn-xs" id="add_allergy_button" value="Save"/>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <!-- Family Social History  form tab  end -->

                        <!--All records tab start -->

                        <div class="tab-pane" id="all_records_tab">

                            <div class="accordion" id="accordion1">

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseTriage"><i class="glyphicon glyphicon-plus"></i> 
                                            Triage Records : 
                                        </a>
                                    </div>

                                    <div id="collapseTriage" class="accordion-body collapse">

                                        <?php
                                        foreach ($patient_triage_records as $value) {
                                            ?>
                                            <div class="accordion-inner">

                                                Here we insert another nested accordion 

                                                <div class="accordion" id="accordion2">
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo 'triage' . $value['triage_id']; ?>">
                                                                <i class="glyphicon glyphicon-plus"></i> <span> <?php
                                                                    $weight = $value['weight'];
                                                                    $height = $value['height'];
                                                                    $one_hundred = 100;
                                                                    $height = $height / $one_hundred;
                                                                    $new_height = $height * $height;


                                                                    if ($new_height == 0) {
                                                                        ?>
                                                                        <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                                                        <?php
                                                                    } else {
                                                                        $BMI = $weight / $new_height;
                                                                        if ($BMI <= 20) {
                                                                            ?>
                                                                            <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                        <?php } ?>

                                                                        <?php if ($BMI >= 25) { ?>
                                                                            <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                        <?php } ?>

                                                                        <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                                            <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>

                                                                </span>
                                                            </a>
                                                        </div>
                                                        <div id="<?php echo 'triage' . $value['triage_id']; ?>" class="accordion-body collapse in">
                                                            <div class="accordion-inner">


                                                                <ul class="list-inline">
                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Visit Date :</label>  <?php
                                                                        echo $value['visit_date'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Posted by : </label>  <?php
                                                                        echo $value['employee_name'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Respiratory Rate :(No/Min)</label>  <?php
                                                                        echo $value['respiratory_rate'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Pulse Rate :(No/Min)</label> <?php
                                                                        echo $value['pulse_rate'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Blood Pressure :(No/Min)</label> <?php
                                                                        echo $value['diastolic'] . " / " . $value['systolic'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Temperature :(°C)</label> <?php
                                                                        echo $value['temperature'];
                                                                        ?></li>
                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Height :(Cm)</label> <?php
                                                                        echo $value['height'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Weight :(Kg)</label> <?php
                                                                        echo $value['weight'];
                                                                        ?></li>

                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>          

                                                Inner accordion ends here 

                                            </div>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>

                            <hr>


                            <div class="accordion" id="accordion1">

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseLab"><i class="glyphicon glyphicon-plus"></i> 
                                            Lab Records : 
                                        </a>
                                    </div>

                                    <div id="collapseLab" class="accordion-body collapse">

                                        <?php
                                        foreach ($patient_lab_records as $value) {
                                            ?>
                                            <div class="accordion-inner">

                                                Here we insert another nested accordion 

                                                <div class="accordion" id="accordion2">
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['lab_test_id']; ?>">
                                                                <i class="glyphicon glyphicon-plus"></i><span> <?php
                                                                    echo $value['date_added'] . '   Test Name : ' . $value['test_name'];
                                                                    ?></span>
                                                            </a>
                                                        </div>
                                                        <div id="<?php echo $value['lab_test_id']; ?>" class="accordion-body collapse in">
                                                            <div class="accordion-inner">

                                                                <ul class="list-inline">


                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Posted by : </label>  <?php
                                                                        echo $value['employee_name'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Test Name</label>  <?php
                                                                        echo $value['test_name'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Test Results : </label> <?php
                                                                        echo $value['test_results'];
                                                                        ?></li>


                                                                </ul>


                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>          

                                                Inner accordion ends here 

                                            </div>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>
                            <hr>

                            <div class="accordion" id="accordion1">

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseConsultation"><i class="glyphicon glyphicon-plus"></i> 
                                            Consultation Records : 
                                        </a>
                                    </div>

                                    <div id="collapseConsultation" class="accordion-body collapse">

                                        <?php
                                        foreach ($patient_consultation_records as $value) {
                                            ?>
                                            <div class="accordion-inner">

                                                Here we insert another nested accordion 

                                                <div class="accordion" id="accordion2">
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['consultation_id']; ?>">
                                                                <i class="glyphicon glyphicon-plus"></i><span> <?php echo $value['date']; ?></span>
                                                            </a>
                                                        </div>
                                                        <div id="<?php echo $value['consultation_id']; ?>" class="accordion-body collapse in">
                                                            <div class="accordion-inner">

                                                                <ul class="list-inline">


                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Posted by : </label>  <?php
                                                                        echo $value['employee_name'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Chief Complaint : </label>  <?php
                                                                        echo $value['complaints'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Working Diagnosis :</label> <?php
                                                                        echo $value['working_diagnosis'];
                                                                        ?></li>




                                                                </ul>


                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>          

                                                Inner accordion ends here 

                                            </div>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>

                            <hr>


                            <div class="accordion" id="accordion1">

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapsePrescription"><i class="glyphicon glyphicon-plus"></i> 
                                            Prescription Records : 
                                        </a>
                                    </div>

                                    <div id="collapsePrescription" class="accordion-body collapse">

                                        <?php
                                        foreach ($patient_prescription_records as $value) {
                                            ?>
                                            <div class="accordion-inner">

                                                Here we insert another nested accordion 

                                                <div class="accordion" id="accordion2">
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['prescription_id']; ?>">
                                                                <i class="glyphicon glyphicon-plus"></i><span> <?php echo $value['date']; ?></span>
                                                            </a>
                                                        </div>
                                                        <div id="<?php echo $value['prescription_id']; ?>" class="accordion-body collapse in">
                                                            <div class="accordion-inner">

                                                                <ul class="list-inline">


                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Posted by : </label>  <?php
                                                                        echo $value['employee_name'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right">
                                                                        <label class="label label-default">Commodity Name : </label>  <?php
                                                                        echo $value['commodity_name'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Strength :</label> <?php
                                                                        echo $value['strength'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Route : </label> <?php
                                                                        echo $value['route'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Frequency : </label> <?php
                                                                        echo $value['frequency'];
                                                                        ?></li>
                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Duration : </label> <?php
                                                                        echo $value['duration'];
                                                                        ?></li>

                                                                    <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">No of Days : </label> <?php
                                                                        echo $value['no_of_days'];
                                                                        ?></li>



                                                                </ul>


                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>          

                                                Inner accordion ends here 

                                            </div>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <!--All records tab end -->

                        <div class="tab-pane" id="immunization">
                            <form class="add_immunization_form" id="add_immunization_form">
                                <input type="hidden" name="add_immunization_patient_id" id="" value="<?php echo $this->uri->segment(3); ?>"/>
                                <input type="hidden" name="add_immunization_visit_id" id="" value="<?php echo $this->uri->segment(4); ?>"/>

                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "immunization_txt_area">Immunization   : </label>
                                        <textarea  class = "form-control immunization_txt_area" name="immunization" id ="immunization_txt_area"  placeholder = "Immunization"></textarea>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="submit" name="add_immunization_button" class="add_immunization_button btn btn-success btn-xs" id="add_immunization_button" value="Save"/>
                                    </div>
                                </div>

                            </form>
                        </div>


                        <!---Triage Tab --->

                        <div class="tab-pane" id="triage_tab">

                            <a  id="export_triage_patient_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Walkin Patient Report" class="export_triage_patient_report_filter_link" href="#export_triage_report_filter_form">
                                <i class="glyphicon glyphicon-download-alt"></i>
                            </a>
                            |



                            <table class="table table-striped table-bordered triage_report  responsive">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Date </th>
                                        <th>Nurse Name</th>
                                        <th>Respiratory (No/Min)</th>
                                        <th>Pulse Rate (No/Min)</th>
                                        <th>Blood pressure (mmHg)</th>
                                        <th>Temperature (°C)</th>
                                        <th>Height (Cm)</th>
                                        <th>Weight (KGs)</th>
                                        <th>BMI</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Date </th>
                                        <th>Nurse Name</th>
                                        <th>Respiratory (No/Min)</th>
                                        <th>Pulse Rate (No/Min)</th>
                                        <th>Blood pressure (mmHg)</th>
                                        <th>Temperature (°C)</th>
                                        <th>Height (Cm)</th>
                                        <th>Weight (KGs)</th>
                                        <th>BMI</th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr>
                                        <?php
                                        $i = 1;
                                        if (empty($patient_triage_records)) {
                                            ?>
                                            <td>Empty</td>
                                            <td>Empty </td>
                                            <td>Empty</td>
                                            <td>Empty </td>
                                            <td>Empty</td>
                                            <td>Empty</td>
                                            <td>Empty</td>
                                            <td>Empty</td>
                                            <td>Empty</td>
                                            <td>Empty</td>
                                            <td>Empty</td>
                                            <?php
                                        } else {


                                            foreach ($patient_triage_records as $value) {
                                                ?>


                                                <td class="center"><?php echo $i; ?></td>
                                                <td class="center">
                                                    <?php
                                                    $visit_date = $value['visit_date'];
                                                    if (empty($visit_date)) {
                                                        echo 'Empty';
                                                    } else {
                                                        echo $visit_date;
                                                    }
                                                    ?>
                                                </td>
                                                <td class="center">
                                                    <?php
                                                    $employee_name = $value['employee_name'];
                                                    if (empty($employee_name)) {
                                                        echo 'Empty';
                                                    } else {
                                                        echo $employee_name;
                                                    }
                                                    ?>
                                                </td>
                                                <td class="center">
                                                    <?php
                                                    $respiratory_rate = $value['respiratory_rate'];
                                                    if (empty($respiratory_rate)) {
                                                        echo 'Empty';
                                                    } else {
                                                        echo $respiratory_rate;
                                                    }
                                                    ?>
                                                </td>
                                                <td class="center">
                                                    <?php
                                                    $pulse_rate = $value['pulse_rate'];
                                                    if (empty($pulse_rate)) {
                                                        echo 'Empty';
                                                    } else {
                                                        echo $pulse_rate;
                                                    }
                                                    ?>
                                                </td>
                                                <td class="center">
                                                    <?php
                                                    $diastolic_systolic = $value['diastolic'] . "/" . $value['systolic'];
                                                    $diastolic = $value['diastolic'];
                                                    $systolic = $value['systolic'];
                                                    if (empty($diastolic) or empty($systolic)) {
                                                        echo 'Empty';
                                                    } else {
                                                        echo $diastolic_systolic;
                                                    }
                                                    ?>
                                                </td>
                                                <td class="center">
                                                    <?php
                                                    $temperature = $value['temperature'];
                                                    if (empty($temperature)) {
                                                        echo 'Empty';
                                                    } else {
                                                        echo $temperature;
                                                    }
                                                    ?>
                                                </td>
                                                <td class="center">
                                                    <?php
                                                    $height = $value['height'];
                                                    if (empty($height)) {
                                                        echo 'Empty';
                                                    } else {
                                                        echo $height;
                                                    }
                                                    ?>
                                                </td>
                                                <td class="center">
                                                    <?php echo $value['weight']; ?>
                                                </td>
                                                <td class="center" ><?php
                                                    $weight = $value['weight'];
                                                    $height = $value['height'];
                                                    $one_hundred = 100;
                                                    $height = $height / $one_hundred;
                                                    $new_height = $height * $height;


                                                    if ($new_height == 0) {
                                                        ?>
                                                        <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                                        <?php
                                                    } elseif (!empty($new_height) and ! empty($weight)) {
                                                        $BMI = $weight / $new_height;
                                                        if ($BMI <= 20) {
                                                            ?>
                                                            <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                        <?php } ?>

                                                        <?php if ($BMI >= 25) { ?>
                                                            <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                        <?php } ?>

                                                        <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                            <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                            <?php
                                                        }
                                                    } elseif (empty($new_height) or empty($weight)) {
                                                        echo 'BMI Cannot be calculated...';
                                                    }
                                                    ?>



                                                </td> 

                                                <td class="center">
                                                    <?php
                                                    $triage_id = $value['triage_id'];
                                                    if (empty($triage_id)) {
                                                        echo 'Empty';
                                                    } else {
                                                        ?>
                                                        <input type="hidden" name="view_triage_patient_id" class="view_triage_patient_id" id="view_triage_patient_id" value="<?php echo $value['patient_id']; ?>"/>
                                                        <input type="hidden" name="view_triage_id" class="view_triage_id" id="view_triage_id" value="<?php echo $value['triage_id']; ?>"/>
                                                        <input type="hidden" name="view_triage_visit_id" class="view_triage_visit_id" id="view_triage_visit_id" value="<?php echo $value['visit_id']; ?>"/>
                                                        <a  id="edit_triage_patient_link" class="edit_triage_patient_link" href="#edit_triage_form">
                                                            <i class="glyphicon glyphicon-edit icon-white"></i>

                                                        </a>|
                                                        <a  id="delete_triage_patient_link" class="delete_triage_patient_link" href="#delete_triage_form">
                                                            <i class="glyphicon glyphicon-trash icon-white"></i>

                                                        </a>
                                                        <?php
                                                    }
                                                    ?>

                                                </td>

                                                <?php
                                                ?>


                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </tr>



                                </tbody>
                            </table>


                        </div>



                        <!-- Triage tab end -->


                        <div class="tab-pane" id="lab_tab">

                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                <thead>
                                    <tr>
                                        <th>Test Date</th>
                                        <th>Doctor Name</th>
                                        <th>Test Name</th>
                                        <th>Test Results </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($patient_lab_records as $value) {
                                        ?>
                                        <tr>
                                            <td class="center"><?php echo $value['date_added']; ?></td>
                                            <td class="center"><?php echo $value['employee_name']; ?></td>
                                            <td class="center"><?php echo $value['test_name']; ?></td>
                                            <td class="center">
                                                <?php echo $value['test_results']; ?>
                                            </td>

                                        </tr>
                                    <?php }
                                    ?>


                                </tbody>
                            </table>

                        </div>

                        <div class="tab-pane" id="consultation_tab">
                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                <thead>
                                    <tr>
                                        <th>Visit Date</th>
                                        <th> Chief Complaints</th>
                                        <th>Diagnosis</th>
                                        <th>Employee Name</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($patient_consultation_records as $value) {
                                        ?>
                                        <tr>
                                            <td><?php echo $value['date']; ?></td>
                                            <td class="center"><?php echo $value['complaints']; ?></td>
                                            <td class="center"> <?php echo $value['working_diagnosis']; ?></td>
                                            <td class="center"<?php echo $value['employee_name']; ?>></td>

                                        </tr>
                                        <?php
                                    }
                                    ?>


                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="prescription_tab">
                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                <thead>
                                    <tr>
                                        <th>Visit Date</th>
                                        <th>Commodity Name</th>
                                        <th>Strength</th>
                                        <th>Route </th>
                                        <th>Frequency</th>
                                        <th>Duration</th>
                                        <th>Employee Name</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($patient_prescription_records as $value) {
                                        ?>
                                        <tr>
                                            <td><?php echo $value['date']; ?></td>
                                            <td class="center"><?php echo $value['commodity_name']; ?></td>
                                            <td class="center"><?php echo $value['strength']; ?></td>
                                            <td class="center"><?php echo $value['route']; ?> </td>
                                            <td class="center"><?php echo $value['frequency']; ?></td>
                                            <td class="center"> <?php echo $value['duration'] . $value['no_of_days']; ?></td>
                                            <td class="center"> <?php echo $value['employee_name']; ?></td>

                                        </tr>
                                    <?php }
                                    ?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->


    </div><!--/row-->

</div>




