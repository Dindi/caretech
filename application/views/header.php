<!DOCTYPE html>
<html lang="en">
    <head>
        <!--
          
        -->
        <meta charset="utf-8">
        <title>Care-tech System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
        <meta name="author" content="Muhammad Usman">

        <!-- The styles -->
        <link id="bs-css" href="<?php echo base_url(); ?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/charisma-app.css" rel="stylesheet">
        <link href='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
        <link href='<?php echo base_url(); ?>assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/jquery.noty.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/noty_theme_default.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/elfinder.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/elfinder.theme.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/uploadify.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/animate.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/jquery.timepicker.css' rel="stylesheet">
        <link href='<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css' rel="stylesheet">



        <link href='<?php echo base_url(); ?>assets/chosen/chosen.css' rel="stylesheet">
        <link href='<?php echo base_url(); ?>assets/chosen/chosen.min.css' rel="stylesheet">

        <link href='<?php echo base_url(); ?>assets/chosen/docsupport/prism.css' rel="stylesheet">

        <!--
                <link href='<?php echo base_url(); ?>assets/chosen/chosen.css' rel="stylesheet">
        
        
                <link href='<?php echo base_url(); ?>assets/jquery-tokeninput/styles/token-input-mac.css' rel="stylesheet">
        
                <link href='<?php echo base_url(); ?>assets/jquery-tokeninput/styles/token-input.css' rel="stylesheet">
        
        -->



        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/jquery.min.js"></script>

        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->


        <!--
        Scripting starts here ...-->

        <script type="text/javascript">
            $(document).ready(function () {

                $('.visit_type').on('change', function () {

                    $visit_type = (this.value);
                    if ($visit_type === "follow_up") {
                        // code to be executed if condition is true

                        $('#previous_visit_div').css('display', 'inline-block');
                        $('#queue_to_join_div').css('display', 'inline-block');

                        $('.queue_to_join').on('change', function () {
                            var doctor = this.value;
                            if (doctor === 'Doctor') {
                                $('#active_doctor_list_div').css('display', 'inline-block');
                            } else {
                                $('#active_doctor_list_div').css('display', 'none');
                            }
                        });
                    }
                    else {
                        // code to be executed if condition is false
                        $('#previous_visit_div').css('display', 'none');
                        $('#queue_to_join_div').css('display', 'none');
                        $('#active_doctor_list_div').css('display', 'none');
                        $('.queue_to_join').val("");
                        $('.previous_visit_div').val("");
                    }

                });

                $('#patient_appointments').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });





                $('#register_patient').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#not_joining').fancybox({
                    paddding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });


                $('#shwari_relation_yes').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });


                $('#add_test_results_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 940,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });



                $('#bill_patient_prescription_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 940,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });



                $('#order_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 940,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });



                $('#lab_send_to_doctor_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 940,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });





                $('#shwari_relation_no').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#visit_records').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });



                $('#patient_procedure_option').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#regular_patient_yes').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#regular_patient_no').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });


                $('#patient_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });



                $('#export_patient_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });

                $('#export_vistation_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });


                $('#export_patient_procedure_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });



                $('#export_walkin_patient_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });

                $('#walkin_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#visit_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#regular_procedure_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#walkin_procedure_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#view_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#edit_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#delete_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#patient_medical_records_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                })


                $('#view_procedure_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#edit_procedure_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#delete_procedure_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });



                $('#view_visit_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#edit_visit_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#delete_visit_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });


                $('#view_walkin_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#edit_walkin_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#delete_walkin_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                //regular payment link
                $('#regular_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                //lab service payment link
                $('#lab_service_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                //pay at the ned link 
                $('#pay_at_the_end_patient_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });


                $('#walkin_nurse_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#walkin_lab_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#release_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });


                $('#walkin_pharmacy_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });


                $('#add_triage_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#edit_triage_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#delete_triage_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#nurse_view_patient_list').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#send_to_doctor_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#book_appointment_form_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });


                $('#test_referral_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });





                $('#imaging_referral_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });




                $('#other_referral_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });


                $('#sick_off_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });







            });


            $('.insurance_exists').on('change', function () {
                alert(this.value);
            });
            $("#save_submit").click(function () {

            });
            $("#save_submit_1").click(function () {

            });

            //delegated submit handlers for the forms inside the table
            $('#save_submit_1').on('click', function (e) {
                e.preventDefault();




                //read the form data ans submit it to someurl
                $.post('visit.html', $('#patient_registration_form_1').serialize(), function () {

                    generateAll();
                    setInterval(function () {
                        var url = "<?php echo base_url() ?>index.php/home";
                        $(location).attr('href', url);
                    }, 30000);
                }).fail(function () {
                    //error do something
                    $(".save_timesheet_notify").notify(
                            "There was an error please try again later or  contact the system support desk  for assistance",
                            "error",
                            {position: "left"}
                    );
                });
            });



            function generate(layout) {
                var n = noty({
                    text: 'Do you want to continue?',
                    type: 'alert',
                    dismissQueue: true,
                    layout: layout,
                    theme: 'defaultTheme',
                    buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                                $noty.close();
                                noty({dismissQueue: true, force: true, layout: layout, theme: 'defaultTheme', text: 'You clicked "Ok" button', type: 'success'});
                            }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                                $noty.close();
                                noty({dismissQueue: true, force: true, layout: layout, theme: 'defaultTheme', text: 'You clicked "Cancel" button', type: 'error'});
                            }
                        }
                    ]
                });
                console.log('html: ' + n.options.id);
            }

            function generateAll() {
                generate('center');
            }



        </script>

        <!--
        Scripting ends here
        -->

        <!-- The fav icon -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.ico">

    </head>

    <body>
        <?php
        if (!isset($no_visible_elements) || !$no_visible_elements) {
            if ($this->session->userdata('id')) {
                ?>
                <!-- topbar starts -->
                <!--                                <div class="navbar navbar-default" role="navigation">
                                
                                                    <div class="navbar-inner">
                                                        <button type="button" class="navbar-toggle pull-left animated flip">
                                                            <span class="sr-only">Toggle navigation</span>
                                                            <span class="icon-bar"></span>
                                                            <span class="icon-bar"></span>
                                                            <span class="icon-bar"></span>
                                                        </button>
                                                        <a class="navbar-brand" href="index.html"> <img alt="Charisma Logo" src="<?php echo base_url(); ?>assets/img/logo20.png" class="hidden-xs"/>
                                                            <span>Charisma</span></a>
                                
                                                         user dropdown starts 
                                                        <div class="btn-group pull-right">
                                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                                                                <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#">Profile</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="login.html">Logout</a></li>
                                                            </ul>
                                                        </div>
                                                         user dropdown ends 
                                
                                                         theme selector starts 
                                                        <div class="btn-group pull-right theme-container animated tada">
                                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                <i class="glyphicon glyphicon-tint"></i><span
                                                                    class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                                                                <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu" id="themes">
                                                                <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                                                                <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                                                                <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                                                                <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                                                                <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                                                                <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                                                                <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                                                                <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                                                                <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                                                            </ul>
                                                        </div>
                                                         theme selector ends 
                                
                                                        <ul class="collapse navbar-collapse nav navbar-nav top-menu">
                                                            <li><a href="#"><i class="glyphicon glyphicon-globe"></i> Visit Site</a></li>
                                                            <li class="dropdown">
                                                                <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> Dropdown <span
                                                                        class="caret"></span></a>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="#">Action</a></li>
                                                                    <li><a href="#">Another action</a></li>
                                                                    <li><a href="#">Something else here</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="#">Separated link</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="#">One more separated link</a></li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <form class="navbar-search pull-left">
                                                                    <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                                                                           type="text">
                                                                </form>
                                                            </li>
                                                        </ul>
                                
                                                    </div>
                                                </div>-->


                <div class="navbar navbar-default" role="navigation">

                    <div class="navbar-inner">
                        <button type="button" class="navbar-toggle pull-left animated flip">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url(); ?>home"> <img alt="Shwari Healthcare" src="<?php echo base_url(); ?>assets/img/shwari.jpg" class="hidden-xs"/>
                            <span>Shwari</span></a>

                        <!-- user dropdown starts -->
                        <div class="btn-group pull-right">
                            <?php
                            $type = $this->session->userdata('type');
                            if ($type === "Support") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Reception</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                            } elseif ($type === "Nurse") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Nurse</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                                ?>  <?php
                            } elseif ($type === "Laboratory") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Doctor</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                                ?><?php
                                ?>  <?php
                            } elseif ($type === "Doctor") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Doctor</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                            } elseif ($type === "Pharmacy") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Doctor</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                            }
                            ?>
                            <ul class="dropdown-menu">
                                <li><a href="#">Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>home/do_logout">Logout</a></li>
                            </ul>
                        </div>



                    </div>
                </div>
                <!-- topbar ends -->
                <?php
            }
        } else {
            
        }
        ?>
        <div class="ch-container">
            <div class="row">
                <?php
                if (!isset($no_visible_elements) || !$no_visible_elements) {


                    if ($this->session->userdata('id')) {
                        if ($type === "Support") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>
                                            <li><a class="ajax-link" id="register_patient" href="#patient_option"><i class="glyphicon glyphicon-edit"></i><span> Register Patient</span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reception/visit"><i class="glyphicon glyphicon-book"></i><span> Book Visit</span></a></li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reception/walkin_patient"><i class="glyphicon glyphicon-plus-sign"></i><span> Add Walkin</span></a></li>

                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier"><i class="glyphicon glyphicon-check"></i><span> Payments</span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reports/visitation_report"><i class="glyphicon glyphicon-check"></i><span> Edit visitation </span></a>
                                            </li>

                                            <li><a class="ajax-link" id="patient_procedure_option" href="#procedure_option"><i class="glyphicon glyphicon-check"></i><span> Book patient for Procedure </span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier/regular_patient_visit_statement"><i class="glyphicon glyphicon-check"></i><span> Regular Patient Visit Statements </span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier/walkin_patient_visit_statement"><i class="glyphicon glyphicon-check"></i><span> Walk-in Patient Visit Statements  </span></a>
                                            </li>

                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        } elseif ($type === "Nurse") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>



                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>stock/manage_stock"><i class="glyphicon glyphicon-check"></i><span> Stock Management </span></a>
                                            </li>

                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        } elseif ($type === "Laboratory") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>
                                            <li><a class="ajax-link" id="register_patient" href="#patient_option"><i class="glyphicon glyphicon-edit"></i><span> Register Patient</span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reception/visit"><i class="glyphicon glyphicon-book"></i><span> Book Visit</span></a></li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reception/walkin_patient"><i class="glyphicon glyphicon-plus-sign"></i><span> Add Walkin</span></a></li>

                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier"><i class="glyphicon glyphicon-check"></i><span> Payments</span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reports/visitation_report"><i class="glyphicon glyphicon-check"></i><span> Edit visitation </span></a>
                                            </li>

                                            <li><a class="ajax-link" id="patient_procedure_option" href="#procedure_option"><i class="glyphicon glyphicon-check"></i><span> Book patient for Procedure </span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier/regular_patient_visit_statement"><i class="glyphicon glyphicon-check"></i><span> Regular Patient Visit Statements </span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier/walkin_patient_visit_statement"><i class="glyphicon glyphicon-check"></i><span> Walk-in Patient Visit Statements  </span></a>
                                            </li>

                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        } elseif ($type === "Doctor") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>
                                            <li><a class="ajax-link" id="register_patient" href="#patient_option"><i class="glyphicon glyphicon-edit"></i><span> Register Patient</span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reception/visit"><i class="glyphicon glyphicon-book"></i><span> Book Visit</span></a></li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reception/walkin_patient"><i class="glyphicon glyphicon-plus-sign"></i><span> Add Walkin</span></a></li>

                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier"><i class="glyphicon glyphicon-check"></i><span> Payments</span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reports/visitation_report"><i class="glyphicon glyphicon-check"></i><span> Edit visitation </span></a>
                                            </li>

                                            <li><a class="ajax-link" id="patient_procedure_option" href="#procedure_option"><i class="glyphicon glyphicon-check"></i><span> Book patient for Procedure </span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier/regular_patient_visit_statement"><i class="glyphicon glyphicon-check"></i><span> Regular Patient Visit Statements </span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier/walkin_patient_visit_statement"><i class="glyphicon glyphicon-check"></i><span> Walk-in Patient Visit Statements  </span></a>
                                            </li>

                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        } elseif ($type === "Pharmacy") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>

                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>pharmacy/commodity_management"><i class="glyphicon glyphicon-shopping-cart"></i><span>Commodity Management</span></a></li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>pharmacy/stock_management"><i class="glyphicon glyphicon-shopping-cart"></i><span>Stock Management </span></a></li>

                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier"><i class="glyphicon glyphicon-check"></i><span> Payments</span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reports/visitation_report"><i class="glyphicon glyphicon-check"></i><span> Edit visitation </span></a>
                                            </li>

                                            <li><a class="ajax-link" id="patient_procedure_option" href="#procedure_option"><i class="glyphicon glyphicon-check"></i><span> Book patient for Procedure </span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier/regular_patient_visit_statement"><i class="glyphicon glyphicon-check"></i><span> Regular Patient Visit Statements </span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier/walkin_patient_visit_statement"><i class="glyphicon glyphicon-check"></i><span> Walk-in Patient Visit Statements  </span></a>
                                            </li>

                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        }
                        ?>

                        <!-- left menu starts -->



                        <!--/span-->
                        <!-- left menu ends -->

                        <noscript>
                        <div class="alert alert-block col-md-12">
                            <h4 class="alert-heading">Warning!</h4>

                            <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                                enabled to use this site.</p>
                        </div>
                        </noscript>






                        <div id = "procedure_option" style = "display: none;">


                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                        <div class = "box-icon">

                                        </div>
                                    </div>

                                    <div class = "box-content">
                                        <label class = "">
                                            Are you a regular patient to any of our Clinics ?
                                        </label><br>

                                        <a id = "regular_patient_yes" class = "btn btn-info btn-sm regular_patient_yes" href ="#shwari_regular_client">
                                            Yes
                                        </a>

                                        <a id = "regular_patient_no" href = "#not_regular_patient" class = "btn btn-info btn-sm regular_patient_no">
                                            No
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->


                        </div>




                        <div id = "not_interested_in_joining" style = "display: none;">

                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Book Procedure</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" id="book_patient_procedure_form" autocomplete="off" class="form-inline  book_patient_procedure_form">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Patient Name (required) </label>

                                                    <div class="controls">
                                                        <input type="text" name="patientname" id="patientname" class="patientname form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Phone Number :  (required)</label>

                                                    <div class="controls">
                                                        <input type="text" name="patient_phone" class="form-control input-sm"  id="patient_phone"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Department  (required)</label>

                                                    <div class="controls">
                                                        <select id="selectError" name="procedure_name" required="" data-rel="">
                                                            <option value="">Please select  Procedure : </option>

                                                            <?php foreach ($procedure_name as $proceudre_type) { ?>
                                                                <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                                            <?php } ?>

                                                        </select>
                                                    </div>



                                                </div>


                                                <hr>



                                                <hr>

                                                <input type="submit" class=" btn btn-info book_patient_procedure_button" id="book_patient_procedure_button" value="Book Patient"/>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>





                        </div>




                        <div id = "shwari_regular_client" style = "display: none;">

                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Book Procedure</h2>


                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">
                                                <form class = "shwari_patient_book_procedure_form" method = "post" autocomplete="off"  id = "shwari_patient_book_procedure_form" role = "form">
                                                    <div class="form-inline">

                                                        <div class = "form-inline">
                                                            <div class="control-group">
                                                                <label class="control-label" for="selectError">Patient Name (required) </label>

                                                                <div class="controls">
                                                                    <select id="selectError" name="patientname" required="" data-rel="">
                                                                        <option value="">Please select Patient Name : </option>
                                                                        <?php foreach ($name as $name_types) { ?>
                                                                            <option  value="<?php echo $name_types['title'] . ' :' . $name_types['f_name'] . ' ' . $name_types['s_name'] . ' ' . $name_types['other_name'] ?>" id="<?php echo $name_types['f_name'] ?>" ><?php echo $name_types['title'] ?> <?php echo $name_types['f_name'] ?>   <?php echo $name_types['s_name'] ?>   <?php echo $name_types['other_name'] ?>(<?php echo $name_types['family_number'] ?>)</option>
                                                                        <?php } ?>

                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <hr>

                                                        </div>


                                                        <div class="form-inline">
                                                            <div class="controls">
                                                                <select id="selectError" name="procedure_name" required="" data-rel="">
                                                                    <option value="">Please select  Procedure : </option>

                                                                    <?php foreach ($procedure_name as $proceudre_type) { ?>
                                                                        <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                                                    <?php } ?>

                                                                </select>
                                                            </div>
                                                        </div>





                                                        <div class = "form-inline">
                                                            <div class = "form-group">
                                                                <input type = "submit" value = "Book for Procedure" class = "btn btn-success shwari_patient_book_procedure_button" id = "shwari_patient_book_procedure_button"/>
                                                                <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                            </div>
                                                        </div>

                                                    </div>


                                                </form>
                                            </div>
                                        </div>






                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>



                        <div id = "not_regular_patient" style = "display: none;">


                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                        <div class = "box-icon">

                                        </div>
                                    </div>

                                    <div class = "box-content">
                                        <label class = "">
                                            Would you like to Join our Clinic network ?
                                        </label><br>

                                        <a id = "shwari_relation_no" href = "#shwari_relation_no_form" class = "btn btn-info btn-sm shwari_relation_no">
                                            Yes
                                        </a>

                                        <a id = "not_joining" href = "#not_interested_in_joining" class = "btn btn-info btn-sm not_joining">
                                            No
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->


                        </div>






                        <div id = "patient_option" style = "display: none;">


                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                        <div class = "box-icon">

                                        </div>
                                    </div>

                                    <div class = "box-content">
                                        <label class = "">
                                            Are you related to any Shwari Member ?
                                        </label><br>

                                        <a id = "shwari_relation_yes" class = "btn btn-info btn-sm shwari_relation_yes" href = "#shwari_relation_yes_form">
                                            Yes
                                        </a>

                                        <a id = "shwari_relation_no" href = "#shwari_relation_no_form" class = "btn btn-info btn-sm shwari_relation_no">
                                            No
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->


                        </div>




                        <div class = "form-control" id = "shwari_relation_yes_form" style = "display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Registration</h2>


                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">
                                                <form class = "register_new_patient_form " autocomplete="off" method = "post"  id = "register_new_patient_form" role = "form">
                                                    <div class="form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputShwariFamilyNumber">Shwari Family Number :</label>
                                                            <input type = "text" class = "form-control" id ="inputShwariFamilyNumber" placeholder = "Shwari Family Number">
                                                        </div>

                                                        <div class = "form-group">
                                                            <div><label>Family Number Result : </label></div>
                                                            <div id="shwari_family_number_result" name="shwari_family_number_result" class="shwari_family_number_result">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class = "form-group">
                                                        <label class = "sr-only" for = "family_number"> Family Number :</label>
                                                        <input type = "text" class = "form-control" name="family_number" id ="family_number" placeholder = "Please paste the  family number : ">
                                                    </div>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "family_number"> Family Number :</label>
                                                            <select name="title" class="selector" id="title" data-bind="">
                                                                <?php foreach ($title as $value) {
                                                                    ?>
                                                                    <option value="<?php echo $value['title_name']; ?>"><?php echo $value['title_name']; ?></option>
                                                                <?php }
                                                                ?>
                                                            </select>   
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputFirstName">First Name : </label>
                                                            <input type = "text" class = "form-control" id = "inputFirstName" name="fname" placeholder = "First Name">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputSurName">Sur Name : </label>
                                                            <input type = "text" class = "form-control" id = "inputSurName" name="sname" placeholder = "Sur Name">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputOtherName">Other Name : </label>
                                                            <input type = "text" class = "form-control" id = "inputOtherName" name="lname" placeholder = "Other Name">
                                                        </div>

                                                    </div>

                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputDateofBirth">Date of Birth</label>
                                                            <input type = "text" class = "form-control" name="dob" id = "datepicker" placeholder = "D.O.B">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputSurName">National/Military/Alien ID or Passport No</label>
                                                            <input type = "text" class = "form-control" name="nationalid" id = "inputSurName" placeholder = "National ID ">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputPostalAddress">Postal Address</label>
                                                            <input type = "text" class = "form-control" name="address" id = "inputPostalAddress" placeholder = "Postal Address">
                                                        </div>
                                                    </div>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputResidence">Residence</label>
                                                            <input type = "text" class = "form-control" name="residence" id = "inputResidence" placeholder = "Residence">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputCity">City</label>
                                                            <input type = "text" class = "form-control" name="city" id = "inputCity" placeholder = "City">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputPhoneNo">Phone No</label>
                                                            <input type = "text" class = "form-control" name="phone_no" id = "inputPhoneNo" placeholder = "Phone No">
                                                        </div>
                                                    </div>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputEmail">Email : </label>
                                                            <input type = "email" class = "form-control" name="email" id ="inputEmail"  placeholder = "Email">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputEmploymentStatus">Employment Status : </label>
                                                            <span class="radio"> <input type = "radio" class = "form-control radio checkbox employmentstatus_yes" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Employed">
                                                                Employed</span>
                                                            <span class="radio">  <input type = "radio" class = "form-control radio checkbox employmentstatus_no" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Not Employed">
                                                                Not Employed</span>

                                                        </div>

                                                        <div class = "form-group employers_name_div" id="employers_name_div" >
                                                            <label class = "sr-only" for = "inputEmployersName">Employer's Name : </label>
                                                            <input type = "text" style="display:none ;" class = "form-control employers_name" name="employers_name" id ="employers_name"  placeholder = "Employmer's Name" >

                                                        </div>


                                                    </div>


                                                    <div class = "form-inline">
                                                        <div class = "control-group">
                                                            <label class = "control-label" for = "inputGender">Gender</label>
                                                            <div class = "controls">
                                                                <select class = "selectError" name="sex" data-rel = "" >
                                                                    <option value = "Male">Male</option>
                                                                    <option value = "Female">Female</option>
                                                                    <option value = "Trans-gender">Trans-gender</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class = "control-group">
                                                            <label class = "control-label" for = "inputMaritalStatus">Marital Status</label>
                                                            <div class = "controls">
                                                                <select class = "selectError" name="maritalstatus" data-rel = "">
                                                                    <option value = "Single">Single</option>
                                                                    <option value = "Married">Married</option>
                                                                    <option value = "Divorced">Divorced</option>
                                                                    <option value = "Widowed">Widowed</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                    </div>



                                                    <label>Next of Kin Details </label>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputFirstName">First Name</label>
                                                            <input type = "text" class = "form-control" id = "inputFirstName" name="kinname" placeholder = "First Name">
                                                        </div><div class = "form-group">
                                                            <label class = "sr-only" for = "inputLastName">Last Name</label>
                                                            <input type = "text" class = "form-control" id = "inputLastName" name="kinsname" placeholder = "Last Name">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputMiddleName">Middle Name</label>
                                                            <input type = "text" class = "form-control" id = "inputMiddleName" placeholder = "Middle Name">
                                                        </div>
                                                    </div>


                                                    <div class = "form-inline">

                                                        <div class = "control-group">
                                                            <label class = "control-label" for = "inputkinrelation"> Relation Type : </label>
                                                            <div class = "controls">
                                                                <select class = "selectError" name="kinrelation" data-rel = "">
                                                                    <option value = "Sibling">Sibling</option>
                                                                    <option value = "Father">Father</option>
                                                                    <option value = "Mother">Mother</option>
                                                                    <option value = "Husband">Husband</option>
                                                                    <option value = "Wife">Wife</option>
                                                                    <option value = "Aunt">Aunt</option>
                                                                    <option value="Uncle">Uncle</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputPhone">Kin's Phone No</label>
                                                            <input type="text" placeholder="Kin's Phone"  class = "form-control"  id="kinphone" name="kinphone" size="30" />
                                                        </div>

                                                    </div>
                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <input type = "submit" value = "Register Patient" class = "btn btn-success add_register_new_patient" id = "add_register_new_patient"/>
                                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                        </div>
                                                    </div>




                                                </form>



                                            </div>
                                        </div>






                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>









                        <div class = "form-control" id = "shwari_relation_no_form" style = "display: none;">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Registration Form</h2>

                                            <div class = "box-icon">
                                                <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-cog"></i></a>
                                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>
                                                <a href = "#" class = "btn btn-close btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-remove"></i></a>
                                            </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">


                                                <form class = "new_family_registration_form " autocomplete="off" method = "post"  id = "new_family_registration_form" role = "form">



                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "family_number"> Title :</label>
                                                            <select name="title"  required="" class="selector" id="title" >
                                                                <option value="">Please select title </option>
                                                                <?php foreach ($title as $value) {
                                                                    ?>
                                                                    <option value="<?php echo $value['title_name']; ?>"><?php echo $value['title_name']; ?></option>
                                                                <?php }
                                                                ?>
                                                            </select>   
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputFirstName">First Name : </label>
                                                            <input type = "text" class = "form-control" id = "inputFirstName" name="fname" placeholder = "First Name">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputSurName">Sur Name : </label>
                                                            <input type = "text" class = "form-control" id = "inputSurName" name="sname" placeholder = "Sur Name">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputOtherName">Other Name : </label>
                                                            <input type = "text" class = "form-control" id = "inputOtherName" name="lname" placeholder = "Other Name">
                                                        </div>

                                                    </div>

                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputDateofBirth">Date of Birth</label>
                                                            <input type = "text" class = "form-control" name="dob" id = "datepicker_1" placeholder = "D.O.B">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputSurName">National/Military/Alien ID or Passport No</label>
                                                            <input type = "text" class = "form-control" name="nationalid" id = "inputSurName" placeholder = "National ID ">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputPostalAddress">Postal Address</label>
                                                            <input type = "text" class = "form-control" name="address" id = "inputPostalAddress" placeholder = "Postal Address">
                                                        </div>
                                                    </div>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputResidence">Residence</label>
                                                            <input type = "text" class = "form-control" name="residence" id = "inputResidence" placeholder = "Residence">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputCity">City</label>
                                                            <input type = "text" class = "form-control" name="city" id = "inputCity" placeholder = "City">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputPhoneNo">Phone No</label>
                                                            <input type = "text" class = "form-control" name="phone_no" id = "inputPhoneNo" placeholder = "Phone No">
                                                        </div>
                                                    </div>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputEmail">Email : </label>
                                                            <input type = "email" class = "form-control" name="email" id ="inputEmail"  placeholder = "Email">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputEmploymentStatus">Employment Status : </label>
                                                            <span class="radio"> <input type = "radio" class = "form-control radio checkbox employmentstatus_yes_1" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Employed">
                                                                Employed</span>
                                                            <span class="radio">  <input type = "radio" class = "form-control radio checkbox employmentstatus_no_1" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Not Employed">
                                                                Not Employed</span>

                                                        </div>

                                                        <div class = "form-group employers_name_div" id="employers_name_div_1" >
                                                            <label class = "sr-only" for = "inputEmployersName">Employer's Name : </label>
                                                            <input type = "text" style="display:none ;" class = "form-control employers_name_1" name="employers_name" id ="employers_name_1"  placeholder = "Employmer's Name" >

                                                        </div>


                                                    </div>


                                                    <div class = "form-inline">
                                                        <div class = "control-group">
                                                            <label class = "control-label" for = "inputGender">Gender</label>
                                                            <div class = "controls">
                                                                <select class = "selectError" name="sex" data-rel = "" >
                                                                    <option value = "Male">Male</option>
                                                                    <option value = "Female">Female</option>
                                                                    <option value = "Trans-gender">Trans-gender</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class = "control-group">
                                                            <label class = "control-label" for = "inputMaritalStatus">Marital Status</label>
                                                            <div class = "controls">
                                                                <select class = "selectError" name="maritalstatus" data-rel = "">
                                                                    <option value = "Single">Single</option>
                                                                    <option value = "Married">Married</option>
                                                                    <option value = "Divorced">Divorced</option>
                                                                    <option value = "Widowed">Widowed</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                    </div>



                                                    <label>Next of Kin Details </label>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputFirstName">First Name</label>
                                                            <input type = "text" class = "form-control" id = "inputFirstName" name="kinname" placeholder = "First Name">
                                                        </div><div class = "form-group">
                                                            <label class = "sr-only" for = "inputLastName">Last Name</label>
                                                            <input type = "text" class = "form-control" id = "inputLastName" name="kinsname" placeholder = "Last Name">
                                                        </div>
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputMiddleName">Middle Name</label>
                                                            <input type = "text" class = "form-control" id = "inputMiddleName" placeholder = "Middle Name">
                                                        </div>
                                                    </div>


                                                    <div class = "form-inline">

                                                        <div class = "control-group">
                                                            <label class = "control-label" for = "inputkinrelation"> Relation Type : </label>
                                                            <div class = "controls">
                                                                <select class = "selectError" name="kinrelation" data-rel = "">
                                                                    <option value = "Sibling">Sibling</option>
                                                                    <option value = "Father">Father</option>
                                                                    <option value = "Mother">Mother</option>
                                                                    <option value = "Husband">Husband</option>
                                                                    <option value = "Wife">Wife</option>
                                                                    <option value = "Aunt">Aunt</option>
                                                                    <option value="Uncle">Uncle</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputPhone">Kin's Phone No</label>
                                                            <input type="text" placeholder="Kin's Phone"  class = "form-control"  id="kinphone" name="kinphone" size="30" />
                                                        </div>

                                                    </div>
                                                    <hr>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <input type = "submit" value = "Register Patient" class = "btn btn-success new_family_registration_button" id = "new_family_registration_button"/>
                                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                        </div>
                                                    </div>




                                                </form>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div><!--/row-->

                        </div>






                        <div id="walkin_report_filter_form" style="display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Care-tech Walk-in Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/walkin_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="walkin_date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="walkin_date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Department Name : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="department_name" class="department_name" required="" data-rel="">
                                                            <option value="">Please select  Department : </option>
                                                            <option value="All">All</option>
                                                            <option value="Nursing">Nursing</option>
                                                            <option value="Laboratory">Laboratory</option>
                                                            <option value="Pharmacy">Pharmacy</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Payment Status  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="payment_status" id="package_type" required="" data-rel="">
                                                            <option value="">Please select  Payment Status : </option>
                                                            <option value="All">All</option>
                                                            <option value="Paid">Paid</option>
                                                            <option value="Not Paid">Not Paid</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                        </div>


                        <div id="regular_procedure_report_filter_form" style="display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Procedure Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/procedure_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="visitation_date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="visitation_date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Patient Name : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="patient_name" class="patient_name" required="" data-rel="">
                                                            <option value="">Please select  Status : </option>
                                                            <option value="All" >All</option>
                                                            <?php foreach ($name as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['patient_id']; ?>"><?php echo $value['title'] . ':' . $value['f_name'] . $value['s_name'] . $value['other_name']; ?></option><?php }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Procedure  Type  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="procedure_type" id="procedure_type" required="" data-rel="">
                                                            <option value="">Please select  Package Type : </option>
                                                            <option value="All" >All</option>
                                                            <?php foreach ($procedure_name as $proceudre_type) { ?>
                                                                <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                        </div>



                        <div id="walkin_procedure_report_filter_form" style="display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Procedure Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/procedure_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="visitation_date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="visitation_date_to"/>
                                                    </div>
                                                </div>
                                                <hr>



                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Procedure Type  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="procedure_type" class="procedure_type" required="" data-rel="">
                                                            <option value="">Please select  Procedure Type : </option>
                                                            <option value="All" >All</option>
                                                            <?php foreach ($procedure_name as $proceudre_type) { ?>
                                                                <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                        </div>




                        <div id="visit_report_filter_form" style="display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Visitation Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/visitation_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="visitation_date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="visitation_date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Patient Name : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="patient_name" class="patient_name" required="" data-rel="">
                                                            <option value="">Please select  Status : </option>
                                                            <option value="All" >All</option>
                                                            <?php foreach ($name as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['patient_id']; ?>"><?php echo $value['title'] . ':' . $value['f_name'] . $value['s_name'] . $value['other_name']; ?></option><?php }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Package Type  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="package_type" id="package_type" required="" data-rel="">
                                                            <option value="">Please select  Package Type : </option>
                                                            <option value="All" >All</option>
                                                            <?php foreach ($pack as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['package_id']; ?>"><?php echo $value['package_name']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                        </div>



                        <div id = "patient_report_filter_form" style = "display: none;">

                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Patient Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/patient_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Patient Status : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="patient_status" class="patient_status" required="" data-rel="">
                                                            <option value="">Please select  Status : </option>
                                                            <option value="All">All</option>
                                                            <option value="Active">Active</option>
                                                            <option value="In Active">In Active</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Gender  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="gender" id="gender" required="" data-rel="">
                                                            <option value="">Please select  Gender : </option>
                                                            <option value="All">All</option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                            <option value="transgender">Trans-Gender</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>


                        </div>






                        <div class = "form-control" id = "walkin_patient_nurse_form" style = "display: none;">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Walkin Nurse Patient Payments </h2>

                                            <div class = "box-icon">
                                                <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-cog"></i></a>
                                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                                            </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">


                                                <form class = "walkin_patient_nurse_payment_form " autocomplete="off" method = "post"  id = "walkin_patient_nurse_payment_form" role = "form">



                                                    <div class = "form-inline">

                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputFirstName">Patient Name : </label>
                                                            <input type = "text" readonly="" class = "form-control walkin_nurse_patient_name" id = "walkin_nurse_patient_name" name="walkin_nurse_patient_name" placeholder = "Patient Name">
                                                        </div>


                                                    </div>

                                                    <div class = "form-inline walkin_patient_nurse_payment_div" id="walkin_patient_nurse_payment_div">




                                                    </div>






                                                    <hr>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <input type = "submit" value = "Make Payment" class = "btn btn-success walkin_payment_button" id = "walkin_payment_button"/>
                                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                        </div>
                                                    </div>




                                                </form>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div><!--/row-->

                        </div>








                        <div class = "form-control" id = "walkin_patient_lab_form" style = "display: none;">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Walkin Nurse Patient Payments </h2>

                                            <div class = "box-icon">
                                                <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-cog"></i></a>
                                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                                            </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">


                                                <form class = "walkin_patient_lab_payment_form " autocomplete="off" method = "post"  id = "walkin_patient_lab_payment_form" role = "form">



                                                    <div class = "form-inline">

                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "inputFirstName">Patient Name : </label>
                                                            <input type = "text" readonly="" class = "form-control walkin_lab_patient_name" id = "walkin_lab_patient_name" name="walkin_lab_patient_name" placeholder = "Patient Name">
                                                        </div>


                                                    </div>

                                                    <div class = "form-inline walkin_patient_lab_payment_div" id="walkin_patient_lab_payment_div">




                                                    </div>






                                                    <hr>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <input type = "submit" value = "Make Payment" class = "btn btn-success walkin_payment_button" id = "walkin_payment_button"/>
                                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                        </div>
                                                    </div>




                                                </form>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div><!--/row-->

                        </div>






                        <div class = "form-control" id = "walkin_patient_pharmacy_form" style = "display: none;">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Walkin Nurse Patient Payments </h2>

                                            <div class = "box-icon">
                                                <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-cog"></i></a>
                                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                                            </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">


                                                <form class = "walkin_patient_pharmacy_payment_form " autocomplete="off" method = "post"  id = "walkin_patient_pharmacy_payment_form" role = "form">



                                                    <div class = "form-inline">

                                                        <div class = "form-group">
                                                            <pharmacyel class = "sr-only" for = "inputFirstName">Patient Name : </pharmacyel>
                                                            <input type = "text" readonly="" class = "form-control walkin_pharmacy_patient_name" id = "walkin_pharmacy_patient_name" name="walkin_pharmacy_patient_name" placeholder = "Patient Name">
                                                        </div>


                                                    </div>

                                                    <div class = "form-inline walkin_patient_pharmacy_payment_div" id="walkin_patient_pharmacy_payment_div">




                                                    </div>






                                                    <hr>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <input type = "submit" value = "Make Payment" class = "btn btn-success walkin_payment_button" id = "walkin_payment_button"/>
                                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                        </div>
                                                    </div>




                                                </form>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div><!--/row-->

                        </div>




                        <!-- 
                        Regular Patient payment form start
                        -->

                        <div class = "form-control" id = "regular_patient_form" style = "display: none;">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Regular Patient Payments </h2>

                                            <div class = "box-icon">
                                                <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-cog"></i></a>
                                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                                            </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">


                                                <form class = "regular_patient_payment_form " autocomplete="off" method = "post"  id = "regular_patient_payment_form" role = "form">



                                                    <div class = "form-inline">

                                                        <div class = "form-group">
                                                            <label class = "label label-info" for = "inputFirstName">Patient Name : </label>
                                                            <input type = "text" readonly="" required="" class = "form-control regular_patient_name" id = "regular_patient_name" name="regular_patient_name" placeholder = "Patient Name">
                                                        </div>


                                                    </div>

                                                    <div class = "form-inline regular_patient_payment_div" id="regular_patient_payment_div">




                                                    </div>






                                                    <hr>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <input type = "submit" value = "Make Payment" class = "btn btn-success regular_payment_button" id = "regular_payment_button"/>
                                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                        </div>
                                                    </div>




                                                </form>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div><!--/row-->

                        </div>

                        <!-- 
                        Regular Patient payment form end
                        -->





                        <!-- 
                        Lab Service  payment form start
                        -->

                        <div class = "form-control" id = "lab_service_payment_div" style = "display: none;">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Regular Patient Payments </h2>

                                            <div class = "box-icon">
                                                <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-cog"></i></a>
                                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                                            </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">


                                                <form class = "lab_service_payment_form " autocomplete="off" method = "post"  id = "lab_service_payment_form" role = "form">



                                                    <div class = "form-inline">

                                                        <div class = "form-group">
                                                            <label class = "label label-info" for = "inputFirstName">Patient Name : </label>
                                                            <input type = "text" readonly="" required="" class = "form-control lab_service_patient_name" id = "lab_service_patient_name" name="lab_service_patient_name" placeholder = "Patient Name">
                                                        </div>


                                                    </div>

                                                    <div class = "form-inline patient_lab_service_payment_div" id="patient_lab_service_payment_div">




                                                    </div>






                                                    <hr>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <input type = "submit" value = "Make Payment" class = "btn btn-success lab_service_payment_button" id = "lab_service_payment_button"/>
                                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                        </div>
                                                    </div>




                                                </form>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div><!--/row-->

                        </div>

                        <!-- 
                        Lab Service  payment form end
                        -->







                        <!--
                        Pay at the  end form start
                        -->


                        <div class = "form-control" id = "pay_at_the_end_patient_form" style = "display: none;">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Regular Patient Payments </h2>

                                            <div class = "box-icon">
                                                <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-cog"></i></a>
                                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                                            </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">


                                                <form class = "pay_at_the_end_patient_payment_form " autocomplete="off" method = "post"  id = "pay_at_the_end_patient_payment_form" role = "form">



                                                    <div class = "form-inline">

                                                        <div class = "form-group">
                                                            <label class = "label label-info" for = "inputFirstName">Patient Name : </label>
                                                            <input type = "text" readonly="" required="" class = "form-control pay_at_the_end_patient_name" id = "pay_at_the_end_patient_name" name="pay_at_the_end_patient_name" placeholder = "Patient Name">
                                                        </div>


                                                    </div>

                                                    <div class = "form-inline pay_at_the_end_patient_payment_div" id="pay_at_the_end_patient_payment_div">




                                                    </div>






                                                    <hr>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <input type = "submit" value = "Make Payment" class = "btn btn-success regular_payment_button" id = "regular_payment_button"/>
                                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                        </div>
                                                    </div>




                                                </form>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div><!--/row-->

                        </div>

                        <!--
                        Pay at the  end form end
                        -->









                        <div class = "form-control edit_triage_form" id = "edit_triage_form" style = "display: none;">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Edit Patient Triage Form</h2>

                                            <div class = "box-icon">
                                                <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-cog"></i></a>
                                                <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>

                                            </div>
                                        </div>
                                        <div class = "box-content">

                                            <div class = "bs-example">


                                                <form class = "edit_patient_triage_form " autocomplete="off" method = "post"  id = "edit_patient_triage_form" role = "form">



                                                    <div class = "form-inline">

                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "patient_name">Patient Name : </label>
                                                            <input type = "text" class = "form-control edit_triage_patient_name" id = "edit_triage_patient_name" name="edit_triage_patient_name"  readonly="" placeholder = "Patient Name">
                                                            <input type="hidden" class="form-control edit_triage_patient_id" id="edit_triage_patient_id" value="<?php echo $this->uri->segment(3); ?>" name="edit_triage_patient_id" />
                                                            <input type="hidden" class="form-control edit_triage_visit_id" id="edit_triage_visit_id" value="<?php echo $this->uri->segment(4); ?>" name="edit_triage_visit_id"/>
                                                            <input type="hidden" id="edit_triage_id" class="form-control edit_triage_id" name="edit_triage_id"/>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "weight">Weight(Kgs) : </label>
                                                            <input type = "number" min="1" class = "form-control edit_weight" id = "edit_weight" name="edit_weight" placeholder = "Weight(Kgs) :">
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "systolic_diastolic">Systolic/Diastolic : </label>
                                                            <input type = "number" class = "form-control edit_systolic" id = "edit_systolic" name="edit_systolic" placeholder = "Systolic">/
                                                            <input type = "number" class = "form-control edit_diastolic" id = "edit_diastolic" name="edit_diastolic" placeholder = "Diastolic">
                                                        </div>

                                                    </div>
                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "temperature">Temperature(°C)</label>
                                                            <input type = "number" class = "form-control edit_temperature" name="edit_temperature" id = "edit_temperature" placeholder = "Temperature(°C)">
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "height">Height (cm)</label>
                                                            <input type = "number" class = "form-control edit_height" name="edit_height" id = "edit_height" placeholder = "Height (cm) ">
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "respiratory">Respiratory (No/Min)</label>
                                                            <input type = "number" class = "form-control edit_respiratory" name="edit_respiratory" id = "edit_respiratory" placeholder = "Respiratory (No/Min)">
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "blood_sugar">Blood Sugar (Mmol/Ltr):</label>
                                                            <input type = "number" class = "form-control edit_blood_sugar" name="edit_blood_sugar" id = "edit_blood_sugar" placeholder = "Blood Sugar (Mmol/Ltr)">
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "pulse_rate">Pulse Rate (Bpm):</label>
                                                            <input type = "number" class = "form-control edit_pulse_rate" name="edit_pulse_rate" id = "edit_pulse_rate" placeholder = "Pulse Rate (Bpm)">
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <?php
                                                    foreach ($patient_bio as $patient_bio_data) {
                                                        $gender = $patient_bio_data['gender'];
                                                        if ($gender === "Female") {
                                                            ?>
                                                            <div class = "form-inline">
                                                                <div class = "form-group">
                                                                    <label class = "sr-only" for = "LMP">LMP Date(YYYY-MM-DD):</label>
                                                                    <input type = "text" class = "form-control edit_LMP" name="edit_LMP" id = "edit_LMP" placeholder = "LMP Date(YYYY-MM-DD)">
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>

                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "general_complaints">General Complaints/Obvious critical symptom's</label>
                                                            <textarea  class = "form-control edit_general_complaints" name="edit_general_complaints" id = "edit_general_complaints" placeholder = "General Complaints/Obvious critical symptom's"></textarea>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label class = "sr-only" for = "allergy">Allergy : </label>
                                                            <textarea class = "form-control edit_allergy" name="edit_allergy" id ="edit_allergy"  placeholder = "Allergy"></textarea>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <label>
                                                                <input type="checkbox" name="edit_urgency" value="urgent" id="edit_urgency">
                                                                Urgent</label>
                                                        </div>
                                                    </div>
                                                    <hr>










                                                    <hr>


                                                    <div class = "form-inline">
                                                        <div class = "form-group">
                                                            <input type = "submit" value = "Update Patient's Triage" class = "btn btn-success edit_traige_button" id = "edit_traige_button"/>
                                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                        </div>
                                                    </div>




                                                </form>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div><!--/row-->

                        </div>





                        <div class = "form-control add_triage_form" id = "add_triage_form" style = "display: none;">



                            <!--
                                                        <div class = "row">
                                                            <div class = "box col-md-12">
                                                                <div class = "box-inner">
                                                                    <div class = "box-header well" data-original-title = "">
                                                                        <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Add Patient Triage Form</h2>
                            
                                                                        <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                                                    class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                                                    </div>
                                                                    <div class = "box-content">
                            
                                                                        <div class = "bs-example">
                            
                            
                                                                            <form class = "add_patient_triage_form " autocomplete="off" method = "post"  id = "add_patient_triage_form" role = "form">
                            
                            
                            
                                                                                <div class = "form-inline">
                            
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "patient_name">Patient Name : </label>
                                                                                        <input type = "text" class = "form-control add_triage_patient_name" id = "add_triage_patient_name" name="add_triage_patient_name"  readonly="" placeholder = "Patient Name">
                                                                                        <input type="hidden" class="form-control add_triage_patient_id" id="add_triage_patient_id" value="<?php echo $this->uri->segment(3); ?>" name="add_triage_patient_id" />
                                                                                        <input type="hidden" class="form-control add_triage_visit_id" id="add_triage_visit_id" value="<?php echo $this->uri->segment(4); ?>" name="add_triage_visit_id"/>
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "weight">Weight(Kgs) : </label>
                                                                                        <input type = "number" min="1" class = "form-control" id = "weight" name="weight" placeholder = "Weight(Kgs) :">
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "systolic_diastolic">Systolic/Diastolic : </label>
                                                                                        <input type = "text" class = "form-control" id = "systolic" name="systolic" placeholder = "Systolic">/
                                                                                        <input type = "text" class = "form-control" id = "diastolic" name="diastolic" placeholder = "Diastolic">
                                                                                    </div>
                            
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "temperature">Temperature(°C)</label>
                                                                                        <input type = "text" class = "form-control temperature" name="temperature" id = "temperature" placeholder = "Temperature(°C)">
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "height">Height (cm)</label>
                                                                                        <input type = "text" class = "form-control" name="height" id = "height" placeholder = "Height (cm) ">
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "respiratory">Respiratory (No/Min)</label>
                                                                                        <input type = "text" class = "form-control" name="respiratory" id = "respiratory" placeholder = "Respiratory (No/Min)">
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "blood_sugar">Blood Sugar (Mmol/Ltr):</label>
                                                                                        <input type = "text" class = "form-control" name="blood_sugar" id = "blood_sugar" placeholder = "Blood Sugar (Mmol/Ltr)">
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "pulse_rate">Pulse Rate (Bpm):</label>
                                                                                        <input type = "text" class = "form-control" name="pulse_rate" id = "pulse_rate" placeholder = "Pulse Rate (Bpm)">
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "LMP">LMP Date(YYYY-MM-DD):</label>
                                                                                        <input type = "text" class = "form-control" name="LMP" id = "LMP" placeholder = "LMP Date(YYYY-MM-DD)">
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "general_complaints">General Complaints/Obvious critical symptom's</label>
                                                                                        <textarea  class = "form-control" name="general_complaints" id = "general_complaints" placeholder = "General Complaints/Obvious critical symptom's"></textarea>
                                                                                    </div>
                                                                                </div>
                            
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "allergy">Allergy : </label>
                                                                                        <textarea  class = "form-control" name="allergy" id ="allergy"  placeholder = "Allergy"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "medication">Medication : </label>
                                                                                        <textarea  class = "form-control" name="medication" id ="medication"  placeholder = "Medication"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label class = "sr-only" for = "visit_reason">Reason for Visit : </label>
                                                                                        <textarea  class = "form-control visit_reason" name="visit_reason" id ="visit_reason"  placeholder = "Reason for Visit"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <label>
                                                                                            <input type="checkbox" name="urgency" value="urgent" id="Emergency_0">
                                                                                            Urgent</label>
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class = "form-inline">
                                                                                    <div class = "form-group">
                                                                                        <input type = "submit" value = "Update Patient's Triage" class = "btn btn-success add_traige_button" id = "add_traige_button"/>
                                                                                        <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                                                    </div>
                                                                                </div>
                            
                            
                            
                            
                                                                            </form>
                            
                            
                            
                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            /span
                            
                                                        </div>/row-->

                        </div>
















                        <div class="box col-md-8 book_appointment_form" id="book_appointment_form" style="display: none;" >
                            <div class="box-inner">
                                <div data-original-title="" class="box-header well">
                                    <h2><i class="glyphicon glyphicon-bell"></i> Book Appointment Form</h2>
                                    <div class="box-icon">

                                        <a class="btn btn-minimize btn-round btn-default" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>

                                    </div>
                                </div>
                                <div class="box-content">
                                    <div class="alert alert-info">

                                    </div>
                                    <p class="center">
                                        <label>Appointment Date : </label>
                                        <input type="text" name="book_appointment_patient_name" class="input input-sm" id="book_appointment_patient_name"/>
                                    </p>
                                    <p class="center">
                                        <label>Appointment Type : </label>
                                        <select name="appointment_type" id="appointment_type">
                                            <option>Please select : </option>
                                            <option></option>
                                        </select>
                                    </p>
                                    <p class="center">
                                        <label>Clinician Name : </label>
                                        <input type="text" name="clinician" id="clinician" class="clinician"/>
                                    </p>
                                    <p class="center">
                                        <label>Between : </label>
                                        <input type="text" />
                                    </p>
                                </div>
                            </div>
                        </div>




                        <div style="display: none;" class="imaging_referral" id="imaging_referral">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Imaging Request  Form </h2>

                                            <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                        </div>
                                        <div class = "box-content">



                                            <form class="imaging_request_form" id="imaging_request_form" method="post" action="<?php echo base_url(); ?>doctor/imaging_request_form">

                                                <input type="hidden" name="patient_id_imaging" id="patient_id_imaging" class="patient_id_imaging" value="<?php echo $this->uri->segment(3); ?>"/>
                                                <input type="hidden" name="visit_id_imaging" id="visit_id_imaging" class="visit_id_imaging" value="<?php echo $this->uri->segment(4); ?>"/>


                                                <label class="label label-info">Imaging Test : </label>
                                                <textarea name="imaging_test" class="autogrow" id="imaging_test"></textarea>
                                                <hr>
                                                <label class="label label-info">Major Complaints : </label>
                                                <textarea name="major_complaint" id="major_complaint" class="autogrow"></textarea>
                                                <hr>
                                                <label class="label label-info">Diagnosis :</label>
                                                <textarea name="diagnosis" id="diagnosis" class="autogrow"></textarea>
                                                <hr>
                                                <label class="label label-info">Special Instructions : </label>
                                                <textarea name="special_instrations" class="autogrow" id="special_instructions"></textarea>
                                                <hr>
                                                <label class="label label-info" >Referring Doctor's Comments :</label>
                                                <textarea name="referring_doctor_comments" class="autogrow" id=""></textarea>

                                                <hr>
                                                <input type="submit" name="save_request_for_imaging" class="save_request_for_imaging btn btn-xs btn-success" id="save_request_for_imaging" value="Save"/>
                                                <input type="reset" class="btn btn-xs btn-close" value="Cancel"/>


                                            </form>




                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>


















                        <div style="display: none;" class="other_referral" id="other_referral">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i>  Referral  Form </h2>

                                            <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                        </div>
                                        <div class = "box-content">



                                            <form class="referral_request_form" id="referral_request_form" method="post" action="<?php echo base_url(); ?>doctor/patient_referral_form">

                                                <input type="hidden" name="patient_id_other" id="patient_id_imaging" class="patient_id_imaging" value="<?php echo $this->uri->segment(3); ?>"/>
                                                <input type="hidden" name="visit_id_other" id="visit_id_imaging" class="visit_id_imaging" value="<?php echo $this->uri->segment(4); ?>"/>


                                                <label class="label label-info">Referral To : </label>
                                                <textarea name="referral_to" class="autogrow" id="referral_to"></textarea>
                                                <hr>
                                                <label class="label label-info">Major Complaints : </label>
                                                <textarea name="major_complaint" id="major_complaint" class="autogrow"></textarea>
                                                <hr>
                                                <label class="label label-info">Diagnosis :</label>
                                                <textarea name="diagnosis" id="diagnosis" class="autogrow"></textarea>
                                                <hr>
                                                <label class="label label-info">Special Instructions : </label>
                                                <textarea name="special_instrations" class="autogrow" id="special_instructions"></textarea>
                                                <hr>
                                                <label class="label label-info" >Referring Doctor's Comments :</label>
                                                <textarea name="referring_doctor_comments" class="autogrow" id=""></textarea>

                                                <hr>
                                                <input type="submit" name="save_request_for_referral" class="save_request_for_referral btn btn-xs btn-success" id="save_request_for_referral" value="Save"/>
                                                <input type="reset" class="btn btn-xs btn-close" value="Cancel"/>


                                            </form>




                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>






                        <div style="display: none;" class="sick_off" id="sick_off">




                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i>  Referral  Form </h2>

                                            <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                        class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                        </div>
                                        <div class = "box-content">



                                            <form class="sick_off_form" id="sick_off_form" method="post" action="<?php echo base_url(); ?>doctor/sick_off_form">

                                                <input type="hidden" name="patient_id_sick_off" id="patient_id_imaging" class="patient_id_imaging" value="<?php echo $this->uri->segment(3); ?>"/>
                                                <input type="hidden" name="visit_id_sick_off" id="visit_id_imaging" class="visit_id_imaging" value="<?php echo $this->uri->segment(4); ?>"/>


                                                <label class="label label-info">Date from  : </label>
                                                <input type="text" name="sick_off_date_from" id="sick_off_date_from" class="sick_off_date_from" placeholder="YYYY-MM-DD"/>
                                                <hr>
                                                <label class="label label-info">Date To  : </label>
                                                <input type="text" name="sick_off_date_to" id="sick_off_date_to" class="sick_off_date_to" placeholder="YYYY-MM-DD"/>
                                                <hr>

                                                <input type="submit" name="save_sick_off" class="save_sick_off btn btn-xs btn-success" id="save_sick_off" value="Save Sick Off"/>
                                                <input type="reset" class="btn btn-xs btn-close" value="Cancel"/>


                                            </form>




                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>

























                        <?php
                        $segment_1 = $this->uri->segment(1);
                        $doctor = "doctor";
                        $nurse = "nurse";
                        $lab = "lab";
                        $pharmacy = "pharmacy";
                        if ($doctor === $segment_1 or $nurse === $segment_1 or $lab === $segment_1 or $pharmacy === $segment_1) {
                            ?>

                            <div style="display: none;" class="test_refferal" id="test_refferal" >


                                <div class = "box-content">



                                    <form class="order_tests_form" id="order_tests_form" method="post" >



                                        <label class="control-label" for="selectError1">Tests</label>
                                        <div class="alert alert-info">
                                            (hold "Ctrl" key to select multiple):
                                        </div>


                                        <div class="controls">
                                            <select name="tests[]" class="select test_order" id="test_order" required="" multiple="multiple" size=10 style='height: 50%;'>

                                                <?php foreach ($tests as $test_data) {
                                                    ?>
                                                    <option value="<?php echo $test_data['test_id']; ?>">
                                                        <?php
                                                        echo $test_data['test_name'];
                                                        ?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>

                                            </select>
                                        </div>
                                        <input type="hidden" name="patient_id_lab" id="patient_id_lab" class="patient_id_lab" value="<?php echo $this->uri->segment(3); ?>"/>
                                        <input type="hidden" name="visit_id_lab" id="visit_id_lab" class="visit_id_lab" value="<?php echo $this->uri->segment(4); ?>"/>

                                        <hr>
                                        <input type="submit" value="Order Lab Tests" class="btn btn-xs btn-info order_tests" id="order_tests"/>






                                    </form>




                                </div>







                            </div>


                            <?php
                        }
                        ?>






                        <?php
                        $doctor_url = $this->uri->segment(2);

                        if ($doctor_url === "doctor_patient_profile") {
                            ?>
                            <div id="appointment_history" class="appointment_history" style="display: none;">

                                <table id="appointment_history_table" class="appointment_history_table">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>About</th>
                                            <th>Time Start</th>
                                            <th>Time End</th>
                                            <th>Queue Booked</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php foreach ($appointment_details as $value) {
                                                ?>
                                                <td><?php echo $value['title']; ?></td>
                                                <td><?php echo $value['about']; ?></td>
                                                <td><?php echo $value['time_start']; ?></td>
                                                <td><?php echo $value['time_end']; ?></td>
                                                <td><?php echo $value['queue_booked']; ?></td>

                                                <?php
                                            }
                                            ?>


                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                            <?php
                        }
                        ?>


                        <div class="bill_patient_prescription_form" id="bill_patient_prescription_form" style="display: none;  margin: 1em 2em ; margin-top: 1em;
                             margin-right: 2em;
                             margin-bottom: 1em;
                             margin-left: 2em ">

                            <div>
                                <div class="row">
                                    <div class="box col-md-11">
                                        <div class="box-inner">
                                            <div class="box-header well" data-original-title="">
                                                <h2><i class="glyphicon glyphicon-th"></i>  Patient Prescription Details </h2>

                                                <div class="box-icon">
                                                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                                                            class="glyphicon glyphicon-cog"></i></a>
                                                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                                            class="glyphicon glyphicon-chevron-up"></i></a>
                                                    <a href="#" class="btn btn-close btn-round btn-default"><i
                                                            class="glyphicon glyphicon-remove"></i></a>
                                                </div>
                                            </div>
                                            <div class="box-content">
                                                <div class="row">
                                                    <div class="form-inline">
                                                        <div class="form-group">
                                                            <input type="text" id="presc_commodity_name" class="presc_commodity_name form-control" name="presc_commodity_name" readonly=""/>
                                                            <input type="text" id="presc_strength" class="presc_strength form-control" name="presc_strenght" readonly=""/>
                                                            <input type="text" id="presc_route_name" class="presc_route_name form-control" name="presc_route_name" readonly=""/>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" id="presc_frequency" class="presc_frequency form-control" name="presc_frequency" readonly=""/>
                                                            <input type="text" id="presc_duration" class="presc_duration form-control" name="presc_duration" readonly=""/>
                                                            <input type="text" id="presc_prescription_tracker" class="presc_prescription_tracker form-control" name="presc_prescription_tracker" readonly=""/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div><!--/row-->


                                <div class="row">
                                    <div class="box col-md-11">
                                        <div class="box-inner">
                                            <div class="box-header well" data-original-title="">
                                                <h2><i class="glyphicon glyphicon-th"></i> Bill Patient  Form </h2>

                                                <div class="box-icon">
                                                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                                                            class="glyphicon glyphicon-cog"></i></a>
                                                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                                            class="glyphicon glyphicon-chevron-up"></i></a>
                                                    <a href="#" class="btn btn-close btn-round btn-default"><i
                                                            class="glyphicon glyphicon-remove"></i></a>
                                                </div>
                                            </div>
                                            <div class="box-content">
                                                <div class="row">
                                                    <form class="form-inline bill_patient_form" id="bill_patient_form" role="form">
                                                        <input type="text" name="bill_patient_patient_id" value="<?php echo $this->uri->segment(3); ?>" class="hidden"/>
                                                        <input type="text" name="bill_patient_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="hidden"/>
                                                        <input type="hidden" name="inputtransaction_id" id="inputtransaction_id" class="inputtransaction_id hidden"/>
                                                        <div class="form-inline" role="form">
                                                            <div class="form-group">
                                                                <label class="sr-only" for="bill_commodity_name">Commodity Name : </label>
                                                                <select id="bill_commodity_name" class="form-control bill_commodity_name" required="" name="bill_commodity_name" >
                                                                    <option>Please select commodity :</option>
                                                                    <?php
                                                                    foreach ($commodities as $value) {
                                                                        ?>

                                                                        <option value="<?php echo $value['commodity_id']; ?>"><?php echo $value['commodity_name']; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>  
                                                                </select>   
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="sr-only" for="inputstrength">Strength</label>
                                                                <input type="text" class="form-control inputstrength" id="inputstrength" placeholder="Strength">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="sr-only" for="inputQuantity_available">Quantity Available</label>
                                                                <input type="text" class="form-control" id="inputQuantityavailable" name="inputQuantityavailable" placeholder="Quantity Available ">
                                                            </div>
                                                        </div>
                                                        <hr>

                                                        <div class="form-inline" role="form">

                                                            <div class="form-group">
                                                                <label class="sr-only" for="inputquantity_billed">Quantity Billed </label>
                                                                <input type="text" class="form-control" id="inputquantity_billed" name="inputquantity_billed" placeholder="Quantity Billed">
                                                            </div>
                                                            <input type="hidden" class="  inputsellingprice" id="inputsellingprice" name="inputsellingprice" placeholder="Seling price per unit " />
                                                            <input type="text" class="input_total_cost" name="input_total_cost" id="input_total_cost" placeholder="Total Cost billed"/>

                                                            <div class="checkbox">
                                                                <label><input type="checkbox" id="service_charged" name="service_charged" value="200" class="service_charged checkbox"> Service charged ? </label>
                                                            </div>




                                                        </div>
                                                        <hr>
                                                        <button type="submit" class="btn btn-primary bill_patient_prescription_button">Bill Patient</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div><!--/row-->


                            </div>



                        </div>




                        <div id="content" class="col-lg-10 col-sm-10">
                            <!-- content starts -->
                            <?php
                        }
                    }
                    ?>