<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Register Patient</a>
        </li>
    </ul>
</div>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Register Patient </h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                  
                </div>
            </div>
            <div class="box-content">




                <!--- Shwari relation yes form  start -->

                <div class = "shwari_relation_yes_form" id = "shwari_relation_yes_form" style = "display: block;">






                    <div class = "box-content">

                        <div class = "bs-example">



                            <form class = "register_new_patient_form " autocomplete="off" method = "post"  id = "register_new_patient_form" role = "form">



                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "family_number"> Title Name :</label>
                                        <select name="title"   class="selector form-control " id="title" >
                                            <option value="">Please select title </option>
                                            <?php foreach ($title_name as $value) {
                                                ?>
                                                <option value="<?php echo $value['title_name']; ?>"><?php echo $value['title_name']; ?></option>
                                            <?php }
                                            ?>
                                        </select>   
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputFirstName">First Name : </label>
                                        <input type = "text" class = "form-control" required="" id = "inputFirstName" name="fname" placeholder = "First Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputSurName">Sur Name : </label>
                                        <input type = "text" class = "form-control" required="" id = "inputSurName" name="sname" placeholder = "Sur Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputOtherName">Other Name : </label>
                                        <input type = "text" class = "form-control"  id = "inputOtherName" name="oname" placeholder = "Other Name">
                                    </div>

                                </div>
                                <hr>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputDateofBirth">Date of Birth</label>
                                        <input type = "text" class = "form-control" required="" name="dob" id = "datepicker" placeholder = "D.O.B (YYYY-MM-DD)">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputSurName">National/Military/Alien ID or Passport No</label>
                                        <input type = "text" class = "form-control"  name="nationalid" id = "inputSurName" placeholder = "National ID ">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputPostalAddress">Postal Address</label>
                                        <input type = "text" class = "form-control "  name="address" id = "inputPostalAddress" placeholder = "Postal Address">
                                    </div>
                                </div>

                                <hr>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputResidence">Residence</label>
                                        <input type = "text" class = "form-control"  name="residence" id = "inputResidence" placeholder = "Residence">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputCity">City</label>
                                        <input type = "text" class = "form-control"  name="city" id = "inputCity" placeholder = "City">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputPhoneNo">Phone No</label>
                                        <input type = "text" class = "form-control"  name="phone_no" id = "inputPhoneNo" placeholder = "Phone No">
                                    </div>
                                </div>
                                <hr>

                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputEmail">Email : </label>
                                        <input type = "email" class = "form-control"  name="email" id ="inputEmail"  placeholder = "Email">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputEmploymentStatus">Employment Status : </label>
                                        <span class="radio"> <input type = "radio" class = "form-control radio checkbox employmentstatus_yes" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Employed">
                                            Employed</span>
                                        <span class="radio">  <input type = "radio" class = "form-control radio checkbox employmentstatus_no" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Not Employed">
                                            Not Employed</span>

                                    </div>

                                    <div class = "form-group employers_name_div" id="employers_name_div" >
                                        <label class = "sr-only" for = "inputEmployersName">Employer's Name : </label>
                                        <input type = "text" style="display:none ;" class = "form-control employers_name" name="employers_name" id ="employers_name"  placeholder = "Employmer's Name" >

                                    </div>


                                </div>

                                <hr>
                                <div class = "form-inline">
                                    <div class = "control-group form-group">
                                        <label class = "control-label" for = "inputGender">Gender</label>
                                        <div class = "controls">
                                            <select class = "selectError form-control" required="" name="gender" data-rel = "" >
                                                <option value = "Male">Male</option>
                                                <option value = "Female">Female</option>
                                                <option value = "Trans-gender">Trans-gender</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class = "control-group form-group">
                                        <label class = "control-label" for = "inputMaritalStatus">Marital Status</label>
                                        <div class = "controls">
                                            <select class = "selectError form-control"  name="marital_status" data-rel = "">
                                                <option value=" ">Please select</option>
                                                <option value = "Single">Single</option>
                                                <option value = "Married">Married</option>
                                                <option value = "Divorced">Divorced</option>
                                                <option value = "Widowed">Widowed</option>
                                            </select>
                                        </div>
                                    </div>


                                </div>

                                <hr>

                                <h6>Next of Kin Details </h6>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputFirstName">First Name</label>
                                        <input type = "text" class = "form-control"  id = "inputFirstName" name="kinfname" placeholder = "First Name">
                                    </div><div class = "form-group">
                                        <label class = "sr-only" for = "inputLastName">Sur Name</label>
                                        <input type = "text" class = "form-control"  id = "inputLastName" name="kinsname" placeholder = "Last Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputMiddleName">Other  Name</label>
                                        <input type = "text" class = "form-control" id = "inputMiddleName" name="kinonmame" placeholder = "Middle Name">
                                    </div>
                                </div>
                                <hr>

                                <div class = "form-inline">

                                    <div class = "control-group form-group">
                                        <label class = "control-label" for = "inputkinrelation"> Relation Type : </label>
                                        <div class = "controls">
                                            <select class = "selectError form-control"  name="kinrelation" data-rel = "">
                                                <option value = "Sibling">Sibling</option>
                                                <option value = "Father">Father</option>
                                                <option value = "Mother">Mother</option>
                                                <option value = "Husband">Husband</option>
                                                <option value = "Wife">Wife</option>
                                                <option value = "Aunt">Aunt</option>
                                                <option value="Uncle">Uncle</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputPhone">Kin's Phone No</label>
                                        <input type="text" placeholder="Kin's Phone"  class = "form-control"  id="kinphone" name="kinphone" size="30" />
                                    </div>

                                </div>
                                <hr>

                                <input type="hidden" id="copied_family_number" placeholder="copied shwari family number " class="copied_family_number form-control hidden " name="copied_family_number"/>
                                <!--- Shwari patient option form  start -->

                                <div id = "patient_option" style = "display: block;">

                                    <div class="form-inline">

                                        <div class="form-group">
                                            <div class = "box-content">
                                                <label class = "">
                                                    Would you like to join our clinic network ? 
                                                </label><br>
                                                <select name="join_network" id="join_network" class="join_network form-control">
                                                    <option value="">Please select : </option>
                                                    <option value="Yes">Yes</option>
                                                    <option value="No">No</option>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <input type="hidden" id="new_registration" class="new_registration form-control hidden" name="new_registration" value="No"/>
                                            <div class="shwari_relation_div" id="shwari_relation_div" style="display: none;">
                                                <div class = "box-content" >
                                                    <label class = "">
                                                        Are you related to any Shwari Member ?
                                                    </label><br>

                                                    <a id = "yes_related_shwari" href="#family_number_div" class = "btn btn-info btn-sm yes_related_shwari" >
                                                        Yes
                                                    </a>

                                                    <a id = "no_related_shwari"  class = "btn btn-info btn-sm no_related_shwari">
                                                        No
                                                    </a>
                                                </div>
                                            </div>

                                        </div>

                                    </div>




                                    <!--/span-->
                                </div>
                                <!--- Shwari patient option form  end -->



                                <div class="form_submit_div" id="form_submit_div" style="display: none;">
                                    <hr>
                                    <div class = "form-inline">
                                        <div class = "form-group">
                                            <input type = "submit" value = "Register Patient" class = "btn btn-success add_register_new_patient" id = "add_register_new_patient"/>
                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                        </div>
                                    </div>  
                                </div>





                            </form>



                        </div>
                    </div>






                </div>


                <!--- Shwari relation yes form  end -->









            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<!-- content ends -->
</div><!--/#content.col-md-0-->
</div><!--/fluid-row-->







<!--- Shwari  procedure form  start --> 

<div id = "family_number_div" class="family_number_div" style = "display: none;">


    <div class = "box col-md-12">
        <div class = "box-inner">
            <div class = "box-header well" data-original-title = "">
                <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                <div class = "box-icon">

                </div>
            </div>

            <div class = "box-content">
                <form id="family_relation_search_form" class="family_relation_search_form">

                    <div class="form-inline">

                        <div class="alert alert-info">
                            <button type="button" class="" data-dismiss="alert">&times;</button>
                            <strong>Note!</strong> This search is real time.
                        </div>


                        <div class = "form-group">
                            <label class = "sr-only" for = "inputf_name">First Name : </label>
                            <input type="text" placeholder="First Name" required="" class = "form-control check_first_name"  id="check_first_name" name="check_first_name" size="30" />
                        </div>

                        <div class = "form-group">
                            <label class = "sr-only" for = "inputPhone">Sur Name :</label>
                            <input type="text" placeholder="Kin's Phone" required="" class = "form-control check_sur_name"  id="check_sur_name" name="check_sur_name" size="30" />
                        </div>
                        <hr>
                        <div class = "form-group">
                            <label class = "sr-only" for = "inputPhone">Phone Number : </label>
                            <input type="text" placeholder="Phone Number : " required="" class = "form-control check_phone_no"  id="check_phone_no" name="check_phone_no" size="30" />
                        </div>
                        <div class = "form-group">
                            <label class = "sr-only" for = "input_id_no">Identification No : </label>
                            <input type="text" placeholder="Identification No : " required="" class = "form-control check_id_no"  id="check_id_no" name="check_id_no" size="30" />
                        </div>
                        <hr>
                        <div class = "form-group">
                            <label class = "sr-only" for = "input_family_no">Family  No : </label>
                            <input type="text" placeholder="Family No : " required="" class = "form-control check_family_number"  id="check_family_number" name="check_family_number" size="30" />
                        </div>
                        <hr>

                        <div class = "form-group">
                            <label class = "sr-only" for = "pasted_family_number">Family  No : </label>
                            <input type="text" placeholder="Please paste Family Number from the  Result Search : " required="" class = "form-control pasted_family_number"  id="pasted_family_number" name="check_family_number" size="30" />
                        </div>

                        <a id="save_family_number" class="save_family_number btn btn-sm btn-success"><i class="glyphicon glyphicon-save"></i>Save Patient Number </a>

                    </div>

                    <div class="form-inline">
                        <div class="box-content ajax_loader" id="ajax_loader" style="display: none;">
                            <ul class="ajax-loaders">

                                <img src="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" title="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" >
                                </br>
                                <br/>

                                <span class=" loader_notify clearfix" id="loader_notify">
                                </span>  

                            </ul>


                        </div>
                        <span><i class="glyphicon glyphicon-filter"></i> Familly Number Filter Results : </span><br>
                        <div class="form-group output_result" id="output_result" style="display: none;">

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <!--/span-->


</div>

<!--- Shwari  procedure form  end -->  











<div style="display: none;">



    <!--- Shwari  procedure form  start --> 

    <div id = "procedure_option" style = "display: none;">


        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                    <div class = "box-icon">

                    </div>
                </div>

                <div class = "box-content">
                    <label class = "">
                        Are you a regular patient to any of our Clinics ?
                    </label><br>

                    <a id = "regular_patient_yes" class = "btn btn-info btn-sm regular_patient_yes" href ="#shwari_regular_client">
                        Yes
                    </a>

                    <a id = "regular_patient_no" href = "#not_regular_patient" class = "btn btn-info btn-sm regular_patient_no">
                        No
                    </a>
                </div>
            </div>
        </div>
        <!--/span-->


    </div>

    <!--- Shwari  procedure form  end -->  


    <!--- Shwari  not joining form  start --> 

    <div id = "not_interested_in_joining" style = "display: none;">

        <style type = "text/css">
            .bs-example{
                margin: 20px;
            }
        </style>


        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class = "box-header well" data-original-title = "">
                        <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Book Procedure</h2>


                    </div>
                    <div class="box-content">
                        <form role="form" method="post" id="book_patient_procedure_form" autocomplete="off" class="form-inline  book_patient_procedure_form">
                            <div class="control-group">
                                <label class="control-label" for="selectError">Patient Name (required) </label>

                                <div class="controls">
                                    <input type="text" name="patientname" id="patientname" class="patientname form-control input-sm"/>
                                </div>
                            </div>

                            <hr>
                            <div class="control-group">
                                <label class="control-label" for="selectError">Phone Number :  (required)</label>

                                <div class="controls">
                                    <input type="text" name="patient_phone" class="form-control input-sm"  id="patient_phone"/>
                                </div>
                            </div>
                            <hr>


                            <div class="control-group">
                                <label class="control-label" for="selectError">Department  (required)</label>

                                <div class="controls">
                                    <select id="selectError" name="procedure_name" required="" data-rel="">
                                        <option value="">Please select  Procedure : </option>

                                        <?php foreach ($procedure_name as $proceudre_type) { ?>
                                            <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                        <?php } ?>

                                    </select>
                                </div>



                            </div>


                            <hr>



                            <hr>

                            <input type="submit" class=" btn btn-info book_patient_procedure_button" id="book_patient_procedure_button" value="Book Patient"/>
                        </form>

                    </div>
                </div>
            </div>
            <!--/span-->
        </div>





    </div>

    <!--- Shwari  not joining form  end --> 


    <!--- Shwari   regular form  start -->

    <div id = "shwari_regular_client" style = "display: none;">

        <style type = "text/css">
            .bs-example{
                margin: 20px;
            }
        </style>


        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class = "box-header well" data-original-title = "">
                        <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Book Procedure</h2>


                    </div>
                    <div class = "box-content">

                        <div class = "bs-example">
                            <form class = "shwari_patient_book_procedure_form" method = "post" autocomplete="off"  id = "shwari_patient_book_procedure_form" role = "form">
                                <div class="form-inline">

                                    <div class = "form-inline">
                                        <div class="control-group">
                                            <label class="control-label" for="selectError">Patient Name (required) </label>

                                            <div class="controls">
                                                <select id="selectError" name="patientname" required="" data-rel="">
                                                    <option value="">Please select Patient Name : </option>
                                                    <?php foreach ($name as $name_types) { ?>
                                                        <option  value="<?php echo $name_types['title'] . ' :' . $name_types['f_name'] . ' ' . $name_types['s_name'] . ' ' . $name_types['other_name'] ?>" id="<?php echo $name_types['f_name'] ?>" ><?php echo $name_types['title'] ?> <?php echo $name_types['f_name'] ?>   <?php echo $name_types['s_name'] ?>   <?php echo $name_types['other_name'] ?>(<?php echo $name_types['family_number'] ?>)</option>
                                                    <?php } ?>

                                                </select>
                                            </div>
                                        </div>

                                        <hr>

                                    </div>


                                    <div class="form-inline">
                                        <div class="controls">
                                            <select id="selectError" name="procedure_name" required="" data-rel="">
                                                <option value="">Please select  Procedure : </option>

                                                <?php foreach ($procedure_name as $proceudre_type) { ?>
                                                    <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                    </div>





                                    <div class = "form-inline">
                                        <div class = "form-group">
                                            <input type = "submit" value = "Book for Procedure" class = "btn btn-success shwari_patient_book_procedure_button" id = "shwari_patient_book_procedure_button"/>
                                            <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                        </div>
                                    </div>

                                </div>


                            </form>
                        </div>
                    </div>






                </div>
            </div>
        </div>
        <!--/span-->
    </div>

    <!--- Shwari   regular form  end -->      


    <!--- Shwari  not regular form  start -->

    <div id = "not_regular_patient" style = "display: none;">


        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                    <div class = "box-icon">

                    </div>
                </div>

                <div class = "box-content">
                    <label class = "">
                        Would you like to Join our Clinic network ?
                    </label><br>

                    <a id = "shwari_relation_no" href = "#shwari_relation_no_form" class = "btn btn-info btn-sm shwari_relation_no">
                        Yes
                    </a>

                    <a id = "not_joining" href = "#not_interested_in_joining" class = "btn btn-info btn-sm not_joining">
                        No
                    </a>
                </div>
            </div>
        </div>
        <!--/span-->


    </div>
    <!--- Shwari  not regular form  end -->

    <!--- Shwari patient option form  start -->

    <div id = "patient_option" style = "display: none;">


        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                    <div class = "box-icon">

                    </div>
                </div>

                <div class = "box-content">
                    <label class = "">
                        Are you related to any Shwari Member ?
                    </label><br>

                    <a id = "shwari_relation_yes" class = "btn btn-info btn-sm shwari_relation_yes" href = "#shwari_relation_yes_form">
                        Yes
                    </a>

                    <a id = "shwari_relation_no" href = "#shwari_relation_no_form" class = "btn btn-info btn-sm shwari_relation_no">
                        No
                    </a>
                </div>
            </div>
        </div>
        <!--/span-->


    </div>
    <!--- Shwari patient option form  start -->

    <!--- Shwari relation yes form  start -->

    <div class = "form-control" id = "shwari_relation_yes_form" style = "display: none;">


        <style type = "text/css">
            .bs-example{
                margin: 20px;
            }
        </style>


        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class = "box-header well" data-original-title = "">
                        <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Registration</h2>


                    </div>
                    <div class = "box-content">

                        <div class = "bs-example">

                            <div class="form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputShwariFamilyNumber">Shwari Family Number :</label>
                                    <input type = "text" class = "form-control" id ="inputShwariFamilyNumber" placeholder = "Shwari Family Number">
                                </div>

                                <div class = "form-group">
                                    <div><label>Family Number Result : </label></div>
                                    <div id="shwari_family_number_result" name="shwari_family_number_result" class="shwari_family_number_result">

                                    </div>
                                </div>
                            </div>

                            <form class = "register_new_patient_form " autocomplete="off" method = "post"  id = "register_new_patient_form" role = "form">


                                <div class = "form-group">
                                    <label class = "sr-only" for = "family_number"> Family Number :</label>
                                    <input type = "text" class = "form-control" name="family_number" id ="family_number" placeholder = "Please paste the  family number : ">
                                </div>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "family_number"> Title Name :</label>
                                        <select name="title"  required="" class="selector" id="title" >
                                            <option value="">Please select title </option>
                                            <?php foreach ($title_name as $value) {
                                                ?>
                                                <option value="<?php echo $value['title_name']; ?>"><?php echo $value['title_name']; ?></option>
                                            <?php }
                                            ?>
                                        </select>   
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputFirstName">First Name : </label>
                                        <input type = "text" class = "form-control" required="" id = "inputFirstName" name="fname" placeholder = "First Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputSurName">Sur Name : </label>
                                        <input type = "text" class = "form-control" required="" id = "inputSurName" name="sname" placeholder = "Sur Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputOtherName">Other Name : </label>
                                        <input type = "text" class = "form-control" required="" id = "inputOtherName" name="lname" placeholder = "Other Name">
                                    </div>

                                </div>

                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputDateofBirth">Date of Birth</label>
                                        <input type = "text" class = "form-control" required="" name="dob" id = "datepicker" placeholder = "D.O.B">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputSurName">National/Military/Alien ID or Passport No</label>
                                        <input type = "text" class = "form-control" required="" name="nationalid" id = "inputSurName" placeholder = "National ID ">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputPostalAddress">Postal Address</label>
                                        <input type = "text" class = "form-control " required="" name="address" id = "inputPostalAddress" placeholder = "Postal Address">
                                    </div>
                                </div>


                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputResidence">Residence</label>
                                        <input type = "text" class = "form-control" required="" name="residence" id = "inputResidence" placeholder = "Residence">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputCity">City</label>
                                        <input type = "text" class = "form-control" required="" name="city" id = "inputCity" placeholder = "City">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputPhoneNo">Phone No</label>
                                        <input type = "text" class = "form-control" required="" name="phone_no" id = "inputPhoneNo" placeholder = "Phone No">
                                    </div>
                                </div>


                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputEmail">Email : </label>
                                        <input type = "email" class = "form-control" required="" name="email" id ="inputEmail"  placeholder = "Email">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputEmploymentStatus">Employment Status : </label>
                                        <span class="radio"> <input type = "radio" class = "form-control radio checkbox employmentstatus_yes" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Employed">
                                            Employed</span>
                                        <span class="radio">  <input type = "radio" class = "form-control radio checkbox employmentstatus_no" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Not Employed">
                                            Not Employed</span>

                                    </div>

                                    <div class = "form-group employers_name_div" id="employers_name_div" >
                                        <label class = "sr-only" for = "inputEmployersName">Employer's Name : </label>
                                        <input type = "text" style="display:none ;" class = "form-control employers_name" name="employers_name" id ="employers_name"  placeholder = "Employmer's Name" >

                                    </div>


                                </div>


                                <div class = "form-inline">
                                    <div class = "control-group">
                                        <label class = "control-label" for = "inputGender">Gender</label>
                                        <div class = "controls">
                                            <select class = "selectError" required="" name="sex" data-rel = "" >
                                                <option value = "Male">Male</option>
                                                <option value = "Female">Female</option>
                                                <option value = "Trans-gender">Trans-gender</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class = "control-group">
                                        <label class = "control-label" for = "inputMaritalStatus">Marital Status</label>
                                        <div class = "controls">
                                            <select class = "selectError" required="" name="maritalstatus" data-rel = "">
                                                <option value = "Single">Single</option>
                                                <option value = "Married">Married</option>
                                                <option value = "Divorced">Divorced</option>
                                                <option value = "Widowed">Widowed</option>
                                            </select>
                                        </div>
                                    </div>


                                </div>



                                <label>Next of Kin Details </label>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputFirstName">First Name</label>
                                        <input type = "text" class = "form-control" required="" id = "inputFirstName" name="kinname" placeholder = "First Name">
                                    </div><div class = "form-group">
                                        <label class = "sr-only" for = "inputLastName">Last Name</label>
                                        <input type = "text" class = "form-control" required="" id = "inputLastName" name="kinsname" placeholder = "Last Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputMiddleName">Middle Name</label>
                                        <input type = "text" class = "form-control" required="" id = "inputMiddleName" placeholder = "Middle Name">
                                    </div>
                                </div>


                                <div class = "form-inline">

                                    <div class = "control-group">
                                        <label class = "control-label" for = "inputkinrelation"> Relation Type : </label>
                                        <div class = "controls">
                                            <select class = "selectError" required="" name="kinrelation" data-rel = "">
                                                <option value = "Sibling">Sibling</option>
                                                <option value = "Father">Father</option>
                                                <option value = "Mother">Mother</option>
                                                <option value = "Husband">Husband</option>
                                                <option value = "Wife">Wife</option>
                                                <option value = "Aunt">Aunt</option>
                                                <option value="Uncle">Uncle</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputPhone">Kin's Phone No</label>
                                        <input type="text" placeholder="Kin's Phone" required="" class = "form-control"  id="kinphone" name="kinphone" size="30" />
                                    </div>

                                </div>
                                <hr>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <input type = "submit" value = "Register Patient" class = "btn btn-success add_register_new_patient" id = "add_register_new_patient"/>
                                        <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                    </div>
                                </div>




                            </form>



                        </div>
                    </div>






                </div>
            </div>
        </div>
        <!--/span-->
    </div>


    <!--- Shwari relation yes form  end -->


    <!--- Shwari relation no form  start -->

    <div class = "form-control" id = "shwari_relation_no_form" style = "display: none;">




        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class = "box-header well" data-original-title = "">
                        <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Registration Form</h2>

                        <div class = "box-icon">
                            <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                    class = "glyphicon glyphicon-cog"></i></a>
                            <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                    class = "glyphicon glyphicon-chevron-up"></i></a>
                            <a href = "#" class = "btn btn-close btn-round btn-default"><i
                                    class = "glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                    <div class = "box-content">

                        <div class = "bs-example">


                            <form class = "new_family_registration_form " autocomplete="off" method = "post"  id = "new_family_registration_form" role = "form">



                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "family_number"> Title :</label>
                                        <select name="title"  required="" class="selector" id="title" >
                                            <option value="">Please select title </option>
                                            <?php foreach ($title_name as $value) {
                                                ?>
                                                <option value="<?php echo $value['title_name']; ?>"><?php echo $value['title_name']; ?></option>
                                            <?php }
                                            ?>
                                        </select> 




                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputFirstName">First Name : </label>
                                        <input type = "text" class = "form-control" required="" id = "inputFirstName" name="fname" placeholder = "First Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputSurName">Sur Name : </label>
                                        <input type = "text" class = "form-control" required="" id = "inputSurName" name="sname" placeholder = "Sur Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputOtherName">Other Name : </label>
                                        <input type = "text" class = "form-control" required="" id = "inputOtherName" name="lname" placeholder = "Other Name">
                                    </div>

                                </div>

                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputDateofBirth">Date of Birth</label>
                                        <input type = "text" class = "form-control" required="" name="dob" id = "datepicker_1" placeholder = "D.O.B">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputSurName">National/Military/Alien ID or Passport No</label>
                                        <input type = "text" class = "form-control" required="" name="nationalid" id = "inputSurName" placeholder = "National ID ">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputPostalAddress">Postal Address</label>
                                        <input type = "text" class = "form-control" required="" name="address" id = "inputPostalAddress" placeholder = "Postal Address">
                                    </div>
                                </div>


                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputResidence">Residence</label>
                                        <input type = "text" class = "form-control" required="" name="residence" id = "inputResidence" placeholder = "Residence">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputCity">City</label>
                                        <input type = "text" class = "form-control" required="" name="city" id = "inputCity" placeholder = "City">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputPhoneNo">Phone No</label>
                                        <input type = "text" class = "form-control" required="" name="phone_no" id = "inputPhoneNo" placeholder = "Phone No">
                                    </div>
                                </div>


                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputEmail">Email : </label>
                                        <input type = "email" class = "form-control" required="" name="email" id ="inputEmail"  placeholder = "Email">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputEmploymentStatus">Employment Status : </label>
                                        <span class="radio"> <input type = "radio" class = "form-control radio checkbox employmentstatus_yes_1" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Employed">
                                            Employed</span>
                                        <span class="radio">  <input type = "radio" class = "form-control radio checkbox employmentstatus_no_1" name="employment_status" id ="employment_status"  placeholder = "Employment Status" value="Not Employed">
                                            Not Employed</span>

                                    </div>

                                    <div class = "form-group employers_name_div" id="employers_name_div_1" >
                                        <label class = "sr-only" for = "inputEmployersName">Employer's Name : </label>
                                        <input type = "text" style="display:none ;" class = "form-control employers_name_1" name="employers_name" id ="employers_name_1"  placeholder = "Employmer's Name" >

                                    </div>


                                </div>


                                <div class = "form-inline">
                                    <div class = "control-group">
                                        <label class = "control-label" for = "inputGender">Gender</label>
                                        <div class = "controls">
                                            <select class = "selectError" required="" name="sex" data-rel = "" >
                                                <option value = "Male">Male</option>
                                                <option value = "Female">Female</option>
                                                <option value = "Trans-gender">Trans-gender</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class = "control-group">
                                        <label class = "control-label" for = "inputMaritalStatus">Marital Status</label>
                                        <div class = "controls">
                                            <select class = "selectError" required="" name="maritalstatus" data-rel = "">
                                                <option value = "Single">Single</option>
                                                <option value = "Married">Married</option>
                                                <option value = "Divorced">Divorced</option>
                                                <option value = "Widowed">Widowed</option>
                                            </select>
                                        </div>
                                    </div>


                                </div>



                                <label>Next of Kin Details </label>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputFirstName">First Name</label>
                                        <input type = "text" class = "form-control" required="" id = "inputFirstName" name="kinname" placeholder = "First Name">
                                    </div><div class = "form-group">
                                        <label class = "sr-only" for = "inputLastName">Last Name</label>
                                        <input type = "text" class = "form-control" required="" id = "inputLastName" name="kinsname" placeholder = "Last Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputMiddleName">Middle Name</label>
                                        <input type = "text" class = "form-control" required="" id = "inputMiddleName" placeholder = "Middle Name">
                                    </div>
                                </div>


                                <div class = "form-inline">

                                    <div class = "control-group">
                                        <label class = "control-label" for = "inputkinrelation"> Relation Type : </label>
                                        <div class = "controls">
                                            <select class = "selectError"required="" name="kinrelation" data-rel = "">
                                                <option value = "Sibling">Sibling</option>
                                                <option value = "Father">Father</option>
                                                <option value = "Mother">Mother</option>
                                                <option value = "Husband">Husband</option>
                                                <option value = "Wife">Wife</option>
                                                <option value = "Aunt">Aunt</option>
                                                <option value="Uncle">Uncle</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputPhone">Kin's Phone No</label>
                                        <input type="text" placeholder="Kin's Phone" required=""  class = "form-control"  id="kinphone" name="kinphone" size="30" />
                                    </div>

                                </div>
                                <hr>


                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <input type = "submit" value = "Register Patient" class = "btn btn-success new_family_registration_button" id = "new_family_registration_button"/>
                                        <input type = "reset" value = "Reset"  class = "btn btn-danger" id = "save_reset"/>
                                    </div>
                                </div>




                            </form>




                        </div>
                    </div>
                </div>
            </div>
            <!--/span-->

        </div><!--/row-->

    </div>


    <!--- Shwari relation no form  end -->


</div>