<!DOCTYPE html>
<html lang="en">
    <head>
        <!--
          
        -->
        <meta charset="utf-8">
        <title>Care-tech System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Out patient Hospital Management System.">
        <meta name="author" content="Harris Samuel Dindi">

        <!-- The styles -->
        <link id="bs-css" href="<?php echo base_url(); ?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/charisma-app.css" rel="stylesheet">

        <?php
        $function_name = $this->uri->segment(2);

        $url_array = array("visit");
        if (in_array($function_name, $url_array)) {
            ?>
            <link href='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
            <link href='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
            <link href='<?php echo base_url(); ?>assets/css/jquery.timepicker.css' rel="stylesheet">
            <link href='<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css' rel="stylesheet">



            <?php
        }
        ?>


        <link href='<?php echo base_url(); ?>assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/jquery.noty.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/noty_theme_default.css' rel='stylesheet'>



        <?php
        $function_name = $this->uri->segment(2);

        $url_array = array("error");
        if (in_array($function_name, $url_array)) {
            ?>
            <link href='<?php echo base_url(); ?>assets/css/elfinder.min.css' rel='stylesheet'>
            <link href='<?php echo base_url(); ?>assets/css/elfinder.theme.css' rel='stylesheet'>
            <link href='<?php echo base_url(); ?>assets/css/uploadify.css' rel='stylesheet'>
            <?php
        }
        ?>
        <link href='<?php echo base_url(); ?>assets/css/jquery.iphone.toggle.css' rel='stylesheet'>

        <link href='<?php echo base_url(); ?>assets/css/animate.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/chosen/chosen.css' rel="stylesheet">
        <link href='<?php echo base_url(); ?>assets/chosen/chosen.min.css' rel="stylesheet">

        <link href='<?php echo base_url(); ?>assets/chosen/docsupport/prism.css' rel="stylesheet">

        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/jquery.min.js"></script>

        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->


        <!--
        Scripting starts here ...-->
        <?php
        $function_name = $this->uri->segment(2);

        $url_array = array("visit");
        $url_profile_array = array("doctor_patient_profile", "pharmacy_patient_profile", "nurse_patient_profile", "lab_patient_profile");
        if (in_array($function_name, $url_array)) {
            ?>
            <script type="text/javascript">
                $(document).ready(function () {

                    $('.visit_type').on('change', function () {

                        $visit_type = (this.value);
                        if ($visit_type === "follow_up_doctor") {
                            // code to be executed if condition is true
                            $('#package_type_visit_div').css('display', 'inline-block');
                            $('#pay_at_end_div').css('display', 'inline-block');
                            $('#active_doctor_list_div').css('display', 'inline-block');
                        } else if ($visit_type === "new_visit_doctor") {
                            $('#package_type_visit_div').css('display', 'inline-block');
                            $('#pay_at_end_div').css('display', 'inline-block');
                            $('#active_doctor_list_div').css('display', 'inline-block');
                        } else if (($visit_type === "walk_in_nurse") || ($visit_type === "walk_in_lab") || ($visit_type === "walk_in_pharmacy")) {

                            $('#package_type_visit_div').css('display', 'none');
                            $('#pay_at_end_div').css('display', 'none');
                            $('#package_type_visit_div').css('display', 'none');
                            $('#active_doctor_list_div').css('display', 'none');
                        } else {
                            // code to be executed if condition is false
                            $('#package_type_visit_div').css('display', 'inline-block');
                            $('#pay_at_end_div').css('display', 'inline-block');
                            $('#active_doctor_list_div').css('display', 'none');
                        }

                        if ($visit_type === "follow_up_nurse" || $visit_type === "follow_up_lab" || $visit_type === "follow_up_doctor") {

                            $('#charge_follow_up_div').css('display', 'inline-block');
                        } else {

                            $('#charge_follow_up_div').css('display', 'none');
                        }

                    });
                });</script>
            <?php
        }
        ?>
        <script type="text/javascript">

            $(document).ready(function () {

                $('#patient_appointments').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#register_patient').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });

                $('#yes_related_shwari').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 800,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });


                $('#not_joining').fancybox({
                    paddding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#shwari_relation_yes').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#add_test_results_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 450,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });
                $('#bill_patient_prescription_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 940,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });
                $('#dipsense_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 940,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });
                $('#order_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 940,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });
                $('#lab_send_to_doctor_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 940,
                    'height': 400,
                    'autoSize': false,
                    overlay: {closeClick: false}
                });
                $('#shwari_relation_no').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#visit_records').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#patient_procedure_option').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#regular_patient_yes').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#regular_patient_no').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#patient_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#book_visit_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#export_stock_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 540,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#export_commodity_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 540,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#add_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 800,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#view_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 800,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#edit_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 800,
                    'height': 500,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#delete_commodity_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    'width': 800,
                    'height': 300,
                    'autoSize': false,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'autoDimensions': false,
                    overlay: {closeClick: false}
                });
                $('#export_patient_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#export_vistation_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#export_patient_procedure_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#export_walkin_patient_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#walkin_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#visit_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#regular_procedure_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#walkin_procedure_report_filter_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#view_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#edit_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#delete_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#patient_medical_records_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                })


                $('#view_procedure_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#edit_procedure_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#delete_procedure_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#view_visit_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#edit_visit_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#delete_visit_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#view_walkin_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#edit_walkin_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#delete_walkin_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                //regular payment link
                $('#regular_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                //lab service payment link
                $('#lab_service_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                //pharmacy service payment link
                $('#pharmacy_service_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                //pay at the ned link 
                $('#pay_at_the_end_patient_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#walkin_nurse_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#walkin_lab_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#release_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#walkin_pharmacy_payment_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#add_triage_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#edit_triage_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#delete_triage_patient_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#nurse_view_patient_list').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#send_to_doctor_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#book_appointment_form_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                $('#test_referral_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#imaging_referral_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#other_referral_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
                $('#sick_off_link').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false},
                    'autoDimensions': false,
                    'width': 940,
                    'height': 400,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                });
            });
            $('.insurance_exists').on('change', function () {
                alert(this.value);
            });
            $("#save_submit").click(function () {

            });
            $("#save_submit_1").click(function () {

            });
            //delegated submit handlers for the forms inside the table
            $('#save_submit_1').on('click', function (e) {
                e.preventDefault();
                //read the form data ans submit it to someurl
                $.post('visit.html', $('#patient_registration_form_1').serialize(), function () {

                    generateAll();
                    setInterval(function () {
                        var url = "<?php echo base_url() ?>index.php/home";
                        $(location).attr('href', url);
                    }, 30000);
                }).fail(function () {
                    //error do something
                    $(".save_timesheet_notify").notify(
                            "There was an error please try again later or  contact the system support desk  for assistance",
                            "error",
                            {position: "left"}
                    );
                });
            });
            function generate(layout) {
                var n = noty({
                    text: 'Do you want to continue?',
                    type: 'alert',
                    dismissQueue: true,
                    layout: layout,
                    theme: 'defaultTheme',
                    buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                                $noty.close();
                                noty({dismissQueue: true, force: true, layout: layout, theme: 'defaultTheme', text: 'You clicked "Ok" button', type: 'success'});
                            }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                                $noty.close();
                                noty({dismissQueue: true, force: true, layout: layout, theme: 'defaultTheme', text: 'You clicked "Cancel" button', type: 'error'});
                            }
                        }
                    ]
                });
                console.log('html: ' + n.options.id);
            }

            function generateAll() {
                generate('center');
            }



        </script>

        <!--
        Scripting ends here
        -->

        <!-- The fav icon -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.ico">

    </head>

    <body>
        <?php
        if (!isset($no_visible_elements) || !$no_visible_elements) {
            if ($this->session->userdata('id')) {
                ?>
                <!-- topbar starts -->



                <div class="navbar navbar-default" role="navigation">

                    <div class="navbar-inner">
                        <button type="button" class="navbar-toggle pull-left animated flip">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url(); ?>home"> <img alt="Shwari Healthcare" src="<?php echo base_url(); ?>assets/img/shwari.jpg" class="hidden-xs"/>
                            <span>Shwari</span></a>

                        <!-- user dropdown starts -->
                        <div class="btn-group pull-right">
                            <?php
                            $type = $this->session->userdata('type');
                            if ($type === "Support") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Reception</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                            } elseif ($type === "Nurse") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Nurse</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                                ?>  <?php
                            } elseif ($type === "Laboratory") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Lab </span>
                                    <span class="caret"></span>
                                </button>
                                <?php ?><?php ?>  <?php
                            } elseif ($type === "Doctor") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Doctor</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                            } elseif ($type === "Pharmacy") {
                                ?>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Pharmacy</span>
                                    <span class="caret"></span>
                                </button>
                                <?php
                            }
                            ?>
                            <ul class="dropdown-menu">
                                <li><a href="#">Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>home/do_logout">Logout</a></li>
                            </ul>
                        </div>



                    </div>
                </div>
                <!-- topbar ends -->
                <?php
            }
        } else {
            
        }
        ?>
        <div class="ch-container">
            <div class="row">
                <?php
                if (!isset($no_visible_elements) || !$no_visible_elements) {


                    if ($this->session->userdata('id')) {
                        if ($type === "Support") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>
                                            <li><a class="ajax-link" id="register_patient" href="<?php echo base_url(); ?>reception/register_patient"><i class="glyphicon glyphicon-edit"></i><span> Register Patient</span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reception/visit"><i class="glyphicon glyphicon-book"></i><span> Book Visit</span></a></li>
                                            <!--<li><a class="ajax-link" href="<?php echo base_url(); ?>reception/walkin_patient"><i class="glyphicon glyphicon-plus-sign"></i><span> Add Walkin</span></a></li>-->

                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier"><i class="glyphicon glyphicon-check"></i><span> Payments</span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>reports/visitation_report"><i class="glyphicon glyphicon-check"></i><span> Edit visitation </span></a>
                                            </li>

            <!--                                            <li><a class="ajax-link" id="patient_procedure_option" href="#procedure_option"><i class="glyphicon glyphicon-check"></i><span> Book patient for Procedure </span></a>
                                                        </li>-->
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier/regular_patient_visit_statement"><i class="glyphicon glyphicon-check"></i><span> Regular Patient Visit Statements </span></a>
                                            </li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>cashier/walkin_patient_visit_statement"><i class="glyphicon glyphicon-check"></i><span> Walk-in Patient Visit Statements  </span></a>
                                            </li>

                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        } elseif ($type === "Nurse") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>



                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>stock/manage_stock"><i class="glyphicon glyphicon-check"></i><span> Stock Management </span></a>
                                            </li>

                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        } elseif ($type === "Laboratory") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>
                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        } elseif ($type === "Doctor") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>


                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        } elseif ($type === "Pharmacy") {
                            ?>

                            <div class="col-sm-2 col-lg-2">
                                <div class="sidebar-nav">
                                    <div class="nav-canvas">
                                        <div class="nav-sm nav nav-stacked">

                                        </div>

                                        <ul class="nav nav-pills nav-stacked main-menu">
                                            <li class="nav-header">Main</li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>home"><i class="glyphicon glyphicon-home"></i><span> Home</span></a>
                                            </li>

                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>pharmacy/commodity_management"><i class="glyphicon glyphicon-shopping-cart"></i><span>Commodity Management</span></a></li>
                                            <li><a class="ajax-link" href="<?php echo base_url(); ?>pharmacy/stock_management"><i class="glyphicon glyphicon-shopping-cart"></i><span>Stock Management </span></a></li>


                                            <li class="accordion">
                                                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reports Menu</span></a>
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="#patient_report_filter_form" id="patient_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Patient</span></a></li>
                                                    <li><a href="#visit_report_filter_form" id="visit_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Visitation</span></a></li>
                                                    <li><a href="#walkin_report_filter_form" id="walkin_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Walk-in</span></a></li>
                                                    <li><a href="#regular_procedure_report_filter_form" id="regular_procedure_report_filter_link"><i class="glyphicon glyphicon-align-justify"></i><span>Procedure</span></a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-header hidden-md"></li>



                                            <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="glyphicon glyphicon-off"></i><span> Logout</span></a>
                                            </li>
                                        </ul>




                                        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                                    </div>
                                </div>
                            </div>


                            <?php
                        }
                        ?>

                        <!-- left menu starts -->



                        <!--/span-->
                        <!-- left menu ends -->

                        <noscript>
                        <div class="alert alert-block col-md-12">
                            <h4 class="alert-heading">Warning!</h4>

                            <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                                enabled to use this site.</p>
                        </div>
                        </noscript>


                        <?php
                        $function_name = $this->uri->segment(2);
                        $cashier_function = $this->uri->segment(1);
                        $controller_name = $this->uri->segment(1);

                        $url_array = array("visit");
                        $url_cashier = array("cashier");
                        $controller_array = array("reception");
                        if (in_array($controller_name, $controller_array) OR in_array($controller_name, $url_cashier)) {
                            ?>




                            <script type="text/javascript">
                                $(document).ready(function () {
                                    //Get



                                    function fancybox_info() {
                                        $("#ajax_loader").fancybox({
                                            'width': 640, // or whatever
                                            'height': 320,
                                            'onComplete': function () {
                                                setTimeout(function () {
                                                    $.fancybox.close();
                                                }, 3000); // 3000 = 3 secs
                                            }
                                        });
                                    }

                                    //Set
                                    $('#txt_name').val('bla');

                                    $("#yes_related_shwari").click(function () {
                                        $('#family_number_div').show('slow');
                                        $('#form_submit_div').show('slow');
                                        var not_related = "Yes";
                                        $('#new_registration').val(not_related);
                                    });
                                    $("#no_related_shwari").click(function () {
                                        $('#copied_family_number').val('');
                                        $('#form_submit_div').show('slow');

                                        // $('#inputShwariFamilyNumber').val('');
            //                                        $('#copied_family_number').val('');
            //                                      
            //                                        $('#form_submit_div').show('slow');
                                    });

                                    $('#check_family_number').keyup(function () {
                                        check_shwari_family_relation();
                                    });
                                    $('#check_sur_name').keyup(function () {
                                        check_shwari_family_relation();
                                    });
                                    $('#check_first_name').keyup(function () {
                                        check_shwari_family_relation();
                                    });
                                    $('#check_phone_no').keyup(function () {
                                        check_shwari_family_relation();
                                    });
                                    $('#check_id_no').keyup(function () {
                                        check_shwari_family_relation();
                                    });
                                    $('#check_residence').keyup(function () {
                                        check_shwari_family_relation();
                                    });
                                    $('#check_city').keyup(function () {
                                        check_shwari_family_relation();
                                    });

                                    $('#save_family_number').click(function () {
                                        var family_number = $('#pasted_family_number').val();
                                        $('#copied_family_number').val(family_number);


                                        fancybox_info();




                                        var copied_family_number = $('#copied_family_number').val();
                                        if (copied_family_number === "") {
                                            alert('Please Key In /Paste the family in the Box below  the search results');
                                        } else {





                                        }


                                    });



                                    function checkshwarirelation() {

                                        $.ajax({
                                            type: "GET",
                                            url: "<?php echo base_url(); ?>reception/check_family_member_relation/",
                                            dataType: "json",
                                            success: function (response) {
                                                shwari_family_number_result = $('#shwari_family_number_result').empty();
                                                $.each(response, function (i, response) {

                                                    var str = response.family_base_number;
                                                    var res = str.substring(0, 14);
                                                    shwari_family_number_result.append('<span>' + res + '</span></br>');
                                                });
                                            },
                                            error: function (data) {

                                            }
                                        });
                                    }



                                    function check_shwari_family_relation() {

                                        dataString = $("#family_relation_search_form").serialize();
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url(); ?>reception/check_family_member_relation/",
                                            dataType: "json",
                                            data: dataString,
                                            success: function (response) {
                                                shwari_family_number_result = $('#output_result').empty();
                                                $('#ajax_loader').show('slow');
                                                $.each(response, function (i, response) {
                                                    $('#ajax_loader').hide('slow');
                                                    $('#output_result').show('slow');
                                                    var str = response.family_base_number;
                                                    var res = str.substring(0, 14);
                                                    var patient_name = response.patient_name;


                                                    shwari_family_number_result.append('<i class="glyphicon glyphicon-hand-right"></i> <span>' + patient_name + '</span>   <span>' + res + '</span></br>');
                                                });
                                            }

                                        });
                                        //event.preventDefault();
                                        return false;
                                    }



                                });
                            </script>








                            <?php
                        }
                        if (in_array($controller_name, $url_cashier)) {
                            ?>

                            <!-- Walkin Patient Nurse form start -->

                            <div class = "form-control" id = "walkin_patient_nurse_form" style = "display: none;">




                                <div class = "row">
                                    <div class = "box col-md-12">
                                        <div class = "box-inner">
                                            <div class = "box-header well" data-original-title = "">
                                                <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Walkin Nurse Patient Payments </h2>

                                                <div class = "box-icon">
                                                    <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-cog"></i></a>
                                                    <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-chevron-up"></i></a>

                                                </div>
                                            </div>
                                            <div class = "box-content">

                                                <div class = "bs-example">


                                                    <form class = "walkin_patient_nurse_payment_form " autocomplete="off" method = "post"  id = "walkin_patient_nurse_payment_form" role = "form">



                                                        <div class = "form-inline">

                                                            <div class = "form-group">
                                                                <label class = "sr-only" for = "inputFirstName">Patient Name : </label>
                                                                <input type = "text" readonly="" class = "form-control walkin_nurse_patient_name" id = "walkin_nurse_patient_name" name="walkin_nurse_patient_name" placeholder = "Patient Name">
                                                            </div>


                                                        </div>

                                                        <div class = "form-inline walkin_patient_nurse_payment_div" id="walkin_patient_nurse_payment_div">




                                                        </div>






                                                        <hr>


                                                        <div class = "form-inline">
                                                            <div class = "form-group">
                                                                <input type = "submit" value = "Make Payment" class = "btn btn-success walkin_payment_button" id = "walkin_payment_button"/>
                                                                <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                            </div>
                                                        </div>




                                                    </form>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->

                                </div><!--/row-->

                            </div>


                            <!-- Walkin Patient Nurse form start -->



                            <!-- Walkin Patient Lab form start -->


                            <div class = "form-control" id = "walkin_patient_lab_form" style = "display: none;">




                                <div class = "row">
                                    <div class = "box col-md-12">
                                        <div class = "box-inner">
                                            <div class = "box-header well" data-original-title = "">
                                                <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Walkin Lab Patient Payments </h2>

                                                <div class = "box-icon">
                                                    <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-cog"></i></a>
                                                    <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-chevron-up"></i></a>

                                                </div>
                                            </div>
                                            <div class = "box-content">

                                                <div class = "bs-example">


                                                    <form class = "walkin_patient_lab_payment_form " autocomplete="off" method = "post"  id = "walkin_patient_lab_payment_form" role = "form">



                                                        <div class = "form-inline">

                                                            <div class = "form-group">
                                                                <label class = "sr-only" for = "inputFirstName">Patient Name : </label>
                                                                <input type = "text" readonly="" class = "form-control walkin_lab_patient_name" id = "walkin_lab_patient_name" name="walkin_lab_patient_name" placeholder = "Patient Name">
                                                            </div>


                                                        </div>

                                                        <div class = "form-inline walkin_patient_lab_payment_div" id="walkin_patient_lab_payment_div">




                                                        </div>






                                                        <hr>


                                                        <div class = "form-inline">
                                                            <div class = "form-group">
                                                                <input type = "submit" value = "Make Payment" class = "btn btn-success walkin_payment_button" id = "walkin_payment_button"/>
                                                                <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                            </div>
                                                        </div>




                                                    </form>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->

                                </div><!--/row-->

                            </div>

                            <!-- Walkin Patient Lab form end -->


                            <!--Walkin Patient pharmacy form start -->

                            <div class = "form-control" id = "walkin_patient_pharmacy_form" style = "display: none;">




                                <div class = "row">
                                    <div class = "box col-md-12">
                                        <div class = "box-inner">
                                            <div class = "box-header well" data-original-title = "">
                                                <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Walkin Nurse Patient Payments </h2>

                                                <div class = "box-icon">
                                                    <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-cog"></i></a>
                                                    <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-chevron-up"></i></a>

                                                </div>
                                            </div>
                                            <div class = "box-content">

                                                <div class = "bs-example">


                                                    <form class = "walkin_patient_pharmacy_payment_form " autocomplete="off" method = "post"  id = "walkin_patient_pharmacy_payment_form" role = "form">



                                                        <div class = "form-inline">

                                                            <div class = "form-group">
                                                                <label class = "sr-only" for = "inputFirstName">Patient Name : </label>
                                                                <input type = "text" readonly="" class = "form-control walkin_pharmacy_patient_name" id = "walkin_pharmacy_patient_name" name="walkin_pharmacy_patient_name" placeholder = "Patient Name">
                                                            </div>


                                                        </div>

                                                        <div class = "form-inline walkin_patient_pharmacy_payment_div" id="walkin_patient_pharmacy_payment_div">




                                                        </div>






                                                        <hr>


                                                        <div class = "form-inline">
                                                            <div class = "form-group">
                                                                <input type = "submit" value = "Make Payment" class = "btn btn-success walkin_payment_button" id = "walkin_payment_button"/>
                                                                <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                            </div>
                                                        </div>




                                                    </form>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->

                                </div><!--/row-->

                            </div>

                            <!--Walkin Patient pharmacy form end -->



                            <!-- 
                            Regular Patient payment form start
                            -->

                            <div class = "form-control" id = "regular_patient_form" style = "display: none;">




                                <div class = "row">
                                    <div class = "box col-md-12">
                                        <div class = "box-inner">
                                            <div class = "box-header well" data-original-title = "">
                                                <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Regular Patient Payments </h2>

                                                <div class = "box-icon">
                                                    <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-cog"></i></a>
                                                    <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-chevron-up"></i></a>

                                                </div>
                                            </div>
                                            <div class = "box-content">

                                                <div class = "bs-example">


                                                    <form class = "regular_patient_payment_form " autocomplete="off" method = "post"  id = "regular_patient_payment_form" role = "form">



                                                        <div class = "form-inline">

                                                            <div class = "form-group">
                                                                <label class = "label label-info" for = "inputFirstName">Patient Name : </label>
                                                                <input type = "text" readonly="" required="" class = "form-control regular_patient_name" id = "regular_patient_name" name="regular_patient_name" placeholder = "Patient Name">
                                                            </div>


                                                        </div>

                                                        <div class = "form-inline regular_patient_payment_div" id="regular_patient_payment_div">




                                                        </div>






                                                        <hr>

                                                        <div class="div_regular_make_payment" id="div_regular_make_payment" style="display: none;">



                                                            <div class = "form-inline">
                                                                <div class = "form-group">
                                                                    <input type = "submit" value = "Make Payment" class = "btn btn-success regular_payment_button" id = "regular_payment_button"/>
                                                                    <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                                </div>
                                                            </div>

                                                        </div>


                                                    </form>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->

                                </div><!--/row-->

                            </div>

                            <!-- 
                            Regular Patient payment form end
                            -->





                            <!-- 
                            pharmacy Service  payment form start
                            -->

                            <div class = "form-control" id = "pharmacy_service_payment_div" style = "display: none;">




                                <div class = "row">
                                    <div class = "box col-md-12">
                                        <div class = "box-inner">
                                            <div class = "box-header well" data-original-title = "">
                                                <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Pharmacy Patient Payments </h2>

                                                <div class = "box-icon">
                                                    <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-cog"></i></a>
                                                    <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-chevron-up"></i></a>

                                                </div>
                                            </div>
                                            <div class = "box-content">

                                                <div class = "bs-example">


                                                    <form class = "pharmacy_service_payment_form " autocomplete="off" method = "post"  id = "pharmacy_service_payment_form" role = "form">



                                                        <div class = "form-inline">

                                                            <div class = "form-group">
                                                                <label class = "label label-info" for = "inputFirstName">Patient Name : </label>
                                                                <input type = "text" readonly="" required="" class = "form-control pharmacy_service_patient_name" id = "pharmacy_service_patient_name" name="pharmacy_service_patient_name" placeholder = "Patient Name">
                                                            </div>


                                                        </div>

                                                        <div class = "form-inline patient_pharmacy_service_payment_div" id="patient_pharmacy_service_payment_div">




                                                        </div>






                                                        <hr>

                                                        <div id="div_pharmacy_make_payment" class="div_pharmacy_make_payment" style="display: none;">


                                                            <div class = "form-inline">
                                                                <div class = "form-group">
                                                                    <input type = "submit" value = "Make Payment" class = "btn btn-success pharmacy_service_payment_button" id = "pharmacy_service_payment_button"/>
                                                                    <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </form>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->

                                </div><!--/row-->

                            </div>

                            <!-- 
                            pharmacy Service  payment form end
                            -->









                            <!-- 
                          Lab Service  payment form start
                            -->

                            <div class = "form-control" id = "lab_service_payment_div" style = "display: none;">




                                <div class = "row">
                                    <div class = "box col-md-12">
                                        <div class = "box-inner">
                                            <div class = "box-header well" data-original-title = "">
                                                <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Regular Patient Payments </h2>

                                                <div class = "box-icon">
                                                    <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-cog"></i></a>
                                                    <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-chevron-up"></i></a>

                                                </div>
                                            </div>
                                            <div class = "box-content">

                                                <div class = "bs-example">


                                                    <form class = "lab_service_payment_form " autocomplete="off" method = "post"  id = "lab_service_payment_form" role = "form">



                                                        <div class = "form-inline">

                                                            <div class = "form-group">
                                                                <label class = "label label-info" for = "inputFirstName">Patient Name : </label>
                                                                <input type = "text" readonly="" required="" class = "form-control lab_service_patient_name" id = "lab_service_patient_name" name="lab_service_patient_name" placeholder = "Patient Name">
                                                            </div>


                                                        </div>

                                                        <div class = "form-inline patient_lab_service_payment_div" id="patient_lab_service_payment_div">




                                                        </div>






                                                        <hr>

                                                        <div class="div_lab_make_payment" id="div_lab_make_payment" style="display: none;">



                                                            <div class = "form-inline">
                                                                <div class = "form-group">
                                                                    <input type = "submit" value = "Make Payment" class = "btn btn-success lab_service_payment_button" id = "lab_service_payment_button"/>
                                                                    <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                                </div>
                                                            </div>
                                                        </div>




                                                    </form>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->

                                </div><!--/row-->

                            </div>

                            <!-- 
                            Lab Service  payment form end
                            -->











                            <!--
                            Pay at the  end form start
                            -->


                            <div class = "form-control" id = "pay_at_the_end_patient_form" style = "display: none;">




                                <div class = "row">
                                    <div class = "box col-md-12">
                                        <div class = "box-inner">
                                            <div class = "box-header well" data-original-title = "">
                                                <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Regular Patient Payments(Express Patient) </h2>

                                                <div class = "box-icon">
                                                    <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-cog"></i></a>
                                                    <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                            class = "glyphicon glyphicon-chevron-up"></i></a>

                                                </div>
                                            </div>
                                            <div class = "box-content">

                                                <div class = "bs-example">


                                                    <form class = "pay_at_the_end_patient_payment_form " autocomplete="off" method = "post"  id = "pay_at_the_end_patient_payment_form" role = "form">



                                                        <div class = "form-inline">

                                                            <div class = "form-group">
                                                                <label class = "label label-info" for = "inputFirstName">Patient Name : </label>
                                                                <input type = "text" readonly="" required="" class = "form-control pay_at_the_end_patient_name" id = "pay_at_the_end_patient_name" name="pay_at_the_end_patient_name" placeholder = "Patient Name">
                                                            </div>


                                                        </div>

                                                        <div class = "form-inline pay_at_the_end_patient_payment_div" id="pay_at_the_end_patient_payment_div">




                                                        </div>






                                                        <hr>


                                                        <div class = "form-inline">
                                                            <div class = "form-group">
                                                                <input type = "submit" value = "Make Payment" class = "btn btn-success regular_payment_button" id = "regular_payment_button"/>
                                                                <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                                            </div>
                                                        </div>




                                                    </form>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->

                                </div><!--/row-->

                            </div>

                            <!--
                            Pay at the  end form end
                            -->






                            <?php
                        }
                        ?>









                        <!-- Walkin report filter form start -->

                        <div id="walkin_report_filter_form" style="display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Care-tech Walk-in Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/walkin_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="walkin_date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="walkin_date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Department Name : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="department_name" class="department_name" required="" data-rel="">
                                                            <option value="">Please select  Department : </option>
                                                            <option value="All">All</option>
                                                            <option value="Nursing">Nursing</option>
                                                            <option value="Laboratory">Laboratory</option>
                                                            <option value="Pharmacy">Pharmacy</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Payment Status  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="payment_status" id="package_type" required="" data-rel="">
                                                            <option value="">Please select  Payment Status : </option>
                                                            <option value="All">All</option>
                                                            <option value="Paid">Paid</option>
                                                            <option value="Not Paid">Not Paid</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                        </div>


                        <!-- Walkin report filter form end -->


                        <!--
                        Regular Patient procedure report filter form start 
                        -->
                        <div id="regular_procedure_report_filter_form" style="display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Procedure Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/procedure_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="visitation_date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="visitation_date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Patient Name : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="patient_name" class="patient_name" required="" data-rel="">
                                                            <option value="">Please select  Status : </option>
                                                            <option value="All" >All</option>
                                                            <?php foreach ($name as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['patient_id']; ?>"><?php echo $value['title'] . ':' . $value['f_name'] . $value['s_name'] . $value['other_name']; ?></option><?php }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Procedure  Type  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="procedure_type" id="procedure_type" required="" data-rel="">
                                                            <option value="">Please select  Package Type : </option>
                                                            <option value="All" >All</option>
                                                            <?php foreach ($procedure_name as $proceudre_type) { ?>
                                                                <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                        </div>

                        <!--
                        Regular Patient procedure report filter form end 
                        -->

                        <!--
                        Walkin Patient procedure report filter form start 
                        -->

                        <div id="walkin_procedure_report_filter_form" style="display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Procedure Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/procedure_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="visitation_date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="visitation_date_to"/>
                                                    </div>
                                                </div>
                                                <hr>



                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Procedure Type  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="procedure_type" class="procedure_type" required="" data-rel="">
                                                            <option value="">Please select  Procedure Type : </option>
                                                            <option value="All" >All</option>
                                                            <?php foreach ($procedure_name as $proceudre_type) { ?>
                                                                <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                        </div>
                        <!--
                        Walkin Patient procedure report form end 
                        -->

                        <!-- Visitation report filter form start -->

                        <div id="visit_report_filter_form" style="display: none;">


                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Visitation Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/visitation_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="visitation_date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="visitation_date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Patient Name : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="patient_name" class="patient_name" required="" data-rel="">
                                                            <option value="">Please select  Status : </option>
                                                            <option value="All" >All</option>
                                                            <?php foreach ($name as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['patient_id']; ?>"><?php echo $value['title'] . ':' . $value['f_name'] . $value['s_name'] . $value['other_name']; ?></option><?php }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Package Type  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="package_type" id="package_type" required="" data-rel="">
                                                            <option value="">Please select  Package Type : </option>
                                                            <option value="All" >All</option>
                                                            <?php foreach ($pack as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['package_id']; ?>"><?php echo $value['package_name']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                        </div>
                        <!-- Visitation report filter form end -->

                        <!-- Patient report filter form start -->

                        <div id = "patient_report_filter_form" style = "display: none;">

                            <style type = "text/css">
                                .bs-example{
                                    margin: 20px;
                                }
                            </style>


                            <div class = "row">
                                <div class = "box col-md-12">
                                    <div class = "box-inner">
                                        <div class = "box-header well" data-original-title = "">
                                            <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Patient Report Filter</h2>


                                        </div>
                                        <div class="box-content">
                                            <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/patient_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date From :  </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="date_from" class="date_from form-control input-sm"/>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Date To : </label>

                                                    <div class="controls">
                                                        <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="date_to"/>
                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Patient Status : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="patient_status" class="patient_status" required="" data-rel="">
                                                            <option value="">Please select  Status : </option>
                                                            <option value="All">All</option>
                                                            <option value="Active">Active</option>
                                                            <option value="In Active">In Active</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="control-group">
                                                    <label class="control-label" for="selectError">Gender  : </label>

                                                    <div class="controls">
                                                        <select id="selectError" name="gender" id="gender" required="" data-rel="">
                                                            <option value="">Please select  Gender : </option>
                                                            <option value="All">All</option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                            <option value="transgender">Trans-Gender</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>

                                                <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                                                <hr>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>


                        </div>

                        <!-- Patient report filter form end -->






































                        <div id="content" class="col-lg-10 col-sm-10">
                            <!-- content starts -->
                            <?php
                        }
                    }
                    ?>


                    <?php $this->load->view($contents); ?>





                    <?php if (!isset($no_visible_elements) || !$no_visible_elements) { ?>
                        <!-- content ends -->
                    </div><!--/#content.col-md-0-->
                <?php } ?>
            </div><!--/fluid-row-->
            <?php if (!isset($no_visible_elements) || !$no_visible_elements) { ?>


                <hr>

                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">

                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h3>Settings</h3>
                            </div>
                            <div class="modal-body">
                                <p>Here settings can be configured...</p>
                            </div>
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                                <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                            </div>
                        </div>date
                    </div>
                </div>

                <footer class="row">
                    <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://healthtechkenya.com" target="_blank">Health Tech Kenya
                        </a> 2012 - <?php echo date('Y') ?></p>


                </footer>
            <?php } ?>

        </div><!--/.fluid-container-->

        <!-- external javascript -->

        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- library for cookie management -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.cookie.js"></script>

        <?php
        $segment_1 = $this->uri->segment(2);

        $profile_array = array("visit");
        if (in_array($segment_1, $profile_array)) {
            ?>


            <!-- notification plugin -->
            <script src="<?php echo base_url(); ?>assets/js/jquery.noty.js"></script>



            <!-- application script for Time-picker --->
            <script src="<?php echo base_url(); ?>assets/js/jquery.timepicker.js"></script>
            <script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
            <script src="<?php echo base_url(); ?>assets/js/transition.js"></script>
            <script src="<?php echo base_url(); ?>assets/js/collapse.js"></script>
            <script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js"></script>

            <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker1').datetimepicker({
                                viewMode: 'years',
                                format: 'YYYY-MM-DD HH:mm:ss'
                            });
                            $('#datetimepicker2').datetimepicker({
                                viewMode: 'years',
                                format: 'YYYY-MM-DD HH:mm:ss'
                            });
                        });</script>
            <?php
        }
        ?>

        <!-- calender plugin -->
        <script src='<?php echo base_url(); ?>assets/bower_components/moment/min/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>



        <!-- star rating plugin -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.raty.min.js"></script>
        <!-- data table plugin -->
        <script src='<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js'></script>

        <!-- select or dropdown enhancer -->
        <script src="<?php echo base_url(); ?>assets/bower_components/chosen/chosen.jquery.min.js"></script>
        <!-- plugin for gallery image view -->
        <script src="<?php echo base_url(); ?>assets/bower_components/colorbox/jquery.colorbox-min.js"></script>

        <!-- library for making tables responsive -->
        <script src="<?php echo base_url(); ?>assets/bower_components/responsive-tables/responsive-tables.js"></script>
        <!-- tour plugin -->
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>

        <!-- for iOS style toggle switch -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.iphone.toggle.js"></script>
        <!-- autogrowing textarea plugin -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.autogrow-textarea.js"></script>
        <!-- multiple file upload plugin -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.uploadify-3.1.min.js"></script>
        <!-- history.js for cross-browser state change on ajax -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.history.js"></script>

        <!-- application script for Charisma demo -->
        <script src="<?php echo base_url(); ?>assets/js/charisma.js"></script>






        <!-- Add mousewheel plugin (this is optional) -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/lib/jquery.mousewheel-3.0.6.pack.js"></script>

        <!-- Add fancyBox -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.pack.js?v=2.1.4"></script>

        <!-- Optionally add helpers - button, thumbnail and/or media -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/fancyapps/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>


        <script type="text/javascript" href="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" href="<?php echo base_url(); ?>assets/fancyapps/source/jquery.fancybox.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/notify.js"></script>







        <div style="display: none !important;">
            <div class="box-content ajax_loader" id="ajax_loader">
                <ul class="ajax-loaders">

                    <img src="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" title="<?php echo base_url(); ?>assets/img/ajax-loader_001.gif" >
                    </br>
                    <br/>

                    <span class=" loader_notify clearfix" id="loader_notify">
                    </span>  

                </ul>


            </div>

        </div>

        <script type="text/javascript">


                    function add_vital_signs() {

                        dataString = $("#add_patient_triage_form").serialize();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/nurse/add_triage",
                            data: dataString,
                            success: function (data) {

                            }

                        });
                        event.preventDefault();
                        return false;
                    }


                    function order_tests_form() {

                        alert('Order made sucessfully');
                        dataString = $("#order_tests_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/doctor/order_lab_tests",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Lab Orders Successfull",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = "<?php echo base_url() ?>home";
                                    $(location).attr('href', url);
                                }, 300000);
                                alert('Order made sucessfully');
                            }

                        });
                        event.preventDefault();
                        return false;
                    }

                    function reason_for_visit() {

                        dataString = $("#reason_for_visit_form").serialize();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/nurse/add_reason_for_visit",
                            data: dataString,
                            success: function (data) {

                            }

                        });
                        event.preventDefault();
                        return false;
                    }


                    function add_lab_test_results() {

                        dataString = $("#patient_test_results_form").serialize();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/lab/add_lab_test_results",
                            data: dataString,
                            success: function (data) {

                            }

                        });
                        event.preventDefault();
                        return false;
                    }

                    function add_allergy() {




                        dataString = $("#add_allergy_form").serialize();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>nurse/add_allergy",
                            data: dataString,
                            success: function (data) {

                            }

                        });
                        event.preventDefault();
                        return false;
                    }

                    function add_social_history() {




                        dataString = $("#add_family_social_form").serialize();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>nurse/add_social_history",
                            data: dataString,
                            success: function (data) {

                            }

                        });
                        event.preventDefault();
                        return false;
                    }

                    function add_immunization() {

                        dataString = $("#add_immunization_form").serialize();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>nurse/add_immunization",
                            data: dataString,
                            success: function (data) {

                            }

                        });
                        event.preventDefault();
                        return false;
                    }

                    function book_appointment() {

                        dataString = $("#appointment_form_book").serialize();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/nurse/book_appointment",
                            data: dataString,
                            success: function (data) {

                            }

                        });
                        event.preventDefault();
                        return false;
                    }
                    function chief_complaints() {

                        dataString = $("#chief_complaints_form").serialize();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/doctor/chief_complaints",
                            data: dataString,
                            success: function (data) {

                            }

                        });
                        event.preventDefault();
                        return false;
                    }
                    function review_of_systems() {

                        dataString = $("#review_of_systems_form").serialize();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/doctor/review_of_systems",
                            data: dataString,
                            success: function (data) {

                            }

                        });
                        event.preventDefault();
                        return false;
                    }

                    function working_diagnosis() {

                        dataString = $("#working_diagnosis_form").serialize();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/doctor/working_diagnosis",
                            data: dataString,
                            success: function (data) {

                            }

                        });
                        event.preventDefault();
                        return false;
                    }

                    function icd_10_code() {

                        dataString = $("#patient_icd_10_form").serialize();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/doctor/patient_icd_10",
                            data: dataString,
                            success: function (data) {

                            }

                        });
                        event.preventDefault();
                        return false;
                    }

        </script>
        <script type="text/javascript">
            $(document).ready(function () {



                var stock_management = window.location.href.indexOf("stock_management") > -1;
                if (stock_management) {

                    $('.add_commodity_name').on('change', function () {

                        var commodity_id = this.value;
                        $(".div_1_hide").hide('slow');
                        $(".div_2_hide").hide('slow');
                        $('.add_commodity_type').val("");
                        $('.add_commodity_code').val("");
                        $('.add_supplier_name').val("");
                        $('.add_no_of_packs').val("");
                        $('.add_unit_per_pack').val("");
                        $('.add_total_quantity').val("");
                        $('.add_buying_price').val("");
                        $('.add_has_expired').val("");
                        $('.add_remarks').val("");
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_commodity_info/" + commodity_id,
                            dataType: "json",
                            success: function (response) {

                                $('.add_commodity_code').val(response[0].code);
                                $('.add_commodity_type').val(response[0].commodity_type);
                                $(".div_1_hide").show('slow');
                                $(".div_2_hide").show('slow');
                            },
                            error: function (data) {
                                $(".add_new_stock_form").blur();
                            }
                        });
                    });
                    $(".add_no_of_packs").keyup(function () {


                        //Get
                        var no_of_packs = $('#add_no_of_packs').val();
                        var no_of_packs = parseFloat(no_of_packs);
                        var unit_per_pack = $('#add_unit_per_pack').val();
                        var unit_per_pack = parseFloat(unit_per_pack);
                        var total_quantity = no_of_packs * unit_per_pack;
                        //Set
                        $('.add_total_quantity').val(total_quantity);
                    });
                    $(".add_unit_per_pack").keyup(function () {

                        //Get
                        var no_of_packs = $('#add_no_of_packs').val();
                        var no_of_packs = parseFloat(no_of_packs);
                        var unit_per_pack = $('#add_unit_per_pack').val();
                        var unit_per_pack = parseFloat(unit_per_pack);
                        var total_quantity = no_of_packs * unit_per_pack;
                        //Set
                        $('.add_total_quantity').val(total_quantity);
                    });
                    $('.edit_commodity_name').on('change', function () {

                        var commodity_id = this.value;
                        $('.info_loader').show('slow');
                        $(".div_1_hide").hide('slow');
                        $(".div_2_hide").hide('slow');
                        $('.edit_commodity_type').val("");
                        $('.edit_commodity_code').val("");
                        $('.edit_supplier_name').val("");
                        $('.edit_no_of_packs').val("");
                        $('.edit_unit_per_pack').val("");
                        $('.edit_total_quantity').val("");
                        $('.edit_buying_price').val("");
                        $('.edit_has_expired').val("");
                        $('.edit_remarks').val("");
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_commodity_info/" + commodity_id,
                            dataType: "json",
                            success: function (response) {

                                $('.edit_commodity_code').val(response[0].code);
                                $('.edit_commodity_type').val(response[0].commodity_type);
                                $('.info_loader').hide('slow');
                                $(".div_1_hide").show('slow');
                                $(".div_2_hide").show('slow');
                            },
                            error: function (data) {
                                $(".edit_new_stock_form").blur();
                            }
                        });
                    });
                    $(".edit_no_of_packs").keyup(function () {


                        //Get
                        var no_of_packs = $('#edit_no_of_packs').val();
                        var no_of_packs = parseFloat(no_of_packs);
                        var unit_per_pack = $('#edit_unit_per_pack').val();
                        var unit_per_pack = parseFloat(unit_per_pack);
                        var total_quantity = no_of_packs * unit_per_pack;
                        //Set
                        $('.edit_total_quantity').val(total_quantity);
                    });
                    $(".edit_unit_per_pack").keyup(function () {

                        //Get
                        var no_of_packs = $('#edit_no_of_packs').val();
                        var no_of_packs = parseFloat(no_of_packs);
                        var unit_per_pack = $('#edit_unit_per_pack').val();
                        var unit_per_pack = parseFloat(unit_per_pack);
                        var total_quantity = no_of_packs * unit_per_pack;
                        //Set
                        $('.edit_total_quantity').val(total_quantity);
                    });
                    $('#add_new_stock_form').submit(function (event) {
                        dataString = $("#add_new_stock_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reports/add_new_stock",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "New Stock Added Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#edit_stock_form').submit(function (event) {
                        dataString = $("#edit_stock_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please $ait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reports/edit_stock_info",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        " Stock Updated Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#delete_stock_form').submit(function (event) {
                        dataString = $("#delete_stock_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please $ait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reports/delete_stock_info",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        " Stock Deleted Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                }


            });</script>

        <script type="text/javascript">
            $(document).ready(function () {

                $('#send_to_pharmacy_form').submit(function (event) {
                    dataString = $("#send_to_pharmacy_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/nurse/send_to_pharmacy",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Patient Sent to Pharmacy Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = "<?php echo base_url() ?>home";
                                $(location).attr('href', url);
                            }, 300000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
                $('#order_tests_form').submit(function (event) {


                    // order_tests_form();

                    dataString = $("#order_tests_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/doctor/order_lab_tests",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Lab Orders  made Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = "<?php echo base_url() ?>home";
                                $(location).attr('href', url);
                            }, 3000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
                $('#order_commodity_form').submit(function (event) {


                    // order_tests_form();

                    dataString = $("#order_commodity_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/doctor/order_commodity",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    " Order made Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = window.location.path;
                                $(location).attr('href', url);
                            }, 300000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
            });</script>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#inputquantity_billed").keyup(function () {

                    var inputQuantityavailable = $('#inputQuantityavailable').val();
                    var inputsellingprice = $('#inputsellingprice').val();
                    var inputquantity_billed = $('#inputquantity_billed').val();
                    var inputQuantityavailable = parseInt(inputQuantityavailable);
                    var inputsellingprice = parseInt(inputsellingprice);
                    var inputquantity_billed = parseInt(inputquantity_billed);
                    var total_cost = inputsellingprice * inputquantity_billed;
                    $('#input_total_cost').val(total_cost);
                    if (inputquantity_billed > inputQuantityavailable) {
                        alert("You cannot bill a patient for more than what is available in Stock.");
                        $(this).val('');
                        $(this).focus();
                    }
                });
                $('#bill_commodity_name').on('change', function () {

                    var commodity_id = this.value;
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>pharmacy/get_available_commodity/" + commodity_id,
                        dataType: "json",
                        success: function (response) {

                            $('#inputstrength').val(response[0].strength);
                            $('#inputtransaction_id').val(response[0].transaction_id);
                            $('#transaction_total_quantiy').val(response[0].transaction_total_quantity);
                            $('#inputQuantityavailable').val(response[0].available_quantity);
                            $('#inputsellingprice').val(response[0].selling_price);
                            var available_quantity_in_stock = response[0].available_quantity;
                            if (available_quantity_in_stock !== null) {
                                $("#bill_commodity_info").show("slow");
                            } else {
                                alert("You cannot issue a commodity with empty quantity. ");
                                $("#bill_commodity_info").hide("slow");
                            }



                        },
                        error: function (data) {

                        }
                    });
                });
                $('#bill_patient_form').submit(function (event) {
                    dataString = $("#bill_patient_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/pharmacy/bill_patient",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Patient Billed Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = window.location.href;
                                $(location).attr('href', url);
                            }, 3000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
                $('#release_patient_form').submit(function (event) {
                    dataString = $("#release_patient_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/doctor/release_patient",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Patient Released from the Queue Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = "<?php echo base_url() ?>home";
                                $(location).attr('href', url);
                            }, 3000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
                $('#send_to_current_doctor_form').submit(function (event) {
                    dataString = $("#send_to_current_doctor_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/nurse/send_to_doctor",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Patient Sent to Doctor Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = "<?php echo base_url() ?>home";
                                $(location).attr('href', url);
                            }, 3000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
                $('#send_to_doctor_form').submit(function (event) {
                    dataString = $("#send_to_doctor_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/nurse/send_to_doctor",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Patient Sent to Doctor Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = "<?php echo base_url() ?>home";
                                $(location).attr('href', url);
                            }, 3000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
                $('#send_to_pharmacy_form').submit(function (event) {
                    dataString = $("#send_to_pharmacy_form").serialize();
                    $.fancybox.open([
                        {
                            href: '#ajax_loader',
                            title: 'Please wait information being updated...'
                        }
                    ], {
                        padding: 0
                    });
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/nurse/send_to_pharmacy",
                        data: dataString,
                        success: function (data) {
                            $(".loader_notify").notify(
                                    "Patient Sent to Pharmacy Successfully",
                                    "success",
                                    {position: "center"}
                            );
                            setInterval(function () {
                                var url = "<?php echo base_url() ?>home";
                                $(location).attr('href', url);
                            }, 300000);
                        }

                    });
                    event.preventDefault();
                    return false;
                });
            });</script>

        <?php
        $third_parameter = $this->uri->segment(2);
        echo 'THIRD PARAMETER:'.$third_parameter.'<br>';
        if (empty($third_parameter)) {
            ?>
            <script type="text/javascript">
                $(document).ready(function () {


                    var reception_index_url = window.location.href.indexOf("reception") > 1;
                    if (reception_index_url) {


                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>nurse/active_doctors_list",
                                dataType: "JSON",
                                success: function (active_doctors_list) {
                                    send_to_doctor_name = $('#send_to_doctor_name').empty();
                                    if (send_to_doctor_name === null) {
                                        send_to_doctor_name.append("<option> No Active Doctors in the  System</option>");
                                    } else {
                                        $.each(active_doctors_list, function (i, active_doctors_lists) {
                                            if (active_doctors_lists.employee_id === null) {
                                                send_to_doctor_name.append("<option> No Active Doctors in the  System</option>");
                                            } else {
                                                if (active_doctors_lists.employee_id === null) {
                                                    send_to_doctor_name.append("<option> No Active Doctors in the  System</option>");
                                                } else {
                                                    send_to_doctor_name.append('<option value="' + active_doctors_lists.employee_id + '">' + active_doctors_lists.employee_name + '</option>');
                                                }
                                            }

                                        });
                                    }



                                },
                                error: function (data) {
                                    //
                                    //  alert('An error occured, kindly try later');
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/get_active_doctors/",
                                dataType: "JSON",
                                success: function (active_doctors) {
                                    active_doctor = $('#active_doctor_list_1').empty();
                                    if (active_doctor === null) {
                                        active_doctor.append("<option>No  Active Doctor</opton>");
                                    }
                                    $.each(active_doctors, function (i, active_doctorss) {
                                        active_doctor.append('<option value="' + active_doctorss.employee_id + '">' + active_doctorss.employee_name + '</option>');
                                    });
                                },
                                error: function (data) {
                                    //
                                    //  alert('An error occured, kindly try later');
                                }
                            });
                        }, 2000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/total_visits",
                                dataType: "JSON",
                                success: function (total_visits) {
                                    total_visit = $('#total_regular_visits').empty();
                                    if (total_visit === null) {
                                        total_visit.append("<option>No Visits</opton>");
                                    } else {
                                        $.each(total_visits, function (i, total_visitss) {
                                            if (total_visitss.total_patient_visits === null) {
                                                total_visit.append("<p>No Visits</p>");
                                            } else {
                                                total_visit.append('<p>' + total_visitss.total_patient_visits + '</p>');
                                            }

                                        });
                                    }



                                },
                                error: function (data) {
                                    //
                                    //  alert('An error occured, kindly try later');
                                }
                            });
                        }, 3000);
                        //reception
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/total_walkin_patients",
                                dataType: "JSON",
                                success: function (walkin_visits) {
                                    walkin_visit = $('#total_walkin_patient_visits').empty();
                                    if (walkin_visit === null) {
                                        walkin_visit.append("<p>No Walkin Visits</p>");
                                    } else {
                                        $.each(walkin_visits, function (i, walkin_visitss) {

                                            if (walkin_visitss.total_walkin_patient === null) {
                                                walkin_visit.append("<p>No Walkin Visits</p>");
                                            } else {
                                                walkin_visit.append('<p>' + walkin_visitss.total_walkin_patient + '</p>');
                                            }

                                        });
                                    }


                                },
                                error: function (data) {
                                    //
                                    //  alert('An error occured, kindly try later');
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/seeappointments",
                                dataType: "JSON",
                                success: function (appointed) {
                                    appointment_list = $('#patient_appointments').empty();
                                    if (appointed === null) {


                                    }
                                    else {

                                        $.each(appointed, function (i, appointment) {

                                            if (appointment.urgency === "urgent") {
                                                appointment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + appointment.f_name + " " + appointment.s_name + " " + appointment.other_name + '</a></li><span style="color:red !important;">' + appointment.urgency + '</span>');
                                            }
                                            else if (appointment_list.urgency !== "urgent")
                                            {
                                                appointment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + appointment.f_name + " " + appointment.s_name + " " + appointment.other_name + '</li>');
                                            }
                                            else {
                                                appointment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                            }


                                        });
                                    }

                                },
                                error: function (data) {

                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/to_paywalkin",
                                dataType: "JSON",
                                success: function (to_paywalkin) {
                                    to_paywalkin_list = $('#walk_in_patient_payments').empty();
                                    if (to_paywalkin === null) {

                                    } else {
                                        $.each(to_paywalkin, function (i, to_paywalkin) {

                                            if (to_paywalkin.urgency === "urgent") {
                                                to_paywalkin_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + to_paywalkin.fname + " " + to_paywalkin.lname + " " + to_paywalkin.sname + '</a><span style="color:red !important;">' + to_paywalkin.urgency + '</span></li>');
                                            }
                                            else if (to_paywalkin.urgency !== "urgent")
                                            {
                                                to_paywalkin_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + to_paywalkin.walkin_patient_name + " " + to_paywalkin.faculty + " " + to_paywalkin.walkin_payments_total + '</a></li>');
                                            }
                                            else {
                                                to_paywalkin_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                            }


                                        });
                                    }


                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/pat_payment",
                                dataType: "JSON",
                                success: function (payment_list) {
                                    pat_payment_list = $('#regular_patients_in_traypayments').empty();
                                    if (payment_list === null) {

                                    } else {
                                        $.each(payment_list, function (i, payment_list) {

                                            if (payment_list.urgency === "urgent") {
                                                if (payment_list.charge_followup === "Yes") {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.urgency + '</span> Kshs :<span style="color:red !important;">' + payment_list.total_cost + '</span>(Follow Up)</li></br>');

                                                } else {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.urgency + '</span> Kshs :<span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');

                                                }
                                            }
                                            else if (payment_list.urgency !== "urgent")
                                            {
                                                if (payment_list.charge_followup === "Yes") {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a> Kshs :<span style="color:red !important;">' + payment_list.total_cost + '</span>(Follow Up)</li></br>');

                                                } else {
                                                    pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a> Kshs :<span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');

                                                }
                                            }
                                            else {
                                                if (payment_list.charge_followup === "Yes") {
                                                    pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");

                                                } else {
                                                    pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");

                                                }
                                                pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                            }

                                        });
                                    }


                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/active_doctors",
                                dataType: "JSON",
                                success: function (doctor_list) {
                                    active_doctor_list = $('#active_doctor_list').empty();
                                    if (doctor_list === null) {
                                        active_doctor_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Doctor</a> </li>");
                                    } else {
                                        $.each(doctor_list, function (i, doctor_list) {


                                            active_doctor_list.append('<li><a class="doctor" href="#active_patients_in_doctor"><i class = "glyphicon glyphicon-user"></i>' + doctor_list.user_name + '</a></li>');
                                            var doctor_id = doctor_list.employee_id;
                                            $.ajax({
                                                type: "GET",
                                                url: "<?php echo base_url(); ?>reception/d_active/" + doctor_id,
                                                dataType: "JSON",
                                                success: function (people) {
                                                    waiting_list = $('#waiting_list').empty();
                                                    if (people === null) {


                                                        waiting_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                    } else {
                                                        $.each(people, function (i, waiting) {

                                                            if (waiting.urgency === "urgent") {
                                                                waiting_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + waiting.f_name + " " + waiting.other_name + " " + waiting.s_name + '<span style="color:red !important;">' + waiting.urgency + '</span></li>');
                                                            }
                                                            else if (waiting_list.urgency !== "urgent") {
                                                                waiting_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + waiting.f_name + " " + waiting.other_name + " " + waiting.s_name + '</a></li>');
                                                            }


                                                        });
                                                    }

                                                },
                                                error: function (data) {
                                                    //error do something
                                                }
                                            });
                                        });
                                    }


                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/active_patient_doctor",
                                dataType: "JSON",
                                success: function (patient_doctor_list) {
                                    active_patient_doctor_list = $('#active_patient_visit_doctor').empty();
                                    $.each(patient_doctor_list, function (i, patient_doctor_list) {

                                        if (patient_doctor_list.urgency === "urgent") {
                                            active_patient_doctor_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + patient_doctor_list.f_name + " " + patient_doctor_list.other_name + " " + patient_doctor_list.s_name + '</a><span style="color:red !important;">' + patient_doctor_list.urgency + '</span></li>');
                                        }
                                        else if (patient_doctor_list.urgency !== "urgent")
                                        {
                                            active_patient_doctor_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + patient_doctor_list.f_name + " " + patient_doctor_list.other_name + " " + patient_doctor_list.s_name + '</a></li>');
                                        }
                                        else {
                                            active_patient_doctor_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                        }

                                    });
                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/active_nurses",
                                dataType: "JSON",
                                success: function (nurse_list) {
                                    active_nurse_list = $('#active_nurses').empty();
                                    if (nurse_list === null) {
                                        active_nurse_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Nurse</a> </li>");
                                    } else {
                                        $.each(nurse_list, function (i, nurse_list) {


                                            active_nurse_list.append('<li><i class = "glyphicon glyphicon-user"></i><a class="nurse" href="#active_patients_in_nurse">' + nurse_list.user_name + '</a></li>');
                                            var nurse_id = nurse_list.employee_id;
                                            $.ajax({
                                                type: "GET",
                                                url: "<?php echo base_url(); ?>reception/load_data/" + nurse_id,
                                                dataType: "JSON",
                                                success: function (nurse) {
                                                    nurse_list = $('#nurse_list').empty();
                                                    if (nurse === null) {

                                                        nurse_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                    }
                                                    else {
                                                        $.each(nurse, function (i, nurse) {

                                                            if (nurse.urgency === "urgent") {
                                                                nurse_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + nurse.f_name + " " + nurse.other_name + " " + nurse.s_name + '<span style="color:red !important;">' + nurse.urgency + '</span></li>');
                                                            }
                                                            else if (nurse.urgency !== "urgent")
                                                            {
                                                                nurse_list.append('<li><i class = "glyphicon glyphicon-user"></i>' + nurse.f_name + " " + nurse.other_name + " " + nurse.s_name + '</li>');
                                                            }
                                                            else {
                                                                nurse_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                            }
                                                        });
                                                    }

                                                },
                                                error: function (data) {
                                                    //error do something                          
                                                }
                                            });
                                        });
                                    }
                                },
                                error: function (data) {
                                    //error do something            
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/active_laboratories",
                                dataType: "JSON",
                                success: function (laboratory_list) {
                                    active_laboratory_list = $('#active_laboratories').empty();
                                    if (laboratory_list === null) {
                                        active_laboratory_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Lab Tech</a> </li>");
                                    } else {
                                        $.each(laboratory_list, function (i, laboratory_list) {

                                            active_laboratory_list.append('<li><a class="laboratorist" href=""><i class="glyphicon glyphicon-user"></i>' + laboratory_list.user_name + '</a></li>');
                                            var laboratorist_id = laboratory_list.employee_id;
                                            $.ajax({
                                                type: "GET",
                                                url: "<?php echo base_url(); ?>reception/l_active/" + laboratorist_id,
                                                dataType: "JSON",
                                                success: function (laboratory) {
                                                    laboratory_list = $('#lab_list').empty();
                                                    if (laboratory === null) {
                                                        laboratory_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                    } else {
                                                        $.each(laboratory, function (i, laboratory) {
                                                            if (laboratory.urgency === "urgent") {
                                                                laboratory_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + laboratory.f_name + " " + laboratory.other_name + " " + laboratory.s_name + '</ul></a><span style="color:red !important;">' + pharm.urgency + '</span></br></ul>');
                                                            }
                                                            else if (laboratory.urgency !== "urgent")
                                                            {
                                                                laboratory_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + laboratory.f_name + " " + laboratory.other_name + " " + laboratory.s_name + '</br></ul>');
                                                            }
                                                            else {
                                                                laboratory_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                            }
                                                        });
                                                    }

                                                },
                                                error: function (data) {
                                                    //error do something
                                                }
                                            });
                                        });
                                    }

                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/active_pharmacist",
                                dataType: "JSON",
                                success: function (pharm_list) {
                                    active_pharm_list = $('#active_pharm_list').empty();
                                    if (pharm_list === null) {
                                        active_pharm_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active Pharm Tech</a> </li>");
                                    } else {
                                        $.each(pharm_list, function (i, pharm_list) {

                                            active_pharm_list.append('<a class="pharmacist"  href=""><i class="glyphicon glyphicon-user"></i>' + pharm_list.user_name + '</a></br>');
                                            var pharmacist_id = pharm_list.employee_id;
                                            $.ajax({
                                                type: "GET",
                                                url: "<?php echo base_url(); ?>reception/p_active/" + pharmacist_id,
                                                dataType: "JSON",
                                                success: function (pharm) {
                                                    pharm_list = $('#pharm_list').empty();
                                                    if (pharm === null) {
                                                        pharm_list.append("<ul>No Active Patient</ul>");
                                                    } else {
                                                        $.each(pharm, function (i, pharm) {
                                                            if (pharm.urgency === "urgent") {
                                                                pharm_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + pharm.f_name + " " + pharm.other_name + " " + pharm.s_name + '</ul></a></br></ul>');
                                                            }
                                                            else if (pharm.urgency !== "urgent")
                                                            {
                                                                pharm_list.append('<ul><i class="glyphicon glyphicon-user"></i>' + pharm.f_name + " " + pharm.other_name + " " + pharm.s_name + '</br></ul>');
                                                            }
                                                            else {
                                                                pharm_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                                            }
                                                        });
                                                    }

                                                },
                                                error: function (data) {
                                                    //error do something
                                                }
                                            });
                                        });
                                    }

                                },
                                error: function (data) {
                                    //error do something
                                }
                            });
                        }, 3000);



                    } else {

                    }

                });


            </script>

            <?php
        } elseif (!empty($third_parameter)) {
            ?>
            <script>
            $(document).ready(function(){
                      setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>nurse/active_doctors_list",
                                dataType: "JSON",
                                success: function (active_doctors_list) {
                                    send_to_doctor_name = $('#send_to_doctor_name').empty();
                                    if (send_to_doctor_name === null) {
                                        send_to_doctor_name.append("<option> No Active Doctors in the  System</option>");
                                    } else {
                                        $.each(active_doctors_list, function (i, active_doctors_lists) {
                                            if (active_doctors_lists.employee_id === null) {
                                                send_to_doctor_name.append("<option> No Active Doctors in the  System</option>");
                                            } else {
                                                if (active_doctors_lists.employee_id === null) {
                                                    send_to_doctor_name.append("<option> No Active Doctors in the  System</option>");
                                                } else {
                                                    send_to_doctor_name.append('<option value="' + active_doctors_lists.employee_id + '">' + active_doctors_lists.employee_name + '</option>');
                                                }
                                            }

                                        });
                                    }



                                },
                                error: function (data) {
                                    //
                                    //  alert('An error occured, kindly try later');
                                }
                            });
                        }, 3000);
                        setInterval(function () {
                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>reception/get_active_doctors/",
                                dataType: "JSON",
                                success: function (active_doctors) {
                                    active_doctor = $('#active_doctor_list_1').empty();
                                    if (active_doctor === null) {
                                        active_doctor.append("<option>No  Active Doctor</opton>");
                                    }
                                    $.each(active_doctors, function (i, active_doctorss) {
                                        active_doctor.append('<option value="' + active_doctorss.employee_id + '">' + active_doctorss.employee_name + '</option>');
                                    });
                                },
                                error: function (data) {
                                    //
                                    //  alert('An error occured, kindly try later');
                                }
                            });
                        }, 2000);
            });
            </script>

            <?php
        } {
            
        }
        ?>



        <script type="text/javascript">
            $(document).ready(function () {

                var reception_url = window.location.href.indexOf("reception") > -1;
                if (reception_url) {



                    $('#join_network').change(function () {
                        var join_network = $("#join_network").val();

                        if (join_network === "Yes") {

                            $('#shwari_relation_div').show('slow');

                        } else {

                            $('#shwari_relation_div').hide('slow');
                            $('#copied_family_number').val('');
                            $('#form_submit_div').show('slow');
                        }
                        ;
                    });

                    $('#no_join_network').click(function () {

                    });


                    //reception
                    $('#new_family_registration_form').submit(function (event) {
                        dataString = $("#new_family_registration_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reception/new_family_tree",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Details saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = "<?php echo base_url(); ?>index.php/reception/reception/newpatvisit";
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('.employmentstatus_yes').click(function () {

                        // $("#txtAge").toggle(this.checked);
                        $('.employers_name').css('display', 'inline');
                    });
                    $('.employmentstatus_no').click(function () {

                        // $("#txtAge").toggle(this.checked);
                        $('.employers_name').val('');
                        $('.employers_name').css('display', 'none');
                    });
                    $('.employmentstatus_yes_1').click(function () {
                        // $("#txtAge").toggle(this.checked);
                        $('.employers_name_1').css('display', 'inline');
                    });
                    $('.employmentstatus_no_1').click(function () {
                        // $("#txtAge").toggle(this.checked);
                        $('.employers_name_1').val('');
                        $('.employers_name_1').css('display', 'none');
                    });
                    $('#register_new_patient_form').submit(function (event) {
                        dataString = $("#register_new_patient_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reception/add_patient",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Details saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = "<?php echo base_url(); ?>index.php/reception/reception/newpatvisit";
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    //reception
                    $('#add_visit_form').submit(function (event) {
                        dataString = $("#add_visit_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reception/new_visit",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "New Visit Added Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                $('.patient_visit_name').prop('selectedIndex', 0);
                                $('.pay_at_the_end').val("");
                                $('.queue_to_join').empty();
                                $('.visit_date_start').val("");
                                $('.visit_date_end').val("");
                                $('.active_doctor_list_1').val("");
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#inputShwariFamilyNumber').keyup(function () {
                        //get

                        member_search = $('#inputShwariFamilyNumber').val();
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reception/check_shari_member_existence/" + member_search,
                            dataType: "json",
                            success: function (response) {
                                shwari_family_number_result = $('#shwari_family_number_result').empty();
                                $.each(response, function (i, response) {

                                    var str = response.family_base_number;
                                    var res = str.substring(0, 14);
                                    shwari_family_number_result.append('<span>' + res + '</span></br>');
                                });
                            },
                            error: function (data) {

                            }
                        });
                    });
                    $('#employmentstatus_yes').click(function () {
                        // $("#txtAge").toggle(this.checked);
                        $('#employer_1').css('display', 'inline');
                    });
                    $('#employmentstatus_no').click(function () {
                        // $("#txtAge").toggle(this.checked);
                        $('#employer_1').css('display', 'none');
                    });
                    $('#add_walkin_patient_form').submit(function (event) {
                        dataString = $("#add_walkin_patient_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reception/add_walkin",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "New Walkin Patient Added Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#book_patient_procedure_form').submit(function (event) {
                        dataString = $("#book_patient_procedure_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reception/add_patient_procedure",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Booked for Procedure",
                                        "success",
                                        {position: "center"}
                                );
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#shwari_patient_book_procedure_form').submit(function (event) {
                        dataString = $("#shwari_patient_book_procedure_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reception/add_procedure",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Booked for Procedure",
                                        "success",
                                        {position: "center"}
                                );
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                }
            });</script>



        <!---Reports PROFILE START  -->
        <script type="text/javascript">

            $(document).ready(function () {
                var reports_url = window.location.href.indexOf("reports") > -1;
                if (reports_url) {

                    //reports
                    $('#edit_patient_information_form').submit(function (event) {
                        dataString = $("#edit_patient_information_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reports/edit_patient_info",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Details saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#edit_visitation_info').submit(function (event) {
                        dataString = $("#edit_visitation_info").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reports/edit_visitation_info",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Visitation Details updated Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('#delete_visit_information_form').submit(function (event) {
                        dataString = $("#delete_visit_information_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/reports/delete_visitation_info",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Vistation Details updated Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    //reports
                    $('.delete_patient_information_form').submit(function (event) {
                        dataString = $(".delete_patient_information_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>reports/delete_patient",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Record Deleted Succesfully ",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('.edit_patient_information_form').submit(function (event) {
                        dataString = $(".edit_patient_information_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>reports/edit_patient",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Record Edited Succesfully ",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('.edit_walkin_patient_info').submit(function (event) {
                        dataString = $(".edit_walkin_patient_info").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>reports/edit_walkin_patient",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Walkin Patient Details Updated Succesfully ",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('.delete_walkin_patient_information_form').submit(function (event) {
                        dataString = $(".delete_walkin_patient_information_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>reports/delete_walkin_patient",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Walkin Patient Details Updated Succesfully ",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('.edit_procedure_visit_info').submit(function (event) {
                        dataString = $(".edit_procedure_visit_info").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>reports/edit_procedure_visit_data",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        " Patient Procedure Details Updated Succesfully ",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    $('.delete_procedure_information_form').submit(function (event) {
                        dataString = $(".delete_procedure_information_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>reports/delete_procedure_visit_data",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        " Patient Procedure Details Updated Succesfully ",
                                        "success",
                                        {position: "center"}
                                );
                                setInterval(function () {
                                    var url = window.location.href;
                                    $(location).attr('href', url);
                                }, 30000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                }
            });</script>
        <!--- Reports profile end -->
        <!-- Cashier Profile Start -->
        <script type="text/javascript">

            $(document).ready(function () {

                var cashier_url = window.location.href.indexOf("cashier") > -1;
                if (cashier_url) {

                    //cashier
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/total_regular_payments",
                            dataType: "JSON",
                            success: function (total_visits) {
                                total_visit = $('#total_regular_payments').empty();
                                if (total_visit === null) {
                                    total_visit.append("<p>No Payments</p>");
                                } else {
                                    $.each(total_visits, function (i, total_visitss) {
                                        if (total_visitss.amount_owed === null) {
                                            total_visit.append("<p>No Payments</p>");
                                        } else {
                                            total_visit.append('<p>' + total_visitss.amount_owed + '</p>');
                                        }

                                    });
                                }



                            },
                            error: function (data) {
                                //
                                //  alert('An error occured, kindly try later');
                            }
                        });
                    }, 3000);
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/total_package_payments",
                            dataType: "JSON",
                            success: function (total_visits) {
                                total_visit = $('#total_package_payments').empty();
                                if (total_visit === null) {
                                    total_visit.append("<p>No Payments</p>");
                                } else {
                                    $.each(total_visits, function (i, total_visitss) {
                                        if (total_visitss.amount_owed === null) {
                                            total_visit.append("<p>No Payments</p>");
                                        } else {
                                            total_visit.append('<p>' + total_visitss.amount_owed + '</p>');
                                        }

                                    });
                                }



                            },
                            error: function (data) {
                                //
                                //  alert('An error occured, kindly try later');
                            }
                        });
                    }, 3000);
                    setInterval(function () {

                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/total_lab_service_payments",
                            dataType: "JSON",
                            success: function (total_visits) {

                                total_visit = $('#total_lab_service_payments').empty();
                                if (total_visits === null) {

                                    total_visit.append("<p>No Payments</p>");
                                } else {
                                    $.each(total_visits, function (i, total_visitss) {
                                        if (total_visitss.amount_owed === null) {

                                            total_visit.append("<p>No Payments</p>");
                                        } else {

                                            total_visit.append('<p>' + total_visitss.amount_owed + '</p>');
                                        }

                                    });
                                }



                            },
                            error: function (data) {
                                //
                                //  alert('An error occured, kindly try later');
                            }
                        });
                    }, 3000);
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/total_procedure_service_payments",
                            dataType: "JSON",
                            success: function (total_visits) {
                                total_visit = $('#total_procedure_service_payments').empty();
                                if (total_visit === null) {
                                    total_visit.append("<p>No Payments</p>");
                                } else {
                                    $.each(total_visits, function (i, total_visitss) {
                                        if (total_visitss.amount_owed === null) {
                                            total_visit.append("<p>No Payments</p>");
                                        } else {
                                            total_visit.append('<p>' + total_visitss.amount_owed + '</p>');
                                        }

                                    });
                                }



                            },
                            error: function (data) {
                                //
                                //  alert('An error occured, kindly try later');
                            }
                        });
                    }, 3000);
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/total_pharmacy_payments",
                            dataType: "JSON",
                            success: function (total_visits) {
                                total_visit = $('#total_pharmacy_payments').empty();
                                if (total_visit === null) {
                                    total_visit.append("<p>No Payments</p>");
                                } else {
                                    $.each(total_visits, function (i, total_visitss) {
                                        if (total_visitss.amount_owed === null) {
                                            total_visit.append("<p>No Payments</p>");
                                        } else {
                                            total_visit.append('<p>' + total_visitss.amount_owed + '</p>');
                                        }

                                    });
                                }



                            },
                            error: function (data) {
                                //
                                //  alert('An error occured, kindly try later');
                            }
                        });
                    }, 3000);
                    //cashier
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/total_walkin_payments",
                            dataType: "JSON",
                            success: function (total_visits) {
                                total_visit = $('#total_walkin_patient_payments').empty();
                                if (total_visits === null) {
                                    total_visit.append("<option>No Payments</option>");
                                } else {
                                    $.each(total_visits, function (i, total_visitss) {
                                        if (total_visitss.amount_owed === null) {
                                            total_visit.append("<p>No Payments</p>");
                                        } else {
                                            total_visit.append('<p>' + total_visitss.amount_owed + '</p>');
                                        }

                                    });
                                }



                            },
                            error: function (data) {
                                //
                                //  alert('An error occured, kindly try later');
                            }
                        });
                    }, 3000);
                    //cashier




                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/regular_patient_payments",
                            dataType: "JSON",
                            success: function (payment_list) {
                                pat_payment_list = $('#regular_patient_payment').empty();
                                $.each(payment_list, function (i, payment_list) {
                                    if (payment_list.charge_followup === 'Yes') {
                                        pat_payment_list.append('<li><a href="#regular_patient_form" id="regular_payment_link" class="regular_payment_link"><i class = "glyphicon glyphicon-user"> </i>\n\
                            ' + payment_list.patient_name + '</a>\n\
                  <input type="hidden" id="patient_id" name="patient_id_list" class="patient_id" value="' + payment_list.patient_id + '"/>\n\
            <input type="hidden" id="visit_id" class="visit_id" name="visit_id_list" value="' + payment_list.visit_id + '" />(Folllow Up)</br></li>');
                                    } else {
                                        pat_payment_list.append('<li><a href="#regular_patient_form" id="regular_payment_link" class="regular_payment_link"><i class = "glyphicon glyphicon-user"> </i>\n\
                            ' + payment_list.patient_name + '</a>\n\
                  <input type="hidden" id="patient_id" name="patient_id_list" class="patient_id" value="' + payment_list.patient_id + '"/>\n\
            <input type="hidden" id="visit_id" class="visit_id" name="visit_id_list" value="' + payment_list.visit_id + '" /></br></li>');
                                    }


                                });
                            },
                            error: function (data) {
                                //error do something
                                // alert(data);
                            }
                        });
                    }, 3000);
                    $('.regular_patient_payment').on('click', '.regular_payment_link', function () {
                        $(".div_regular_make_payment").hide("slow");
                        //get
                        var visit_id = $(this).closest('li').find('input[name="visit_id_list"]').val();
                        var patient_id = $(this).closest('li').find('input[name="patient_id_list"]').val();
                        html1 = '';
                        htmlhead1 = '';
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/regular_patient_payments_details/" + visit_id + "/" + patient_id,
                            dataType: "JSON",
                            success: function (data) {
                                for (i = 0; i < data.length; i++) {


                                    html1 += '<div>\n\
                        <div class = "form-group">\n\
            \n\
                        <input type = "text" required=""  class = "form-control" style="width:80px" readonly="" name="description[]" id = "description" value="' + data[i].description + '" placeholder = "Description">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control amount_owed' + data[i].patient_visit_statement_id + '" name="amount_owed[]" readonly="" style="width:80px" id = "amount_owed' + data[i].patient_visit_statement_id + '" value="' + data[i].amount_owed + '" placeholder = "Amount Owed">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control amount_paid' + data[i].patient_visit_statement_id + '" name="amount_paid[]"  style="width:80px" id = "amount_paid' + data[i].patient_visit_statement_id + '"  placeholder = "Amount Paid">\n\
            </div>\n\
             <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
            <input type="hidden" required="" readonly="" name="patient_visit_statement_id[]" id="patient_visit_statement_id" value="' + data[i].patient_visit_statement_id + '">\n\
            <input type="hidden" required="" readonly="" name="patient_id[]" id="patient_id" value="' + data[i].patient_id + '">\n\
            <input type="hidden" required="" readonly="" name="visit_id[]" id="visit_id" value="' + data[i].visit_id + '">\n\
            <input type="hidden" required="" readonly="" name="patient_payment_id[]" id="patient_payment_id" value="' + data[i].patient_payment_id + '">\n\
            <input type="hidden" required="" readonly="" name="amount_paid_td[]" id="amount_paid_td" value="' + data[i].amount_paid + '">\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            \n\
            <select required="" name="payment_method[]" class="form-control" id="payment_method">\n\
            <option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
            <option value="Cash">Cash</option>\n\
            <option value="M-Pesa">M-Pesa</option>\n\
            <option value="Airtel Money">Airtel Money</option>\n\
            <option value="Orange Money">Orange Money</option>\n\
            <option value="PDQ">PDQ </option></select>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <textarea type = "text" style="width:80px" required="" class = "form-control" name="payment_code[]" id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
            </div>\n\
                        </div>';
                                    $("#regular_patient_payment_div").on("keyup", ".amount_paid" + data[i].patient_visit_statement_id, function () {
                                        var amount_paid = this.value;
                                        var amount_paid = parseFloat(amount_paid);
                                        var amount_owed = $("#" + this.id.replace("amount_paid", "amount_owed")).val();
                                        var amount_owed = parseFloat(amount_owed);
                                        if (amount_paid > amount_owed) {
                                            alert('You cannot pay more than you owe');
                                            $(".div_regular_make_payment").hide("slow");
                                        } else {

                                            $(".div_regular_make_payment").show("slow");
                                        }
                                    });
                                }

                                htmlhead1 += '<div>\n\
                        <div class = "form-group">\n\
            <label class = "label label-info" for = "description">Description</label>\n\
            </div>\n\
            <div class = "form-group">\n\
             <label class = "label label-info" for = "amount_charged">Amount Charged</label>\n\
            </div>\n\
            \n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "balance_remaining">Balance Remaining</label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "amount_paid">Amount Paid</label>\n\
            </div>\n\
             <div class = "form-group">\n\
            <label class = "label label-info" for = "quantity">Quantity</label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_method">Payment Method </label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_code">Payment Code </label>\n\
            </div>\n\
                        </div>';
                                $('#regular_patient_payment_div').empty();
                                $('#regular_patient_payment_div').append(htmlhead1);
                                $('#regular_patient_payment_div').append(html1);
                            },
                            error: function (data) {

                            }
                        });
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/get_patient_name/" + visit_id + "/" + patient_id,
                            dataType: "JSON",
                            success: function (response) {
                                $('#regular_patient_name').val(response[0].patient_name);
                            }
                        });
                    });
                    $('#regular_patient_payment_form').submit(function (event) {
                        dataString = $("#regular_patient_payment_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/cashier/regular_patient_make_payment",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Payments saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    //Pharmacy Service Payments


                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/pharmacy_service_payments",
                            dataType: "JSON",
                            success: function (payment_list) {
                                pat_payment_list = $('#pharmacy_service_payments').empty();
                                $.each(payment_list, function (i, payment_list) {
                                    pat_payment_list.append('<li><a href="#pharmacy_service_payment_div" id="pharmacy_service_payment_link" class="pharmacy_service_payment_link"><i class = "glyphicon glyphicon-user"> </i>\n\
                            ' + payment_list.patient_name + '</a>\n\
                  <input type="hidden" id="patient_id" name="patient_id_list" class="patient_id" value="' + payment_list.patient_id + '"/>\n\
            <input type="hidden" id="visit_id" class="visit_id" name="visit_id_list" value="' + payment_list.visit_id + '" /></br></li>');
                                });
                            },
                            error: function (data) {
                                //error do something
                                // alert(data);
                            }
                        });
                    }, 3000);
                    $('.pharmacy_service_payments').on('click', '.pharmacy_service_payment_link', function () {

                        //get
                        $(".div_pharmacy_make_payment").hide("slow");
                        var visit_id = $(this).closest('li').find('input[name="visit_id_list"]').val();
                        //alert(visit_id);
                        var patient_id = $(this).closest('li').find('input[name="patient_id_list"]').val();
                        //alert(patient_id);
                        html1 = '';
                        htmlhead1 = '';
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/pharmacy_service_payments_details/" + visit_id + "/" + patient_id,
                            dataType: "JSON",
                            success: function (data) {
                                for (i = 0; i < data.length; i++) {

                                    // alert(data[i].stock_id);
                                    html1 += '<div>\n\
                        <div class = "form-group">\n\
            \n\
                        <textarea required=""  class = "form-control" style="width:100px" readonly="" name="description[]" id = "description"  >' + data[i].description + '</textarea>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control amount_owed' + data[i].patient_visit_statement_id + '" name="amount_owed[]" readonly="" style="width:80px" id = "amount_owed' + data[i].patient_visit_statement_id + '" value="' + data[i].amount_owed + '" placeholder = "Amount Paid">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control amount_paid' + data[i].patient_visit_statement_id + '" name="amount_paid[]"  style="width:80px" id = "amount_paid' + data[i].patient_visit_statement_id + '"  placeholder = "Amount Paid">\n\
            </div>\n\
             <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
            <input type="hidden" required="" readonly="" name="patient_visit_statement_id[]" id="patient_visit_statement_id" value="' + data[i].patient_visit_statement_id + '">\n\
            <input type="hidden" required="" readonly="" name="patient_id[]" id="patient_id" value="' + data[i].patient_id + '">\n\
            <input type="hidden" required="" readonly="" name="visit_id[]" id="visit_id" value="' + data[i].visit_id + '">\n\
            <input type="hidden" required="" readonly="" name="patient_payment_id[]" id="patient_payment_id" value="' + data[i].patient_payment_id + '">\n\
            <input type="hidden" required="" readonly="" name="payment_prescription_tracker[]" id="payment_prescription_tracker" value="' + data[i].prescription_tracker + '">\n\<input type="hidden" required="" readonly="" name="amount_paid_td[]" id="amount_paid_td" value="' + data[i].amount_paid + '">\n\
            \n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            \n\
            <select required="" name="payment_method[]" class="form-control" id="payment_method">\n\
            <option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
            <option value="Cash">Cash</option>\n\
            <option value="M-Pesa">M-Pesa</option>\n\
            <option value="Airtel Money">Airtel Money</option>\n\
            <option value="Orange Money">Orange Money</option>\n\
            <option value="PDQ">PDQ </option></select>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <textarea type = "text" style="width:80px" required="" class = "form-control" name="payment_code[]" id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
            </div>\n\
                        </div>';
                                    $("#patient_pharmacy_service_payment_div").on("keyup", ".amount_paid" + data[i].patient_visit_statement_id, function () {
                                        var amount_paid = this.value;
                                        var amount_paid = parseFloat(amount_paid);
                                        var amount_owed = $("#" + this.id.replace("amount_paid", "amount_owed")).val();
                                        var amount_owed = parseFloat(amount_owed);
                                        if (amount_paid > amount_owed) {
                                            alert('You cannot pay more than you owe');
                                            $(".div_pharmacy_make_payment").hide("slow");
                                        } else {

                                            $(".div_pharmacy_make_payment").show("slow");
                                        }
                                    });
                                }

                                htmlhead1 += '<div>\n\
                        <div class = "form-group">\n\
            <label class = "label label-info" for = "description">Description</label>\n\
            </div>\n\
            <div class = "form-group">\n\
             <label class = "label label-info" for = "amount_charged">Amount Charged</label>\n\
            </div>\n\
            \n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "balance_remaining">Balance Remaining</label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "amount_paid">Amount Paid</label>\n\
            </div>\n\
             <div class = "form-group">\n\
            <label class = "label label-info" for = "quantity">Quantity</label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_method">Payment Method </label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_code">Payment Code </label>\n\
            </div>\n\
                        </div>';
                                $('#patient_pharmacy_service_payment_div').empty();
                                $('#patient_pharmacy_service_payment_div').append(htmlhead1);
                                $('#patient_pharmacy_service_payment_div').append(html1);
                            },
                            error: function (data) {

                            }
                        });
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/get_patient_name/" + visit_id + "/" + patient_id,
                            dataType: "JSON",
                            success: function (response) {
                                $('#pharmacy_service_patient_name').val(response[0].patient_name);
                            }
                        });
                    });
                    $('#pharmacy_service_payment_form').submit(function (event) {
                        dataString = $("#pharmacy_service_payment_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/cashier/pharmacy_service_make_payment",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Payments saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    //Lab Service Payments


                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/lab_service_payments",
                            dataType: "JSON",
                            success: function (payment_list) {
                                pat_payment_list = $('#lab_service_payments').empty();
                                $.each(payment_list, function (i, payment_list) {
                                    pat_payment_list.append('<li><a href="#lab_service_payment_div" id="lab_service_payment_link" class="lab_service_payment_link"><i class = "glyphicon glyphicon-user"> </i>\n\
                            ' + payment_list.patient_name + '</a>\n\
                  <input type="hidden" id="patient_id" name="patient_id_list" class="patient_id" value="' + payment_list.patient_id + '"/>\n\
            <input type="hidden" id="visit_id" class="visit_id" name="visit_id_list" value="' + payment_list.visit_id + '" /></br></li>');
                                });
                            },
                            error: function (data) {
                                //error do something
                                // alert(data);
                            }
                        });
                    }, 3000);
                    $('.lab_service_payments').on('click', '.lab_service_payment_link', function () {
                        $(".div_lab_make_payment").hide("slow");
                        //get
                        var visit_id = $(this).closest('li').find('input[name="visit_id_list"]').val();
                        //alert(visit_id);
                        var patient_id = $(this).closest('li').find('input[name="patient_id_list"]').val();
                        //alert(patient_id);
                        html1 = '';
                        htmlhead1 = '';
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/lab_service_payments_details/" + visit_id + "/" + patient_id,
                            dataType: "JSON",
                            success: function (data) {
                                for (i = 0; i < data.length; i++) {

                                    // alert(data[i].stock_id);
                                    html1 += '<div>\n\
                        <div class = "form-group">\n\
            \n\
                        <input type = "text" required=""  class = "form-control" style="width:80px" readonly="" name="description[]" id = "description" value="' + data[i].description + '" placeholder = "Description">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control amount_owed' + data[i].patient_visit_statement_id + '" name="amount_owed[]" readonly="" style="width:80px" id = "amount_paid' + data[i].patient_visit_statement_id + '" value="' + data[i].amount_owed + '" placeholder = "Amount Paid">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control amount_paid' + data[i].patient_visit_statement_id + '" name="amount_paid[]"  style="width:80px" id = "amount_paid' + data[i].patient_visit_statement_id + '"  placeholder = "Amount Paid">\n\
            </div>\n\
             <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
            <input type="hidden" required="" readonly="" name="patient_visit_statement_id[]" id="patient_visit_statement_id" value="' + data[i].patient_visit_statement_id + '">\n\
            <input type="hidden" required="" readonly="" name="patient_id[]" id="patient_id" value="' + data[i].patient_id + '">\n\
            <input type="hidden" required="" readonly="" name="visit_id[]" id="visit_id" value="' + data[i].visit_id + '">\n\
            <input type="hidden" required="" readonly="" name="patient_payment_id[]" id="patient_payment_id" value="' + data[i].patient_payment_id + '">\n\
            <input type="hidden" required="" readonly="" name="amount_paid_td[]" id="amount_paid_td" value="' + data[i].amount_paid + '">\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            \n\
            <select required="" name="payment_method[]" class="form-control" id="payment_method">\n\
            <option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
            <option value="Cash">Cash</option>\n\
            <option value="M-Pesa">M-Pesa</option>\n\
            <option value="Airtel Money">Airtel Money</option>\n\
            <option value="Orange Money">Orange Money</option>\n\
            <option value="PDQ">PDQ </option></select>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <textarea type = "text" style="width:80px" required="" class = "form-control" name="payment_code[]" id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
            </div>\n\
                        </div>';
                                    $("#patient_lab_service_payment_div").on("keyup", ".amount_paid" + data[i].patient_visit_statement_id, function () {
                                        var amount_paid = this.value;
                                        var amount_paid = parseFloat(amount_paid);
                                        var amount_owed = $("#" + this.id.replace("amount_paid", "amount_owed")).val();
                                        var amount_owed = parseFloat(amount_owed);
                                        if (amount_paid > amount_owed) {
                                            alert('You cannot pay more than you owe');
                                            $(".div_lab_make_payment").hide("slow");
                                        } else {

                                            $(".div_lab_make_payment").show("slow");
                                        }
                                    });
                                }

                                htmlhead1 += '<div>\n\
                        <div class = "form-group">\n\
            <label class = "label label-info" for = "description">Description</label>\n\
            </div>\n\
            <div class = "form-group">\n\
             <label class = "label label-info" for = "amount_charged">Amount Charged</label>\n\
            </div>\n\
            \n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "balance_remaining">Balance Remaining</label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "amount_paid">Amount Paid</label>\n\
            </div>\n\
             <div class = "form-group">\n\
            <label class = "label label-info" for = "quantity">Quantity</label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_method">Payment Method </label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_code">Payment Code </label>\n\
            </div>\n\
                        </div>';
                                $('#patient_lab_service_payment_div').empty();
                                $('#patient_lab_service_payment_div').append(htmlhead1);
                                $('#patient_lab_service_payment_div').append(html1);
                            },
                            error: function (data) {

                            }
                        });
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/get_patient_name/" + visit_id + "/" + patient_id,
                            dataType: "JSON",
                            success: function (response) {
                                $('#lab_service_patient_name').val(response[0].patient_name);
                            }
                        });
                    });
                    $('#lab_service_payment_form').submit(function (event) {
                        dataString = $("#lab_service_payment_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/cashier/lab_service_make_payment",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Payments saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/pay_at_the_end",
                            dataType: "JSON",
                            success: function (payment_list) {
                                pat_payment_list = $('#pay_at_the_end_payment_list').empty();
                                if (payment_list === null) {

                                } else {
                                    $.each(payment_list, function (i, payment_list) {


                                        pat_payment_list.append('<li><a href="#pay_at_the_end_patient_form" id="pay_at_the_end_patient_payment_link" class="pay_at_the_end_patient_payment_link"><i class = "glyphicon glyphicon-user"> </i>' + payment_list.patient_name + '</a>\n\
                  <input type="hidden" id="patient_id" name="patient_id" class="patient_id" value="' + payment_list.patient_id + '"/>\n\
            <input type="hidden" id="visit_id" class="visit_id" name="visit_id" value="' + payment_list.visit_id + '" /></br></li>');
                                    });
                                }


                            },
                            error: function (data) {
                                //error do something
                            }
                        });
                    }, 3000);
                    $('.pay_at_the_end_payment_list').on('click', '.pay_at_the_end_patient_payment_link', function () {
                        // alert('Alert!!');
                        //get
                        var visit_id = $(this).closest('li').find('input[name="visit_id"]').val();
                        // alert(visit_id);
                        var patient_id = $(this).closest('li').find('input[name="patient_id"]').val();
                        // alert(patient_id);
                        html1 = '';
                        htmlhead1 = '';
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/pay_at_the_end_details/" + visit_id + "/" + patient_id,
                            dataType: "JSON",
                            success: function (data) {
                                for (i = 0; i < data.length; i++) {

                                    // alert(data[i].stock_id);
                                    html1 += '<div>\n\
                        <div class = "form-group">\n\
            \n\
                        <input type = "text" required=""  class = "form-control" style="width:80px" readonly="" name="description[]" id = "description" value="' + data[i].description + '" placeholder = "Description">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control" name="amount_owed[]" readonly="" style="width:80px" id = "amount_owed" value="' + data[i].amount_owed + '" placeholder = "Amount Paid">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control" name="amount_paid[]"  style="width:80px" id = "amount_paid"  placeholder = "Amount Paid">\n\
            </div>\n\
             <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
            <input type="hidden" required="" readonly="" name="patient_visit_statement_id[]" id="patient_visit_statement_id" value="' + data[i].patient_visit_statement_id + '">\n\
            <input type="hidden" required="" readonly="" name="patient_id[]" id="patient_id" value="' + data[i].patient_id + '">\n\
            <input type="hidden" required="" readonly="" name="visit_id[]" id="visit_id" value="' + data[i].visit_id + '">\n\
            <input type="hidden" required="" readonly="" name="patient_payment_id[]" id="patient_payment_id" value="' + data[i].patient_payment_id + '">\n\
            <input type="hidden" required="" readonly="" name="amount_paid_td[]" id="amount_paid_td" value="' + data[i].amount_paid + '">\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            \n\
            <select required="" name="payment_method[]" class="form-control" id="payment_method">\n\
            <option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
            <option value="Cash">Cash</option>\n\
            <option value="M-Pesa">M-Pesa</option>\n\
            <option value="Airtel Money">Airtel Money</option>\n\
            <option value="Orange Money">Orange Money</option>\n\
            <option value="PDQ">PDQ </option></select>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <textarea type = "text" style="width:80px" required="" class = "form-control" name="payment_code[]" id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
            </div>\n\
                        </div>';
                                }

                                htmlhead1 += '<div>\n\
                        <div class = "form-group">\n\
            <label class = "label label-info" for = "description">Description</label>\n\
            </div>\n\
            <div class = "form-group">\n\
             <label class = "label label-info" for = "amount_charged">Amount Charged</label>\n\
            </div>\n\
            \n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "balance_remaining">Balance Remaining</label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "amount_paid">Amount Paid</label>\n\
            </div>\n\
             <div class = "form-group">\n\
            <label class = "label label-info" for = "quantity">Quantity</label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_method">Payment Method </label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_code">Payment Code </label>\n\
            </div>\n\
                        </div>';
                                $('#pay_at_the_end_patient_payment_div').empty();
                                $('#pay_at_the_end_patient_payment_div').append(htmlhead1);
                                $('#pay_at_the_end_patient_payment_div').append(html1);
                            },
                            error: function (data) {

                            }
                        });
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/get_patient_name/" + visit_id + "/" + patient_id,
                            dataType: "JSON",
                            success: function (response) {
                                $('#pay_at_the_end_patient_name').val(response[0].patient_name);
                            }
                        });
                    });
                    $('#pay_at_the_end_patient_payment_form').submit(function (event) {
                        dataString = $("#pay_at_the_end_patient_payment_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/cashier/pay_at_the_end_make_payment",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Payments saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/walkin_patient_nurse_payments",
                            dataType: "JSON",
                            success: function (payment_list) {
                                pat_payment_list = $('#walkin_nurse_patient_payment').empty();
                                if (payment_list === null) {

                                } else {
                                    $.each(payment_list, function (i, payment_list) {


                                        pat_payment_list.append('<li><a href="#walkin_patient_nurse_form" id="walkin_nurse_payment_link" class="walkin_nurse_payment_link"><i class = "glyphicon glyphicon-user"> </i>' + payment_list.patient_name + '</a>\n\
                  <input type="hidden" id="walkin_id_list" name="walkin_id_list" class="walkin_id_list" value="' + payment_list.visit_id + '"/>\n\
            </br></li>');
                                    });
                                }


                            },
                            error: function (data) {
                                //error do something
                            }
                        });
                    }, 3000);
                    $('.walkin_nurse_patient_payment').on('click', '.walkin_nurse_payment_link', function () {

                        //get

                        var walkin_id = $(this).closest('li').find('input[name="walkin_id_list"]').val();

                        html1 = '';
                        htmlhead1 = '';
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/walkin_patient_nurse_payments_details/" + walkin_id + "/",
                            dataType: "JSON",
                            success: function (data) {
                                for (i = 0; i < data.length; i++) {


                                    html1 += '<div>\n\
                        <div class = "form-group">\n\
            \n\
                  <input type="hidden" class="form-control" name="walkin_id[]" id="walkin_id" class="walkin_id" value="' + data[i].walkin_id + '"> \n\
            <input type="hidden" class="form-control" name="walkin_patient_visit_statement_id[]" id="walkin_patient_visit_statement_id" class="walkin_patient_visit_statement_id" value="' + data[i].walkin_patient_visit_statement_id + '">  \n\
                <textarea type = "text" class = "form-control" readonly="" name="description[]" id = "description"  placeholder = "Description">' + data[i].description + '</textarea>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" class = "form-control" readonly="" style="width:80px" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" class = "form-control"  name="amount_paid[]" style="width:80px" id = "amount_paid" value="' + data[i].amount_paid + '" placeholder = "Amount Paid">\n\
            </div>\n\
             <div class = "form-group">\n\
            \n\
            <input type = "text" class = "form-control" readonly="" style="width:80px" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            \n\
            <select required="" name="payment_method[]" class="form-control" id="payment_method">\n\
            <option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
            <option value="Cash">Cash</option>\n\
            <option value="M-Pesa">M-Pesa</option>\n\
            <option value="Airtel Money">Airtel Money</option>\n\
            <option value="Orange Money">Orange Money</option>\n\
            <option value="PDQ">PDQ </option></select>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <textarea type = "text" style="width:80px" class = "form-control" name="payment_code[] id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
            </div>\n\
                        </div>';
                                }

                                htmlhead1 += '<div>\n\
                        <div class = "form-group">\n\
            <label class = "label label-info" for = "description">Description</label>\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
             <label class = "label label-info" for = "amount_charged">Amount Charged</label>\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "amount_paid">Amount Paid</label>\n\
            \n\
            </div>\n\
             <div class = "form-group">\n\
            <label class = "label label-info" for = "quantity">Quantity</label>\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_method">Payment Method </label>\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_code">Payment Code </label>\n\
            \n\
            </div>\n\
                        </div>';
                                $('#walkin_patient_nurse_payment_div').empty();
                                $('#walkin_patient_nurse_payment_div').append(htmlhead1);
                                $('#walkin_patient_nurse_payment_div').append(html1);
                            },
                            error: function (data) {

                            }
                        });
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/get_walkin_patient_name/" + walkin_id + "/",
                            dataType: "JSON",
                            success: function (response) {
                                $('#walkin_nurse_patient_name').val(response[0].patient_name);
                            }
                        });
                    });
                    $('#walkin_patient_nurse_payment_form').submit(function (event) {
                        dataString = $("#walkin_patient_nurse_payment_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/cashier/walkin_patient_nurse_make_payment",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Payments saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/walkin_patient_lab_payments",
                            dataType: "JSON",
                            success: function (payment_list) {
                                pat_payment_list = $('#walkin_lab_patient_payment').empty();
                                if (payment_list === null) {

                                } else {
                                    $.each(payment_list, function (i, payment_list) {


                                        pat_payment_list.append('<li><a href="#walkin_patient_lab_form" id="walkin_lab_payment_link" class="walkin_lab_payment_link"><i class = "glyphicon glyphicon-user"> </i>' + payment_list.patient_name + '</a>\n\
                  <input type="hidden" id="walkin_id_list" name="walkin_id_list" class="walkin_id_list" value="' + payment_list.visit_id + '"/>\n\
            </br></li>');
                                    });
                                }


                            },
                            error: function (data) {
                                //error do something
                            }
                        });
                    }, 3000);
                    $('.walkin_lab_patient_payment').on('click', '.walkin_lab_payment_link', function () {

                        //get


                        var walkin_id = $(this).closest('li').find('input[name="walkin_id_list"]').val();

                        html1 = '';
                        htmlhead1 = '';
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/walkin_patient_lab_payments_details/" + walkin_id + "/",
                            dataType: "JSON",
                            success: function (data) {
                                for (i = 0; i < data.length; i++) {


                                    html1 += '<div>\n\
                        <div class = "form-group">\n\
            \n\
                  <input type="hidden" class="form-control" name="lab_test_id[]" id="lab_test_id" class="lab_test_id" value="' + data[i].lab_test_id + '"><input type="hidden" class="form-control" name="walkin_id[]" id="walkin_id" class="walkin_id" value="' + data[i].walkin_id + '"> \n\
            <input type="hidden" class="form-control" name="walkin_patient_visit_statement_id[]" id="walkin_patient_visit_statement_id" class="walkin_patient_visit_statement_id" value="' + data[i].walkin_patient_visit_statement_id + '">  \n\
                <textarea type = "text" class = "form-control" readonly="" name="description[]" id = "description"  placeholder = "Description">' + data[i].description + '</textarea>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" class = "form-control" readonly="" style="width:80px" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" class = "form-control"  name="amount_paid[]" style="width:80px" id = "amount_paid" value="' + data[i].amount_paid + '" placeholder = "Amount Paid">\n\
            </div>\n\
            \n\<input type="text" class="form-control" name="balance_remaining[]" style="width:80px;" id="balance_remaining" value="' + data[i].balance_remaining + '" placeholder="Balance Reamining" readonly/>\n\
             <div class = "form-group">\n\
            <input type = "text" class = "form-control" readonly="" style="width:80px" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            \n\
            <select required="" name="payment_method[]" class="form-control" id="payment_method">\n\
            <option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
            <option value="Cash">Cash</option>\n\
            <option value="M-Pesa">M-Pesa</option>\n\
            <option value="Airtel Money">Airtel Money</option>\n\
            <option value="Orange Money">Orange Money</option>\n\
            <option value="PDQ">PDQ </option></select>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <textarea type = "text" style="width:80px" class = "form-control" name="payment_code[] id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
            </div>\n\
                        </div>';
                                }

                                htmlhead1 += '<div>\n\
                        <div class = "form-group">\n\
            <label class = "label label-info" for = "description">Description</label>\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
             <label class = "label label-info" for = "amount_charged">Amount Charged</label>\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "amount_paid">Amount Paid</label>\n\ </div>\n\
            \n\<div class= "form-group" >\n\
            <label class="label label-info" for="balance_remaining">Balance Remaining </label></div>\n\
             <div class = "form-group">\n\
            <label class = "label label-info" for = "quantity">Quantity</label>\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_method">Payment Method </label>\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_code">Payment Code </label>\n\
            \n\
            </div>\n\
                        </div>';
                                $('#walkin_patient_lab_payment_div').empty();
                                $('#walkin_patient_lab_payment_div').append(htmlhead1);
                                $('#walkin_patient_lab_payment_div').append(html1);
                            },
                            error: function (data) {

                            }
                        });
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/get_walkin_patient_name/" + walkin_id + "/",
                            dataType: "JSON",
                            success: function (response) {
                                $('#walkin_lab_patient_name').val(response[0].patient_name);
                            }
                        });
                    });
                    $('#walkin_patient_lab_payment_form').submit(function (event) {
                        dataString = $("#walkin_patient_lab_payment_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/cashier/walkin_patient_lab_make_payment",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Payments saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/walkin_patient_pharmacy_payments",
                            dataType: "JSON",
                            success: function (payment_list) {
                                pat_payment_list = $('#walkin_pharmacy_patient_payment').empty();
                                if (payment_list === null) {

                                } else {
                                    $.each(payment_list, function (i, payment_list) {


                                        pat_payment_list.append('<li><a href="#walkin_patient_pharmacy_form" id="walkin_pharmacy_payment_link" class="walkin_pharmacy_payment_link"><i class = "glyphicon glyphicon-user"> </i>' + payment_list.patient_name + '</a>\n\
                  <input type="hidden" id="walkin_id_list" name="walkin_id_list" class="walkin_id_list" value="' + payment_list.visit_id + '"/>\n\
            </br></li>');
                                    });
                                }


                            },
                            error: function (data) {
                                //error do something
                            }
                        });
                    }, 3000);
                    $('.walkin_pharmacy_patient_payment').on('click', '.walkin_pharmacy_payment_link', function () {

                        //get


                        var walkin_id = $(this).closest('li').find('input[name="walkin_id_list"]').val();

                        html1 = '';
                        htmlhead1 = '';
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/walkin_patient_pharmacy_payments_details/" + walkin_id + "/",
                            dataType: "JSON",
                            success: function (data) {
                                for (i = 0; i < data.length; i++) {


                                    html1 += '<div>\n\
                        <div class = "form-group">\n\
            \n\
                  <input type="hidden" class="form-control" name="walkin_id[]" id="walkin_id" class="walkin_id" value="' + data[i].walkin_id + '"> \n\
            <input type="hidden" class="form-control" name="walkin_statement_id[]" id="walkin_statement_id" class="walkin_statement_id" value="' + data[i].walkin_patient_visit_statement_id + '">  \n\
                <textarea type = "text" class = "form-control" readonly="" name="description[]" id = "description"  placeholder = "Description">' + data[i].description + '</textarea>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" class = "form-control" readonly="" style="width:80px" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" class = "form-control"  name="amount_paid[]" style="width:80px" id = "amount_paid" value="' + data[i].amount_paid + '" placeholder = "Amount Paid">\n\
            </div>\n\
            \n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" class = "form-control" readonly="" name="amount_owed[]" style="width:80px" id = "amount_owed" value="' + data[i].amount_owed + '" placeholder = "Balance Remaining">\n\
            </div>\n\
            \n\
             <div class = "form-group">\n\
            \n\
            <input type = "text" class = "form-control" readonly="" style="width:80px" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <select required="" name="payment_method[]" class="form-control" id="payment_method">\n\
            <option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
            <option value="Cash">Cash</option>\n\
            <option value="M-Pesa">M-Pesa</option>\n\
            <option value="Airtel Money">Airtel Money</option>\n\
            <option value="Orange Money">Orange Money</option>\n\
            <option value="PDQ">PDQ </option></select>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <textarea type = "text" style="width:80px" class = "form-control" name="payment_code[] id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
            </div>\n\
                        </div>';
                                }

                                htmlhead1 += '<div>\n\
                        <div class = "form-group">\n\
            <label class = "label label-info" style="-webkit-column-gap: 50px; /* Chrome, Safari, Opera */-moz-column-gap: 50px; /* Firefox */column-gap: 50px;" for = "description">Description</label>\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
             <label class = "label label-info" style="-webkit-column-gap: 50px; /* Chrome, Safari, Opera */-moz-column-gap: 50px; /* Firefox */column-gap: 50px;" for = "amount_charged">Amount Charged</label>\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" style="-webkit-column-gap: 50px; /* Chrome, Safari, Opera */-moz-column-gap: 50px; /* Firefox */column-gap: 50px;"  for = "amount_paid">Amount Paid</label>\n\
            \n\
            </div>\n\
             <div class = "form-group">\n\
            <label class = "label label-info" style="-webkit-column-gap: 50px; /* Chrome, Safari, Opera */-moz-column-gap: 50px; /* Firefox */column-gap: 50px;" for = "quantity">Quantity</label>\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" style="-webkit-column-gap: 50px; /* Chrome, Safari, Opera */-moz-column-gap: 50px; /* Firefox */column-gap: 50px;" for = "payment_method">Payment Method </label>\n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" style="-webkit-column-gap: 50px; /* Chrome, Safari, Opera */-moz-column-gap: 50px; /* Firefox */column-gap: 50px;" for = "payment_code">Payment Code </label>\n\
            \n\
            </div>\n\
                        </div>';
                                $('#walkin_patient_pharmacy_payment_div').empty();
                                $('#walkin_patient_pharmacy_payment_div').append(htmlhead1);
                                $('#walkin_patient_pharmacy_payment_div').append(html1);
                            },
                            error: function (data) {

                            }
                        });
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/get_walkin_patient_name/" + walkin_id + "/",
                            dataType: "JSON",
                            success: function (response) {
                                $('#walkin_pharmacy_patient_name').val(response[0].patient_name);
                            }
                        });
                    });
                    $('#walkin_patient_pharmacy_payment_form').submit(function (event) {
                        dataString = $("#walkin_patient_pharmacy_payment_form").serialize();
                        $.fancybox.open([
                            {
                                href: '#ajax_loader',
                                title: 'Please wait information being updated...'
                            }
                        ], {
                            padding: 0
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() ?>index.php/cashier/walkin_patient_pharmacy_make_payment",
                            data: dataString,
                            success: function (data) {
                                $(".loader_notify").notify(
                                        "Patient Payments saved Successfully",
                                        "success",
                                        {position: "center"}
                                );
                                setTimeout(function () {
                                    parent.$.fancybox.close([
                                        {
                                            href: '#ajax_loader',
                                            title: 'Please wait information being updated...'
                                        }
                                    ]);
                                }, 3000);
                            }

                        });
                        event.preventDefault();
                        return false;
                    });
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/procedure_payments",
                            dataType: "JSON",
                            success: function (payment_list) {
                                pat_payment_list = $('#procedure_patient_payments').empty();
                                if (payment_list === null) {

                                } else {
                                    $.each(payment_list, function (i, payment_list) {

                                        if (payment_list.urgency === "urgent") {
                                            pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.urgency + '</span><span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');
                                        }
                                        else if (payment_list.urgency !== "urgent")
                                        {
                                            pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');
                                        }
                                        else {

                                            pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                        }

                                    });
                                }


                            },
                            error: function (data) {
                                //error do something
                            }
                        });
                    }, 3000);
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/lab_payments",
                            dataType: "JSON",
                            success: function (payment_list) {
                                pat_payment_list = $('#lab_patient_payments').empty();
                                if (payment_list === null) {

                                } else {
                                    $.each(payment_list, function (i, payment_list) {

                                        if (payment_list.urgency === "urgent") {
                                            pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.urgency + '</span><span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');
                                        }
                                        else if (payment_list.urgency !== "urgent")
                                        {
                                            pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + payment_list.f_name + " " + payment_list.s_name + " " + payment_list.other_name + '</a><span style="color:red !important;">' + payment_list.total_cost + '</span></li></br>');
                                        }
                                        else {

                                            pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                        }

                                    });
                                }


                            },
                            error: function (data) {
                                //error do something
                            }
                        });
                    }, 3000);
                    setInterval(function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>cashier/pharmacy_payments",
                            dataType: "JSON",
                            success: function (pharm_payment_list) {
                                pat_payment_list = $('#pharmacy_patient_payments').empty();
                                if (pharm_payment_list === null) {

                                } else {
                                    $.each(pharm_payment_list, function (i, pharm_payment_list) {

                                        if (pharm_payment_list.urgency === "urgent") {
                                            pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + pharm_payment_list.f_name + " " + pharm_payment_list.s_name + " " + pharm_payment_list.other_name + '</a><span style="color:red !important;">' + pharm_payment_list.urgency + '</span><span style="color:red !important;">' + pharm_payment_list.total_cost + '</span></li></br>');
                                        }
                                        else if (pharm_payment_list.urgency !== "urgent")
                                        {
                                            pat_payment_list.append('<li><a><i class = "glyphicon glyphicon-user"></i>' + pharm_payment_list.f_name + " " + pharm_payment_list.s_name + " " + pharm_payment_list.other_name + '</a><span style="color:red !important;">' + pharm_payment_list.total_cost + '</span></li></br>');
                                        }
                                        else {

                                            pat_payment_list.append("<li><a><i class='glyphicon glyphicon-user'></i>No Active patient</a> </li>");
                                        }

                                    });
                                }


                            },
                            error: function (data) {
                                //error do something
                            }
                        });
                    }, 3000);
                }
            });</script>
        <!-- Cashier Profile End  -->







        <link href='<?php echo base_url(); ?>assets/css/jquery-ui.css' rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>-->


        <?php
        $function_name = $this->uri->segment(2);

        $url_array = array("visit");
        if (in_array($function_name, $url_array)) {
            ?>
            <script type="text/javascript">

                $(document).ready(function () {


                    $(".appointment_calendar_form").hide("slow");
                    $(".show_calendar").click(function () {
                        $(".appointment_calendar_form").show("slow");
                        $(".book_visit_form").hide("slow");
                        $(".show_calendar").hide("slow");
                        $(".show_book_visit").show("slow");
                    });
                    $(".show_book_visit").click(function () {
                        $(".book_visit_form").show("slow");
                        $(".appointment_calendar_form").hide("slow");
                        $(".show_book_visit").hide("slow");
                        $(".show_calendar").show("slow");
                    });
                });</script>

            <?php
        }
        ?>



        <script type="text/javascript">
            $(function () {

                $(".add_expiry_date").datepicker({
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $(".edit_stock_expiry_date").datepicker({
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $(".stock_date_from").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $(".stock_date_to").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $("#sick_off_date_from").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $("#sick_off_date_to").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $("#previous_visit_date").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $("#datepicker").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $("#datepcker").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#date_to").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#date_from").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#visitation_date_to").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#visitation_date_from").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#walkin_date_to").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#walkin_date_from").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#LMP").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
                $("#LMP").datepicker({minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true});
            });</script>




        <script type="text/javascript">
            $(function () {
                $("#datepicker_1").datepicker({
                    minDate: '-100Y',
                    maxDate: '+0m +0w',
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    changeYear: true
                });
                $("#datepcker_1").datepicker({dateFormat: 'yy-mm-dd'});
            });
            $(document).ready(function () {
                $('#example').dataTable({
                    "scrollY": 200,
                    "scrollX": true
                });
            });</script>



        <?php
        $segment_1 = $this->uri->segment(1);

        $profile_array = array("reports", "lab");
        if (in_array($segment_1, $profile_array)) {
            ?>



            <link href='<?php echo base_url(); ?>assets/datatables-fixedcolumn/jquery.dataTables.css' rel='stylesheet'>
            <link href='<?php echo base_url(); ?>assets/datatables-fixedcolumn/dataTables.fixedColumns.css' rel='stylesheet'>
            <script type="text/css">
                /* Ensure that the demo table scrolls */
                th, td { white-space: nowrap; }
                div.dataTables_wrapper {
                    width: 800px;
                    margin: 0 auto;
                }
            </script>

            <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
            <script type="text/javascript" src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>

            <link href='http://cdn.datatables.net/1.10.5/css/jquery.dataTables.css' rel='stylesheet'>



            <script type="text/javascript">
                var j = jQuery.noConflict();
                j(document).ready(function () {
                    j('.patient_test_table').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.patients_report').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('#bill_patient_prescription_table').dataTable({
                        "scrollY": "400px",
                        "scrollCollapse": true,
                        "paging": false,
                        "pageLength": 20
                    });
                    j('.procedure_reportss').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.visitation_report').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.walkin_report').dataTable({
                        "scrollY": "400px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.commodity_report').dataTable({
                        "scrollY": "400px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.triage_report_1').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.triage_report').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.procedure_report').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                    j('.appointment_history_table').dataTable({
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "paging": false
                    });
                });
                j(document).ready(function () {

                    j('.bill_patient_prescription_link').click(function () {


                        //get data
                        var prescription_id = j(this).closest('tr').find('input[name="prescription_id"]').val();
                        var prescription_tracker = j(this).closest('tr').find('input[name="hidden_prescription_tracker"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>pharmacy/get_presciprion_details/" + prescription_id,
                            dataType: "json",
                            success: function (response) {

                                j('#patient_lab_test_idsend_to_doctor_form').val(response[0].prescription_id);
                                j('#presc_prescription_tracker').val(response[0].prescription_tracker);
                                j('#presc_route_name').val(response[0].route);
                                j('#presc_frequency').val(response[0].frequency);
                                j('#presc_prescription_tracker_2').val(response[0].prescription_tracker);
                                j('#presc_strength').val(response[0].strength);
                                j('#presc_commodity_name').val(response[0].commodity_name);
                                j('#presc_is_dispensed').val(response[0].is_dispensed);
                                j('#presc_quantity_issued').val(response[0].quantity_issued);
                                j('#test_results_lab_test_id').val(response[0].consultation_id);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.dipsense_commodity_link').click(function () {


                        //get data
                        var dispensing_transaction_id = j(this).closest('tr').find('input[name="dispensing_transaction_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>pharmacy/get_available_commodities/" + dispensing_transaction_id,
                            dataType: "json",
                            success: function (response) {

                                j('#dispensing_form_patient_visit_statement_id').val(response[0].patient_visit_statement_id);
                                j('#dispensing_form_prescription_tracker').val(response[0].prescription_tracker);
                                j('#dispensing_form_prescription_id').val(response[0].prescription_id);
                                j('#dispensing_form_patient_id').val(response[0].patient_id);
                                j('#dispensing_form_visit_id').val(response[0].visit_id);
                                j('#dispensing_form_transaction_id').val(response[0].transaction_id);
                                j('#dispensing_form_commodity_name').val(response[0].commodity_name);
                                j('#dispensinf_form_stock_id').val(response[0].stock_id);
                                j('#dispensing_form_available_quantity').val(response[0].available_quantity);
                                j('#dispensing_form_batch_no').val(response[0].batch_no);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.add_test_results_link').click(function () {


                        //get data
                        var lab_test_id = j(this).closest('tr').find('input[name="lab_test_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>lab/get_lab_test_results/" + lab_test_id,
                            dataType: "json",
                            success: function (response) {

                                j('#patient_lab_test_id').val(response[0].lab_test_id);
                                j('#patient_test_name').val(response[0].test_name);
                                j('#test_results_text_area').val(response[0].test_results);
                                j('#test_results_visit_id').val(response[0].visit_id);
                                j('#test_results_lab_test_id').val(response[0].lab_test_id);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.edit_triage_patient_link').click(function () {


                        //get data
                        var triage_id = j(this).closest('tr').find('input[name="view_triage_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>nurse/get_triage_details/" + triage_id,
                            dataType: "json",
                            success: function (response) {
                                j('#edit_weight').val(response[0].weight);
                                j('#edit_systolic').val(response[0].systolic);
                                j('#edit_diastolic').val(response[0].diastolic);
                                j('#edit_temperature').val(response[0].temperature);
                                j('#edit_height').val(response[0].height);
                                j('#edit_respiratory').val(response[0].respiratory_rate);
                                j('#edit_blood_sugar').val(response[0].blood_sugar);
                                j('#edit_pulse_rate').val(response[0].pulse_rate);
                                j('#edit_LMP').val(response[0].lmp);
                                j('#edit_general_complaints').val(response[0].OCS);
                                j('#edit_allergy').val(response[0].allergy);
                                j('#edit_urgency').val(response[0].urgency);
                                j('#edit_triage_id').val(response[0].triage_id);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.edit_patient_link').click(function () {


                        //get data
                        var patient_id = j(this).closest('tr').find('input[name="view_patient_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_patient_reports_details/" + patient_id,
                            dataType: "json",
                            success: function (response) {
                                j('#edit_patient_id').val(response[0].patient_id);
                                j('#edit_title').val(response[0].title);
                                j('#edit_phone_no').val(response[0].phone_no);
                                j('#edit_inputFirstName').val(response[0].f_name);
                                j('#edit_inputSurName').val(response[0].s_name);
                                j('#edit_inputOtherName').val(response[0].other_name);
                                j('#edit_dob').val(response[0].dob);
                                j('#edit_gender').val(response[0].gender);
                                j('#edit_maritalstatus').val(response[0].marital_status);
                                j('#edit_nationalid').val(response[0].identification_number);
                                j('#edit_email').val(response[0].email);
                                j('#edit_address').val(response[0].address);
                                j('#edit_residence').val(response[0].residence);
                                j('#edit_employement_status').val(response[0].employment_status);
                                j('#edit_employers_name').val(response[0].employer);
                                j('#edit_kinname').val(response[0].next_of_kin_fname);
                                j('#edit_kinsname').val(response[0].next_of_kin_lname);
                                j('#edit_kinrelation').val(response[0].next_of_kin_relation);
                                j('#edit_kinphone').val(response[0].next_of_kin_phone);
                                j('#edit_family_number').val(response[0].family_base_number);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.view_patient_link').click(function () {


                        //get data
                        var patient_id = j(this).closest('tr').find('input[name="view_patient_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_patient_reports_details/" + patient_id,
                            dataType: "json",
                            success: function (response) {
                                j('#view_patient_id').val(response[0].patient_id);
                                j('#view_title').val(response[0].title);
                                j('#view_phone_no').val(response[0].phone_no);
                                j('#view_inputFirstName').val(response[0].f_name);
                                j('#view_inputSurName').val(response[0].s_name);
                                j('#view_inputOtherName').val(response[0].other_name);
                                j('#view_dob').val(response[0].dob);
                                j('#view_gender').val(response[0].gender);
                                j('#view_maritalstatus').val(response[0].marital_status);
                                j('#view_nationalid').val(response[0].identification_number);
                                j('#view_email').val(response[0].email);
                                j('#view_residence').val(response[0].residence);
                                j('#view_employement_status').val(response[0].employment_status);
                                j('#view_employers_name').val(response[0].employer);
                                j('#view_kinname').val(response[0].next_of_kin_fname);
                                j('#view_kinsname').val(response[0].next_of_kin_lname);
                                j('#view_kinrelation').val(response[0].next_of_kin_relation);
                                j('#view_kinphone').val(response[0].next_of_kin_phone);
                                j('#view_family_number').val(response[0].family_base_number);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.delete_patient_link').click(function () {


                        //get data
                        var patient_id = j(this).closest('tr').find('input[name="view_patient_id"]').val();
                        j('.input_patient_id_delete').val(patient_id);
                    });
                    j('.edit_visit_link').click(function () {


                        //get data
                        var visit_id = j(this).closest('tr').find('input[name="view_visit_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_visit_reports_details/" + visit_id,
                            dataType: "json",
                            success: function (response) {
                                var title = response[0].title;
                                var f_name = response[0].f_name;
                                var s_name = response[0].s_name;
                                var other_name = response[0].other_name;
                                var space = ' ';
                                var patient_name = title + space + f_name + space + s_name + space + other_name;
                                j('#edit_visit_id').val(response[0].visit_id);
                                j('#edit_visitation_status').val(response[0].visit_status);
                                j('#edit_phone_no').val(response[0].phone_no);
                                j('#edit_inputPatientName').val(patient_name);
                                j('#edit_inputPatientFamilyNumber').val(response[0].family_number);
                                j('#edit_visit_date').val(response[0].visit_date);
                                j('#edit_nurse_queue').val(response[0].nurse_queue);
                                j('#edit_doctor_queue').val(response[0].doctor_queue);
                                j('#edit_doctor_name').val(response[0].doctor_name);
                                j('#edit_lab_queue').val(response[0].lab_queue);
                                j('#edit_pharm_queue').val(response[0].pharm_queue);
                                j('#edit_urgency').val(response[0].urgency);
                                j('#edit_package_name').val(response[0].package_name);
                                j('#edit_pay_at_the_end').val(response[0].pay_at_the_end);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.view_visit_link').click(function () {


                        //get data
                        var visit_id = j(this).closest('tr').find('input[name="view_visit_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_visit_reports_details/" + visit_id,
                            dataType: "json",
                            success: function (response) {
                                var title = response[0].title;
                                var f_name = response[0].f_name;
                                var s_name = response[0].s_name;
                                var other_name = response[0].other_name;
                                var space = ' ';
                                var patient_name = title + space + f_name + space + s_name + space + other_name;
                                j('#view_visit_id').val(response[0].visit_id);
                                j('#view_visitation_status').val(response[0].visit_status);
                                j('#view_phone_no').val(response[0].phone_no);
                                j('#view_inputPatientName').val(patient_name);
                                j('#view_inputPatientFamilyNumber').val(response[0].family_number);
                                j('#view_visit_date').val(response[0].visit_date);
                                j('#view_nurse_queue').val(response[0].nurse_queue);
                                j('#view_doctor_queue').val(response[0].doctor_queue);
                                j('#view_doctor_name').val(response[0].doctor_name);
                                j('#view_lab_queue').val(response[0].lab_queue);
                                j('#view_pharm_queue').val(response[0].pharm_queue);
                                j('#view_urgency').val(response[0].urgency);
                                j('#view_package_name').val(response[0].package_name);
                                j('#view_pay_at_the_end').val(response[0].pay_at_the_end);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.delete_visit_link').click(function () {


                        //get data
                        var visit_id = j(this).closest('tr').find('input[name="view_visit_id"]').val();
                        j('.input_visit_id_delete').val(visit_id);
                    });
                    j('.view_walkin_patient_link').click(function () {


                        //get data
                        var walkin_patient_id = j(this).closest('tr').find('input[name="view_walkin_patient_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_walkinpatient_reports_details/" + walkin_patient_id,
                            dataType: "json",
                            success: function (response) {

                                j('#view_walkin_id').val(response[0].walkin_id);
                                j('#view_inputwalkinPatientName').val(response[0].walkin_patient_name);
                                j('#view_inputPhoneNumber').val(response[0].walkin_phone_no);
                                j('#view_department_name').val(response[0].walkin_department);
                                j('#view_payment_status').val(response[0].paid);
                                j('#view_walkin_visit_date').val(response[0].walkin_date);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.edit_walkin_patient_link').click(function () {


                        //get data
                        var walkin_patient_id = j(this).closest('tr').find('input[name="view_walkin_patient_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_walkinpatient_reports_details/" + walkin_patient_id,
                            dataType: "json",
                            success: function (response) {

                                j('#edit_walkin_id').val(response[0].walkin_id);
                                j('#edit_inputwalkinPatientName').val(response[0].walkin_patient_name);
                                j('#edit_inputPhoneNumber').val(response[0].walkin_phone_no);
                                j('#edit_department_name').val(response[0].walkin_department);
                                j('#edit_payment_status').val(response[0].paid);
                                j('#edit_walkin_status').val(response[0].walkin_status);
                                j('#edit_walkin_visit_date').val(response[0].walkin_date);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.delete_walkin_patient_link').click(function () {


                        //get data
                        var visit_id = j(this).closest('tr').find('input[name="view_walkin_patient_id"]').val();
                        j('.input_walkin_patient_id_delete').val(visit_id);
                    });
                    j('.edit_procedure_link').click(function () {


                        //get data
                        var procedure_visit_id = j(this).closest('tr').find('input[name="view_procedure_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_procedure_visit_reports_details/" + procedure_visit_id,
                            dataType: "json",
                            success: function (response) {

                                j('#edit_procedure_visit_id').val(response[0].procedure_visit_id);
                                j('#edit_inputPatientName').val(response[0].patient_name);
                                j('#edit_inputPhoneNumber').val(response[0].patient_phone);
                                j('#edit_patient_type').val(response[0].patient_type);
                                j('#edit_procedure_name').val(response[0].procedure_name);
                                j('#edit_visit_date').val(response[0].date_added);
                                j('#edit_visit_status').val(response[0].status);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.view_procedure_link').click(function () {


                        //get data
                        var procedure_visit_id = j(this).closest('tr').find('input[name="view_procedure_id"]').val();
                        j.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>reports/get_procedure_visit_reports_details/" + procedure_visit_id,
                            dataType: "json",
                            success: function (response) {

                                j('#view_procedure_visit_id').val(response[0].procedure_visit_id);
                                j('#view_inputPatientName').val(response[0].patient_name);
                                j('#view_inputPhoneNumber').val(response[0].patient_phone);
                                j('#view_procedure_name').val(response[0].procedure_name);
                                j('#view_visit_date').val(response[0].date_added);
                                j('#view_visit_status').val(response[0].status);
                            },
                            error: function (data) {

                            }
                        });
                    });
                    j('.delete_procedure_link').click(function () {


                        //get data
                        var visit_id = j(this).closest('tr').find('input[name="view_procedure_id"]').val();
                        j('.input_procedure_patient_id_delete').val(visit_id);
                    });
                });</script>


            <?php
        }
        ?>


        <script src="<?php echo base_url(); ?>assets/hotkeys/jquery-1.4.2.js"></script>
        <script src="<?php echo base_url(); ?>assets/hotkeys/jquery.hotkeys.js"></script>
        <script type="text/javascript">
            var y = jQuery.noConflict();
            function activaTab(tab) {
                $('.nav-tabs a[href="#' + tab + '"]').tab('show');
            }
            ;
            function fancypopup(link_id) {

                $.fancybox({
                    href: link_id,
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
            }
            ;
            y(document).ready(function () {

                $('#patient_procedure_option').fancybox({
                    padding: 0,
                    openEffect: 'elastic',
                    overlay: {closeClick: false}
                });
                y(document).bind('keydown', 'shift+1', function () {
                    activaTab('nurse_notes');
                })
                        .bind('keydown', 'shift+2', function () {
                            activaTab('chief_complaints');
                        })
                        .bind('keydown', 'shift+3', function () {
                            activaTab('review_of_systems');
                        })
                        .bind('keydown', 'shift+4', function () {
                            activaTab('working_diagnosis');
                        })
                        .bind('keydown', 'shift+5', function () {
                            activaTab('tests_referrals');
                        })
                        .bind('keydown', 'shift+6', function () {
                            activaTab('all_records_tab');
                        })
                        .bind('keydown', 'shift+7', function () {
                            activaTab('triage_tab');
                        })
                        .bind('keydown', 'shift+8', function () {
                            activaTab('lab_tab');
                        })
                        .bind('keydown', 'shift+9', function () {
                            activaTab('consultation_tab');
                        })
                        .bind('keydown', 'shift+q', function () {
                            activaTab('prescription_tab');
                        })
                        .bind('keydown', 'shift+w', function () {
                            activaTab('order_commodity_tab');
                        })
                        .bind('keydown', 'shift+e', function () {
                            activaTab('bill_patient_tab');
                        })
                        .bind('keydown', 'shift+r', function () {
                            activaTab('dispense_commodity_tab');
                        })
                        .bind('keydown', 'shift+h', function () {
                            var url = "<?php echo base_url(); ?>home";
                            window.location = url;
                        })
                        .bind('keydown', 'alt+r', function () {
                            var url = "<?php echo base_url(); ?>reception/register_patient";
                            window.location = url;
                        })
                        .bind('keydown', 'shift+b', function () {
                            var url = "<?php echo base_url(); ?>reception/visit";
                            window.location = url;
                        })
                        .bind('keydown', 'shift+a', function () {
                            var url = "<?php echo base_url(); ?>reception/walkin_patient";
                            window.location = url;
                        })
                        .bind('keydown', 'shift+p', function () {
                            var url = "<?php echo base_url(); ?>cashier";
                            window.location = url;
                        })


                        .bind('keydown', 'shift+l', function () {
                            var url = "<?php echo base_url(); ?>home/do_logout";
                            window.location = url;
                        })
                        .bind('keydown', 'shift+z', function () {
                            fancypopup('#edit_triage_form');
                        })
                        .bind('keydown', 'shift+x', function () {
                            fancypopup('#add_walkin_form');
                        })

                        .bind('keydown', 'shift+e', function () {
                            var url = "<?php echo base_url(); ?>reports/visitation_report";
                            window.location = url;
                        })

                        .bind('keydown', 'shift+d', function () {
                            fancypopup('#procedure_option');
                        })
                        .bind('keydown', 'alt+v', function () {
                            var url = "<?php echo base_url(); ?>cashier/regular_patient_visit_statement";
                            window.location = url;
                        })

                        .bind('keydown', 'ctrl+alt+w', function () {
                            var url = "<?php echo base_url(); ?>cashier/walkin_patient_visit_statement";
                            window.location = url;
                        })

                        .bind('keydown', 'alt+p', function () {
                            fancypopup('#patient_report_filter_form');
                        })


                        .bind('keydown', 'alt+v', function () {
                            fancypopup('#visit_report_filter_form');
                        })


                        .bind('keydown', 'alt+i', function () {
                            fancypopup('#walkin_report_filter_form');
                        })

                        .bind('keydown', 'alt+d', function () {
                            fancypopup('#regular_procedure_report_filter_form');
                        })



                setInterval(function () {
                    window.location.reload();
                }, 3000000);



            });

        </script>

    </body>
</html>
