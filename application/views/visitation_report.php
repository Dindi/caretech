<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Reports</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Visitation Report</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <a  id="export_patient_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Visitation Report" class="export_patient_report_filter_link" href="#export_visitation_report_filter_form">
                    <i class="glyphicon glyphicon-download-alt"></i>

                </a>
                <table class="table-bordered table-condensed table-responsive visitation_report responsive">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Visit Date</th>
                            <th>Patient Name</th>
                            <th>Gender</th>
                            <th>Phone No</th>
                            <th>Email</th>
                            <th>Service Offered</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Visit Date</th>
                            <th>Patient Name</th>
                            <th>Gender</th>
                            <th>Phone No</th>
                            <th>Email</th>
                            <th>Service Offered</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <?php
                            $i = 1;
                            foreach ($visitations as $value) {
                                ?>
                                <td class="center"><?php echo $i; ?></td>
                                <td class="center">
                                    <?php
                                    echo $value['date_added'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['f_name'] . $value['s_name'] . $value['other_name'];
                                    ?>
                                </td>

                                <td class="center">
                                    <?php echo $value['gender']; ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['phone_no'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['email'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['package_name'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    $status = $value['visit_status'];
                                  
                                    if ($status === "Active") {
                                        ?>
                                        <span class="label-success label label-default"><?php echo $status; ?></span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="label-danger label"><?php echo $status; ?></span>
                                        <?php
                                    }
                                    ?>
                                </td>


                                <td class="center">
                                    <input type="hidden" name="view_visit_id" class="view_visit_id" id="view_visit_id" value="<?php echo $value['visit_id']; ?>"/>
                                    <a  id="view_visit_link" class="view_visit_link" href="#view_visit_form">
                                        <i class="glyphicon glyphicon-zoom-in icon-white"></i>

                                    </a>|
                                    <a  id="edit_visit_link" class="edit_visit_link" href="#edit_visit_form">
                                        <i class="glyphicon glyphicon-edit icon-white"></i>

                                    </a>|
                                    <a  id="delete_visit_link" class="delete_visit_link" href="#delete_visit_form">
                                        <i class="glyphicon glyphicon-trash icon-white"></i>

                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>




                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->


    <div id="edit_visit_form" style="display: none;">



        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class = "box-header well" data-original-title = "">
                        <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Visitation Form</h2>

                        <div class = "box-icon">
                            <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                    class = "glyphicon glyphicon-chevron-up"></i></a>

                        </div>
                    </div>



                    <div class = "box-content">

                        <div class = "bs-example">


                            <form class = "edit_visitation_info " autocomplete="off" method = "post"  id = "edit_visitation_info" role = "form">

                                <input type="hidden" name="edit_visit_id" class="edit_visit_id" id="edit_visit_id" />

                                <div class = "form-inline">

                                    <div class = "form-group">
                                        <label class = "sr-only" for = "edit_inputPatientName">Patient Name : </label>
                                        <input type = "text" readonly="" class = "form-control edit_inputPatientName" id = "edit_inputPatientName" name="edit_inputPatientName" placeholder = "Patient Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "edit_inputPatientFamilyNumber"> Patient Family Number : </label>
                                        <input type = "text" readonly="" class = "form-control edit_inputPatientFamilyNumber" id = "edit_inputPatientFamilyNumber" name="edit_inputPatientFamilyNumber" placeholder = "Patient Family NUmber">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "edit_package_name">Package Name : </label>
                                        <input type = "text" readonly="" class = "form-control edit_package_name" id = "edit_package_name" name="edit_package_name" placeholder = "Package Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "edit_visit_date">Visit Date : </label>
                                        <input type = "text" readonly="" class = "form-control edit_visit_date" id = "edit_visit_date" name="edit_visit_date" placeholder = "Visit Date">
                                    </div>
                                    <hr>

                                </div>

                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "control-label" for = "edit_nurse_queue"> Nurse Queue :</label>
                                        <select name="edit_nurse_queue"  required="" class="selector edit_nurse_queue" id="edit_nurse_queue" >
                                            <option value="">Please select status </option>
                                            <option value="active">Active</option>
                                            <option value="in-active">In Active </option>
                                        </select>   
                                    </div>
                                    <div class = "form-group">
                                        <label class = "control-label" for = "edit_doctor_queue"> Doctor Queue :</label>
                                        <select name="edit_doctor_queue"  required="" class="selector edit_doctor_queue" id="edit_doctor_queue" >
                                            <option value="">Please select status </option>
                                            <option value="active">Active</option>
                                            <option value="in-active">In Active </option>
                                        </select>   
                                    </div>
                                    <hr>
                                    <div class = "form-group">
                                        <label class = "control-label" for = "edit_title"> Lab Queue :</label>
                                        <select name="edit_lab_queue"  required="" class="selector edit_lab_queue" id="edit_lab_queue" >
                                            <option value="">Please select status </option>
                                            <option value="active">Active</option>
                                            <option value="in-active">In Active </option>
                                        </select>   
                                    </div>
                                    <div class = "form-group">
                                        <label class = "control-label" for = "edit_pharm_queue"> Pharm Queue :</label>
                                        <select name="edit_pharm_queue"  required="" class="selector edit_pharm_queue" id="edit_pharm_queue" >
                                            <option value="">Please select status </option>
                                            <option value="active">Active</option>
                                            <option value="in-active">In Active </option>
                                        </select>   
                                    </div>
                                    <hr>

                                </div>


                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = " control-label" for = "edit_urgency"> Urgency :</label>
                                        <select name="edit_urgency"  required="" class="selector edit_urgency" id="edit_urgency" >
                                            <option value="">Please select status </option>
                                            <option value="active">Active</option>
                                            <option value="in-active">In Active </option>
                                        </select>   
                                    </div>
                                    <div class = "form-group">
                                        <label class = "control-label" for = "edit_pay_at_the_end"> Pay at the End :</label>
                                        <select name="edit_pay_at_the_end"  required="" class="selector edit_pay_at_the_end" id="edit_pay_at_the_end" >
                                            <option value="">Please select status </option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No </option>
                                        </select>   
                                    </div>

                                    <div class = "form-group">
                                        <label class = "control-label" for = "edit_visitation_status"> Visitation Status :</label>
                                        <select name="edit_visitation_status"  required="" class="selector edit_visitation_status" id="edit_visitation_status" >
                                            <option value="">Please select status </option>
                                            <option value="Active"> Active</option>
                                            <option value="In Active">In Active </option>
                                        </select>   
                                    </div>
                                    <hr>
                                </div>


                                <hr>

                                <div class = "form-group">

                                    <input type="submit" class="btn btn-small btn-success" value="Save Edit"/>
                                    <input type="reset" class="btn btn-close btn-danger"/>
                                </div>
                            </form>


                        </div>


                    </div>
                </div>
                <!--/span-->

            </div><!--/row-->


        </div>


    </div>



    <div id="view_visit_form" style="display: none;">



        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class = "box-header well" data-original-title = "">
                        <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Visitation Form</h2>

                        <div class = "box-icon">
                            <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                    class = "glyphicon glyphicon-chevron-up"></i></a>

                        </div>
                    </div>



                    <div class = "box-content">

                        <div class = "bs-example">


                            <form class = "view_visitation_info " autocomplete="off" method = "post"  id = "view_visitation_info" role = "form">

                                <input type="hidden" name="view_visit_id" class="view_visit_id" id="view_visit_id" />

                                <div class = "form-inline">

                                    <div class = "form-group">
                                        <label class = "sr-only" for = "view_inputPatientName">Patient Name : </label>
                                        <input type = "text" readonly="" class = "form-control view_inputPatientName" id = "view_inputPatientName" name="view_inputPatientName" placeholder = "Patient Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "view_inputPatientFamilyNumber"> Patient Family Number : </label>
                                        <input type = "text" readonly="" class = "form-control view_inputPatientFamilyNumber" id = "view_inputPatientFamilyNumber" name="view_inputPatientFamilyNumber" placeholder = "Patient Family NUmber">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "view_package_name">Package Name : </label>
                                        <input type = "text" readonly="" class = "form-control view_package_name" id = "view_package_name" name="view_package_name" placeholder = "Package Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "view_visit_date">Visit Date : </label>
                                        <input type = "text" readonly="" class = "form-control view_visit_date" id = "view_visit_date" name="view_visit_date" placeholder = "Visit Date">
                                    </div>
                                    <hr>

                                </div>

                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "control-label" for = "view_nurse_queue"> Nurse Queue :</label>
                                        <input name="view_nurse_queue" readonly=""  class="form-control view_nurse_queue" id="view_nurse_queue" />

                                    </div>
                                    <div class = "form-group">
                                        <label class = "control-label" for = "view_doctor_queue"> Doctor Queue :</label>
                                        <input name="view_doctor_queue" readonly=""   class="form-control view_doctor_queue" id="view_doctor_queue" />


                                    </div>
                                    <hr>
                                    <div class = "form-group">
                                        <label class = "control-label" for = "view_title"> Lab Queue :</label>
                                        <input name="view_lab_queue" readonly=""   class="form-control view_lab_queue" id="view_lab_queue" />


                                    </div>
                                    <div class = "form-group">
                                        <label class = "control-label" for = "view_pharm_queue"> Pharm Queue :</label>
                                        <input name="view_pharm_queue"  readonly=""  class="form-control view_pharm_queue" id="view_pharm_queue" />

                                    </div>
                                    <hr>

                                </div>


                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = " control-label" for = "view_urgency"> Urgency :</label>
                                        <input name="view_urgency" readonly=""   class="form-control view_urgency" id="view_urgency" />


                                    </div>
                                    <div class = "form-group">
                                        <label class = "control-label" for = "view_pay_at_the_end"> Pay at the End :</label>
                                        <input name="view_pay_at_the_end" readonly=""   class="form-control view_pay_at_the_end" id="view_pay_at_the_end" />

                                    </div>

                                    <div class = "form-group">
                                        <label class = "control-label" for = "view_visitation_status"> Visitation Status :</label>
                                        <input name="view_visitation_status" readonly=""  class="form-control view_visitation_status" id="view_visitation_status" />


                                    </div>
                                    <hr>
                                </div>


                                <hr>

                            </form>


                        </div>


                    </div>
                </div>
                <!--/span-->

            </div><!--/row-->


        </div>


    </div>


    <div class="delete_visit_form" id="delete_visit_form" style="display: none;">



        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class="box-header well">


                        <div class="box-icon">

                        </div>
                    </div>




                    <div class = "box-content">

                        <div class = "bs-example">


                            <form class = "delete_visit_information_form " autocomplete="off" method = "post"  id = "delete_visit_information_form" role = "form">


                                <input type="hidden" id="input_visit_id_delete" name="input_visit_id_delete" class="input_visit_id_delete"/>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <h5 class = "" for = "">Are you sure you want to Delete the visit details? </h5>

                                    </div>

                                    <br>
                                    <div class = "form-group">

                                        <input type="submit" class="btn btn-small btn-success delete_visit_record_yes" value="Yes"/>
                                        <input type="reset" class="btn btn-close delete_visit_record_no" value="No"/>
                                    </div>
                                </div>



                            </form>


                        </div>


                    </div>
                </div>
            </div>

        </div>

    </div>




</div><!--/row-->



<div id="export_visitation_report_filter_form" style="display: none;">


    <style type = "text/css">
        .bs-example{
            margin: 20px;
        }
    </style>


    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Visitation Export Report Filter</h2>


                </div>
                <div class="box-content">
                    <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/export_visitation_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                        <div class="control-group">
                            <label class="control-label" for="selectError">Date From :  </label>

                            <div class="controls">
                                <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="visitation_date_from" class="date_from form-control input-sm"/>
                            </div>
                        </div>

                        <hr>
                        <div class="control-group">
                            <label class="control-label" for="selectError">Date To : </label>

                            <div class="controls">
                                <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="visitation_date_to"/>
                            </div>
                        </div>
                        <hr>


                        <div class="control-group">
                            <label class="control-label" for="selectError">Patient Name : </label>

                            <div class="controls">
                                <select id="selectError" name="patient_name" class="patient_name" required="" data-rel="">
                                    <option value="">Please select  Status : </option>
                                    <option value="All" >All</option>
                                    <?php foreach ($name as $value) {
                                        ?>
                                        <option value="<?php echo $value['patient_id']; ?>"><?php echo $value['title'] . ':' . $value['f_name'] . $value['s_name'] . $value['other_name']; ?></option><?php }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="control-group">
                            <label class="control-label" for="selectError">Package Type  : </label>

                            <div class="controls">
                                <select id="selectError" name="package_type" id="package_type" required="" data-rel="">
                                    <option value="">Please select  Package Type : </option>
                                    <option value="All" >All</option>
                                    <?php foreach ($pack as $value) {
                                        ?>
                                        <option value="<?php echo $value['package_id']; ?>"><?php echo $value['package_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <hr>

                        <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                        <hr>
                    </form>

                </div>
            </div>
        </div>
        <!--/span-->
    </div>

</div>