<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?php echo base_url(); ?>">Home</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>">Patient Profile </a>
        </li>
    </ul>
</div>




<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-user"></i> Patient Bio Data </h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-7 col-md-12">

                    <table>
                        <tbody>
                            <?php
                            foreach ($patient_bio as $patient_bio_data) {
                                ?>
                                <tr>`
                                    <td> <h5>Name : <?php echo $patient_bio_data['patient_name']; ?> </h5></td>
                            <input type="hidden" name="hidden_patient_name" class="hidden_patient_name" id="hidden_patient_name" value="<?php echo $patient_bio_data['patient_name']; ?>"/>
                            <td><h6>EMR No : <?php echo $patient_bio_data['family_number']; ?> </h6></td>
                            <td> <h6 class="">Age : <?php
                                    $dob = $patient_bio_data['dob'];
                                    $age = date_diff(date_create($dob), date_create('now'))->y;

                                    if ($age <= 0) {
                                        $bage = date_diff(date_create($dob), date_create('now'))->d;
                                        $nage = $bage . " Days Old";
                                    } elseif ($age > 0) {
                                        $nage = $age . " Years Old";
                                    }
                                    echo $nage;
                                    ?> </h6><td>

                            <td> <h6>Sex : <?php
                                    echo $patient_bio_data['gender'];
                                    ?>
                                </h6> </td>
                            <td> <h6>Residence : <?php
                                    echo $patient_bio_data['residence'];
                                    ?>
                                </h6> </td>
                            <td><span class="label">Allergies : </span><h6 id="patient_allergies_list" class="patient_allergies_list"></h6></td>
                            <td> <a href="#send_to_doctor" class=" send_to_doctor_link btn btn-xs btn-info" id="send_to_doctor_link">
                                    <i class="glyphicon  icon-white"></i>
                                    Send To Doctor : 
                                </a></td>
                            <td> 
                                <form class="send_to_pharmacy_form" id="send_to_pharmacy_form">
                                    <input type="hidden" name="send_to_pharm_visit_id" id="send_to_pharm_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="send_to_pharm_visit_id"/>
                                    <input type="submit" name="send_to_pharmacy_link" value=" Send To Pharmacy : " id="send_to_pharmacy_link" class=" send_to_pharmacy_link btn btn-xs btn-info"/>
                                </form>
                            </td>
                            <td> 
                                <a id="release_patient_link" href="#notification_release_patient" class="release_patient_link btn btn-xs btn-danger">Release Patient</a>

                            </td>
                            </tr>
                            <?php
                        }
                        ?> 

                        </tbody>
                    </table>

                    <div id="notification_release_patient" class="notification_release_patient" style="display: none;">




                        <div class = "box col-md-12">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "">
                                    <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                                    <div class = "box-icon">

                                    </div>
                                </div>

                                <div class = "box-content">
                                    <label class = "">
                                        Are you sure you want to release the  Patient ? 
                                    </label><br>
                                    <table>
                                        <tr>
                                            <td>
                                                <form class="release_patient_form" id="release_patient_form">
                                                    <input type="hidden" name="release_patient_visit_id" id="release_patient_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="relaease_patient_visit_id"/>
                                                    <input type="submit" name="release_patient_link" value="Yes " id="yes_release_patient" class=" yes_release_patient btn btn-xs btn-danger"/>
                                                </form>
                                            </td>
                                            <td>

                                                <a id = "relase_patient_no" href = "#relase_patient_no" class = " relase_patient_no btn btn-default btn-xs ">
                                                    No
                                                </a>
                                            </td>
                                        </tr>
                                    </table>




                                </div>
                            </div>
                        </div>
                        <!--/span-->



                    </div>


                </div>


            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-th"></i> Patient Records</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                </div>
            </div>
            <div class="box-content">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"> <a href="#nurse_notes">Nurse Notes : </a></li>
                    <li > <a href="#chief_complaints">Chief Complaints</a></li>
                    <li> <a href="#review_of_systems">Review of Systems</a></li>
                    <li><a href="#working_diagnosis">Working Diagnosis</a></li>
                    <li> <a href="#tests_referrals">Tests/Referrals </a></li>
                    <li><a href="#all_records_tab">All History</a></li>
                    <li ><a href="#triage_tab">Triage</a></li>
                    <li><a href="#lab_tab">Lab</a></li>
                    <li><a href="#consultation_tab">Consultation</a></li>
                    <li><a href="#prescription_tab">Prescription</a></li>

                    <li> <a href="#order_commodity_tab">Order Commodity</a></li>
                    <li> <a href="#bill_patient_tab">Bill Patient</a></li>
                    <li><a href="#dispense_commodity_tab">Dispense Commodity</a></li>
                </ul>

                <div id="myTabContent" class="tab-content">
                    <!--Nurse notes tab start -->
                    <div class="tab-pane active" id="nurse_notes">
                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapsevitalsigns"><i class="glyphicon glyphicon-plus"></i> 
                                        Vital Signs Records : 
                                    </a>
                                </div>

                                <div id="collapsevitalsigns" class="accordion-body collapse">

                                    <?php
                                    foreach ($vital_signs as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo 'triage' . $value['triage_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i> <span> <?php
                                                                $weight = $value['weight'];
                                                                $height = $value['height'];
                                                                $one_hundred = 100;
                                                                $height = $height / $one_hundred;
                                                                $new_height = $height * $height;


                                                                if ($new_height == 0) {
                                                                    ?>
                                                                    <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                                                    <?php
                                                                } else {
                                                                    $BMI = $weight / $new_height;
                                                                    if ($BMI <= 20) {
                                                                        ?>
                                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                    <?php } ?>

                                                                    <?php if ($BMI >= 25) { ?>
                                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                    <?php } ?>

                                                                    <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                                        <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>

                                                            </span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo 'triage' . $value['triage_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">


                                                            <ul class="list-inline">
                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Visit Date :</label>  <?php
                                                                    echo $value['visit_date'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Respiratory Rate :(No/Min)</label>  <?php
                                                                    echo $value['respiratory_rate'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Pulse Rate :(No/Min)</label> <?php
                                                                    echo $value['pulse_rate'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Blood Pressure :(No/Min)</label> <?php
                                                                    echo $value['diastolic'] / $value['systolic'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Temperature :(°C)</label> <?php
                                                                    echo $value['temperature'];
                                                                    ?></li>
                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Height :(Cm)</label> <?php
                                                                    echo $value['height'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Weight :(Kg)</label> <?php
                                                                    echo $value['weight'];
                                                                    ?></li>

                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                        <hr>


                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapsevisitreason"><i class="glyphicon glyphicon-plus"></i> 
                                        Reason for Visit : 
                                    </a>
                                </div>

                                <div id="collapsevisitreason" class="accordion-body collapse">

                                    <?php
                                    foreach ($reason_for_visit as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#">
                                                            <i class="glyphicon glyphicon-hand-right"></i><span> <?php
                                                                echo'   Reason for visit : ' . $value['visit_reason'];
                                                                ?></span>
                                                        </a>
                                                    </div>

                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <hr>

                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapsetobaccosocial"><i class="glyphicon glyphicon-plus"></i> 
                                        Tobacco/Social History : 
                                    </a>
                                </div>

                                <div id="collapsetobaccosocial" class="accordion-body collapse">

                                    <?php
                                    foreach ($allergy_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['allergy_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php echo $value['date_added']; ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['allergy_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Tobacco/Social History  : </label>  <?php
                                                                    echo $value['social_history'];
                                                                    ?></li>







                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                        <hr>


                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseimmunization"><i class="glyphicon glyphicon-plus"></i> 
                                        Immunization History : 
                                    </a>
                                </div>

                                <div id="collapseimmunization" class="accordion-body collapse">

                                    <?php
                                    foreach ($allergy_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['allergy_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php $value['date_added']; ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['allergy_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">



                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default"> Immunization : </label>  <?php
                                                                    echo $value['social_history'];
                                                                    ?></li>




                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>


                    </div>
                    <!-- Nurse notes tab end -->
                    <!-- Chief complaints start -->
                    <div class="tab-pane" id="chief_complaints">
                        <div class = "row">
                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> Chief Complaints</h2>

                                        <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                    class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                    </div>
                                    <div class = "box-content">

                                        <div class = "bs-example">
                                            <form id="chief_complaints_form" class="chief_complaints_form">
                                                <input type="hidden" name="checking_patient_id" class="checking_patient_id" id="checking_patient_id" value="<?php echo $this->uri->segment(3); ?>"/>
                                                <input type="hidden" name="checking_visit_id" class="checking_visit_id" id="checking_visit_id" value="<?php echo $this->uri->segment(4); ?>"/>

                                                <input type="hidden" name="patient_id" class=" form-control patient_id" id="patient_id" value="<?php echo $this->uri->segment(3); ?>"/>
                                                <input type="hidden" name="visit_id" class=" form-control visit_id" id="visit_id" value="<?php echo $this->uri->segment(4); ?>"/>
                                                <textarea class="form-control chief_complaints_txt_area" rows="2" cols="100" name="chief_complaints" id="chief_complaints_txt_area"></textarea>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Chief complaints  end -->
                    <!-- Allergies start -->
                    <div class="tab-pane" id="allergies">

                        <form class="add_allergy_form" id="add_allergy_form">
                            <input type="hidden" name="add_allergy_patient_id" id="" value="<?php echo $this->uri->segment(3); ?>"/>
                            <input type="hidden" name="add_allergy_visit_id" id="" value="<?php echo $this->uri->segment(4); ?>"/>
                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "allergy">Allergy : </label>
                                    <textarea  class = "form-control" name="allergy" id ="allergy"  placeholder = "Allergy"></textarea>
                                </div>
                            </div>
                            <hr>
                            <div class="form-inline">
                                <div class="form-group">
                                    <input type="submit" name="add_allergy_button" class="add_allergy_button btn btn-success btn-xs" id="add_allergy_button" value="Save"/>
                                </div>
                            </div>
                        </form>

                    </div>
                    <!-- Allergies end -->

                    <!-- Review of Systems start -->
                    <div class="tab-pane" id="review_of_systems">
                        <div class = "row">
                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> Review of Systems </h2>

                                        <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                    class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                    </div>
                                    <div class = "box-content">

                                        <div class = "bs-example">



                                            <form id="review_of_systems_form" class="review_of_systems_form">





                                                <div class="accordion" id="accordion1">

                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseConstitutional"><i class="glyphicon glyphicon-plus"></i> 
                                                                Constitutional >> : 
                                                            </a>
                                                        </div>

                                                        <div id="collapseConstitutional" class="accordion-body collapse">

                                                            <textarea class="form-control constitutional" placeholder="Constitutional System  : " rows="2" cols="100" name="constitutional" id="constitutional"></textarea>

                                                        </div>

                                                    </div>
                                                </div>

                                                <hr>


                                                <div class="accordion" id="accordion1">

                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseENT"><i class="glyphicon glyphicon-plus"></i> 
                                                                ENT >> : 
                                                            </a>
                                                        </div>

                                                        <div id="collapseENT" class="accordion-body collapse">

                                                            <textarea class="form-control autogrow ent" placeholder="ENT System  : " rows="2" cols="100" name="ent" id="ent"></textarea>


                                                        </div>

                                                    </div>
                                                </div>
                                                <hr>

                                                <div class="accordion" id="accordion1">

                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseCardiovascular"><i class="glyphicon glyphicon-plus"></i> 
                                                                Cardiovascular >> : 
                                                            </a>
                                                        </div>

                                                        <div id="collapseCardiovascular" class="accordion-body collapse">

                                                            <textarea class="form-control autogrow cardio_vascular" placeholder="Cardio Vascular System  : " rows="2" cols="100" name="cardio_vascular" id="cardio_vascular"></textarea>

                                                        </div>

                                                    </div>
                                                </div>

                                                <hr>


                                                <div class="accordion" id="accordion1">

                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseEye"><i class="glyphicon glyphicon-plus"></i> 
                                                                Eye >> : 
                                                            </a>
                                                        </div>

                                                        <div id="collapseEye" class="accordion-body collapse">

                                                            <textarea class="form-control autogrow eye" placeholder="Eye System  : " rows="2" cols="100" name="eye" id="eye"></textarea>

                                                        </div>

                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="accordion" id="accordion1">

                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseRespiratory"><i class="glyphicon glyphicon-plus"></i> 
                                                                Respiratory >> : 
                                                            </a>
                                                        </div>

                                                        <div id="collapseRespiratory" class="accordion-body collapse">

                                                            <textarea class="form-control autogrow respiratory" placeholder="Respiratory System  : " rows="2" cols="100" name="respiratory" id="respiratory"></textarea>

                                                        </div>

                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="accordion" id="accordion1">

                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_gastro_intestinal"><i class="glyphicon glyphicon-plus"></i> 
                                                                Gastro-Intenstinal >> : 
                                                            </a>
                                                        </div>

                                                        <div id="collapse_gastro_intestinal" class="accordion-body collapse">
                                                            <textarea class="form-control autogrow gastro_intestinal" placeholder="Gastro Intestinal System  : " rows="2" cols="100" name="gastro_intestinal" id="gastro_intestinal"></textarea>


                                                        </div>

                                                    </div>
                                                </div>


                                                <hr>

                                                <div class="accordion" id="accordion1">

                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_genito_urinary"><i class="glyphicon glyphicon-plus"></i> 
                                                                Genito-Urinary  >> : 
                                                            </a>
                                                        </div>

                                                        <div id="collapse_genito_urinary" class="accordion-body collapse">
                                                            <textarea class="form-control autogrow genito_urinary" placeholder="Genito Urinary System  : " rows="2" cols="100" name="genito_urinary" id="genito_urinary"></textarea>


                                                        </div>

                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="accordion" id="accordion1">

                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_masculo_skeletal"><i class="glyphicon glyphicon-plus"></i> 
                                                                Masculo - Skeletal  >> : 
                                                            </a>
                                                        </div>

                                                        <div id="collapse_masculo_skeletal" class="accordion-body collapse">

                                                            <textarea class="form-control autogrow masculo_skeletal" placeholder="Masculo Skeletal System  : " rows="2" cols="100" name="masculo_skeletal" id="masculo_skeletal"></textarea>

                                                        </div>

                                                    </div>
                                                </div>

                                                <hr>


                                                <div class="accordion" id="accordion1">

                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_skin"><i class="glyphicon glyphicon-plus"></i> 
                                                                Skin  >> : 
                                                            </a>
                                                        </div>

                                                        <div id="collapse_skin" class="accordion-body collapse">

                                                            <textarea class="form-control autogrow skin" placeholder="Skin : " rows="2" cols="100" name="skin" id="skin"></textarea>


                                                        </div>

                                                    </div>
                                                </div>


                                                <hr>

                                                <div class="accordion" id="accordion1">

                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_neurologic"><i class="glyphicon glyphicon-plus"></i> 
                                                                Neuro Logic  >> : 
                                                            </a>
                                                        </div>

                                                        <div id="collapse_neurologic" class="accordion-body collapse">

                                                            <textarea class="form-control autogrow neuro_logic" placeholder="Neuro Logic  Systems" rows="2" cols="100" name="neuro_logic" id="neuro_logic"></textarea>

                                                        </div>

                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="accordion" id="accordion1">

                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_others"><i class="glyphicon glyphicon-plus"></i> 
                                                                Others  >> : 
                                                            </a>
                                                        </div>

                                                        <div id="collapse_others" class="accordion-body collapse">

                                                            <input type="hidden" name="patient_id" class=" form-control patient_id" id="patient_id" value="<?php echo $this->uri->segment(3); ?>"/>
                                                            <input type="hidden" name="visit_id" class=" form-control visit_id" id="viist_id" value="<?php echo $this->uri->segment(4); ?>"/>
                                                            <textarea class="form-control autogrow other_systems" placeholder="Other Systems" rows="2" cols="100" name="other_systems" id="other_systems"></textarea>



                                                        </div>

                                                    </div>
                                                </div>


                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Review of Systems end -->
                    <!-- working_diagnosis tab start -->
                    <div class="tab-pane" id="working_diagnosis">
                        <div class = "row">
                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> Working Diagnosis</h2>

                                        <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                    class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                    </div>
                                    <div class = "box-content">



                                        <div class = "bs-example">
                                            <form id="working_diagnosis_form" class="working_diagnosis_form">


                                                <input type="hidden" name="patient_id" class=" form-control patient_id" id="patient_id" value="<?php echo $this->uri->segment(3); ?>"/>
                                                <input type="hidden" name="visit_id" class=" form-control visit_id" id="viist_id" value="<?php echo $this->uri->segment(4); ?>"/>
                                                <!--<input type="text" name="icd_10" class="form-control" placeholder="ICD 10 Codes and Description(Please type Diesase Code or Disease Name)" id="icd_10"/>-->
                                                <hr>
                                                <textarea class="form-control autogrow  working_diagnosis_txt_area" placeholder="Working Diagnosis Form" rows="2" cols="100" name="working_diagnosis" id="working_diagnosis_txt_area"></textarea>



                                                <div class="control-group">
                                                    <label class="control-label" for="selectError1">ICD 10 Codes (Please select at least one ) : </label>

                                                    <div class="controls">
                                                        <select id="selectError1" multiple class="chosen-select icd10_selector" data-rel="chosen" name="patient_icd_10_code[]" placeholder="Please select atleast one ..."   >

                                                            <?php foreach ($cache_data as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['id']; ?>">
                                                                    <?php echo $value['icd_description']; ?>
                                                                </option>
                                                            <?php }
                                                            ?>


                                                        </select>
                                                    </div>
                                                </div>

                                                <hr>





                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- working diagnosis tab end -->
                    <!-- Tests/Referrals start -->
                    <div class="tab-pane" id="tests_referrals">
                        <div class="">
                            <h6>Please select the  type of test you would like to perform : </h6>
                            <hr>
                            <a href="#test_refferal" class="btn btn-xs test_referral_link" id="test_referral_link"> Order Test</a>
                            <hr>
                            <a href="#imaging_referral" class="btn btn-xs imaging_referral_link" id="imaging_referral_link">Request for Imaging</a> |
                            <a href="#other_referral" class="btn btn-xs other_referral_link" id="other_referral_link">Refer Patient </a> |
                            <a href="#sick_off" class="btn btn-xs sick_off_link" id="sick_off_link">Sick Off </a>

                        </div>

                        <hr>



                    </div>
                    <!-- Tests/Referrals end -->



                    <!--Prescription tab start --> 
                    <div class="tab-pane" id="prescription_tab">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                            <thead>
                                <tr>
                                    <th>Visit Date</th>
                                    <th>Commodity Name</th>
                                    <th>Strength</th>
                                    <th>Route </th>
                                    <th>Frequency</th>
                                    <th>Duration</th>
                                    <th>Employee Name</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($patient_prescription_records as $value) {
                                    ?>
                                    <tr>
                                        <td><?php echo $value['date']; ?></td>
                                        <td class="center"><?php echo $value['commodity_name']; ?></td>
                                        <td class="center"><?php echo $value['strength']; ?></td>
                                        <td class="center"><?php echo $value['route']; ?> </td>
                                        <td class="center"><?php echo $value['frequency']; ?></td>
                                        <td class="center"> <?php echo $value['duration'] . $value['no_of_days']; ?></td>
                                        <td class="center"> <?php echo $value['employee_name']; ?></td>

                                    </tr>
                                <?php }
                                ?>


                            </tbody>
                        </table>
                    </div>
                    <!-- Prescription tab end -->



                    <!-- Patient instructions tab start -->
                    <div class="tab-pane" id="patient_instruction_tab">

                    </div>
                    <!-- Patient instructions tab end -->




                    <!--All records tab start -->

                    <div class="tab-pane" id="all_records_tab">



                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseTriage"><i class="glyphicon glyphicon-plus"></i> 
                                        Triage Records : 
                                    </a>
                                </div>

                                <div id="collapseTriage" class="accordion-body collapse">

                                    <?php
                                    foreach ($patient_triage_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo 'triage' . $value['triage_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i> <span> <?php
                                                                $weight = $value['weight'];
                                                                $height = $value['height'];
                                                                $one_hundred = 100;
                                                                $height = $height / $one_hundred;
                                                                $new_height = $height * $height;


                                                                if ($new_height == 0) {
                                                                    ?>
                                                                    <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                                                    <?php
                                                                } else {
                                                                    $BMI = $weight / $new_height;
                                                                    if ($BMI <= 20) {
                                                                        ?>
                                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                    <?php } ?>

                                                                    <?php if ($BMI >= 25) { ?>
                                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                                    <?php } ?>

                                                                    <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                                        <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>

                                                            </span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo 'triage' . $value['triage_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">


                                                            <ul class="list-inline">
                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Visit Date :</label>  <?php
                                                                    echo $value['visit_date'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Respiratory Rate :(No/Min)</label>  <?php
                                                                    echo $value['respiratory_rate'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Pulse Rate :(No/Min)</label> <?php
                                                                    echo $value['pulse_rate'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Blood Pressure :(No/Min)</label> <?php
                                                                    echo $value['diastolic'] . "/  " . $value['systolic'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Temperature :(°C)</label> <?php
                                                                    echo $value['temperature'];
                                                                    ?></li>
                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Height :(Cm)</label> <?php
                                                                    echo $value['height'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Weight :(Kg)</label> <?php
                                                                    echo $value['weight'];
                                                                    ?></li>

                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                        <hr>


                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseLab"><i class="glyphicon glyphicon-plus"></i> 
                                        Lab Records : 
                                    </a>
                                </div>

                                <div id="collapseLab" class="accordion-body collapse">

                                    <?php
                                    foreach ($patient_lab_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['lab_test_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php
                                                                echo $value['date_added'] . '   Test Name : ' . $value['test_name'];
                                                                ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['lab_test_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Test Name</label>  <?php
                                                                    echo $value['test_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Test Results : </label> <?php
                                                                    echo $value['test_results'];
                                                                    ?></li>


                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <hr>

                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseConsultation"><i class="glyphicon glyphicon-plus"></i> 
                                        Consultation Records : 
                                    </a>
                                </div>

                                <div id="collapseConsultation" class="accordion-body collapse">

                                    <?php
                                    foreach ($patient_consultation_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['consultation_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php $value['date']; ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['consultation_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Chief Complaint  : </label>  <?php
                                                                    echo $value['complaints'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">  Diagnosis :</label> <?php
                                                                    echo $value['working_diagnosis'];
                                                                    ?></li>




                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                        <hr>


                        <div class="accordion" id="accordion1">

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapsePrescription"><i class="glyphicon glyphicon-plus"></i> 
                                        Prescription Records : 
                                    </a>
                                </div>

                                <div id="collapsePrescription" class="accordion-body collapse">

                                    <?php
                                    foreach ($patient_prescription_records as $value) {
                                        ?>
                                        <div class="accordion-inner">

                                            <!-- Here we insert another nested accordion -->

                                            <div class="accordion" id="accordion2">
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $value['prescription_id']; ?>">
                                                            <i class="glyphicon glyphicon-plus"></i><span> <?php $value['date']; ?></span>
                                                        </a>
                                                    </div>
                                                    <div id="<?php echo $value['prescription_id']; ?>" class="accordion-body collapse in">
                                                        <div class="accordion-inner">

                                                            <ul class="list-inline">


                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Posted by : </label>  <?php
                                                                    echo $value['employee_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right">
                                                                    <label class="label label-default">Commodity name : </label>  <?php
                                                                    echo $value['commodity_name'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Strength :</label> <?php
                                                                    echo $value['strength'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Route : </label> <?php
                                                                    echo $value['route'];
                                                                    ?></li>

                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Frequency : </label> <?php
                                                                    echo $value['frequency'];
                                                                    ?></li>
                                                                <li class="glyphicon glyphicon-hand-right"> <label class="label label-default">Duration : </label> <?php
                                                                    echo $value['duration'];
                                                                    ?></li>



                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>          

                                            <!-- Inner accordion ends here -->

                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>



                    </div>

                    <!--All records tab end -->



                    <!--Order Commodity Tab -->
                    <div class="tab-pane" id="order_commodity_tab">




                        <div class="order_commodity_form" class="order_commodity_form" style="display: none">

                        </div>



                        <div class = "row">
                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> Order Commodity</h2>

                                        <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                    class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                    </div>
                                    <div class = "box-content">

                                        <div class = "bs-example">

                                            <form class="form-inline order_commodity_form" id="order_commodity_form" method="post">
                                                <table class="table table-striped table-bordered">
                                                    <tr>
                                                        <td style="font-size:14px;">Commodity Name</td>
                                                        <td style="font-size:14px;">Strength</td>
                                                        <td style="font-size:14px;">Route</td>
                                                        <td style="font-size:14px;">Frequency</td>
                                                        <td style="font-size:14px;">Occurence</td>
                                                        <td style="font-size:14px;">Duration</td>
                                                        <td>

<!--                                                            <span cl
      ass="add_more_commoditites btn btn-xs btn-info" id="add_more_commoditites" >
    Add More
</span>-->

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>      <div class="form-group">
                                                                <div class="control-group">
                                                                    <label class="control-label" for="selectError">Commodity Name : </label>
                                                                    <div class="controls">
                                                                        <select id="selectError" required="" name="commodity_name" data-rel="chosen">
                                                                            <?php
                                                                            foreach ($commodities as $value) {
                                                                                ?>
                                                                                <option>Please select commodity :</option>
                                                                                <option value="<?php echo $value['commodity_name']; ?>"><?php echo $value['commodity_name']; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </td>
                                                        <td><div class="form-group">
                                                                <label class="" for="inputStrength">Strength : </label><br>
                                                                <input id="inputStrength" required="" class="inputStrength form-control" placeholder="e.g 500Mg" name="Strength" />
                                                            </div></td>
                                                        <td>
                                                            <div class="form-group">
                                                                <div class="control-group">
                                                                    <label class="control-label" for="selectError">Route : </label>
                                                                    <div class="controls">
                                                                        <select id="selectError" required="" name="route" data-rel="chosen">
                                                                            <?php
                                                                            foreach ($route as $value) {
                                                                                ?>
                                                                                <option> Please select route : </option>
                                                                                <option value="<?php echo $value['route_name']; ?>"><?php echo $value['route_name']; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div></td>
                                                        <td>
                                                            <div class="form-group">
                                                                <div class="control-group">
                                                                    <label class="control-label" for="selectError">Frequency : </label>
                                                                    <div class="controls">
                                                                        <select id="selectError" required="" name="frequency" data-rel="chosen">
                                                                            <?php
                                                                            foreach ($frequency as $value) {
                                                                                ?>
                                                                                <option>Please select frequency : </option>
                                                                                <option value="<?php echo $value['frequency_name']; ?>"><?php echo $value['frequency_name']; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <div class="control-group">
                                                                    <label class="control-label" for="selectError"> Occurrence : </label>
                                                                    <div class="controls">
                                                                        <select id="selectError" required="" name="no_of_occurence" data-rel="chosen">
                                                                            <?php
                                                                            foreach ($occurence as $value) {
                                                                                ?>
                                                                                <option>Please select occurence: </option>
                                                                                <option value="<?php echo $value['name']; ?>"><?php echo $value['name']; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td><div class="form-group">
                                                                <label class="" for="inputDuration">Duration : </label><br>
                                                                <input id="inputDuration" required=""  class="inputDuration form-control" placeholder="e.g 5 Days" name="Duration" />
                                                            </div></td>
                                                    </tr>
                                                </table>


                                                <div id="addedRows"></div>

                                                <input type="hidden" name="order_commodity_patient_id" value="<?php echo $this->uri->segment(3); ?>"/>
                                                <input type="hidden" name="order_commodiy_visit_id" value="<?php echo $this->uri->segment(4); ?>"/>
                                                <hr>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <input type="submit" value="Order Commodity" name="order_commodity_button" class="btn btn-xs btn-success" id="order_commodity_button"/>

                                                    </div>
                                                    <div class="form-group">
                                                        <input type="reset" value="Cancel"  name="reset_order_commodity" class="btn btn-xs btn-danger" id="reset_order_commodity"/>

                                                    </div>

                                                </div>

                                            </form>



                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->

                        </div><!--/row-->







                    </div>
                    <!--Order Commodity Tab end -->



                    <!--Bill Patient Tab -->
                    <div class="tab-pane" id="bill_patient_tab">







                        <div class = "row">
                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> Bill Patient</h2>

                                        <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                    class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                    </div>
                                    <div class = "box-content">

                                        <div class = "bs-example">

                                            <table class="table table-striped table-bordered bill_patient_prescription_table " id="bill_patient_prescription_table">
                                                <thead>
                                                    <tr>
                                                        <th>No . </th>
                                                        <th>Visit Date</th>
                                                        <th>Commodity Name</th>
                                                        <th>Strength</th>
                                                        <th>Route </th>
                                                        <th>Frequency</th>
                                                        <th>Duration</th>
                                                        <th>Employee Name</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                
                                                
                                                  <tfoot>
                                                    <tr>
                                                        <th>No . </th>
                                                        <th>Visit Date</th>
                                                        <th>Commodity Name</th>
                                                        <th>Strength</th>
                                                        <th>Route </th>
                                                        <th>Frequency</th>
                                                        <th>Duration</th>
                                                        <th>Employee Name</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </tfoot>
                                                
                                                <tbody>
                                                    <?php
                                                    $i = 1;
                                                    if (empty($bill_patient_records)) {
                                                        ?>
                                                    <tr>
                                                    <td>Empty</td>
                                                    <td>Empty</td>
                                                    <td>Empty</td>
                                                    <td>Empty</td>
                                                    <td>Empty</td>
                                                    <td>Empty</td>
                                                    <td>Empty</td>
                                                    <td>Empty</td>
                                                    <td>Empty</td>
                                                    </tr>
                                                    <?php
                                                } else {


                                                    foreach ($bill_patient_records as $value) {
                                                        ?>
                                                        <tr>
                                                            <td class="center"><?php echo $i; ?></td>
                                                            <td><?php echo $value['date']; ?></td>
                                                            <td class="center"><?php echo $value['commodity_name']; ?></td>
                                                            <td class="center"><?php echo $value['strength']; ?></td>
                                                            <td class="center"><?php echo $value['route']; ?> </td>
                                                            <td class="center"><?php echo $value['frequency']; ?></td>
                                                            <td class="center"> <?php echo $value['duration'] . $value['no_of_days']; ?></td>
                                                            <td class="center"> <?php echo $value['employee_name']; ?></td>
                                                            <td><input type="hidden" name="prescription_id" id="bill_patient_prescription_id" value="<?php echo $value['prescription_id']; ?>" class="bill_patient_prescription_id"/>
                                                                <input type="hidden" name="hidden_prescription_tracker" id="hidden_prescription_tracker" value="<?php echo $value['prescription_tracker']; ?>" class="hidden_prescription_tracker hidden"/>
                                                                <?php
                                                                $billed = $value['billed'];

                                                                if ($billed === 'Yes') {
                                                                    ?>
                                                                    <span class=" alert-info ">Patient Billed</span>
                                                                    <a class="btn btn-xs btn-info bill_patient_prescription_link" id="bill_patient_prescription_link" href="#bill_patient_prescription_form">Edit Bill </a>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <a class="btn btn-xs btn-info bill_patient_prescription_link" id="bill_patient_prescription_link" href="#bill_patient_prescription_form">Bill Patient</a>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $i++;
                                                    }
                                                }
                                                ?>


                                                </tbody>
                                            </table>



                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->

                        </div><!--/row-->







                    </div>
                    <!--Bill Patient Tab end -->





                    <!---Triage Tab start --->


                    <div class="tab-pane" id="triage_tab">

                        <a  id="export_triage_patient_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Walkin Patient Report" class="export_triage_patient_report_filter_link" href="#export_triage_report_filter_form">
                            <i class="glyphicon glyphicon-download-alt"></i>
                        </a>
                        |



                        <table class="table table-striped table-bordered triage_report  responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Date </th>
                                    <th>Nurse Name</th>
                                    <th>Respiratory (No/Min)</th>
                                    <th>Pulse Rate (No/Min)</th>
                                    <th>Blood pressure (mmHg)</th>
                                    <th>Temperature (°C)</th>
                                    <th>Height (Cm)</th>
                                    <th>Weight (KGs)</th>
                                    <th>BMI</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>

                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Date </th>
                                    <th>Nurse Name</th>
                                    <th>Respiratory (No/Min)</th>
                                    <th>Pulse Rate (No/Min)</th>
                                    <th>Blood pressure (mmHg)</th>
                                    <th>Temperature (°C)</th>
                                    <th>Height (Cm)</th>
                                    <th>Weight (KGs)</th>
                                    <th>BMI</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <?php
                                    $i = 1;
                                    if (empty($patient_triage_records)) {
                                        ?>
                                        <td>Empty</td>
                                        <td>Empty </td>
                                        <td>Empty</td>
                                        <td>Empty </td>
                                        <td>Empty</td>
                                        <td>Empty</td>
                                        <td>Empty</td>
                                        <td>Empty</td>
                                        <td>Empty</td>
                                        <td>Empty</td>
                                        <td>Empty</td>
                                        <?php
                                    } else {


                                        foreach ($patient_triage_records as $value) {
                                            ?>


                                            <td class="center"><?php echo $i; ?></td>
                                            <td class="center">
                                                <?php
                                                $visit_date = $value['visit_date'];
                                                if (empty($visit_date)) {
                                                    echo 'Empty';
                                                } else {
                                                    echo $visit_date;
                                                }
                                                ?>
                                            </td>
                                            <td class="center">
                                                <?php
                                                $employee_name = $value['employee_name'];
                                                if (empty($employee_name)) {
                                                    echo 'Empty';
                                                } else {
                                                    echo $employee_name;
                                                }
                                                ?>
                                            </td>
                                            <td class="center">
                                                <?php
                                                $respiratory_rate = $value['respiratory_rate'];
                                                if (empty($respiratory_rate)) {
                                                    echo 'Empty';
                                                } else {
                                                    echo $respiratory_rate;
                                                }
                                                ?>
                                            </td>
                                            <td class="center">
                                                <?php
                                                $pulse_rate = $value['pulse_rate'];
                                                if (empty($pulse_rate)) {
                                                    echo 'Empty';
                                                } else {
                                                    echo $pulse_rate;
                                                }
                                                ?>
                                            </td>
                                            <td class="center">
                                                <?php
                                                $diastolic_systolic = $value['diastolic'] . "/" . $value['systolic'];
                                                $diastolic = $value['diastolic'];
                                                $systolic = $value['systolic'];
                                                if (empty($diastolic) or empty($systolic)) {
                                                    echo 'Empty';
                                                } else {
                                                    echo $diastolic_systolic;
                                                }
                                                ?>
                                            </td>
                                            <td class="center">
                                                <?php
                                                $temperature = $value['temperature'];
                                                if (empty($temperature)) {
                                                    echo 'Empty';
                                                } else {
                                                    echo $temperature;
                                                }
                                                ?>
                                            </td>
                                            <td class="center">
                                                <?php
                                                $height = $value['height'];
                                                if (empty($height)) {
                                                    echo 'Empty';
                                                } else {
                                                    echo $height;
                                                }
                                                ?>
                                            </td>
                                            <td class="center">
                                                <?php echo $value['weight']; ?>
                                            </td>
                                            <td class="center" ><?php
                                                $weight = $value['weight'];
                                                $height = $value['height'];
                                                $one_hundred = 100;
                                                $height = $height / $one_hundred;
                                                $new_height = $height * $height;


                                                if ($new_height == 0) {
                                                    ?>
                                                    <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                                    <?php
                                                } elseif (!empty($new_height) and ! empty($weight)) {
                                                    $BMI = $weight / $new_height;
                                                    if ($BMI <= 20) {
                                                        ?>
                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                    <?php } ?>

                                                    <?php if ($BMI >= 25) { ?>
                                                        <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                                    <?php } ?>

                                                    <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                        <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                        <?php
                                                    }
                                                } elseif (empty($new_height) or empty($weight)) {
                                                    echo 'BMI Cannot be calculated...';
                                                }
                                                ?>



                                            </td> 

                                            <td class="center">
                                                <?php
                                                $triage_id = $value['triage_id'];
                                                if (empty($triage_id)) {
                                                    echo 'Empty';
                                                } else {
                                                    ?>
                                                    <input type="hidden" name="view_triage_patient_id" class="view_triage_patient_id" id="view_triage_patient_id" value="<?php echo $value['patient_id']; ?>"/>
                                                    <input type="hidden" name="view_triage_id" class="view_triage_id" id="view_triage_id" value="<?php echo $value['triage_id']; ?>"/>
                                                    <input type="hidden" name="view_triage_visit_id" class="view_triage_visit_id" id="view_triage_visit_id" value="<?php echo $value['visit_id']; ?>"/>
                                                    <a  id="edit_triage_patient_link" class="edit_triage_patient_link" href="#edit_triage_form">
                                                        <i class="glyphicon glyphicon-edit icon-white"></i>

                                                    </a>|
                                                    <a  id="delete_triage_patient_link" class="delete_triage_patient_link" href="#delete_triage_form">
                                                        <i class="glyphicon glyphicon-trash icon-white"></i>

                                                    </a>
                                                    <?php
                                                }
                                                ?>

                                            </td>

                                            <?php
                                            ?>


                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </tr>



                            </tbody>
                        </table>


                    </div>


                    <!-- Triage tab end -->
                    <!-- Laboratory Tab Start -->

                    <div class="tab-pane" id="lab_tab">










                        <div class = "row">
                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> Lab Records</h2>

                                        <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                    class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                    </div>
                                    <div class = "box-content">

                                        <div class = "bs-example">



                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th>Test Date</th>
                                                        <th>Doctor Name</th>
                                                        <th>Test Name</th>
                                                        <th>Test Results </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($patient_lab_records as $value) {
                                                        ?>
                                                        <tr>
                                                            <td class="center"><?php echo $value['date_added']; ?></td>
                                                            <td class="center"><?php echo $value['employee_name']; ?></td>
                                                            <td class="center"><?php echo $value['test_name']; ?></td>
                                                            <td class="center">
                                                                <?php echo $value['test_results']; ?>
                                                            </td>

                                                        </tr>
                                                    <?php }
                                                    ?>


                                                </tbody>
                                            </table>




                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->

                        </div><!--/row-->






















                    </div>

                    <!-- Laboratory tab end -->



                    <!-- Consultation tab start -->
                    <div class="tab-pane" id="consultation_tab">






                        <div class = "row">
                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> Consultation Records </h2>

                                        <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                    class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                    </div>
                                    <div class = "box-content">

                                        <div class = "bs-example">


                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th>Visit Date</th>
                                                        <th>Chief Complaints</th>
                                                        <th>Diagnosis</th>
                                                        <th>Employee Name</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($patient_consultation_records as $value) {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $value['date']; ?></td>
                                                            <td class="center"><?php echo $value['complaints']; ?></td>
                                                            <td class="center"> <?php echo $value['working_diagnosis']; ?></td>
                                                            <td class="center"<?php echo $value['employee_name']; ?>></td>

                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>


                                                </tbody>
                                            </table>




                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->

                        </div><!--/row-->









                    </div>

                    <!-- Consultation tab end -->


                    <!-- Prescription tab start -->


                    <!-- Prescription tab end -->

                    <!--Others tab start -->

                    <div class="tab-pane" id="dispense_commodity_tab">






                        <div class = "row">
                            <div class = "box col-md-12">
                                <div class = "box-inner">
                                    <div class = "box-header well" data-original-title = "">
                                        <h2><i class = "glyphicon glyphicon-edit"></i> Dispense Commodity </h2>

                                        <div class = "box-icon">  <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                    class = "glyphicon glyphicon-chevron-up"></i></a>  </div>
                                    </div>
                                    <div class = "box-content">

                                        <div class = "bs-example">

                                            <div class="patient_dispense_div" id="patient_dispense_div">

                                                <table class="table table-striped table-bordered  patient_dispense_table" id="patient_dispense_table">
                                                    <thead>
                                                        <tr>
                                                            <th>Commodity Name : </th>
                                                            <th>Visit Date : </th>
                                                            <th>Quantity Available : </th>
                                                            <th>Quantity Billed : </th>
                                                            <th>Batch No : </th>
                                                            <th>Is Dispensed ? :</th>
                                                            <th>Action : </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($dispensing_data as $value) {
                                                            ?>
                                                            <tr>
                                                                <td class="center"><?php echo $value['commodity_name']; ?></td>
                                                                <td><?php echo $value['date_added']; ?></td>
                                                                <td class="center"> <?php echo $value['available_quantity']; ?></td>
                                                                <td class="center"> <?php echo $value['quantity_billed']; ?></td>
                                                                <td class="center"><?php echo $value['batch_no']; ?></td>
                                                                <td class="center" ><?php echo $value['is_dispensed']; ?></td>
                                                                <td>

                                                                    <input type="hidden" name="dispensing_patient_visit_statement_id" class="dispensing_patient_visit_statement_id hidden" id="dispensing_patient_visit_statement_id" value="<?php echo $value['patient_visit_statement_id']; ?>"/>
                                                                    <input type="hidden" name="dispensing_visit_id" class="dispensing_visit_id hidden" id="dispensing_visit_id" value="<?php echo $value['visit_id']; ?>"/>
                                                                    <input type="hidden" name="dipsensing_patient_id" class="dipsensing_patient_id hidden" id="dipsensing_patient_id" value="<?php echo $value['patient_id']; ?>"/>
                                                                    <input type="hidden" name="dispensing_prescription_id" class="dispensing_prescription_id hidden" id="dispensing_prescription_id" value="<?php echo $value['prescription_id']; ?>"/>
                                                                    <input type="hidden" name="dispensing_transaction_id" class="dispensing_transaction_id hidden" id="dispensing_transaction_id" value="<?php echo $value['transaction_id']; ?>"/>
                                                                    <input type="hidden" name="dispensing_stock_id" class="dispensing_stock_id hidden" id="dispensing_stock_id" value="<?php echo $value['stock_id']; ?>"/>
                                                                    <a id="dipsense_commodity_link" class="dipsense_commodity_link btn btn-default" href="#dipsense_commodity_form">Dispense Commodity</a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>


                                                    </tbody>
                                                </table>


                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->

                        </div><!--/row-->








                    </div>
                    <!--Others tab end -->
                </div>
            </div>
        </div>
    </div>
    <!--/span-->









</div><!--/row-->












































<div class="all" style="display: none;">



    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2>Patient Bio Data</h2>
                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <div class="well">
                    <?php
                    foreach ($patient_bio as $patient_bio_data) {
                        
                    }
                    ?>
                    <h5>Patient Name : <?php echo $patient_bio_data['patient_name']; ?> </h5>
                    <input type="hidden" name="hidden_patient_name" class="hidden_patient_name" id="hidden_patient_name" value="<?php echo $patient_bio_data['patient_name']; ?>"/>
                    <hr>
                    <h6 class="">Age : <?php
                    $dob = $patient_bio_data['dob'];
                    $age = date_diff(date_create($dob), date_create('now'))->y;

                    if ($age <= 0) {
                        $bage = date_diff(date_create($dob), date_create('now'))->d;
                        $nage = $bage . " Days Old";
                    } elseif ($age > 0) {
                        $nage = $age . " Years Old";
                    }
                    echo $nage;
                    ?> </h6>
                    <hr>
                    <h6>Residence : <?php echo $patient_bio_data['residence']; ?> </h6>

                    <a class="btn btn-info btn-xs" href="#send_to_doctor">Send to Doctor</a>
                </div>
            </div>
        </div>
    </div>

    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2>Patient Allergies </h2>
                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <div class="well">
                    <p id="patient_allergies_list" class="patient_allergies_list"></p>
                </div>
            </div>
        </div>
    </div>


    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2>Patient </h2>
                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <div class="well">
                    <a href="#nurse_patient_list_div" id="nurse_view_patient_list" class="nurse_view_patient_list btn btn-xs btn-success "><i class="glyphicon glyphicon-zoom-in"></i>View Patients List</a>


                    <hr>
                    <a class="btn btn-info btn-xs patient_medical_records_link" href="#patient_medical_records" id="patient_medical_records_link"><i class="glyphicon glyphicon-align-justify"> Medical Records :</i></a>

                </div>
            </div>
        </div>
    </div>



    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2>Patient </h2>
                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">
                <div class="well">
                    <a href="#book_appointment_form" class=" book_appointment_form_link btn btn-xs btn-info" id="book_appointment_form_link">
                        <i class="glyphicon  icon-white"></i>
                        Book an Appointment : 
                    </a>
                    <hr>
                    <a href="#send_to_doctor" class=" send_to_doctor_link btn btn-xs btn-info" id="send_to_doctor_link">
                        <i class="glyphicon  icon-white"></i>
                        Send To Doctor : 
                    </a>
                    <hr>
                    <form class="send_to_pharmacy_form_a" id="send_to_pharmacy_form_a">
                        <input type="hidden" name="send_to_pharm_visit_id" id="send_to_pharm_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="send_to_pharm_visit_id"/>
                        <input type="submit" name="send_to_pharmacy_link" value=" Send To Pharmacy : " id="send_to_pharmacy_link" class=" send_to_pharmacy_link btn btn-xs btn-info"/>
                    </form>

                    <hr>

                </div>
            </div>
        </div>
    </div>


    <div class="box-content patient_medical_records" id="patient_medical_records" style="display: none">
        <div class="well">
            <a href="<?php echo base_url() ?>reports/patient_lab_history/<?php echo $this->uri->segment(3); ?>" id="" class=" btn btn-info "><i class="glyphicon icon-white glyphicon-align-justify"></i> Lab Records</a>
            <hr>
            <a href="<?php echo base_url() ?>reports/patient_consultation_history/<?php echo $this->uri->segment(3); ?>" class="  btn btn-info" id="send_to_doctor_link">
                <i class="glyphicon glyphicon-align-justify icon-white"></i>
                Consultation Records 
            </a>
            <hr>
            <a href="<?php echo base_url() ?>reports/patient_prescription_history/<?php echo $this->uri->segment(3); ?>" class="btn btn-info"><i class="glyphicon icon-white glyphicon-align-justify"> Prescription Records </i></a>

        </div>
    </div>



    <!--/span-->
    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-edit"></i> Patient Triage Report</h2>

                    <div class="box-icon">

                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                class="glyphicon glyphicon-chevron-up"></i></a>

                    </div>
                </div>
                <div class="box-content">

                    <!--/span-->

                    <a  id="export_triage_patient_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Walkin Patient Report" class="export_triage_patient_report_filter_link" href="#export_triage_report_filter_form">
                        <i class="glyphicon glyphicon-download-alt"></i>
                    </a>
                    |
                    <a  id="add_triage_link" data-toggle="tooltip" data-placement="top" data-original-title="Add Triage " class="add_triage_link" href="#add_triage_form">
                        Add Triage <i class="glyphicon glyphicon-plus"></i>
                    </a>


                    <table class="table table-striped table-bordered triage_report_1  responsive">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Date </th>
                                <th>Nurse Name</th>
                                <th>Respiratory (No/Min)</th>
                                <th>Pulse Rate (No/Min)</th>
                                <th>Blood pressure (mmHg)</th>
                                <th>Temperature (°C)</th>
                                <th>Height (Cm)</th>
                                <th>Weight (KGs)</th>
                                <th>BMI</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                $i = 1;
                                foreach ($patient_triage_report as $value) {
                                    ?>
                                    <td class="center"><?php echo $i; ?></td>
                                    <td class="center">
                                        <?php
                                        echo $value['visit_date'];
                                        ?>
                                    </td>
                                    <td class="center">
                                        <?php
                                        echo $value['employee_name'];
                                        ?>
                                    </td>
                                    <td class="center">
                                        <?php
                                        echo $value['respiratory_rate'];
                                        ?>
                                    </td>
                                    <td class="center">
                                        <?php echo $value['pulse_rate']; ?>
                                    </td>
                                    <td class="center">
                                        <?php echo $value['diastolic'] . "/" . $value['systolic']; ?>
                                    </td>
                                    <td class="center">
                                        <?php echo $value['temperature']; ?>
                                    </td>
                                    <td class="center">
                                        <?php echo $value['height']; ?>
                                    </td>
                                    <td class="center">
                                        <?php echo $value['weight']; ?>
                                    </td>

                                    <td class="center" ><?php
                                        $weight = $value['weight'];
                                        $height = $value['height'];
                                        $one_hundred = 100;
                                        $height = $height / $one_hundred;
                                        $new_height = $height * $height;


                                        if ($new_height == 0) {
                                            ?>
                                            <font color=red class="label label-danger">Division by error : B.M.I Cannot be equals to zero </font>
                                            <?php
                                        } else {
                                            $BMI = $weight / $new_height;
                                            if ($BMI <= 20) {
                                                ?>
                                                <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                            <?php } ?>

                                            <?php if ($BMI >= 25) { ?>
                                                <font color=red class="label label-danger"><?php echo round($BMI, 2) ?> </font> 
                                            <?php } ?>

                                            <?php if ($BMI > 20 && $BMI < 25) { ?>
                                                <font color=green class="label label-success"><?php echo round($BMI, 2) ?> </font> 
                                                <?php
                                            }
                                        }
                                        ?></td> 



                                    <td class="center">
                                        <input type="hidden" name="view_triage_patient_id" class="view_triage_patient_id" id="view_triage_patient_id" value="<?php echo $value['patient_id']; ?>"/>
                                        <input type="hidden" name="view_triage_id" class="view_triage_id" id="view_triage_id" value="<?php echo $value['triage_id']; ?>"/>
                                        <input type="hidden" name="view_triage_visit_id" class="view_triage_visit_id" id="view_triage_visit_id" value="<?php echo $value['visit_id']; ?>"/>
                                        <a  id="edit_triage_patient_link" class="edit_triage_patient_link" href="#edit_triage_form">
                                            <i class="glyphicon glyphicon-edit icon-white"></i>

                                        </a>|
                                        <a  id="delete_triage_patient_link" class="delete_triage_patient_link" href="#delete_triage_form">
                                            <i class="glyphicon glyphicon-trash icon-white"></i>

                                        </a>
                                    </td>
                                </tr>

                                <?php
                                $i++;
                            }
                            ?>




                        </tbody>
                    </table>



                </div>
            </div>
        </div>
        <!--/span-->



    </div>







    <div class="send_to_doctor" id="send_to_doctor" style="display: none;">



        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class="box-header well">


                        <div class="box-icon">

                        </div>
                    </div>
                    <div class = "box-content">

                        <div class = "bs-example">


                            <form class = "send_to_doctor_form " autocomplete="off"   id = "send_to_doctor_form" role = "form">

                                <input type="hidden" id="send_to_doctor_visit_id" value="<?php echo $this->uri->segment(4); ?>" class="send_to_doctor_visit_id" name="send_to_doctor_visit_id"/>
                                <input type="hidden" id="send_to_doctor_patient_id" value="<?php echo $this->uri->segment(3); ?>" class="send_to_doctor_patient_id" name="send_to_doctor_patient_id"/>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <span class="label label-default">Patient Name : </span>
                                        <input type="text" readonly="" id="send_to_doctor_patient_name" class="send_to_doctor_patient_name uneditable-input form-control "/>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="label label-default">Active Doctors :</label>
                                        <select name="send_to_doctor_name" required="" class="send_to_doctor_name form-control" id="send_to_doctor_name">
                                            <option>Please select Doctor's Name :</option>
                                        </select>
                                    </div>

                                    <hr>
                                    <div class = "form-group">

                                        <input type="submit" class="btn btn-small btn-success btn-xs send_to_doctor_button" id="send_to_doctor_button" value="Send tO Doctor"/>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>







    <div class="delete_triage_form" id="delete_triage_form" style="display: none;">



        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class="box-header well">


                        <div class="box-icon">

                        </div>
                    </div>
                    <div class = "box-content">

                        <div class = "bs-example">


                            <form class = "delete_triage_information_form " autocomplete="off"   id = "delete_tri_information_form" role = "form">


                                <input type="hidden" id="input_triage_id_delete" name="input_triage_id_delete" class="input_triage_id_delete"/>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <h5 class = "" for = "">Are you sure you want to Delete the Triage details? </h5>

                                    </div>

                                    <br>
                                    <div class = "form-group">

                                        <input type="submit" class="btn btn-small btn-success delete_triage_record_yes" value="Yes"/>
                                        <input type="reset" class="btn btn-close delete_triage_record_no" value="No"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>





    <!--/span-->




    <div class="nurse_patient_list_div" id="nurse_patient_list_div" style="display: none;">


        <div class = "row">
            <div class = "box col-md-12">
                <div class = "box-inner">
                    <div class = "box-header well">
                        <h2><i class = "glyphicon glyphicon-info-sign"></i> Nurse </h2>

                        <div class = "box-icon">

                            <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                    class = "glyphicon glyphicon-chevron-up"></i></a>

                        </div>
                    </div>
                    <div class = "box-content row">


                        <div class = "box col-md-4">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "">
                                    <h2><i class = "glyphicon glyphicon-list"></i> Regular Patients In Tray</h2>

                                    <div class = "box-icon">

                                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                class = "glyphicon glyphicon-chevron-up"></i></a>

                                    </div>
                                </div>
                                <div class = "box-content">
                                    <ul class = "dashboard-list regular_patients_in_tray" id="regular_patients_in_tray">

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--/span-->





                        <div class = "box col-md-4">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "">
                                    <h2><i class = "glyphicon glyphicon-list"></i> Walk-in Patients In Tray</h2>

                                    <div class = "box-icon">

                                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                class = "glyphicon glyphicon-chevron-up"></i></a>

                                    </div>
                                </div>
                                <div class = "box-content">
                                    <ul class = "dashboard-list walkin_patients_in_tray" id="walkin_patients_in_tray">


                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--/span-->


                        <!--Appointments Start-->
                        <div class = "box col-md-4">
                            <div class = "box-inner">
                                <div class = "box-header well" data-original-title = "">
                                    <h2><i class = "glyphicon glyphicon-list"></i> Appointments Today</h2>

                                    <div class = "box-icon">

                                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                                class = "glyphicon glyphicon-chevron-up"></i></a>

                                    </div>
                                </div>
                                <div class = "box-content">
                                    <ul class = "dashboard-list appointments_today" id="appointments_today">


                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--Appointments End-->



                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

