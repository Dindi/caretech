<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Reports</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Procedure Report</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <a  id="export_patient_procedure_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Patient Procedure Report" class="export_patient_procedure_report_filter_link" href="#export_procedure_report_filter_form">
                    <i class="glyphicon glyphicon-download-alt"></i>

                </a>
                <table class="table table-striped table-bordered procedure_reportss responsive">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Visit Date</th>
                            <th>Patient Name</th>

                            <th>Phone No</th>
                            <th>Patient Type</th>
                            <th>Service Offered</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $i = 1;
                            foreach ($procedures as $value) {
                                ?>
                                <td class="center"><?php echo $i; ?></td>
                                <td class="center">
                                    <?php
                                    echo $value['date_added'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['patient_name'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['patient_phone'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['patient_type']; ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['procedure_name'];
                                    ?>
                                </td>




                                <td class="center">
                                    <input type="hidden" name="view_procedure_id" class="view_procedure_id" id="view_procedure_id" value="<?php echo $value['procedure_visit_id']; ?>"/>
                                    <a  id="view_procedure_link" class="view_procedure_link" href="#view_procedure_form">
                                        <i class="glyphicon glyphicon-zoom-in icon-white"></i>

                                    </a>|
                                    <a  id="edit_procedure_link" class="edit_procedure_link" href="#edit_procedure_form">
                                        <i class="glyphicon glyphicon-edit icon-white"></i>

                                    </a>|
                                    <a  id="delete_procedure_link" class="delete_procedure_link" href="#delete_procedure_form">
                                        <i class="glyphicon glyphicon-trash icon-white"></i>

                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>




                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->


<div id="view_procedure_form" style="display: none;">


    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Patient Procedure Form</h2>

                    <div class = "box-icon">
                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                class = "glyphicon glyphicon-chevron-up"></i></a>

                    </div>
                </div>



                <div class = "box-content">

                    <div class = "bs-example">

                        <form class = "view_procedure_visit_info " autocomplete="off" method = "post"  id = "view_procedure_visit_info" role = "form">

                            <input type="hidden" name="view_procedure_visit_id" class="view_procedure_visit_id" id="view_procedure_visit_id" />

                            <div class = "form-inline">

                                <div class = "form-group">
                                    <label class = "sr-only" for = "view_inputwalkinPatientName">Patient Name : </label>
                                    <input type = "text" readonly=""  class = "form-control view_inputPatientName" id = "view_inputPatientName" name="view_inputPatientName" placeholder = "Patient Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "view_inputPhoneNumber"> Phone Number : </label>
                                    <input type = "text" readonly="" class = "form-control view_inputPhoneNumber" id = "view_inputPhoneNumber" name="view_inputPhoneNumber" placeholder = "Patient Phone Number">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "view_procedure_name">Procedure Name : </label>
                                    <input type = "text" readonly=""  class = "form-control view_procedure_name" id = "view_procedure_name" name="view_procedure_name" placeholder = "Procedure Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "view_visit_date">Visit Date : </label>
                                    <input type = "text" readonly="" class = "form-control view_visit_date" id = "view_visit_date" name="view_visit_date" placeholder = "Visit Date">
                                </div>
                                <hr>

                            </div>

                            <div class = "form-inline">

                                <div class = "form-group">
                                    <label class = "control-label" for = "view_visit_status"> Procedure Visit Status :</label>
                                    <input name="view_visit_status" type="text" readonly="" class="form-control view_visit_status" id="view_visit_status" />

                                </div>

                                <hr>

                            </div>



                        </form>


                    </div>


                </div>
            </div>
            <!--/span-->

        </div><!--/row-->


    </div>

</div>


<div id="edit_procedure_form" style="display: none;">

    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Patient Procedure Form</h2>

                    <div class = "box-icon">
                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                class = "glyphicon glyphicon-chevron-up"></i></a>

                    </div>
                </div>



                <div class = "box-content">

                    <div class = "bs-example">


                        <form class = "edit_procedure_visit_info " autocomplete="off" method = "post"  id = "edit_procedure_visit_info" role = "form">

                            <input type="text" name="edit_procedure_visit_id" class="edit_procedure_visit_id" id="edit_procedure_visit_id" />

                            <div class = "form-inline">

                                <div class = "form-group">
                                    <label class = "sr-only" for = "edit_inputwalkinPatientName">Patient Name : </label>
                                    <input type = "text"  class = "form-control edit_inputPatientName" id = "edit_inputPatientName" name="edit_inputPatientName" placeholder = "Patient Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "edit_inputPhoneNumber"> Phone Number : </label>
                                    <input type = "text"  class = "form-control edit_inputPhoneNumber" id = "edit_inputPhoneNumber" name="edit_inputPhoneNumber" placeholder = "Patient Phone Number">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "edit_procedure_name">Procedure Name : </label>
                                    <select id="selectError" name="edit_procedure_name" id="edit_procedure_name" class="edit_procedure_name" required="" data-rel="">
                                        <option value="">Please select  Procedure Type : </option>

                                        <?php foreach ($procedure_name as $proceudre_type) { ?>
                                            <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "edit_visit_date">Visit Date : </label>
                                    <input type = "text" class = "form-control edit_visit_date" id = "edit_visit_date" name="edit_visit_date" placeholder = "Visit Date">
                                </div>
                                <hr>

                            </div>

                            <div class = "form-inline">

                                <div class = "form-group">
                                    <label class = "control-label" for = "edit_patient_type"> Patient Type :</label>
                                    <select name="edit_patient_type"  required="" class="selector edit_patient_type" id="edit_patient_type" >
                                        <option value="">Please select patient type  </option>
                                        <option value="Shwari">Shwari</option>
                                        <option value="Walkin">Walk-in </option>
                                    </select>   
                                </div>

                                <div class = "form-group">
                                    <label class = "control-label" for = "edit_visit_status"> Procedure Visit Status :</label>
                                    <select name="edit_visit_status"  required="" class="selector edit_visit_status" id="edit_visit_status" >
                                        <option value="">Please select status </option>
                                        <option value="Active">Active</option>
                                        <option value="In Active">In Active </option>
                                    </select>   
                                </div>
                                <hr>

                            </div>


                            <div class = "form-group">

                                <input type="submit" class="btn btn-small btn-success" value="Save Edit"/>
                                <input type="reset" class="btn btn-close btn-danger"/>
                            </div>
                        </form>


                    </div>


                </div>
            </div>
            <!--/span-->

        </div><!--/row-->


    </div>

</div>


<div id="delete_procedure_form" style="display: none;">
    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class="box-header well">


                    <div class="box-icon">

                    </div>
                </div>




                <div class = "box-content">

                    <div class = "bs-example">


                        <form class = "delete_procedure_information_form " autocomplete="off" method = "post"  id = "delete_procedure_information_form" role = "form">


                            <input type="hidden" id="input_procedure_patient_id_delete" name="input_procedure_patient_id_delete" class="input_procedure_patient_id_delete"/>
                            <div class = "form-inline">
                                <div class = "form-group">
                                    <h5 class = "" for = "">Are you sure you want to Delete the details? </h5>

                                </div>

                                <br>
                                <div class = "form-group">

                                    <input type="submit" class="btn btn-small btn-success delete_patient_record_yes" value="Yes"/>
                                    <input type="reset" class="btn btn-close delete_patient_record_no" value="No"/>
                                </div>
                            </div>



                        </form>

                    </div>
                </div>


            </div>
        </div>
    </div>
</div>




<div id="export_procedure_report_filter_form" style="display: none;">


    <style type = "text/css">
        .bs-example{
            margin: 20px;
        }
    </style>


    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Procedure Export Report Filter</h2>


                </div>
                <div class="box-content">
                    <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/export_procedure_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                        <div class="control-group">
                            <label class="control-label" for="selectError">Date From :  </label>

                            <div class="controls">
                                <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="visitation_date_from" class="date_from form-control input-sm"/>
                            </div>
                        </div>

                        <hr>
                        <div class="control-group">
                            <label class="control-label" for="selectError">Date To : </label>

                            <div class="controls">
                                <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="visitation_date_to"/>
                            </div>
                        </div>
                        <hr>


                        <div class="control-group">
                            <label class="control-label" for="selectError">Patient Name : </label>

                            <div class="controls">
                                <select id="selectError" name="patient_name" class="patient_name" required="" data-rel="">
                                    <option value="">Please select  Status : </option>
                                    <option value="All" >All</option>
                                    <?php foreach ($name as $value) {
                                        ?>
                                        <option value="<?php echo $value['patient_id']; ?>"><?php echo $value['title'] . ':' . $value['f_name'] . $value['s_name'] . $value['other_name']; ?></option><?php }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="control-group">
                            <label class="control-label" for="selectError">Procedure  Type  : </label>

                            <div class="controls">
                                <select id="selectError" name="procedure_type" id="procedure_type" required="" data-rel="">
                                    <option value="">Please select  Package Type : </option>
                                    <option value="All" >All</option>
                                    <?php foreach ($procedure_name as $proceudre_type) { ?>
                                        <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <hr>

                        <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                        <hr>
                    </form>

                </div>
            </div>
        </div>
        <!--/span-->
    </div>

</div>