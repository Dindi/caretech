<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Walkin Patient</a>
        </li>
    </ul>
</div>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Book Patient Visit</h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form role="form" method="post" id="add_walkin_patient_form" autocomplete="off" class="form-inline add_visit_form add_walkin_patient_form">
                    <div class="control-group">
                        <label class="control-label" for="selectError">Patient Name (required) </label>

                        <div class="controls">
                            <input type="text" name="patientname" id="patientname" class="patientname form-control input-sm"/>
                        </div>
                    </div>

                    <hr>
                    <div class="control-group">
                        <label class="control-label" for="selectError">Phone Number :  (required)</label>

                        <div class="controls">
                            <input type="text" name="patient_phone" class="form-control input-sm"  id="patient_phone"/>
                        </div>

                    </div>
                    <hr>


                    <div class="control-group">
                        <label class="control-label" for="selectError">Department  (required)</label>

                        <div class="controls">
                            <select id="selectError" name="department" required="" data-rel="chosen">
                                <option value="">Please select  Queue to join : </option>

                                <?php foreach ($queue_name as $name_types) { ?>
                                    <option  value="<?php echo $name_types['queue_name'] ?>" id="<?php echo $name_types['queue_name'] ?>" ><?php echo $name_types['queue_name'] ?>  </option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>


                    <hr>



                    <hr>

                    <input type="submit" class=" btn btn-info add_walkin_patient_button" id="add_walkin_patient_button" value="Add Visit"/>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<!-- content ends -->
</div><!--/#content.col-md-0-->
</div><!--/fluid-row-->






<hr>