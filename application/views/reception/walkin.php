<?php
$this->load->view('header');
?>
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Walkin Patient</a>
        </li>
    </ul>
</div>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Book Patient Visit</h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form role="form" method="post" id="add_walkin_patient_form" class="form-inline add_visit_form add_walkin_patient_form">
                    <div class="control-group">
                        <label class="control-label" for="selectError">Patient Name (required) </label>

                        <div class="controls">
                            <input type="text" name="patientname" id="patientname" class="patientname form-control input-sm"/>
                        </div>
                    </div>

                    <hr>
                    <div class="control-group">
                        <label class="control-label" for="selectError">Phone Number :  (required)</label>

                        <div class="controls">
                            <input type="text" name="patient_phone" class="form-control input-sm"  id="patient_phone"/>
                        </div>

                        <hr>


                        <div class="control-group">
                            <label class="control-label" for="selectError">Department  (required)</label>

                            <div class="controls">
                                <select id="selectError" name="department" required="" data-rel="chosen">
                                    <option value="">Please select  Queue to join : </option>

                                    <?php foreach ($queue_name as $name_types) { ?>
                                        <option  value="<?php echo $name_types['queue_name'] ?>" id="<?php echo $name_types['queue_name'] ?>" ><?php echo $name_types['queue_name'] ?>  </option>
                                    <?php } ?>

                                </select>
                            </div>
                        </div>


                        <hr>



                        <hr>

                        <input type="submit" class=" btn btn-info add_walkin_patient_button" id="add_walkin_patient_button" value="Add Visit"/>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<!-- content ends -->
</div><!--/#content.col-md-0-->
</div><!--/fluid-row-->


<div id = "patient_option" style = "display: none;">


    <div class = "box col-md-12">
        <div class = "box-inner">
            <div class = "box-header well" data-original-title = "">
                <h2><i class = "glyphicon glyphicon-edit"></i> </h2>

                <div class = "box-icon">

                </div>
            </div>

            <div class = "box-content">
                <label class = "">
                    Are you related to any Shwari Member ?
                </label><br>

                <a id = "shwari_relation_yes" class = "btn btn-info btn-sm shwari_relation_yes" href = "#shwari_relation_yes_form">
                    Yes
                </a>

                <a id = "shwari_relation_no" href = "#shwari_relation_no_form" class = "btn btn-info btn-sm shwari_relation_no">
                    No
                </a>
            </div>
        </div>
    </div>
    <!--/span-->


</div>






<div class = "form-control" id = "shwari_relation_yes_form" style = "display: none;">


    <style type = "text/css">
        .bs-example{
            margin: 20px;
        }
    </style>


    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Shwari Member Registration</h2>


                </div>
                <div class = "box-content">

                    <div class = "bs-example">
                        <form class = "" method = "post" action = "visit.html" id = "patient_registration_form" role = "form">
                            <div class="form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputShwariFamilyNumber">Shwari Family Number :</label>
                                    <input type = "text" class = "form-control" id ="inputShwariFamilyNumber" placeholder = "Shwari Family Number">
                                </div>

                                <div class = "form-group">
                                    <div><label>Family Number Result : </label></div>
                                    <div id="shwari_family_number_result" name="shwari_family_number_result" class="shwari_family_number_result">

                                    </div>
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "family_number"> Family Number :</label>
                                    <input type = "text" class = "form-control" name="family_number" id ="family_number" placeholder = "Please paste the  family number : ">
                                </div>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputFirstName">First Name : </label>
                                        <input type = "text" class = "form-control" id = "inputFirstName" name="fname" placeholder = "First Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputSurName">Sur Name : </label>
                                        <input type = "text" class = "form-control" id = "inputSurName" name="sname" placeholder = "Sur Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputOtherName">Other Name : </label>
                                        <input type = "text" class = "form-control" id = "inputOtherName" name="lname" placeholder = "Other Name">
                                    </div>

                                </div>

                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputDateofBirth">Date of Birth</label>
                                        <input type = "text" class = "form-control" name="dob" id = "datepicker" placeholder = "D.O.B">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputSurName">National/Military/Alien ID or Passport No</label>
                                        <input type = "text" class = "form-control" name="nationalid" id = "inputSurName" placeholder = "National ID ">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputPostalAddress">Postal Address</label>
                                        <input type = "email" class = "form-control" name="address" id = "inputPostalAddress" placeholder = "Postal Address">
                                    </div>
                                </div>


                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputResidence">Residence</label>
                                        <input type = "text" class = "form-control" name="residence" id = "inputResidence" placeholder = "Residence">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputCity">City</label>
                                        <input type = "text" class = "form-control" name="city" id = "inputCity" placeholder = "City">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputPhoneNo">Phone No</label>
                                        <input type = "text" class = "form-control" name="phone_no" id = "inputPhoneNo" placeholder = "Phone No">
                                    </div>
                                </div>


                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputEmail">Email : </label>
                                        <input type = "email" class = "form-control" name="email" id ="inputEmail" name="" placeholder = "Email">
                                    </div>


                                </div>


                                <div class = "form-inline">
                                    <div class = "control-group">
                                        <label class = "control-label" for = "inputGender">Gender</label>
                                        <div class = "controls">
                                            <select class = "selectError" name="sex" data-rel = "" >
                                                <option value = "Male">Male</option>
                                                <option value = "Female">Female</option>
                                                <option value = "Trans-gender">Trans-gender</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class = "control-group">
                                        <label class = "control-label" for = "inputMaritalStatus">Marital Status</label>
                                        <div class = "controls">
                                            <select class = "selectError" name="maritalstatus" data-rel = "">
                                                <option value = "Single">Single</option>
                                                <option value = "Married">Married</option>
                                                <option value = "Divorced">Divorced</option>
                                                <option value = "Widowed">Widowed</option>
                                            </select>
                                        </div>
                                    </div>


                                </div>



                                <label>Next of Kin Details </label>
                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputFirstName">First Name</label>
                                        <input type = "text" class = "form-control" id = "inputFirstName" name="kinname" placeholder = "First Name">
                                    </div><div class = "form-group">
                                        <label class = "sr-only" for = "inputLastName">Last Name</label>
                                        <input type = "text" class = "form-control" id = "inputLastName" name="kinsname" placeholder = "Last Name">
                                    </div>
                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputMiddleName">Middle Name</label>
                                        <input type = "text" class = "form-control" id = "inputMiddleName" placeholder = "Middle Name">
                                    </div>
                                </div>


                                <div class = "form-inline">

                                    <div class = "control-group">
                                        <label class = "control-label" for = "inputkinrelation"> Relation Type : </label>
                                        <div class = "controls">
                                            <select class = "selectError" name="kinrelation" data-rel = "">
                                                <option value = "Sibling">Sibling</option>
                                                <option value = "Father">Father</option>
                                                <option value = "Mother">Mother</option>
                                                <option value = "Husband">Husband</option>
                                                <option value = "Wife">Wife</option>
                                                <option value = "Aunt">Aunt</option>
                                                <option value="Uncle">Uncle</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class = "form-group">
                                        <label class = "sr-only" for = "inputPhone">Kin's Phone No</label>
                                        <input type="text" placeholder="Kin's Phone"  class = "form-control"  id="kinphone" name="kinphone" size="30" />
                                    </div>

                                </div>



                                <div class = "form-inline">
                                    <div class = "form-group">
                                        <input type = "submit" value = "Submit" class = "btn btn-success" id = "add_register_new_patient"/>
                                        <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset"/>
                                    </div>
                                </div>




                        </form>
                    </div>
                </div>






            </div>
        </div>
    </div>
    <!--/span-->
</div>
</div><!--/row-->











<div class = "form-control" id = "shwari_relation_no_form" style = "display: none;">
    Shwari Relation No form...



    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Form Elements</h2>

                    <div class = "box-icon">
                        <a href = "#" class = "btn btn-setting btn-round btn-default"><i
                                class = "glyphicon glyphicon-cog"></i></a>
                        <a href = "#" class = "btn btn-minimize btn-round btn-default"><i
                                class = "glyphicon glyphicon-chevron-up"></i></a>
                        <a href = "#" class = "btn btn-close btn-round btn-default"><i
                                class = "glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class = "box-content">

                    <div class = "bs-example">
                        <form class = "" method = "post" action = "visit.html" id = "patient_registration_form_1" role = "form">

                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputFirstName">First Name</label>
                                    <input type = "text" class = "form-control" id = "inputFirstName" placeholder = "First Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputSurName">Sur Name</label>
                                    <input type = "text" class = "form-control" id = "inputSurName" placeholder = "Sur Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputEmail">Email</label>
                                    <input type = "email" class = "form-control" id = "inputEmail" placeholder = "Email">
                                </div>
                            </div>

                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputDateofBirth">Date of Birth</label>
                                    <input type = "text" class = "form-control" id = "inputDateofBirth" placeholder = "D.O.B">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputSurName">National/Military/Alien ID or Passport No</label>
                                    <input type = "text" class = "form-control" id = "inputSurName" placeholder = "National/Military/Alliend ID or Passport No">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputPostalAddress">Postal Address</label>
                                    <input type = "email" class = "form-control" id = "inputPostalAddress" placeholder = "Postal Address">
                                </div>
                            </div>


                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputResidence">Residence</label>
                                    <input type = "text" class = "form-control" id = "inputResidence" placeholder = "Residence">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputCity">City</label>
                                    <input type = "text" class = "form-control" id = "inputCity" placeholder = "City">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputPhoneNo">Phone No</label>
                                    <input type = "text" class = "form-control" id = "inputPhoneNo" placeholder = "Phone No">
                                </div>
                            </div>


                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputContactName">Emergency Contact Name</label>
                                    <input type = "text" class = "form-control" id = "inputContactName" placeholder = "Emergency Contact Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputEmergencyContactNo">Emergency Contact No</label>
                                    <input type = "text" class = "form-control" id = "inputEmergencyContactNo" placeholder = "Emergency Contanct No">
                                </div>

                            </div>


                            <div class = "form-inline">
                                <div class = "control-group">
                                    <label class = "control-label" for = "inputGender">Gender</label>
                                    <div class = "controls">
                                        <select class = "" data-rel = "">
                                            <option value = "Male">Male</option>
                                            <option value = "Female">Female</option>
                                            <option value = "Trans-gender">Trans-gender</option>
                                        </select>
                                    </div>
                                </div>
                                <div class = "control-group">
                                    <label class = "control-label" for = "inputMaritalStatus">Marital Status</label>
                                    <div class = "controls">
                                        <select class = "" data-rel = "">
                                            <option value = "Single">Single</option>
                                            <option value = "Married">Married</option>
                                            <option value = "Divorced">Divorced</option>
                                            <option value = "Widowed">Widowed</option>
                                        </select>
                                    </div>
                                </div>

                            </div>



                            <label>If patient is under 18, please complete the responsible party’s or guardian information below </label>
                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputFirstName">First Name</label>
                                    <input type = "text" class = "form-control" id = "inputFirstName" placeholder = "First Name">
                                </div><div class = "form-group">
                                    <label class = "sr-only" for = "inputLastName">Last Name</label>
                                    <input type = "text" class = "form-control" id = "inputLastName" placeholder = "Last Name">
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputMiddleName">Middle Name</label>
                                    <input type = "text" class = "form-control" id = "inputMiddleName" placeholder = "Middle Name">
                                </div>
                            </div>


                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputDateofBirth">Date of Birth</label>
                                    <input type = "text" class = "form-control" id = "inputDateofBirth" placeholder = "Date of Birth">
                                </div><div class = "form-group">
                                    <label class = "sr-only" for = "inputNationalID">National ID/Passport No :</label>
                                    <input type = "text" class = "form-control" id = "inputLastName" placeholder = "Last Name">
                                </div>

                            </div>

                            <div class = "form-inline">
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputAddress">Address(if different from above )</label>
                                    <textarea class = "form-control" id = "inputAddress" placeholder = "Address(If different from above :)"></textarea>
                                </div>
                                <div class = "form-group">
                                    <label class = "sr-only" for = "inputPhone">Phone No(if different from above )</label>
                                    <textarea class = "form-control" id = "inputPhone" placeholder = "Phone No(If different from above :)"></textarea>
                                </div>
                            </div>

                            <div class = "form-inline">
                                <div class = "form-group">
                                    <input type = "submit" value = "Submit" class = "btn btn-success" id = "save_submit_1"/>
                                    <input type = "reset" value = "Reset" class = "btn btn-danger" id = "save_reset_1"/>
                                </div>
                            </div>



                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->

    </div><!--/row-->

</div>


<hr>

<?php $this->load->view('footer'); ?>