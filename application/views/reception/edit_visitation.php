<?php $this->load->view('header'); ?>
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Forms</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Form Elements</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                
                 <form method="POST" action="<?php echo base_url(); ?>reception/save_edit">
                     <input type="hidden" value="<?php echo $entry->visit_id; ?>" name="visit_id"/>
                     <input type="hidden" value="<?php echo $entry->patient_id; ?>" name="patient_id"/>
                    <div class="form-group has-success col-md-4">
                        <label class="control-label" for="">Patient Name</label>
                        <input type="text" readonly="" name="patient_name"  value="<?php echo $entry->patient_name; ?>"class="form-control" id="patient_name">
                    </div>
                     <br>
                    <div class="form-group has-warning col-md-4">
                        <label class="control-label" for="">Package Type</label>
                        <input type="text" class="form-control" id="package_type" value="<?php echo $entry->package_name; ?>" name="package_type">
                    </div>
                     <br>
                    <div class="form-group has-error col-md-4">
                        <label class="control-label" for="" style="width: 190px;">Pay at the End</label>
                        <select name="pay_at_the_end" >
                            <option value="<?php echo $entry->pay_at_the_end; ?>"><?php echo $entry->pay_at_the_end; ?> </option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                     <br>
                    <div class="form-group has-error col-md-4">
                        <label class="control-label" for="inputError1">Pay at the End</label>
                        <select name="urgency" >
                            <option value="<?php echo $entry->urgency; ?>"><?php echo $entry->urgency; ?> </option>
                            <option value="urgent">Urgent</option>
                            <option value="not urgent">Not Urgent</option>
                        </select>
                    </div>
                     <br>
                    <div class="form-group has-success col-md-4">
                        <label class="control-label" for="inputSuccess1">Nurse Queue </label>
                        <select name="nurse_queue" >
                            <option value="<?php echo $entry->nurse_queue; ?>"> <?php echo $entry->nurse_queue; ?></option>
                            <option value="active">Active</option>
                            <option value="in active">In Active</option>
                        </select>
                    </div>
                     <br>
                    <div class="form-group has-warning col-md-4">
                        <label class="control-label" for="inputWarning1">Lab Queue </label>
                        <select name="lab_queue" >
                            <option value="<?php echo $entry->lab_queue; ?>"> <?php echo $entry->lab_queue; ?></option>
                            <option value="active">Active</option>
                            <option value="in active">In Active</option>
                        </select>
                    </div>
                     <br>
                     
                    <div class="form-group has-error col-md-4">
                        <label class="control-label" for="inputError1">Doctor Queue </label>
                        <select name="doctor_queue" >
                            <option value="<?php echo $entry->doctor_queue; ?>"> <?php echo $entry->doctor_queue; ?></option>
                            <option value="active">Active</option>
                            <option value="in active">In Active</option>
                        </select>
                    </div>
                     <br>
                     
                    <div class="form-group has-error col-md-4">
                        <label class="control-label" for="inputError1">Pharmacy Queue </label>
                        <select name="pharm_queue" >
                            <option value="<?php echo $entry->pharm_queue; ?>"> <?php echo $entry->pharm_queue; ?></option>
                            <option value="active">Active</option>
                            <option value="in active">In Active</option>
                        </select>
                    </div>
                     <br>

                    <input type="submit" value="Update" />
                </form>


                




                
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



<?php $this->load->view('footer'); ?>

