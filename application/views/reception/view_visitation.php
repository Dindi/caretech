<?php $this->load->view('header'); ?>
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Forms</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Form Elements</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">

                <form method="POST" action="<?php echo base_url(); ?>reception/save_edit">
                    <input type="hidden" value="<?php echo $entry->visit_id; ?>" name="visit_id"/>
                    <input type="hidden" value="<?php echo $entry->patient_id; ?>" name="patient_id"/>
                    <div class="form-group has-success col-md-4">
                        <label class="control-label" for="">Patient Name</label>
                        <input type="text" readonly="" name="patient_name"  value="<?php echo $entry->patient_name; ?>"class="form-control" id="patient_name">
                    </div>
                    <br>
                    <div class="form-group has-warning col-md-4">
                        <label class="control-label" for="">Package Type</label>
                        <input type="text" class="form-control" id="package_type" value="<?php echo $entry->package_name; ?>" name="package_type">
                    </div>
                    <br>
                    <div class="form-group has-warning col-md-4">
                        <label class="control-label" for="">Nurse Queue</label>
                        <input type="text" class="form-control" id="package_type" value="<?php echo $entry->nurse_queue; ?>" name="package_type">
                    </div>  <br>
                    <div class="form-group has-warning col-md-4">
                        <label class="control-label" for="">Doctor Queue</label>
                        <input type="text" class="form-control" id="package_type" value="<?php echo $entry->doctor_queue; ?>" name="package_type">
                    </div>  <br>
                    <div class="form-group has-warning col-md-4">
                        <label class="control-label" for="">Pharmacy Queue</label>
                        <input type="text" class="form-control" id="package_type" value="<?php echo $entry->pharm_queue; ?>" name="package_type">
                    </div>  <br>


                </form>








            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



<?php $this->load->view('footer'); ?>

