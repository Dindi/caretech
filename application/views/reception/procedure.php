<?php
$this->load->view('header');
?>
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Procedure Patient</a>
        </li>
    </ul>
</div>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Book Patient for Procedure </h2>

                <div class="box-icon">

                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form role="form" method="post" id="book_patient_procedure_form" class="form-inline add_visit_form book_patient_procedure_form">
                    <div class="control-group">
                        <label class="control-label" for="selectError">Patient Name (required) </label>

                        <div class="controls">
                            <input type="text" name="patientname" id="patientname" class="patientname form-control input-sm"/>
                        </div>
                    </div>

                    <hr>
                    <div class="control-group">
                        <label class="control-label" for="selectError">Phone Number :  (required)</label>

                        <div class="controls">
                            <input type="text" name="patient_phone" class="form-control input-sm"  id="patient_phone"/>
                        </div>
                    </div>
                    <hr>


                    <div class="control-group">
                        <label class="control-label" for="selectError">Department  (required)</label>

                        <div class="controls">
                            <select id="selectError" name="procedure_name" required="" data-rel="chosen">
                                <option value="">Please select  Procedure : </option>

                                <?php foreach ($procedure_name as $proceudre_type) { ?>
                                    <option  value="<?php echo $proceudre_type['procedure_id'] ?>" id="<?php echo $proceudre_type['procedure_name'] ?>" ><?php echo $proceudre_type['procedure_name'] ?>  </option>
                                <?php } ?>

                            </select>

<!--<input type='text' name='location_search' id='procedure_name_1' placeholder='Type Prcedure' >-->
                        </div>
                    </div>


                    <hr>



                    <hr>

                    <input type="submit" class=" btn btn-info book_patient_procedure_button" id="book_patient_procedure_button" value="Book Patient"/>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<!-- content ends -->
</div><!--/#content.col-md-0-->
</div><!--/fluid-row-->



<hr>

<?php $this->load->view('footer'); ?>