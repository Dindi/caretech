<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Regular Patient Visit Report</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Regular Patient Visit Report</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                
                <table class="table table-striped table-bordered procedure_reportss datatable dataTables_processing dataTables_scroll responsive">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Visit Date</th>
                            <th>Patient Name</th>
                            <th>Amount </th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $i = 1;
                            foreach ($patient_statement as $value) {
                                ?>
                                <td class="center"><?php echo $i; ?></td>
                                <td class="center">
                                    <?php
                                    echo $value['visit_date'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['patient_name'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['total_payments_dr'];
                                    ?>
                                </td>







                                <td class="center">
                                    <input type="hidden" name="view_procedure_id" class="view_procedure_id" id="view_procedure_id" value="<?php echo $value['patient_id']; ?>"/>
                                    <a  id="view_procedure_link" class="view_procedure_link" href="<?php echo base_url(); ?>cashier/print_patient_receipt/<?php echo $value['visit_id']; ?>">
                                        <i class="glyphicon glyphicon-download-alt"></i>

                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>




                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->