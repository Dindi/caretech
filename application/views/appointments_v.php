<!DOCTYPE html>
<html>
    <head>
        <!-- <link href='fullcalendar/fullcalendar.css' rel='stylesheet' />
           <script src='fullcalendar/jquery-1.9.1.min.js'></script>
           <script src='fullcalendar/jquery-ui-1.10.2.custom.min.js'></script>
           <script src='fullcalendar/fullcalendar.min.js'></script> -->
        <link href='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
        <script src='<?php echo base_url(); ?>assets/fullcalendar/lib/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/fullcalendar/lib/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.min.js'></script>






        <script type="text/javascript">

            $(document).ready(function () {


                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: {
                        url: '<?php echo base_url(); ?>appointments/get_appointmentss',
                        error: function () {
                            $('#script-warning').show();
                        }
                    },
                    // Convert the allDay from string to boolean
                    eventRender: function (event) {

                        if (event.allDay === 'true') {
                            event.allDay = true;
                        } else {
                            event.allDay = false;
                        }

                    },
                    loading: function (bool) {
                        $('#loading').toggle(bool);
                    }
                });



            });

        </script>
        <style>

            body {
                margin: 0;
                padding: 0;
                font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
                font-size: 14px;
            }

            #script-warning {
                display: none;
                background: #eee;
                border-bottom: 1px solid #ddd;
                padding: 0 10px;
                line-height: 40px;
                text-align: center;
                font-weight: bold;
                font-size: 12px;
                color: red;
            }

            #loading {
                display: none;
                position: absolute;
                top: 10px;
                right: 10px;
            }

            #calendar {
                max-width: 900px;
                margin: 40px auto;
                padding: 0 10px;
            }

        </style>







    </head>
    <body>
        <div id='calendar'></div>
    </body>
</html>