<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Reports</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Stock Bin Card Report</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <a  id="export_stock_bin_card_report_filter_link" data-toggle="tooltip" data-placement="top" data-original-title="Export Walkin Patient Report" class="export_stock_bin_card_report_filter_link" href="#export_stock_bin_card_report_filter_form">
                    <i class="glyphicon glyphicon-download-alt"></i>

                </a>
                <table class="table table-striped table-bordered walkin_report  responsive">
                    <thead>
                        <tr>
                            <th>No :</th>
                            <th>Date :</th>
                            <th>Commodity Name :</th>
                            <th>Batch No :</th>
                            <th>Opening Balance :</th>
                            <th>Issues:</th>
                            <th>Closing Balance :</th>
                            <th>Description :</th>
                            <th>Identification Code :</th>
                            <th>Status :</th>
                            <th>Transaction Type:</th>
                            <th>Issuing Officer </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $i = 1;
                            foreach ($stock_bin_card as $value) {
                                ?>
                                <td class="center"><?php echo $i; ?></td>
                                <td class="center">
                                    <?php
                                    echo $value['date_added'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['commodity_name'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['batch_no'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['opening_bal']; ?>
                                </td>
                                <td class="center">
                                    <?php
                                    echo $value['issues'];
                                    ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['closing_bal']; ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['description']; ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['identification_code']; ?>
                                </td>
                                <td class="center">
                                    <?php
                                    $entry_status = $value['entry_status'];
                                    if ($entry_status === 'Active') {
                                        ?>
                                        <label class="label label-success"><?php echo $entry_status; ?></label>
                                        <?php
                                    } elseif ($entry_status === 'Cancelled') {
                                        ?>
                                        <label class="label label-danger"><?php echo $entry_status; ?></label>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['user_type']; ?>
                                </td>
                                <td class="center">
                                    <?php echo $value['employee_name']; ?>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>




                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->


</div><!--/row-->


<div id="export_walkin_report_filter_form" style="display: none;">


    <style type = "text/css">
        .bs-example{
            margin: 20px;
        }
    </style>


    <div class = "row">
        <div class = "box col-md-12">
            <div class = "box-inner">
                <div class = "box-header well" data-original-title = "">
                    <h2><i class = "glyphicon glyphicon-edit"></i> Care-tech Stock Bin Card Report Filter</h2>


                </div>
                <div class="box-content">
                    <form role="form" method="post" autocomplete="off" action="<?php echo base_url(); ?>reports/export_walkin_report" id="patient_report_filter" class="form-inline  patient_report_filter">
                        <div class="control-group">
                            <label class="control-label" for="selectError">Date From :  </label>

                            <div class="controls">
                                <input type="text" name="date_from" placeholder="YYYY-MM-DD" id="walkin_date_from" class="date_from form-control input-sm"/>
                            </div>
                        </div>

                        <hr>
                        <div class="control-group">
                            <label class="control-label" for="selectError">Date To : </label>

                            <div class="controls">
                                <input type="text" name="date_to" placeholder="YYYY-MM-DD" class=" date_to form-control input-sm"  id="walkin_date_to"/>
                            </div>
                        </div>
                        <hr>


                        <div class="control-group">
                            <label class="control-label" for="selectError">Department Name : </label>

                            <div class="controls">
                                <select id="selectError" name="department_name" class="department_name" required="" data-rel="">
                                    <option value="">Please select  Department : </option>
                                    <option value="All">All</option>
                                    <option value="Nursing">Nursing</option>
                                    <option value="Laboratory">Laboratory</option>
                                    <option value="Pharmacy">Pharmacy</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="control-group">
                            <label class="control-label" for="selectError">Payment Status  : </label>

                            <div class="controls">
                                <select id="selectError" name="payment_status" id="package_type" required="" data-rel="">
                                    <option value="">Please select  Payment Status : </option>
                                    <option value="All">All</option>
                                    <option value="Paid">Paid</option>
                                    <option value="Not Paid">Not Paid</option>
                                </select>
                            </div>
                        </div>

                        <hr>

                        <input type="submit" class=" btn btn-info patient_report_filter_button" id="patient_report_filter_button" value="Filter Patient"/>
                        <hr>
                    </form>

                </div>
            </div>
        </div>
        <!--/span-->
    </div>

</div>