<?php
$no_visible_elements = true;

//$this->load->view('header');
require 'header.php';
?>

<div class="row">
    <div class="col-md-12 center login-header">
        <h2>Welcome to Care-tech</h2>
    </div>
    <!--/span-->
</div><!--/row-->

<div class="row">
    <div class="well col-md-5 center login-box">

        <div class="alert alert-info">
            Password Reset . 
        </div>
        <?php if (!is_null($msg)) echo $msg; ?>
        <form class="form-horizontal" autocomplete="off" action="<?php echo base_url() ?>resetpassword/reset_password/<?php echo $this->uri->segment(3); ?>" method="post">
            <fieldset>
                <div class="input-group input-group-lg">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                <div class="clearfix"></div><br>

                <div class="input-group input-group-lg">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                    <input type="password" class="form-control" name="password1" placeholder="Re-Type Password">
                </div>
                <div class="clearfix"></div>


                <div class="clearfix"></div>

                <p class="center col-md-5">
                    <button type="submit" class="btn btn-primary">Reset Password</button>
                </p>

            </fieldset>
        </form>

        <hr>
        <p class="center col-md-5">
            <a href="<?php echo base_url(); ?>resetpassword" class="btn btn-primary btn-small">Forgot Password? </a>

        </p>


    </div>
    <!--/span-->
</div><!--/row-->
<?php
//$this->load->view('footer');
require 'footer.php';
?>