<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);
        // $this->output->cache(30);
        $this->check_isvalidated();
    }

    private function check_isvalidated() {
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
    }

    public function patient_bio() {
        $patient_id = $this->uri->segment(3);
        $sql = "Select concat(patient.title,':',patient.f_name,' ',patient.s_name,' ',patient.other_name) as patient_name,"
                . "patient.dob,patient.gender,patient.phone_no,patient.residence,patient.family_number from patient where patient.patient_id='$patient_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_doctors() {
        $sql = "Select DISTINCT employee.title , employee.employee_id, employee.user_name,
                employee.f_name,employee.l_name,employee.other_name, concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name,
                department.department_name from login_logs inner join
                employee on employee.employee_id = login_logs.employee_id 
                inner join department on department.department_id = employee.employment_category where 
                department.department_name='Doctor'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_patients() {

        $active = "Active";
        $query = $this->db->get_where('walkin', array('walkin_status' => $active));
        return $query->result_array();
    }

    public function get_packages() {
        $active = "Active";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = $this->db->get_where('packages', array('status' => $active, 'member_id' => $member_id, 'branch_id' => $branch_id));
        return $query->result_array();
    }

    public function get_patient_paymetn_id($visit_id) {
        $sql = "select * from patient_payments where visit_id ='$visit_id'";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {
            $patient_payments_id = $value->patient_payment_id;
            return $patient_payments_id;
        }
    }

    public function get_tests() {
        $active = "Active";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = $this->db->get_where('test', array('status' => $active, 'member_id' => $member_id, 'branch_id' => $branch_id));
        return $query->result_array();
    }

    public function get_frequency() {
        $active = "Active";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = $this->db->get_where('frequency', array('status' => $active, 'member_id' => $member_id, 'branch_id' => $branch_id));
        return $query->result_array();
    }

    public function get_duration() {
        $active = "Active";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = $this->db->get_where('duration', array('status' => $active, 'member_id' => $member_id, 'branch_id' => $branch_id));
        return $query->result_array();
    }

    public function get_route() {
        $active = "Active";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = $this->db->get_where('route', array('status' => $active, 'member_id' => $member_id, 'branch_id' => $branch_id));
        return $query->result_array();
    }

    public function get_commodity() {
        $active = "Active";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = $this->db->get_where('commodity', array('status' => $active, 'member_id' => $member_id, 'branch_id' => $branch_id));
        return $query->result_array();
    }

    public function get_supplier() {
        $active = "Active";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = $this->db->get_where('supplier', array('is_active' => $active, 'member_id' => $member_id, 'branch_id' => $branch_id));
        return $query->result_array();
    }

    public function get_insurance_providers() {
        $active = "Active";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = $this->db->get_where('insurance_providers', array('status' => $active, 'member_id' => $member_id, 'branch_id' => $branch_id));
        return $query->result_array();
    }

    public function get_department() {
        $active = "Active";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = $this->db->get_where('department', array('status' => $active, 'member_id' => $member_id, 'branch_id' => $branch_id));
        return $query->result_array();
    }

    public function get_ttl_regular_visit_revenue() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT sum(amount) as amount FROM `patient_visit_statement` where member_id='$member_id' and branch_id='$branch_id'";
        $query = $this->db->query();
        return $query->result_array();
    }

    public function get_ttl_walkin_visit_revenue() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT sum(amount) as amount FROM `walkin_patient_visit_statement` where member_id='$member_id' and branch_id='$branch_id'";
        $query = $this->db->query();
        return $query->result_array();
    }

    function limit() {
        $limit = 0;
        return $limit;
    }

    function offset() {
        $offset = 1;
        return $offset;
    }

    public function workload() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "select visit.visit_id , patient.patient_id, concat(employee.title,'',employee.f_name,'',employee.other_name,'',employee.l_name,'') as employee_name,"
                . "visit.visit_date, concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name) as patient_name , visit.doctor_start,"
                . " visit.doctor_end from visit inner join patient on patient.patient_id = visit.patient_id inner join employee on employee.employee_id = visit.doctor_id"
                . " where visit.visit_date >= CURDATE() and visit.member_id='$member_id' and visit.branch_id='$branch_id'";


        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function total_visits() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "select count(visit_id) as total_patient_visits from visit where visit_date >=CURDATE() and visit.member_id='$member_id' and visit.branch_id='$branch_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function total_walkin_patients() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "select count(walkin_id) as total_walkin_patient from walkin where walkin_date >=CURDATE() and walkin.member_id='$member_id' and walkin.branch_id='$branch_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_all_entries() {

        $sql = "select visit.family_number as family_number, visit.visit_date as visit_date,visit.visit_id as visit_id , packages.package_name as package_name, concat(patient.title,'',patient.f_name,' ',patient.s_name,' ',' ',patient.other_name) as patient_name from visit"
                . " inner join packages on packages.package_id=visit.package_type "
                . "inner join patient on patient.patient_id= visit.patient_id order by visit.visit_date desc";
        $query = $this->db->query($sql);

        return $query->result();
    }

    public function title() {

        $sql = "SELECT * FROM `title` order by title_name ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function active_nurses() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT DISTINCT employee.employee_id,employee.user_name,login_logs.is_active , department.department_name
                 FROM employee
                 INNER JOIN login_logs
               ON employee.employee_id = login_logs.employee_id
               INNER JOIN department 
               ON employee.employment_category = department.department_id
                 WHERE login_logs.is_active = 'Active' 
                 AND department.department_name='Nurse' and login_logs.member_id = '$member_id' and login_logs.branch_id='$branch_id'
                 ORDER BY (employee.employee_id)asc";
        $result = $this->db->query($sql);
        $active_nurse = $result->result_array();
        return $active_nurse;
    }

    public function active_doctors() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT DISTINCT employee.employee_id,employee.user_name,login_logs.is_active , department.department_name
                 FROM employee
                 INNER JOIN login_logs
               ON employee.employee_id = login_logs.employee_id
               INNER JOIN department 
               ON employee.employment_category = department.department_id
                 WHERE login_logs.is_active = 'Active' and login_logs.member_id='$member_id' and login_logs.branch_id='$branch_id'
                 AND department.department_name='Doctor'
                 ORDER BY (employee.employee_id)asc";
        $result = $this->db->query($sql);
        $active_doctor = $result->result_array();
        return $active_doctor;
    }

    public function active_pharmacist() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT DISTINCT employee.employee_id,employee.user_name,login_logs.is_active , department.department_name
                 FROM employee
                 INNER JOIN login_logs
               ON employee.employee_id = login_logs.employee_id
               INNER JOIN department 
               ON employee.employment_category = department.department_id
                 WHERE login_logs.is_active = 'Active' and login_logs.member_id='$member_id' and login_logs.branch_id='$branch_id'
                 AND department.department_name='Pharmacy'
                 ORDER BY (employee.employee_id)asc";
        $result = $this->db->query($sql);
        $active_pharmacy = $result->result_array();
        return $active_pharmacy;
    }

    public function active_laboratories() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT DISTINCT employee.employee_id,employee.user_name,login_logs.is_active , department.department_name
                 FROM employee
                 INNER JOIN login_logs
               ON employee.employee_id = login_logs.employee_id
               INNER JOIN department 
               ON employee.employment_category = department.department_id
                 WHERE login_logs.is_active = 'Active' and login_logs.member_id='$member_id' and login_logs.branch_id='$branch_id'
                 AND department.department_name='Laboratory'
                 ORDER BY (employee.employee_id)asc";
        $result = $this->db->query($sql);
        $active_laboratories = $result->result_array();
        return $active_laboratories;
    }

    public function package() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT DISTINCT *
        FROM packages where member_id='$member_id' AND branch_id='$branch_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_patient_details($patient_id) {
        $this->db->trans_start();
        $sql = "Select concat(patient.title,'',patient.f_name,' ',patient.s_name,' ',' ',patient.other_name) as patient_name , patient.patient_id , dob, gender from patient where patient_id='$patient_id'";
        $query = $this->db->query($sql);
        $this->db->trans_complete();
        if ($this->db->trans_complete() === FALSE) {
            
        } else {
            return $query->result();
        }
    }

    public function getName() {
        $this->db->distinct();
        $query = $this->db->get('patient');

        return $query->result_array();
    }

    public function getProcedure() {
        $this->db->distinct();
        $query = $this->db->get('procedure_list');

        return $query->result_array();
    }

    public function allergy_records($patient_id) {
        $sql = "select concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name,allergy_id, patient_id, allergies,Medication,social_history,immunization,allergy.date_added,user_id,allergy.member_id,allergy.branch_id  from allergy inner join employee on employee.employee_id = allergy.user_id where patient_id='$patient_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function lab_records($patient_id) {
        $sql = "select concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name, 
 lab_test_result.date_added as date_added,lab_test_result.lab_test_id, concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name,'') as patient_name, test.test_name, 
 lab_test_result.test_results from lab_test_result 
 inner join employee on employee.employee_id = lab_test_result.lab_tech_id inner join patient on patient.patient_id = lab_test_result.patient_id inner join test on test.test_id = lab_test_result.test where patient.patient_id='$patient_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function get_tests_results($patient_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT * FROM lab_test_result inner join test on test.test_id = lab_test_result.test  where patient_id='$patient_id' and lab_test_result.member_id='$member_id' and lab_test_result.visit_type='Walk-in' and lab_test_result.branch_id='$branch_id' order by date_added ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function consultation_records($patient_id) {
        $sql = "select concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name, 
            concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name,'') as patient_name,consultation.consultation_id,
            consultation.complaints,consultation.medical_history,consultation.systematic_inquiry,
            consultation.examination_findings,consultation.final_diagnosis,consultation.working_diagnosis, consultation.obstetrics_gynaecology,
            consultation.history_of_presenting_illness,consultation.family_social_history,
            consultation.date from consultation 
            inner join employee on employee.employee_id = consultation.doctor_id 
            inner join patient on patient.patient_id = consultation.patient_id where patient.patient_id='$patient_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function bill_patient_prescription($patient_id, $visit_id) {
        $sql = "select prescription.prescription_id ,prescription.prescription_tracker ,prescription.consultation_id,prescription.visit_id , prescription.patient_id , concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name, 
            concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name,'') as patient_name,
            prescription.commodity_name,prescription.strength,prescription.route,prescription.prescription_id,
            prescription.frequency,prescription.duration,prescription.no_of_days,
            prescription.date ,prescription.billed from prescription 
            inner join employee on employee.employee_id = prescription.doctor_id 
            inner join patient on patient.patient_id = prescription.patient_id"
                . " where prescription.patient_id='$patient_id' and prescription.visit_id='$visit_id' and prescription.visit_type='Regular'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function bill_walkin_patient_prescription($patient_id, $visit_id) {
        $sql = "select prescription.prescription_id ,prescription.prescription_tracker ,prescription.consultation_id,prescription.visit_id , prescription.patient_id , concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name, 
            concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name,'') as patient_name,
            prescription.commodity_name,prescription.strength,prescription.route,prescription.prescription_id,
            prescription.frequency,prescription.duration,prescription.no_of_days,
            prescription.date ,prescription.billed from prescription 
            inner join employee on employee.employee_id = prescription.doctor_id 
            inner join patient on patient.patient_id = prescription.patient_id"
                . " where prescription.patient_id='$patient_id' and prescription.visit_id='$visit_id' and prescription.visit_type='Walk-in'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function prescription_records($patient_id) {
        $sql = "select prescription.prescription_id ,prescription.prescription_tracker ,prescription.consultation_id,prescription.visit_id , prescription.patient_id , concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name, 
            concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name,'') as patient_name,
            prescription.commodity_name,prescription.strength,prescription.route,prescription.prescription_id,
            prescription.frequency,prescription.duration,prescription.no_of_days,
            prescription.date ,prescription.billed from prescription 
            inner join employee on employee.employee_id = prescription.doctor_id 
            inner join patient on patient.patient_id = prescription.patient_id"
                . " where prescription.patient_id='$patient_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function patient_dispensing_data($visit_id) {
        $sql = "select * from patient_dispensing_view where visit_id='$visit_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function current_prescription_records($visit_id) {
        $sql = "select concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name, 
                concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name,'') as patient_name,
                prescription.commodity_name,prescription.strength,prescription.route,prescription.prescription_id,
                prescription.frequency,prescription.duration,prescription.no_of_days,
                prescription.date , prescription.billed from prescription 
                inner join employee on employee.employee_id = prescription.doctor_id 
                inner join patient on patient.patient_id = prescription.patient_id"
                . " where prescription.visit_id='$visit_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function patient_triage_records($patient_id) {
        $sql = "SELECT DISTINCT concat(employee.title,' ' ,employee.f_name,' ', employee.l_name) as employee_name, triage.triage_id,triage.nurse_id, triage.visit_id, visit.visit_date, visit.patient_id,triage.weight, triage.height, 
			triage.OCS, triage.diastolic, triage.systolic,triage.temperature,triage.respiratory_rate, triage.pulse_rate
            FROM triage
            INNER JOIN visit
            ON triage.visit_id=visit.visit_id
            INNER JOIN employee
            ON triage.nurse_id = employee.employee_id
            WHERE visit.patient_id = '" . $patient_id . "'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function patient_vital_signs($visit_id) {
        $sql = "SELECT DISTINCT concat(employee.title,' ' ,employee.f_name,' ', employee.l_name) as employee_name, triage.triage_id,triage.nurse_id, triage.visit_id, visit.visit_date, visit.patient_id,triage.weight, triage.height, 
			triage.OCS, triage.diastolic, triage.systolic,triage.temperature,triage.respiratory_rate, triage.pulse_rate
            FROM triage
            INNER JOIN visit
            ON triage.visit_id=visit.visit_id
            INNER JOIN employee
            ON triage.nurse_id = employee.employee_id
            WHERE visit.visit_id = '" . $visit_id . "'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function reason_for_visit($visit_id) {
        $sql = "SELECT DISTINCT concat(employee.title,' ' ,employee.f_name,' ', employee.l_name) as employee_name, triage.triage_id,triage.nurse_id, triage.visit_id, visit.visit_date, visit.patient_id,triage.weight, triage.height, 
			triage.OCS, triage.diastolic, triage.systolic,triage.temperature,triage.respiratory_rate,triage.visit_reason, triage.pulse_rate
            FROM triage
            INNER JOIN visit
            ON triage.visit_id=visit.visit_id
            INNER JOIN employee
            ON triage.nurse_id = employee.employee_id
            WHERE visit.visit_id = '" . $visit_id . "'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function icd_10_codes() {

        $sql = "Select concat(diagnosis_code,' : ',description) as icd_description , id   from consultation_icd10";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function cached_icd10() {
        /*
          Get information from database and cache the information */
        $this->load->driver('cache', array('adapter' => 'file'));

        $cacheID = "icd10";
        if (!$cache_data = $this->cache->get($cacheID)) {
            //ge information from the  database 
            $data['icd10_codes'] = $this->icd_10_codes();

            // Save into the cache for 5 minutes
            $this->cache->save($cacheID, $data['icd10_codes'], 86400);
            $cache_data = $data['icd10_codes'];
        }
        return $cache_data;
    }

    function route() {
        $sql = "SELECT * FROM `route`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function occurence() {
        $sql = "SELECT * FROM `occurence`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function frequency() {
        $sql = "SELECT * FROM `frequency`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function commodities() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT * FROM `commodity` where member_id='$member_id' and branch_id='$branch_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function suppliers() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT * FROM `supplier` where member_id='$member_id' and branch_id='$branch_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function commodity_type() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT * FROM `commodity_type` where member_id='$member_id' and branch_id='$branch_id' ORDER BY `id` ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function cached_commodity() {
        /*
          Get information from database and cache the information */
        $this->load->driver('cache', array('adapter' => 'file'));

        $cacheID = "commodities";
        if (!$cache_data = $this->cache->get($cacheID)) {
            //get information from the  database 
            $data['commodities'] = $this->commodity_management();

            // Save into the cache for 5 minutes
            $this->cache->save($cacheID, $data['commodities'], 21600);
            $cache_data = $data['commodities'];
        }
        return $cache_data;
    }

    public function procedure_list() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "select * from packages where type='Procedure' and member_id='$member_id' and branch_id='$branch_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function test_list() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "select * from test where test_type='Laboratory' and member_id='$member_id' and branch_id='$branch_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_procedure_results($patient_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "select id , visit_id,package_name as procedure_name, procedure_id,results as procedure_result ,date_added,procedure_result.status,payment_status , patient_id,procedure_result.member_id , procedure_result.branch_id"
                . " from procedure_result inner join packages on packages.package_id = procedure_result.procedure_id where patient_id='$patient_id' and procedure_result.member_id='$member_id' and procedure_result.branch_id='$branch_id' order by date_added ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    public function get_icd10_list() {
        $this->db->trans_start();
        $query = "select * from icd10_list";
        $query = $this->db->query($query);
             return $query->result_array();
        $this->db->trans_complete();
        if ($this->db->trans_status()===FALSE) {
                 return $query->result_array();
        }  else {
            return $query->result_array();
        }
    }

}
