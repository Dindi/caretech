<?php

class Resetpassword extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index($msg = NULL) {
        $data['msg'] = $msg;
        $this->load->view('resetpassword', $data);
    }

    function base_param($data) {
        $data['title'] = 'Reset Password';
        $this->load->view('template', $data);
    }

    public function forget() {
        if (isset($_GET['info'])) {
            $data['info'] = $_GET['info'];
        }
        if (isset($_GET['error'])) {
            $data['error'] = $_GET['error'];
        }

        $this->load->view('login_forget', $data);
    }

    public function doforget() {

        $email = $_POST['email_address'];
        $q = $this->db->query("select * from employee where email='" . $email . "'");
        if ($q->num_rows > 0) {
            $r = $q->result();
            $user = $r[0];

            $this->resetpassword($user);
        } else {

            $error = "<font color=red>The email id you entered does not exist in the  System </font>";
            $msg = $error;
            $this->index($msg);
        }
    }

    public function check_email_address() {
        
    }

    public function reset_p($msg = NULL) {
        $data1['msg'] = $msg;
        $random_key = $this->uri->segment(3);
        $sql = $this->db->query("Select * from employee where random_key='" . $random_key . "' and is_active='No'");
        if ($sql->num_rows > 0) {
            $result = $sql->result();
            $user = $result[0];

            $data['msg'] = $msg;


            $this->load->view('password_reset', $data);
        } else {

            $msg = '<font color=red>Invalid Activation Link, The link has either expired or does not exist in the system please try again later.</font><br />';
            $this->index($msg);
        }
    }

    public function reset_password() {

        $random_key = $this->uri->segment(3);
        $base_url = $this->config->item('base_url');
        //$random_key = $this->input->post('random_key');
        $password = $this->input->post('password');
        $password1 = $this->input->post('password1');
        if ($password === $password1) {
            $Enc_password = md5($password);
            $sql = $this->db->query("Select * from employee where random_key='" . $random_key . "' and is_active='No'");
            if ($sql->num_rows > 0) {
                $result = $sql->result();
                $user = $result[0];
                $forgotten_password = $user->password;
                if ($Enc_password === $forgotten_password) {
                    $error = "<font color=red>Sorry you cannot use a Password that was used previoously used in the system , Please click on the link below to try reset the password again" . '</font></br>';
                    echo $error . '</br>';
                    $notify = '<font color=red>Please click the link below to Try again:<a href="' . $base_url . 'resetpassword/reset_p/' . $random_key . '">Link </a></font>';
                    echo $notify . '</br>';
                } else {
                    $this->db->where('random_key', $user->random_key);
                    $this->db->update('employee', array('is_active' => 'Yes', 'password' => $Enc_password));
                    $notification = "<font color=green>Password Updated successfully, You can now access you account with the new Password</font>";
                    $msg = $notification;
                    $this->index($msg);
                }
            }
        } else {
            $error = "<font color=red>The password do not match please try again" . '</font></br>';
            $notify = '<font color=red>Please click the link below to go back to the previous page:<a href="' . $base_url . 'resetpassword/reset_p/' . $random_key . '">Link </a></font>';
            $error = $error . '<br>' . $notify;
            $msg = $error;
            $this->index($msg);
        }
    }

    private function resetpassword($user) {



        $base_url = $this->config->item('base_url');
        //set the random id length 
        $random_id_length = 10;
        $today = date("Y-m-d H:i:s");
        //generate a random id encrypt it and store it in $rnd_id 
        $rnd_id = crypt(uniqid($today, 1));
        //to remove any slashes that might have come 
        $rnd_id = strip_tags(stripslashes($rnd_id));

        //Removing any . or / and reversing the string 
        $rnd_id = str_replace(".", "", $rnd_id);
        $rnd_id = strrev(str_replace("/", "", $rnd_id));

        //finally I take the first 10 characters from the $rnd_id 
        $rnd_id = substr($rnd_id, 0, $random_id_length);


        date_default_timezone_set('Africa/Nairobi');
        $this->load->helper('string');
        $password = random_string('alnum', 16);
        $random_key = $rnd_id;

        $modtime = date("Y-m-d H:i:s", strtotime('+2 hours'));


        $this->db->where('employee_id', $user->employee_id);
        $this->db->update('employee', array('is_active' => 'No', 'random_key' => $random_key, 'expiry_date' => $modtime));
        $this->load->library('email');
        $this->email->from('no_reply@shwarihealthcare.co.ke', 'Shwari System Support');
        $this->email->to($user->email);
        $this->email->subject('Password Reset');

        $mail_body = '
                                                                           ---------------------
                                                                                Hey :' . $user->user_name . '!
                                                                                    <fieldset>
                                                                                We currently received a request for resetting your SHWARI  ACCOUNT Password. You can reset your SHWARI  Personal Account Password 
                                                                                through the link below:
                                                                                <hr>
                                                                                ------------------------
                                                                                Please click this link to activate your account:<a href="' . $base_url . 'resetpassword/reset_p/' . $random_key . '">Reset</a>
                                                                                

                                                                                ------------------------ ';




        $this->email->message($mail_body);
        $this->email->send();


        $notififaction = "<font color=green>An email has  been sent to the following account :.'$user->email;'  " . '</br>' . "Please follow the Instructions given to activate and reset your password!</font>";

        $msg = $notififaction;
        $this->index($msg);
    }

}

?>
