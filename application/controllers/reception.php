<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reception extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $data['pack'] = $this->package();
        $data['name'] = $this->getName();

        $data['procedure_name'] = $this->getProcedure();
        $data['patient_bio'] = $this->patient_bio();
        $data['commodities'] = $this->commodities();

        $data['pack'] = $this->package();

        $data['title'] = $this->reception_model->title();
        $data['title_name'] = $this->title();
        $data1['contents'] = 'reception';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function base_params($data) {
        $data['title'] = 'Receptionist';
        $this->load->view('reception_template', $data);
    }

    public function register_patient() {
        $data['pack'] = $this->package();
        $data['name'] = $this->getName();

        $data['procedure_name'] = $this->getProcedure();
        $data['patient_bio'] = $this->patient_bio();
        $data['commodities'] = $this->commodities();

        $data['pack'] = $this->package();

        $data['title'] = $this->reception_model->title();
        $data['title_name'] = $this->title();
        $data1['contents'] = 'register_patient';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function load_data() {
        $patients = $this->mactive->nactive();
        $this->config->set_item('compress_output', FALSE);
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    function total_visits() {
        $total_visit = $this->total_visits();
        $this->config->set_item('compress_output', FALSE);
        if (!empty($total_visit)) {
            if ($total_visit === 0) {
                $no_patients = 'No Regular Patients visits';
                echo json_encode($no_patients);
            } else {
                echo json_encode($total_visit);
            }
        } else {
            
        }
    }

    function total_walkin_patients() {
        $total_walkin_patients = $this->total_walkin_patients();
        $this->config->set_item('compress_output', FALSE);
        if (!empty($total_walkin_patients)) {
            if ($total_walkin_patients === 0) {
                $no_patients = 'No Walkin Patients';
                echo json_encode($no_patients);
            } else {
                echo json_encode($total_walkin_patients);
            }
        } else {
            
        }
    }

    public function d_active() {
        $patients = $this->reception_model->dactive();
        $this->config->set_item('compress_output', FALSE);
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function dactive() {//Doctor
        $doctor_id = $this->uri->segment(3);
        //echo 'Doctor ID'.$doctor_id;
        $query = "SELECT DISTINCT patient.f_name, employee.employee_id,visit.patient_id, patient.other_name, patient.s_name, visit.visit_date, visit.visit_id,visit.urgency, visit.results,employee.user_name,login_logs.is_active
		FROM patient
		INNER JOIN visit
		ON patient.patient_id=visit.patient_id
                INNER JOIN employee 
               ON employee.employee_id =  visit.doctor_id
              INNER JOIN login_logs
              ON employee.employee_id = login_logs.employee_id
		WHERE visit.visit_date >= CURDATE()
		AND visit.doctor_queue = 'active'
                AND employee.employee_id='$doctor_id'
               AND login_logs.is_active = 'Active'
		ORDER BY (visit.visit_date)asc";
        $result = $this->db->query($query);
        $result = $result->result_array();
        return $result;
    }

    public function l_active() {
        $patients = $this->doc_active->lactive();
        $this->config->set_item('compress_output', FALSE);
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function p_active() {
        $patients = $this->pharmacy_model->pactive();
        $this->config->set_item('compress_output', FALSE);

        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function pat_payment() {
        $patients = $this->reception_model->patient_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function to_payphar() {
        $patients = $this->reception_model->topayphar();
        $this->config->set_item('compress_output', FALSE);
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function to_paywalkin() {
        $patients = $this->reception_model->topaywalkin();
        $this->config->set_item('compress_output', FALSE);
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function seeappointments() {
        $patients = $this->reception_model->see_appointments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function active_patient_doctor() {
        $patients = $this->reception_model->active_paitentdoctors();
        $this->config->set_item('compress_output', FALSE);
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function viewpat() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('newpat');
    }

    public function newpat() {
        $this->reception_model->addpat();
    }

    public function visit() {

        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['queue_name'] = $this->reception_model->getQueue();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['title'] = $this->reception_model->title();
        $data['title_name'] = $this->title();
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        //$this->load->view('visit', $data);
        $data1['contents'] = 'visit';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function patients() {
        $crud = new grocery_CRUD();

        $crud->set_theme('datatables');
        $crud->set_table('patients');
        $crud->columns('fname', 'sname', 'lname', 'sex', 'city', 'phone', 'status');
        $crud->set_subject('Patient');
        $crud->unset_delete();
        $crud->unset_edit();
        $crud->unset_add();

        $crud->display_as('fname', 'first name')
                ->display_as('sname', 'second name')
                ->display_as('lname', 'third name');

        $data['tests'] = $this->get_tests();
        $output = $crud->render();
        $this->load->view('patients', $output);

        $this->_example_output($output);
    }

    public function newpatvisit() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['patient_bio'] = $this->patient_bio();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('visit', $data);
    }

    public function pharm_payments() {
        $id = $this->uri->segment(3);
        $patient = $this->uri->segment(4);
        $data['tested'] = $this->reception_model->getmeds($id);
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['patient_bio'] = $this->patient_bio();
        $this->load->view('pharm_payment', $data);
    }

    public function payments() {
        $id = $this->uri->segment(3);
        $patient = $this->uri->segment(4);
        $data['tested'] = $this->reception_model->pat_payments($id);
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['patient_bio'] = $this->patient_bio();
        $this->load->view('payment', $data);
    }

    public function appointment() {
        $data['home'] = $this->reception_model->see_appointments();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['patient_bio'] = $this->patient_bio();
        $this->load->view('rece_appointments', $data);
    }

    public function add_patient() {
        $this->reception_model->add_patient();
    }

    public function new_family_tree() {
        $this->reception_model->new_family_tree();
    }

    public function new_visit() {
        $this->reception_model->to_package();
        $this->reception_model->check_member();
        $this->reception_model->check_branch();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
    }

    public function labcost($id) {
        $id = $this->uri->segment(3);
        $this->reception_model->lab_cost();
        $this->reception_model->ToLab($id);
    }

    public function pharm_cost() {
        $this->reception_model->pharmcost();
    }

    public function lab_cost() {
        $this->reception_model->labcost();
    }

    public function walkin_patient() {
        $data['queue_name'] = $this->reception_model->getQueue_walkin();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['patient_bio'] = $this->patient_bio();
        //$this->load->view('walkin', $data);
        $data1['contents'] = 'walkin';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function procedure() {
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['patient_bio'] = $this->patient_bio();
        $data['tests'] = $this->get_tests();
        $this->load->view('procedure', $data);
    }

    function procedures() {

        $term = $this->input->get('procedure_name', TRUE);
        $procedures = $this->reception_model->get_procedures($term);
        $this->config->set_item('compress_output', FALSE);
        echo json_encode($procedures);
    }

    public function add_procedure() {
        $this->reception_model->add_procedure();
    }

    public function add_patient_procedure() {
        $this->reception_model->add_patient_procedure();
    }

    function get_active_doctors() {
        $active_doctor = $this->get_doctors();
        $this->config->set_item('compress_output', FALSE);
        if ($active_doctor) {
            echo json_encode($active_doctor);
        } else {
            echo json_encode($active_doctor);
        }
    }

    public function add_walkin() {
        $this->reception_model->walkin();
        redirect('reception/index');
    }

    public function walkin() {
        $id = $this->uri->segment(3);
        $data['topay'] = $this->reception_model->pay_data($id);
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $this->load->view('topay', $data);
    }

    public function walkin_cost() {
        $this->reception_model->walkinpay();

        redirect('reception/index');
    }

    public function new_pay() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $this->load->view('payment', $data);
    }

    public function demo() {
        $this->reception_model->pay_demo();
        redirect('reception/index');
    }

    function base_param($data) {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $data['title'] = ' Receptionist';
        $this->load->view('reception_template', $data);
    }

    public function check_shari_member_existence() {

        $value = $this->uri->segment(3);



        //Multiple select from the Table patient to check for shwari family number 
        // $sql = "Select distinct family_base_number from patient where s_name like '' and f_name like '' and phone_no like '' and family_base_number like '' and identification_number like '' and residence like '' and  city like '' ";
        $sql = "SELECT DISTINCT family_base_number FROM patient WHERE s_name IN ('" . $value . "') OR f_name IN ('" . $value . "') OR other_name IN ('" . $value . "')OR phone_no IN ('" . $value . "')OR family_base_number IN ('" . $value . "')OR family_number IN ('" . $value . "')OR identification_number IN ('" . $value . "') OR next_of_kin_fname IN ('" . $value . "') OR next_of_kin_lname IN ('" . $value . "') OR next_of_kin_phone IN ('" . $value . "') GROUP BY family_number";
        $query = $this->db->query($sql);
        $query = $query->result_array();
        $this->config->set_item('compress_output', FALSE);
        echo json_encode($query);
    }

    public function check_family_member_relation() {
        $family_base_number = $this->input->post('check_family_number');
        $s_name = $this->input->post('check_sur_name');
        $f_name = $this->input->post('check_first_name');
        $phone_no = $this->input->post('check_phone_no');
        $residence = $this->input->post('check_residence');
        $city = $this->input->post('check_city');
        $identification_no = $this->input->post('check_id_no');


        // echo 'family number :' . $family_base_number . '<br> s name :' . $s_name . '<br> f name : ' . $f_name . '<br> phone no : ' . $phone_no . ' <br> residence : ' . $residence . ' <br> city : ' . $city . '<br> identification no : ' . $identification_no . '<br> ';
        //Multiple select from the Table patient to check for shwari family number 
        
        $sql = "SELECT DISTINCT family_base_number , concat(title,':',f_name,' ',s_name,' ',other_name) as patient_name FROM patient WHERE MATCH(s_name) AGAINST ('.$s_name.' IN BOOLEAN MODE ) or MATCH(f_name) AGAINST ('.$f_name.'  IN BOOLEAN MODE) OR MATCH(phone_no) AGAINST ('.$phone_no.' IN BOOLEAN MODE) or MATCH(family_base_number) AGAINST('.$family_base_number.' IN BOOLEAN MODE) OR MATCH(identification_number) AGAINST('.$identification_no.' IN BOOLEAN MODE) OR MATCH(residence) AGAINST('.$residence.' IN BOOLEAN MODE)";
        $query = $this->db->query($sql);
        $query = $query->result_array();
       
        echo json_encode($query);
    }

    public function view_all_visits() {
        $messages = $this->reception_model->get_all_entries();
        $data['messages'] = $messages;
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $this->load->view('view_all_visit', $data);
    }

    function update_entry($data) {
        $this->name = $data['name']; // please read the below note
        $this->message = $data['message'];
        $this->contact = $data['contact'];
        $this->date = time();
        $this->db->update('visit', $this, array('visit_id' => $data['visit_id']));
    }

    function delete_entry($id) {
        $this->db->delete('visit', array('id' => $id));
    }

    public function edit_visit() {
        $data['entry'] = $this->reception_model->get_entry($this->uri->segment(3, 0));
        if (!isset($data['entry'][0]) || $data['entry'][0] == "") {
            echo "error please contact the  system administrato or help desk for assistance";
        } else {
            $data['entry'] = $data['entry'][0];


            $data1['contents'] = 'edit_view';
            $finaldata = array_merge($data, $data1);
            $this->base_param($finaldata);
        }
    }

    public function release_patient() {
        if ($this->uri->segment(3, 0) != "") {
            $this->reception_model->release_patient($this->uri->segment(3, 0));
        }
        redirect("/reception/view_all_visits");
    }

    public function save_edit() {
        if (
                $this->input->post('package_name') != "" &&
                $this->input->post('nurse_queue') != "" &&
                $this->input->post('lab_queue') != "" &&
                $this->input->post('doctor_queue') != "" &&
                $this->input->post('pharm_queue') != "" &&
                $this->input->post('urgency') != "" &&
                $this->input->post('pay_at_the_end') != "" &&
                $this->input->post('visit_id') != ""
        ) {
            $data['package_name'] = $this->input->post('package_name');
            $data['nurse_queue'] = $this->input->post('nurse_queue');
            $data['lab_queue'] = $this->input->post('lab_queue');
            $data['doctor_queue'] = $this->input->post('doctor_queue');
            $data['pharm_queue'] = $this->input->post('pharm_queue');
            $data['urgency'] = $this->input->post('urgency');
            $data['pay_at_the_end'] = $this->input->post('pay_at_the_end');
            $data['visit_id'] = $this->input->post('visit_id');
            $this->reception_model->update_entry($data);
        } else {
            echo 'error please try again later';
        }
        redirect("/reception/view_all_visits");
    }

    public function active_nurses() {
        $active_nurse = $this->reception_model->active_nurses();
        $this->config->set_item('compress_output', FALSE);
        if (empty($active_nurse)) {
            
        } else {
            echo json_encode($active_nurse);
        }
    }

    public function active_doctors() {
        $active_doctors = $this->reception_model->active_doctors();
        $this->config->set_item('compress_output', FALSE);
        if (empty($active_doctors)) {
            
        } else {
            echo json_encode($active_doctors);
        }
    }

    public function active_pharmacist() {
        $active_pharmacist = $this->reception_model->active_pharmacist();
        $this->config->set_item('compress_output', FALSE);
        if (empty($active_pharmacist)) {
            
        } else {
            echo json_encode($active_pharmacist);
        }
    }

    public function active_laboratories() {
        $active_laboratories = $this->reception_model->active_laboratories();
        $this->config->set_item('compress_output', FALSE);
        if (empty($active_laboratories)) {
            
        } else {
            echo json_encode($active_laboratories);
        }
    }

    public function date_convert() {
        $date = DateTime::createFromFormat('d/m/Y', "24/04/2012");
        echo $date->format('Y-m-d');
    }

    public function trial() {
        $data['tests'] = $this->get_tests();
        $this->load->view('trial');
    }

}
