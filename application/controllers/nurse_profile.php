<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->check_isvalidated();
    }

    public function index() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
    }

    private function check_isvalidated() {
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
    }

    public function nurse_patient_profile() {
        $this->load->view('nurse_patient_profile');
    }

}

?>