<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->check_isvalidated();
    }

    public function index() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
    }

    private function check_isvalidated() {
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
    }

    function patient_report() {
        $data['patients'] = $this->reports_model->patient_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
       // $this->load->view('reports/patients_report', $data);
       $this->load->view('patients_report',$data);
    }

    function visitation_report() {
        $data['visitations'] = $this->reports_model->visitation_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reports/visitation_report', $data);
    }

    function edit_visitation() {
        $data['visitations'] = $this->reports_model->visitation_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();

        $data['entry'] = $this->reports_model->get_visitation_entry($this->uri->segment(3, 0));
        if (!isset($data['entry'][0]) || $data['entry'][0] == "") {
            echo "error please contact the  system administrato or help desk for assistance";
        } else {
            $data['entry'] = $data['entry'][0];

            $this->load->view('reports/edit_visitation', $data);
        }
    }

    function view_visitation() {
        $data['visitations'] = $this->reports_model->visitation_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();

        $data['entry'] = $this->reports_model->get_visitation_entry($this->uri->segment(3, 0));
        if (!isset($data['entry'][0]) || $data['entry'][0] == "") {
            echo "error please contact the  system administrato or help desk for assistance";
        } else {
            $data['entry'] = $data['entry'][0];

            $this->load->view('reports/view_visitation', $data);
        }
    }

    function walkin_report() {
        $data['walkins'] = $this->reports_model->walkin_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reports/walkin_report', $data);
    }

    function procedure_report() {
        $data['procedures'] = $this->reports_model->procedure_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reports/procedure_report', $data);
    }

    function get_patient_reports_details() {
        $patient_id = $this->uri->segment(4);
       
        $data = $this->reports_model->get_patient_reports_details($patient_id);
        if (empty($data)) {
            $no_data = 'No Data';
            echo json_encode($no_data);
        } else {
            echo json_encode($data);
        }
    }

}

?>