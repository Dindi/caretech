<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Appointments extends MY_Controller {

    // Tests whether the given ISO8601 string has a time-of-day or not
    const ALL_DAY_REGEX = '/^\d{4}-\d\d-\d\d$/'; // matches strings like "2013-12-29"

    public $title;
    public $allDay; // a boolean
    public $start; // a DateTime
    public $end; // a DateTime, or null
    public $properties = array(); // an array of other misc properties

    // Constructs an Event object from the given array of key=>values.
    // You can optionally force the timezone of the parsed dates.

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('appointments_v');
    }

    public function get_appointments() {
        $this->load->view('appointments_v');
    }

    public function get_appointmentss() {
        $appointments = $this->operations_model->get_appointments();
          $this->config->set_item('compress_output', FALSE);
        if (empty($appointments)) {
            echo json_encode($appointments);
        } else {
            echo json_encode($appointments);
        }
    }

}

?>