<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index($msg = NULL) {


        $data['user_data'] = $this->session->userdata('user');
        $department = $data['user_data']["department"];



        if (isset($department)) {
            redirect('home');
        } else {

            $data['msg'] = $msg;


            $this->load->view('login', $data);
        }
    }

    public function get_client_ip() {
        $ipaddress = '';
        $ipaddress = getenv("REMOTE_ADDR");
        return $ipaddress;
    }

    public function no_access() {

        $this->db->trans_start();
        $data['user_data'] = $this->session->userdata('user');
        $employee_id = $data['user_data']["employee_id"];
        $department = $data['user_data']["department"];
        $login_logs_id = $data['user_data']["login_logs_id"];
        $user_name = $data['user_data']["employee_no"];
        $is_active = $data['user_data']["is_active"];
        $employee_type = $data['user_data']["employee_type"];
        $in_active = 'In_Active';
        $data = array(
            'is_active' => $in_active
        );

        $this->db->where('login_logs_id', $login_logs_id);
        $this->db->update('login_logs', $data);
        $entry_status = "Active";
        $transaction_type = "Update";



        $sql_count = "SELECT count(login_logs_id) as log_id_count from login_logs_log where transaction_type='$transaction_type' and entry_status='$entry_status' and login_logs_id='$login_logs_id'";
        $query = $this->db->query($sql_count);
        foreach ($query->result() as $value) {
            $login_id_count = $value->log_id_count;


            if ($login_id_count > 0) {
                $entry_status_history = "History";
                $login_logs_update = array(
                    'entry_status' => $entry_status
                );

                $this->db->where('transaction_type', $transaction_type);
                $this->db->where('entry_status', $entry_status);
                $this->db->where('login_logs_id', $login_logs_id);
                $this->db->update('login_logs_log', $login_logs_update);
            }
        }

        $month = date('M');
        $year = date('Y');

        $client_ip = $this->get_client_ip();
        $login_logs_log_insert = array(
            'login_logs_id' => $login_logs_id,
            'employee_id' => $employee_id,
            'user_name' => $user_name,
            'login_ip_address' => $client_ip,
            'month' => $month,
            'year' => $year,
            'is_active' => $is_active,
            'employee_type' => $employee_type,
            'entry_status' => $entry_status,
            'transaction_type' => $transaction_type
        );
        $this->db->insert('login_logs_log', $login_logs_log_insert);


        $this->session->sess_destroy();

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            $this->load->view('no_access_v');
        }
    }

    public function process() {
 
        // Load the model
        $this->load->model('login_model');
        // Validate the user can login

        $result = $this->login_model->validate();
        // Now we verify the result

        if (!$result) {
        $password = $this->input->post('password');
            if (empty($password)) {
//                            redirect('home');


                $msg = '<font color=red>Invalid username and/or password.</font><br />';
                $this->index($msg);
            } else {
                $msg = '<font color=red>Invalid username and/or password.</font><br />';
                $this->index($msg);
            }
        } else {
            // If user did validate, 
            // Send them to members area
            redirect('home');
            //echo 'dnbiugfiufu';
        }
    }

    public function get_department() {
        $sql = "SELECT * from department";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function base_param($data) {
        $data['title'] = ' Login';
        $this->load->view('template', $data);
    }

}
