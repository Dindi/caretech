<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nurse extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();

        $data['procedure_name'] = $this->reception_model->getProcedure();

        $data1['contents'] = 'nurse';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function base_params($data) {
        $data['title'] = 'Nurse';
        $this->load->view('nurse_template', $data);
    }

    public function nurse_patient_profile() {
        $patient_id = $this->uri->segment(3);
        $visit_id = $this->uri->segment(4);
        $data['patient_bio'] = $this->patient_bio();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();

        $data['patient_triage_records'] = $this->patient_triage_records($patient_id);
        $data['patient_lab_records'] = $this->lab_records($patient_id);
        $data['patient_consultation_records'] = $this->consultation_records($patient_id);
        $data['patient_prescription_records'] = $this->prescription_records($patient_id);
        $data['patient_triage_report'] = $this->nurse_model->patient_triage_report($patient_id);
        $data['procedure_name'] = $this->reception_model->getProcedure();

        $data1['contents'] = 'nurse_patient_profile';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function add_reason_for_visit() {
        $patient_id = $this->input->post('reason_for_visit_patient_id_1');
        echo 'Patient Id' . $patient_id . '<br/>';
        $visit_id = $this->input->post('reason_for_visit_visit_id');
        echo 'Visit Id' . $visit_id . '<br/>';
        $reason_for_visit = $this->input->post('reason_for_visit_txt_area');
        echo 'Reason for visit' . $reason_for_visit . '</br>';
        $medication = $this->input->post('medication_txt_area');
        echo 'Medication' . $medication . '</br>';
        $emergency = $this->input->post('urgency');
        echo 'Urgency : ' . $emergency . '</br>';
        $update_reason_for_visit = $this->nurse_model->update_reason_for_visit($patient_id, $visit_id, $reason_for_visit, $medication, $emergency);


        if ($update_reason_for_visit) {
            echo 'Update Successfull';
        } else {

            $this->nurse_model->add_reason_for_visit($patient_id, $visit_id, $reason_for_visit, $medication, $emergency);
            echo 'Update failed';
        }
    }

    public function check_profile_type() {
        $department = $this->session->userdata('type');
        if ($department === "Nurse") {
            //do nothing 
        } else {
            // redirect to login page ....trying to access a wrong account
            redirect('home/do_logout');
        }
    }

    public function nurse_patient_list_express() {
        $nurse_patient_list = $this->nurse_model->nurse_patient_list_express();
        $this->config->set_item('compress_output', FALSE);
        if (empty($nurse_patient_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($nurse_patient_list);
        }
    }

    public function nurse_patient_list() {
        $nurse_patient_list = $this->nurse_model->nurse_patient_list();
        $this->config->set_item('compress_output', FALSE);
        if (empty($nurse_patient_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($nurse_patient_list);
        }
    }

    public function nurse_walkin_patient_list() {
        $nurse_patient_list = $this->nurse_model->nurse_walkin_patient_list();
        $this->config->set_item('compress_output', FALSE);
        if (empty($nurse_patient_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($nurse_patient_list);
        }
    }

    public function total_regular_patients_in_tray() {
        $nurse_patient_list = $this->nurse_model->total_regular_patients_in_tray();
        $this->config->set_item('compress_output', FALSE);
        if (empty($nurse_patient_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($nurse_patient_list);
        }
    }

    public function total_walkin_patients_in_tray() {
        $total_walkin_patients_in_tray = $this->nurse_model->total_walkin_patients_in_tray();
        $this->config->set_item('compress_output', FALSE);
        if (empty($total_walkin_patients_in_tray)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($total_walkin_patients_in_tray);
        }
    }

    public function nurse_patient_list_xpress() {
        $nurse_patient_list_xpress = $this->nurse_model->nurse_patient_list_xpress();
        $this->config->set_item('compress_output', FALSE);
        if (empty($nurse_patient_list_xpress)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($nurse_patient_list_xpress);
        }
    }

    public function patient_procedure_list() {
        $patient_procedure_list = $this->nurse_model->patient_procedure_list();
        $this->config->set_item('compress_output', FALSE);
        if (empty($patient_procedure_list)) {
            $no_data = "No Data";
        } else {
            echo json_encode($patient_procedure_list);
        }
    }

    public function appointment_list() {
        $appointment_list = $this->nurse_model->appointment_list();
        $this->config->set_item('compress_output', FALSE);
        if (empty($appointment_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($appointment_list);
        }
    }

    public function total_appointments_to_date() {
        $total_appointments_to_date = $this->nurse_model->total_appointments_to_date();
        $this->config->set_item('compress_output', FALSE);
        if (empty($total_appointments_to_date)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($total_appointments_to_date);
        }
    }

    public function get_triage_details() {
        $triage_id = $this->uri->segment(3);
        $triage_details = $this->nurse_model->get_triage_details($triage_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($triage_details)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($triage_details);
        }
    }

    public function add_triage() {
        $visit_id = $this->input->post('add_triage_visit_id', TRUE);
        $patient_id = $this->input->post('add_triage_patient_id', TRUE);
        $weight = $this->input->post('weight', TRUE);
        $systolic = $this->input->post('systolic', TRUE);
        $diastolic = $this->input->post('diastolic', TRUE);
        $temperature = $this->input->post('temperature', TRUE);
        $height = $this->input->post('height', TRUE);
        $respiratory = $this->input->post('respiratory', TRUE);
        $blood_sugar = $this->input->post('blood_sugar', TRUE);
        $lmp = $this->input->post('LMP', TRUE);

        $urgent = $this->input->post('urgency', TRUE);
        $pulse_rate = $this->input->post('pulse_rate', TRUE);

        $user_id = $this->session->userdata('id');

        $update_triage = $this->nurse_model->update_patient_traige($visit_id, $patient_id, $weight, $systolic, $diastolic, $temperature, $height, $respiratory, $blood_sugar, $lmp, $urgent, $pulse_rate, $user_id);
        if ($update_triage) {
            echo 'Update Successfull....';
        } else {
            $this->nurse_model->add_triage();
        }
    }

    public function update_triage() {
        $this->nurse_model->update_triage();
    }

    public function delete_triage() {
        $triage_id = $this->input->post('input_triage_id_delete');
        $this->nurse_model->delete_triage($triage_id);
    }

    public function active_doctors_list() {
        $active_doctors_list = $this->nurse_model->active_doctors_list();
        $this->config->set_item('compress_output', FALSE);
        if (empty($active_doctors_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($active_doctors_list);
        }
    }

    public function send_to_doctor() {

        $patient_id = $this->input->post('send_to_doctor_patient_id');
        $visit_id = $this->input->post('send_to_doctor_visit_id');
        $doctor_id = $this->input->post('send_to_doctor_name');
        $this->operations_model->send_to_doctor($patient_id, $visit_id, $doctor_id);
    }

    public function send_to_pharmacy() {
        $visit_id = $this->input->post('send_to_pharm_visit_id');
        $this->operations_model->send_to_pharmacy($visit_id);
    }

    public function employee_workload() {
        $data['workload'] = $this->workload();
        $data['tests'] = $this->get_tests();
        $this->load->view('workload', $data);
    }

    public function book_appointment() {
        $patient_id = $this->input->post('appointment_patient_id');
        $visit_id = $this->input->post('appointment_visit_id');
        $appointment_type = $this->input->post('appointment_type');
        $person_to_see = $this->input->post('person_to_see');
        $date_from = $this->input->post('date_from');
        $date_to = $this->input->post('date_to');
        $reason_for_appointment = $this->input->post('reason_for_appointment');

        $update_appointment = $this->operations_model->update_appointment($patient_id, $visit_id, $appointment_type, $person_to_see, $date_from, $date_to, $reason_for_appointment);
        if ($update_appointment) {
            echo 'Successfullly Updated!!!!!' . '</br>';
        } else {
            $this->operations_model->add_appointment($patient_id, $visit_id, $appointment_type, $person_to_see, $date_from, $date_to, $reason_for_appointment);
        }
    }

    public function add_allergy() {
        $visit_id = $this->input->post('add_allergy_visit_id');
        $patient_id = $this->input->post('add_allergy_patient_id');
        $allergy = $this->input->post('allergy');
        $update_allergy = $this->nurse_model->update_patient_allergy($visit_id, $patient_id, $allergy);
        if ($update_allergy) {
            echo 'Updated successfullyyy!!!';
        } else {
            $this->nurse_model->add_patient_allergy($visit_id, $patient_id, $allergy);
        }
    }

    public function add_immunization() {
        $visit_id = $this->input->post('add_immunization_visit_id');
        $patient_id = $this->input->post('add_immunization_patient_id');
        $immunization = $this->input->post('immunization');
        $update_allergy = $this->nurse_model->update_patient_immunization($visit_id, $patient_id, $immunization);
        if ($update_allergy) {
            echo 'Updated successfullyyy!!!';
        } else {
            $this->nurse_model->add_patient_immunization($visit_id, $patient_id, $immunization);
        }
    }

    public function add_social_history() {
        $visit_id = $this->input->post('add_family_social_visit_id');
        $patient_id = $this->input->post('add_family_social_patient_id');
        $family_social = $this->input->post('family_social');
        $update_allergy = $this->nurse_model->update_patient_social_history($visit_id, $patient_id, $family_social);
        if ($update_allergy) {
            echo 'Updated successfullyyy!!!';
        } else {
            $this->nurse_model->add_patient_social_history($visit_id, $patient_id, $family_social);
        }
    }

    public function get_allergy() {
        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $get_allergy = $this->nurse_model->get_allergy($patient_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($get_allergy)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($get_allergy);
        }
    }

    public function get_triage() {
        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $get_allergy = $this->nurse_model->get_triage($patient_id, $visit_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($get_allergy)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($get_allergy);
        }
    }

    public function nurse_walkin_list() {
        $nurse_walkin_list = $this->nurse_model->nurse_walkin_list();
        if (empty($nurse_walkin_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($nurse_walkin_list);
        }
    }

    public function nurse_walkin_profile() {
        $patient_id = $this->uri->segment(3);
        $visit_id = $this->uri->segment(4);
        $data['patient_bio'] = $this->patient_bio();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();

        $data['patient_triage_records'] = $this->patient_triage_records($patient_id);
        $data['patient_lab_records'] = $this->lab_records($patient_id);
        $data['patient_consultation_records'] = $this->consultation_records($patient_id);
        $data['patient_prescription_records'] = $this->prescription_records($patient_id);
        $data['patient_triage_report'] = $this->nurse_model->patient_triage_report($patient_id);
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['procedure_list'] = $this->procedure_list();
        $data['procedure_results'] = $this->get_procedure_results($patient_id);
        $data1['contents'] = 'nurse_walkin_patient_profile';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function add_walkin_procedure() {
        $visit_id = $this->input->post('visit_id');
        $patient_id = $this->input->post('patient_id');
        $procedure_type = $this->input->post('walkin_procedure');

        $this->nurse_model->add_walkin_procedure($visit_id, $patient_id, $procedure_type);
    }

    public function add_walkin_procedure_notes() {
        $id = $this->input->post('add_procedure_result_id');
        $procedure_notes = $this->input->post('add_procedure_notes');

        $update_procedure_notes = $this->nurse_model->update_procedure_notes($id, $procedure_notes);
        if ($update_procedure_notes) {
            echo 'Updated Successfully...';
        } else {
            echo 'Update failed';
        }
    }

}
