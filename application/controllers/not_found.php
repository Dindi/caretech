<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Not_found extends CI_Controller {
    /*
      Controllers that are considered secure extend Secure_area, optionally a $module_id can
      be set to also check if a user can access a particular module in the system.
     */

    function __construct() {
        parent::__construct();
    }
    function index() {
        $this->load->view('error');
    }

}
?>

