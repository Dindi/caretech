<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reception extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->check_isvalidated();
    }

    function index() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reception/reception', $data);
    }

    private function check_isvalidated() {
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
    }

    public function load_data() {
        $patients = $this->mactive->nactive();
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    function total_visits() {
        $total_visit = $this->reception_model->total_visits();
        if (!empty($total_visit)) {
            if ($total_visit === 0) {
                $no_patients = 'No Regular Patients visits';
                echo json_encode($no_patients);
            } else {
                echo json_encode($total_visit);
            }
        } else {
            
        }
    }

    function total_walkin_patients() {
        $total_walkin_patients = $this->reception_model->total_walkin_patients();
        if (!empty($total_walkin_patients)) {
            if ($total_walkin_patients === 0) {
                $no_patients = 'No Walkin Patients';
                echo json_encode($no_patients);
            } else {
                echo json_encode($total_walkin_patients);
            }
        } else {
            
        }
    }

    public function active_nurses() {
        $sql = "SELECT DISTINCT employee.employee_id,employee.user_name,login_logs.is_active , department.department_name
                 FROM employee
                 INNER JOIN login_logs
               ON employee.employee_id = login_logs.employee_id
               INNER JOIN department 
               ON employee.employment_category = department.department_id
                 WHERE login_logs.is_active = 'Active' 
                 AND department.department_name='Nurse'
                 ORDER BY (employee.employee_id)asc";
        $result = $this->db->query($sql);
        $active_nurse = $result->result_array();
        echo json_encode($active_nurse);
    }

    public function active_doctors() {
        $sql = "SELECT DISTINCT employee.employee_id,employee.user_name,login_logs.is_active , department.department_name
                 FROM employee
                 INNER JOIN login_logs
               ON employee.employee_id = login_logs.employee_id
               INNER JOIN department 
               ON employee.employment_category = department.department_id
                 WHERE login_logs.is_active = 'Active' 
                 AND department.department_name='Doctor'
                 ORDER BY (employee.employee_id)asc";
        $result = $this->db->query($sql);
        $active_doctor = $result->result_array();
        echo json_encode($active_doctor);
    }

    public function active_pharmacist() {
        $sql = "SELECT DISTINCT employee.employee_id,employee.user_name,login_logs.is_active , department.department_name
                 FROM employee
                 INNER JOIN login_logs
               ON employee.employee_id = login_logs.employee_id
               INNER JOIN department 
               ON employee.employment_category = department.department_id
                 WHERE login_logs.is_active = 'Active' 
                 AND department.department_name='Pharmacy'
                 ORDER BY (employee.employee_id)asc";
        $result = $this->db->query($sql);
        $active_pharmacy = $result->result_array();
        echo json_encode($active_pharmacy);
    }

    public function active_laboratories() {
        $sql = "SELECT DISTINCT employee.employee_id,employee.user_name,login_logs.is_active , department.department_name
                 FROM employee
                 INNER JOIN login_logs
               ON employee.employee_id = login_logs.employee_id
               INNER JOIN department 
               ON employee.employment_category = department.department_id
                 WHERE login_logs.is_active = 'Active' 
                 AND department.department_name='Laboratory'
                 ORDER BY (employee.employee_id)asc";
        $result = $this->db->query($sql);
        $active_laboratories = $result->result_array();
        echo json_encode($active_laboratories);
    }

    public function d_active() {
        $patients = $this->dactive();
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function dactive() {//Doctor
        $doctor_id = $this->uri->segment(3);
        //echo 'Doctor ID'.$doctor_id;
        $query = "SELECT DISTINCT patient.f_name, employee.employee_id,visit.patient_id, patient.other_name, patient.s_name, visit.visit_date, visit.visit_id,visit.urgency, visit.results,employee.user_name,login_logs.is_active
		FROM patient
		INNER JOIN visit
		ON patient.patient_id=visit.patient_id
                INNER JOIN employee 
               ON employee.employee_id =  visit.doctor_id
              INNER JOIN login_logs
              ON employee.employee_id = login_logs.employee_id
		WHERE visit.visit_date >= CURDATE()
		AND visit.doctor_queue = 'active'
                AND employee.employee_id='$doctor_id'
               AND login_logs.is_active = 'Active'
		ORDER BY (visit.visit_date)asc";
        $result = $this->db->query($query);
        $result = $result->result_array();
        return $result;
    }

    public function l_active() {
        $patients = $this->doc_active->lactive();
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function p_active() {
        $patients = $this->pharmacy_model->pactive();


        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function pat_payment() {
        $patients = $this->reception_model->patient_payments();
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function to_payphar() {
        $patients = $this->reception_model->topayphar();

        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function to_paywalkin() {
        $patients = $this->reception_model->topaywalkin();
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function seeappointments() {
        $patients = $this->reception_model->see_appointments();
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function active_patient_doctor() {
        $patients = $this->reception_model->active_paitentdoctors();
        if (empty($patients)) {
            
        } else {
            echo json_encode($patients);
        }
    }

    public function viewpat() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reception/newpat');
    }

    public function newpat() {
        $this->reception_model->addpat();
    }

    public function visit() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['queue_name'] = $this->reception_model->getQueue();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['title'] = $this->reception_model->title();
        $this->load->view('reception/visit', $data);
    }

    public function patients() {
        $crud = new grocery_CRUD();

        $crud->set_theme('datatables');
        $crud->set_table('patients');
        $crud->columns('fname', 'sname', 'lname', 'sex', 'city', 'phone', 'status');
        $crud->set_subject('Patient');
        $crud->unset_delete();
        $crud->unset_edit();
        $crud->unset_add();

        $crud->display_as('fname', 'first name')
                ->display_as('sname', 'second name')
                ->display_as('lname', 'third name');


        $output = $crud->render();
        $this->load->view('reception/patients', $output);

        $this->_example_output($output);
    }

    public function newpatvisit() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();

        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reception/visit', $data);
    }

    public function pharm_payments() {
        $id = $this->uri->segment(3);
        $patient = $this->uri->segment(4);
        $data['tested'] = $this->reception_model->getmeds($id);
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();

        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reception/pharm_payment', $data);
    }

    public function payments() {
        $id = $this->uri->segment(3);
        $patient = $this->uri->segment(4);
        $data['tested'] = $this->reception_model->pat_payments($id);
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();

        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reception/payment', $data);
    }

    public function appointment() {
        $data['home'] = $this->reception_model->see_appointments();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();

        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reception/rece_appointments', $data);
    }

    public function add() {
        $this->reception_model->addpat();
    }

    public function new_family_tree() {
        $this->reception_model->new_family_tree();
    }

    public function new_visit() {
        $this->reception_model->to_package();
        $this->reception_model->check_member();
        $this->reception_model->check_branch();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
    }

    function all_visit() {
        $crud = new grocery_CRUD();

        $crud->set_theme('datatables');
        $crud->set_table('visit');
        $crud->columns('patient_id', 'family_number', 'doctor_id', 'visit_id', 'visit_date');
        $crud->display_as('visit_date', 'Visit Date')
                ->display_as('doctor_id', 'Doctor');


        $crud->set_subject('All Patient Visits');


        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_edit();

        $crud->set_relation('doctor_id', 'employee', '{f_name}  {l_name} {other_name} ');
        $crud->set_relation('visit_id', 'patient_payments', 'cost');
        $crud->display_as('visit_id', 'Package Cost');

        $crud->set_relation('patient_id', 'patient', '{f_name}{s_name}{other_name}');
        $crud->display_as('patient_id', 'Patient Names');
        $crud->display_as('doctor_id', 'Doctors Names');

        $output = $crud->render();

        $this->_example_output($output);
    }

    function _example_output($output = null) {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reception/all_visit_view.php', $output);
    }

    public function labcost($id) {
        $id = $this->uri->segment(3);
        $this->reception_model->lab_cost();
        $this->reception_model->ToLab($id);
    }

    public function pharm_cost() {
        $this->reception_model->pharmcost();
    }

    public function lab_cost() {
        $this->reception_model->labcost();
    }

    public function walkin_patient() {
        $data['queue_name'] = $this->reception_model->getQueue_walkin();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reception/walkin', $data);
    }

    public function procedure() {
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $this->load->view('reception/procedure', $data);
    }

    function procedures() {

        $term = $this->input->get('procedure_name', TRUE);
        $procedures = $this->reception_model->get_procedures($term);
        echo json_encode($procedures);
    }

    public function add_procedure() {
        $this->reception_model->add_procedure();
    }

    function get_active_doctors() {
        $active_doctor = $this->reception_model->get_active_doctor();

        if ($active_doctor) {
            echo json_encode($active_doctor);
        } else {
            echo json_encode($active_doctor);
        }
    }

    public function add_walkin() {
        $this->reception_model->walkin();
        redirect('reception/reception');
    }

    public function walkin() {
        $id = $this->uri->segment(3);
        $data['topay'] = $this->reception_model->pay_data($id);
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reception/topay', $data);
    }

    public function walkin_cost() {
        $this->reception_model->walkinpay();
        redirect('reception/reception/index');
    }

    public function new_pay() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reception/payment', $data);
    }

    public function demo() {
        $this->reception_model->pay_demo();
        redirect('reception/reception/index');
    }

    function base_param($data) {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['title'] = ' Receptionist';
        $this->load->view('reception/reception_template', $data);
    }

    public function check_shari_member_existence() {

        $value = $this->uri->segment(3);



        //Multiple select from the Table patient to check for shwari family number 

        $sql = "SELECT DISTINCT family_base_number FROM patient WHERE s_name IN ('" . $value . "') OR f_name IN ('" . $value . "') OR other_name IN ('" . $value . "')OR phone_no IN ('" . $value . "')OR family_base_number IN ('" . $value . "')OR family_number IN ('" . $value . "')OR identification_number IN ('" . $value . "') OR next_of_kin_fname IN ('" . $value . "') OR next_of_kin_lname IN ('" . $value . "') OR next_of_kin_phone IN ('" . $value . "') GROUP BY family_number";
        $query = $this->db->query($sql);
        $query = $query->result_array();
        echo json_encode($query);
    }

    public function view_all_visits() {
        $messages = $this->reception_model->get_all_entries();
        $data['messages'] = $messages;
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $this->load->view('reception/view_all_visit', $data);
    }

    function update_entry($data) {
        $this->name = $data['name']; // please read the below note
        $this->message = $data['message'];
        $this->contact = $data['contact'];
        $this->date = time();
        $this->db->update('visit', $this, array('visit_id' => $data['visit_id']));
    }

    function delete_entry($id) {
        $this->db->delete('visit', array('id' => $id));
    }

    public function edit_visit() {
        $data['entry'] = $this->reception_model->get_entry($this->uri->segment(3, 0));
        if (!isset($data['entry'][0]) || $data['entry'][0] == "") {
            echo "error please contact the  system administrato or help desk for assistance";
        } else {
            $data['entry'] = $data['entry'][0];


            $data1['contents'] = 'edit_view';
            $finaldata = array_merge($data, $data1);
            $this->base_param($finaldata);
        }
    }

    public function release_patient() {
        if ($this->uri->segment(3, 0) != "") {
            $this->reception_model->release_patient($this->uri->segment(3, 0));
        }
        redirect("reception/reception/view_all_visits");
    }

    public function time_now() {
        $time = time();
        echo 'Time now is : ' . $time . '</br>';
    }

    public function save_edit() {
        if (
                $this->input->post('package_name') != "" &&
                $this->input->post('nurse_queue') != "" &&
                $this->input->post('lab_queue') != "" &&
                $this->input->post('doctor_queue') != "" &&
                $this->input->post('pharm_queue') != "" &&
                $this->input->post('urgency') != "" &&
                $this->input->post('pay_at_the_end') != "" &&
                $this->input->post('visit_id') != ""
        ) {
            $data['package_name'] = $this->input->post('package_name');
            $data['nurse_queue'] = $this->input->post('nurse_queue');
            $data['lab_queue'] = $this->input->post('lab_queue');
            $data['doctor_queue'] = $this->input->post('doctor_queue');
            $data['pharm_queue'] = $this->input->post('pharm_queue');
            $data['urgency'] = $this->input->post('urgency');
            $data['pay_at_the_end'] = $this->input->post('pay_at_the_end');
            $data['visit_id'] = $this->input->post('visit_id');
            $this->reception_model->update_entry($data);
        } else {
            echo 'error please try again later';
        }
        redirect("reception/reception/view_all_visits");
    }

}
