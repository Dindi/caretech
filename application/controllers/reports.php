 <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->check_isvalidated();
    }

    public function index() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();
    }

    function base_params($data) {
        $data['title'] = 'Reports';
        $this->load->view('template', $data);
    }

    private function check_isvalidated() {
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
    }

    function patient_report() {
        $data['patients'] = $this->reports_model->patient_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();

        $data1['contents'] = 'patients_report';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function visitation_report() {

        $data['visitations'] = $this->reports_model->visitation_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();

        $data1['contents'] = 'visitation_report';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function edit_visitation() {
        $data['visitations'] = $this->reports_model->visitation_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();

        $data['entry'] = $this->reports_model->get_visitation_entry($this->uri->segment(3, 0));
        if (!isset($data['entry'][0]) || $data['entry'][0] == "") {
            echo "error please contact the  system administrator or help desk for assistance";
        } else {
            $data['entry'] = $data['entry'][0];

            $this->load->view('reports/edit_visitation', $data);
        }
    }

    function view_visitation() {
        $data['visitations'] = $this->reports_model->visitation_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();

        $data['entry'] = $this->reports_model->get_visitation_entry($this->uri->segment(3, 0));
        if (!isset($data['entry'][0]) || $data['entry'][0] == "") {
            echo "error please contact the  system administrato or help desk for assistance";
        } else {
            $data['entry'] = $data['entry'][0];

            $this->load->view('reports/view_visitation', $data);
        }
    }

    function walkin_report() {
        $data['walkins'] = $this->reports_model->walkin_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        //$this->load->view('walkin_report', $data);
        $data1['contents'] = 'walkin_report';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function procedure_report() {
        $data['procedures'] = $this->reports_model->procedure_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        //$this->load->view('procedure_report', $data);
        $data1['contents'] = 'procedure_report';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function stock_bin_card_report() {
        $data['procedures'] = $this->reports_model->procedure_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['stock_bin_card'] = $this->operations_model->get_pharm_bin_card();
        //$this->load->view('procedure_report', $data);
        $data1['contents'] = 'stock_bin_card_v';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function commodity_management() {
        $data['procedures'] = $this->reports_model->procedure_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['commodity_management'] = $this->reports_model->export_commodity_report();
        $data['suppliers'] = $this->suppliers();
        $data['commodity_type'] = $this->commodity_type();
        $data['commodities'] = $this->commodities();
        //$this->load->view('procedure_report', $data);
        $data1['contents'] = 'commodity_management_v';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function commodity_report() {
        $data['procedures'] = $this->reports_model->procedure_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['commodity_management'] = $this->reports_model->export_commodity_report();
        $data['suppliers'] = $this->suppliers();
        $data['commodity_type'] = $this->commodity_type();
        $data['commodities'] = $this->commodities();
        //$this->load->view('procedure_report', $data);
        $data1['contents'] = 'commodity_management_v';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function stock_management() {
        $data['pack'] = $this->package();
        $data['name'] = $this->getName();
        $data['title'] = $this->title();
        $data['name'] = $this->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->getProcedure();
        $data['patient_bio'] = $this->patient_bio();
        $data['commodities'] = $this->commodities();
        $data['frequency'] = $this->frequency();
        $data['route'] = $this->route();
        $data['occurence'] = $this->occurence();
        $data['pack'] = $this->package();
        $data['name'] = $this->getName();
        $data['title'] = $this->title();
        $cache_data = $this->cached_icd10();
        $data['cache_data'] = $cache_data;
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $data['procedure_name'] = $this->getProcedure();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['commodities'] = $this->commodities();
        $data['frequency'] = $this->frequency();
        $data['route'] = $this->route();
        $data['occurence'] = $this->occurence();

        $data['procedures'] = $this->reports_model->procedure_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['stock_management'] = $this->reports_model->export_stock_report();
        $data['suppliers'] = $this->suppliers();
        $data['commodity_type'] = $this->commodity_type();
        $data['commodities'] = $this->commodities();
        //$this->load->view('procedure_report', $data);
        $data1['contents'] = 'stock_management';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function get_commodity_info() {
        $commodity_id = $this->uri->segment(3);
        $commodity_info = $this->reports_model->get_commodity_info($commodity_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($commodity_info)) {
            echo json_encode($commodity_info);
        } else {
            echo json_encode($commodity_info);
        }
    }

    public function add_new_stock() {
        $new_stock = $this->reports_model->add_new_stock();
    }

    public function add_new_commodity() {
        $new_commodity = $this->reports_model->add_new_commodity();
    }

    public function edit_commodity() {
        $edit_commodity = $this->reports_model->edit_new_commodity();
    }

    public function delete_commodity() {
        $delete_commodity = $this->reports_model->delete_commodity();
    }

    public function edit_stock_info() {
        $edit_stock = $this->reports_model->edit_stock_info();
    }

    public function delete_stock_info() {
        $delete_stock = $this->reports_model->delete_stock_info();
    }

    function get_patient_reports_details() {
        $patient_id = $this->uri->segment(3);

        $data = $this->reports_model->get_patient_reports_details($patient_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($data)) {
            $no_data = 'No Data';
            echo json_encode($no_data);
        } else {
            echo json_encode($data);
        }
    }

    function get_procedure_visit_reports_details() {
        $procedure_visit_id = $this->uri->segment(3);

        $data = $this->reports_model->get_procedure_visit_reports_details($procedure_visit_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($data)) {
            $no_data = 'No Data';
            echo json_encode($no_data);
        } else {
            echo json_encode($data);
        }
    }

    function get_visit_reports_details() {
        $visit_id = $this->uri->segment(3);
        $data = $this->reports_model->get_visit_reports_details($visit_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($data)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($data);
        }
    }

    function get_walkinpatient_reports_details() {
        $walkin_patient_id = $this->uri->segment(3);
        $data = $this->reports_model->get_walkinpatient_reports_details($walkin_patient_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($data)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($data);
        }
    }

    public function delete_patient() {
        $patient_id = $this->input->post('input_patient_id_delete');
        $this->reports_model->delete_patient($patient_id);
    }

    public function edit_patient() {
        $this->reports_model->edit_patient();
    }

    public function edit_visitation_info() {
        $this->reports_model->edit_visitation_info();
    }

    public function edit_walkin_patient() {
        $this->reports_model->edit_walkin_patient();
    }

    public function edit_procedure_visit_data() {
        $this->reports_model->edit_procedure_visit_data();
    }

    public function delete_procedure_visit_data() {
        $procedure_visit_id = $this->input->post('input_procedure_patient_id_delete');
        $this->reports_model->delete_procedure_visit_data($procedure_visit_id);
    }

    public function delete_visitation_info() {
        $visit_id = $this->input->post('input_visit_id_delete');
        $this->reports_model->delete_visitation_info($visit_id);
    }

    public function delete_walkin_patient() {
        $walkin_id = $this->input->post('input_walkin_patient_id_delete');
        $this->reports_model->delete_walkin_patient($walkin_id);
    }

    public function export_visitation_report() {
        $exported_data = $this->reports_model->visitation_report();

        $this->load->library('excel');



        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Vistation Report');
        $this->excel->getActiveSheet()->SetCellValue('A1', "No :");
        $this->excel->getActiveSheet()->SetCellValue('B1', "Date :");
        $this->excel->getActiveSheet()->SetCellValue('C1', "Patient Name :");
        $this->excel->getActiveSheet()->SetCellValue('D1', "Gender :");
        $this->excel->getActiveSheet()->SetCellValue('E1', "Phone No :");
        $this->excel->getActiveSheet()->SetCellValue('F1', "Email :");
        $this->excel->getActiveSheet()->SetCellValue('G1', "Service Offered :");
        $this->excel->getActiveSheet()->SetCellValue('H1', "Status : ");



        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);

        $author = $this->session->userdata('user_name');






        $i = 2;
        $no = 1;
        foreach ($exported_data as $result) {

            $this->excel->getActiveSheet()->SetCellValue('A' . $i, $no);
            $this->excel->getActiveSheet()->SetCellValue('B' . $i, $result["date_added"]);
            $this->excel->getActiveSheet()->SetCellValue('C' . $i, $result["title"] . ' ' . $result["f_name"] . ' ' . $result["s_name"] . ' ' . $result["other_name"]);
            $this->excel->getActiveSheet()->SetCellValue('D' . $i, $result["gender"]);
            $this->excel->getActiveSheet()->SetCellValue('E' . $i, $result["phone_no"]);
            $this->excel->getActiveSheet()->SetCellValue('F' . $i, $result["email"]);
            $this->excel->getActiveSheet()->SetCellValue('G' . $i, $result["package_name"]);
            $this->excel->getActiveSheet()->SetCellValue('H' . $i, $result["status"]);




            $no++;
            $i++;
        }




        $filename = "Patients_Report.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

    public function export_patient_report() {
        $exported_data = $this->reports_model->patient_report();

        $this->load->library('excel');



        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Pateint Report');
        $this->excel->getActiveSheet()->SetCellValue('A1', "No :");
        $this->excel->getActiveSheet()->SetCellValue('B1', "Date :");
        $this->excel->getActiveSheet()->SetCellValue('C1', "Patient Name :");
        $this->excel->getActiveSheet()->SetCellValue('D1', "DoB :");
        $this->excel->getActiveSheet()->SetCellValue('E1', "Gender :");
        $this->excel->getActiveSheet()->SetCellValue('F1', "Phone NO :");
        $this->excel->getActiveSheet()->SetCellValue('G1', "Email :");
        $this->excel->getActiveSheet()->SetCellValue('H1', "Status : ");



        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);

        $author = $this->session->userdata('user_name');






        $i = 2;
        $no = 1;
        foreach ($exported_data as $result) {

            $this->excel->getActiveSheet()->SetCellValue('A' . $i, $no);
            $this->excel->getActiveSheet()->SetCellValue('B' . $i, $result["date_added"]);
            $this->excel->getActiveSheet()->SetCellValue('C' . $i, $result["title"] . ' ' . $result["f_name"] . ' ' . $result["s_name"] . ' ' . $result["other_name"]);
            $this->excel->getActiveSheet()->SetCellValue('D' . $i, $result["dob"]);
            $this->excel->getActiveSheet()->SetCellValue('E' . $i, $result["gender"]);
            $this->excel->getActiveSheet()->SetCellValue('F' . $i, $result["phone_no"]);
            $this->excel->getActiveSheet()->SetCellValue('G' . $i, $result["email"]);
            $this->excel->getActiveSheet()->SetCellValue('H' . $i, $result["status"]);



            $no++;
            $i++;
        }




        $filename = "Patients_Report.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

    public function export_walkin_report() {
        $exported_data = $this->reports_model->walkin_report();

        $this->load->library('excel');



        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Walkin Pateint Report');
        $this->excel->getActiveSheet()->SetCellValue('A1', "No :");
        $this->excel->getActiveSheet()->SetCellValue('B1', "Date :");
        $this->excel->getActiveSheet()->SetCellValue('C1', "Patient Name :");
        $this->excel->getActiveSheet()->SetCellValue('D1', "Phone No :");
        $this->excel->getActiveSheet()->SetCellValue('E1', "Department :");
        $this->excel->getActiveSheet()->SetCellValue('F1', "Payment Status :");
        $this->excel->getActiveSheet()->SetCellValue('G1', "Status :");



        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);

        $author = $this->session->userdata('user_name');






        $i = 2;
        $no = 1;
        foreach ($exported_data as $result) {

            $this->excel->getActiveSheet()->SetCellValue('A' . $i, $no);
            $this->excel->getActiveSheet()->SetCellValue('B' . $i, $result["walkin_date"]);
            $this->excel->getActiveSheet()->SetCellValue('C' . $i, $result["walkin_patient_name"]);
            $this->excel->getActiveSheet()->SetCellValue('D' . $i, $result["walkin_phone_no"]);
            $this->excel->getActiveSheet()->SetCellValue('E' . $i, $result["walkin_department"]);
            $this->excel->getActiveSheet()->SetCellValue('F' . $i, $result["paid"]);




            $no++;
            $i++;
        }




        $filename = "Walkin_Patient_Report.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

    public function export_procedure_report() {



        $exported_data = $this->reports_model->procedure_report();

        $this->load->library('excel');



        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Pateint Procedure Report');
        $this->excel->getActiveSheet()->SetCellValue('A1', "No :");
        $this->excel->getActiveSheet()->SetCellValue('B1', "Date :");
        $this->excel->getActiveSheet()->SetCellValue('C1', "Patient Name :");
        $this->excel->getActiveSheet()->SetCellValue('D1', "Phone No :");
        $this->excel->getActiveSheet()->SetCellValue('E1', "Patient Type :");
        $this->excel->getActiveSheet()->SetCellValue('F1', "Service Offered :");
        $this->excel->getActiveSheet()->SetCellValue('G1', "Status :");



        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);

        $author = $this->session->userdata('user_name');






        $i = 2;
        $no = 1;
        foreach ($exported_data as $result) {

            $this->excel->getActiveSheet()->SetCellValue('A' . $i, $no);
            $this->excel->getActiveSheet()->SetCellValue('B' . $i, $result["date_added"]);
            $this->excel->getActiveSheet()->SetCellValue('C' . $i, $result["patient_name"]);
            $this->excel->getActiveSheet()->SetCellValue('D' . $i, $result["patient_phone"]);
            $this->excel->getActiveSheet()->SetCellValue('E' . $i, $result["patient_type"]);
            $this->excel->getActiveSheet()->SetCellValue('F' . $i, $result["procedure_name"]);
            $this->excel->getActiveSheet()->SetCellValue('F' . $i, $result["status"]);



            $no++;
            $i++;
        }




        $filename = "Patient_Procedure_Report.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

    public function export_stock_report() {
        $exported_data = $this->reports_model->export_stock_report();

        $this->load->library('excel');



        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Stock Report');
        $this->excel->getActiveSheet()->SetCellValue('A1', "No :");
        $this->excel->getActiveSheet()->SetCellValue('B1', "Date :");
        $this->excel->getActiveSheet()->SetCellValue('C1', "Commodity Name :");
        $this->excel->getActiveSheet()->SetCellValue('D1', "Commodity Type :");
        $this->excel->getActiveSheet()->SetCellValue('E1', "Batch No :");
        $this->excel->getActiveSheet()->SetCellValue('F1', "No of Packs :");
        $this->excel->getActiveSheet()->SetCellValue('G1', "Unit per Pack :");
        $this->excel->getActiveSheet()->SetCellValue('H1', "Expired ? : ");
        $this->excel->getActiveSheet()->SetCellValue('I1', "Buying Price :");
        $this->excel->getActiveSheet()->SetCellValue('J1', "Selling Price :");
        $this->excel->getActiveSheet()->SetCellValue('K1', "Employee Name :");




        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('I1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('J1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('K1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);

        $author = $this->session->userdata('user_name');






        $i = 2;
        $no = 1;
        foreach ($exported_data as $result) {

            $this->excel->getActiveSheet()->SetCellValue('A' . $i, $no);
            $this->excel->getActiveSheet()->SetCellValue('B' . $i, $result["date_added"]);
            $this->excel->getActiveSheet()->SetCellValue('C' . $i, $result["commodity_name"]);
            $this->excel->getActiveSheet()->SetCellValue('D' . $i, $result["commodity_type"]);
            $this->excel->getActiveSheet()->SetCellValue('E' . $i, $result["batch_no"]);
            $this->excel->getActiveSheet()->SetCellValue('F' . $i, $result["no_of_packs"]);
            $this->excel->getActiveSheet()->SetCellValue('G' . $i, $result["unit_per_pack"]);
            $this->excel->getActiveSheet()->SetCellValue('H' . $i, $result["has_expired"]);
            $this->excel->getActiveSheet()->SetCellValue('I' . $i, $result["buying_price"]);
            $this->excel->getActiveSheet()->SetCellValue('J' . $i, $result["selling_price"]);
            $this->excel->getActiveSheet()->SetCellValue('K' . $i, $result["employee_name"]);




            $no++;
            $i++;
        }




        $filename = "Stock_Report.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

    public function patient_lab_history() {
        $patient_id = $this->uri->segment(3);

        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['patient_lab_history'] = $this->reports_model->patient_lab_history($patient_id);
        // $this->load->view('patient_lab_history', $data);
        $data1['contents'] = 'patient_lab_history';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function patient_prescription_history() {
        $patient_id = $this->uri->segment(3);

        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['patient_prescription_history'] = $this->reports_model->patient_prescription_history($patient_id);
        // $this->load->view('patient_prescription_history', $data);
        $data1['contents'] = 'patient_prescription_history';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function patient_consultation_history() {
        $patient_id = $this->uri->segment(3);

        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['patient_consultation_history'] = $this->reports_model->patient_consultation_history($patient_id);
        //$this->load->view('patient_consultation_history', $data);
        $data1['contents'] = 'patient_consultation_history';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function workload() {

        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['tests'] = $this->get_tests();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['workload'] = $this->reports_model->workload();
        $this->load->view('workload', $data);
        $data1['contents'] = 'workload';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function patient_allergies() {
        $patient_id = $this->uri->segment(3);
        $allergies = $this->reports_model->patient_allergies($patient_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($allergies)) {
            $no_data = "No Data";

            echo json_encode($no_data);
        } else {
            echo json_encode($allergies);
        }
    }

    public function get_stock_view() {
        $stock_id = $this->uri->segment(3);
        $stock_data = $this->reports_model->get_stock_view($stock_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($stock_data)) {
            $no_data = "No Data";

            echo json_encode($no_data);
        } else {
            echo json_encode($stock_data);
        }
    }

    public function get_commodity_view() {
        $commodity_id = $this->uri->segment(3);
        $stock_data = $this->reports_model->get_commodity_view($commodity_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($stock_data)) {
            $no_data = "No Data";

            echo json_encode($no_data);
        } else {
            echo json_encode($stock_data);
        }
    }

    function list_all() {
        $this->load->library('Datatables');
        $this->datatables
                ->select('commodity_id, commodity_name, commodity_type, batch_no')
                ->from('stock_view');

        $data['result'] = $this->datatables->generate();
        $this->load->view('ajax_v', $data);
    }

}

?>