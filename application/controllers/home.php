<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->library('form_validation', 'session');
        $this->load->helper('url');

        $this->session->userdata('username');
        $this->session->userdata('type');

        $username = $this->session->userdata('username');
        $type = $this->session->userdata('type');

        if ($type == "Support") {
            redirect('reception');
        } else if ($type == "Nurse") {
            redirect('nurse');
        } elseif (empty($type)) {
            redirect('login');
        } elseif ($type === "Laboratory") {
            redirect('lab');
        } elseif ($type === "Doctor") {
            redirect('doctor');
        } elseif ($type === "Pharmacy") {
            redirect('pharmacy');
        }
    }

    public function do_logout() {
        $login_logs_id = $this->session->userdata('login_logs_id');
        $last_inserted_id = $this->session->userdata('last_inserted_id');
        $username = $this->session->userdata('username');
        $in_active = 'In_Active';
        $time_end = date("H:i:s");
        $data = array(
            'is_active' => $in_active,
            'time_end' => $time_end
        );

        $this->db->where('login_logs_id', $login_logs_id);
        $this->db->update('login_logs', $data);


        $update_logout = "SELECT login_logs_id, is_active FROM `login_logs` where login_logs_id < '$last_inserted_id' and user_name='$username' and is_active = 'Active'";
        $query = $this->db->query($update_logout);
        foreach ($query->result() as $value) {
            $login_logs_id = $value->login_logs_id;

            $in_active = "In Active";
            $login_logs_update = array(
                'is_active' => $in_active,
                'time_end' => $time_end
            );
            $this->db->where('login_logs_id', $login_logs_id);
            $this->db->update('login_logs', $login_logs_update);
        }



        $this->session->sess_destroy();
        redirect('login');
    }

    public function get_appointments() {
        $this->load->view('appointments_v');
    }

    public function get_timezones() {
        $this->load->helper('date');
        //echo timezone_menu('UM8');
        $timestamp = '1140153693';
        $timezone = 'UM8';
        $daylight_saving = TRUE;

        echo gmt_to_local($timestamp, $timezone, $daylight_saving);
    }

    public function get_events() {
        //--------------------------------------------------------------------------------------------------
// This script reads event data from a JSON file and outputs those events which are within the range
// supplied by the "start" and "end" GET parameters.
//
// An optional "timezone" GET parameter will force all ISO8601 date stings to a given timezone.
//
// Requires PHP 5.2.0 or higher.
//--------------------------------------------------------------------------------------------------
// Require our Event class and datetime utilities
        //require dirname(__FILE__) . '/utils.php';

        $range_start = '2015-02-01';
        $range_end = '2015-03-15';
        //echo 'Start' . $range_start . 'End' . $range_end . '<br>';
// Short-circuit if the client did not give us a date range.
        if (!isset($range_start) || !isset($range_end)) {
            die("Please provide a date range.");
        }

// Parse the start/end parameters.
// These are assumed to be ISO8601 strings with no time nor timezone, like "2013-12-29".
// Since no timezone will be present, they will parsed as UTC.
        // $range_start = parseDateTime($range_start);
        // $range_end = parseDateTime($range_end);
// Parse the timezone parameter if it is present.
        $timezone = null;
        if (isset($_GET['timezone'])) {
            $timezone = new DateTimeZone($_GET['timezone']);
        }

// Read and parse our events JSON file into an array of event data arrays.
        $json = file_get_contents(dirname(__FILE__) . '/json/events.json');
        $input_arrays = json_decode($json, true);

// Accumulate an output array of event data arrays.
        $output_arrays = array();
        foreach ($input_arrays as $array) {

            $event = $this->load->library('Event', $array, $timezone);
            // Convert the input array into a useful Event object
            // $event = new Event($array, $timezone);
            // If the event is in-bounds, add it to the output
            if ($event->isWithinDayRange($range_start, $range_end)) {
                $output_arrays[] = $event->toArray();
            }
        }
        $this->config->set_item('compress_output', FALSE);
// Send JSON to the client.
        echo json_encode($output_arrays);
    }

}

?>