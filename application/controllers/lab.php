<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lab extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {

        $data['pack'] = $this->package();
        $data['name'] = $this->getName();
        $data['title'] = $this->title();
        $data['name'] = $this->getName();
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $data['tests_name'] = $this->getProcedure();

        //$this->load->view('lab', $data);
        $data1['contents'] = 'lab';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function base_params($data) {
        $data['title'] = 'Laboratory';
        $this->load->view('lab_template', $data);
    }

    public function lab_patient_list_express() {
        $lab_patient_list = $this->lab_model->lab_patient_list_express();
        $this->config->set_item('compress_output', FALSE);
        if (empty($lab_patient_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($lab_patient_list);
        }
    }

    public function lab_patient_list() {
        $lab_patient_list = $this->lab_model->lab_patient_list();
        $this->config->set_item('compress_output', FALSE);
        if (empty($lab_patient_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($lab_patient_list);
        }
    }

    public function lab_walkin_patient_list() {
        $lab_patient_list = $this->lab_model->lab_walkin_patient_list();
        $this->config->set_item('compress_output', FALSE);
        if (empty($lab_patient_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($lab_patient_list);
        }
    }

    public function total_regular_patients_in_tray() {
        $lab_patient_list = $this->lab_model->total_regular_patients_in_tray();
        $this->config->set_item('compress_output', FALSE);
        if (empty($lab_patient_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($lab_patient_list);
        }
    }

    public function total_walkin_patients_in_tray() {
        $total_walkin_patients_in_tray = $this->lab_model->total_walkin_patients_in_tray();
        $this->config->set_item('compress_output', FALSE);
        if (empty($total_walkin_patients_in_tray)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($total_walkin_patients_in_tray);
        }
    }

    public function nurse_patient_list_xpress() {
        $lab_patient_list_xpress = $this->lab_model->lab_patient_list_xpress();
        $this->config->set_item('compress_output', FALSE);
        if (empty($lab_patient_list_xpress)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($lab_patient_list_xpress);
        }
    }

    public function lab_patient_profile() {
        $patient_id = $this->uri->segment(3);
        $visit_id = $this->uri->segment(4);
        $data['pack'] = $this->package();
        $data['name'] = $this->getName();
        $data['title'] = $this->title();
        $cache_data = $this->cached_icd10();
        $data['cache_data'] = $cache_data;
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $data['tests_name'] = $this->getProcedure();
        $data['patient_triage_records'] = $this->patient_triage_records($patient_id);
        $data['patient_lab_records'] = $this->lab_records($patient_id);
        $data['patient_consultation_records'] = $this->consultation_records($patient_id);
        $data['patient_prescription_records'] = $this->prescription_records($patient_id);
        $data['patient_triage_report'] = $this->nurse_model->patient_triage_report($patient_id);
        $data['tests_name'] = $this->reception_model->getProcedure();
        $data['vital_signs'] = $this->reason_for_visit($visit_id);
        $data['appointment_details'] = $this->doctor_model->patient_appointments($patient_id);
        $data['patient_tests'] = $this->patient_tests();
        $data['allergy_records'] = $this->allergy_records($patient_id);
        $data['reason_for_visit'] = $this->reason_for_visit($visit_id);


        //$this->load->view('lab_patient_profile', $data);

        $data1['contents'] = 'lab_patient_profile';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function order_lab_tests() {
        $patient_id = $this->input->post('patient_id_lab');
        $visit_id = $this->input->post('visit_id_lab');
        $lab_tests = $this->input->post('tests');
        $patient_payment_id = $this->get_patient_paymetn_id($visit_id);
        $this->doctor_model->order_lab_tests($patient_id, $visit_id, $lab_tests, $patient_payment_id);
        $this->operations_model->send_to_lab($visit_id);
    }

    public function get_cwd() {
        $working_directory = getcwd();

        return $working_directory;
    }

    public function patient_bio_data($patient_id) {

        $sql = "Select concat(patient.title,':',patient.f_name,' ',patient.s_name,' ',patient.other_name) as patient_name,"
                . "patient.dob,patient.gender,patient.identification_number,patient.phone_no,patient.residence,patient.family_number from patient where patient.patient_id='$patient_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function sick_off_form() {
        $patient_id = $this->input->post('patient_id_sick_off');
        $visit_id = $this->input->post('visit_id_sick_off');
        $sick_off_date_from = $this->input->post('sick_off_date_from');
        $sick_off_date_to = $this->input->post('sick_off_date_to');

        $patient_name = $this->getName();
        $this->doctor_model->sick_off_form($patient_id, $visit_id, $sick_off_date_from, $sick_off_date_to);
        $patient_bio_data = $this->patient_bio_data($patient_id);

        $date_from = new DateTime($sick_off_date_from);
        $date_to = new DateTime($sick_off_date_to);
        $diff = date_diff($date_from, $date_to);
        $no_of_days = $diff->format('%d DayS');


        $current_working_directory = $this->get_cwd();
        $this->load->library('excel');

        $objPHPexcel = PHPExcel_IOFactory::load($current_working_directory . '/files/sick_off_form.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet();


        foreach ($patient_bio_data as $patient_bio) {
            $today = date("l jS \of F Y h:i:s A");
            $patient_name = 'Patient Name: ' . $patient_bio['patient_name'];
            $objWorksheet->getCell('A12')->SetValue($patient_name);
            $today = 'Date: ' . $today;
            $objWorksheet->getCell('A9')->SetValue($today);
            $gender = 'Gender: ' . $patient_bio['gender'];
            $objWorksheet->getCell('A14')->SetValue($gender);
            $dob = $patient_bio['dob'];
        }

        $title = $this->session->userdata('title');
        $f_name = $this->session->userdata('Fname');
        $l_name = $this->session->userdata('Lname');
        $other_name = $this->session->userdata('other_name');
        $doctor_name = 'Medical Practitioner Name: ' . $title . ' ' . $f_name . ' ' . $l_name . ' ' . $other_name;


        $objWorksheet->getCell('C17')->setValue($sick_off_date_from);
        $objWorksheet->getCell('E17')->setValue($sick_off_date_to);
        $objWorksheet->getCell('B20')->setValue($no_of_days);
        $objWorksheet->getCell('B22')->setValue($doctor_name);


        $today = date("d-m-Y");


        $filename = "Shwari.$today.sick_off_form.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

    public function patient_tests() {
        $patient_id = $this->uri->segment(3);
        $visit_id = $this->uri->segment(4);
        $patient_tests = $this->lab_model->patient_tests($patient_id, $visit_id);
        return $patient_tests;
    }

    public function get_lab_test_results() {
        $lab_test_id = $this->uri->segment(3);
        $get_lab_test_results = $this->lab_model->get_lab_test_results($lab_test_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($get_lab_test_results)) {
            
        } else {
            echo json_encode($get_lab_test_results);
        }
    }

    public function add_lab_test_results() {
        $lab_test_id = $this->input->post('lab_test_id');
        $patient_id = $this->input->post('lab_patient_id');
        $visit_id = $this->input->post('lab_patient_visit_id');
        $test_name = $this->input->post('patient_test_name');
        $test_results = $this->input->post('tests_results');


        $patient_lab_test_results = $this->lab_model->update_lab_test_results($patient_id, $visit_id, $lab_test_id, $test_name, $test_results);
        if ($patient_lab_test_results) {
            echo 'Addd';
        } else {
            $this->lab_model->add_lab_test_results($patient_id, $visit_id, $lab_test_id, $test_name, $test_results);
            echo 'Updated...';
        }
    }

   

    public function lab_walkin_profile() {
        $patient_id = $this->uri->segment(3);
        $visit_id = $this->uri->segment(4);
        $data['patient_bio'] = $this->patient_bio();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();

        $data['patient_triage_records'] = $this->patient_triage_records($patient_id);
        $data['patient_lab_records'] = $this->lab_records($patient_id);
        $data['patient_consultation_records'] = $this->consultation_records($patient_id);
        $data['patient_prescription_records'] = $this->prescription_records($patient_id);
        $data['patient_triage_report'] = $this->nurse_model->patient_triage_report($patient_id);
        $data['tests_name'] = $this->reception_model->getProcedure();
        $data['test_list'] = $this->test_list();
        $data['tests_results'] = $this->get_tests_results($patient_id);
        $data1['contents'] = 'lab_walkin_patient_profile';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function add_walkin_tests() {
        $visit_id = $this->input->post('visit_id');
        $patient_id = $this->input->post('patient_id');
        $test_name = $this->input->post('walkin_test');

        $this->lab_model->add_walkin_test($visit_id, $patient_id, $test_name);
    }

    public function add_walkin_tests_notes() {
        $id = $this->input->post('add_procedure_result_id');
        $procedure_notes = $this->input->post('add_procedure_notes');

        $update_procedure_notes = $this->lab_model->update_lab_notes($id, $procedure_notes);
        if ($update_procedure_notes) {
            echo 'Updated Successfully...';
        } else {
            echo 'Update failed';
        }
    }

    public function add_walkin_lab_results_notes() {
        $lab_test_id = $this->input->post('add_test_result_id');
        $lab_results = $this->input->post('add_test_notes');
        $update_lab_result = $this->lab_model->update_walkin_lab_results($lab_test_id, $lab_results);
        if ($update_lab_result) {
            echo 'Update successfull...';
        } else {
            echo 'Update failed';
        }
    }

    public function download_walkin_lab_results() {
        $patient_id = $this->uri->segment(3);
        $visit_id = $this->uri->segment(4);
        $patient_bio_data = $this->get_patient_details($patient_id);
        $visit_type = $this->input->post('visit_type');
        $date_from = $this->input->post('date_from');
        $date_to = $this->input->post('date_to');
        $lab_results = $this->lab_model->get_lab_results($patient_id, $visit_id, $visit_type, $date_from, $date_to);


        $date_from = new DateTime($date_from);
        $date_to = new DateTime($date_to);
        $diff = date_diff($date_from, $date_to);
        $no_of_days = $diff->format('%d DayS');


        $current_working_directory = $this->get_cwd();
        $this->load->library('excel');

        $objPHPexcel = PHPExcel_IOFactory::load($current_working_directory . '/files/shwari_lab_result_template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet();
        $objWorksheet->setTitle('Patient Lab Results');


        foreach ($patient_bio_data as $patient_bio) {
            $today = date("l jS \of F Y h:i:s A");
            $patient_name = 'Patient Name: ' . $patient_bio['patient_name'];
            $objWorksheet->getCell('A10')->SetValue($patient_name);
            $today = 'Date: ' . $today;
            $objWorksheet->getCell('A12')->SetValue($today);
            $gender = 'Gender: ' . $patient_bio['gender'];
            $objWorksheet->getCell('J10')->SetValue($gender);
            $dob = $patient_bio['dob'];
        }

        $objWorksheet->setCellValue('A14', 'Test Name : ');
        $objWorksheet->setCellValue('B14', 'Test Results');
        $objWorksheet->setCellValue('C14', 'Normal Values');


        $this->excel->getActiveSheet()->SetCellValue('H1', "Status : ");
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(15);
        $i = 15;
        $no = 1;
        foreach ($lab_results as $lab_results_details) {
            $objWorksheet->setCellValue('A' . $i, $no);
            $objWorksheet->setCellValue('B' . $i, $lab_results_details["test_name"]);
            $objWorksheet->setCellValue('C' . $i, $lab_results_details["test_result"]);
            $objWorksheet->setCellValue('D' . $i, $lab_results_details["test_range"]);


            $no++;
            $i++;
        }


        $filename = "Shwari.$today.Patient_Lab_Report.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

}
