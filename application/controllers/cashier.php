<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cashier extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();

        $data['patient_bio'] = $this->patient_bio();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        //$this->load->view('cashier', $data);
        $data1['contents'] = 'cashier';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function base_params($data) {
        $data['title'] = 'Cashier';
        $this->load->view('reception_template', $data);
    }

    public function regular_patient_payments() {
        $regular_patinet_payments = $this->cashier_model->regular_patient_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($regular_patinet_payments)) {
            $no_data = "No Data ";
            echo json_encode($no_data);
        } else {
            echo json_encode($regular_patinet_payments);
        }
    }

    public function regular_patient_payments_details() {
        $visit_id = $this->uri->segment(3);
        $patient_id = $this->uri->segment(4);
        $regular_patient_payments_details = $this->cashier_model->regular_patient_payments_details($visit_id, $patient_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($regular_patient_payments_details)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($regular_patient_payments_details);
        }
    }

    public function walkin_patient_nurse_payments() {
        $walkin_patient_payments = $this->cashier_model->walkin_patient_nurse_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($walkin_patient_payments)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($walkin_patient_payments);
        }
    }

    public function walkin_patient_nurse_payments_details() {
        $walkin_id = $this->uri->segment(3);
        $walkin_patient_payments_details = $this->cashier_model->walkin_patient_nurse_payments_details($walkin_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($walkin_patient_payments_details)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($walkin_patient_payments_details);
        }
    }

    public function walkin_patient_lab_payments() {
        $walkin_patient_payments = $this->cashier_model->walkin_patient_lab_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($walkin_patient_payments)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($walkin_patient_payments);
        }
    }

    public function walkin_patient_lab_payments_details() {
        $walkin_id = $this->uri->segment(3);
        $walkin_patient_payments_details = $this->cashier_model->walkin_patient_lab_payments_details($walkin_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($walkin_patient_payments_details)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($walkin_patient_payments_details);
        }
    }

    public function walkin_patient_pharmacy_payments() {
        $walkin_patient_payments = $this->cashier_model->walkin_patient_pharmacy_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($walkin_patient_payments)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($walkin_patient_payments);
        }
    }

    public function walkin_patient_pharmacy_payments_details() {
        $walkin_id = $this->uri->segment(3);
        $walkin_patient_payments_details = $this->cashier_model->walkin_patient_pharmacy_payments_details($walkin_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($walkin_patient_payments_details)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($walkin_patient_payments_details);
        }
    }

    public function get_patient_name() {
        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $patient_name = $this->cashier_model->get_patient_name($patient_id, $visit_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($patient_name)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($patient_name);
        }
    }

    public function get_walkin_patient_name() {

        $walkin_id = $this->uri->segment(3);
        $patient_name = $this->cashier_model->get_walkin_patient_name($walkin_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($patient_name)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($patient_name);
        }
    }

    public function lab_service_payments() {
        $lab_service_payments = $this->cashier_model->lab_service_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($lab_service_payments)) {
            $no_data = "No Data";
            echo json_encode($lab_service_payments);
        } else {
            echo json_encode($lab_service_payments);
        }
    }

    public function lab_service_payments_details() {
        $visit_id = $this->uri->segment(3);
        $patient_id = $this->uri->segment(4);
        $lab_service_payments = $this->cashier_model->lab_service_payments_details($visit_id, $patient_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($lab_service_payments)) {
            $no_data = "No Data";
            echo json_encode(
                    $lab_service_payments);
        } else {
            echo json_encode($lab_service_payments);
        }
    }

    public function lab_service_make_payment() {
        $this->cashier_model->lab_service_make_payment();
    }

    public function pharmacy_service_payments() {
        $pharmacy_service_payments = $this->cashier_model->pharmacy_service_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($pharmacy_service_payments)) {

            $no_data = "No Data";
        } else {
            echo json_encode($pharmacy_service_payments);
        }
    }

    public function pharmacy_service_payments_details() {
        $visit_id = $this->uri->segment(3);
        $patient_id = $this->uri->segment(4);
        $pharmacy_service_payments = $this->cashier_model->pharmacy_service_payments_details($visit_id, $patient_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($pharmacy_service_payments)) {
            $no_data = "No Data";
            echo json_encode(
                    $pharmacy_service_payments);
        } else {
            echo json_encode($pharmacy_service_payments);
        }
    }

    public function pharmacy_service_make_payment() {
        $this->cashier_model->pharmacy_service_make_payment();
    }

    public function procedure_service_payments() {
        $procedure_service_payments = $this->cashier_model->procedure_service_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($procedure_service_payments)) {

            $no_data = "No Data";
        } else {
            echo json_encode($procedure_service_payments);
        }
    }

    public function pay_at_the_end() {
        $pay_at_the_end = $this->cashier_model->pay_at_the_end();
        $this->config->set_item('compress_output', FALSE);
        if (empty($pay_at_the_end)) {
            $no_data = "No Data";

            echo json_encode($no_data);
        } else {
            echo json_encode($pay_at_the_end);
        }
    }

    public function pay_at_the_end_details() {

        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $pay_at_the_end_details = $this->cashier_model->pay_at_the_end_details($visit_id, $patient_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($pay_at_the_end_details)) {
            $no_data = "No Data";
            echo

            json_encode($no_data);
        } else {
            echo json_encode($pay_at_the_end_details);
        }
    }

    public function total_package_payments() {
        $total_package_payments = $this->cashier_model->total_package_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($total_package_payments)) {
            $no_data = "No Data";
            echo json_encode(
                    $no_data);
        } else {
            echo json_encode($total_package_payments);
        }
    }

    public function total_lab_service_payments() {
        $total_lab_service_payments = $this->cashier_model->total_lab_service_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($total_lab_service_payments)) {
            $no_data = "No Data";
            echo json_encode
                    ($no_data);
        } else {
            echo json_encode($total_lab_service_payments);
        }
    }

    public function total_procedure_service_payments() {
        $total_procedure_service_payments = $this->cashier_model->total_procedure_service_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($total_procedure_service_payments)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($total_procedure_service_payments);
        }
    }

    public function total_pharmacy_payments() {
        $total_pharmacy_payments = $this->cashier_model->total_pharmacy_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($total_pharmacy_payments)) {
            $no_data = "No Data";
            echo

            json_encode($no_data);
        } else {
            echo json_encode($total_pharmacy_payments);
        }
    }

    public function total_regular_payments() {
        $total_regular_payments = $this->cashier_model->total_regular_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($total_regular_payments)) {
            $no_data = "No Data";
            echo

            json_encode($no_data);
        } else {
            echo json_encode($total_regular_payments);
        }
    }

    public function total_walkin_payments() {
        $total_walkin_payments = $this->cashier_model->total_walkin_payments();
        $this->config->set_item('compress_output', FALSE);
        if (empty($total_walkin_payments)) {
            $no_data = "No Data";
            echo

            json_encode($no_data);
        } else {
            echo json_encode($total_walkin_payments);
        }
    }

    public function regular_patient_make_payment() {
        $this->cashier_model->regular_patient_make_payment();
    }

    public function pay_at_the_end_make_payment() {
        $this->cashier_model->regular_patient_make_payment();
    }

    public function

    walkin_patient_nurse_make_payment() {
        $this->cashier_model->walkin_patient_nurse_make_payment();
    }

    public
            function walkin_patient_lab_make_payment() {
        $this->cashier_model->walkin_patient_lab_make_payment();
    }

    public function walkin_patient_pharmacy_make_payment() {
        $this->cashier_model->walkin_patient_pharmacy_make_payment();
    }

    public function walkin_patient_visit_statement() {

        $data['patient_payments'] = $this->cashier_model->walkin_patient_visit_statement();
        $data['procedures'] = $this->reports_model->procedure_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        //$this->load->view('walkin_patient_payments', $data);
        $data1['contents'] = 'walkin_patient_payments';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function regular_patient_visit_statement() {


        $data['procedures'] = $this->reports_model->procedure_report();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['title'] = $this->reception_model->title();
        $data['name'] = $this->reception_model->getName();
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['patient_statement'] = $this->cashier_model->patient_statements();

        $data1['contents'] = 'regular_patient_payments';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function get_cwd() {
        $working_directory = getcwd();

        return $working_directory;
    }

    public function walkin_print_patient_receipt() {
        $visit_id = $this->uri->segment(3);

        $patient_id = $this->input->post('patient_id');
        $patient_receipt = $this->cashier_model->walkin_print_patient_receipt($visit_id);
        $patient_visit_contact_info = $this->cashier_model->walkin_get_patient_receipt_details($visit_id);
        $current_working_directory = $this->get_cwd();
        $this->load->library('excel');

        $objPHPexcel = PHPExcel_IOFactory::load($current_working_directory . '/files/shwari_receipt.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet();


        foreach ($patient_visit_contact_info as $patient_receipt_info) {
            $today = date("l jS \of F Y h:i:s A");
            $objWorksheet->getCell('E5')->SetValue($patient_receipt_info["receipt_no"]);
            $objWorksheet->getCell('E6')->SetValue($today);
            $objWorksheet->getCell('B10')->SetValue($patient_receipt_info["walkin_patient_name"]);
            $objWorksheet->getCell('B14')->SetValue($patient_receipt_info["walkin_phone_no"]);
        }

        $i = 18;
        foreach ($patient_receipt as $result) {
            $total_amount = $result['amount'];
            $quantity = $result['quantity'];
            if ($quantity == 0.0) {
                $quantity = 1;
                $unit_price = $total_amount / $quantity;

                if ($unit_price == 0.0) {
                    $unit_price = 0;
                    $objWorksheet->getCell('A' . $i)->SetValue(1);
                    $objWorksheet->getCell('B' . $i)->SetValue($result["payment_method"]);
                    $objWorksheet->getCell('C' . $i)->SetValue($result["description"]);
                    $objWorksheet->getCell('E' . $i)->SetValue($unit_price);
                    $objWorksheet->getCell('F' . $i)->SetValue($result["discount"]);
                    $discount = $result['discount'];
                    $total_amount_to_pay = $total_amount - $discount;
                    $objWorksheet->getCell('G' . $i)->SetValue($total_amount_to_pay);
                } else {
                    $objWorksheet->getCell('A' . $i)->SetValue(1);
                    $objWorksheet->getCell('B' . $i)->SetValue($result["payment_method"]);
                    $objWorksheet->getCell('C' . $i)->SetValue($result["description"]);
                    $objWorksheet->getCell('E' . $i)->SetValue($unit_price);
                    $objWorksheet->getCell('F' . $i)->SetValue($result["discount"]);
                    $discount = $result['discount'];
                    $total_amount_to_pay = $total_amount - $discount;
                    $objWorksheet->getCell('G' . $i)->SetValue($total_amount_to_pay);
                }
            } else {
                $unit_price = $total_amount / $quantity;

                if ($unit_price == 0.0) {
                    $unit_price = 0;
                    $objWorksheet->getCell('A' . $i)->SetValue($result["quantity"]);
                    $objWorksheet->getCell('B' . $i)->SetValue($result["payment_method"]);
                    $objWorksheet->getCell('C' . $i)->SetValue($result["description"]);
                    $objWorksheet->getCell('E' . $i)->SetValue($unit_price);
                    $objWorksheet->getCell('F' . $i)->SetValue($result["discount"]);
                    $discount = $result['discount'];
                    $total_amount_to_pay = $total_amount - $discount;
                    $objWorksheet->getCell('G' . $i)->SetValue($total_amount_to_pay);
                } else {
                    $objWorksheet->getCell('A' . $i)->SetValue($result["quantity"]);
                    $objWorksheet->getCell('B' . $i)->SetValue($result["payment_method"]);
                    $objWorksheet->getCell('C' . $i)->SetValue($result["description"]);
                    $objWorksheet->getCell('E' . $i)->SetValue($unit_price);
                    $objWorksheet->getCell('F' . $i)->SetValue($result["discount"]);
                    $discount = $result['discount'];
                    $total_amount_to_pay = $total_amount - $discount;
                    $objWorksheet->getCell('G' . $i)->SetValue($total_amount_to_pay);
                }
            }


            $i++;
        }


        //$objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel2007');


        $today = date("d-m-Y");


        $filename = "Shwari.$today.patient_receipt.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

    public function print_patient_receipt() {
        $visit_id = $this->uri->segment(3);

        $patient_id = $this->input->post('patient_id');
        $patient_receipt = $this->cashier_model->print_patient_receipt($visit_id);
        $patient_visit_contact_info = $this->cashier_model->get_patient_receipt_details($visit_id);
        $current_working_directory = $this->get_cwd();
        $this->load->library('excel');

        $objPHPexcel = PHPExcel_IOFactory::load($current_working_directory . '/files/shwari_receipt.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet();


        foreach ($patient_visit_contact_info as $patient_receipt_info) {
            $today = date("l jS \of F Y h:i:s A");
            $objWorksheet->getCell('E5')->SetValue($patient_receipt_info["receipt_no"]);
            $objWorksheet->getCell('E6')->SetValue($today);
            $objWorksheet->getCell('E7')->SetValue($patient_receipt_info["phone_no"]);
            $objWorksheet->getCell('B10')->SetValue($patient_receipt_info["patient_name"]);
            $objWorksheet->getCell('B11')->SetValue($patient_receipt_info["company_name"]);
            $objWorksheet->getCell('B12')->SetValue($patient_receipt_info["residence"]);
            $objWorksheet->getCell('B13')->SetValue($patient_receipt_info["residence"]);
            $objWorksheet->getCell('B14')->SetValue($patient_receipt_info["phone_no"]);
        }

        $i = 18;
        foreach ($patient_receipt as $result) {
            $total_amount = $result['amount'];
            $quantity = $result ['quantity'];
            $unit_price = $total_amount / $quantity;

            $objWorksheet->getCell('A' . $i)->SetValue($result["quantity"]);
            $objWorksheet->getCell('B' . $i)->SetValue($result["payment_method"]);
            $objWorksheet->getCell('C' . $i)->SetValue($result["description"]);
            $objWorksheet->getCell('E' . $i)->SetValue($unit_price);
            $objWorksheet->getCell('F' . $i)->SetValue($result ["discount"]);
            $discount = $result ['discount'];
            $total_amount_to_pay = $total_amount - $discount;
            $objWorksheet->getCell('G' . $i)->SetValue($total_amount_to_pay);

            $i++;
        }


        //$objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel2007');


        $today = date("d-m-Y");


        $filename = "Shwari.$today.patient_receipt.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

}
