<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Doctor extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {

        $data['pack'] = $this->package();
        $data['name'] = $this->getName();
        $data['title'] = $this->title();
        $data['name'] = $this->getName();
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $data['procedure_name'] = $this->getProcedure();

        //$this->load->view('doctor', $data);
        $data1['contents'] = 'doctor';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function add_remove_table() {
        $this->load->view('add_remove');
    }
    
    function base_params($data) {
        $data['title'] = 'Doctor';
        $this->load->view('doctor_template', $data);
    }

    public function doctor_patient_list() {
        $doctor_patient_list = $this->doctor_model->doctor_patient_list();
        $this->config->set_item('compress_output', FALSE);
        if (empty($doctor_patient_list)) {
            
        } else {
            echo json_encode($doctor_patient_list);
        }
    }

    public function doctor_patient_list_xpress() {
        $doctor_patient_list = $this->doctor_model->doctor_patient_list_xpress();
        $this->config->set_item('compress_output', FALSE);
        if (empty($doctor_patient_list)) {
            
        } else {
            echo json_encode($doctor_patient_list);
        }
    }

    public function doctor_patient_profile() {
        $patient_id = $this->uri->segment(3);
        $visit_id = $this->uri->segment(4);
        $data['pack'] = $this->package();
        $data['name'] = $this->getName();
        $data['title'] = $this->title();
        $cache_data = $this->cached_icd10();
        $data['cache_data'] = $cache_data;
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $data['procedure_name'] = $this->getProcedure();
        $data['patient_triage_records'] = $this->patient_triage_records($patient_id);
        $data['patient_lab_records'] = $this->lab_records($patient_id);
        $data['patient_consultation_records'] = $this->consultation_records($patient_id);
        $data['patient_prescription_records'] = $this->prescription_records($patient_id);
        $data['patient_triage_report'] = $this->nurse_model->patient_triage_report($patient_id);
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['vital_signs'] = $this->reason_for_visit($visit_id);
        $data['appointment_details'] = $this->doctor_model->patient_appointments($patient_id);
        $data['reason_for_visit'] = $this->reason_for_visit($visit_id);
        $data['allergy_records'] = $this->allergy_records($patient_id);
        $data['commodities'] = $this->commodities();
        $data['frequency'] = $this->frequency();
        $data['route'] = $this->route();
        $data['occurence'] = $this->occurence();
        $data['icd10_list'] = $this->get_icd10_list();
        //  $this->load->view('doctor_patient_profile', $data);
        $data1['contents'] = 'doctor_patient_profile';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function release_patient() {
        $visit_id = $this->input->post('release_patient_visit_id');
        $this->operations_model->release_patient($visit_id);
    }

    public function order_lab_tests() {
        $patient_id = $this->input->post('patient_id_lab');
        $visit_id = $this->input->post('visit_id_lab');
        $lab_tests = $this->input->post('tests');
        $patient_payment_id = $this->get_patient_paymetn_id($visit_id);
        $this->doctor_model->order_lab_tests($patient_id, $visit_id, $lab_tests, $patient_payment_id);
        $this->operations_model->send_to_lab($visit_id);
    }

    public function order_commodity() {
        $commodity_name = $this->input->post('commodity_name');
        $strength = $this->input->post('Strength');
        $route = $this->input->post('route');
        $frequency = $this->input->post('frequency');
        $occurence = $this->input->post('no_of_occurence');
        $duration = $this->input->post('Duration');
        $visit_id = $this->input->post('order_commodiy_visit_id');
        $patient_id = $this->input->post('order_commodity_patient_id');
        echo 'Commodity name' . $commodity_name . '<br> Strenghth' . $strength . '<br> Route' . $route . '<br> Frequency ' . $frequency . '</br> Occurence ' . $occurence . '<br>Duration ' . $duration . '<br> Visit ID ' . $visit_id . '<br> Patient id ' . $patient_id . '<br>';
        $this->doctor_model->order_commodity($commodity_name, $strength, $route, $frequency, $occurence, $duration, $visit_id, $patient_id);
    }

    public function chief_complaints() {
        $visit_id = $this->input->post('visit_id');
        $patient_id = $this->input->post('patient_id');
        $chief_complaints = $this->input->post('chief_complaints');
        $update_chief_complaints = $this->doctor_model->update_chief_complaints($visit_id, $patient_id, $chief_complaints);
        if ($update_chief_complaints) {
            
        } else {
            $this->doctor_model->insert_chief_complaints($visit_id, $patient_id, $chief_complaints);
        }
    }

    public function review_of_systems() {
        $patient_id = $this->input->post('patient_id');
        $visit_id = $this->input->post('visit_id');
        $constitutional = $this->input->post('constitutional');
        $ent = $this->input->post('ent');
        $cardiovascular = $this->input->post('cardio_vascular');
        $eye = $this->input->post('eye');
        $respiratory = $this->input->post('respiratory');
        $gastrointestinal = $this->input->post('gastro_intestinal');
        $genitourinary = $this->input->post('genito_urinary');
        $masculoskeletal = $this->input->post('masculo_skeletal');
        $skin = $this->input->post('skin');
        $neurologic = $this->input->post('neuro_logic');
        $other_systems = $this->input->post('other_systems');

        $review_of_systems = $this->doctor_model->update_review_of_systems($patient_id, $visit_id, $constitutional, $ent, $cardiovascular, $eye, $respiratory, $gastrointestinal, $genitourinary, $masculoskeletal, $skin, $neurologic, $other_systems);
        if ($review_of_systems) {
            
        } else {
            $this->doctor_model->insert_review_of_systems($patient_id, $visit_id, $constitutional, $ent, $cardiovascular, $eye, $respiratory, $gastrointestinal, $genitourinary, $masculoskeletal, $skin, $neurologic, $other_systems);
        }
    }

    public function working_diagnosis() {
        $visit_id = $this->input->post('visit_id');
        $patient_id = $this->input->post('patient_id');
        $working_diagnosis = $this->input->post('working_diagnosis');
        $patient_icd_10_code = $this->input->post('patient_icd_10_code');
        //$this->doctor_model->patient_icd_10_code($patient_id, $visit_id, $patient_icd_10_code);
        $update_working_diagnosis = $this->doctor_model->update_working_diagnosis($visit_id, $patient_id, $working_diagnosis, $patient_icd_10_code);
        if ($update_working_diagnosis) {
            
        } else {
            $this->doctor_model->insert_working_diagnosis($visit_id, $patient_id, $working_diagnosis, $patient_icd_10_code);
        }
    }

    public function patient_icd_10() {
        $visit_id = $this->input->post('visit_id');
        $patient_id = $this->input->post('patient_id');

        $patient_icd_10_code = $this->input->post('patient_icd_10_code');
        $update_working_diagnosis = $this->doctor_model->update_patient_icd10_code($visit_id, $patient_id, $patient_icd_10_code);
        if ($update_working_diagnosis) {
            
        } else {
            $this->doctor_model->insert_patient_icd10_code($visit_id, $patient_id, $patient_icd_10_code);
        }
    }

    public function get_cwd() {
        $working_directory = getcwd();

        return $working_directory;
    }

    public function patient_bio_data($patient_id) {

        $sql = "Select concat(patient.title,':',patient.f_name,' ',patient.s_name,' ',patient.other_name) as patient_name,"
                . "patient.dob,patient.gender,patient.identification_number,patient.phone_no,patient.residence,patient.family_number from patient where patient.patient_id='$patient_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function sick_off_form() {
        $patient_id = $this->input->post('patient_id_sick_off');
        $visit_id = $this->input->post('visit_id_sick_off');
        $sick_off_date_from = $this->input->post('sick_off_date_from');
        $sick_off_date_to = $this->input->post('sick_off_date_to');

        $patient_name = $this->getName();
        $this->doctor_model->sick_off_form($patient_id, $visit_id, $sick_off_date_from, $sick_off_date_to);
        $patient_bio_data = $this->patient_bio_data($patient_id);

        $date_from = new DateTime($sick_off_date_from);
        $date_to = new DateTime($sick_off_date_to);
        $diff = date_diff($date_from, $date_to);
        $no_of_days = $diff->format('%d DayS');


        $current_working_directory = $this->get_cwd();
        $this->load->library('excel');

        $objPHPexcel = PHPExcel_IOFactory::load($current_working_directory . '/files/sick_off_form.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet();


        foreach ($patient_bio_data as $patient_bio) {
            $today = date("l jS \of F Y h:i:s A");
            $patient_name = 'Patient Name: ' . $patient_bio['patient_name'];
            $objWorksheet->getCell('A12')->SetValue($patient_name);
            $today = 'Date: ' . $today;
            $objWorksheet->getCell('A9')->SetValue($today);
            $gender = 'Gender: ' . $patient_bio['gender'];
            $objWorksheet->getCell('A14')->SetValue($gender);
            $dob = $patient_bio['dob'];
        }

        $title = $this->session->userdata('title');
        $f_name = $this->session->userdata('Fname');
        $l_name = $this->session->userdata('Lname');
        $other_name = $this->session->userdata('other_name');
        $doctor_name = 'Medical Practitioner Name: ' . $title . ' ' . $f_name . ' ' . $l_name . ' ' . $other_name;


        $objWorksheet->getCell('C17')->setValue($sick_off_date_from);
        $objWorksheet->getCell('E17')->setValue($sick_off_date_to);
        $objWorksheet->getCell('B20')->setValue($no_of_days);
        $objWorksheet->getCell('B22')->setValue($doctor_name);


        $today = date("d-m-Y");


        $filename = "Shwari.$today.sick_off_form.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

    public function patient_referral_form() {
        $patient_id = $this->input->post('patient_id_other');
        $visit_id = $this->input->post('visit_id_other');
        $referral_to = $this->input->post('referral_to');
        $major_complaints = $this->input->post('major_complaint');
        $diagnosis = $this->input->post('diagnosis');
        $special_instructions = $this->input->post('special_instrations');
        $doctor_comments = $this->input->post('referring_doctor_comments');
        $doctor_id = $this->session->userdata('id');
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $patient_name = $this->getName();
        $this->doctor_model->referral_request_form($patient_id, $visit_id, $referral_to, $major_complaints, $diagnosis, $special_instructions, $doctor_comments, $doctor_id, $member_id, $branch_id);
        $patient_bio_data = $this->patient_bio_data($patient_id);




        $current_working_directory = $this->get_cwd();
        $this->load->library('excel');

        $objPHPexcel = PHPExcel_IOFactory::load($current_working_directory . '/files/referral_request_form.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet();


        foreach ($patient_bio_data as $patient_bio) {
            $today = date("l jS \of F Y h:i:s A");
            $patient_name = 'Patient Name: ' . $patient_bio['patient_name'];
            // $patient_name = 'Patient Name: ' . $patient_bio["title"] . ' : ' . $patient_bio["f_name"] . ' ' . $patient_bio["s_name"] . ' ' . $patient_bio["other_name"];
            $objWorksheet->getCell('A12')->SetValue($patient_name);
            $today = 'Date: ' . $today;
            $objWorksheet->getCell('A9')->SetValue($today);
            //$objWorksheet->getCell('A9')->SetValue($today);
            $id_no = 'ID No:' . $patient_bio['identification_number'];
            $objWorksheet->getCell('C13')->SetValue($id_no);
            $gender = 'Gender: ' . $patient_bio['gender'];
            $objWorksheet->getCell('B14')->SetValue($gender);
            $dob = $patient_bio['dob'];
            $age = date_diff(date_create($dob), date_create('now'))->y;

            if ($age <= 0) {
                $bage = date_diff(date_create($dob), date_create('now'))->d;
                $nage = $bage . " Days Old";
            } elseif ($age > 0) {
                $nage = $age . " Years Old";
            }
            $nage = 'Age:' . $nage;
            $dob = 'Date of Birth: ' . $dob;

            $objWorksheet->getCell('C14')->setValue($nage);
            $objWorksheet->getCell('A14')->setValue($dob);
        }

        $referral_to = '' . $referral_to;
        $major_complaints = '' . $major_complaints;
        $diagnosis = '' . $diagnosis;
        $special_instructions = '' . $special_instructions;
        $doctor_comments = "" . $doctor_comments;
        $title = $this->session->userdata('title');
        $f_name = $this->session->userdata('Fname');
        $l_name = $this->session->userdata('Lname');
        $other_name = $this->session->userdata('other_name');
        $doctor_name = 'Doctor Name: ' . $title . ' ' . $f_name . ' ' . $l_name . ' ' . $other_name;


        $objWorksheet->getCell('A20')->setValue($referral_to);
        $objWorksheet->getCell('A24')->setValue($major_complaints);
        $objWorksheet->getCell('A28')->setValue($diagnosis);
        $objWorksheet->getCell('A32')->setValue($special_instructions);
        $objWorksheet->getCell('A36')->setValue($doctor_comments);
        $objWorksheet->getCell('A40')->setValue($doctor_name);



        $today = date("d-m-Y");


        $filename = "Shwari.$today.patient_imaging_request.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

    public function imaging_request_form() {
        $patient_id = $this->input->post('patient_id_imaging');
        $visit_id = $this->input->post('visit_id_imaging');
        $imaging_test = $this->input->post('imaging_test');
        $major_complaints = $this->input->post('major_complaint');
        $diagnosis = $this->input->post('diagnosis');
        $special_instructions = $this->input->post('special_instrations');
        $doctor_comments = $this->input->post('referring_doctor_comments');
        $doctor_id = $this->session->userdata('id');
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $patient_name = $this->getName();
        $this->doctor_model->imaging_request_form($patient_id, $visit_id, $imaging_test, $major_complaints, $diagnosis, $special_instructions, $doctor_comments, $doctor_id, $member_id, $branch_id);
        $patient_bio_data = $this->patient_bio_data($patient_id);




        $current_working_directory = $this->get_cwd();
        $this->load->library('excel');

        $objPHPexcel = PHPExcel_IOFactory::load($current_working_directory . '/files/imaging_request_form.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet();


        foreach ($patient_bio_data as $patient_bio) {
            $today = date("l jS \of F Y h:i:s A");
            $patient_name = 'Patient Name: ' . $patient_bio['patient_name'];
            // $patient_name = 'Patient Name: ' . $patient_bio["title"] . ' : ' . $patient_bio["f_name"] . ' ' . $patient_bio["s_name"] . ' ' . $patient_bio["other_name"];
            $objWorksheet->getCell('A9')->SetValue($patient_name);
            $today = 'Date: ' . $today;
            $objWorksheet->getCell('A6')->SetValue($today);
            $id_no = 'ID No:' . $patient_bio['identification_number'];
            $objWorksheet->getCell('C9')->SetValue($id_no);
            $gender = 'Gender: ' . $patient_bio['gender'];
            $objWorksheet->getCell('B11')->SetValue($patient_bio["gender"]);
            $dob = $patient_bio['dob'];
            $age = date_diff(date_create($dob), date_create('now'))->y;

            if ($age <= 0) {
                $bage = date_diff(date_create($dob), date_create('now'))->d;
                $nage = $bage . " Days Old";
            } elseif ($age > 0) {
                $nage = $age . " Years Old";
            }
            $nage = 'Age:' . $nage;
            $dob = 'Date of Birth: ' . $dob;

            $objWorksheet->getCell('C11')->setValue($nage);
            $objWorksheet->getCell('A11')->setValue($dob);
        }

        $imaging_test = '' . $imaging_test;
        $major_complaints = '' . $major_complaints;
        $diagnosis = '' . $diagnosis;
        $special_instructions = '' . $special_instructions;
        $doctor_comments = "" . $doctor_comments;
        $title = $this->session->userdata('title');
        $f_name = $this->session->userdata('Fname');
        $l_name = $this->session->userdata('Lname');
        $other_name = $this->session->userdata('other_name');
        $doctor_name = 'Doctor Name: ' . $title . ' ' . $f_name . ' ' . $l_name . ' ' . $other_name;


        $objWorksheet->getCell('A16')->setValue($imaging_test);
        $objWorksheet->getCell('A22')->setValue($major_complaints);
        $objWorksheet->getCell('A27')->setValue($diagnosis);
        $objWorksheet->getCell('A32')->setValue($special_instructions);
        $objWorksheet->getCell('A37')->setValue($doctor_comments);
        $objWorksheet->getCell('A42')->setValue($doctor_name);



        $today = date("d-m-Y");


        $filename = "Shwari.$today.patient_imaging_request.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

    public function consultation_codes() {
        $this->load->view('auto_complete');
    }

    public function auto_icd_10() {
        $returned_value = $this->uri->segment(3);
        $get_icd_10 = $this->doctor_model->icd_10_codes($returned_value);
        $get_icd_10_2 = $this->doctor_model->icd_10_codes_2($returned_value);
        $this->config->set_item('compress_output', FALSE);
        if (empty($get_icd_10)) {
            echo json_encode($get_icd_10_2);
        } else {
            echo json_encode($get_icd_10);
        }
    }

    public function get_review_of_systems() {
        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $get_review_of_systems = $this->doctor_model->get_review_of_systems($patient_id, $visit_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($get_review_of_systems)) {
            $empty_data = "Data";
            echo json_encode($empty_data);
        } else {
            echo json_encode($get_review_of_systems);
        }
    }

    public function get_chief_complaints() {
        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $get_chief_complaints = $this->doctor_model->get_chief_complaints($patient_id, $visit_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($get_chief_complaints)) {
            $empty_data = "Data";
            echo json_encode($empty_data);
        } else {
            echo json_encode($get_chief_complaints);
        }
    }

    public function form() {
        //Fetch all ICD10 Codes from the cache file which is icd10
        $cache_data = $this->cached_icd10();
        $data['cache_data'] = $cache_data;
        $this->load->view('form', $data);
    }

    public function multiple_select() {
        $this->load->view('multiple_select');
    }

}
