<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pharmacy extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {

        $data['pack'] = $this->package();
        $data['name'] = $this->getName();
        $data['title'] = $this->title();
        $data['name'] = $this->getName();
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $data['procedure_name'] = $this->getProcedure();

        // $this->load->view('pharmacy', $data);
        //$this->load->view('walkin', $data);
        $data1['contents'] = 'pharmacy';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    function base_params($data) {

        $data['title'] = 'Pharmacy';
        $this->load->view('pharmacy_template', $data);
    }

    public function pharmacy_patient_profile() {

        $patient_id = $this->uri->segment(3);
        $visit_id = $this->uri->segment(4);
        $data['pack'] = $this->package();
        $data['name'] = $this->getName();
        $data['title'] = $this->title();
        $cache_data = $this->cached_icd10();
        $data['cache_data'] = $cache_data;
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $data['procedure_name'] = $this->getProcedure();
        $data['patient_triage_records'] = $this->patient_triage_records($patient_id);
        $data['patient_lab_records'] = $this->lab_records($patient_id);
        $data['allergy_records'] = $this->allergy_records($patient_id);
        $data['patient_consultation_records'] = $this->consultation_records($patient_id);
        $data['patient_prescription_records'] = $this->prescription_records($patient_id);
        $data['bill_patient_records'] = $this->bill_patient_prescription($patient_id, $visit_id);
        $data['patient_triage_report'] = $this->nurse_model->patient_triage_report($patient_id);
        $data['procedure_name'] = $this->reception_model->getProcedure();
        $data['vital_signs'] = $this->reason_for_visit($visit_id);
        $data['appointment_details'] = $this->doctor_model->patient_appointments($patient_id);
        $data['reason_for_visit'] = $this->reason_for_visit($visit_id);
        $data['current_prescription_records'] = $this->current_prescription_records($visit_id);
        $data['commodities'] = $this->commodities();
        $data['frequency'] = $this->frequency();
        $data['route'] = $this->route();
        $data['occurence'] = $this->occurence();
        $data['dispensing_data'] = $this->patient_dispensing_data($visit_id);
        $data1['contents'] = 'pharmacy_patient_profile';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function bill_patient() {
        $patient_id = $this->input->post('bill_patient_patient_id');
        $visit_id = $this->input->post('bill_patient_visit_id');
        $commodity = $this->input->post('bill_commodity_name');
        $quantity_available = $this->input->post('inputQuantityavailable');
        $quantity_issued = $this->input->post('inputquantity_billed');
        $selling_price = $this->input->post('inputsellingprice');
        $billed_price = $this->input->post('input_total_cost');
        $service_charge = $this->input->post('service_charged');
        $transaction_id = $this->input->post('inputtransaction_id');
        $prescription_tracker = $this->input->post('presc_prescription_tracker_2');
        $this->Pharmacy_model->bill_patient($transaction_id, $prescription_tracker, $patient_id, $visit_id, $commodity, $quantity_available, $quantity_issued, $selling_price, $billed_price, $service_charge);
    }

    public function bill_walkin_patient() {
        $patient_id = $this->input->post('bill_patient_patient_id');
        $visit_id = $this->input->post('bill_patient_visit_id');
        $commodity = $this->input->post('bill_commodity_name');
        $quantity_available = $this->input->post('inputQuantityavailable');
        $quantity_issued = $this->input->post('inputquantity_billed');
        $selling_price = $this->input->post('inputsellingprice');
        $billed_price = $this->input->post('input_total_cost');
        $service_charge = $this->input->post('service_charged');
        $transaction_id = $this->input->post('inputtransaction_id');
        $prescription_tracker = $this->input->post('presc_prescription_tracker_2');
        $this->Pharmacy_model->bill_patient($transaction_id, $prescription_tracker, $patient_id, $visit_id, $commodity, $quantity_available, $quantity_issued, $selling_price, $billed_price, $service_charge);
    }

    public function get_available_commodities() {
        $transaction_id = $this->uri->segment(3);
        $get_available_commodities = $this->Pharmacy_model->get_available_commodities($transaction_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($get_available_commodities)) {
            echo json_encode($get_available_commodities);
        } else {
            echo json_encode($get_available_commodities);
        }
    }

    public function get_patient_visit_stmnt() {
        $prescription_tracker = $this->input->post('presc_prescription_tracker_2');
        $get_patient_visit_stmnt = $this->Pharmacy_model->get_patient_visit_stmnt($prescription_tracker);
        $this->config->set_item('compress_output', FALSE);
        if (empty($get_patient_visit_stmnt)) {
            echo json_encode($get_patient_visit_stmnt);
        } else {
            echo json_encode($get_patient_visit_stmnt);
        }
    }

    public function get_presciprion_details() {
        $prescription_id = $this->uri->segment(3);

        $get_presciprion_details = $this->Pharmacy_model->get_presciprion_details($prescription_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($get_presciprion_details)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($get_presciprion_details);
        }
    }

    public function get_available_commodity() {
        $commodity_id = $this->uri->segment(3);

        $get_available_commodity = $this->Pharmacy_model->get_available_commodity($commodity_id);
        $this->config->set_item('compress_output', FALSE);
        if (empty($get_available_commodity)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($get_available_commodity);
        }
    }

    public function pharmacy_patient_list() {
        $pharmacy_patient_list = $this->Pharmacy_model->pharmacy_patient_list();
        $this->config->set_item('compress_output', FALSE);
        if (empty($pharmacy_patient_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($pharmacy_patient_list);
        }
    }

    public function pharmacy_walkin_patient_list() {
        $pharmacy_patient_list = $this->Pharmacy_model->pharmacy_walkin_patient_list();
        $this->config->set_item('compress_output', FALSE);
        if (empty($pharmacy_patient_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($pharmacy_patient_list);
        }
    }

    public function total_regular_patients_in_tray() {
        $pharmacy_patient_list = $this->Pharmacy_model->total_regular_patients_in_tray();
        $this->config->set_item('compress_output', FALSE);
        if (empty($pharmacy_patient_list)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($pharmacy_patient_list);
        }
    }

    public function total_walkin_patients_in_tray() {
        $total_walkin_patients_in_tray = $this->Pharmacy_model->total_walkin_patients_in_tray();
        $this->config->set_item('compress_output', FALSE);
        if (empty($total_walkin_patients_in_tray)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($total_walkin_patients_in_tray);
        }
    }

    public function pharmacy_patient_list_xpress() {
        $pharmacy_patient_list_xpress = $this->Pharmacy_model->pharmacy_patient_list_xpress();
        $this->config->set_item('compress_output', FALSE);
        if (empty($pharmacy_patient_list_xpress)) {
            $no_data = "No Data";
            echo json_encode($no_data);
        } else {
            echo json_encode($pharmacy_patient_list_xpress);
        }
    }

    public function commodity_managements() {
        $start = $this->uri->segment(3);
        $end = $this->uri->segment(4);
        if (empty($start) and empty($end)) {
            $sql = "SELECT * FROM `commodity_view` order by commodity_name ";
        } else {
            $sql = "SELECT * FROM `commodity_view`where date_added between '$start' and '$end' order by commodity_name ";
        }

        $query = $this->db->query($sql);
        $result = $query->result();

        foreach ($result AS $data):
            $return[] = $data;
        endforeach;
        if (!empty($return)) {
            echo json_encode($return);
        } else {
            echo '[]';
        }
    }

    public function commodity_view() {
        $this->load->view('commodity_view');
    }

    public function commodity_management() {
        $data['pack'] = $this->package();
        $data['name'] = $this->getName();
        $data['title'] = $this->title();
        $data['name'] = $this->getName();
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $data['procedure_name'] = $this->getProcedure();
        $data['commodities'] = $this->commodity_managements();
        $data1['contents'] = 'commodity_management';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function stock_management() {
        $data['pack'] = $this->package();
        $data['name'] = $this->getName();
        $data['title'] = $this->title();
        $data['name'] = $this->getName();
        $data['tests'] = $this->get_tests();
        $data['patient_bio'] = $this->patient_bio();
        $data['procedure_name'] = $this->getProcedure();
        $data['stocks'] = $this->stock_management();
        // $this->load->view('stock_management', $data);
        //$this->load->view('walkin', $data);
        $data1['contents'] = 'stock_management';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function patient_dispense_commodity() {
        $patient_visit_statement_id = $this->input->post('dispensing_form_patient_visit_statement_id');
        $prescription_tracker = $this->input->post('dispensing_form_prescription_tracker');
        $prescription_id = $this->input->post('dispensing_form_prescription_id');
        $patient_id = $this->input->post('dispensing_form_patient_id');
        $visit_id = $this->input->post('dispensing_form_visit_id');
        $transaction_id = $this->input->post('dispensing_form_transaction_id');
        $commodity_name = $this->input->post('dispensing_form_commodity_name');
        $stock_id = $this->input->post('dispensing_form_stock_id');
        $available_quantity = $this->input->post('dispensing_form_available_quantity');
        $batch_no = $this->input->post('dispensing_form_batch_no');
        $quantity_issued = $this->input->post('dispensing_quantity_issued');
        $visit_type = $this->input->post('dispense_visit_type');

        $update_patient_dispense_commodity = $this->Pharmacy_model->update_patient_dispense_commodity($patient_visit_statement_id, $prescription_tracker, $prescription_id, $patient_id
                , $visit_id, $transaction_id, $commodity_name, $stock_id, $available_quantity, $batch_no, $quantity_issued,$visit_type);
        if ($update_patient_dispense_commodity) {
            
        } else {
            $insert_patient_dispense_commodity = $this->Pharmacy_model->insert_patient_dispense_commodity($patient_visit_statement_id, $prescription_tracker, $prescription_id, $patient_id
                    , $visit_id, $transaction_id, $commodity_name, $stock_id, $available_quantity, $batch_no, $quantity_issued,$visit_type);
        }
    }

    public function walkin_paitent_dispense_commodity() {
        
    }
    
    public function walkin_patient_profile() {

        $patient_id = $this->uri->segment(3);
        $visit_id = $this->uri->segment(4);
        $data['patient_bio'] = $this->patient_bio();
        $data['pack'] = $this->reception_model->package();
        $data['name'] = $this->reception_model->getName();
        $data['dispensing_data'] = $this->patient_dispensing_data($visit_id);
        $data['patient_triage_records'] = $this->patient_triage_records($patient_id);
        $data['patient_lab_records'] = $this->lab_records($patient_id);
        $data['patient_consultation_records'] = $this->consultation_records($patient_id);
        $data['patient_prescription_records'] = $this->prescription_records($patient_id);
        $data['patient_triage_report'] = $this->nurse_model->patient_triage_report($patient_id);
        $data['tests_name'] = $this->reception_model->getProcedure();
        $data['test_list'] = $this->test_list();
        $data['tests_results'] = $this->get_tests_results($patient_id);
        $data['commodities'] = $this->commodities();
        $data['frequency'] = $this->frequency();
        $data['route'] = $this->route();
        $data['occurence'] = $this->occurence();
        $data['bill_patient_records'] = $this->bill_walkin_patient_prescription($patient_id, $visit_id);

        $data1['contents'] = 'pharmacy_walkin_patient_profile';
        $finaldata = array_merge($data, $data1);
        $this->base_params($finaldata);
    }

    public function order_commodity() {
        $commodity_name = $this->input->post('commodity_name');
        $strength = $this->input->post('Strength');
        $route = $this->input->post('route');
        $frequency = $this->input->post('frequency');
        $occurence = $this->input->post('no_of_occurence');
        $duration = $this->input->post('Duration');
        $visit_id = $this->input->post('order_commodiy_visit_id');
        $patient_id = $this->input->post('order_commodity_patient_id');
        echo 'Commodity name' . $commodity_name . '<br> Strenghth' . $strength . '<br> Route' . $route . '<br> Frequency ' . $frequency . '</br> Occurence ' . $occurence . '<br>Duration ' . $duration . '<br> Visit ID ' . $visit_id . '<br> Patient id ' . $patient_id . '<br>';
        $this->Pharmacy_model->order_commodity($commodity_name, $strength, $route, $frequency, $occurence, $duration, $visit_id, $patient_id);
    }

}
