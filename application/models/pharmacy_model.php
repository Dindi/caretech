<?php

class Pharmacy_model extends CI_Model {

    public function _construct() {
        parent::_construct();
    }

    public function pharmacy_patient_list() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT  DISTINCT patient.title ,visit.urgency,patient.f_name,visit.patient_id, patient.other_name, patient.s_name,"
                . " visit.visit_date,charge_followup, visit.visit_id,visit.urgency, visit.results"
                . " FROM patient INNER JOIN visit ON patient.patient_id=visit.patient_id"
                . " INNER JOIN patient_visit_statement ON patient_visit_statement.visit_id=visit.visit_id "
                . " WHERE DATE(visit.start) = DATE(NOW()) AND visit.pharm_queue = 'Active'"
                . " AND patient_visit_statement.amount - patient_visit_statement.amount_dr = 0"
                . " and visit.member_id='$member_id' and visit.branch_id='$branch_id' group by visit.visit_id order by visit.start";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function pharmacy_walkin_patient_list() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "select patient.patient_id, concat(title,' ',f_name,' ',' ',s_name,' ',other_name) as patient_name
 , visit.visit_id as walkin_visit_id , visit.end , lab_queue, visit.member_id ,
 visit.branch_id from visit inner join patient on patient.patient_id = visit.patient_id where pharm_queue='Active' and DATE(start) = DATE(NOW()) and type='Walk-in' order by visit.start desc";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function pharmacy_patient_list_xpress() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT  DISTINCT patient.f_name,visit.patient_id, patient.other_name, patient.s_name, visit.visit_date,"
                . " visit.visit_id,visit.urgency,charge_followup, visit.results FROM patient INNER JOIN visit ON patient.patient_id=visit.patient_id"
                . " INNER JOIN patient_visit_statement ON patient_visit_statement.visit_id=visit.visit_id "
                . "WHERE DATE(visit.start) = DATE(NOW()) AND visit.pharm_queue = 'Active' AND visit.pay_at_the_end='yes' group by visit.visit_id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function total_regular_patients_in_tray() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT count(visit_id) as total_regular_patients_in_tray FROM visit where pharm_queue='Active' and member_id='$member_id' and branch_id='$branch_id' and type='Regular' and  DATE(visit.start) = DATE(NOW())";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function total_walkin_patients_in_tray() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $type = $this->session->userdata('type');
        $sql = "SELECT count(visit_id) as total_regular_patients_in_tray FROM visit where pharm_queue='Active' and member_id='$member_id' and branch_id='$branch_id' and type='Walk-in' and  DATE(visit.start) = DATE(NOW())";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function total_appointments_to_date() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $type = $this->session->userdata('type');
        $sql = "select sum(appointment_id) as total_appointments_to_date from appointment where queue_booked='$type' and member_id='$member_id' and branch_id='$branch_id' and date >= curdate()";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_available_commodity($commodity_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $sql = "SELECT commodity.commodity_name , commodity.strength , sum(transaction.available_quantity) as available_quantity , stock.selling_price,commodity.commodity_id  FROM `transaction` "
                . "inner join stock on stock.batch_no = transaction.batch_no"
                . " inner join commodity on commodity.commodity_id = stock.commodity_id"
                . " where transaction.available_quantity > 0 and commodity.commodity_id='$commodity_id' and transaction.member_id='$member_id' and transaction.branch_id='$branch_id'  and transaction.has_expired='No'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_presciprion_details($prescription_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $sql = "SELECT * FROM prescription where prescription_id='$prescription_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_available_commodities($transaction_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $sql = "SELECT * FROM patient_dispensing_view where transaction_id='$transaction_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function update_bill_patient($transaction_id, $prescription_tracker, $patient_id, $visit_id, $commodity, $quantity_available, $quantity_issued, $selling_price, $billed_price, $service_charge) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $user_id = $this->session->userdata('id');
        $revenue_type = "medicine";
        $charged = "Yes";
        $paid = "No";
        $this->db->trans_start();
        $query = $this->db->get_where('patient_visit_statement', array('precription_tracker' => $prescription_tracker));
        foreach ($query->result() as $value) {

            $patient_visit_statement_id = $value->patient_visit_statement_id;
            $description = $value->description;
            $quantity_to_returned = $value->quantity;



            if (empty($service_charge)) {
                $service_charge = "No";
                $patient_visit_statement_data = array(
                    'visit_id' => $visit_id,
                    'patient_id' => $patient_id,
                    'description' => $commodity,
                    'quantity' => $quantity_issued,
                    'amount' => $billed_price,
                    'charged' => $charged,
                    'paid' => $paid,
                    'service_charge' => $service_charge,
                    'member_id' => $member_id,
                    'branch_id' => $branch_id,
                    'user_id' => $user_id,
                    'revenue_type' => $revenue_type
                );
            } else {
                $service_charge = "Yes";
                $patient_visit_statement_data = array(
                    'visit_id' => $visit_id,
                    'patient_id' => $patient_id,
                    'description' => $commodity,
                    'quantity' => $quantity_issued,
                    'amount' => $billed_price,
                    'charged' => $charged,
                    'paid' => $paid,
                    'service_charge' => $service_charge,
                    'member_id' => $member_id,
                    'branch_id' => $branch_id,
                    'user_id' => $user_id,
                    'revenue_type' => $revenue_type
                );
            }

            $this->db->where('patient_visit_statement_id', $patient_visit_statement_id);
            $this->db->update('patient_visit_statement', $patient_visit_statement_data);







            /*   $transaction_sql = "SELECT transaction.transaction_id, transaction.available_quantity FROM `transaction` inner join stock on stock.batch_no = transaction.batch_no where stock.commodity_name  LIKE '%$description%' LIMIT 0,1";
              $query = $this->db->query($transaction_sql);
              foreach ($query->result() as $value) {
              $rtrn_transaction_id = $value->transaction_id;
              $rtn_available_quantity = $value->available_quantity;
              $new_available_quantity = $rtn_available_quantity + $quantity_to_returned;
              $data_transaction_update = array(
              'available_quantity' => $new_available_quantity
              );
              $this->db->where('transaction_id', $rtrn_transaction_id);
              $this->db->update('transaction', $data_transaction_update);
              } */
        }



        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    
     public function bill_patient($transaction_id, $prescription_tracker, $patient_id, $visit_id, $commodity, $quantity_available, $quantity_issued, $selling_price, $billed_price, $service_charge) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $user_id = $this->session->userdata('id');
        $revenue_type = "medicine";
        $charged = "Yes";
        $paid = "No";
        $this->db->trans_start();
        $query = $this->db->get_where('commodity', array('commodity_id' => $commodity));
        foreach ($query->result() as $value) {
            $commodity_name = $value->commodity_name;
            if (empty($service_charge)) {
                $service_charge = "No";
                $data = array(
                    'visit_id' => $visit_id,
                    'patient_id' => $patient_id,
                    'description' => $commodity_name,
                    'quantity' => $quantity_issued,
                    'amount' => $billed_price,
                    'charged' => $charged,
                    'paid' => $paid,
                    'service_charge' => $service_charge,
                    'member_id' => $member_id,
                    'branch_id' => $branch_id,
                    'user_id' => $user_id,
                    'prescription_tracker' => $prescription_tracker,
                    'revenue_type' => $revenue_type
                );
                $this->db->insert('patient_visit_statement', $data);
            } else {
                $service_charge = "Yes";
                $service_amount = 200;
                $data_1 = array(
                    'visit_id' => $visit_id,
                    'patient_id' => $patient_id,
                    'description' => $commodity_name,
                    'quantity' => $quantity_issued,
                    'amount' => $billed_price,
                    'charged' => $charged,
                    'paid' => $paid,
                    'service_charge' => $service_charge,
                    'member_id' => $member_id,
                    'branch_id' => $branch_id,
                    'user_id' => $user_id,
                    'prescription_tracker' => $prescription_tracker,
                    'revenue_type' => $revenue_type,
                    'service_amount' => $service_amount
                );
                $this->db->insert('patient_visit_statement', $data_1);
            }

           

            echo 'prescription tracker: ' . $prescription_tracker . '<br>';
            $query = $this->db->get_where('prescription', array('prescription_tracker' => $prescription_tracker));
            foreach ($query->result() as $value) {
                $prescription_id = $value->prescription_id;
                $data_prescription_update = array(
                    'billed' => 'Yes'
                );
                $this->db->where('prescription_id', $prescription_id);
                $this->db->update('prescription', $data_prescription_update);
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    
    
    public function bill_walkin_patient($transaction_id, $prescription_tracker, $patient_id, $visit_id, $commodity, $quantity_available, $quantity_issued, $selling_price, $billed_price, $service_charge) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $user_id = $this->session->userdata('id');
        $revenue_type = "medicine";
        $charged = "Yes";
        $paid = "No";
        $visit_type = "Walk-in";
        $this->db->trans_start();
        $query = $this->db->get_where('commodity', array('commodity_id' => $commodity));
        foreach ($query->result() as $value) {
            $commodity_name = $value->commodity_name;
            if (empty($service_charge)) {
                $service_charge = "No";
                $data = array(
                    'visit_id' => $visit_id,
                    'patient_id' => $patient_id,
                    'description' => $commodity_name,
                    'quantity' => $quantity_issued,
                    'amount' => $billed_price,
                    'charged' => $charged,
                    'paid' => $paid,
                    'service_charge' => $service_charge,
                    'member_id' => $member_id,
                    'branch_id' => $branch_id,
                    'user_id' => $user_id,
                    'prescription_tracker' => $prescription_tracker,
                    'revenue_type' => $revenue_type,
                    'visit_type'=>$visit_type
                );
                $this->db->insert('patient_visit_statement', $data);
            } else {
                $service_charge = "Yes";
                $service_amount = 200;
                $data_1 = array(
                    'visit_id' => $visit_id,
                    'patient_id' => $patient_id,
                    'description' => $commodity_name,
                    'quantity' => $quantity_issued,
                    'amount' => $billed_price,
                    'charged' => $charged,
                    'paid' => $paid,
                    'service_charge' => $service_charge,
                    'member_id' => $member_id,
                    'branch_id' => $branch_id,
                    'user_id' => $user_id,
                    'prescription_tracker' => $prescription_tracker,
                    'revenue_type' => $revenue_type,
                    'service_amount' => $service_amount,
                    'visit_type'=>$visit_type
                );
                $this->db->insert('patient_visit_statement', $data_1);
            }

           

         
            $query = $this->db->get_where('prescription', array('prescription_tracker' => $prescription_tracker));
            foreach ($query->result() as $value) {
                $prescription_id = $value->prescription_id;
                $data_prescription_update = array(
                    'billed' => 'Yes'
                );
                $this->db->where('prescription_id', $prescription_id);
                $this->db->update('prescription', $data_prescription_update);
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function get_patient_visit_stmnt($prescription_tracker) {
        $this->db->select('*');
        $this->db->from('patient_visit_statement');
        $this->db->where('prescription_tracker', $prescription_tracker);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update_patient_dispense_commodity($patient_visit_statement_id, $prescription_tracker, $prescription_id, $patient_id
    , $visit_id, $transaction_id, $commodity_name, $stock_id, $available_quantity, $batch_no, $quantity_issued,$visit_type) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $issuing_officer = $this->session->userdata('id');
        $is_expired = "No";
        $user_type = "Patient";
        $is_dispensed = "Dispensed";
        $this->db->trans_start();
        $sql = "Select * from pharm_bin_card where user_id='$patient_id' and user_type='Patient' and batch_no='$batch_no' and visit_id='$visit_id' and entry_status='Active'";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {
            $pharm_bin_card_id = $value->pharm_bin_card_id;
            $issues = $value->issues;
            $opeinig_bal = $value->opening_bal;
            $closing_bal = $value->closing_bal;
            $identification_code = $value->identification_code;

            $transaction_query = $this->db->get_where('transaction', array('batch_no' => $batch_no));
            foreach ($transaction_query->result() as $value) {
                $transaction_id = $value->transaction_id;
                $trans_avaialble_quantity = $value->available_quantity;
                $new_available_quantity = $trans_avaialble_quantity + $issues;

                $update_available_qty = array(
                    'available_quantity' => $new_available_quantity
                );
                $this->db->where('transaction_id', $transaction_id);
                $this->db->update('transaction', $update_available_qty);


                $current_available_qty = $new_available_quantity - $quantity_issued;
                $update_trans_available_qty = array(
                    'available_quantity' => $current_available_qty
                );
                $this->db->where('batch_no', $batch_no);
                $this->db->update('transaction', $update_trans_available_qty);
                $update_pharm_bin_card = array(
                    'entry_status' => 'Cancelled'
                );
                $this->db->where('pharm_bin_card_id', $pharm_bin_card_id);
                $this->db->update('pharm_bin_card', $update_pharm_bin_card);

                $query = $this->db->get_where('patient', array('patient_id' => $patient_id));
                foreach ($query->result() as $value) {
                    $title = $value->title;
                    $f_name = $value->f_name;
                    $s_name = $value->s_name;
                    $other_name = $value->other_name;

                    $description = "Commodity: '.$commodity_name.' has been returned from Patient : '.$title.' '.$f_name.' '.$s_name.' '.$other_name.' '.'";
                    $pharm_bin_card_insert = array(
                        'commodity_name' => $commodity_name,
                        'batch_no' => $batch_no,
                        'opening_bal' => $available_quantity,
                        'closing_bal' => $current_available_qty,
                        'issues' => $quantity_issued,
                        'issuing_officer' => $issuing_officer,
                        'user_type' => $user_type,
                        'user_id' => $patient_id,
                        'identification_code' => $identification_code,
                        'description' => $description,
                        'member_id' => $member_id,
                        'branch_id' => $branch_id,
                        'entry_status' => 'Active',
                        'visit_type'=>$visit_type
                    );
                    $this->db->insert('pharm_bin_card', $pharm_bin_card_insert);
                }
            }

            $is_dispensed = "Dispensed";
            $prescription_update = array(
                'is_dispensed' => $is_dispensed,
                'quantity_issued' => $quantity_issued
            );
            $this->db->where('prescription_id', $prescription_id);
            $this->db->update('prescription', $prescription_update);





            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    public function insert_patient_dispense_commodity($patient_visit_statement_id, $prescription_tracker, $prescription_id, $patient_id
    , $visit_id, $transaction_id, $commodity_name, $stock_id, $available_quantity, $batch_no, $quantity_issued,$visit_type) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $issuing_officer = $this->session->userdata('id');
        $department = $this->session->userdata('department');
        $is_expired = "No";
        $user_type = "Patient";
        $this->db->trans_start();

        $is_dispensed = "Dispensed";
        $prescription_update = array(
            'is_dispensed' => $is_dispensed,
            'quantity_issued' => $quantity_issued
        );
        $this->db->where('prescription_id', $prescription_id);
        $this->db->update('prescription', $prescription_update);

        $query = $this->db->get_where('transaction', array('batch_no' => $batch_no, 'branch_id' => $branch_id, 'member_id' => $member_id, 'has_expired' => $is_expired));
        foreach ($query->result() as $value) {
            $transaction_id = $value->transaction_id;
            $available_quantity = $value->available_quantity;
            $remaining_quantiy = $available_quantity - $quantity_issued;

            $update_transaction = array(
                'available_quantity' => $remaining_quantiy
            );
            $this->db->where('transaction_id', $transaction_id);
            $this->db->update('transaction', $update_transaction);
//set the random id length 
            $random_id_length = 5;

            $time_now = date(DATE_RFC2822);

            //generate a random id encrypt it and store it in $rnd_id 
            $rnd_id = crypt(uniqid($time_now, 1));


            //to remove any slashes that might have come 
            $rnd_id = strip_tags(stripslashes($rnd_id));

            //Removing any . or / and reversing the string 
            $rnd_id = str_replace(".", "", $rnd_id);
            $rnd_id = strrev(str_replace("/", "", $rnd_id));

            //finally I take the first 10 characters from the $rnd_id 
            $rnd_id = substr($rnd_id, 0, $random_id_length);

            $identification_code = "ISS:$rnd_id";
            $query = $this->db->get_where('patient', array('patient_id' => $patient_id));
            foreach ($query->result() as $value) {
                $title = $value->title;
                $f_name = $value->f_name;
                $s_name = $value->s_name;
                $other_name = $value->other_name;

                $description = "Commodity: $commodity_name has been dispensed to Patient : $title : $f_name $s_name $other_name '";
                $pharm_bin_card_insert = array(
                    'commodity_name' => $commodity_name,
                    'batch_no' => $batch_no,
                    'opening_bal' => $available_quantity,
                    'closing_bal' => $remaining_quantiy,
                    'issues' => $quantity_issued,
                    'issuing_officer' => $issuing_officer,
                    'user_type' => $user_type,
                    'user_id' => $patient_id,
                    'identification_code' => $identification_code,
                    'description' => $description,
                    'member_id' => $member_id,
                    'branch_id' => $branch_id,
                    'visit_type'=>$visit_type,
                    'visit_id'=>$visit_id,
                    'department'=>$department
                );
                $this->db->insert('pharm_bin_card', $pharm_bin_card_insert);
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function order_commodity($commodity_name, $strength, $route, $frequency, $occurence, $duration, $visit_id, $patient_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $doctor_id = $this->session->userdata('id');
        $no = 'No';
        $yes = 'Yes';
        $today = date("Y-m-d H:i:s");
        $is_dispensed = 'Not Dispensed';
        $paid = 'Not Paid';
         echo 'Commodity name' . $commodity_name . '<br> Strength ' . $strength . '<br> Route' . $route . '<br> Frequency' . $frequency . '<br> Occurrence' . $occurence . '<br> Duration' . $duration . '<br> Visit id' . $visit_id . '<br> Patient id ' . $patient_id;
        $this->db->trans_start();

        $data_insert = array(
            'commodity_name' => $commodity_name,
            'route' => $route,
            'strength' => $strength,
            'frequency' => $frequency,
            'occurence' => $occurence,
            'duration' => $duration,
            'visit_id' => $visit_id,
            'patient_id' => $patient_id,
            'member_id' => $member_id,
            'branch_id' => $branch_id,
            'user_id' => $doctor_id,
            'billed' => $no,
            'is_dispensed' => $is_dispensed,
            'paid' => $paid,
            'date' => $today,
            'doctor_id' => $doctor_id,
            'visit_type'=>'Walk-in'
        );
        $this->db->insert('prescription', $data_insert);

        $last_insert_id = $this->db->insert_id();
        $prescription_tracker = "PRESC_" . $last_insert_id;
        echo '<br> Prescription Tracker : '.$prescription_tracker.'<br>';
        $data_update = array(
            'prescription_tracker' => $prescription_tracker
        );
        $this->db->where('prescription_id', $last_insert_id);
        $this->db->update('prescription', $data_update);


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

}
