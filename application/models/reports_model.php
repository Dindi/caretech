<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Author: Harris Dindi
 * Description: Login model class
 */

class Reports_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function patient_report() {
        $date_to = $this->input->post('date_to');

        $date_from = $this->input->post('date_from');

        $patient_status = $this->input->post('patient_status');

        $gender = $this->input->post('gender');


        if ($patient_status == 'All' && $gender == 'All') {

            if ($patient_status == 'All' and $gender === 'All' and empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient";
            } elseif ($patient_status === 'All' and $gender === 'All' and ! empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where date_joined >='$date_from'";
            } elseif ($patient_status === 'All' and $gender === 'All' and empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where date_joined <='$date_to'";
            } elseif ($patient_status === 'All' and $gender === 'All' and ! empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where (date_joined between '$date_from' and '$date_to')";
            }
        } elseif ($patient_status === 'All') {
            if ($patient_status === 'All' and ! empty($gender) and empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where gender='$gender'";
            } elseif ($patient_status === 'All' and ! empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where date_joined >='$date_from'";
            } elseif ($patient_status === 'All' and empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where date_joined <='$date_to'";
            } elseif ($patient_status === 'All' and ! empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient  where (date_joined between '$date_from' and '$date_to')";
            }
        } elseif ($gender === 'All') {
            if ($gender = 'All' and empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient";
            } elseif ($gender === 'All' and ! empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where date_joined >='$date_from'";
            } elseif ($gender === 'All' and empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where date_joined <='$date_to'";
            } elseif ($gender === 'All' and ! empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where (date_joined between '$date_from' and '$date_to')";
            }
        } else {
            if (empty($date_from) && empty($date_to) && empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient";
            } elseif (!empty($date_from) && !empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where (date_joined between '$date_from' and '$date_to') and status ='$patient_status' and gender='$gender'";
            } elseif (empty($date_from) && !empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where date_joined <='$date_to' and status='$patient_status' and gender='$gender'";
            } elseif (!empty($date_from) && empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where date_joined >= '$date_from' and status='$patient_status'";
            } elseif (!empty($date_from) && !empty($date_to) && empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where gender='$gender' and (date_joined between '$date_from' and '$date_to')";
            } elseif (!empty($date_from) && !empty($date_to) && !empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient where status='$patient_status' and (date_joined between '$date_from' and '$date_to')";
            } elseif (empty($date_from) && empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where status='$patient_status' and gender='$gender'";
            } elseif (!empty($date_from) && !empty($date_to) && empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient  where (date_joined between '$date_from' and '$date_to')";
            } elseif (empty($date_from) && !empty($date_to) && empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where date_joined >= '$date_to' and gender='$gender'";
            } elseif (empty($date_from) && !empty($date_to) && !empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient where date_joined <='$date_to' and status='$patient_status'";
            } elseif (!empty($date_from) && empty($date_to) && !empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient where date_joined >='$date_from' and status='$patient_status'";
            }
        }


        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function export_patient_report() {

        $date_to = $this->input->post('date_to');

        $date_from = $this->input->post('date_from');

        $patient_status = $this->input->post('patient_status');

        $gender = $this->input->post('gender');


        if ($patient_status == 'All' && $gender == 'All') {

            if ($patient_status == 'All' and $gender === 'All' and empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient";
            } elseif ($patient_status === 'All' and $gender === 'All' and ! empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where date_joined >='$date_from'";
            } elseif ($patient_status === 'All' and $gender === 'All' and empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where date_joined <='$date_to'";
            } elseif ($patient_status === 'All' and $gender === 'All' and ! empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where (date_joined between '$date_from' and '$date_to')";
            }
        } elseif ($patient_status === 'All') {
            if ($patient_status === 'All' and ! empty($gender) and empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where gender='$gender'";
            } elseif ($patient_status === 'All' and ! empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where date_joined >='$date_from'";
            } elseif ($patient_status === 'All' and empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where date_joined <='$date_to'";
            } elseif ($patient_status === 'All' and ! empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient  where (date_joined between '$date_from' and '$date_to')";
            }
        } elseif ($gender === 'All') {
            if ($gender = 'All' and empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient";
            } elseif ($gender === 'All' and ! empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where date_joined >='$date_from'";
            } elseif ($gender === 'All' and empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where date_joined <='$date_to'";
            } elseif ($gender === 'All' and ! empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where (date_joined between '$date_from' and '$date_to')";
            }
        } else {
            if (empty($date_from) && empty($date_to) && empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient";
            } elseif (!empty($date_from) && !empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where (date_joined between '$date_from' and '$date_to') and status ='$patient_status' and gender='$gender'";
            } elseif (empty($date_from) && !empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where date_joined <='$date_to' and status='$patient_status' and gender='$gender'";
            } elseif (!empty($date_from) && empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where date_joined >= '$date_from' and status='$patient_status'";
            } elseif (!empty($date_from) && !empty($date_to) && empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where gender='$gender' and (date_joined between '$date_from' and '$date_to')";
            } elseif (!empty($date_from) && !empty($date_to) && !empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient where status='$patient_status' and (date_joined between '$date_from' and '$date_to')";
            } elseif (empty($date_from) && empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where status='$patient_status' and gender='$gender'";
            } elseif (!empty($date_from) && !empty($date_to) && empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient  where (date_joined between '$date_from' and '$date_to')";
            } elseif (empty($date_from) && !empty($date_to) && empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where date_joined >= '$date_to' and gender='$gender'";
            } elseif (empty($date_from) && !empty($date_to) && !empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient where date_joined <='$date_to' and status='$patient_status'";
            } elseif (!empty($date_from) && empty($date_to) && !empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient where date_joined >='$date_from' and status='$patient_status'";
            }
        }


        $query = $this->db->query($sql);
        $result = $query->result_array();

//load our new PHPExcel library
        $this->load->library('excel');



        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Patients Report');
        $this->excel->getActiveSheet()->SetCellValue('A1', "Date Registered");
        $this->excel->getActiveSheet()->SetCellValue('B1', "Patient Name");
        $this->excel->getActiveSheet()->SetCellValue('C1', "Dob");
        $this->excel->getActiveSheet()->SetCellValue('D1', "Gender");
        $this->excel->getActiveSheet()->SetCellValue('E1', "Phone No");
        $this->excel->getActiveSheet()->SetCellValue('F1', "Email");
        $this->excel->getActiveSheet()->SetCellValue('G1', "Added By");



        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);





        $i = 2;
        foreach ($results as $result) {

            $this->excel->getActiveSheet()->SetCellValue('A' . $i, $result["unit_name"]);
            $this->excel->getActiveSheet()->SetCellValue('B' . $i, $result["registration_number"]);
            $this->excel->getActiveSheet()->SetCellValue('C' . $i, $result["unit_code"]);
            $this->excel->getActiveSheet()->SetCellValue('D' . $i, $result["unit_type"]);
            $this->excel->getActiveSheet()->SetCellValue('E' . $i, $result["date_added"]);
            $this->excel->getActiveSheet()->SetCellValue('F' . $i, $result["is_Active"]);
            $this->excel->getActiveSheet()->SetCellValue('G' . $i, $result["user_name"]);




            $i++;
        }




        $today = date("d-m-Y");


        $filename = "Crescent_.$today.Units_report.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

    function visitation_report() {
        $patient_id = $this->input->post('patient_name');
        $package_type = $this->input->post('package_type');
        $date_to = $this->input->post('date_to');
        $date_from = $this->input->post('date_from');
        $all = "All";
        $sql = "select * from visit "
                . "inner join patient on patient.patient_id = visit.patient_id "
                . "inner join packages on packages.package_id = visit.package_type where 1 ";
        if ($package_type === $all and $patient_id !== $all) {
            if (!empty($patient_id)) {
                $sql .= " AND visit.patient_id='$patient_id' and visit.visit_status='Active' ";
            }

            if (!empty($date_from)) {
                $sql .= " AND visit.visit_date >='$date_from' and visit.visit_status='Active' ";
            }
            if (!empty($date_to)) {
                $sql .= " AND visit.visit_date <='$date_to' and visit.visit_status='Active' ";
            }
            if (!empty($date_from) && !empty($date_to) && !empty($patient_id)) {
                $sql .= " AND visit.visit_date between '$date_from' and '$date_to' and patient.patient_id='$patient_id' and visit.visit_status='Active' ";
            }
        } elseif ($patient_id === $all and $package_type !== $all) {
            if (!empty($package_type)) {
                $sql .= " AND visit.package_type='$package_type' and visit.visit_status='Active' ";
            }

            if (!empty($date_from)) {
                $sql .= " AND visit.visit_date >='$date_from' and visit.visit_status='Active' ";
            }
            if (!empty($date_to)) {
                $sql .= " AND visit.visit_date <='$date_to' and visit.visit_status='Active' ";
            }
            if (!empty($date_from) and ! empty($package_type)) {
                $sql .="AND visit.visit_date >='$date_from' and visit.package_type='$package_type' and visit.visit_status='Active'";
            }
            if (!empty($date_to) and ! empty($package_type)) {
                $sql .="AND visit.visit_date <='$date_to' and visit.package_type='$package_type' and visit.visit_status='Active'";
            }
            if (!empty($date_from) && !empty($date_to) && !empty($package_type)) {
                $sql .= " AND visit.visit_date between '$date_from' and '$date_to' and visit.package_type='$package_type' and visit.visit_status='Active' ";
            }
        } elseif ($package_type === $all and $patient_id === $all) {
            if (!empty($date_from) and empty($date_to)) {
                $sql .= " AND visit.visit_date >='$date_from' and visit.visit_status='Active' ";
            }
            if (!empty($date_to) and empty($date_from)) {
                $sql .= " AND visit.visit_date <='$date_to' and visit.visit_status='Active' ";
            }
            if (!empty($date_from) && !empty($date_to)) {
                $sql .= " AND visit.visit_date between '$date_from' and '$date_to' and visit.visit_status='Active' ";
            }
        } else {
            if (!empty($patient_id) and empty($package_type) and empty($date_from) and empty($date_to)) {
                $sql .= " AND visit.patient_id='$patient_id' and visit.visit_status='Active' ";
            }
            if (!empty($package_type) and empty($patient_id) and empty($date_from) and empty($date_to)) {
                $sql .= " AND visit.package_type='$package_type' and visit.visit_status='Active' ";
            }
            if (!empty($date_from) and empty($date_to) and empty($package_type) and empty($patient_id)) {
                $sql .= " AND visit.visit_date >='$date_from' and visit.visit_status='Active' ";
            }
            if (!empty($date_to) and empty($date_from) and empty($package_type) and empty($patient_id)) {
                $sql .= " AND visit.visit_date <='$date_to' and visit.visit_status='Active' ";
            }
            if (!empty($date_from) && !empty($date_to) and empty($package_type) and empty($patient_id)) {
                $sql .= " AND visit.visit_date between '$date_from' and '$date_to' and visit.visit_status='Active' ";
            }
            if (!empty($date_from) && !empty($date_to) and ! empty($patient_id) and empty($package_type)) {
                $sql .= " AND visit.visit_date between '$date_from' and '$date_to' and patient.patient_id='$patient_id' and visit.visit_status='Active' ";
            }
            if (!empty($date_from) && !empty($date_to) and ! empty($package_type) and empty($patient_id)) {
                $sql .= " AND visit.visit_date between '$date_from' and '$date_to' and visit.package_type='$package_type' and visit.visit_status='Active' ";
            }
        }


        $result = $this->db->query($sql);
        $result = $result->result_array();
        return $result;
    }

    public function export_commodity_report() {
        $date_from = $this->input->post('date_from');
        $date_to = $this->input->post('date_to');
        $commodity_name = $this->input->post('commmodity_name');
        $commodity_type = $this->input->post('commmodity_type');
        $sql = "SELECT * FROM `commodity_view` where 1 ";
        if ($commodity_name === "All") {

            if (!empty($date_from) and empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //1 1
                $sql .= "AND date_added >= '$date_from'";
            } elseif (empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //2 2
                $sql .= "AND date_added <='$date_to'";
            } elseif (empty($date_from) and empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //4 4
                $sql .= "and commodity_type='$commodity_type'";
            } elseif (!empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //5 12
                $sql .="and date_added between '$date_from' and '$date_to'";
            } elseif (!empty($date_from) and empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //7 14
                $sql .= "and date_added >='$date_from' and commodity_type='$commodity_type'";
            } elseif (empty($date_from) and ! empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //9 24
                $sql .="and date_added <='$date_to' and commodity_type='$commodity_type'";
            } elseif (empty($date_from) and empty($date_to) and ! empty($commodity_name) and ! empty($commodity_type)) {
                //10 34
                $sql .= "and commodity_name='$commodity_name' and commodity_type='$commodity_type'";
            } elseif (!empty($commodity_type) and ! empty($commodity_name) and ! empty($date_from) and ! empty($date_to)) {
                //11 1234
                $sql .= "and date_added <= '$date_to' and commodity_name = '$commodity_name' and commodity_type='$commodity_type'";
            }
        } elseif ($commodity_type === "All") {

            if (!empty($date_from) and empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //1 1
                $sql .= "AND date_added >= '$date_from'";
            } elseif (empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //2 2
                $sql .= "AND date_added <='$date_to'";
            } elseif (empty($date_from) and empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //3 3
                $sql .= "and commodity_name='$commodity_name'";
            } elseif (!empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //5 12
                $sql .="and date_added between '$date_from' and '$date_to'";
            } elseif (!empty($date_from) and empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //6 13
                $sql .= "and date_added >= '$date_from' and commodity_name = '$commodity_name'";
            } elseif (empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //8 23
                $sql .= "and date_added <='$date_to' and commodity_name='$commodity_name'";
            } elseif (!empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and ! empty($commodity_type)) {
                //11 1234
                $sql .= "and date_added <= '$date_to' and commodity_name = '$commodity_name' and commodity_type='$commodity_type'";
            } elseif (!empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //12 123
                $sql .= "and date_added <= '$date_to' and commodity_name='$commodity_name'";
            } elseif (!empty($date_from) and ! empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //13 124
                $sql .= "and date_added <= '$date_to' and commodity_name = '$commodity_name'";
            }
        } elseif ($commodity_name === "All" and $commodity_type === "All") {

            if (!empty($date_from) and empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //1 1
                $sql .= "AND date_added >= '$date_from'";
            } elseif (empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //2 2
                $sql .= "AND date_added <='$date_to'";
            } elseif (!empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //5 12
                $sql .="and date_added between '$date_from' and '$date_to'";
            }
        } else {

            if (!empty($date_from) and empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //1 1
                $sql .= "AND date_added >= '$date_from'";
            } elseif (empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //2 2
                $sql .= "AND date_added <='$date_to'";
            } elseif (empty($date_from) and empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //3 3
                $sql .= "and commodity_name='$commodity_name'";
            } elseif (empty($date_from) and empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //4 4
                $sql .= "and commodity_type='$commodity_type'";
            } elseif (!empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //5 12
                $sql .="and date_added between '$date_from' and '$date_to'";
            } elseif (!empty($date_from) and empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //6 13
                $sql .= "and date_added >= '$date_from' and commodity_name = '$commodity_name'";
            } elseif (!empty($date_from) and empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //7 14
                $sql .= "and date_added >='$date_from' and commodity_type='$commodity_type'";
            } elseif (empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //8 23
                $sql .= "and date_added <='$date_to' and commodity_name='$commodity_name'";
            } elseif (empty($date_from) and ! empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //9 24
                $sql .="and date_added <='$date_to' and commodity_type='$commodity_type'";
            } elseif (empty($date_from) and empty($date_to) and ! empty($commodity_name) and ! empty($commodity_type)) {
                //10 34
                $sql .= "and commodity_name='$commodity_name' and commodity_type='$commodity_type'";
            } elseif (!empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and ! empty($commodity_type)) {
                //11 1234
                $sql .= "and date_added <= '$date_to' and commodity_name = '$commodity_name' and commodity_type='$commodity_type'";
            } elseif (!empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //12 123
                $sql .= "and date_added <= '$date_to' and commodity_name='$commodity_name'";
            } elseif (!empty($date_from) and ! empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //13 124
                $sql .= "and date_added <= '$date_to' and commodity_name = '$commodity_name'";
            } elseif (!empty($date_from) and empty($date_to) and ! empty($commodity_name) and ! empty($commodity_type)) {
                //14 134
                $sql .= "and commodity_name ='$commodity_name' and commodity_type='$commodity_type'";
            } elseif (empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and ! empty($commodity_type)) {
                // 234
                $sql .="and date_added <= '$date_to' and commodity_name='$commodity_name' and commodity_type='$commodity_type'";
            } elseif (empty($date_from) and empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                $sql .=" ";
            }
        }




        $result = $this->db->query($sql);
        $result = $result->result_array();
        return $result;
    }

    public function export_stock_report() {
        $date_from = $this->input->post('date_from');
        $date_to = $this->input->post('date_to');
        $commodity_name = $this->input->post('commmodity_name');
        $commodity_type = $this->input->post('commmodity_type');
        $sql = "Select * from stock_view where 1 ";
        if ($commodity_name === "All") {

            if (!empty($date_from) and empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //1 1
                $sql .= "AND date_added >= '$date_from'";
            } elseif (empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //2 2
                $sql .= "AND date_added <='$date_to'";
            } elseif (empty($date_from) and empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //4 4
                $sql .= "and commodity_type='$commodity_type'";
            } elseif (!empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //5 12
                $sql .="and date_added between '$date_from' and '$date_to'";
            } elseif (!empty($date_from) and empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //7 14
                $sql .= "and date_added >='$date_from' and commodity_type='$commodity_type'";
            } elseif (empty($date_from) and ! empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //9 24
                $sql .="and date_added <='$date_to' and commodity_type='$commodity_type'";
            } elseif (empty($date_from) and empty($date_to) and ! empty($commodity_name) and ! empty($commodity_type)) {
                //10 34
                $sql .= "and commodity_name='$commodity_name' and commodity_type='$commodity_type'";
            } elseif (!empty($commodity_type) and ! empty($commodity_name) and ! empty($date_from) and ! empty($date_to)) {
                //11 1234
                $sql .= "and date_added <= '$date_to' and commodity_name = '$commodity_name' and commodity_type='$commodity_type'";
            }
        } elseif ($commodity_type === "All") {

            if (!empty($date_from) and empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //1 1
                $sql .= "AND date_added >= '$date_from'";
            } elseif (empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //2 2
                $sql .= "AND date_added <='$date_to'";
            } elseif (empty($date_from) and empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //3 3
                $sql .= "and commodity_name='$commodity_name'";
            } elseif (!empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //5 12
                $sql .="and date_added between '$date_from' and '$date_to'";
            } elseif (!empty($date_from) and empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //6 13
                $sql .= "and date_added >= '$date_from' and commodity_name = '$commodity_name'";
            } elseif (empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //8 23
                $sql .= "and date_added <='$date_to' and commodity_name='$commodity_name'";
            } elseif (!empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and ! empty($commodity_type)) {
                //11 1234
                $sql .= "and date_added <= '$date_to' and commodity_name = '$commodity_name' and commodity_type='$commodity_type'";
            } elseif (!empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //12 123
                $sql .= "and date_added <= '$date_to' and commodity_name='$commodity_name'";
            } elseif (!empty($date_from) and ! empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //13 124
                $sql .= "and date_added <= '$date_to' and commodity_name = '$commodity_name'";
            }
        } elseif ($commodity_name === "All" and $commodity_type === "All") {

            if (!empty($date_from) and empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //1 1
                $sql .= "AND date_added >= '$date_from'";
            } elseif (empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //2 2
                $sql .= "AND date_added <='$date_to'";
            } elseif (!empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //5 12
                $sql .="and date_added between '$date_from' and '$date_to'";
            }
        } else {

            if (!empty($date_from) and empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //1 1
                $sql .= "AND date_added >= '$date_from'";
            } elseif (empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //2 2
                $sql .= "AND date_added <='$date_to'";
            } elseif (empty($date_from) and empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //3 3
                $sql .= "and commodity_name='$commodity_name'";
            } elseif (empty($date_from) and empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //4 4
                $sql .= "and commodity_type='$commodity_type'";
            } elseif (!empty($date_from) and ! empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                //5 12
                $sql .="and date_added between '$date_from' and '$date_to'";
            } elseif (!empty($date_from) and empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //6 13
                $sql .= "and date_added >= '$date_from' and commodity_name = '$commodity_name'";
            } elseif (!empty($date_from) and empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //7 14
                $sql .= "and date_added >='$date_from' and commodity_type='$commodity_type'";
            } elseif (empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //8 23
                $sql .= "and date_added <='$date_to' and commodity_name='$commodity_name'";
            } elseif (empty($date_from) and ! empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //9 24
                $sql .="and date_added <='$date_to' and commodity_type='$commodity_type'";
            } elseif (empty($date_from) and empty($date_to) and ! empty($commodity_name) and ! empty($commodity_type)) {
                //10 34
                $sql .= "and commodity_name='$commodity_name' and commodity_type='$commodity_type'";
            } elseif (!empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and ! empty($commodity_type)) {
                //11 1234
                $sql .= "and date_added <= '$date_to' and commodity_name = '$commodity_name' and commodity_type='$commodity_type'";
            } elseif (!empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and empty($commodity_type)) {
                //12 123
                $sql .= "and date_added <= '$date_to' and commodity_name='$commodity_name'";
            } elseif (!empty($date_from) and ! empty($date_to) and empty($commodity_name) and ! empty($commodity_type)) {
                //13 124
                $sql .= "and date_added <= '$date_to' and commodity_name = '$commodity_name'";
            } elseif (!empty($date_from) and empty($date_to) and ! empty($commodity_name) and ! empty($commodity_type)) {
                //14 134
                $sql .= "and commodity_name ='$commodity_name' and commodity_type='$commodity_type'";
            } elseif (empty($date_from) and ! empty($date_to) and ! empty($commodity_name) and ! empty($commodity_type)) {
                // 234
                $sql .="and date_added <= '$date_to' and commodity_name='$commodity_name' and commodity_type='$commodity_type'";
            } elseif (empty($date_from) and empty($date_to) and empty($commodity_name) and empty($commodity_type)) {
                $sql .=" ";
            }
        }




        $result = $this->db->query($sql);
        $result = $result->result_array();
        return $result;
    }

    function edit_stock_info() {
        $this->db->trans_start();

        $commodity_name = $this->input->post('commmodity_name');
        $commodity_type = $this->input->post('commodity_type');
        $commodity_code = $this->input->post('commodity_code');
        $supplier = $this->input->post('supplier_name');
        $no_of_packs = $this->input->post('no_of_packs');
        $unit_per_pack = $this->input->post('unit_per_pack');
        $total_quantity = $this->input->post('total_quantity');
        $buying_price = $this->input->post('buying_price');
        $remarks = $this->input->post('remarks');
        $expired = $this->input->post('has_expired');
        $user_id = $this->session->userdata('id');
        $selling_price = floatval($buying_price) * 3;
        $expiry_date = $this->input->post('expiry_date');
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $stock_id = $this->input->post('edit_stock_id');
        $batch_no_sql = "SELECT max(batch_no) as batch_no  FROM stock WHERE commodity_id = '$commodity_name' LIMIT 0,1";
        $last_batch_number_sql = $this->db->query($batch_no_sql);
        foreach ($last_batch_number_sql->result() as $value_last) {
            $max_batch_no = $value_last->batch_no;
            echo 'Max Batch No' . $max_batch_no . '</br>';

            $str = $max_batch_no;
            $last_batch_number_sql = substr($str, strpos($str, '_') + 1);
            $new_truncated_batch_number = $last_batch_number_sql + 1;
            $under_score = '_';
            $new_batch_no = $commodity_code . $under_score . $new_truncated_batch_number;
            echo 'New Batch Number' . $new_batch_no . '</br>';
            $stock_update = array(
                'commodity_id' => $commodity_name,
                'supplier_name' => $supplier,
                'no_of_packs' => $no_of_packs,
                'unit_per_pack' => $unit_per_pack,
                'total_quantity' => $total_quantity,
                'buying_price' => $buying_price,
                'selling_price' => $selling_price,
                'remarks' => $remarks,
                'has_expired' => $expired,
                'batch_no' => $new_batch_no,
                'user_id' => $user_id,
                'member_id' => $member_id,
                'branch_id' => $branch_id,
                'expiry_date' => $expiry_date
            );
            $this->db->where('stock_id', $stock_id);
            $this->db->update('stock', $stock_update);

            $query = $this->db->get_where('commodity', array('commodity_id' => $commodity_name));
            foreach ($query->result() as $value) {
                $code = $value->code;
                $commodity_name = $value->commodity_name;

                $update_stock = array(
                    'commodity_name' => $commodity_name,
                    'code' => $commodity_code
                );
                $this->db->where('stock_id', $stock_id);
                $this->db->update('stock', $update_stock);
            }
        }



        $query = $this->db->get_where('commodity', array('commodity_id' => $commodity_name));
        foreach ($query->result() as $value) {
            $code = $value->code;
            $commodity_name = $value->commodity_name;
        }





        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function delete_stock_info() {
        $this->db->trans_start();
        $stock_id = $this->input->post('delete_stock_id');
        $update_stock_data = array(
            'status' => 'In Active'
        );
        $this->db->where('stock_id', $stock_id);
        $this->db->update('stock', $update_stock_data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function add_new_commodity() {
        $this->db->trans_start();
        $commodity_name = $this->input->post('commodity_name');
        $commodity_type = $this->input->post('commodity_type');
        $commodity_code = $this->input->post('commodity_code');
        $unit = $this->input->post('unit');
        $strength = $this->input->post('strength');
        $max_stock = $this->input->post('max_stock');
        $min_stock = $this->input->post('min_stock');
        $remarks = $this->input->post('remarks');
        $status = $this->input->post('status');

        $data_insert = array(
            'commodity_name' => $commodity_name,
            'code' => $commodity_code,
            'commodity_type' => $commodity_type,
            'unit' => $unit,
            'strength' => $strength,
            'max_stock' => $max_stock,
            'min_stock' => $min_stock,
            'remarks' => $remarks,
            'status' => $status
        );
        $this->db->insert('commodity', $data_insert);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function update_commodity() {
        $this->db->trnas_start();
        $commodity_name = $this->input->post('commodity_name');
        $commodity_type = $this->input->post('commodity_type');
        $commodity_code = $this->input->post('commodity_code');
        $unit = $this->input->post('unit');
        $strength = $this->input->post('strength');
        $max_stock = $this->input->post('max_stock');
        $min_stock = $this->input->post('min_stock');
        $remarks = $this->input->post('remarks');
        $status = $this->input->post('status');
        $commodity_id = $this->input->post('commodity_id');

        $data_insert = array(
            'commodity_name' => $commodity_name,
            'code' => $commodity_code,
            'commodity_type' => $commodity_type,
            'unit' => $unit,
            'strength' => $strength,
            'max_stock' => $max_stock,
            'min_stock' => $min_stock,
            'remarks' => $remarks,
            'status' => $status
        );
        $this->db->where('commodity_id', $commodity_id);
        $this->db->update('commodity', $data_insert);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function delete_commodity() {
        $this->db->trans_start();
        $commodity_id = $this->input->post('commodity_id');
        $in_Active = "In Active";
        $data_delete = array(
            'status' => $in_Active
        );
        $this->db->where('commodity_id', $commodity_id);
        $this->db->update('commodity', $data_delete);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function add_new_stock() {
        $this->db->trans_start();
        $commodity_name = $this->input->post('commmodity_name');
        $commodity_type = $this->input->post('commodity_type');
        $commodity_code = $this->input->post('commodity_code');
        $supplier = $this->input->post('supplier_name');
        $no_of_packs = $this->input->post('no_of_packs');
        $unit_per_pack = $this->input->post('unit_per_pack');
        $total_quantity = $this->input->post('total_quantity');
        $buying_price = $this->input->post('buying_price');
        $remarks = $this->input->post('remarks');
        $expired = $this->input->post('has_expired');
        $user_id = $this->session->userdata('id');
        $selling_price = floatval($buying_price) * 3;
        $expiry_date = $this->input->post('expiry_date');
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $last_batch_number_sql = "SELECT max(batch_no) as batch_no  FROM stock WHERE commodity_id = '$commodity_name' LIMIT 0,1";
        $last_batch_number_sql = $this->db->query($last_batch_number_sql);
        foreach ($last_batch_number_sql->result() as $value_last) {
            $max_batch_no = $value_last->batch_no;
            echo 'Max Batch No' . $max_batch_no . '</br>';

            $str = $max_batch_no;
            $last_batch_number_sql = substr($str, strpos($str, '_') + 1);
            $new_truncated_batch_number = $last_batch_number_sql + 1;
            $under_score = '_';
            $new_batch_no = $commodity_code . $under_score . $new_truncated_batch_number;
            echo 'New Batch Number' . $new_batch_no . '</br>';
            $stock_insert = array(
                'commodity_id' => $commodity_name,
                'supplier_name' => $supplier,
                'no_of_packs' => $no_of_packs,
                'unit_per_pack' => $unit_per_pack,
                'total_quantity' => $total_quantity,
                'buying_price' => $buying_price,
                'selling_price' => $selling_price,
                'remarks' => $remarks,
                'has_expired' => $expired,
                'batch_no' => $new_batch_no,
                'user_id' => $user_id,
                'member_id' => $member_id,
                'branch_id' => $branch_id,
                'expiry_date' => $expiry_date
            );

            $this->db->insert('stock', $stock_insert);
            $last_inserted_id = $this->db->insert_id();
            $delivery_no = "DEL_" . $last_inserted_id . " ";
            $query = $this->db->get_where('commodity', array('commodity_id' => $commodity_name));
            foreach ($query->result() as $value) {
                $code = $value->code;
                $commodity_name = $value->commodity_name;

                $update_stock = array(
                    'delivery_no' => $delivery_no,
                    'commodity_name' => $commodity_name,
                    'code' => $commodity_code
                );
                $this->db->where('stock_id', $last_inserted_id);
                $this->db->update('stock', $update_stock);
            }
        }



        $query = $this->db->get_where('commodity', array('commodity_id' => $commodity_name));
        foreach ($query->result() as $value) {
            $code = $value->code;
            $commodity_name = $value->commodity_name;
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function get_visitation_entry($id) {

        $sql = " select visit.family_number as family_number,visit.urgency,visit.patient_id,visit.pay_at_the_end, visit.nurse_queue,visit.doctor_queue,visit.lab_queue,visit.pharm_queue,visit.visit_date as visit_date,visit.visit_id as visit_id , packages.package_name as package_name, concat(patient.title,'',patient.f_name,' ',patient.s_name,' ',' ',patient.other_name) as patient_name from visit
                  inner join packages on packages.package_id=visit.package_type 
                  inner join patient on patient.patient_id= visit.patient_id where visit.visit_id='$id'";
        $query = $this->db->query($sql);

        return $query->result();
    }

    function walkin_report() {

        $department_name = $this->input->post('department_name');
        $payment_status = $this->input->post('payment_status');
        $date_to = $this->input->post('date_to');
        $date_from = $this->input->post('date_from');
        $sql = "select * from walkin where 1 ";
        if (!empty($department_name)) {
            if ($department_name === 'All') {
                $sql .= "";
            } else {
                $sql .= " AND walkin.walkin_department='$department_name' and walkin.walkin_status='Active' ";
            }
        }
        if (!empty($payment_status)) {
            if ($payment_status === 'All') {
                $sql .="";
            } else {
                $sql .= " AND walkin.paid='$payment_status'and walkin.walkin_status='Active' ";
            }
        }
        if (!empty($date_from)) {
            $sql .= " AND walkin.walkin_date >='$date_from' and walkin.walkin_status='Active' ";
        }
        if (!empty($date_to)) {
            $sql .= " AND walkin.walkin_date <='$date_to' and walkin.walkin_status='Active' ";
        }
        if (!empty($date_from) && !empty($date_to)) {
            $sql .= " AND walkin.walkin_date between '$date_from' and '$date_to' and walkin.walkin_status='Active' ";
        }
        $result = $this->db->query($sql);
        $result = $result->result_array();
        return $result;
    }

    function procedure_report() {

        $procedure_name = $this->input->post('procedure_name');
        $patient_type = $this->input->post('patient_type');
        $date_to = $this->input->post('date_to');
        $date_from = $this->input->post('date_from');
        $sql = "select procedure_result.id as procedure_id,visit.visit_id,packages.package_id , packages.package_name as procedure_name,packages.cost as procedure_price ,patient.patient_id, concat(title,' ',f_name,' ',' ',s_name,' ',other_name) as patient_name, patient.phone_no ,visit.visit_type as patient_type , visit.start as date_added from procedure_result inner join packages on packages.package_id = procedure_result.procedure_id inner join visit on visit.visit_id = procedure_result.visit_id inner join patient on patient.patient_id = procedure_result.patient_id where 1 ";
        if (!empty($procedure_name)) {
            if ($procedure_name === 'All') {
                $sql .= "and status='Active'";
            } else {
                $sql .= " AND procedure_result.id='$procedure_name'and status='Active' ";
            }
        }
        if (!empty($payment_status)) {
            if ($payment_status === 'All') {
                $sql .="and status='Active'";
            } else {
                $sql .= " AND visit.visit_type='$payment_status' and status='Active'";
            }
        }
        if (!empty($date_from)) {
            $sql .= " AND visit.start >='$date_from' and status='Active' ";
        }
        if (!empty($date_to)) {
            $sql .= " AND visit.start <='$date_to' and status='Active' ";
        }
        if (!empty($date_from) && !empty($date_to)) {
            $sql .= " AND visit.start between '$date_from' and '$date_to' and status='Active' ";
        }
        $result = $this->db->query($sql);
        $result = $result->result_array();
        return $result;
    }

    function get_procedure_visit_reports_details($procedure_visit_id) {
        $sql = "select procedure_list.procedure_id,procedure_list.procedure_name,"
                . "procedure_list.procedure_price,procedure_visit.procedure_visit_id,"
                . "procedure_visit.patient_name,procedure_visit.patient_phone,procedure_visit.patient_type,"
                . "procedure_visit.date_added,procedure_visit.status  from procedure_visit inner join procedure_list"
                . " on procedure_visit.procedure_list_id = procedure_list.procedure_id where procedure_visit.procedure_visit_id='$procedure_visit_id' ";

        $result = $this->db->query($sql);
        $result = $result->result_array();
        return $result;
    }

    function get_patient_reports_details($patient_id) {

        $sql = "Select * from patient where patient_id='$patient_id'";
        $result = $this->db->query($sql);
        $result_array = $result->result_array();
        return $result_array;
    }

    function get_walkinpatient_reports_details($walkin_patient_id) {
        $sql = "Select * from walkin";
        $result = $this->db->query($sql);
        $result_array = $result->result_array();
        return $result_array;
    }

    function get_visit_reports_details($visit_id) {
        $sql = "select visit.visit_id, visit.doctor_queue, visit.nurse_queue, visit.lab_queue, visit.pharm_queue, visit.pay_at_the_end,visit.visit_date,visit.family_number, visit.urgency, visit.visit_type,patient.title, patient.f_name,patient.s_name,patient.other_name,packages.package_name from visit "
                . "inner join patient on patient.patient_id = visit.patient_id "
                . "inner join packages on packages.package_id = visit.package_type where visit.visit_id='$visit_id' ";
        $result = $this->db->query($sql);
        $result_array = $result->result_array();
        return $result_array;
    }

    public function delete_patient($patient_id) {

        $in_Active = 'In Active';
        $this->db->trans_start();
        $data = array(
            'status' => $in_Active
        );

        $this->db->where('patient_id', $patient_id);
        $this->db->update('patient', $data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            // generate an error... or use the log_message() function to log your error
        }
    }

    public function edit_patient() {
        $patient_id = $this->input->post('edit_patient_id');
        $title = $this->input->post('edit_title');
        $f_name = $this->input->post('edit_inputFirstName');
        $s_name = $this->input->post('edit_inputSurName');
        $other_name = $this->input->post('edit_inputOtherName');
        $dob = $this->input->post('edit_dob');
        $gender = $this->input->post('edit_gender');
        $marital_status = $this->input->post('edit_maritalstatus');
        $phone_no = $this->input->post('edit_phone_no');
        $identification_number = $this->input->post('edit_nationalid');
        $email = $this->input->post('edit_email');
        $residence = $this->input->post('edit_address');
        $employment_status = $this->input->post('edit_employement_status');
        $employer = $this->input->post('edit_employers_name');
        $kin_fname = $this->input->post('edit_kinname');
        $kin_lname = $this->input->post('edit_kinsname');
        $kin_relation = $this->input->post('edit_kinrelation');
        $kin_phone = $this->input->post('edit_kinphone');
        $status = $this->input->post('');
        $city = $this->input->post('edit_city');


        $in_Active = 'In Active';
        $this->db->trans_start();
        $data = array(
            'title' => $title,
            'f_name' => $f_name,
            's_name' => $s_name,
            'other_name' => $other_name,
            'dob' => $dob,
            'gender' => $gender,
            'marital_status' => $marital_status,
            'phone_no' => $phone_no,
            'identification_number' => $identification_number,
            'email' => $email,
            'residence' => $residence,
            'employment_status' => $employment_status,
            'employer' => $employer,
            'next_of_kin_fname' => $kin_fname,
            'next_of_kin_lname' => $kin_lname,
            'next_of_kin_relation' => $kin_relation,
            'next_of_kin_phone' => $kin_phone,
            'city' => $city
        );

        $this->db->where('patient_id', $patient_id);
        $this->db->update('patient', $data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            // generate an error... or use the log_message() function to log your error
        }
    }

    function edit_visitation_info() {
        $visit_id = $this->input->post('edit_visit_id');

        $nurse_queue = $this->input->post('edit_nurse_queue');
        $doctor_queue = $this->input->post('edit_doctor_queue');
        $lab_queue = $this->input->post('edit_lab_queue');
        $pharm_queue = $this->input->post('edit_pharm_queue');
        $pay_at_the_end = $this->input->post('edit_pay_at_the_end');
        $urgency = $this->input->post('edit_urgency');
        $edit_visitation_status = $this->input->post('edit_visitation_status');
        $this->db->trans_start();
        $vistation_update_data = array(
            'nurse_queue' => $nurse_queue,
            'doctor_queue' => $doctor_queue,
            'lab_queue' => $lab_queue,
            'pharm_queue' => $pharm_queue,
            'urgency' => $urgency,
            'pay_at_the_end' => $pay_at_the_end,
            'visit_status' => $edit_visitation_status
        );
        $this->db->where('visit_id', $visit_id);
        $this->db->update('visit', $vistation_update_data);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            //generate an error.. or use the  log_message() function to log your error
        }
    }

    public function edit_walkin_patient() {
        $walkin_id = $this->input->post('edit_walkin_id');
        $walkin_patient_name = $this->input->post('edit_inputwalkinPatientName');
        $walkin_phone = $this->input->post('edit_inputPhoneNumber');
        $walkin_department = $this->input->post('edit_department_name');
        $walkin_paid = $this->input->post('edit_payment_status');
        $walkin_status = $this->input->post('edit_walkin_status');
        $this->db->trans_start();
        $walkin_patient_update_data = array(
            'walkin_patient_name' => $walkin_patient_name,
            'walkin_department' => $walkin_department,
            'paid' => $walkin_paid,
            'walkin_status' => $walkin_status,
            'walkin_phone_no' => $walkin_phone
        );
        $this->db->where('walkin_id', $walkin_id);
        $this->db->update('walkin', $walkin_patient_update_data);
        $this->db->trans_complete();
        if ($this->db->trans_statsu() === FALSE) {
            
        }
    }

    public function edit_procedure_visit_data() {
        $this->db->trans_start();
        $procedure_visit_id = $this->input->post('edit_procedure_visit_id');
        $patient_name = $this->input->post('edit_inputPatientName');
        $service_offered = $this->input->post('edit_procedure_name');
        $patient_type = $this->input->post('edit_patient_type');
        $phone_no = $this->input->post('edit_inputPhoneNumber');
        $visit_status = $this->input->post('edit_visit_date');
        $procedure_visit_update = array(
            'patient_name' => $patient_name,
            'procedure_list_id' => $service_offered,
            'patient_type' => $patient_type,
            'patient_phone' => $phone_no,
            'status' => $visit_status
        );
        $this->db->where('procedure_visit_id', $procedure_visit_id);
        $this->db->update('procedure_visit', $procedure_visit_update);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function delete_procedure_visit_data($procedure_visit_id) {
        $in_Active = 'In Active';
        $this->db->trans_start();
        $data = array(
            'status' => $in_Active
        );

        $this->db->where('procedure_visit_id', $procedure_visit_id);
        $this->db->update('procedure_visit', $data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            // generate an error... or use the log_message() function to log your error
        }
    }

    public function delete_visitation_info($visit_id) {
        $in_Active = 'In Active';
        $this->db->trans_start();
        $data = array(
            'visit_status' => $in_Active
        );

        $this->db->where('visit_id', $visit_id);
        $this->db->update('visit', $data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            // generate an error... or use the log_message() function to log your error
        }
    }

    public function delete_walkin_patient($walkin_id) {
        $in_Active = 'In Active';
        $this->db->trans_start();
        $data = array(
            'walkin_status' => $in_Active
        );

        $this->db->where('walkin_id', $walkin_id);
        $this->db->update('walkin', $data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            // generate an error... or use the log_message() function to log your error
        }
    }

    public function patient_lab_history($patient_id) {
        $sql = "select concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name,"
                . "lab_test_result.date_added as date_added, concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name,'') as patient_name, "
                . "lab_test_result.test, lab_test_result.test_results from lab_test_result"
                . " inner join employee on employee.employee_id = lab_test_result.lab_tech_id inner join patient on patient.patient_id = lab_test_result.patient_id"
                . " where patient.patient_id='$patient_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function patient_prescription_history($patient_id) {
        $sql = "select concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name, 
            concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name,'') as patient_name,
            prescription.commodity_name,prescription.strength,prescription.route,
            prescription.frequency,prescription.duration,prescription.no_of_days,
            prescription.date from prescription 
            inner join employee on employee.employee_id = prescription.doctor_id 
            inner join patient on patient.patient_id = prescription.patient_id"
                . " where prescription.patient_id='$patient_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function patient_consultation_history($patient_id) {
        $sql = "select concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name, 
            concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name,'') as patient_name,
            consultation.complaints,consultation.medical_history,consultation.systematic_inquiry,
            consultation.examination_findings,consultation.final_diagnosis,consultation.working_diagnosis,
            consultation.date from consultation 
            inner join employee on employee.employee_id = consultation.doctor_id 
            inner join patient on patient.patient_id = consultation.patient_id where patient.patient_id='$patient_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function workload() {
        $sql = "select visit.visit_id , patient.patient_id, concat(employee.title,'',employee.f_name,'',employee.other_name,'',employee.l_name,'') as employee_name,"
                . "visit.visit_date, concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name) as patient_name , visit.doctor_start,"
                . " visit.doctor_end from visit inner join patient on patient.patient_id = visit.patient_id inner join employee on employee.employee_id = visit.doctor_id"
                . " where DATE(visit.start) = DATE(NOW())";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function patient_allergies($patient_id) {
        $sql = "select * from allergy where patient_id='$patient_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function get_commodity_info($commodity_id) {
        $sql = "Select * from commodity where commodity_id='$commodity_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function get_commodity_view($commodity_id) {
        $sql = "Select * from commodity_view where commodity_id='$commodity_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function get_stock_view($stock_id) {
        $sql = "Select * from stock_view where stock_id='$stock_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

}

?>