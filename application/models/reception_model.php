<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reception_model extends CI_Model {

    public function _construct() {
        parent::_construct();
    }

    function total_visits() {
        $sql = "select count(visit_id) as total_patient_visits from visit where DATE(start) = DATE(NOW())";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function total_walkin_patients() {
        $sql = "select count(walkin_id) as total_walkin_patient from walkin where walkin_date >=CURDATE()";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_all_entries() {

        $sql = "select visit.family_number as family_number, visit.visit_date as visit_date,visit.visit_id as visit_id , packages.package_name as package_name, concat(patient.title,'',patient.f_name,' ',patient.s_name,' ',' ',patient.other_name) as patient_name from visit"
                . " inner join packages on packages.package_id=visit.package_type "
                . "inner join patient on patient.patient_id= visit.patient_id order by visit.visit_date desc";
        $query = $this->db->query($sql);

        return $query->result();
    }

    public function title() {

        $sql = "select * from title  order by title_name asc";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function update_entry($data) {

        $this->db->trans_start();
        $this->nurse_queue = $data['nurse_queue']; // please read the below note
        $this->lab_queue = $data['lab_queue'];
        $this->doctor_queue = $data['doctor_queue'];
        $this->pharm_queue = $data['pharm_queue'];
        $this->urgency = $data['urgency'];

        $this->db->update('visit', $this, array('visit_id' => $data['visit_id']));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            // generate an error... or use the log_message() function to log your error
        }
    }

    public function Active_nurses() {
        $sql = "SELECT DISTINCT employee.employee_id,employee.user_name,login_logs.is_Active , department.department_name
                 FROM employee
                 INNER JOIN login_logs
               ON employee.employee_id = login_logs.employee_id
               INNER JOIN department 
               ON employee.employment_category = department.department_id
                 WHERE login_logs.is_Active = 'Active' 
                 AND department.department_name='Nurse'
                 ORDER BY (employee.employee_id)asc";
        $result = $this->db->query($sql);
        $Active_nurse = $result->result_array();
        return $Active_nurse;
    }

    public function Active_doctors() {
        $sql = "SELECT DISTINCT employee.employee_id,employee.user_name,login_logs.is_Active , department.department_name
                 FROM employee
                 INNER JOIN login_logs
               ON employee.employee_id = login_logs.employee_id
               INNER JOIN department 
               ON employee.employment_category = department.department_id
                 WHERE login_logs.is_Active = 'Active' 
                 AND department.department_name='Doctor'
                 ORDER BY (employee.employee_id)asc";
        $result = $this->db->query($sql);
        $Active_doctor = $result->result_array();
        return $Active_doctor;
    }

    public function Active_pharmacist() {
        $sql = "SELECT DISTINCT employee.employee_id,employee.user_name,login_logs.is_Active , department.department_name
                 FROM employee
                 INNER JOIN login_logs
               ON employee.employee_id = login_logs.employee_id
               INNER JOIN department 
               ON employee.employment_category = department.department_id
                 WHERE login_logs.is_Active = 'Active' 
                 AND department.department_name='Pharmacy'
                 ORDER BY (employee.employee_id)asc";
        $result = $this->db->query($sql);
        $Active_pharmacy = $result->result_array();
        return $Active_pharmacy;
    }

    public function Active_laboratories() {
        $sql = "SELECT DISTINCT employee.employee_id,employee.user_name,login_logs.is_Active , department.department_name
                 FROM employee
                 INNER JOIN login_logs
               ON employee.employee_id = login_logs.employee_id
               INNER JOIN department 
               ON employee.employment_category = department.department_id
                 WHERE login_logs.is_Active = 'Active' 
                 AND department.department_name='Laboratory'
                 ORDER BY (employee.employee_id)asc";
        $result = $this->db->query($sql);
        $Active_laboratories = $result->result_array();
        return $Active_laboratories;
    }

    public function release_patient($id) {
        $today = date("H:i:s");
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $data = array(
            'nurse_queue' => 'In Active',
            'doctor_queue' => 'In Active',
            'lab_queue' => 'In Active',
            'rad_queue' => 'In Active',
            'pharm_queue' => 'In Active',
            'pharm_end' => $today
        );
        $this->db->where('member_id', $member_id);
        $this->db->where('branch_id', $branch_id);
        $this->db->where('visit_id', $id);
        $this->db->update('visit', $data);
    }

    function get_entry($id) {

        $sql = " select visit.family_number as family_number,visit.urgency,visit.pay_at_the_end, visit.nurse_queue,visit.doctor_queue,visit.lab_queue,visit.pharm_queue,visit.visit_date as visit_date,visit.visit_id as visit_id , packages.package_name as package_name, concat(patient.title,'',patient.f_name,' ',patient.s_name,' ',' ',patient.other_name) as patient_name from visit
                  inner join packages on packages.package_id=visit.package_type 
                  inner join patient on patient.patient_id= visit.patient_id where visit.visit_id='$id'";
        $query = $this->db->query($sql);

        return $query->result();
    }

    public function all() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT patients.fname, visit.patientid, patients.lname, patients.sname, visit.visitdate, visit.id
        FROM patients
        INNER JOIN visit ON patients.id = visit.patientid
        WHERE visit.visitdate >= DATE_ADD( CURDATE( ) , INTERVAL -1
        MONTH )
        AND visit.nq = 'Active' AND visit.member_id='$member_id' AND visit.branch_id='$branch_id'
        ORDER BY visit.visitdate
        LIMIT 6";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT patients.fname, patients.lname, patients.sname,visit.id,patients.id,costs.visitid
        FROM patients
        INNER JOIN visit ON patients.id = visit.patientid
        INNER JOIN costs ON visit.id = costs.visitid
        WHERE costs.paid = 'not paid' AND patients.member_id='$member_id' AND patients.branch_id='$branch_id'";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function get_new_patient_addpat_count() {
        $month = date('F');
        $year = date('Y');
        $Continuing_Family = "Continuing Family";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT distinct count from new_patients_tracker where new_patients_tracker.month='$month' AND new_patients_tracker.year='$year' AND patient_type='$Continuing_Family' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->count;
        }
    }

    public function get_new_patient_addpat_year() {
        $month = date('F');
        $year = date('Y');
        $Continuing_Family = "Continuing Family";
        $sql = "SELECT distinct year from new_patients_tracker where new_patients_tracker.month='$month' AND new_patients_tracker.year='$year' AND patient_type='$Continuing_Family' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->year;
        }
    }

    public function get_new_patient_addpat_month() {
        $month = date('F');
        $year = date('Y');
        $Continuing_Family = "Continuing Family";
        $sql = "SELECT distinct month from new_patients_tracker where new_patients_tracker.month='$month' AND new_patients_tracker.year='$year' AND patient_type='$Continuing_Family' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->month;
        }
    }

    public function member_get_new_patient_addpat_count() {
        $month = date('F');
        $year = date('Y');
        $Continuing_Family = "Continuing Family";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT distinct count from member_patient_tracker where member_patient_tracker.month='$month' AND member_patient_tracker.year='$year' AND patient_type='$Continuing_Family' AND branch_id='$branch_id' AND member_id = '$member_id' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->count;
        }
    }

    public function member_get_new_patient_addpat_year() {
        $month = date('F');
        $year = date('Y');
        $Continuing_Family = "Continuing Family";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT distinct year from member_patient_tracker where member_patient_tracker.month='$month' AND member_patient_tracker.year='$year' AND patient_type='$Continuing_Family' AND branch_id='$branch_id' AND member_id = '$member_id' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->year;
        }
    }

    public function member_get_new_patient_addpat_month() {
        $month = date('F');
        $year = date('Y');
        $Continuing_Family = "Continuing Family";
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT distinct month from member_patient_tracker where member_patient_tracker.month='$month' AND member_patient_tracker.year='$year' AND patient_type='$Continuing_Family' AND branch_id='$branch_id' AND member_id = '$member_id' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->month;
        }
    }

    public function get_new_patient_count() {
        $month = date('F');
        $year = date('Y');
        $New_Patient = "New Patient";
        $sql = "SELECT distinct count from new_patients_tracker where new_patients_tracker.month='$month' AND new_patients_tracker.year='$year' AND patient_type='$New_Patient' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->count;
        }
    }

    public function get_new_patient_year() {
        $month = date('F');
        $year = date('Y');
        $New_Patient = "New Patient";
        $sql = "SELECT distinct year from new_patients_tracker where new_patients_tracker.month='$month' AND new_patients_tracker.year='$year' AND patient_type='$New_Patient' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->year;
        }
    }

    public function get_new_patient_month() {
        $month = date('F');
        $year = date('Y');
        $New_Patient = "New Patient";
        $sql = "SELECT distinct month from new_patients_tracker where new_patients_tracker.month='$month' AND new_patients_tracker.year='$year' AND patient_type='$New_Patient' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->month;
        }
    }

    function generate_random_letters($length) {
        $random = '';
        for ($i = 0; $i < $length; $i++) {
            $random_1 = chr(rand(ord('1'), ord('999')));
            $random_2 = chr(rand(ord('A'), ord('Z')));
            $stamp = date("Ymdhis");
            $randomised_stamp = rand($stamp, 1);
            $randomised_stamp = str_replace("-", "", $randomised_stamp);
            $randomised_stamp = strrev(str_replace("!@#$%^&*()|:}{<>?|-_+=", " ", $randomised_stamp));


            $random .= $random_1 . $randomised_stamp . $random_2;
        }
        return $random;
    }

    public function add_patient() {
        $title = $this->input->post('title');
        $f_name = $this->input->post('fname');
        $s_name = $this->input->post('sname');
        $other_name = $this->input->post('oname');
        $dob = $this->input->post('dob');
        $national_id = $this->input->post('nationalid');
        $postal_address = $this->input->post('address');
        $residence = $this->input->post('residence');
        $city = $this->input->post('city');
        $phone_no = $this->input->post('phone_no');
        $email = $this->input->post('email');
        $employment_status = $this->input->post('employment_status');
        $employer = $this->input->post('employers_name');
        $gender = $this->input->post('gender');
        $marital_status = $this->input->post('marital_status');
        $kin_f_name = $this->input->post('kinfname');
        $kin_s_name = $this->input->post('kinsname');
        $kin_o_name = $this->input->post('kinoname');
        $kin_relation = $this->input->post('kinrelation');
        $kin_phone = $this->input->post('kinphone');
        $join_clinic = $this->input->post('join_network');
        $identification_number = $this->input->post('nationalid');
        $family_base_number = $this->input->post('copied_family_number');
        echo 'Title :' . $title . '<br> F name : ' . $f_name . '<br> S Name : ' . $s_name . '<br> O Name : ' . $other_name . '<br> Dob : ' . $dob . '<br> National ID : ' . $national_id . '<br> Address : ' . $postal_address . '<br> Residence :  ' . $residence . '<br> City : ' . $city . '<br> Phone No : ' . $phone_no . '<br> Email :  ' . $email . '<br> Employed :  ' . $employment_status . '<br> Employer : ' . $employer . '<br> Gender :  ' . $gender . '<br> Marital Status : ' . $marital_status . '';
        echo '<br> Kind F Name : ' . $kin_f_name . '<br> Kin Oname : ' . $kin_o_name . '<br> Kin Relation : ' . $kin_relation . '<br> Kin Phone  : ' . $kin_phone . '<br> Join Clinic :  ' . $join_clinic . '<br>';

        echo 'Family Base Number : ' . $family_base_number . '<br>';


        $date_joined = date("Y-m-d H:i:s");

        echo 'Date Joined is : ' . $date_joined . '<br>';
        //Generate a unique EMR Number : 
        $start = 0;
        $length = 1;

        $random_numbers = $this->generate_random_letters(1);
        echo 'Random Number : ' . $random_numbers . '<br>';
        $emr_number = substr($f_name, $start, $length) . substr($s_name, $start, $length) . '-' . substr($gender, $start, $length) . '-' . $random_numbers;
        echo 'EMR NUMBER IS .....: ' . $emr_number . '<br>';
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $this->db->set('emr_no', $emr_number);
        $this->db->set('title', $title);
        $this->db->set('title', $title);
        $this->db->set('f_name', $f_name);
        $this->db->set('s_name', $s_name);
        $this->db->set('other_name', $other_name);
        $this->db->set('dob', $dob);
        $this->db->set('residence', $residence);
        $this->db->set('phone_no', $phone_no);
        $this->db->set('email', $email);
        $this->db->set('marital_status', $marital_status);
        $this->db->set('next_of_kin_fname', $kin_f_name);
        $this->db->set('next_of_kin_lname', $kin_s_name);
        $this->db->set('next_of_kin_phone', $kin_phone);
        $this->db->set('employment_status', $employment_status);
        $this->db->set('employer', $employer);
        $this->db->set('gender', $gender);
        $this->db->set('city', $city);
        $this->db->set('next_of_kin_relation', $kin_relation);
        $this->db->set('identification_number', $identification_number);


        if ($join_clinic === "Yes") {

            if (empty($family_base_number)) {
                //new family number registration
                echo 'ONe';

                $query = $this->db->query("SELECT MAX(family_base_number) as family_base_number FROM patient");
                $row = $query->row();
                $last_inserted_family_number_base = $row->family_base_number;
                if (!is_null($last_inserted_family_number_base)) {




                    $last_inserted_family_number_base = $row->family_base_number;
                    $under_score = "_";
                    $family_number_parent = 0;
                    $zero = 0;

                    $one = 1;
                    $last_inserted_family_number_base_value = substr($last_inserted_family_number_base, 7, 13);
                    $new_patient_family_number_last_number_base = $last_inserted_family_number_base_value + $one;
                    $family_number_base_letter = substr($last_inserted_family_number_base, 0, 6);



                    $new_patient_family_number_parent = $family_number_base_letter . $under_score . $new_patient_family_number_last_number_base . $under_score . $family_number_parent;

                    $new_family_parent_base_number = $family_number_base_letter . $under_score . $new_patient_family_number_last_number_base;


                    $this->db->set('family_base_number', $new_family_parent_base_number);
                    $this->db->set('family_number', $new_patient_family_number_parent);
                } else {

                    $family_base_number = "SHWARI_1000001";
                    $family_member_number = "SHWARI_1000001_0";
                    $this->db->set('family_base_number', $family_base_number);
                    $this->db->set('family_number', $family_member_number);
                }
            } else if (!empty($family_base_number)) {

                //new family member registration
                $query = $this->db->query("SELECT MAX(family_number) as family_number FROM patient WHERE family_number LIKE '%$family_base_number%'");
                if ($query->num_rows() > 0) {
                    $row = $query->row();
                    $last_family_number = $row->family_number;
                    $under_score = "_";

                    $last_family_number = substr($last_family_number, 15, 17);


                    $one = 1;
                    $new_patient_family_number_last_number = $last_family_number + $one;

                    $new_patient_family_number = $family_base_number . $under_score . $new_patient_family_number_last_number;

                    $this->db->set('family_number', $new_patient_family_number);
                    $this->db->set('family_base_number', $family_base_number);
                } else {

                    $family_base_number = "SHWARI_1000001";
                    $family_member_number = "SHWARI_1000001_0";
                    $this->db->set('family_base_number', $family_base_number);
                    $this->db->set('family_number', $family_member_number);
                }
            }
        } elseif ($join_clinic === "No") {
            
        }

        $this->db->set('member_id', $member_id);
        $this->db->set('branch_id', $branch_id);
        $this->db->set('date_joined', $date_joined);

        $this->db->insert('patient');
    }

    public function addpat() {
        $this->db->trans_start();
        $family_number = $this->input->post('family_number', TRUE);
        $family_number = mysql_real_escape_string($family_number);
        if (empty($family_number)) {
            
        } else {

            $family_number_base = substr($family_number, 0, 14);
            echo 'Family Number Base' . $family_number_base . '</br>';
            $date_joined = date('Y-m-d H:i:s');
            $query = $this->db->query("SELECT MAX(family_number) as family_number FROM patient WHERE family_number LIKE '%$family_number_base%'");
            if ($query->num_rows() > 0) {
                $row = $query->row();

                echo $row->family_number;
                echo 'Dindi _ Family Number';
                echo $row->family_number;

                $last_inserted_family_number = $row->family_number;
                $under_score = "_";

                $last_inserted_family_number = substr($last_inserted_family_number, 15, 17);


                $one = 1;
                $new_patient_family_number_last_number = $last_inserted_family_number + $one;

                $new_patient_family_number = $family_number_base . $under_score . $new_patient_family_number_last_number;


                $title = $this->input->post('title_name', TRUE);
                $title = mysql_real_escape_string($title);
                $nationalid = $this->input->post('nationalid', TRUE);
                $nationalid = mysql_real_escape_string($nationalid);
                $sname = $this->input->post('sname', TRUE);
                $sname = mysql_real_escape_string($sname);
                $fname = $this->input->post('fname', TRUE);
                $fname = mysql_real_escape_string($fname);
                $lname = $this->input->post('lname', TRUE);
                $lname = mysql_real_escape_string($lname);
                $dob = $this->input->post('dob', TRUE);
                $dob = mysql_real_escape_string($dob);
                $address = $this->input->post('residence', TRUE);
                $address = mysql_real_escape_string($address);
                $phone = $this->input->post('phone_no', TRUE);
                $phone = mysql_real_escape_string($phone);
                $email = $this->input->post('email', TRUE);
                $email = mysql_real_escape_string($email);
                $maritalstatus = $this->input->post('maritalstatus', TRUE);
                $maritalstatus = mysql_real_escape_string($maritalstatus);
                $kinfname = $this->input->post('kinname', TRUE);
                $kinfname = mysql_real_escape_string($kinfname);
                $kinsname = $this->input->post('kinsname', TRUE);
                $kinsname = mysql_real_escape_string($kinsname);
                $kinphone = $this->input->post('kinphone', TRUE);
                $kinphone = mysql_real_escape_string($kinphone);
                $sex = $this->input->post('sex', TRUE);
                $sex = mysql_real_escape_string($sex);
                $employer = $this->input->post('employer', TRUE);
                $employer = mysql_real_escape_string($employer);
                $residence = $this->input->post('residence', TRUE);
                $residence = mysql_real_escape_string($residence);
                $employmentstatus = $this->input->post('employment_status', TRUE);
                $employmentstatus = mysql_real_escape_string($employmentstatus);
                $kinrelation = $this->input->post('kinrelation', TRUE);
                $kinrelation = mysql_real_escape_string($kinrelation);
                $is_shwari_member = "YES";
                $member_id = $this->session->userdata('member_id');
                $branch_id = $this->session->userdata('branch_id');

                $this->db->set('title', $title);
                $this->db->set('identification_number', $nationalid);
                $this->db->set('f_name', $fname);
                $this->db->set('s_name', $sname);
                $this->db->set('other_name', $lname);
                $this->db->set('dob', $dob);
                $this->db->set('residence', $address);
                $this->db->set('phone_no', $phone);
                $this->db->set('email', $email);
                $this->db->set('marital_status', $maritalstatus);
                $this->db->set('next_of_kin_fname', $kinfname);
                $this->db->set('next_of_kin_lname', $kinsname);
                $this->db->set('next_of_kin_phone', $kinphone);
                $this->db->set('employment_status', $employmentstatus);
                $this->db->set('employer', $employer);
                $this->db->set('gender', $sex);
                $this->db->set('residence', $residence);
                $this->db->set('next_of_kin_relation', $kinrelation);
                $this->db->set('family_number', $new_patient_family_number);
                $this->db->set('family_base_number', $family_number);
                $this->db->set('member_id', $member_id);
                $this->db->set('branch_id', $branch_id);
                $this->db->set('date_joined', $date_joined);

                $this->db->insert('patient');

                $new_patient_addpat_month = $this->get_new_patient_addpat_month();
                $new_patient_addpat_year = $this->get_new_patient_addpat_year();
                $new_patient_addpat_count = $this->get_new_patient_addpat_count();



                $member_new_patient_addpat_month = $this->member_get_new_patient_addpat_month();
                $member_new_patient_addpat_year = $this->member_get_new_patient_addpat_year();
                $member_new_patient_addpat_count = $this->member_get_new_patient_addpat_count();

                $number = 1;
                $month = date('F');
                $year = date('Y');
                $Continuing_Family = "Continuing Family";


                if (empty($new_patient_addpat_month) && empty($new_patient_addpat_year)) {

                    if (empty($member_new_patient_addpat_count) && empty($member_new_patient_addpat_month) && empty($member_new_patient_addpat_year)) {
                        $member_branch_data = array(
                            'count' => $number,
                            'month' => $month,
                            'year' => $year,
                            'patient_type' => $Continuing_Family,
                            'member_id' => $member_id,
                            'branch_id' => $branch_id
                        );
                        $this->db->insert('member_patient_tracker', $member_branch_data);
                    } else {
                        $number = 1;
                        $new_count = $member_new_patient_addpat_count + $number;
                        $new_member_count = array(
                            'count' => $new_count
                        );
                        $this->db->where('member_id', $member_id);
                        $this->db->where('branch_id', $branch_id);
                        $this->db->where('month', $month);
                        $this->db->where('year', $year);
                        $this->db->where('patient_type', $Continuing_Family);
                        $this->db->update('member_patient_tracker', $new_member_count);
                    }


                    $new_patient_month = $this->get_new_patient_month();
                    $new_patient_year = $this->get_new_patient_year();
                    $new_patient_count = $this->get_new_patient_count();

                    $number = 1;
                    $month = date('F');
                    $year = date('Y');
                    $new_patient = "New Patient";

                    if (empty($new_patient_month) && empty($new_patient_year)) {

                        $data1 = array(
                            'count' => $number,
                            'month' => $month,
                            'year' => $year,
                            'patient_type' => $new_patient,
                            'branch_id' => $branch_id,
                            'member_id' => $member_id
                        );
                        $this->db->insert('member_patient_tracker', $data1);
                    } else {
                        $new_count = $new_patient_count + $number;
                        $data3 = array(
                            'count' => $new_count
                        );
                        $this->db->where('month', $month);
                        $this->db->where('year', $year);
                        $this->db->where('member_id', $member_id);
                        $this->db->where('branch_id', $branch_id);
                        $this->db->where('patient_type', $new_patient);
                        $this->db->update('member_patient_tracker', $data3);
                    }


                    $data1 = array(
                        'count' => $number,
                        'month' => $month,
                        'year' => $year,
                        'patient_type' => $Continuing_Family
                    );
                    $this->db->insert('new_patients_tracker', $data1);
                } else {

                    if (empty($member_new_patient_addpat_count) && empty($member_new_patient_addpat_month) && empty($member_new_patient_addpat_year)) {
                        $member_branch_data = array(
                            'count' => $number,
                            'month' => $month,
                            'year' => $year,
                            'patient_type' => $Continuing_Family,
                            'member_id' => $member_id,
                            'branch_id' => $branch_id
                        );
                        $this->db->insert('member_patient_tracker', $member_branch_data);
                    } else {
                        $number = 1;
                        $new_count = $member_new_patient_addpat_count + $number;
                        $new_member_count = array(
                            'count' => $new_count
                        );
                        $this->db->where('member_id', $member_id);
                        $this->db->where('branch_id', $branch_id);
                        $this->db->where('month', $month);
                        $this->db->where('year', $year);
                        $this->db->where('patient_type', $Continuing_Family);
                        $this->db->update('member_patient_tracker', $new_member_count);


                        $new_patient_month = $this->get_new_patient_month();
                        $new_patient_year = $this->get_new_patient_year();
                        $new_patient_count = $this->get_new_patient_count();

                        $number = 1;
                        $month = date('F');
                        $year = date('Y');
                        $new_patient = "New Patient";

                        if (empty($new_patient_month) && empty($new_patient_year)) {

                            $data1 = array(
                                'count' => $number,
                                'month' => $month,
                                'year' => $year,
                                'patient_type' => $new_patient,
                                'branch_id' => $branch_id,
                                'member_id' => $member_id
                            );
                            $this->db->insert('member_patient_tracker', $data1);
                        } else {
                            $new_count = $new_patient_count + $number;
                            $data3 = array(
                                'count' => $new_count
                            );
                            $this->db->where('month', $month);
                            $this->db->where('year', $year);
                            $this->db->where('member_id', $member_id);
                            $this->db->where('branch_id', $branch_id);
                            $this->db->where('patient_type', $new_patient);
                            $this->db->update('member_patient_tracker', $data3);
                        }
                    }



                    $new_count = $new_patient_addpat_count + $number;
                    $data3 = array(
                        'count' => $new_count
                    );
                    $this->db->where('month', $month);
                    $this->db->where('year', $year);
                    $this->db->where('patient_type', $Continuing_Family);
                    $this->db->update('new_patients_tracker', $data3);
                }



                $new_patient_month = $this->get_new_patient_month();
                $new_patient_year = $this->get_new_patient_year();
                $new_patient_count = $this->get_new_patient_count();

                $number = 1;
                $month = date('F');
                $year = date('Y');
                $new_patient = "New Patient";

                if (empty($new_patient_month) && empty($new_patient_year)) {

                    $data1 = array(
                        'count' => $number,
                        'month' => $month,
                        'year' => $year,
                        'patient_type' => $new_patient
                    );
                    $this->db->insert('new_patients_tracker', $data1);
                    $new_patient_month = $this->get_new_patient_month();
                    $new_patient_year = $this->get_new_patient_year();
                    $new_patient_count = $this->get_new_patient_count();

                    $number = 1;
                    $month = date('F');
                    $year = date('Y');
                    $new_patient = "New Patient";

                    if (empty($new_patient_month) && empty($new_patient_year)) {

                        $data1 = array(
                            'count' => $number,
                            'month' => $month,
                            'year' => $year,
                            'patient_type' => $new_patient
                        );
                        $this->db->insert('new_patients_tracker', $data1);
                    } else {
                        $new_count = $new_patient_count + $number;
                        $data3 = array(
                            'count' => $new_count
                        );
                        $this->db->where('month', $month);
                        $this->db->where('year', $year);
                        $this->db->where('patient_type', $new_patient);
                        $this->db->update('new_patients_tracker', $data3);
                    }
                } else {
                    $new_count = $new_patient_count + $number;
                    $data3 = array(
                        'count' => $new_count
                    );
                    $this->db->where('month', $month);
                    $this->db->where('year', $year);
                    $this->db->where('patient_type', $new_patient);
                    $this->db->update('new_patients_tracker', $data3);

                    $new_patient_month = $this->get_new_patient_month();
                    $new_patient_year = $this->get_new_patient_year();
                    $new_patient_count = $this->get_new_patient_count();

                    $number = 1;
                    $month = date('F');
                    $year = date('Y');
                    $new_patient = "New Patient";

                    if (empty($new_patient_month) && empty($new_patient_year)) {

                        $data1 = array(
                            'count' => $number,
                            'month' => $month,
                            'year' => $year,
                            'patient_type' => $new_patient
                        );
                        $this->db->insert('new_patients_tracker', $data1);
                    } else {
                        $new_count = $new_patient_count + $number;
                        $data3 = array(
                            'count' => $new_count
                        );
                        $this->db->where('month', $month);
                        $this->db->where('year', $year);
                        $this->db->where('patient_type', $new_patient);
                        $this->db->update('new_patients_tracker', $data3);
                    }
                }
            }
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                // generate an error... or use the log_message() function to log your error
            }
        }
    }

    public function get_new_patient_new_family_tree_count() {
        $month = date('F');
        $year = date('Y');
        $new_family = 'New Family';
        $sql = "SELECT distinct count from new_patients_tracker where new_patients_tracker.month='$month' AND new_patients_tracker.year='$year' AND patient_type='$new_family' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->count;
        }
    }

    public function get_new_patient_new_family_tree_year() {
        $month = date('F');
        $year = date('Y');
        $new_family = 'New Family';
        $sql = "SELECT distinct year from new_patients_tracker where new_patients_tracker.month='$month' AND new_patients_tracker.year='$year' AND patient_type='$new_family' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->year;
        }
    }

    public function get_new_patient_new_family_tree_month() {
        $month = date('F');
        $year = date('Y');
        $new_family = 'New Family';
        $sql = "SELECT distinct month from new_patients_tracker where new_patients_tracker.month='$month' AND new_patients_tracker.year='$year' AND patient_type='$new_family' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->month;
        }
    }

    public function member_get_new_patient_new_family_tree_count() {
        $month = date('F');
        $year = date('Y');
        $new_family = 'New Family';
        $branch_id = $this->session->userdata('branch_id');
        $member_id = $this->session->userdata('member_id');
        $sql = "SELECT distinct count from member_patient_tracker where member_patient_tracker.month='$month' AND member_patient_tracker.year='$year' AND patient_type='$new_family' AND member_id='$member_id' AND branch_id='$branch_id' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->count;
        }
    }

    public function member_get_new_patient_new_family_tree_year() {
        $month = date('F');
        $year = date('Y');
        $new_family = 'New Family';
        $branch_id = $this->session->userdata('branch_id');
        $member_id = $this->session->userdata('member_id');
        $sql = "SELECT distinct year from member_patient_tracker where member_patient_tracker.month='$month' AND member_patient_tracker.year='$year' AND patient_type='$new_family' AND member_id='$member_id' AND branch_id='$branch_id' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->year;
        }
    }

    public function member_get_new_patient_new_family_tree_month() {
        $month = date('F');
        $year = date('Y');
        $new_family = 'New Family';
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT distinct month from member_patient_tracker where member_patient_tracker.month='$month' AND member_patient_tracker.year='$year' AND patient_type='$new_family' AND branch_id = '$branch_id' AND member_id='$member_id' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->month;
        }
    }

    public function new_family_tree() {

        $this->db->trans_start();


        $query = $this->db->query("SELECT MAX(family_base_number) as family_base_number FROM patient");
        if ($query->num_rows() > 0) {
            $row = $query->row();


            $last_inserted_family_number_base = $row->family_base_number;
            $under_score = "_";
            $family_number_parent = 0;
            $zero = 0;

            $one = 1;
            $last_inserted_family_number_base_value = substr($last_inserted_family_number_base, 7, 13);
            $new_patient_family_number_last_number_base = $last_inserted_family_number_base_value + $one;
            $family_number_base_letter = substr($last_inserted_family_number_base, 0, 6);



            $new_patient_family_number_parent = $family_number_base_letter . $under_score . $new_patient_family_number_last_number_base . $under_score . $family_number_parent;

            $new_family_parent_base_number = $family_number_base_letter . $under_score . $new_patient_family_number_last_number_base;

            $date_joined = date('Y-m-d H:i:s');
            $title = $this->input->post('title', TRUE);
            $title = mysql_real_escape_string($title);
            $nationalid = $this->input->post('nationalid', TRUE);
            $nationalid = mysql_real_escape_string($nationalid);
            $sname = $this->input->post('sname', TRUE);
            $sname = mysql_real_escape_string($sname);
            $fname = $this->input->post('fname', TRUE);
            $fname = mysql_real_escape_string($fname);
            $lname = $this->input->post('lname', TRUE);
            $lname = mysql_real_escape_string($lname);
            $dob = $this->input->post('dob', TRUE);
            $dob = mysql_real_escape_string($dob);
            $address = $this->input->post('address', TRUE);
            $address = mysql_real_escape_string($address);
            $phone = $this->input->post('phone_no', TRUE);
            $phone = mysql_real_escape_string($phone);
            $email = $this->input->post('email', TRUE);
            $email = mysql_real_escape_string($email);
            $maritalstatus = $this->input->post('maritalstatus', TRUE);
            $maritalstatus = mysql_real_escape_string($maritalstatus);
            $kinfname = $this->input->post('kinname', TRUE);
            $kinfname = mysql_real_escape_string($kinfname);
            $kinsname = $this->input->post('kinsname', TRUE);
            $kinsname = mysql_real_escape_string($kinsname);
            $kinphone = $this->input->post('kinphone', TRUE);
            $kinphone = mysql_real_escape_string($kinphone);
            $sex = $this->input->post('sex', TRUE);
            $sex = mysql_real_escape_string($sex);
            $employer = $this->input->post('employers_name', TRUE);
            $employer = mysql_real_escape_string($employer);
            $residence = $this->input->post('address', TRUE);
            $residence = mysql_real_escape_string($residence);
            $employmentstatus = $this->input->post('employment_status', TRUE);
            $employmentstatus = mysql_real_escape_string($employmentstatus);
            $kinrelation = $this->input->post('kinrelation', TRUE);
            $kinrelation = mysql_real_escape_string($kinrelation);
            $is_shwari_member = "YES";
            $member_id = $this->session->userdata('member_id');
            $branch_id = $this->session->userdata('branch_id');
            $this->db->set('title', $title);
            $this->db->set('date_joined', $date_joined);
            $this->db->set('identification_number', $nationalid);
            $this->db->set('f_name', $fname);
            $this->db->set('s_name', $sname);
            $this->db->set('other_name', $lname);
            $this->db->set('dob', $dob);
            $this->db->set('residence', $address);
            $this->db->set('phone_no', $phone);
            $this->db->set('email', $email);
            $this->db->set('marital_status', $maritalstatus);
            $this->db->set('next_of_kin_fname', $kinfname);
            $this->db->set('next_of_kin_lname', $kinsname);
            $this->db->set('next_of_kin_phone', $kinphone);
            $this->db->set('employment_status', $employmentstatus);
            $this->db->set('employer', $employer);
            $this->db->set('gender', $sex);
            $this->db->set('residence', $residence);
            $this->db->set('next_of_kin_relation', $kinrelation);
            $this->db->set('family_base_number', $new_family_parent_base_number);
            $this->db->set('family_number', $new_patient_family_number_parent);
            $this->db->set('member_id', $member_id);
            $this->db->set('branch_id', $branch_id);

            $this->db->insert('patient');



            $new_patient_month = $this->get_new_patient_new_family_tree_month();
            $new_patient_year = $this->get_new_patient_new_family_tree_year();
            $new_patient_count = $this->get_new_patient_new_family_tree_year();

            $member_new_patient_month = $this->member_get_new_patient_new_family_tree_month();
            $member_new_patient_year = $this->member_get_new_patient_new_family_tree_year();
            $member_new_patient_count = $this->member_get_new_patient_new_family_tree_count();

            $number = 1;
            $month = date('F');
            $year = date('Y');
            $NewFamily = "New Family";

            if (empty($new_patient_month) && empty($new_patient_year)) {

                if (empty($member_new_patient_count) && empty($member_new_patient_month) && empty($member_new_patient_year)) {
                    $member_insert = array(
                        'count' => $number,
                        'month' => $month,
                        'year' => $year,
                        'patient_type' => $NewFamily,
                        'member_id' => $member_id,
                        'branch_id' => $branch_id
                    );
                    $this->db->insert('member_patient_tracker', $member_insert);


                    $new_patient_month = $this->get_new_patient_month();
                    $new_patient_year = $this->get_new_patient_year();
                    $new_patient_count = $this->get_new_patient_count();

                    $number = 1;
                    $month = date('F');
                    $year = date('Y');
                    $new_patient = "New Patient";

                    if (empty($new_patient_month) && empty($new_patient_year)) {

                        $data1 = array(
                            'count' => $number,
                            'month' => $month,
                            'year' => $year,
                            'patient_type' => $new_patient,
                            'branch_id' => $branch_id,
                            'member_id' => $member_id
                        );
                        $this->db->insert('member_patient_tracker', $data1);
                    } else {
                        $new_count = $new_patient_count + $number;
                        $data3 = array(
                            'count' => $new_count
                        );
                        $this->db->where('month', $month);
                        $this->db->where('year', $year);
                        $this->db->where('member_id', $member_id);
                        $this->db->where('branch_id', $branch_id);
                        $this->db->where('patient_type', $new_patient);
                        $this->db->update('member_patient_tracker', $data3);
                    }
                } else {
                    $count = $this->member_get_new_patient_new_family_tree_count();
                    $new_count = $count + $number;
                    $updated_member_count = array(
                        'count' => $new_count
                    );
                    $this->db->where('month', $month);
                    $this->db->where('year', $year);
                    $this->db->where('patient_type', $NewFamily);
                    $this->db->where('branch_id', $branch_id);
                    $this->db->where('member_id', $member_id);
                    $this->db->update('member_patient_tracker', $updated_member_count);

                    $new_patient_month = $this->get_new_patient_month();
                    $new_patient_year = $this->get_new_patient_year();
                    $new_patient_count = $this->get_new_patient_count();

                    $number = 1;
                    $month = date('F');
                    $year = date('Y');
                    $new_patient = "New Patient";

                    if (empty($new_patient_month) && empty($new_patient_year)) {

                        $data1 = array(
                            'count' => $number,
                            'month' => $month,
                            'year' => $year,
                            'patient_type' => $new_patient,
                            'branch_id' => $branch_id,
                            'member_id' => $member_id
                        );
                        $this->db->insert('member_patient_tracker', $data1);
                    } else {
                        $new_count = $new_patient_count + $number;
                        $data3 = array(
                            'count' => $new_count
                        );
                        $this->db->where('month', $month);
                        $this->db->where('year', $year);
                        $this->db->where('member_id', $member_id);
                        $this->db->where('branch_id', $branch_id);
                        $this->db->where('patient_type', $new_patient);
                        $this->db->update('member_patient_tracker', $data3);
                    }
                }

                $data1 = array(
                    'count' => $number,
                    'month' => $month,
                    'year' => $year,
                    'patient_type' => $NewFamily
                );
                $this->db->insert('new_patients_tracker', $data1);
            } else {

                if (empty($member_new_patient_count) && empty($member_new_patient_month) && empty($member_new_patient_year)) {
                    $member_insert = array(
                        'count' => $number,
                        'month' => $month,
                        'year' => $year,
                        'patient_type' => $NewFamily,
                        'member_id' => $member_id,
                        'branch_id' => $branch_id
                    );
                    $this->db->insert('member_patient_tracker', $member_insert);


                    $new_patient_month = $this->get_new_patient_month();
                    $new_patient_year = $this->get_new_patient_year();
                    $new_patient_count = $this->get_new_patient_count();

                    $number = 1;
                    $month = date('F');
                    $year = date('Y');
                    $new_patient = "New Patient";

                    if (empty($new_patient_month) && empty($new_patient_year)) {

                        $data1 = array(
                            'count' => $number,
                            'month' => $month,
                            'year' => $year,
                            'patient_type' => $new_patient,
                            'branch_id' => $branch_id,
                            'member_id' => $member_id
                        );
                        $this->db->insert('member_patient_tracker', $data1);
                    } else {
                        $new_count = $new_patient_count + $number;
                        $data3 = array(
                            'count' => $new_count
                        );
                        $this->db->where('month', $month);
                        $this->db->where('year', $year);
                        $this->db->where('member_id', $member_id);
                        $this->db->where('branch_id', $branch_id);
                        $this->db->where('patient_type', $new_patient);
                        $this->db->update('member_patient_tracker', $data3);
                    }
                } else {
                    $count = $this->member_get_new_patient_new_family_tree_count();
                    $new_count = $count + $number;
                    $updated_member_count = array(
                        'count' => $new_count
                    );
                    $this->db->where('month', $month);
                    $this->db->where('year', $year);
                    $this->db->where('patient_type', $NewFamily);
                    $this->db->where('branch_id', $branch_id);
                    $this->db->where('member_id', $member_id);
                    $this->db->update('member_patient_tracker', $updated_member_count);


                    $new_patient_month = $this->get_new_patient_month();
                    $new_patient_year = $this->get_new_patient_year();
                    $new_patient_count = $this->get_new_patient_count();

                    $number = 1;
                    $month = date('F');
                    $year = date('Y');
                    $new_patient = "New Patient";

                    if (empty($new_patient_month) && empty($new_patient_year)) {

                        $data1 = array(
                            'count' => $number,
                            'month' => $month,
                            'year' => $year,
                            'patient_type' => $new_patient,
                            'branch_id' => $branch_id,
                            'member_id' => $member_id
                        );
                        $this->db->insert('member_patient_tracker', $data1);
                    } else {
                        $new_count = $new_patient_count + $number;
                        $data3 = array(
                            'count' => $new_count
                        );
                        $this->db->where('month', $month);
                        $this->db->where('year', $year);
                        $this->db->where('member_id', $member_id);
                        $this->db->where('branch_id', $branch_id);
                        $this->db->where('patient_type', $new_patient);
                        $this->db->update('member_patient_tracker', $data3);
                    }
                }


                $new_count = $new_patient_count + $number;
                $data3 = array(
                    'count' => $new_count
                );
                $this->db->where('month', $month);
                $this->db->where('year', $year);
                $this->db->where('patient_type', $NewFamily);
                $this->db->update('new_patients_tracker', $data3);
            }

            $new_patient_month = $this->get_new_patient_month();
            $new_patient_year = $this->get_new_patient_year();
            $new_patient_count = $this->get_new_patient_count();

            $number = 1;
            $month = date('F');
            $year = date('Y');
            $new_patient = "New Patient";

            if (empty($new_patient_month) && empty($new_patient_year)) {

                $data1 = array(
                    'count' => $number,
                    'month' => $month,
                    'year' => $year,
                    'patient_type' => $new_patient
                );
                $this->db->insert('new_patients_tracker', $data1);
            } else {
                $new_count = $new_patient_count + $number;
                $data3 = array(
                    'count' => $new_count
                );
                $this->db->where('month', $month);
                $this->db->where('year', $year);
                $this->db->where('patient_type', $new_patient);
                $this->db->update('new_patients_tracker', $data3);
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function get_new_visit_week() {
        $month = date('F');
        $year = date('Y');

        $sql = "SELECT distinct count from patient_visit_tracker where patient_visit_tracker.month='$month' AND patient_visit_tracker.year='$year'  LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {
            // echo $row->year;
            return $row->week;
        }
    }

    public function get_new_visit_count() {
        $month = date('F');
        $year = date('Y');
        $sql = "SELECT distinct count from patient_visit_tracker where patient_visit_tracker.month='$month' AND patient_visit_tracker.year='$year' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->count;
        }
    }

    public function get_new_visit_year() {
        $month = date('F');
        $year = date('Y');
        $sql = "SELECT distinct year from patient_visit_tracker where patient_visit_tracker.month='$month' AND patient_visit_tracker.year='$year' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->year;
        }
    }

    public function get_new_visit_month() {
        $month = date('F');
        $year = date('Y');
        $sql = "SELECT distinct month from patient_visit_tracker where patient_visit_tracker.month='$month' AND patient_visit_tracker.year='$year' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->month;
        }
    }

    public function check_member() {
        $member_id = $this->session->userdata('member_id');
        $month = date('F');
        $year = date('Y');
        $sql = "SELECT member_id from patient_visit_tracker WHERE member_id='$member_id' AND year='$year' AND month='$month' LIMIT 1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $value) {
            return $value->member_id;
        }
    }

    public function check_branch() {
        $branch_id = $this->session->userdata('branch_id');
        $month = date('F');
        $year = date('Y');
        $sql = "SELECT branch_id from patient_visit_tracker WHERE branch_id='$branch_id' AND year='$year' AND month='$month' LIMIT 1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $value) {
            return $value->branch_id;
        }
    }

    public function to_package() {
        $this->db->trans_start();
        $patient_id = $this->input->post('visit_id', TRUE);
        $patient_id = mysql_real_escape_string($patient_id);

        $pay_at_the_end = $this->input->post('pay_at_the_end', TRUE);
        $pay_at_the_end = mysql_real_escape_string($pay_at_the_end);

        $packageid = $this->input->post('package_id', TRUE);
        $packageid = mysql_real_escape_string($packageid);


        $paid = $this->input->post('paid', TRUE);
        $paid = mysql_real_escape_string($paid);
        $total = $this->input->post('total', TRUE);
        $total = mysql_real_escape_string($total);
        $visit_type = $this->input->post('visit_type', TRUE);
        $visit_type = mysql_real_escape_string($visit_type);
        $previous_visit_date = $this->input->post('previous_visit_date', TRUE);
        $previous_visit_date = mysql_real_escape_string($previous_visit_date);
        $queue_to_join = $this->input->post('queue_to_join');
        $queue_to_join = mysql_real_escape_string($queue_to_join);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $user_id = $this->session->userdata('id');
        $date_from = $this->input->post('date_from');
        $date_to = $this->input->post('date_to');
        $charge_follow_up = $this->input->post('charge_follow_up');



        $today = date("H:i:s");
        $start = 0;
        $length = 7;

        $check_visit_type = substr($visit_type, $start, $length);
        if ($check_visit_type === "walk_in") {
            echo 'WALKIN PICKED....';
            $sql = "Select * from patient where patient_id='$patient_id'";
            $query = $this->db->query($sql);
            foreach ($query->result() as $value) {
                $family_member_number = $value->family_number;
                $this->db->set('branch_id', $branch_id);
                $this->db->set('member_id', $member_id);
                $this->db->set('visit_start', $date_from);
                $this->db->set('visit_end', $date_to);
                $this->db->set('patient_id', $patient_id);
                $this->db->set('user_id', $user_id);
                if ($visit_type === "walk_in_nurse") {
                    $this->db->set('nurse_queue', 'Active');
                    $this->db->set('lab_queue', 'In Active');
                    $this->db->set('doctor_queue', 'In Active');
                    $this->db->set('nurse_start', $today);
                } elseif ($visit_type === "walk_in_lab") {
                    $this->db->set('nurse_queue', 'In Active');
                    $this->db->set('lab_queue', 'Active');
                    $this->db->set('doctor_queue', 'In Active');
                    $this->db->set('nurse_start', $today);
                } elseif ($visit_type === "walk_in_pharmacy") {
                    $this->db->set('nurse_queue', 'In Active');
                    $this->db->set('lab_queue', 'In Active');
                    $this->db->set('doctor_queue', 'Active');
                    $this->db->set('nurse_start', $today);
                }
                $this->db->insert('walkin_visit');
            }




            $patient_sql = "SELECT * from patient WHERE patient.patient_id='$patient_id' LIMIT 0,1";
            $patient_sql = $this->db->query($patient_sql);
            foreach ($patient_sql->result() as $row) {
                $this->db->set('package_type', $packageid);
                $family_member_number = $row->family_number;
                $this->db->set('nurse_queue', 'Active');
                $this->db->set('doctor_queue', 'In Active');
                $this->db->set('lab_queue', 'In Active');
                $this->db->set('pharm_queue', 'In Active');
                $this->db->set('rad_queue', 'In Active');


                if ($queue_to_join === 'Nurse') {
                    $this->db->set('nurse_queue', 'Active');
                    $this->db->set('doctor_queue', 'in-acitve');
                    $this->db->set('lab_queue', 'In Active');
                    $this->db->set('pharm_queue', 'In Active');
                    $this->db->set('rad_queue', 'In Active');
                    $this->db->set('nurse_start', $today);
                } elseif ($queue_to_join === 'Support') {
                    $this->db->set('nurse_queue', 'Active');
                    $this->db->set('doctor_queue', 'in-acitve');
                    $this->db->set('lab_queue', 'In Active');
                    $this->db->set('pharm_queue', 'In Active');
                    $this->db->set('rad_queue', 'In Active');
                } elseif ($queue_to_join === 'Doctor') {
                    $this->db->set('nurse_queue', 'In Active');
                    $this->db->set('doctor_queue', 'acitve');
                    $this->db->set('lab_queue', 'In Active');
                    $this->db->set('pharm_queue', 'In Active');
                    $this->db->set('rad_queue', 'In Active');
                } elseif ($queue_to_join === 'Laboratory') {
                    $this->db->set('nurse_queue', 'In Active');
                    $this->db->set('doctor_queue', 'In Active');
                    $this->db->set('lab_queue', 'Active');
                    $this->db->set('pharm_queue', 'In Active');
                    $this->db->set('rad_queue', 'In Active');
                    $this->db->set('lab_start', $today);
                } elseif ($queue_to_join === 'Pharmacy') {
                    $this->db->set('nurse_queue', 'In Active');
                    $this->db->set('doctor_queue', 'In Active');
                    $this->db->set('lab_queue', 'In Active');
                    $this->db->set('pharm_queue', 'Active');
                    $this->db->set('rad_queue', 'In Active');
                    $this->db->set('pharm_start', $today);
                } else if ($visit_type === "walk_in_nurse") {
                    $this->db->set('nurse_queue', 'Active');
                    $this->db->set('lab_queue', 'In Active');
                    $this->db->set('doctor_queue', 'In Active');
                    $this->db->set('nurse_start', $today);
                    $this->db->set('rad_queue', 'In Active');
                } elseif ($visit_type === "walk_in_lab") {
                    $this->db->set('nurse_queue', 'In Active');
                    $this->db->set('lab_queue', 'Active');
                    $this->db->set('doctor_queue', 'In Active');
                    $this->db->set('lab_start', $today);
                    $this->db->set('rad_queue', 'In Active');
                } elseif ($visit_type === "walk_in_pharmacy") {
                    $this->db->set('nurse_queue', 'In Active');
                    $this->db->set('lab_queue', 'In Active');
                    $this->db->set('doctor_queue', 'In Active');
                    $this->db->set('pharm_start', $today);
                    $this->db->set('rad_queue', 'In Active');
                    $this->db->set('pharm_queue', 'Active');
                }
                $date_visited = date('Y-m-d');
                $this->db->set('start', $date_from);
                $this->db->set('end', $date_to);
                $this->db->set('branch_id', $branch_id);
                $this->db->set('member_id', $member_id);
                $this->db->set('charge_followup', $charge_follow_up);
                if (!empty($pay_at_the_end)) {
                    $this->db->set('pay_at_the_end', $pay_at_the_end);
                } else {
                    $no = "no";
                    $this->db->set('pay_at_the_end', $no);
                }

                $this->db->set('patient_id', $patient_id);
                $this->db->set('nurse_start', $today);
                $this->db->set('family_number', $family_member_number);
                $this->db->set('visit_type', $visit_type);
                $this->db->set('type', 'Walk-in');

                $this->db->insert('visit');
                $visit_id = $this->db->insert_id();





                $new_visit_month = $this->get_new_visit_month();
                $new_visit_year = $this->get_new_visit_year();
                $new_visit_count = $this->get_new_visit_count();


                $check_member = $this->check_member();
                $check_branch = $this->check_branch();

                $number = 1;
                $month = date('F');
                $year = date('Y');
                $ddate = "2012-10-18";
                $duedt = explode("-", $ddate);
                $date = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
                $week = (int) date('W', $date);


                if (empty($new_visit_month) && empty($new_visit_year) && empty($check_member)) {

                    $data1 = array(
                        'count' => $number,
                        'month' => $month,
                        'year' => $year,
                        'week' => $week,
                        'member_id' => $member_id,
                        'branch_id' => $branch_id
                    );
                    $this->db->insert('patient_visit_tracker', $data1);
                    echo 'DID NOT EXIST';
                } else if ($new_visit_month == $month && $new_visit_year == $year && $member_id == $check_member && $check_branch == $branch_id) {
                    echo 'Both Exist';
                    $new_count = $new_visit_count + $number;

                    $data = array(
                        'count' => $new_count
                    );
                    $this->db->where('month', $month);
                    $this->db->where('year', $year);
                    $this->db->where('week', $week);
                    $this->db->where('member_id', $member_id);
                    $this->db->where('branch_id', $branch_id);
                    $this->db->update('patient_visit_tracker', $data);
                    echo 'Both Branch and Member Exists';
                }
            }
        } else {



            $patient_sql = "SELECT * from patient WHERE patient.patient_id='$patient_id' LIMIT 0,1";
            $patient_sql = $this->db->query($patient_sql);
            foreach ($patient_sql->result() as $row) {
                $this->db->set('package_type', $packageid);
                $family_member_number = $row->family_number;
                $this->db->set('nurse_queue', 'Active');
                $this->db->set('doctor_queue', 'In Active');
                $this->db->set('lab_queue', 'In Active');
                $this->db->set('pharm_queue', 'In Active');
                $this->db->set('rad_queue', 'In Active');


                if ($queue_to_join === 'Nurse') {
                    $this->db->set('nurse_queue', 'Active');
                    $this->db->set('doctor_queue', 'in-acitve');
                    $this->db->set('lab_queue', 'In Active');
                    $this->db->set('pharm_queue', 'In Active');
                    $this->db->set('rad_queue', 'In Active');
                } elseif ($queue_to_join === 'Support') {
                    $this->db->set('nurse_queue', 'Active');
                    $this->db->set('doctor_queue', 'in-acitve');
                    $this->db->set('lab_queue', 'In Active');
                    $this->db->set('pharm_queue', 'In Active');
                    $this->db->set('rad_queue', 'In Active');
                } elseif ($queue_to_join === 'Doctor') {
                    $this->db->set('nurse_queue', 'In Active');
                    $this->db->set('doctor_queue', 'acitve');
                    $this->db->set('lab_queue', 'In Active');
                    $this->db->set('pharm_queue', 'In Active');
                    $this->db->set('rad_queue', 'In Active');
                } elseif ($queue_to_join === 'Laboratory') {
                    $this->db->set('nurse_queue', 'In Active');
                    $this->db->set('doctor_queue', 'in-acitve');
                    $this->db->set('lab_queue', 'Active');
                    $this->db->set('pharm_queue', 'In Active');
                    $this->db->set('rad_queue', 'In Active');
                } elseif ($queue_to_join === 'Pharmacy') {
                    $this->db->set('nurse_queue', 'In Active');
                    $this->db->set('doctor_queue', 'in-acitve');
                    $this->db->set('lab_queue', 'In Active');
                    $this->db->set('pharm_queue', 'Active');
                    $this->db->set('rad_queue', 'In Active');
                }
                $date_visited = date('Y-m-d');
                $this->db->set('start', $date_from);
                $this->db->set('end', $date_to);
                $this->db->set('branch_id', $branch_id);
                $this->db->set('member_id', $member_id);
                $this->db->set('charge_followup', $charge_follow_up);
                if (!empty($pay_at_the_end)) {
                    $this->db->set('pay_at_the_end', $pay_at_the_end);
                } else {
                    $no = "no";
                    $this->db->set('pay_at_the_end', $no);
                }

                $this->db->set('patient_id', $patient_id);
                $this->db->set('nurse_start', $today);
                $this->db->set('family_number', $family_member_number);
                $this->db->set('visit_type', $visit_type);
                $this->db->set('type', 'Regular');
                $this->db->insert('visit');
                $visit_id = $this->db->insert_id();



                $department = $this->session->userdata('type');
                $user_id = $this->session->userdata('id');

                $follow_up_array = array('follow_up_nurse', 'follow_up_lab', 'follow_up_doctor');

                if (in_array($visit_type, $follow_up_array)) {
                    if (empty($charge_follow_up)) {
                        
                    } else {
                        $sql = "SELECT * from packages WHERE package_id = '$packageid' LIMIT 0,1";
                        $sql = $this->db->query($sql);
                        foreach ($sql->result() as $row) {


                            $package_name = $row->package_name;
                            $cost = $row->cost;
                            $revenue_type = 'package';
                            $this->db->set('visit_id', $visit_id);
                            $this->db->set('patient_id', $patient_id);
                            $this->db->set('package_id', $packageid);
                            $this->db->set('cost', $cost);
                            $this->db->set('total_payments_dr', $total);
                            $this->db->set('member_id', $member_id);
                            $this->db->set('branch_id', $branch_id);
                            $this->db->insert('patient_payments');
                            $last_insert_id = $this->db->insert_id();

                            $this->db->set('patient_id', $patient_id);
                            $this->db->set('visit_id', $visit_id);
                            $this->db->set('description', $package_name);
                            $this->db->set('amount', $cost);
                            $this->db->set('charged', 'Yes');
                            $this->db->set('patient_payment_id', $last_insert_id);
                            $this->db->set('department', $department);
                            $this->db->set('user_id', $user_id);
                            $this->db->set('revenue_type', $revenue_type);
                            $this->db->set('member_id', $member_id);
                            $this->db->set('branch_id', $branch_id);
                            $this->db->set('paid', 'No');

                            $this->db->insert('patient_visit_statement');
                        }
                    }
                } else {


                    $sql = "SELECT * from packages WHERE package_id = '$packageid' LIMIT 0,1";
                    $sql = $this->db->query($sql);
                    foreach ($sql->result() as $row) {


                        $package_name = $row->package_name;
                        $cost = $row->cost;
                        $revenue_type = 'package';
                        $this->db->set('visit_id', $visit_id);
                        $this->db->set('patient_id', $patient_id);
                        $this->db->set('package_id', $packageid);
                        $this->db->set('cost', $cost);
                        $this->db->set('total_payments_dr', $total);
                        $this->db->set('member_id', $member_id);
                        $this->db->set('branch_id', $branch_id);
                        $this->db->insert('patient_payments');
                        $last_insert_id = $this->db->insert_id();

                        $this->db->set('patient_id', $patient_id);
                        $this->db->set('visit_id', $visit_id);
                        $this->db->set('description', $package_name);
                        $this->db->set('amount', $cost);
                        $this->db->set('charged', 'Yes');
                        $this->db->set('patient_payment_id', $last_insert_id);
                        $this->db->set('department', $department);
                        $this->db->set('user_id', $user_id);
                        $this->db->set('revenue_type', $revenue_type);
                        $this->db->set('member_id', $member_id);
                        $this->db->set('branch_id', $branch_id);
                        $this->db->set('paid', 'No');

                        $this->db->insert('patient_visit_statement');
                    }
                }


                $new_visit_month = $this->get_new_visit_month();
                $new_visit_year = $this->get_new_visit_year();
                $new_visit_count = $this->get_new_visit_count();


                $check_member = $this->check_member();
                $check_branch = $this->check_branch();

                $number = 1;
                $month = date('F');
                $year = date('Y');
                $ddate = "2012-10-18";
                $duedt = explode("-", $ddate);
                $date = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
                $week = (int) date('W', $date);


                if (empty($new_visit_month) && empty($new_visit_year) && empty($check_member)) {

                    $data1 = array(
                        'count' => $number,
                        'month' => $month,
                        'year' => $year,
                        'week' => $week,
                        'member_id' => $member_id,
                        'branch_id' => $branch_id
                    );
                    $this->db->insert('patient_visit_tracker', $data1);
                    echo 'DID NOT EXIST';
                } else if ($new_visit_month == $month && $new_visit_year == $year && $member_id == $check_member && $check_branch == $branch_id) {
                    echo 'Both Exist';
                    $new_count = $new_visit_count + $number;

                    $data = array(
                        'count' => $new_count
                    );
                    $this->db->where('month', $month);
                    $this->db->where('year', $year);
                    $this->db->where('week', $week);
                    $this->db->where('member_id', $member_id);
                    $this->db->where('branch_id', $branch_id);
                    $this->db->update('patient_visit_tracker', $data);
                    echo 'Both Branch and Member Exists';
                }
            }
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            // generate an error... or use the log_message() function to log your error
        }
    }

    public function get_Active_doctor() {
        $sql = "Select DISTINCT employee.title , employee.employee_id, employee.user_name,
                employee.f_name,employee.l_name,employee.other_name, concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name,
                department.department_name from login_logs inner join
                employee on employee.employee_id = login_logs.employee_id 
                inner join department on department.department_id = employee.employment_category where 
                department.department_name='Doctor' AND login_logs.login_date >= CURDATE() and login_logs.is_Active='Active'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function package() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT DISTINCT *
        FROM packages where member_id='$member_id' AND branch_id='$branch_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getQueue() {

        $this->db->distinct();

        $query = $this->db->get('queue_name');

        return $query->result_array();
    }

    public function getQueue_walkin() {
        $name = 'Doctor';
        $this->db->distinct();
        $this->db->where('queue_name !=', $name);
        $query = $this->db->get('queue_name');

        return $query->result_array();
    }

    public function getProcedure() {
        $this->db->distinct();
        $query = $this->db->get('procedure_list');

        return $query->result_array();
    }

    public function add_procedure() {
        $patientname = $this->input->post('patientname');
        $patient_phone = $this->input->post('patient_phone');
        $procedure_name = $this->input->post('procedure_name');
        $patient_type = 'walkin';
        $this->db->trans_start();
        $this->db->set('procedure_list_id', $procedure_name);
        $this->db->set('patient_name', $patientname);
        $this->db->set('patient_phone', $patient_phone);
        $this->db->set('patient_type', $patient_type);
        $this->db->insert('procedure_visit');
        $procedure_visit_id = $this->db->insert_id();

        if (empty($patient_phone)) {


            $query = $this->db->get_where('patient', array('patient_id' => $patientname), 1, 0);
            foreach ($query->result() as $value) {
                $phone_no = $value->phone_no;
                $patient_type = 'Shwari';
                $data_update = array(
                    'patient_phone' => $phone_no,
                    'patient_type' => $patient_type
                );
                $this->db->where('procedure_visit_id', $procedure_visit_id);
                $this->db->update('procedure_visit', $data_update);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function add_patient_procedure() {
        $patient_name = $this->input->post('patientname');
        $phone_no = $this->input->post('patient_phone');
        $procedure_name = $this->input->post('procedure_name');
        $patient_type = 'walkin';
        $this->db->trans_start();
        $data = array(
            'patient_name' => $patient_name,
            'patient_phone' => $phone_no,
            'procedure_list_id' => $procedure_name,
            'patient_type' => $patient_type
        );

        $this->db->insert('procedure_visit', $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function get_procedures($term) {
        $this->db->like('procedure_name', $term, 'after');
        $query = $this->db->get('procedure_list');
        return $query->result();
    }

    public function getName() {
        $this->db->distinct();
        $query = $this->db->get('patient');

        return $query->result_array();
    }

    public function see_appointments() {

        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $query = "SELECT patient.f_name, appointment.patient_id, patient.other_name, patient.s_name,
         appointment.visit_id, appointment.date, appointment.time_start,appointment.time_end,
        appointment.about, appointment.title, appointment.booked_by, patient.phone_no, CONCAT(employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name,employee.phone_no as employee_phone ,department.department_name
        FROM appointment
        INNER JOIN patient
        ON appointment.patient_id = patient.patient_id
        INNER JOIN employee
        ON employee.employee_id = appointment.employee_id
                INNER JOIN department
                ON department.department_id = employee.employment_category
        WHERE appointment.Date>=CURDATE() AND appointment.member_id='$member_id' AND appointment.branch_id='$branch_id'
        ORDER BY appointment.time_start";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function gettest($id) {

        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT payments.cost, patients.fname, patients.sname, patients.lname,
            payments.id, payments.visitid
            FROM payments
            INNER JOIN visit ON visit.id=payments.visitid
            INNER JOIN patients ON patients.id=visit.patientid
            WHERE payments.paid=1 AND payments.member_id='$member_id' AND payments.branch_id='$branch_id'
            AND payments.visitid='" . $id . "'";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function topaytest() {

        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT payments.visitid, payments.cost, payments.faculty,patients.fname,
            patients.sname, patients.lname, visit.patientid, visit.VisitDate, payments.id
            FROM patient_payments
            INNER JOIN visit ON visit.id=payments.visitid
            INNER JOIN patients ON patients.id=visit.patientid
            WHERE DATE(visit.start) = DATE(NOW()) AND patient_payments.member_id='$member_id' AND patient_payments.branch_id='$branch_id'
            AND payments.paid=1";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function topayphar() {

        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT patient_payments.pharm_payments,patient.f_name, patient_payments.visit_id,
            patient.s_name, patient.other_name, visit.patient_id, visit.Visit_Date, patient_payments.patient_payment_id
            FROM patient_payments
            INNER JOIN visit ON visit.visit_id=patient_payments.visit_id
            INNER JOIN patient ON patient.patient_id=visit.patient_id
            WHERE DATE(visit.start) = DATE(NOW()) AND patient_payments.member_id='$member_id' AND patient_payments.branch_id='$branch_id'
            AND patient_payments.paid='not paid'";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function getmeds($id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT pharm_payments.cost, patients.fname, patients.sname, patients.lname,
            pharm_payments.id, pharm_payments.visitid
            FROM pharm_payments
            INNER JOIN visit ON visit.id=pharm_payments.visitid
            INNER JOIN patients ON patients.id=visit.patientid
            WHERE pharm_payments.paid=1 AND pharm_payments.member_id='$member_id' AND pharm_payments.branch_id='$branch_id'
            AND pharm_payments.visitid='" . $id . "'";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function ToLab($id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $today = date("H:i:s");
        $data = array(
            'lq' => 'Active',
            'lstart' => $today
        );
        $this->db->where('member_id', $member_id);
        $this->db->where('branch_id', $branch_id);
        $this->db->where('id', $id);
        $this->db->update('visit', $data);
    }

    public function pharmcost() {
        $repid = $this->session->userdata('id');

        $mpesacode = $this->input->post('mpesa');
        $mpesacode = mysql_real_escape_string($mpesacode);

        $id = $this->input->post('id');
        $id = mysql_real_escape_string($id);

        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $data = array(
            'mpesa' => $mpesacode,
            'paid' => "0"
        );

        $this->db->where('id', $id);
        $this->db->where('member_id', $member_id);
        $this->db->where('branch_id', $branch_id);
        $this->db->update('pharm_payments', $data);
    }

    public function labcost() {


        $mpesacode = $this->input->post('MpesaCode');
        $mpesacode = mysql_real_escape_string($mpesacode);

        $visitid = $this->input->post('visitid');
        $visitid = mysql_real_escape_string($visitid);

        $cpay = $this->input->post('cpay');
        $cpay = mysql_real_escape_string($cpay);

        $cash = $this->input->post('cash');
        $cash = mysql_real_escape_string($cash);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $this->db->set('MpesaCode', $mpesacode);
        $this->db->set('visitid', $visitid);
        $this->db->set('cpay', $cpay);
        $this->db->set('cash', $cash);
        $this->db->set('member_id', $member_id);
        $this->db->set('branch_id', $branch_id);

        $this->db->insert('patient_payments');

        $data = array(
            'paid' => 'paid'
        );
        $this->db->where('visitid', $visitid);
        $this->db->where('member_id', $member_id);
        $this->db->where('branch_id', $branch_id);
        $this->db->update('costs', $data);
    }

    public function walkin() {
        $patientname = $this->input->post('patientname');
        $patientname = mysql_real_escape_string($patientname);

        $patient_phone = $this->input->post('patient_phone');
        $patient_phone = mysql_real_escape_string($patient_phone);

        $department = $this->input->post('department');
        $department = mysql_real_escape_string($department);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $this->db->set('walkin_patient_name', $patientname);
        $this->db->set('walkin_phone_no', $patient_phone);
        $this->db->set('walkin_department', $department);
        $this->db->set('member_id', $member_id);
        $this->db->set('branch_id', $branch_id);

        $this->db->insert('walkin');
    }

    public function topaywalkin() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT walkin.walkin_patient_name, walkin_payments.faculty,walkin_payments.walkin_payments_total 
                FROM walkin_payments INNER JOIN walkin ON walkin.walkin_id = walkin_payments.patient_id
        WHERE walkin.walkin_date >=CURDATE()
        AND walkin.paid='not paid' AND walkin.member_id='$member_id' AND walkin.branch_id='$branch_id'
        AND walkin_payments.walkin_payments_paid='no'";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function pay_data($id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT walkin_payments.total,walkin_payments.id,
        walkin.patientname, walkin.Date, walkin_payments.patientid
        FROM walkin_payments
        INNER JOIN walkin ON walkin.id=walkin_payments.patientid
        WHERE walkin.Date >=CURDATE()
        AND walkin_payments.patientid='" . $id . "' AND walkin_payments.member_id='$member_id' AND walkin_payments.branch_id='$branch_id'
        AND walkin_payments.paymentid=1";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function Active_paitentdoctors() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT DISTINCT * FROM visit INNER JOIN patient ON patient.patient_id = visit.patient_id  where visit.doctor_queue='Active' AND visit.member_id='$member_id' AND visit.branch_id='$branch_id' AND  DATE(visit.start) = DATE(NOW())";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function walkinpay() {
        $repid = $this->session->userdata('id');

        $mpesa = $this->input->post('cpay');
        $mpesa = mysql_real_escape_string($mpesa);

        $mpesacode = $this->input->post('mpesa');
        $mpesacode = mysql_real_escape_string($mpesacode);

        $cash = $this->input->post('cash');
        $cash = mysql_real_escape_string($cash);

        $id = $this->input->post('id');
        $id = mysql_real_escape_string($id);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $data = array(
            'cpay' => $mpesa,
            'mpesa' => $mpesacode,
            'cash' => $cash,
            'paymentid' => "0",
        );

        $this->db->where('id', $id);
        $this->db->where('member_id', $member_id);
        $this->db->where('branch_id', $branch_id);
        $this->db->update('walkin_payments', $data);
    }

    public function patient_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT patient_payments.cost AS package, patient_payments.lab_payments AS labs, patient.f_name,patient.other_name,patient.s_name,visit.visit_id AS visitid,
            patient.patient_id, patient_payments.pharm_payments AS pharm_costs ,patient_payments.cost+patient_payments.pharm_payments+patient_payments.lab_payments as total_cost
            from patient_payments
            LEFT JOIN visit ON patient_payments.visit_id=visit.visit_id
            LEFT JOIN patient ON visit.patient_id=patient.patient_id
            WHERE patient_payments.paid='not paid'  AND patient_payments.member_id='$member_id' AND patient_payments.branch_id='$branch_id'
            AND DATE(visit.start) = DATE(NOW())";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function pat_payments($visitid) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT costs.cost AS package, payments.cost AS labs, patients.fname,patients.lname,patients.sname,visit.id AS visitid,
            patients.id,pharm_payments.cost AS pharm_costs 
            from patients
            LEFT JOIN visit ON patients.id=visit.patientid
            LEFT JOIN costs ON costs.visitid=visit.id
            LEFT JOIN pharm_payments ON pharm_payments.visitid=visit.id
            LEFT JOIN payments ON payments.visitid=visit.id
            WHERE costs.paid='not paid' AND costs.member_id='$member_id' AND costs.branch_id='$branch_id'
            AND visit.id='" . $visitid . "'";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function pay_demo() {
        $visit_id = $this->input->post('visitid', TRUE);
        $visit_id = mysql_real_escape_string($visit_id);

        $payment_option = $this->input->post('pay', TRUE);
        $payment_option = mysql_real_escape_string($payment_option);


        $cost = $this->input->post('cost', TRUE);
        $cost = mysql_real_escape_string($cost);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $this->db->set('visitid', $visit_id);
        $this->db->set('payment_option', $payment_option);
        $this->db->set('total', $cost);
        $this->db->set('member_id', $member_id);
        $this->db->set('branch_id', $branch_id);

        $this->db->insert('payment_demo');
        $data = array(
            'paid' => 'paid'
        );
        $this->db->where('visitid', $visit_id);
        $this->db->update('costs', $data);
    }

}
