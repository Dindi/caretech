<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */

class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Function to get the client ip address
    public function get_client_ip() {
        $ipaddress = '';
        if ($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    public function validate() {
        $this->db->trans_start();
        // grab user input
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));
        $Enc_password = md5($password);
        // Prep the query
        $this->db->where('user_name', $username);
        $this->db->where('password', $Enc_password);
        $this->db->where('is_active', "Yes");
        $this->db->join('department', 'department.department_id = employee.employment_category');
        // Run the query
        $query = $this->db->get('employee');



        if ($query->num_rows() == 1) {

            $login_ip_address = $this->get_client_ip();
            $this->load->library('user_agent');

            if ($this->agent->is_browser()) {
                $agent = $this->agent->browser() . ' ' . $this->agent->version();
            } elseif ($this->agent->is_robot()) {
                $agent = $this->agent->robot();
            } elseif ($this->agent->is_mobile()) {
                $agent = $this->agent->mobile();
            } else {
                $agent = 'Unidentified User Agent';
            }


            $paltform = $this->agent->platform();

            $login_ip_address_details = 'Platform : ' . $paltform . ' / Agent : ' . $agent . ' / Login IP address : ' . $login_ip_address;
            $month = date('F');
            $year = date('Y');
            $row = $query->row();
            $is_active = "Active";
            $member_id = $this->session->userdata('member_id');
            $branch_id = $this->session->userdata('branch_id');
            $time_start = date("H:i:s");
            //Grab the  information and dump into log-in logs table for tracking user activities
            $this->db->set('employee_id', $row->employee_id);
            $this->db->set('user_name', $row->user_name);
            $this->db->set('login_ip_address', $login_ip_address_details);
            $this->db->set('month', $month);
            $this->db->set('year', $year);
            $this->db->set('member_id', $member_id);
            $this->db->set('branch_id', $branch_id);
            $this->db->set('time_start', $time_start);
            $this->db->insert('login_logs');
            $insert_id = $this->db->insert_id();
            $last_inserted_id = $this->db->insert_id();
            //create user session
            $data = array(
                'id' => $row->employee_id,
                'title'=>$row->title,
                'Fname' => $row->f_name,
                'Lname' => $row->l_name,
                'other_name' => $row->other_name,
                'id_no' => $row->id_no,
                'dob' => $row->dob,
                'gender' => $row->gender,
                'marital_status' => $row->marital_status,
                'phone_no' => $row->phone_no,
                'email' => $row->email,
                'residence' => $row->residence,
                'next_of_kin_fname' => $row->next_of_kin_fname,
                'next_of_kin_lname' => $row->next_of_kin_lname,
                'next_of_kin_relation' => $row->next_of_kin_relation,
                'next_of_kin_phone_no' => $row->next_of_kin_phone_no,
                'dept' => $row->dept,
                'username' => $row->user_name,
                'type' => $row->department_name,
                'employment_category' => $row->employment_category,
                'is_global' => $row->is_global,
                'login_logs_id' => $insert_id,
                'department' => $row->department_name,
                'member_id' => $row->member_id,
                'branch_id' => $row->branch_id,
                'last_inserted_id' => $last_inserted_id,
                'validated' => true
            );
            $this->session->set_userdata($data);
            //update the  user branch and member id in the Login logs table
            $member_id = $this->session->userdata('member_id');
            $branch_id = $this->session->userdata('branch_id');
            $data_update_login_logs = array(
                'member_id' => $member_id,
                'branch_id' => $branch_id
            );
            $this->db->where('login_logs_id', $last_inserted_id);
            $this->db->update('login_logs', $data_update_login_logs);

            $member_month_check = $this->member_get_login_logs_month();

            $member_year_check = $this->member_get_login_logs_year();

            $member_member_check = $this->member_get_login_logs_member();

            $member_count_check = $this->member_get_login_logs_count();


            $month_check = $this->get_login_logs_month();

            $year_check = $this->get_login_logs_year();



            $count_check = $this->get_login_logs_count();

            if (empty($month_check) && empty($year_check)) {

                //check the  member login logs periodic track statistics
                if (empty($member_month_check) && empty($member_year_check)) {
                    if (empty($member_member_check)) {


                        $member_id = $this->session->userdata('member_id');
                        $branch_id = $this->session->userdata('branch_id');
                        $number = 1;
                        $this->db->set('month', $month);
                        $this->db->set('year', $year);
                        $this->db->set('count', $number);
                        $this->db->set('member_id', $member_id);
                        $this->db->set('branch_id', $branch_id);
                        $this->db->insert('member_log_in_logs_periodic_track');
                    }
                } else {
                    $number = 1;
                    $total_count = $number + $member_count_check;
                    $data = array(
                        'count' => $total_count
                    );

                    $this->db->where('month', $month);
                    $this->db->where('year', $year);
                    $this->db->where('member_id', $member_id);
                    $this->db->update('member_log_in_logs_periodic_track', $data);
                }

                $member_id = $this->session->userdata('member_id');
                $branch_id = $this->session->userdata('branch_id');
                $number = 1;
                $this->db->set('month', $month);
                $this->db->set('year', $year);
                $this->db->set('count', $number);
                $this->db->set('member_id', $member_id);
                $this->db->set('branch_id', $branch_id);
                $this->db->insert('login_logs_periodic_track');
            } else {


                if (empty($member_month_check) && empty($member_year_check) && empty($member_member_check)) {
                    $member_id = $this->session->userdata('member_id');
                    $branch_id = $this->session->userdata('branch_id');
                    $number = 1;
                    $this->db->set('month', $month);
                    $this->db->set('year', $year);
                    $this->db->set('count', $number);
                    $this->db->set('member_id', $member_id);
                    $this->db->set('branch_id', $branch_id);
                    $this->db->insert('member_log_in_logs_periodic_track');
                } else {
                    $number = 1;
                    $total_count = $number + $count_check;
                    $data = array(
                        'count' => $total_count
                    );

                    $this->db->where('month', $month);
                    $this->db->where('year', $year);
                    $this->db->where('member_id', $member_id);
                    $this->db->update('member_log_in_logs_periodic_track', $data);
                }
                $number = 1;
                $total_count = $number + $count_check;
                $data = array(
                    'count' => $total_count
                );

                $this->db->where('month', $month);
                $this->db->where('year', $year);

                $this->db->update('login_logs_periodic_track', $data);
            }


            $time_end = date("H:i:s");
            $update_logout = "SELECT login_logs_id, is_active FROM `login_logs` where login_logs_id < '$last_inserted_id' and user_name='$username' and is_active = 'Active'";
            $query = $this->db->query($update_logout);
            foreach ($query->result() as $value) {
                $login_logs_id = $value->login_logs_id;
                $is_active = $value->is_active;
                $in_active = "In Active";
                $login_logs_update = array(
                    'is_active' => $in_active,
                    'time_end' => $time_end
                );
                $this->db->where('login_logs_id', $login_logs_id);
                $this->db->update('login_logs', $login_logs_update);
            }


            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            }

            return true;
        }
        // If the previous process did not validate
        // then return false.
        return false;
    }

    public function member_get_login_logs_month() {
        $month = date('F');
        $year = date('Y');
        $member_id = $this->session->userdata('member_id');
        $sql = "SELECT distinct year from member_log_in_logs_periodic_track where member_log_in_logs_periodic_track.month='$month' AND member_log_in_logs_periodic_track.year='$year' AND member_log_in_logs_periodic_track.member_id='$member_id' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->year;
        }
    }

    public function member_get_login_logs_year() {
        $month = date('F');
        $year = date('Y');
        $member_id = $this->session->userdata('member_id');
        $sql = "SELECT distinct month from member_log_in_logs_periodic_track where member_log_in_logs_periodic_track.month='$month' AND member_log_in_logs_periodic_track.year='$year' AND member_log_in_logs_periodic_track.member_id='$member_id' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->month;
        }
    }

    public function member_get_login_logs_member() {
        $month = date('F');
        $year = date('Y');
        $member_id = $this->session->userdata('member_id');
        $sql = "SELECT distinct member_id from member_log_in_logs_periodic_track where member_log_in_logs_periodic_track.month='$month' AND member_log_in_logs_periodic_track.year='$year' AND member_log_in_logs_periodic_track.year='$member_id'  LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->member_id;
        }
    }

    public function member_get_login_logs_count() {
        $month = date('F');
        $year = date('Y');
        $memmber_id = $this->session->userdata('member_id');
        $sql = "SELECT distinct count from member_log_in_logs_periodic_track where member_log_in_logs_periodic_track.month='$month' AND member_log_in_logs_periodic_track.year='$year' AND member_log_in_logs_periodic_track.member_id='$memmber_id' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->count;
        }
    }

    public function get_login_logs_month() {
        $month = date('F');
        $year = date('Y');
        $member_id = $this->session->userdata('member_id');
        $sql = "SELECT distinct year from login_logs_periodic_track where login_logs_periodic_track.month='$month' AND login_logs_periodic_track.year='$year'  LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->year;
        }
    }

    public function get_login_logs_year() {
        $month = date('F');
        $year = date('Y');

        $sql = "SELECT distinct month from login_logs_periodic_track where login_logs_periodic_track.month='$month' AND login_logs_periodic_track.year='$year'  LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->month;
        }
    }

    public function get_login_logs_count() {
        $month = date('F');
        $year = date('Y');
        $sql = "SELECT distinct count from login_logs_periodic_track where login_logs_periodic_track.month='$month' AND login_logs_periodic_track.year='$year' LIMIT 0,1";
        $result = $this->db->query($sql);
        foreach ($result->result() as $row) {

            return $row->count;
        }
    }

}

?>