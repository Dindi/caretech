<?php

class Nurse_model extends CI_Model {

    public function _construct() {
        parent::_construct();
    }

    public function patient_triage_report($patient_id) {
        $sql = "SELECT DISTINCT concat(employee.title,' ' ,employee.f_name,' ', employee.l_name) as employee_name, triage.triage_id,triage.nurse_id, triage.visit_id, visit.visit_date, visit.patient_id,triage.weight, triage.height, 
			triage.OCS, triage.diastolic, triage.systolic,triage.temperature,triage.respiratory_rate, triage.pulse_rate
            FROM triage
            INNER JOIN visit
            ON triage.visit_id=visit.visit_id
            INNER JOIN employee
            ON triage.nurse_id = employee.employee_id
            WHERE visit.patient_id = '" . $patient_id . "'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function nurse_patient_list() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT  DISTINCT patient.title ,visit.urgency,patient.f_name,visit.patient_id, patient.other_name, patient.s_name,"
                . " visit.visit_date,charge_followup,visit.start, visit.visit_id,visit.urgency, visit.results"
                . " FROM patient INNER JOIN visit ON patient.patient_id=visit.patient_id"
                . " INNER JOIN patient_visit_statement ON patient_visit_statement.visit_id=visit.visit_id "
                . " WHERE DATE(visit.start) = DATE(NOW()) AND visit.nurse_queue = 'active'"
                . " AND patient_visit_statement.amount - patient_visit_statement.amount_dr = 0"
                . " and visit.member_id='$member_id' and visit.branch_id='$branch_id' and type='Regular' group by visit.visit_id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function nurse_patient_list_express() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT  DISTINCT patient.title ,visit.urgency,patient.f_name,visit.patient_id, patient.other_name, patient.s_name,"
                . " visit.visit_date,charge_followup, visit.visit_id,visit.urgency,visit.start, visit.results"
                . " FROM patient INNER JOIN visit ON patient.patient_id=visit.patient_id"
                . " INNER JOIN patient_visit_statement ON patient_visit_statement.visit_id=visit.visit_id "
                . " WHERE DATE(visit.start) = DATE(NOW()) AND visit.nurse_queue = 'active'"
                . "and visit.pay_at_the_end='Yes' "
                . " and visit.member_id='$member_id' and visit.branch_id='$branch_id' group by visit.visit_id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function walkin_patient_list() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "select patient.patient_id, concat(title,' ',f_name,' ',' ',s_name,' ',other_name) as patient_name "
                . ", start , visit_end , nurse_queue, walkin_visit.member_id , walkin_visit.branch_id"
                . " from walkin_visit inner join patient on patient.patient_id = walkin_visit.patient_id where walkin_visit.member_id='$member_id' and nurse_queue='Active' and walkin_visit.branch_id='$branch_id' and DATE(walkin_visit.start) = DATE(NOW())";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function nurse_patient_list_xpress() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT  DISTINCT patient.f_name,visit.patient_id, patient.other_name, patient.s_name, visit.visit_date,"
                . " visit.visit_id,visit.urgency,charge_followup,visit.start, visit.results FROM patient INNER JOIN visit ON patient.patient_id=visit.patient_id"
                . " INNER JOIN patient_visit_statement ON patient_visit_statement.visit_id=visit.visit_id "
                . "WHERE DATE(visit.start) = DATE(NOW()) AND visit.nurse_queue = 'active' AND visit.pay_at_the_end='yes' group by visit.visit_id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function patient_procedure_list() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function appointment_list() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function total_regular_patients_in_tray() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT count(visit.visit_id) as total_regular_patients_in_tray FROM visit"
                . " inner join patient_visit_statement on patient_visit_statement.visit_id = visit.visit_id"
                . " where nurse_queue='active' and visit.member_id='1' and visit.branch_id='1' and DATE(visit.start) = DATE(NOW()) and (patient_visit_statement.amount-patient_visit_statement.amount_dr)  = 0";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function total_walkin_patients_in_tray() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $type = $this->session->userdata('type');
        $sql = "Select count(walkin_id) as total_walkin_patients_in_tray from walkin where walkin_department='$type' and member_id='$member_id' and branch_id='$branch_id' and walkin_date>= curdate() ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function total_appointments_to_date() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $type = $this->session->userdata('type');
        $sql = "select count(appointment_id) as total_appointments_to_date from appointment where queue_booked='$type' and member_id='$member_id' and branch_id='$branch_id' and date >= curdate()";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function add_triage() {
        $this->db->trans_start();
        $visit_id = $this->input->post('add_triage_visit_id', TRUE);
        $patient_id = $this->input->post('add_triage_patient_id', TRUE);
        $weight = $this->input->post('weight', TRUE);
        $systolic = $this->input->post('systolic', TRUE);
        $diastolic = $this->input->post('diastolic', TRUE);
        $temperature = $this->input->post('temperature', TRUE);
        $height = $this->input->post('height', TRUE);
        $respiratory = $this->input->post('respiratory', TRUE);
        $blood_sugar = $this->input->post('blood_sugar', TRUE);
        $lmp = $this->input->post('LMP', TRUE);
        $general_complaints = $this->input->post('general_complaints', TRUE);
        $allergy = $this->input->post('allergy', TRUE);
        $urgent = $this->input->post('urgency', TRUE);
        $pulse_rate = $this->input->post('pulse_rate', TRUE);
        $medication = $this->input->post('medication', TRUE);
        $visit_reason = $this->input->post('visit_reason', TRUE);
        $user_id = $this->session->userdata('id');

        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $this->db->set('visit_id', $visit_id);
        $this->db->set('nurse_id', $user_id);
        $this->db->set('weight', $weight);
        $this->db->set('height', $height);
        $this->db->set('diastolic', $diastolic);
        $this->db->set('systolic', $systolic);
        $this->db->set('temperature', $temperature);
        $this->db->set('respiratory_rate', $respiratory);
        $this->db->set('pulse_rate', $pulse_rate);
        $this->db->set('patient_id', $patient_id);
        $this->db->set('lmp', $lmp);
        $this->db->set('blood_sugar', $blood_sugar);
        $this->db->set('OCS', $general_complaints);
        $this->db->set('member_id', $member_id);
        $this->db->set('branch_id', $branch_id);
        $this->db->set('visit_reason', $visit_reason);

        $this->db->insert('triage');

        if (!empty($allergy)) {
            $this->db->set('allergies', $allergy);
            $this->db->set('Medication', $medication);
            $this->db->set('patient_id', $patient_id);
            $this->db->insert('allergy');
        } else {
            $data = array(
                'allergies' => $allergy,
                'Medication' => $medication
            );

            $this->db->where('patient_id', $patient_id);
            $this->db->update('allergy', $data);
        }

        if ($this->db->affected_rows()) {
            $this->urgent($visit_id);
        }



        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            echo 'Triage Update Successfully ...';
        }
    }

    public function urgent($visit_id) {
        $urgent = $this->input->post('urgency', TRUE);

        $data = array(
            'urgency' => $urgent
        );
        $this->db->where('visit_id', $visit_id);
        $this->db->update('visit', $data);
    }

    public function update_patient_traige($visit_id, $patient_id, $weight, $systolic, $diastolic, $temperature, $height, $respiratory, $blood_sugar, $lmp, $urgent, $pulse_rate, $user_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "Select * from triage where visit_id='$visit_id' LIMIT 0,1";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {
            $this->db->trans_start();
            $triage_id = $value->triage_id;
            echo 'Triage ID' . $triage_id . '<br>';
            $update_patient_triage_array = array(
                'weight' => $weight,
                'systolic' => $systolic,
                'diastolic' => $diastolic,
                'temperature' => $temperature,
                'height' => $height,
                'respiratory_rate' => $respiratory,
                'blood_sugar' => $blood_sugar,
                'lmp' => $lmp,
                'pulse_rate' => $pulse_rate,
                'nurse_id' => $user_id,
                'member_id' => $member_id,
                'branch_id' => $branch_id
            );
            $this->db->where('triage_id', $triage_id);
            $this->db->update('triage', $update_patient_triage_array);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function get_triage_details($triage_id) {
        $query = "Select * from triage where triage_id='$triage_id'";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function update_triage() {

        $this->db->trans_start();
        $visit_id = $this->input->post('edit_triage_visit_id', TRUE);
        $patient_id = $this->input->post('edit_triage_patient_id', TRUE);
        $weight = $this->input->post('edit_weight', TRUE);
        $systolic = $this->input->post('edit_systolic', TRUE);
        $diastolic = $this->input->post('edit_diastolic', TRUE);
        $temperature = $this->input->post('edit_temperature', TRUE);
        $height = $this->input->post('edit_height', TRUE);
        $respiratory = $this->input->post('edit_respiratory', TRUE);
        $blood_sugar = $this->input->post('edit_blood_sugar', TRUE);
        $lmp = $this->input->post('edit_LMP', TRUE);
        $general_complaints = $this->input->post('edit_general_complaints', TRUE);
        $allergy = $this->input->post('edit_allergy', TRUE);
        $urgent = $this->input->post('edit_urgency', TRUE);
        $pulse_rate = $this->input->post('edit_pulse_rate', TRUE);
        $triage_id = $this->input->post('edit_triage_id', TRUE);
        $user_id = $this->session->userdata('id');

        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $triage_update = array(
            'weight' => $weight,
            'systolic' => $systolic,
            'diastolic' => $diastolic,
            'temperature' => $temperature,
            'height' => $height,
            'respiratory_rate' => $respiratory,
            'blood_sugar' => $blood_sugar,
            'lmp' => $lmp,
            'OCS' => $general_complaints,
            'pulse_rate' => $pulse_rate,
            'member_id' => $member_id,
            'branch_id' => $branch_id
        );
        $this->db->where('triage_id', $triage_id);
        $this->db->update('triage', $triage_update);



        if (!empty($allergy)) {
            $this->db->set('allergies', $allergy);
            $this->db->set('patient_id', $patient_id);
            $this->db->insert('allergy');
        } else {
            $data = array(
                'allergies' => $allergy
            );

            $this->db->where('patient_id', $patient_id);
            $this->db->update('allergy', $data);
        }

        if ($this->db->affected_rows()) {
            $this->urgent($visit_id);
        }



        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            echo 'Triage Update Successfully ...';
        }
    }

    public function delete_triage($triage_id) {
        $in_active = "In Active";
        $delete_triage = array(
            'status' => $in_active
        );
        $this->db->where('triage_id', $triage_id);
        $this->db->update('triage', $delete_triage);
    }

    public function active_doctors_list() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT distinct concat(employee.title,': ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name"
                . " , employee.employee_id FROM employee inner join login_logs on login_logs.employee_id = employee.employee_id"
                . " inner join department on department.department_id = employee.employment_category"
                . " where login_logs.is_active = 'Active' and department.department_name='Doctor'"
                . " and login_logs.member_id='$member_id' and login_logs.branch_id='$branch_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function update_reason_for_visit($patient_id, $visit_id, $reason_for_visit, $medication, $emergency) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $limit = 0;
        $offset = 1;
        // $query = $this->db->get_where('triage', array('visit_id' => $visit_id), $limit, $offset);
        $sql = "Select * from triage where visit_id='$visit_id' LIMIT 0,1";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {
            $this->db->trans_start();
            $triage_id = $value->triage_id;
            echo 'Triage ID' . $triage_id . '<br/>';
            $update_triage = array(
                'visit_reason' => $reason_for_visit,
                'OCS' => $reason_for_visit,
                'medication' => $medication,
                'urgency' => $emergency,
                'member_id' => $member_id,
                'branch_id' => $branch_id,
                'patient_id' => $patient_id
            );
            $this->db->where('triage_id', $triage_id);
            $this->db->update('triage', $update_triage);
            $urgent = $this->urgent($visit_id);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function add_reason_for_visit($patient_id, $visit_id, $reason_for_visit, $medication, $emergency) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $this->db->trans_start();
        $add_reason_for_visit = array(
            'visit_id' => $visit_id,
            'patient_id' => $patient_id,
            'visit_reason' => $reason_for_visit,
            'OCS' => $reason_for_visit,
            'member_id' => $member_id,
            'branch_id' => $branch_id,
            'medication' => $medication,
            'urgency' => $emergency
        );

        $this->db->insert('triage', $add_reason_for_visit);
        $urgent = $this->urgent($visit_id);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

   

    public function update_procedure_notes($id, $procedure_notes) {
        $sql_1 = "Select id from procedure_result where id='$id'";
        $query = $this->db->query($sql_1);
        foreach ($query->result() as $value) {
            $id = $value->id;
            $update_procedure_result = array(
                'results' => $procedure_notes
            );
            $this->db->where('id', $id);
            $this->db->update('procedure_result', $update_procedure_result);

            $this->db->trans_start();


            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function add_patient_allergy($visit_id, $patient_id, $allergy) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $user_id = $this->session->userdata('id');
        $this->db->trans_start();
        $add_allergy = array(
            'patient_id' => $patient_id,
            'allergies' => $allergy,
            'branch_id' => $branch_id,
            'member_id' => $member_id,
            'user_id' => $user_id);
        $this->db->insert('allergy', $add_allergy);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function update_patient_allergy($visit_id, $patient_id, $allergy) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "Select * from allergy where patient_id='$patient_id'";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {
            $this->db->trans_start();
            $allergy_id = $value->allergy_id;
            echo 'Allergy id' . $allergy_id . '<br>';
            $update_allergy = array(
                'allergies' => $allergy,
                'member_id' => $member_id,
                'branch_id' => $branch_id
            );
            $this->db->where('allergy_id', $allergy_id);
            $this->db->update('allergy', $update_allergy);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function add_patient_immunization($visit_id, $patient_id, $immunization) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $this->db->trans_start();
        $add_allergy = array(
            'patient_id' => $patient_id,
            'immunization' => $immunization,
            'branch_id' => $branch_id,
            'member_id' => $member_id);
        $this->db->insert('allergy', $add_allergy);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function update_patient_immunization($visit_id, $patient_id, $family_social) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "Select * from allergy where patient_id='$patient_id'";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {
            $this->db->trans_start();
            $allergy_id = $value->allergy_id;
            echo 'Allergy id' . $allergy_id . '<br>';
            $update_allergy = array(
                'immunization' => $family_social,
                'member_id' => $member_id,
                'branch_id' => $branch_id
            );
            $this->db->where('allergy_id', $allergy_id);
            $this->db->update('allergy', $update_allergy);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function add_patient_social_history($visit_id, $patient_id, $immunization) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $this->db->trans_start();
        $add_allergy = array(
            'patient_id' => $patient_id,
            'immunization' => $immunization,
            'branch_id' => $branch_id,
            'member_id' => $member_id);
        $this->db->insert('allergy', $add_allergy);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function update_patient_social_history($visit_id, $patient_id, $family_social) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "Select * from allergy where patient_id='$patient_id'";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {
            $this->db->trans_start();
            $allergy_id = $value->allergy_id;
            echo 'Allergy id' . $allergy_id . '<br>';
            $update_allergy = array(
                'social_history' => $family_social,
                'member_id' => $member_id,
                'branch_id' => $branch_id
            );
            $this->db->where('allergy_id', $allergy_id);
            $this->db->update('allergy', $update_allergy);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function get_allergy($patient_id) {

        $sql = "Select * from allergy where patient_id='$patient_id' ORDER BY `patient_id` ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_triage($patient_id, $visit_id) {
        $sql = "SELECT * FROM `triage` where patient_id='$patient_id' and visit_id= '$visit_id' ORDER BY `patient_id` ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function nurse_walkin_list() {
        $sql = "select patient.patient_id, concat(title,' ',f_name,' ',' ',s_name,' ',other_name) as patient_name
 , visit.visit_id as walkin_visit_id , visit.end , nurse_queue, visit.member_id ,
 visit.branch_id from visit inner join patient on patient.patient_id = visit.patient_id where nurse_queue='Active' and   DATE(start) = DATE(NOW()) and type='Walk-in'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function add_walkin_procedure($visit_id, $patient_id, $procedure_type) {
        $this->db->trans_start();

        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $user_id = $this->session->userdata('id');
        $department = $this->session->userdata('type');

        foreach ($procedure_type as $value) {

            $procedure_insert = array(
                'patient_id' => $patient_id,
                'visit_id' => $visit_id,
                'procedure_id' => $value,
                'member_id' => $member_id,
                'branch_id' => $branch_id,
                'user_id' => $user_id,
                'status' => 'Active'
            );

            $this->db->insert('procedure_result', $procedure_insert);

            $query_result = $this->db->query("Select * from packages where package_id='$value'");
            foreach ($query_result->result() as $value) {
                $test_name = $value->package_name;
                $test_price = $value->cost;

                $revenue_type = 'Procedure';

                $patient_viist_statement_insert = array(
                    'patient_id' => $patient_id,
                    'visit_id' => $visit_id,
                    'amount' => $test_price,
                    'description' => $test_name,
                    'member_id' => $member_id,
                    'branch_id' => $branch_id,
                    'revenue_type' => $revenue_type,
                    'department' => $department,
                    'user_id' => $user_id,
                    'charged' => 'Yes',
                    'paid' => 'No',
                    'patient_payment_id' => $visit_id
                );
                $this->db->insert('patient_visit_statement', $patient_viist_statement_insert);
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

}
