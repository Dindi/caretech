<?php

class Operations_Model extends CI_Model {

    public function _construct() {
        parent::_construct();
    }

    public function send_to_pharmacy($visit_id) {
        $this->db->trans_start();

        $time = date("H:i:s");
        $in_active = "In Active";
        $active = "Active";
        $department = $this->session->userdata('type');
        if ($department === "Nurse") {

            $visit_update = array(
                'nurse_end' => $time,
                'nurse_queue' => $in_active,
                'doctor_queue' => $in_active,
                'lab_queue' => $in_active,
                'pharm_queue' => $active,
                'pharm_start' => $time
            );
        } elseif ($department === "Laboratory") {

            $visit_update = array(
                'lab_end' => $time,
                'lab_queue' => $in_active,
                'nurse_queue' => $in_active,
                'doctor_queue' => $in_active,
                'pharm_queue' => $active,
                'pharm_start' => $time
            );
        } elseif ($department === "Doctor") {

            $visit_update = array(
                'doctor_end' => $time,
                'doctor_queue' => $in_active,
                'nurse_queue' => $in_active,
                'lab_queue' => $in_active,
                'pharm_queue' => $active,
                'pharm_start' => $time
            );
        } elseif ($department === "Pharmacy") {

            $visit_update = array(
                'pharm_end' => $time,
                'pharm_queue' => $in_active,
                'nurse_queue' => $in_active,
                'doctor_queue' => $in_active,
                'lab_queue' => $in_active,
                'pharm_queue' => $active,
                'pharm_start' => $time
            );
        }


        $this->db->where('visit_id', $visit_id);
        $this->db->update('visit', $visit_update);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function send_to_lab($visit_id) {
        $this->db->trans_start();

        $time = date("H:i:s");
        $in_active = "In Active";
        $active = "Active";
        $department = $this->session->userdata('type');
        if ($department === "Nurse") {

            $visit_update = array(
                'nurse_end' => $time,
                'nurse_queue' => $in_active,
                'doctor_queue' => $in_active,
                'pharm_queue' => $in_active,
                'lab_queue' => $active,
                'lab_start' => $time
            );
        } elseif ($department === "Laboratory") {

            $visit_update = array(
                'pharm_end' => $time,
                'pharm_queue' => $in_active,
                'nurse_queue' => $in_active,
                'doctor_queue' => $in_active,
                'lab_queue' => $active,
                'lab_start' => $time
            );
        } elseif ($department === "Doctor") {

            $visit_update = array(
                'doctor_end' => $time,
                'doctor_queue' => $in_active,
                'nurse_queue' => $in_active,
                'pharm_queue' => $in_active,
                'lab_queue' => $active,
                'lab_start' => $time
            );
        } elseif ($department === "Pharmacy") {

            $visit_update = array(
                'pharm_end' => $time,
                'pharm_queue' => $in_active,
                'nurse_queue' => $in_active,
                'doctor_queue' => $in_active,
                'lab_queue' => $active,
                'lab_start' => $time
            );
        }


        $this->db->where('visit_id', $visit_id);
        $this->db->update('visit', $visit_update);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function send_to_doctor($patient_id, $visit_id, $doctor_id) {
        $current_doctor = $this->input->post('current_doctor');
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $active = "Active";
        $in_active = "In Active";
        $time_start = date("H:i:s");
        $time_end = date("H:i:s");

        if (!empty($current_doctor)) {
            $send_to_doctor = array(
                'doctor_queue' => $active,
                'nurse_queue' => $in_active,
                'nurse_end' => $time_end,
                'doctor_start' => $time_start,
                'member_id' => $member_id,
                'branch_id' => $branch_id,
                'lab_queue' => $in_active,
                'pharm_queue' => $in_active
            );
            $this->db->where('visit_id', $visit_id);
            $this->db->update('visit', $send_to_doctor);
        } else {
            $send_to_doctor = array(
                'doctor_id' => $doctor_id,
                'doctor_queue' => $active,
                'nurse_queue' => $in_active,
                'nurse_end' => $time_end,
                'doctor_start' => $time_start,
                'member_id' => $member_id,
                'branch_id' => $branch_id
            );
            $this->db->where('visit_id', $visit_id);
            $this->db->update('visit', $send_to_doctor);
        }
    }

    public function release_patient($visit_id) {
        $in_active = 'In Active';
        $release_patient = array(
            'doctor_queue' => $in_active,
            'nurse_queue' => $in_active,
            'nurse_end' => $in_active,
            'doctor_start' => $in_active,
        );
        $this->db->where('visit_id', $visit_id);
        $this->db->update('visit', $release_patient);
    }

    public function update_appointment($patient_id, $visit_id, $appointment_type, $person_to_see, $date_from, $date_to, $reason_for_appointment) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "Select * from appointment where visit_id='$visit_id'";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {
            $this->db->trans_start();
            $appointment_id = $value->appointment_id;
            $update_appointment_array = array(
                'visit_id' => $visit_id,
                'title' => $appointment_type,
                'queue_booked' => $person_to_see,
                'time_start' => $date_from,
                'time_end' => $date_to,
                'about' => $reason_for_appointment,
                'member_id' => $member_id,
                'branch_id' => $branch_id
            );
            $this->db->where('appointment_id', $appointment_id);
            $this->db->update('appointment', $update_appointment_array);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function add_appointment($patient_id, $visit_id, $appointment_type, $person_to_see, $date_from, $date_to, $reason_for_appointment) {

        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $this->db->trans_start();
        $data_insert_array = array(
            'patient_id' => $patient_id,
            'visit_id' => $visit_id,
            'title' => $appointment_type,
            'queue_booked' => $person_to_see,
            'time_start' => $date_from,
            'time_end' => $date_to,
            'about' => $reason_for_appointment,
            'member_id' => $member_id,
            'branch_id' => $branch_id
        );
        $this->db->insert('appointment', $data_insert_array);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function get_appointments() {
        $sql = "SELECT concat(patient.title,' ' ,patient.f_name,' ' , patient.s_name,' ' , patient.other_name) as title  , visit.allDay,visit.start , visit.end  FROM `visit` inner join patient on patient.patient_id = visit.patient_id
                 inner join packages on packages.package_id = visit.package_type where DATE(visit.start) = DATE(NOW())  ORDER BY `visit_status` DESC";



//        $sql = "SELECT * FROM `evenement`";
//        
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_pharm_bin_card() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT * FROM `stock_bin_card`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function stock_management() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT * FROM `stock_view`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

   

}
