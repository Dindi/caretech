<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Author: Harris Dindi
 * Description: Login model class
 */

class Reports_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function patient_report() {
        $date_to = $this->input->post('date_to');

        $date_from = $this->input->post('date_from');

        $patient_status = $this->input->post('patient_status');

        $gender = $this->input->post('gender');


        if ($patient_status == 'All' && $gender == 'All') {

            if ($patient_status == 'All' and $gender === 'All' and empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient";
            } elseif ($patient_status === 'All' and $gender === 'All' and ! empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where date_joined >='$date_from'";
            } elseif ($patient_status === 'All' and $gender === 'All' and empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where date_joined <='$date_to'";
            } elseif ($patient_status === 'All' and $gender === 'All' and ! empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where (date_joined between '$date_from' and '$date_to')";
            }
        } elseif ($patient_status === 'All') {
            if ($patient_status === 'All' and ! empty($gender) and empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where gender='$gender'";
            } elseif ($patient_status === 'All' and ! empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where date_joined >='$date_from'";
            } elseif ($patient_status === 'All' and empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where date_joined <='$date_to'";
            } elseif ($patient_status === 'All' and ! empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient  where (date_joined between '$date_from' and '$date_to')";
            }
        } elseif ($gender === 'All') {
            if ($gender = 'All' and empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient";
            } elseif ($gender === 'All' and ! empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where date_joined >='$date_from'";
            } elseif ($gender === 'All' and empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where date_joined <='$date_to'";
            } elseif ($gender === 'All' and ! empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where (date_joined between '$date_from' and '$date_to')";
            }
        } else {
            if (empty($date_from) && empty($date_to) && empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient";
            } elseif (!empty($date_from) && !empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where (date_joined between '$date_from' and '$date_to') and status ='$patient_status' and gender='$gender'";
            } elseif (empty($date_from) && !empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where date_joined <='$date_to' and status='$patient_status' and gender='$gender'";
            } elseif (!empty($date_from) && empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where date_joined >= '$date_from' and status='$patient_status'";
            } elseif (!empty($date_from) && !empty($date_to) && empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where gender='$gender' and (date_joined between '$date_from' and '$date_to')";
            } elseif (!empty($date_from) && !empty($date_to) && !empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient where status='$patient_status' and (date_joined between '$date_from' and '$date_to')";
            } elseif (empty($date_from) && empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where status='$patient_status' and gender='$gender'";
            } elseif (!empty($date_from) && !empty($date_to) && empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient  where (date_joined between '$date_from' and '$date_to')";
            } elseif (empty($date_from) && !empty($date_to) && empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where date_joined >= '$date_to' and gender='$gender'";
            } elseif (empty($date_from) && !empty($date_to) && !empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient where date_joined <='$date_to' and status='$patient_status'";
            } elseif (!empty($date_from) && empty($date_to) && !empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient where date_joined >='$date_from' and status='$patient_status'";
            }
        }


        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function export_patient_report() {

        $date_to = $this->input->post('date_to');

        $date_from = $this->input->post('date_from');

        $patient_status = $this->input->post('patient_status');

        $gender = $this->input->post('gender');


        if ($patient_status == 'All' && $gender == 'All') {

            if ($patient_status == 'All' and $gender === 'All' and empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient";
            } elseif ($patient_status === 'All' and $gender === 'All' and ! empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where date_joined >='$date_from'";
            } elseif ($patient_status === 'All' and $gender === 'All' and empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where date_joined <='$date_to'";
            } elseif ($patient_status === 'All' and $gender === 'All' and ! empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where (date_joined between '$date_from' and '$date_to')";
            }
        } elseif ($patient_status === 'All') {
            if ($patient_status === 'All' and ! empty($gender) and empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where gender='$gender'";
            } elseif ($patient_status === 'All' and ! empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where date_joined >='$date_from'";
            } elseif ($patient_status === 'All' and empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where date_joined <='$date_to'";
            } elseif ($patient_status === 'All' and ! empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient  where (date_joined between '$date_from' and '$date_to')";
            }
        } elseif ($gender === 'All') {
            if ($gender = 'All' and empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient";
            } elseif ($gender === 'All' and ! empty($date_from) and empty($date_to)) {

                $sql = "Select * from patient where date_joined >='$date_from'";
            } elseif ($gender === 'All' and empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where date_joined <='$date_to'";
            } elseif ($gender === 'All' and ! empty($date_from) and ! empty($date_to)) {

                $sql = "Select * from patient where (date_joined between '$date_from' and '$date_to')";
            }
        } else {
            if (empty($date_from) && empty($date_to) && empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient";
            } elseif (!empty($date_from) && !empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where (date_joined between '$date_from' and '$date_to') and status ='$patient_status' and gender='$gender'";
            } elseif (empty($date_from) && !empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where date_joined <='$date_to' and status='$patient_status' and gender='$gender'";
            } elseif (!empty($date_from) && empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where date_joined >= '$date_from' and status='$patient_status'";
            } elseif (!empty($date_from) && !empty($date_to) && empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where gender='$gender' and (date_joined between '$date_from' and '$date_to')";
            } elseif (!empty($date_from) && !empty($date_to) && !empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient where status='$patient_status' and (date_joined between '$date_from' and '$date_to')";
            } elseif (empty($date_from) && empty($date_to) && !empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where status='$patient_status' and gender='$gender'";
            } elseif (!empty($date_from) && !empty($date_to) && empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient  where (date_joined between '$date_from' and '$date_to')";
            } elseif (empty($date_from) && !empty($date_to) && empty($patient_status) && !empty($gender)) {

                $sql = "Select * from patient where date_joined >= '$date_to' and gender='$gender'";
            } elseif (empty($date_from) && !empty($date_to) && !empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient where date_joined <='$date_to' and status='$patient_status'";
            } elseif (!empty($date_from) && empty($date_to) && !empty($patient_status) && empty($gender)) {

                $sql = "Select * from patient where date_joined >='$date_from' and status='$patient_status'";
            }
        }


        $query = $this->db->query($sql);
        $result = $query->result_array();

//load our new PHPExcel library
        $this->load->library('excel');



        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Patients Report');
        $this->excel->getActiveSheet()->SetCellValue('A1', "Date Registered");
        $this->excel->getActiveSheet()->SetCellValue('B1', "Patient Name");
        $this->excel->getActiveSheet()->SetCellValue('C1', "Dob");
        $this->excel->getActiveSheet()->SetCellValue('D1', "Gender");
        $this->excel->getActiveSheet()->SetCellValue('E1', "Phone No");
        $this->excel->getActiveSheet()->SetCellValue('F1', "Email");
        $this->excel->getActiveSheet()->SetCellValue('G1', "Added By");



        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(15);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);





        $i = 2;
        foreach ($results as $result) {

            $this->excel->getActiveSheet()->SetCellValue('A' . $i, $result["unit_name"]);
            $this->excel->getActiveSheet()->SetCellValue('B' . $i, $result["registration_number"]);
            $this->excel->getActiveSheet()->SetCellValue('C' . $i, $result["unit_code"]);
            $this->excel->getActiveSheet()->SetCellValue('D' . $i, $result["unit_type"]);
            $this->excel->getActiveSheet()->SetCellValue('E' . $i, $result["date_added"]);
            $this->excel->getActiveSheet()->SetCellValue('F' . $i, $result["is_active"]);
            $this->excel->getActiveSheet()->SetCellValue('G' . $i, $result["user_name"]);




            $i++;
        }




        $today = date("d-m-Y");


        $filename = "Crescent_.$today.Units_report.xlsx";
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');



        $objWriter->save('php://output');

        $this->excel->disconnectWorksheets();
    }

    function visitation_report() {
        $patient_id = $this->input->post('patient_name');
        $package_type = $this->input->post('package_type');
        $date_to = $this->input->post('date_to');
        $date_from = $this->input->post('date_from');
        $sql = "select * from visit "
                . "inner join patient on patient.patient_id = visit.patient_id "
                . "inner join packages on packages.package_id = visit.package_type where 1 ";
        if (!empty($patient_id)) {
            $sql .= " AND visit.patient_id='$patient_id' ";
        }
        if (!empty($package_type)) {
            $sql .= " AND visit.package_type='$package_type' ";
        }
        if (!empty($date_from)) {
            $sql .= " AND visit.visit_date >='$date_from' ";
        }
        if (!empty($date_to)) {
            $sql .= " AND visit.visit_date <='$date_to' ";
        }
        if (!empty($date_from) && !empty($date_to)) {
            $sql .= " AND visit.visit_date between '$date_from' and '$date_to' ";
        }
        $result = $this->db->query($sql);
        $result = $result->result_array();
        return $result;
    }

    function get_visitation_entry($id) {

        $sql = " select visit.family_number as family_number,visit.urgency,visit.patient_id,visit.pay_at_the_end, visit.nurse_queue,visit.doctor_queue,visit.lab_queue,visit.pharm_queue,visit.visit_date as visit_date,visit.visit_id as visit_id , packages.package_name as package_name, concat(patient.title,'',patient.f_name,' ',patient.s_name,' ',' ',patient.other_name) as patient_name from visit
                  inner join packages on packages.package_id=visit.package_type 
                  inner join patient on patient.patient_id= visit.patient_id where visit.visit_id='$id'";
        $query = $this->db->query($sql);

        return $query->result();
    }

    function walkin_report() {

        $department_name = $this->input->post('department_name');
        $payment_status = $this->input->post('payment_status');
        $date_to = $this->input->post('date_to');
        $date_from = $this->input->post('date_from');
        $sql = "select * from walkin where 1 ";
        if (!empty($department_name)) {
            if ($department_name === 'All') {
                $sql .= "";
            } else {
                $sql .= " AND walkin.walkin_department='$department_name' ";
            }
        }
        if (!empty($payment_status)) {
            if ($payment_status === 'All') {
                $sql .="";
            } else {
                $sql .= " AND walkin.paid='$payment_status' ";
            }
        }
        if (!empty($date_from)) {
            $sql .= " AND walkin.walkin_date >='$date_from' ";
        }
        if (!empty($date_to)) {
            $sql .= " AND walkin.walkin_date <='$date_to' ";
        }
        if (!empty($date_from) && !empty($date_to)) {
            $sql .= " AND walkin.walkin_date between '$date_from' and '$date_to' ";
        }
        $result = $this->db->query($sql);
        $result = $result->result_array();
        return $result;
    }

    function procedure_report() {

        $procedure_name = $this->input->post('procedure_name');
        $patient_type = $this->input->post('patient_type');
        $date_to = $this->input->post('date_to');
        $date_from = $this->input->post('date_from');
        $sql = "select * from procedure_visit inner join procedure_list on procedure_visit.procedure_list_id = procedure_list.procedure_id where 1 ";
        if (!empty($procedure_name)) {
            if ($procedure_name === 'All') {
                $sql .= "";
            } else {
                $sql .= " AND procedure_visit.procedure_list_id='$procedure_name' ";
            }
        }
        if (!empty($payment_status)) {
            if ($payment_status === 'All') {
                $sql .="";
            } else {
                $sql .= " AND procedure_visit.patient_type='$payment_status' ";
            }
        }
        if (!empty($date_from)) {
            $sql .= " AND procedure_visit.date_added >='$date_from' ";
        }
        if (!empty($date_to)) {
            $sql .= " AND procedure_visit.date_added <='$date_to' ";
        }
        if (!empty($date_from) && !empty($date_to)) {
            $sql .= " AND procedure_visit.date_added between '$date_from' and '$date_to' ";
        }
        $result = $this->db->query($sql);
        $result = $result->result_array();
        return $result;
    }

    function get_patient_reports_details($patient_id) {
       
        $sql = "Select * from patient where patient_id='$patient_id'";
        $result = $this->db->query($sql);
        $result = $result->result_array();
        return $result;
    }

}

?>