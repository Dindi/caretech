<?php

class Mactive extends CI_Model {

    public function _construct() {
        parent::_construct();
    }

    public function nactive() {//Nurse
        $nurse_id = $this->uri->segment(3);
        $query = "SELECT  DISTINCT patient.f_name,visit.patient_id, patient.other_name, patient.s_name, visit.visit_date, visit.visit_id,visit.urgency, visit.results FROM patient
		INNER JOIN visit
		ON patient.patient_id=visit.patient_id INNER JOIN patient_visit_statement ON patient_visit_statement.visit_id=visit.visit_id
               WHERE DATE(visit.start) = DATE(NOW())
		AND visit.nurse_queue = 'active' AND patient_visit_statement.amount - patient_visit_statement.amount_dr = 0
                group by visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function nactive_end() {//Nurse
        $nurse_id = $this->uri->segment(3);
        $query = "SELECT  DISTINCT patient.f_name,visit.patient_id, patient.other_name, patient.s_name, visit.visit_date, visit.visit_id,visit.urgency, visit.results FROM patient
		INNER JOIN visit
		ON patient.patient_id=visit.patient_id INNER JOIN patient_visit_statement ON patient_visit_statement.visit_id=visit.visit_id
               WHERE DATE(visit.start) = DATE(NOW())
		AND visit.nurse_queue = 'active' AND visit.pay_at_the_end='yes'
                group by visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }
    public function doctor_workload() {
        $sql = "select visit.visit_id , patient.patient_id, concat(employee.title,'',employee.f_name,'',employee.other_name,'',employee.l_name,'') as employee_name,"
                . "visit.visit_date, concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name) as patient_name , visit.doctor_start,"
                . " visit.doctor_end from visit inner join patient on patient.patient_id = visit.patient_id inner join employee on employee.employee_id = visit.doctor_id"
                . " where DATE(visit.start) = DATE(NOW())";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function load_allergies($patient_id) {
        $sql = "select   allergies  from allergy  where patient_id='$patient_id' group by patient_id";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function past_medical_records($patient_id) {
        $sql = "select concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name, 
            concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name,'') as patient_name,
            consultation.complaints,consultation.medical_history,consultation.systematic_inquiry,
            consultation.examination_findings,consultation.final_diagnosis,consultation.working_diagnosis,
            consultation.date from consultation 
            inner join employee on employee.employee_id = consultation.doctor_id 
            inner join patient on patient.patient_id = consultation.patient_id where patient.patient_id='$patient_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function past_prescriptions($patient_id) {
        $sql = "select commodity.commodity_name, concat(employee.title,'',employee.f_name,'',employee.l_name,'',employee.other_name,'')as employee_name,"
                . " prescription.route, prescription.frequency,prescription.strength,prescription.date,prescription.duration, prescription.remarks from prescription"
                . " inner join commodity on commodity.commodity_name = prescription.commodity_name inner join employee on employee.employee_id = prescription.doctor_id"
                . " where prescription.patient_id='$patient_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function past_lab_results($patient_id) {

        $sql = "select concat(employee.title,' ',employee.f_name,' ',employee.l_name,' ',employee.other_name) as employee_name,"
                . "lab_test_result.date_added as date_added, concat(patient.title,' ',patient.f_name,' ',patient.s_name,' ',patient.other_name,'') as patient_name, "
                . "lab_test_result.test, lab_test_result.test_results from lab_test_result"
                . " inner join employee on employee.employee_id = lab_test_result.lab_tech_id inner join patient on patient.patient_id = lab_test_result.patient_id"
                . " where patient.patient_id='$patient_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function patient_triage_history($patient_id) {

        $sql = "select triage.weight, triage.diastolic, triage.systolic, triage.temperature,triage.height,triage.respiratory_rate,triage.pulse_rate,triage.lmp,triage.blood_sugar,triage.OCS,triage.date, concat(employee.f_name,'',employee.l_name,'',employee.other_name) as employee_name , concat(patient.title,'',patient.f_name,' ',patient.s_name,' ',patient.other_name,' ')as patient_name from triage inner join patient on patient.patient_id=triage.patient_id inner join employee on employee.employee_id = triage.nurse_id"
                . " where patient.patient_id='$patient_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function requests() {
        $trans_type = 'Request';

        $sql = "SELECT request.request_id,request_department_from,request.user_from , request.date_added, request.total_quantity_requested, request.commodity_name, employee.username
                 FROM request
                 INNER JOIN employee ON employee.employee_id = request.user_id
                 WHERE request.request_order_id IS NOT NULL 
                 ORDER BY (
                 request.date_added
                 ) DESC";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function request_type() {//Request
        //$trans_type = 'Request';
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "SELECT distinct department.department_name
                FROM request
                INNER JOIN employee ON employee.employee_id = request.user_from
                INNER JOIN department ON department.department_id = employee.employment_category
                WHERE  department.department_name LIKE  '%Pharm%' 
                AND request.date_added >= CURDATE() 
                AND request.request_order_id IS NOT NULL 
                AND request.stock_id='0' AND request.member_id='$member_id' AND request.branch_id='$branch_id'
                ORDER BY (
                request.date_added
                ) DESC";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function request_details() {//Request
        //$trans_type = 'Request';
        $sql = "SELECT distinct department.department_name , request.request_order_id , stock.commodity_name , request.total_quantity_requested , request.date_added
                FROM request
                INNER JOIN employee ON employee.employee_id = request.user_from  
                INNER JOIN department ON department.department_id = employee.employment_category
                INNER JOIN stock ON stock.code = request.commodity_name
                WHERE  employee.employment_category  ='6' 
                AND request.date_added >= CURDATE() 
                AND request.request_order_id IS NOT NULL
                AND request.stock_id='0' 
                ORDER BY (
                request.date_added
                ) DESC";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function dactive() {//Doctor
        $query = "SELECT patient.f_name, visit.patient_id, patients.other_name, patients.s_name, visit.visit_date, visit.visit_id
		FROM patient
		INNER JOIN visit
		ON patient.patient_id=visit.patient_id
		WHERE DATE(visit.start) = DATE(NOW())
		AND visit.doctor_queue = 'active'
		ORDER BY visit.start ";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function lactive() {//lab
        $query = "SELECT patient.f_name, visit.patient_id, patients.other_name, patients.s_name, visit.visit_date, visit.visit_id
		FROM patient
		INNER JOIN visit
		ON patient.patient_id=visit.patient_id
		WHERE DATE(visit.start) = DATE(NOW())
		AND visit.lab_queue = 'active'
		ORDER BY visit.start ";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function pactive() {//pharm
        $query = "SELECT patient.f_name, visit.patient_id, patients.other_name, patients.s_name, visit.visit_date, visit.visit_id
		FROM patient
		INNER JOIN visit
		ON patient.patient_id=visit.patient_id
		WHERE DATE(visit.start) = DATE(NOW())
		AND visit.pharm_queue = 'active'
		ORDER BY visit.start ";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function see_history($id) {
        $sql = "SELECT DISTINCT employee.f_name, employee.l_name, triage.nurse_id, triage.visit_id, visit.visit_date, visit.visit_id,triage.weight, triage.height, 
			triage.OCS, triage.diastolic, triage.systolic,triage.temperature,triage.respiratory_rate, triage.pulse_rate
            FROM triage
            INNER JOIN visit
            ON triage.visit_id=visit.visit_id
            INNER JOIN employee
            ON triage.nurse_id = employee.employee_id
            WHERE visit.patient_id = '" . $id . "'";
        $query = $this->db->query($sql);
        return $query->result_array();
        //$query=$this->db->get_where('visit',array('patientid'=> $patientid));
        //$result=$this->db->query($query);
        //return $query->result_array();
    }

    public function getallergy($patient) {
        $query = "SELECT triage.allergy
		FROM patient
		INNER JOIN triage
		ON patient.patient_id=triage.patient_id
		WHERE triage.patient_id = '" . $patient . "'";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function see_visit($id) {

        //$query = $this->db->get_where('triage', array('visit_id' => $id,''=>));
        $query = "Select * from triage where visit_id='$id' and date >= curdate()";
        $query = $this->db->query($query);
        return $query->result_array();
    }

    public function see_details($visit_id) {
        //$visit_id=$this->uri->segment(3);
        $query = $this->db->get_where('visit', array('visit_id' => $visit_id));
        // if($query->num_rows()==1){
        foreach ($query->result_array() as $row) {
            $patient_id = $row['patient_id'];
            $sql = $this->db->get_where('patient', array('patient_id' => $patient_id));
            return $sql->result_array();
            //}
        }
    }

    public function triage() {
        $Nurseid = $this->session->userdata('id');
        echo 'Nurse ID' . $Nurseid . '</br>';
        $visitid = $this->input->post('visitid', TRUE);
        $visitid = mysql_real_escape_string($visitid);
        echo 'visit _id' . $visitid . '</br>';
        $patientid = $this->input->post('patientid', TRUE);
        echo 'Patient id' . $patientid . '</br>';
        $patientid = mysql_real_escape_string($patientid);
        echo $patientid;
        $Height = $this->input->post('Height', TRUE);
        $Height = mysql_real_escape_string($Height);

        $diastle = $this->input->post('diastle', TRUE);
        $diastle = mysql_real_escape_string($diastle);

        $systle = $this->input->post('systle', TRUE);
        $systle = mysql_real_escape_string($systle);

        $Temperature = $this->input->post('Temperature', TRUE);
        $Temperature = mysql_real_escape_string($Temperature);

        $Weight = $this->input->post('Weight', TRUE);
        $Weight = mysql_real_escape_string($Weight);

        $respiratory = $this->input->post('respiratory', TRUE);
        $respiratory = mysql_real_escape_string($respiratory);

        $pulse = $this->input->post('pulse', TRUE);
        $pulse = mysql_real_escape_string($pulse);

        $OCS = $this->input->post('OCS', TRUE);
        $OCS = mysql_real_escape_string($OCS);

        $blood_sugar = $this->input->post('blood_sugar', TRUE);
        $blood_sugar = mysql_real_escape_string($blood_sugar);

        $lmp = $this->input->post('lmp', TRUE);
        $lmp = mysql_real_escape_string($lmp);

        $general_complaints = $this->input->post('general_complaints', TRUE);
        $general_complaints = mysql_real_escape_string($general_complaints);

        $allergy = $this->input->post('allergy', TRUE);
        $allergy = mysql_real_escape_string($allergy);

        //$urgency=mysql_real_escape_string($urgency);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $this->db->set('visit_id', $visitid);
        $this->db->set('nurse_id', $Nurseid);
        $this->db->set('weight', $Weight);
        $this->db->set('height', $Height);
        $this->db->set('diastolic', $diastle);
        $this->db->set('systolic', $systle);
        $this->db->set('temperature', $Temperature);
        $this->db->set('OCS', $OCS);
        $this->db->set('respiratory_rate', $respiratory);
        $this->db->set('pulse_rate', $pulse);
        $this->db->set('patient_id', $patientid);
        $this->db->set('lmp', $lmp);
        $this->db->set('blood_sugar', $blood_sugar);
        $this->db->set('allergy', $general_complaints);
        $this->db->set('member_id', $member_id);
        $this->db->set('branch_id', $branch_id);

        $this->db->insert('triage');

        if (!empty($allergy)) {
            $this->db->set('allergies', $allergy);
            $this->db->set('patient_id', $patientid);
            $this->db->insert('allergy');
        } else {
            $data = array(
                'allergies' => $allergy
            );

            $this->db->where('patient_id', $patientid);
            $this->db->update('allergy', $data);
        }

        if ($this->db->affected_rows()) {
            $this->urgent($visitid);
        }
    }

    public function urgent($id) {
        $urgent = $this->input->post('urgency', TRUE);

        $data = array(
            'urgency' => $urgent
        );
        $this->db->where('visit_id', $id);
        $this->db->update('visit', $data);
    }

    public function ToDoc($id) {
        $doctor_id = $this->input->post('doctor_id', TRUE);
        $doctor_id = mysql_real_escape_string($doctor_id);
        $today = date("H:i:s");
        $data = array(
            'doctor_queue' => 'active',
            'doctor_start' => $today,
            'nurse_queue' => 'In Active',
            'nurse_end' => $today,
            'doctor_id' => $doctor_id
        );

        $this->db->where('visit_id', $id);
        $this->db->update('visit', $data);
    }

    public function Finish($id) {
        $today = date("H:i:s");
        $data = array(
            'nq' => 'In Active',
            'nend' => $today
        );

        $this->db->where('id', $id);
        $this->db->update('visit', $data);
    }

    public function name($patientid) {
        $query = $this->db->get_where('patient', array('patient_id' => $patientid));
        return $query->result_array();
    }

    public function see_appointments() {
        $empid = $this->session->userdata('id');
        $sql = "SELECT patient.f_name, patient.other_name, patient.s_name,patient.phone_no, visit.visit_date, visit.visit_id, 
		appointment.About,appointment.Time
		FROM appointment
		INNER JOIN visit
		ON appointment.visit_id=visit.visit_id
		INNER JOIN patient
		ON appointment.patient_id=patient.patient_id
		INNER JOIN employee
		ON appointment.employee_id=employee.employee_id
		WHERE appointment.date = CURDATE()
		AND employee.employee_id='" . $empid . "'
		ORDER BY appointment.Time";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

}

?>