<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Doctor_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function doctor_patient_list() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $doctor_id = $this->session->userdata('id');
        $sql = "SELECT  DISTINCT patient.title ,visit.urgency,patient.f_name,visit.patient_id, patient.other_name, patient.s_name,"
                . " visit.visit_date,charge_followup, visit.visit_id,visit.urgency, visit.results"
                . " FROM patient INNER JOIN visit ON patient.patient_id=visit.patient_id"
                . " INNER JOIN patient_visit_statement ON patient_visit_statement.visit_id=visit.visit_id "
                . " WHERE DATE(visit.start) = DATE(NOW()) AND visit.doctor_queue = 'Active'"
                . " AND patient_visit_statement.amount - patient_visit_statement.amount_dr = 0 and visit.doctor_id='$doctor_id'"
                . " and visit.pay_at_the_end='no' and visit.member_id='$member_id' and visit.branch_id='$branch_id' group by visit.visit_id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    
     public function doctor_patient_list_xpress() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $doctor_id = $this->session->userdata('id');
        $sql = "SELECT  DISTINCT patient.title ,visit.urgency,patient.f_name,visit.patient_id, patient.other_name, patient.s_name,"
                . " visit.visit_date, visit.visit_id,visit.urgency,charge_followup, visit.results"
                . " FROM patient INNER JOIN visit ON patient.patient_id=visit.patient_id"
                . " INNER JOIN patient_visit_statement ON patient_visit_statement.visit_id=visit.visit_id "
                . " WHERE DATE(visit.start) = DATE(NOW()) AND visit.doctor_queue = 'Active'"
                . "and visit.pay_at_the_end='yes'  and visit.doctor_id='$doctor_id'"
                . " and visit.member_id='$member_id' and visit.branch_id='$branch_id' group by visit.visit_id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function icd_10_codes($returned_value) {

        $sql = "Select concat(diagnosis_code,',',description) as icd_description  from consultation_icd10 where description like '%$returned_value%' OR diagnosis_code like '%$returned_value%' LIMIT 0,10 ";
        $query = $this->db->query($sql);
        $query = $query->result_array();
        return $query;
    }

    public function icd_10_codes_2($returned_value) {

        $sql = "Select concat(diagnosis_code,',',description) as icd_description  from consultation_icd10 where description = '$returned_value' OR diagnosis_code = '$returned_value' LIMIT 0,10 ";
        $query = $this->db->query($sql);
        $query = $query->result_array();
        return $query;
    }

    public function update_chief_complaints($visit_id, $patient_id, $chief_complaints) {
        $sql = "Select * from consultation where visit_id='$visit_id'";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {
            $this->db->trans_start();
            $consultation_id = $value->consultation_id;

            $consultation_data_update = array(
                'complaints' => $chief_complaints
            );

            $this->db->where('consultation_id', $consultation_id);
            $this->db->update('consultation', $consultation_data_update);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            } else {
                return TRUE;
            }
        }
    }

    public function update_review_of_systems($patient_id, $visit_id, $constitutional, $ent, $cardiovascular, $eye, $respiratory, $gastrointestinal, $genitourinary, $masculoskeletal, $skin, $neurologic, $other_systems) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $doctor_id = $this->session->userdata('id');
        $sql = "Select * from review_of_systems where visit_id='$visit_id'";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {

            $review_of_systems_id = $value->id;
            $this->db->trans_start();
            $review_of_systems_update = array(
                'patient_id' => $patient_id,
                'visit_id' => $visit_id,
                'constitutional' => $constitutional,
                'ent' => $ent,
                'cardiovascular' => $cardiovascular,
                'eye' => $eye,
                'respiratory' => $respiratory,
                'gastrointestinal' => $gastrointestinal,
                'genitourinary' => $genitourinary,
                'masculoskeletal' => $masculoskeletal,
                'skin' => $skin,
                'neurologic' => $neurologic,
                'other' => $other_systems,
                'member_id' => $member_id,
                'branch_id' => $branch_id,
                'user_id' => $doctor_id
            );
            $this->db->where('id', $review_of_systems_id);
            $this->db->update('review_of_systems', $review_of_systems_update);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                
            } else {
                return TRUE;
            }
        }
    }

    public function insert_review_of_systems($patient_id, $visit_id, $constitutional, $ent, $cardiovascular, $eye, $respiratory, $gastrointestinal, $genitourinary, $masculoskeletal, $skin, $neurologic, $other_systems) {
        $this->db->trans_start();
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $doctor_id = $this->session->userdata('id');
        $review_of_systems_insert = array(
            'patient_id' => $patient_id,
            'visit_id' => $visit_id,
            'constitutional' => $constitutional,
            'ent' => $ent,
            'cardiovascular' => $cardiovascular,
            'eye' => $eye,
            'respiratory' => $respiratory,
            'gastrointestinal' => $gastrointestinal,
            'genitourinary' => $genitourinary,
            'masculoskeletal' => $masculoskeletal,
            'skin' => $skin,
            'neurologic' => $neurologic,
            'other' => $other_systems,
            'member_id' => $member_id,
            'branch_id' => $branch_id,
            'user_id' => $doctor_id
        );
        $this->db->insert('review_of_systems', $review_of_systems_insert);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function insert_chief_complaints($visit_id, $patient_id, $chief_complaints) {
        $this->db->trans_start();
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $doctor_id = $this->session->userdata('id');
        $compaint_insert = array(
            'complaints' => $chief_complaints,
            'visit_id' => $visit_id,
            'patient_id' => $patient_id,
            'member_id' => $member_id,
            'branch_id' => $branch_id
        );
        $this->db->insert('consultation', $compaint_insert);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function patient_icd_10_code($patient_id, $visit_id, $patient_icd_10_code) {
        $this->db->trans_start();
        $temp = $patient_icd_10_code;
        for ($i = 0; $i < count($temp); $i++) {
            $icd_10_code = $patient_icd_10_code[$i];
            $user_id = $this->session->userdata('id');
            $date_added = date("Y-m-d H:i:s");

            $check_visit_icd_10 = $this->check_patient_icd_10_code($visit_id, $icd_10_code);

            if (empty($check_visit_icd_10)) {
                $patient_diagnosis = array(
                    'icd10' => $icd_10_code,
                    'visit_id' => $visit_id,
                    'patient_id' => $patient_id,
                    'date_added' => $date_added,
                    'user_id' => $user_id
                );
                $this->db->insert('visit_icd10', $patient_diagnosis);
            } else {
                
            }
        } $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_patient_icd_10_code($visit_id, $icd_10_code) {


        $query = "select * from visit_icd10 where visit_id='$visit_id' and icd10='$icd_10_code' ";
        $query_1 = $this->db->query($query);
        foreach ($query_1->result() as $value) {
            $icd_10_codes = $value['icd10'];
            return $icd_10_codes;
        }
    }

    public function insert_patient_icd10_code($visit_id, $patient_id, $patient_icd_10_code) {
        $this->db->trans_start();
        $temp = $patient_icd_10_code;
        for ($i = 0; $i < count($temp); $i++) {
            $icd_10_code = $patient_icd_10_code[$i];
            $user_id = $this->session->userdata('id');
            $date_added = date("Y-m-d H:i:s");

            $patient_diagnosis = array(
                'icd10' => $icd_10_code,
                'visit_id' => $visit_id,
                'patient_id' => $patient_id,
                'date_added' => $date_added,
                'user_id' => $user_id
            );
            $this->db->insert('visit_icd10', $patient_diagnosis);
        } $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function update_patient_icd10_code($visit_id, $patient_id, $patient_icd_10_code) {

        $temp = $patient_icd_10_code;
        for ($i = 0; $i < count($temp); $i++) {
            $icd_10_code = $patient_icd_10_code[$i];
            $user_id = $this->session->userdata('id');
            $date_added = date("Y-m-d H:i:s");
            $sql = "Select * from visit_icd10 where visit_id='$visit_id'";
            $query = $this->db->query($sql);
            foreach ($query->result() as $value) {
                $this->db->trans_start();
                $id = $value->id;
          
                $patient_icd_10 = array(
                    'icd10' => $icd_10_code
                );
                $this->db->where('id', $id);
                $this->db->update('visit_icd10', $patient_icd_10);
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    return FALSE;
                } else {
                    return TRUE;
                }
            }
        }
    }

    public function update_working_diagnosis($visit_id, $patient_id, $working_diagnosis, $patient_icd_10_code) {
        $sql = "Select * from consultation where visit_id='$visit_id' and patient_id='$patient_id'";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {
            $this->db->trans_start();
            $consultation_id = $value->consultation_id;

            $working_daignosis_update = array(
                'working_diagnosis' => $working_diagnosis
            );
            $this->db->where('consultation_id', $consultation_id);
            $this->db->update('consultation', $working_daignosis_update);

            $this->patient_icd_10_code($patient_id, $visit_id, $patient_icd_10_code);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    public function insert_working_diagnosis($visit_id, $patient_id, $working_diagnosis, $patient_icd_10_code) {
        $this->db->trans_start();
        $working_diagnosis_insert = array(
            'working_diagnosis' => $working_diagnosis
        );
        $this->db->insert('consultation', $working_diagnosis_insert);
        $patient_icd10_code = $this->patient_icd_10_code($patient_id, $visit_id, $patient_icd_10_code);
        if (true) {
            
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function order_lab_tests($patient_id, $visit_id, $lab_tests, $patient_payment_id) {
        $this->db->trans_start();

        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $doctor_id = $this->session->userdata('id');
        $department = $this->session->userdata('type');
        foreach ($lab_tests as $value) {

            $lab_test_insert = array(
                'patient_id' => $patient_id,
                'visit_id' => $visit_id,
                'test' => $value,
                'member_id' => $member_id,
                'branch_id' => $branch_id,
                'doctor_id' => $doctor_id
            );

            $this->db->insert('lab_test_result', $lab_test_insert);

            $query_result = $this->db->query("Select * from test where test_id='$value'");
            foreach ($query_result->result() as $value) {
                $test_name = $value->test_name;
                $test_price = $value->test_price;

                $revenue_type = 'laboratory';

                $patient_viist_statement_insert = array(
                    'patient_id' => $patient_id,
                    'visit_id' => $visit_id,
                    'amount' => $test_price,
                    'description' => $test_name,
                    'member_id' => $member_id,
                    'branch_id' => $branch_id,
                    'revenue_type' => $revenue_type,
                    'department' => $department,
                    'user_id' => $doctor_id,
                    'charged' => 'Yes',
                    'paid' => 'No',
                    'patient_payment_id' => $patient_payment_id
                );
                $this->db->insert('patient_visit_statement', $patient_viist_statement_insert);
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function imaging_request_form($patient_id, $visit_id, $imaging_test, $major_complaints, $diagnosis, $special_instructions, $doctor_comments, $doctor_id, $member_id, $branch_id) {


        $this->db->trans_start();
        $data = array(
            'patient_id' => $patient_id,
            'visit_id' => $visit_id,
            'imaging_test' => $imaging_test,
            'major_complaints' => $major_complaints,
            'diagnosis' => $diagnosis,
            'instructions' => $special_instructions,
            'doctor_comments' => $doctor_comments,
            'user_id' => $doctor_id,
            'member_id' => $member_id,
            'branch_id' => $branch_id
        );
        $this->db->insert('imaging', $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function referral_request_form($patient_id, $visit_id, $referral_to, $major_complaints, $diagnosis, $special_instructions, $doctor_comments, $doctor_id, $member_id, $branch_id) {


        $this->db->trans_start();
        $data = array(
            'patient_id' => $patient_id,
            'visit_id' => $visit_id,
            'referred_doctor' => $referral_to,
            'major_complaint' => $major_complaints,
            'diagnosis' => $diagnosis,
            'special_instructions' => $special_instructions,
            'referring_doctor_comments' => $doctor_comments,
            'referring_doctor' => $doctor_id,
            'member_id' => $member_id,
            'branch_id' => $branch_id
        );
        $this->db->insert('referral', $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function sick_off_form($visit_id, $patient_id, $date_from, $date_to) {

        $user_id = $this->session->userdata('id');
        $this->db->trans_start();
        $data = array(
            'patient_id' => $patient_id,
            'visit_id' => $visit_id,
            'date_start' => $date_from,
            'date_end' => $date_to,
            'user_id' => $user_id
        );
        $this->db->insert('sick_offs', $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    public function patient_appointments($patient_id) {
        $sql = "SELECT * FROM appointment where patient_id='$patient_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_review_of_systems($patient_id, $visit_id) {
        $sql = "SELECT * FROM `review_of_systems` where visit_id='$visit_id' and patient_id='$patient_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_chief_complaints($patient_id, $visit_id) {
        $sql = "SELECT * FROM `consultation` where visit_id='$visit_id' and patient_id='$patient_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function order_commodity($commodity_name, $strength, $route, $frequency, $occurence, $duration, $visit_id, $patient_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $doctor_id = $this->session->userdata('id');
        $no = 'No';
        $yes = 'Yes';
        $today = date("Y-m-d H:i:s");
        $is_dispensed = 'Not Dispensed';
        $paid = 'Not Paid';
        // echo 'Commodity name' . $commodity_name . '<br> Strength ' . $strength . '<br> Route' . $route . '<br> Frequency' . $frequency . '<br> Occurrence' . $occurence . '<br> Duration' . $duration . '<br> Visit id' . $visit_id . '<br> Patient id ' . $patient_id;
        $this->db->trans_start();
        $query = $this->db->get_where('consultation', array('visit_id' => $visit_id), 1, 0);
        foreach ($query->result() as $value) {
            $consultation_id = $value->consultation_id;
            // echo 'Consultation id' . $consultation_id . '<br>';
            $data_insert = array(
                'commodity_name' => $commodity_name,
                'route' => $route,
                'strength' => $strength,
                'frequency' => $frequency,
                'occurence' => $occurence,
                'duration' => $duration,
                'visit_id' => $visit_id,
                'patient_id' => $patient_id,
                'member_id' => $member_id,
                'branch_id' => $branch_id,
                'user_id' => $doctor_id,
                'consultation_id' => $consultation_id,
                'billed' => $no,
                'is_dispensed' => $is_dispensed,
                'paid' => $paid,
                'date' => $today,
                'doctor_id' => $doctor_id
            );
            $this->db->insert('prescription', $data_insert);

            $last_insert_id = $this->db->insert_id();
            $prescription_tracker = "PRESC_" . $last_insert_id;
            $data_update = array(
                'prescription_tracker' => $prescription_tracker
            );
            $this->db->where('prescription_id', $last_insert_id);
            $this->db->update('prescription', $data_update);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

}
