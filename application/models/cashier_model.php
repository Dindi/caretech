<?php

class Cashier_model extends CI_Model {

    public function _construct() {
        parent::_construct();
    }

    public function total_package_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "select sum(amount-amount_dr) as amount_owed from patient_visit_statement inner join visit on visit.visit_id = patient_visit_statement.visit_id where DATE(visit.start) = DATE(NOW()) and revenue_type='package' and visit.member_id='$member_id' and visit.branch_id='$branch_id'";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function total_lab_service_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "select sum(amount-amount_dr) as amount_owed from patient_visit_statement inner join visit on visit.visit_id = patient_visit_statement.visit_id  where DATE(visit.start) = DATE(NOW()) and revenue_type='laboratory' and visit.member_id='$member_id' and visit.branch_id='$branch_id'";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function total_procedure_service_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "select sum(amount-amount_dr) as amount_owed from patient_visit_statement inner join visit on visit.visit_id = patient_visit_statement.visit_id where DATE(visit.start) = DATE(NOW()) and revenue_type='procedure' and visit.member_id='$member_id' and visit.branch_id='$branch_id'";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function total_pharmacy_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "select sum(amount-amount_dr) as amount_owed from patient_visit_statement inner join visit on visit.visit_id = patient_visit_statement.visit_id where DATE(visit.start) = DATE(NOW()) and revenue_type='medicine' and visit.member_id='$member_id' and visit.branch_id='$branch_id'";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function total_regular_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "select sum(amount-amount_dr) as amount_owed from patient_visit_statement inner join visit on visit.visit_id = patient_visit_statement.visit_id  where DATE(visit.start) = DATE(NOW()) and visit.member_id='$member_id' and visit.branch_id='$branch_id'";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function total_walkin_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $sql = "select sum(amount-amount_dr) as amount_owed from patient_visit_statement inner join visit on visit.visit_id = patient_visit_statement.visit_id  where DATE(visit.start) = DATE(NOW())  and visit.member_id='$member_id' and visit.branch_id='$branch_id' ";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function regular_patient_payments() {//Cashier Payments
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,charge_followup,patient.patient_id, 
            visit.visit_id, sum(patient_visit_statement.amount-patient_visit_statement.amount_dr) as amount_owed FROM patient_visit_statement 
inner join patient on patient.patient_id = patient_visit_statement.patient_id 
inner join visit on visit.visit_id = patient_visit_statement.visit_id where DATE(visit.start) = DATE(NOW())
and patient_visit_statement.member_id='$member_id' and patient_visit_statement.charged='Yes' and visit.type='Regular' and visit.pay_at_the_end='no' and patient_visit_statement.revenue_type='package' and patient_visit_statement.paid='No' and (patient_visit_statement.amount-patient_visit_statement.amount_dr)  >0 and patient_visit_statement.branch_id='$branch_id' group by visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function regular_patient_payment_followup() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,charge_followup,patient.patient_id, 
            visit.visit_id, sum(patient_visit_statement.amount-patient_visit_statement.amount_dr) as amount_owed FROM patient_visit_statement 
inner join patient on patient.patient_id = patient_visit_statement.patient_id 
inner join visit on visit.visit_id = patient_visit_statement.visit_id where DATE(visit.start) = DATE(NOW())
and patient_visit_statement.member_id='$member_id' and patient_visit_statement.charged='Yes' and visit.type='Regular'  and patient_visit_statement.revenue_type='package' and patient_visit_statement.paid='No' and (patient_visit_statement.amount-patient_visit_statement.amount_dr)  >0 and patient_visit_statement.branch_id='$branch_id' and visit.charge_followup='Yes' group by visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function regular_patient_payments_details($visit_id, $patient_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,charge_followup,patient.patient_id,patient_visit_statement.patient_payment_id, 
            visit.visit_id, (patient_visit_statement.amount-patient_visit_statement.amount_dr) as amount_owed ,patient_visit_statement.visit_id,patient_visit_statement.patient_id,
            patient_visit_statement.patient_visit_statement_id, patient_visit_statement.amount as amount_charged, patient_visit_statement.amount_dr as amount_paid,patient_visit_statement.quantity,
            patient_visit_statement.description,patient_visit_statement.payment_code,patient_visit_statement.payment_method FROM patient_visit_statement 
            inner join patient on patient.patient_id = patient_visit_statement.patient_id 
            inner join visit on visit.visit_id = patient_visit_statement.visit_id where DATE(visit.start) = DATE(NOW())
            and patient_visit_statement.member_id='$member_id' and patient_visit_statement.charged='Yes' and visit.type='Regular'  and patient_visit_statement.paid='No'  and patient_visit_statement.revenue_type='package'  "
                . "  and patient_visit_statement.branch_id='$branch_id' and patient_visit_statement.visit_id='$visit_id' and patient_visit_statement.patient_id='$patient_id'";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function walkin_patient_nurse_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "Select  patient_visit_statement_id , visit.visit_id , patient.patient_id,"
                . " concat(title,' ',f_name,' ',' ',s_name,' ',other_name) as patient_name  from patient_visit_statement "
                . "inner join procedure_result on procedure_result.visit_id = patient_visit_statement.visit_id"
                . " inner join visit on visit.visit_id = patient_visit_statement.visit_id  "
                . "inner join patient on patient.patient_id = visit.patient_id"
                . " where DATE(start) = DATE(NOW()) and visit_type='walk_in_nurse' and amount-amount_dr > 0 GROUP BY visit.visit_id ";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function walkin_patient_nurse_payments_details($walkin_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $query = "Select  patient_visit_statement_id as walkin_patient_visit_statement_id ,patient_visit_statement.quantity, visit.visit_id as walkin_id , patient.patient_id,"
                . " concat(title,' ',f_name,' ',' ',s_name,' ',other_name) as patient_name , patient_visit_statement.description,"
                . " patient_visit_statement.quantity , patient_visit_statement.amount as amount_charged ,patient_visit_statement.amount_dr as amount_paid,"
                . "patient_visit_statement.payment_code,patient_visit_statement.payment_method, (amount-amount_dr) as balance_remaining "
                . " from patient_visit_statement inner join procedure_result on procedure_result.visit_id = patient_visit_statement.visit_id "
                . "inner join visit on visit.visit_id = patient_visit_statement.visit_id "
                . " inner join patient on patient.patient_id = visit.patient_id "
                . "where DATE(start) = DATE(NOW()) and visit_type='walk_in_nurse' and patient_visit_statement.visit_id='$walkin_id' and amount-amount_dr > 0"
                . " GROUP BY patient_visit_statement.patient_visit_statement_id ";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function walkin_patient_lab_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "Select  patient_visit_statement_id , visit.visit_id , patient.patient_id, concat(title,' ',f_name,' ',' ',s_name,' ',other_name) as patient_name "
                . " from patient_visit_statement inner join lab_test_result on lab_test_result.visit_id = patient_visit_statement.visit_id "
                . "inner join visit on visit.visit_id = patient_visit_statement.visit_id  inner join patient on patient.patient_id = visit.patient_id "
                . "where DATE(visit.start) = DATE(NOW()) and visit.visit_type='walk_in_lab' and amount-amount_dr > 0 GROUP BY visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function walkin_patient_lab_payments_details($walkin_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $query = "Select  patient_visit_statement_id as walkin_patient_visit_statement_id ,lab_test_result.lab_test_id, visit.visit_id as walkin_id , patient.patient_id,"
                . " concat(title,' ',f_name,' ',' ',s_name,' ',other_name) as patient_name , patient_visit_statement.description,"
                . " patient_visit_statement.quantity , patient_visit_statement.amount as amount_charged ,patient_visit_statement.amount_dr as amount_paid,"
                . "patient_visit_statement.payment_code,patient_visit_statement.payment_method, (amount-amount_dr) as balance_remaining "
                . " from patient_visit_statement inner join lab_test_result on lab_test_result.visit_id = patient_visit_statement.visit_id "
                . "inner join visit on visit.visit_id = patient_visit_statement.visit_id "
                . " inner join patient on patient.patient_id = visit.patient_id "
                . "where DATE(start) = DATE(NOW()) and visit.visit_type='walk_in_lab' and patient_visit_statement.visit_id='$walkin_id' and amount-amount_dr > 0"
                . " GROUP BY patient_visit_statement.patient_visit_statement_id";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function walkin_patient_pharmacy_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "Select  patient_visit_statement_id , visit.visit_id , patient.patient_id,"
                . " concat(title,' ',f_name,' ',' ',s_name,' ',other_name) as patient_name  from patient_visit_statement "
                . "inner join procedure_result on procedure_result.visit_id = patient_visit_statement.visit_id"
                . " inner join visit on visit.visit_id = patient_visit_statement.visit_id  "
                . "inner join patient on patient.patient_id = visit.patient_id"
                . " where DATE(start) = DATE(NOW()) and visit_type='walk_in_nurse' and amount-amount_dr > 0 GROUP BY visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function walkin_patient_pharmacy_payments_details($walkin_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');

        $query = "Select  patient_visit_statement_id as walkin_patient_visit_statement_id , visit.visit_id as walkin_id , patient.patient_id,"
                . " concat(title,' ',f_name,' ',' ',s_name,' ',other_name) as patient_name , patient_visit_statement.description,"
                . " patient_visit_statement.quantity , patient_visit_statement.amount as amount_charged ,patient_visit_statement.amount_dr as amount_paid,"
                . "patient_visit_statement.payment_code,patient_visit_statement.payment_method, (amount-amount_dr) as balance_remaining "
                . " from patient_visit_statement inner join procedure_result on procedure_result.visit_id = patient_visit_statement.visit_id "
                . "inner join visit on visit.visit_id = patient_visit_statement.visit_id "
                . " inner join patient on patient.patient_id = visit.patient_id "
                . "where DATE(start) = DATE(NOW()) and visit_type='walk_in_nurse' and patient_visit_statement.visit_id='$walkin_id' and amount-amount_dr > 0"
                . " GROUP BY patient_visit_statement.patient_visit_statement_id";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function pay_at_the_end() {//Cashier Payments
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,charge_followup, patient.patient_id,
 visit.visit_id, sum(patient_visit_statement.amount-patient_visit_statement.amount_dr) as amount_owed FROM patient_visit_statement
inner join patient on patient.patient_id = patient_visit_statement.patient_id
inner join visit on visit.visit_id = patient_visit_statement.visit_id where DATE(visit.start) = DATE(NOW())
and patient_visit_statement.member_id = '$member_id' and patient_visit_statement.charged = 'Yes' and visit.type='Regular'  and visit.pay_at_the_end = 'yes' and patient_visit_statement.paid = 'No' and (patient_visit_statement.amount-patient_visit_statement.amount_dr) >0 and patient_visit_statement.branch_id = '$branch_id' group by visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function pay_at_the_end_details($visit_id, $patient_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name, patient.patient_id,charge_followup, patient_visit_statement.patient_payment_id,
 visit.visit_id, (patient_visit_statement.amount-patient_visit_statement.amount_dr) as amount_owed, patient_visit_statement.visit_id, patient_visit_statement.patient_id, patient_visit_statement.patient_visit_statement_id, patient_visit_statement.amount as amount_charged, patient_visit_statement.amount_dr as amount_paid, patient_visit_statement.quantity, patient_visit_statement.description, patient_visit_statement.payment_code, patient_visit_statement.payment_method FROM patient_visit_statement
inner join patient on patient.patient_id = patient_visit_statement.patient_id
inner join visit on visit.visit_id = patient_visit_statement.visit_id where (patient_visit_statement.amount-patient_visit_statement.amount_dr) > 0 and DATE(patient_visit_statement.date_added) = DATE(NOW())
and patient_visit_statement.member_id = '$member_id' and patient_visit_statement.charged = 'Yes' and visit.type='Regular'  and patient_visit_statement.paid = 'No' and visit.pay_at_the_end = 'yes' and patient_visit_statement.branch_id = '$branch_id' and patient_visit_statement.visit_id = '$visit_id' and patient_visit_statement.patient_id = '$patient_id'";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function get_walkin_patient_name($walkin_id) {
        $query = "Select CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name from patient inner join visit on visit.patient_id = patient.patient_id  where visit.visit_id = '$walkin_id'";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function get_patient_name($patient_id, $visit_id) {
        $query = "Select CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name from patient where patient.patient_id = '$patient_id'";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function lab_service_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,charge_followup, patient.patient_id,
 visit.visit_id, sum(patient_visit_statement.amount-patient_visit_statement.amount_dr) as amount_owed FROM patient_visit_statement
inner join patient on patient.patient_id = patient_visit_statement.patient_id
inner join visit on visit.visit_id = patient_visit_statement.visit_id where DATE(visit.start) = DATE(NOW())
and patient_visit_statement.member_id = '$member_id' and patient_visit_statement.branch_id = '$branch_id' and visit.type='Regular'  and patient_visit_statement.charged = 'Yes' and patient_visit_statement.paid = 'No' and visit.pay_at_the_end='no' and patient_visit_statement.revenue_type = 'laboratory' group by visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function lab_service_followup_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,charge_followup, patient.patient_id,
 visit.visit_id, sum(patient_visit_statement.amount-patient_visit_statement.amount_dr) as amount_owed FROM patient_visit_statement
inner join patient on patient.patient_id = patient_visit_statement.patient_id
inner join visit on visit.visit_id = patient_visit_statement.visit_id where DATE(visit.start) = DATE(NOW())
and patient_visit_statement.member_id = '$member_id' and patient_visit_statement.branch_id = '$branch_id' and visit.type='Regular'   and visit.charge_followup='Yes' and patient_visit_statement.charged = 'Yes' and patient_visit_statement.paid = 'No' and patient_visit_statement.revenue_type = 'laboratory' group by visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function lab_service_payments_details($visit_id, $patient_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,patient.patient_id,charge_followup,patient_visit_statement.patient_payment_id, 
            visit.visit_id, (patient_visit_statement.amount-patient_visit_statement.amount_dr) as amount_owed ,patient_visit_statement.visit_id,patient_visit_statement.patient_id,
            patient_visit_statement.patient_visit_statement_id, patient_visit_statement.amount as amount_charged, patient_visit_statement.amount_dr as amount_paid,patient_visit_statement.quantity,
            patient_visit_statement.description,patient_visit_statement.payment_code,patient_visit_statement.payment_method FROM patient_visit_statement 
            inner join patient on patient.patient_id = patient_visit_statement.patient_id 
            inner join visit on visit.visit_id = patient_visit_statement.visit_id where DATE(visit.start) = DATE(NOW())
            and patient_visit_statement.member_id='$member_id' and patient_visit_statement.charged='Yes' and patient_visit_statement.paid='No'  and patient_visit_statement.revenue_type='Laboratory'  "
                . "  and patient_visit_statement.branch_id='$branch_id' and visit.type='Regular'   and patient_visit_statement.visit_id='$visit_id' and patient_visit_statement.patient_id='$patient_id'";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function lab_service_make_payment() {
        $temp = $this->input->post('patient_id');
        $amount_charged = $this->input->post('amount_charged');
        $amount_paid = $this->input->post('amount_paid');
        $quantity = $this->input->post('quantity');
        $patient_visit_statement_id = $this->input->post('patient_visit_statement_id');
        $patient_id = $this->input->post('patient_id');
        $visit_id = $this->input->post('visit_id');
        $patient_payment_id = $this->input->post('patient_payment_id');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');
        $amount_owed = $this->input->post('amount_owed');
        $amount_paid_td = $this->input->post('amount_paid_td');
        $paid_status = 'paid';
        $yes = 'yes';
        for ($i = 0; $i < count($temp); $i++) {
            $paid = 'paid';
            $check = 1;
            $payment_id = 1;
            $zero = 0;
            for ($i = 0; $i < count($temp); $i++) {
                if ($amount_owed[$i] === $zero) {
                    $total_amount_paid = $amount_charged[$i];
                } else {
                    $total_amount_paid = $amount_paid[$i] + $amount_paid_td[$i];
                }
                if (empty($patient_id[$i])) {
                    
                } else {

                    $this->db->trans_start();

                    $visit_id_1 = $visit_id[$i];
                    $check_receipt = $this->check_if_receipt_no_exist($visit_id_1);
                    if (empty($check_receipt)) {
                        $generate_receipt = $this->generate_receipt();
                        $receipt_numbers = substr($generate_receipt, 5);
                        $data = array(
                            'paid' => $paid,
                            'cost_cr' => $amount_paid[$i],
                            'receipt_no' => $generate_receipt,
                            'receipt_numeric_no' => $receipt_numbers
                        );

                        $this->db->where('visit_id', $visit_id[$i]);
                        $this->db->where('patient_id', $patient_id[$i]);
                        $this->db->update('patient_payments', $data);
                    } else {
                        $check_receipt = $check_receipt;
                        $receipt_numerical_no = substr($check_receipt, 5);
                        $data = array(
                            'paid' => $paid,
                            'cost_cr' => $amount_paid[$i],
                            'receipt_no' => $check_receipt,
                            'receipt_numeric_no' => $receipt_numerical_no
                        );

                        $this->db->where('visit_id', $visit_id[$i]);
                        $this->db->where('patient_id', $patient_id[$i]);
                        $this->db->update('patient_payments', $data);
                    }



                    $yes = 'Yes';

                    $data4 = array(
                        'charged' => $yes,
                        'payment_code' => $payment_code[$i],
                        'payment_method' => $payment_method[$i],
                        'amount_dr' => $total_amount_paid,
                        'payment_code' => $payment_code[$i]
                    );


                    $this->db->where('patient_visit_statement_id', $patient_visit_statement_id[$i]);
                    $this->db->update('patient_visit_statement', $data4);



                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE) {
// generate an error... or use the log_message() function to log your error
                    }
                }
            }
        }
    }

    public function pharmacy_service_payments_details($visit_id, $patient_id) {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,patient.patient_id,charge_followup,patient_visit_statement.patient_payment_id, 
            visit.visit_id, (patient_visit_statement.amount+patient_visit_statement.service_amount-patient_visit_statement.amount_dr) as amount_owed ,patient_visit_statement.visit_id,patient_visit_statement.patient_id,
            patient_visit_statement.patient_visit_statement_id, patient_visit_statement.amount+patient_visit_statement.service_amount as amount_charged, patient_visit_statement.amount_dr as amount_paid,patient_visit_statement.quantity,
            patient_visit_statement.description,patient_visit_statement.prescription_tracker,patient_visit_statement.payment_code,patient_visit_statement.payment_method FROM patient_visit_statement 
            inner join patient on patient.patient_id = patient_visit_statement.patient_id 
            inner join visit on visit.visit_id = patient_visit_statement.visit_id where DATE(patient_visit_statement.date_added) = DATE(NOW())
            and patient_visit_statement.member_id='$member_id' and patient_visit_statement.charged='Yes' and patient_visit_statement.paid='No'  and patient_visit_statement.revenue_type='medicine'  "
                . "  and patient_visit_statement.branch_id='$branch_id' and patient_visit_statement.visit_id='$visit_id' and patient_visit_statement.patient_id='$patient_id'";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function pharmacy_service_make_payment() {
        $temp = $this->input->post('patient_id');
        $amount_charged = $this->input->post('amount_charged');
        $amount_paid = $this->input->post('amount_paid');
        $quantity = $this->input->post('quantity');
        $patient_visit_statement_id = $this->input->post('patient_visit_statement_id');
        $patient_id = $this->input->post('patient_id');
        $visit_id = $this->input->post('visit_id');
        $patient_payment_id = $this->input->post('patient_payment_id');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');
        $amount_owed = $this->input->post('amount_owed');
        $amount_paid_td = $this->input->post('amount_paid_td');
        $prescription_tracker = $this->input->post('payment_prescription_tracker');
        $paid = "Paid";

        $this->db->trans_start();


        $paid_status = 'paid';
        $yes = 'yes';
        for ($i = 0; $i < count($temp); $i++) {


            $paid = 'paid';
            $check = 1;
            $payment_id = 1;
            $zero = 0;
            for ($i = 0; $i < count($temp); $i++) {
                if ($amount_owed[$i] === $zero) {
                    $total_amount_paid = $amount_charged[$i];
                } else {
                    $total_amount_paid = $amount_paid[$i] + $amount_paid_td[$i];
                }
                if (empty($patient_id[$i])) {
                    
                } else {
                    $yes = "Yes";
                    $query = $this->db->get_where('prescription', array('prescription_tracker' => $prescription_tracker[$i]));
                    foreach ($query->result() as $value) {
                        $prescription_id = $value->prescription_id;
                        $data_prescription = array(
                            'paid' => $paid
                        );
                        $this->db->where('prescription_id', $prescription_id);
                        $this->db->update('prescription', $data_prescription);
                    }

                    $visit_id_1 = $visit_id[$i];
                    $check_receipt = $this->check_if_receipt_no_exist($visit_id_1);
                    if (empty($check_receipt)) {
                        $generate_receipt = $this->generate_receipt();
                        $receipt_numbers = substr($generate_receipt, 5);
                        $data = array(
                            'paid' => $paid,
                            'cost_cr' => $amount_paid[$i],
                            'receipt_no' => $generate_receipt,
                            'receipt_numeric_no' => $receipt_numbers
                        );

                        $this->db->where('visit_id', $visit_id[$i]);
                        $this->db->where('patient_id', $patient_id[$i]);
                        $this->db->update('patient_payments', $data);
                    } else {
                        $check_receipt = $check_receipt;
                        $receipt_numerical_no = substr($check_receipt, 5);
                        $data = array(
                            'paid' => $paid,
                            'cost_cr' => $amount_paid[$i],
                            'receipt_no' => $check_receipt,
                            'receipt_numeric_no' => $receipt_numerical_no
                        );

                        $this->db->where('visit_id', $visit_id[$i]);
                        $this->db->where('patient_id', $patient_id[$i]);
                        $this->db->update('patient_payments', $data);
                    }



                    $yes = 'Yes';

                    $data4 = array(
                        'charged' => $yes,
                        'payment_code' => $payment_code[$i],
                        'payment_method' => $payment_method[$i],
                        'amount_dr' => $total_amount_paid,
                        'payment_code' => $payment_code[$i],
                        'paid' => $yes
                    );


                    $this->db->where('patient_visit_statement_id', $patient_visit_statement_id[$i]);
                    $this->db->update('patient_visit_statement', $data4);



                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE) {
// generate an error... or use the log_message() function to log your error
                    }
                }
            }
        }
    }

    public function pharmacy_service_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "select CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,charge_followup, patient.patient_id,
 visit.visit_id, sum(patient_visit_statement.amount+patient_visit_statement.service_amount-patient_visit_statement.amount_dr) as amount_owed from patient_visit_statement 
 inner join visit on visit.visit_id = patient_visit_statement.visit_id inner join patient on patient.patient_id = patient_visit_statement.patient_id 
 where DATE(patient_visit_statement.date_added) = DATE(NOW()) and patient_visit_statement.charged = 'Yes' and patient_visit_statement.paid = 'No' 
 and patient_visit_statement.revenue_type='medicine' and patient_visit_statement.member_id='$member_id' and patient_visit_statement.branch_id='$branch_id' and (patient_visit_statement.amount+patient_visit_statement.service_amount-patient_visit_statement.amount_dr) >0  group by patient_visit_statement.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function pharmacy_service_followup_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "select CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,charge_followup, patient.patient_id,
 visit.visit_id, sum(patient_visit_statement.amount+patient_visit_statement.service_amount-patient_visit_statement.amount_dr) as amount_owed from patient_visit_statement 
 inner join visit on visit.visit_id = patient_visit_statement.visit_id inner join patient on patient.patient_id = patient_visit_statement.patient_id 
 where DATE(patient_visit_statement.date_added) = DATE(NOW()) and patient_visit_statement.charged = 'Yes' and patient_visit_statement.paid = 'No' 
 and patient_visit_statement.revenue_type='medicine' and patient_visit_statement.member_id='$member_id' and visit.charge_followup='Yes' and patient_visit_statement.branch_id='$branch_id' and (patient_visit_statement.amount+patient_visit_statement.service_amount-patient_visit_statement.amount_dr) >0  group by patient_visit_statement.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function procedure_service_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,charge_followup, patient.patient_id,
 visit.visit_id, sum(patient_visit_statement.amount-patient_visit_statement.amount_dr) as amount_owed FROM caretech.patient_visit_statement
inner join patient on patient.patient_id = patient_visit_statement.patient_id
inner join visit on visit.visit_id = patient_visit_statement.visit_id where DATE(patient_visit_statement.date_added) = DATE(NOW())
and patient_visit_statement.member_id = '$member_id' and patient_visit_statement.branch_id = '$branch_id' and patient_visit_statement.revenue_type = 'procedure' and patient_visit_statement.charged = 'Yes' and patient_visit_statement.paid = 'No' group by visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function pharmacy_payments() {//Nurse
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "select CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,charge_followup, visit.pay_at_the_end,
 visit.visit_id, patient.patient_id, patient_visit_statement.amount - patient_visit_statement.amount_dr as total_payment,
 patient_visit_statement.amount as cost_cr, patient_visit_statement.amount_dr as cost_dr
from patient_visit_statement inner join patient on patient.patient_id = patient_visit_statement.patient_id
inner join visit on visit.visit_id = patient_visit_statement.visit_id where patient_visit_statement.revenue_type = 'Medicine'
and patient_visit_statement.amount - patient_visit_statement.amount_dr > 0
and DATE(patient_visit_statement.date_added) = DATE(NOW()) and patient_visit_statement.branch_id = '$branch_id' and patient_visit_statement.member_id = '$member_id' group by visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function package_payments() {//Cashier Payments
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,charge_followup, patient_payments.paid, visit.pay_at_the_end,
 visit.visit_id, patient.patient_id, patient_payments.cost - patient_payments.cost_cr as total_payment, patient_payments.cost_cr as cost_cr, patient_payments.cost as cost_dr FROM patient_payments
INNER JOIN patient ON patient.patient_id = patient_payments.patient_id
INNER JOIN visit ON visit.visit_id = patient_payments.visit_id
INNER JOIN packages ON packages.package_id = patient_payments.package_id
WHERE DATE(visit.start) = DATE(NOW()) AND
patient_payments.paid = 'not paid' and patient_payments.member_id = '$member_id' and patient_payments.branch_id = '$branch_id'";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function laboratory_payments() {//Lab Payments
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "select CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,charge_followup, visit.pay_at_the_end,
 visit.visit_id, patient.patient_id, patient_visit_statement.amount - patient_visit_statement.amount_dr as total_payment,
 patient_visit_statement.amount as cost_cr, patient_visit_statement.amount_dr as cost_dr
from patient_visit_statement inner join patient on patient.patient_id = patient_visit_statement.patient_id
inner join visit on visit.visit_id = patient_visit_statement.visit_id where patient_visit_statement.revenue_type = 'Laboratory'
and patient_visit_statement.amount - patient_visit_statement.amount_dr > 0
and DATE(patient_visit_statement.date_added) = DATE(NOW()) and patient_visit_statement.member_id = '$member_id' and patient_visit_statement.branch_id = '$branch_id' group by visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function total_payments() {//Total Payments
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "select patient_visit_statement.visit_id, patient_visit_statement.patient_id, sum(patient_visit_statement.amount) as amount, "
                . " sum(patient_visit_statement.amount_dr), concat(patient.title, ' :', patient.f_name, ' ', patient.s_name, ' ', patient.other_name) as patient_name "
                . "from patient_visit_statement inner join visit on visit.visit_id = patient_visit_statement.visit_id"
                . " inner join patient on patient.patient_id = patient_visit_statement.patient_id where visit.pay_at_the_end = 'Yes'"
                . " and DATE(patient_visit_statement.date_added) = DATE(NOW()) and patient_visit_statement.member_id = '$member_id' and patient_visit_statement.branch_id = '$branch_id' group by visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function patient_package_payments() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "select CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name, visit.pay_at_the_end,
 visit.visit_id, patient.patient_id, patient_visit_statement.amount - patient_visit_statement.amount_dr as total_payment,
 patient_visit_statement.amount as cost_cr, patient_visit_statement.amount_dr as cost_dr
from patient_visit_statement inner join patient on patient.patient_id = patient_visit_statement.patient_id
inner join visit on visit.visit_id = patient_visit_statement.visit_id where patient_visit_statement.revenue_type = 'Package'
and patient_visit_statement.amount - patient_visit_statement.amount_dr > 0 and patient_visit_statement.member_id = '$member_id' and patient_visit_statement.branch_id = '$branch_id'
and DATE(patient_visit_statement.date_added) = DATE(NOW()) group by visit.visit_id";
        $result = $this->db->query($query);
        return $result->result();
    }

//    public function pay_at_the_end_details() {
//        $member_id = $this->session->userdata('member_id');
//        $branch_id = $this->session->userdata('branch_id');
//        $query = "select CONCAT(patient.f_name, ' ', patient.s_name, ' ', patient.other_name ) as patient_name, visit.pay_at_the_end,
//                 visit.visit_id, patient.patient_id,patient_visit_statement.amount - patient_visit_statement.amount_dr as total_payment,
//                 patient_visit_statement.amount as cost_cr , patient_visit_statement.amount_dr as cost_dr 
//                  from patient_visit_statement inner join patient on patient.patient_id = patient_visit_statement.patient_id
//                  inner join visit on visit.visit_id = patient_visit_statement.visit_id where patient_visit_statement.revenue_type='Package'
//                  and visit.pay_at_the_end='Yes' and patient_visit_statement.member_id='$member_id' and patient_visit_statement.branch_id='$branch_id'
//                  and patient_visit_statement.date_added >= CURDATE()group by visit.visit_id";
//        $result = $this->db->query($query);
//        return $result->result();
//    }

    public function lab_walkin_payment_data() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $pharmacy_walkin_payment_data = "SELECT DISTINCT * FROM walkin 
                 INNER JOIN walkin_lab ON walkin_lab.patient_id = walkin.walkin_id 
                 INNER JOIN walkin_payments ON walkin_payments.patient_id = walkin.walkin_id
                 WHERE walkin_lab.date_added >= CURDATE() AND walkin_lab.walkin_lab_paid='No' and walkin_payments.member_id='$member_id' and walkin_payments.branch_id='$branch_id'  group by walkin.walkin_id";
        $result = $this->db->query($pharmacy_walkin_payment_data);
        return $result->result();
    }

    public function pharmacy_walkin_payment_data() {
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $pharmacy_walkin_payment_data = "SELECT DISTINCT * FROM walkin 
                 INNER JOIN walkin_pharm ON walkin_pharm.patient_id = walkin.walkin_id
                 WHERE walkin_pharm.date >= CURDATE() AND walkin_pharm.paid='no' and walkin_pharm.member_id='$member_id' and walkin_pharm.branch_id='$branch_id' group by walkin.walkin_id ";
        $result = $this->db->query($pharmacy_walkin_payment_data);
        return $result->result();
    }

    public function walkin_lab_details() {
        $walkin_id = $this->uri->segment(4);
        $walkin_lab_id = $this->uri->segment(3);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $walkin_pharmacy_details = "SELECT * FROM walkin_lab 
                 INNER JOIN walkin ON walkin.walkin_id=walkin_lab.patient_id 
                 INNER JOIN walkin_patient_visit_statement ON walkin.walkin_id=walkin_patient_visit_statement.walkin_patient_id
                 WHERE walkin.walkin_id='$walkin_id' AND walkin_lab.walkin_lab_id='$walkin_lab_id' and walkin_lab.member_id='$member_id' and walkin_lab.branch_id='$branch_id'";
        $result = $this->db->query($walkin_pharmacy_details);
        return $result->result();
    }

    public function walkin_pharmacy_details() {
        $walkin_id = $this->uri->segment(4);
        $walkin_pharm_id = $this->uri->segment(3);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $walkin_pharmacy_details = "SELECT DISTINCT * FROM walkin 
                 INNER JOIN walkin_pharm ON walkin_pharm.patient_id = walkin.walkin_id 
                 INNER JOIN walkin_patient_visit_statement on walkin_patient_visit_statement.walkin_patient_id = walkin_pharm.patient_id
                 WHERE walkin_pharm.date >= CURDATE() AND walkin.walkin_id='$walkin_id' AND walkin_pharm.walkin_pharm_id='$walkin_pharm_id'"
                . " and walkin_pharm.administered_status='not dispensed' and  walkin_patient_visit_statement.prescription_tracker != '' and walkin_patient_visit_statement.member_id='$member_id' and walkin_patient_visit_statement.branch_id='$branch_id'";
        $result = $this->db->query($walkin_pharmacy_details);
        return $result->result();
    }

    public function patient_payment_details() {//Patient Payment Details
        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT  DISTINCT (patient_payments.cost+patient_payments.pharm_payments+patient_payments.rad_payments) as total_payment ,visit.pay_at_the_end,
                     CONCAT(patient.f_name ,' ',patient.s_name,' ',patient.other_name ) as patient_name, visit.visit_id, patient.patient_id,
                     patient_payments.pharm_payments,patient_payments.pharm_payment,patient_payments.lab_payments,patient_payments.lab_payment,
                     patient_payments.cost as package_cost,patient_payments.paid as package_cost_paid,patient_payments.rad_payments, patient_payments.rad_payment FROM patient_payments "
                . "INNER JOIN patient ON patient.patient_id=patient_payments.patient_id "
                . "INNER JOIN visit ON visit.visit_id = patient_payments.visit_id"
                . " WHERE DATE(visit.start) = DATE(NOW()) and patient_visit_statement.amount - patient_visit_statement.amount_dr > 0 AND patient.patient_id='$patient_id'AND visit.visit_id='$visit_id' and visit.member_id='$member_id' and visit.branch_id='$branch_id'";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function patient_total_payment_price() {//Patient Payment Details
        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT DISTINCT SUM(patient_visit_statement.amount) as amount from patient_visit_statement ,visit.pay_at_the_end,
                     INNER JOIN patient ON patient_visit_statement.patient_id = patient.patient_id 
                     INNER JOIN visit ON visit.visit_id=patient_visit_statement.visit_id 
                     INNER JOIN patient_payments ON patient_payments.patient_payment_id = patient_visit_statement.patient_payment_id WHERE patient_visit_statement.charged='No'
                     and patient_visit_statement.amount - patient_visit_statement.amount_dr > 0 AND DATE(patient_visit_statement.date_added) = DATE(NOW()) AND patient_visit_statement.patient_id='$patient_id'"
                . "AND patient_visit_statement.visit_id='$visit_id' and patient_visit_statement.member_id='$member_id' and patient_visit_statement.branch_id='$branch_id'";
        $result = $this->db->query($query);
        foreach ($result->result() as $value) {
            $total_amount = $value->amount;

            return $total_amount;
        }
    }

    public function patient_package_payment_details() {//Patient Payment Details
        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT DISTINCT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,patient_visit_statement.payment_code,patient_visit_statement.payment_method,
                     visit.visit_id,patient_visit_statement.description, patient.patient_id,visit.pay_at_the_end, patient_visit_statement.patient_payment_id, patient_visit_statement.amount,patient_visit_statement.amount_dr,patient_visit_statement.amount-patient_visit_statement.amount_dr as amount_remaining,
                     patient_visit_statement.quantity,patient_visit_statement.charged ,patient_visit_statement.patient_visit_statement_id from patient_visit_statement 
                     INNER JOIN patient ON patient.patient_id= patient_visit_statement.patient_id 
                     INNER JOIN visit ON visit.visit_id = patient_visit_statement.visit_id 
                     WHERE DATE(patient_visit_statement.date_added) = DATE(NOW())  AND 
                     patient_visit_statement.patient_id='$patient_id' AND patient_visit_statement.visit_id='$visit_id' and patient_visit_statement.amount - patient_visit_statement.amount_dr > 0
                         AND patient_visit_statement.member_id='$member_id' and patient_visit_statement.branch_id='$branch_id' and
                      patient_visit_statement.revenue_type='package' ";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function patient_laboratory_payment_details() {//Patient Payment Details
        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT DISTINCT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,patient_visit_statement.payment_code,patient_visit_statement.payment_method,
                     visit.visit_id,patient_visit_statement.description, patient.patient_id,visit.pay_at_the_end, patient_visit_statement.patient_payment_id, patient_visit_statement.amount,patient_visit_statement.amount_dr,patient_visit_statement.amount-patient_visit_statement.amount_dr as amount_remaining,
                     patient_visit_statement.quantity,patient_visit_statement.charged,patient_visit_statement.patient_visit_statement_id from patient_visit_statement 
                     INNER JOIN patient ON patient.patient_id= patient_visit_statement.patient_id 
                     INNER JOIN visit ON visit.visit_id = patient_visit_statement.visit_id 
                     WHERE DATE(patient_visit_statement.date_added) = DATE(NOW())  AND patient_visit_statement.member_id='$member_id' and patient_visit_statement.branch_id='$branch_id' and 
                     patient_visit_statement.patient_id='$patient_id' AND patient_visit_statement.visit_id='$visit_id' and patient_visit_statement.amount - patient_visit_statement.amount_dr > 0 AND
                      patient_visit_statement.revenue_type='laboratory'";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function patient_total_payment_details() {//Patient Payment Details
        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT DISTINCT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,patient_visit_statement.payment_code,patient_visit_statement.payment_method,
                     visit.visit_id,patient_visit_statement.description, patient.patient_id,visit.pay_at_the_end, patient_visit_statement.patient_payment_id, patient_visit_statement.amount,patient_visit_statement.amount_dr,patient_visit_statement.amount-patient_visit_statement.amount_dr as amount_remaining,
                     patient_visit_statement.quantity,patient_visit_statement.charged,patient_visit_statement.patient_visit_statement_id from patient_visit_statement 
                     INNER JOIN patient ON patient.patient_id= patient_visit_statement.patient_id 
                     INNER JOIN visit ON visit.visit_id = patient_visit_statement.visit_id 
                     WHERE DATE(patient_visit_statement.date_added) = DATE(NOW())  AND patient_visit_statement.member_id='$member_id' and patient_visit_statement.branch_id='$branch_id'
                     patient_visit_statement.patient_id='$patient_id' AND patient_visit_statement.visit_id='$visit_id' and patient_visit_statement.amount - patient_visit_statement.amount_dr > 0 ";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function patient_radiology_payment_details() {//Patient Payment Details
        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT DISTINCT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,patient_visit_statement.payment_code,patient_visit_statement.payment_method,
                     visit.visit_id,patient_visit_statement.description, patient.patient_id,visit.pay_at_the_end, patient_visit_statement.patient_payment_id, patient_visit_statement.amount,patient_visit_statement.amount_dr,patient_visit_statement.amount-patient_visit_statement.amount_dr as amount_remaining,
                     patient_visit_statement.quantity,patient_visit_statement.charged,patient_visit_statement.patient_visit_statement_id from patient_visit_statement 
                     INNER JOIN patient ON patient.patient_id= patient_visit_statement.patient_id 
                     INNER JOIN visit ON visit.visit_id = patient_visit_statement.visit_id 
                     WHERE DATE(patient_visit_statement.date_added) = DATE(NOW())  AND patient_visit_statement.member_id='$member_id' and patient_visit_statement.branch_id='$branch_id'
                     patient_visit_statement.patient_id='$patient_id' AND patient_visit_statement.visit_id='$visit_id' and patient_visit_statement.amount - patient_visit_statement.amount_dr > 0 AND
                      patient_visit_statement.revenue_type='radiology'";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function patient_pharmacy_payment_details() {//Patient Payment Details
        $patient_id = $this->uri->segment(4);
        $visit_id = $this->uri->segment(3);
        $member_id = $this->session->userdata('member_id');
        $branch_id = $this->session->userdata('branch_id');
        $query = "SELECT DISTINCT CONCAT(patient.title,': ',patient.f_name,' ',patient.s_name,' ',patient.other_name ) as patient_name,patient_visit_statement.payment_code,patient_visit_statement.payment_method,
                     visit.visit_id,patient_visit_statement.description, patient.patient_id,visit.pay_at_the_end, patient_visit_statement.patient_payment_id, patient_visit_statement.amount,patient_visit_statement.amount_dr,patient_visit_statement.amount-patient_visit_statement.amount_dr as amount_remaining,
                     patient_visit_statement.quantity,patient_visit_statement.charged,patient_visit_statement.patient_visit_statement_id from patient_visit_statement 
                     INNER JOIN patient ON patient.patient_id= patient_visit_statement.patient_id 
                     INNER JOIN visit ON visit.visit_id = patient_visit_statement.visit_id 
                     WHERE DATE(patient_visit_statement.date_added) = DATE(NOW())  AND patient_visit_statement.member_id='$member_id' and patient_visit_statement.branch_id='$branch_id'
                     patient_visit_statement.patient_id='$patient_id' AND patient_visit_statement.visit_id='$visit_id' and patient_visit_statement.amount - patient_visit_statement.amount_dr > 0 AND
                      patient_visit_statement.revenue_type='medicine'";
        $result = $this->db->query($query);
        return $result->result();
    }

    public function regular_patient_make_payment() {
        $temp = $this->input->post('patient_id');
        $amount_charged = $this->input->post('amount_charged');
        $amount_paid = $this->input->post('amount_paid');
        $quantity = $this->input->post('quantity');
        $patient_visit_statement_id = $this->input->post('patient_visit_statement_id');
        $patient_id = $this->input->post('patient_id');
        $visit_id = $this->input->post('visit_id');
        $patient_payment_id = $this->input->post('patient_payment_id');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');
        $amount_owed = $this->input->post('amount_owed');
        $amount_paid_td = $this->input->post('amount_paid_td');
        $paid_status = 'paid';
        $yes = 'yes';
        for ($i = 0; $i < count($temp); $i++) {








            $paid = 'paid';
            $check = 1;
            $payment_id = 1;
            $zero = 0;
            for ($i = 0; $i < count($temp); $i++) {
                if ($amount_owed[$i] === $zero) {
                    $total_amount_paid = $amount_charged[$i];
                } else {
                    $total_amount_paid = $amount_paid[$i] + $amount_paid_td[$i];
                }
                if (empty($patient_id[$i])) {
                    
                } else {

                    $this->db->trans_start();

                    $visit_id_1 = $visit_id[$i];
                    $check_receipt = $this->check_if_receipt_no_exist($visit_id_1);
                    if (empty($check_receipt)) {
                        $generate_receipt = $this->generate_receipt();
                        $receipt_numbers = substr($generate_receipt, 5);
                        $data = array(
                            'paid' => $paid,
                            'cost_cr' => $amount_paid[$i],
                            'receipt_no' => $generate_receipt,
                            'receipt_numeric_no' => $receipt_numbers
                        );

                        $this->db->where('visit_id', $visit_id[$i]);
                        $this->db->where('patient_id', $patient_id[$i]);
                        $this->db->update('patient_payments', $data);
                    } else {
                        $check_receipt = $check_receipt;
                        $receipt_numerical_no = substr($check_receipt, 5);
                        $data = array(
                            'paid' => $paid,
                            'cost_cr' => $amount_paid[$i],
                            'receipt_no' => $check_receipt,
                            'receipt_numeric_no' => $receipt_numerical_no
                        );

                        $this->db->where('visit_id', $visit_id[$i]);
                        $this->db->where('patient_id', $patient_id[$i]);
                        $this->db->update('patient_payments', $data);
                    }



                    $yes = 'Yes';

                    $data4 = array(
                        'charged' => $yes,
                        'payment_code' => $payment_code[$i],
                        'payment_method' => $payment_method[$i],
                        'amount_dr' => $total_amount_paid,
                        'payment_code' => $payment_code[$i]
                    );


                    $this->db->where('patient_visit_statement_id', $patient_visit_statement_id[$i]);
                    $this->db->update('patient_visit_statement', $data4);



                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE) {
// generate an error... or use the log_message() function to log your error
                    }
                }
            }
        }
    }

    public function get_walkin_patient_amount_paid() {
        $temp = $this->input->post('walkin_patient_visit_statement_id');
        $walkin_id = $this->input->post('walkin_id');
        // $walkin_statement_id = $this->input->post('walkin_statement_id');


        $walkin_id = $this->input->post('walkin_id');
        $walkin_statement_id = $this->input->post('walkin_patient_visit_statement_id');
        $description = $this->input->post('description');
        $amount_charged = $this->input->post('amount_charged');
        $amount_paid = $this->input->post('amount_paid');
        $quantity = $this->input->post('quantity');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');
        $lab_test_id = $this->input->post('lab_test_id');
        $balance_remaining = $this->input->post('balance_remaining');



//        echo 'Function called ....';
        for ($i = 0; $i < count($temp); $i++) {
//           echo 'Walkin id : '.$walkin_id[$i].'<br> Walkin statement id : '.$walkin_statement_id[$i].'<br> Description : '.$amount_charged[$i].'<br> Amount paid : '.$amount_paid[$i].'<br> ';
//           echo 'DETAILS AFTER FUNCTION!!!!!';
            $query = $this->db->get_where('patient_visit_statement', array('patient_visit_statement_id' => $walkin_statement_id[$i]));
            foreach ($query->result() as $value) {
                $amount_dr = $value->amount_dr;
                // echo 'Amount Dr from function' . $amount_dr . '<br>';
                return $amount_dr;
            }
        }
    }

    public function walkin_pharm_make_payment() {
        $temp = $this->input->post('walkin_id');
        $walkin_id = $this->input->post('walkin_id');
        $walkin_patient_visit_statement_id = $this->input->post('walkin_patient_visit_statement_id');
        $description = $this->input->post('description');
        $amount_charged = $this->input->post('amount_charged');
        $amount_paid = $this->input->post('amount_paid');
        $quantity = $this->input->post('quantity');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');
        $payment_method = $this->input->post('walkin_pharmacy_payment_method');
        $payment_code = $this->input->post('walkin_pharmacy_payment_code');
        $paid_status = 'paid';
        $yes = 'yes';



        for ($i = 0; $i < count($temp); $i++) {

            if (empty($patient_id)) {
                
            } else {
                $this->db->trans_start();
                $limit = 0;
                $offset = 1;

                $query = $this->db->get_where('walkin_pharm', array('patient_id' => $walkin_id[$i]));
                foreach ($query->result() as $value) {
                    $walkin_pharm_id = $value->walkin_pharm_id;

                    $data = array(
                        'paid' => $yes
                    );


                    $this->db->where('walkin_pharm_id', $walkin_pharm_id);
                    $this->db->update('walkin_pharm', $data);
                }

                $get_amount_dr = $this->get_walkin_patient_amount_paid();
                if (empty($get_amount_dr)) {

                    $data1 = array(
                        'charged' => $yes,
                        'payment_method' => $payment_method[$i],
                        'payment_code' => $payment_code[$i],
                        'amount_dr' => $amount_paid[$i]
                    );
                    $this->db->where('walkin_patient_visit_statement_id', $walkin_patient_visit_statement_id[$i]);
                    $this->db->update('walkin_patient_visit_statement', $data1);
                } else {
                    $new_amount = $get_amount_dr + $amount_paid[$i];
                    $data1 = array(
                        'charged' => $yes,
                        'payment_method' => $payment_method[$i],
                        'payment_code' => $payment_code[$i],
                        'amount_dr' => $new_amount[$i]
                    );
                    $this->db->where('walkin_patient_visit_statement_id', $walkin_patient_visit_statement_id[$i]);
                    $this->db->update('walkin_patient_visit_statement', $data1);
                }



                $data2 = array(
                    'paid' => $paid_status
                );
                $this->db->where('walkin_id', $walkin_id[$i]);
                $this->db->update('walkin', $data2);

                $query = $this->db->get_where('walkin_total_patient', array('walkin_patient_id' => $walkin_id[$i]));
                foreach ($query->result() as $value) {
                    $walkin_total_patient_id = $value->walkin_total_patient_id;


                    $data3 = array(
                        'paid' => $yes
                    );
                    $this->db->where('	walkin_total_patient_id', $walkin_total_patient_id);
                    $this->db->update('walkin_total_patient', $data3);
                }



                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
// generate an error... or use the log_message() function to log your error
                }
            }
        }
    }

    public function walkin_lab_make_payment() {
        $temp = $this->input->post('walkin_id');
        $walkin_id = $this->input->post('walkin_id');
        $walkin_patient_visit_statement_id = $this->input->post('walkin_patient_visit_statement_id');
        $description = $this->input->post('description');
        $amount_charged = $this->input->post('amount_charged');
        $amount_paid = $this->input->post('amount_paid');
        $quantity = $this->input->post('quantity');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');
        $payment_method = $this->input->post('walkin_pharmacy_payment_method');
        $payment_code = $this->input->post('walkin_pharmacy_payment_code');
        $paid_status = 'paid';
        $yes = 'yes';



        for ($i = 0; $i < count($temp); $i++) {

            if (empty($patient_id)) {
                
            } else {
                $this->db->trans_start();
                $limit = 0;
                $offset = 1;

                $query = $this->db->get_where('walkin_lab', array('patient_id' => $walkin_id[$i]));
                foreach ($query->result() as $value) {
                    $walkin_pharm_id = $value->walkin_lab_id;

                    $data = array(
                        'paid' => $yes
                    );


                    $this->db->where('walkin_lab_id', $walkin_pharm_id);
                    $this->db->update('walkin_lab', $data);
                }
                $walkin_patient_visit_statement_id = $walkin_patient_visit_statement_id[$i];
                $get_amount_dr = $this->get_walkin_patient_amount_paid($walkin_patient_visit_statement_id);
                if (empty($get_amount_dr)) {

                    $data1 = array(
                        'charged' => $yes,
                        'payment_method' => $payment_method[$i],
                        'payment_code' => $payment_code[$i],
                        'amount_dr' => $amount_paid[$i]
                    );
                    $this->db->where('walkin_patient_visit_statement_id', $walkin_patient_visit_statement_id[$i]);
                    $this->db->update('walkin_patient_visit_statement', $data1);
                } else {
                    $new_amount = $get_amount_dr + $amount_paid[$i];
                    $data1 = array(
                        'charged' => $yes,
                        'payment_method' => $payment_method[$i],
                        'payment_code' => $payment_code[$i],
                        'amount_dr' => $new_amount[$i]
                    );
                    $this->db->where('walkin_patient_visit_statement_id', $walkin_patient_visit_statement_id[$i]);
                    $this->db->update('walkin_patient_visit_statement', $data1);
                }



                $data2 = array(
                    'paid' => $paid_status
                );
                $this->db->where('walkin_id', $walkin_id[$i]);
                $this->db->update('walkin', $data2);

                $query = $this->db->get_where('walkin_total_patient', array('walkin_patient_id' => $walkin_id[$i]));
                foreach ($query->result() as $value) {
                    $walkin_total_patient_id = $value->walkin_total_patient_id;


                    $data3 = array(
                        'paid' => $yes
                    );
                    $this->db->where('	walkin_total_patient_id', $walkin_total_patient_id);
                    $this->db->update('walkin_total_patient', $data3);
                }



                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
// generate an error... or use the log_message() function to log your error
                }
            }
        }
    }

    public function make_lab_payment() {

        $temp = $this->input->post('patient_name');

        $patient_name = $this->input->post('patient_name');

        $lab_payments = $this->input->post('lab_payments');
        $lab_payment = $this->input->post('lab_payment');

        $patient_id = $this->input->post('patient_id');
        $visit_id = $this->input->post('visit_id');
        $amount = $this->input->post('amount');
        $amount_remaining = $this->input->post('amount_remaining');
        $amount_paid = $this->input->post('amount_paid');
        $amount_dr = $this->input->post('amount_dr');

        $patient_visit_statement_id = $this->input->post('patient_visit_statement_id');
        $paid = 'paid';
        $check = 1;
        $payment_id = 1;

        for ($i = 0; $i < count($temp); $i++) {

            if ($amount[$i] === $amount_remaining[$i]) {
                $total_amount_paid = $amount_paid[$i];
            } else {
                $total_amount_paid = $amount_paid[$i] + $amount_dr[$i];
            }

            if (empty($patient_id[$i])) {
                
            } else {

                $this->db->trans_start();



                $visit_id_1 = $visit_id[$i];
                $check_receipt = $this->check_if_receipt_no_exist($visit_id_1);
                if (empty($check_receipt)) {
                    $generate_receipt = $this->generate_receipt();
                    $receipt_numbers = substr($generate_receipt, 5);
                    $data = array(
                        'lab_payment' => $paid,
                        'lab_payments_cr' => $lab_payments[$i],
                        'receipt_no' => $generate_receipt,
                        'receipt_numeric_no' => $receipt_numbers
                    );

                    $this->db->where('visit_id', $visit_id[$i]);
                    $this->db->where('patient_id', $patient_id[$i]);
                    $this->db->update('patient_payments', $data);



                    $data2 = array(
                        'paid' => $paid,
                        'check' => $check,
                        'payment_id' => $payment_id,
                    );
                    $this->db->where('visit_id', $visit_id[$i]);
                    $this->db->where('patient_id', $patient_id[$i]);
                    $this->db->update('lab_test_result', $data2);


                    $yes = 'Yes';

                    $data4 = array(
                        'charged' => $yes,
                        'payment_code' => $payment_code,
                        'payment_method' => $payment_method
                    );

// $this->db->where('patient_id', $patient_id[$i]);
                    $this->db->where('patient_visit_statement_id', $patient_visit_statement_id[$i]);
                    $this->db->update('patient_visit_statement', $data4);
                } else {
                    $check_receipt = $check_receipt;
                    $receipt_numerical_no = substr($check_receipt, 5);
                    $data = array(
                        'lab_payment' => $paid,
                        'lab_payments_cr' => $lab_payments[$i],
                        'receipt_no' => $check_receipt,
                        'receipt_numeric_no' => $receipt_numerical_no
                    );

                    $this->db->where('visit_id', $visit_id[$i]);
                    $this->db->where('patient_id', $patient_id[$i]);
                    $this->db->update('patient_payments', $data);



                    $data2 = array(
                        'paid' => $paid,
                        'check' => $check,
                        'payment_id' => $payment_id
                    );
                    $this->db->where('visit_id', $visit_id[$i]);
                    $this->db->where('patient_id', $patient_id[$i]);
                    $this->db->update('lab_test_result', $data2);


                    $yes = 'Yes';

                    $data4 = array(
                        'charged' => $yes,
                        'payment_code' => $payment_code,
                        'payment_method' => $payment_method[$i],
                        'amount_dr' => $total_amount_paid,
                        'payment_code' => $payment_code[$i]
                    );

//$this->db->where('patient_id', $patient_id[$i]);
                    $this->db->where('patient_visit_statement_id', $patient_visit_statement_id[$i]);
                    $this->db->update('patient_visit_statement', $data4);
                }


                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
// generate an error... or use the log_message() function to log your error
                }
            }
        }
    }

    public function make_package_payment() {

        $temp = $this->input->post('patient_name');

        $patient_name = $this->input->post('patient_name');
        $package_cost = $this->input->post('package_cost');
        $amount = $this->input->post('amount');
        $amount_paid = $this->input->post('amount_paid');
        $package_cost_paid = $this->input->post('package_cost_paid');

        $patient_id = $this->input->post('patient_id');
        $visit_id = $this->input->post('visit_id');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');
        $amount_remaining = $this->input->post('amount_remaining');
        $amount_dr = $this->input->post('amount_dr');
        $patient_visit_statement_id = $this->input->post('patient_visit_statement_id');
        $paid = 'paid';
        $check = 1;
        $payment_id = 1;
        for ($i = 0; $i < count($temp); $i++) {
            if ($amount[$i] === $amount_remaining[$i]) {
                $total_amount_paid = $amount_paid[$i];
            } else {
                $total_amount_paid = $amount_paid[$i] + $amount_dr[$i];
            }
            if (empty($patient_id[$i])) {
                
            } else {

                $this->db->trans_start();

                $visit_id_1 = $visit_id[$i];
                $check_receipt = $this->check_if_receipt_no_exist($visit_id_1);
                if (empty($check_receipt)) {
                    $generate_receipt = $this->generate_receipt();
                    $receipt_numbers = substr($generate_receipt, 5);
                    $data = array(
                        'paid' => $paid,
                        'cost_cr' => $amount[$i],
                        'receipt_no' => $generate_receipt,
                        'receipt_numeric_no' => $receipt_numbers
                    );

                    $this->db->where('visit_id', $visit_id[$i]);
                    $this->db->where('patient_id', $patient_id[$i]);
                    $this->db->update('patient_payments', $data);
                } else {
                    $check_receipt = $check_receipt;
                    $receipt_numerical_no = substr($check_receipt, 5);
                    $data = array(
                        'paid' => $paid,
                        'cost_cr' => $amount_paid[$i],
                        'receipt_no' => $check_receipt,
                        'receipt_numeric_no' => $receipt_numerical_no
                    );

                    $this->db->where('visit_id', $visit_id[$i]);
                    $this->db->where('patient_id', $patient_id[$i]);
                    $this->db->update('patient_payments', $data);
                }



                $yes = 'Yes';

                $data4 = array(
                    'charged' => $yes,
                    'payment_code' => $payment_code,
                    'payment_method' => $payment_method[$i],
                    'amount_dr' => $total_amount_paid,
                    'payment_code' => $payment_code[$i]
                );

// $this->db->where('patient_id', $patient_id[$i]);
                $this->db->where('patient_visit_statement_id', $patient_visit_statement_id[$i]);
                $this->db->update('patient_visit_statement', $data4);



                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
// generate an error... or use the log_message() function to log your error
                }
            }
        }
    }

    public function make_pharmacy_payment() {

        $temp = $this->input->post('patient_name');

        $patient_name = $this->input->post('patient_name');
// $package_cost = $this->input->post('package_cost');
// $package_cost_paid = $this->input->post('package_cost_paid');
//$rad_payments = $this->input->post('rad_payments');
//$rad_payment = $this->input->post('rad_payment');
        $lab_payments = $this->input->post('lab_payments');
        $lab_payment = $this->input->post('lab_payment');
        $pharm_payments = $this->input->post('pharm_payments');
        $pharm_payment = $this->input->post('pharm_payment');
        $amount = $this->input->post('amount');
        $patient_id = $this->input->post('patient_id');
        $visit_id = $this->input->post('visit_id');
        $amount_paid = $this->input->post('amount_paid');
        $amount_remaining = $this->input->post('amount_remaining');
        $amount_dr = $this->input->post('amount_dr');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');
        $patient_visit_statement_id = $this->input->post('patient_visit_statement_id');
        $paid = 'paid';
        $billed = 'Yes';
        $check = 1;
        $payment_id = 1;
        for ($i = 0; $i < count($temp); $i++) {
            if ($amount[$i] === $amount_remaining[$i]) {
                $total_amount_paid = $amount_paid[$i];
            } else {
                $total_amount_paid = $amount_paid[$i] + $amount_dr[$i];
            }
            if (empty($patient_id[$i])) {
                
            } else {

                $this->db->trans_start();

                echo 'Total Amount paid : ' . $total_amount_paid . '</br>';
                $data = array(
                    'pharm_payment' => $paid,
                    'pharm_payments_cr' => $pharm_payments[$i],
                );

                $this->db->where('visit_id', $visit_id[$i]);
                $this->db->where('patient_id', $patient_id[$i]);
                $this->db->update('patient_payments', $data);


                $data3 = array(
                    'paid' => $paid
                );
                $this->db->where('patient_id', $patient_id[$i]);
                $this->db->where('visit_id', $visit_id[$i]);
                $this->db->update('prescription', $data3);

                $yes = 'Yes';

                $data4 = array(
                    'charged' => $yes,
                    'payment_code' => $payment_code,
                    'payment_method' => $payment_method[$i],
                    'amount_dr' => $total_amount_paid,
                    'payment_code' => $payment_code[$i]
                );

// $this->db->where('patient_id', $patient_id[$i]);
                $this->db->where('patient_visit_statement_id', $patient_visit_statement_id[$i]);
                $this->db->update('patient_visit_statement', $data4);


                $query = $this->db->get_where('patient_visit_statement', array('patient_visit_statement_id' => $patient_visit_statement_id[$i]), 1, 0);
                foreach ($query->result() as $value) {
                    $prescription_tracker = $value->prescription_tracker;

                    $data5 = array(
                        'billed' => $billed,
                        'paid' => $paid
                    );

                    $this->db->where('prescription_tracker', $prescription_tracker);
                    $this->db->update('patient_visit_statement', $data5);
                }




                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
// generate an error... or use the log_message() function to log your error
                }
            }
        }
    }

    public function make_payment() {

        $temp = $this->input->post('patient_name');

        $patient_name = $this->input->post('patient_name');
        $package_cost = $this->input->post('package_cost');
        $package_cost_paid = $this->input->post('package_cost_paid');
        $rad_payments = $this->input->post('rad_payments');
        $rad_payment = $this->input->post('rad_payment');
        $lab_payments = $this->input->post('lab_payments');
        $lab_payment = $this->input->post('lab_payment');
        $pharm_payments = $this->input->post('pharm_payments');
        $pharm_payment = $this->input->post('pharm_payment');
        $total_payment = $this->input->post('total_payment');
        $patient_id = $this->input->post('patient_id');
        $visit_id = $this->input->post('visit_id');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');
        $amount = $this->input->post('amount');
        $amount_paid = $this->input->post('amount_paid');
        $amount_remaining = $this->input->post('amount_remaining');
        $patient_visit_statement_id = $this->input->post('patient_visit_statement_id');
        $paid = 'paid';
        $check = 1;
        $payment_id = 1;
        for ($i = 0; $i < count($temp); $i++) {

            if (empty($patient_id[$i])) {
                
            } else {

                $this->db->trans_start();

                $total_amount_paid = $amount_paid[$i] + $amount_paid[$i];
                $data = array(
                    'paid' => $paid,
                    'lab_payment' => $paid,
                    'rad_payment' => $paid,
                    'pharm_payment' => $paid,
                    'cost_cr' => $amount_paid[$i],
                    'pharm_payments_cr' => $pharm_payments[$i],
                    'lab_payments_cr' => $lab_payments[$i],
                    'rad_payments_cr' => $rad_payments[$i],
                    'payment_code' => $payment_code,
                    'payment_method' => $payment_method
                );

                $this->db->where('visit_id', $visit_id[$i]);
                $this->db->where('patient_id', $patient_id[$i]);
                $this->db->update('patient_payments', $data);

                $data1 = array(
                    'paid' => $paid,
                    'check' => $check,
                    'payment_id' => $payment_id
                );
                $this->db->where('visit_id', $visit_id[$i]);
                $this->db->where('patient_id', $patient_id[$i]);
                $this->db->update('rad_test_results', $data1);

                $data2 = array(
                    'paid' => $paid,
                    'check' => $check,
                    'payment_id' => $payment_id
                );
                $this->db->where('visit_id', $visit_id[$i]);
                $this->db->where('patient_id', $patient_id[$i]);
                $this->db->update('lab_test_result', $data2);

                $data3 = array(
                    'paid' => $paid
                );
                $this->db->where('patient_id', $patient_id[$i]);
                $this->db->where('visit_id', $visit_id[$i]);
                $this->db->update('prescription', $data3);

                $yes = 'Yes';

                $data4 = array(
                    'charged' => $yes,
                    'payment_code' => $payment_code,
                    'payment_method' => $payment_method,
                    'amount_dr' => $total_amount_paid
                );
//$this->db->where('patient_id', $patient_id[$i]);
                $this->db->where('patient_visit_statement_id', $patient_visit_statement_id[$i]);
                $this->db->update('patient_visit_statement', $data4);



                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
// generate an error... or use the log_message() function to log your error
                }
            }
        }
    }

    public function walkin_patient_nurse_make_payment() {
        $temp = $this->input->post('walkin_id');
        $walkin_id = $this->input->post('walkin_id');
        $walkin_statement_id = $this->input->post('walkin_statement_id');
        $description = $this->input->post('description');
        $amount_charged = $this->input->post('amount_charged');
        $amount_paid = $this->input->post('amount_paid');
        $quantity = $this->input->post('quantity');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');

        $paid_status = 'paid';
        $yes = 'yes';



        for ($i = 0; $i < count($temp); $i++) {

            if (empty($walkin_id)) {
                
            } else {
                $this->db->trans_start();




                $get_amount_dr = $this->get_walkin_patient_amount_paid();
                $walkin_id_r = $walkin_id[$i];

                $check_receipt = $this->get_walkin_receipt_no();
                if (empty($get_amount_dr)) {
                    if (empty($check_receipt)) {
                        $new_receipt_no = $this->generate_walkin_receipt();
                        $data1 = array(
                            'charged' => $yes,
                            'payment_method' => $payment_method[$i],
                            'payment_code' => $payment_code[$i],
                            'amount_dr' => $amount_paid[$i],
                            'receipt_no' => $new_receipt_no
                        );
                        $this->db->where('patient_visit_statement_id', $walkin_statement_id[$i]);
                        $this->db->update('patient_visit_statement', $data1);
                    } else {
                        $data1 = array(
                            'charged' => $yes,
                            'payment_method' => $payment_method[$i],
                            'payment_code' => $payment_code[$i],
                            'amount_dr' => $amount_paid[$i],
                            'receipt_no' => $check_receipt
                        );
                        $this->db->where('patient_visit_statement_id', $walkin_statement_id[$i]);
                        $this->db->update('patient_visit_statement', $data1);
                    }
                } else {
                    if (empty($check_receipt)) {
                        $new_amount = $get_amount_dr + $amount_paid[$i];
                        echo 'new amount' . $new_amount . '<br>';
                        $new_receipt_no = $this->generate_walkin_receipt();
                        $new_amount = $get_amount_dr + $amount_paid[$i];
                        echo 'new amount' . $new_amount . '<br>';
                        $data1 = array(
                            'charged' => $yes,
                            'payment_method' => $payment_method[$i],
                            'payment_code' => $payment_code[$i],
                            'amount_dr' => $new_amount,
                            'receipt_no' => $new_receipt_no
                        );
                        $this->db->where('patient_visit_statement_id', $walkin_statement_id[$i]);
                        $this->db->update('patient_visit_statement', $data1);
                    } else {
                        $new_amount = $get_amount_dr + $amount_paid[$i];
                        $data1 = array(
                            'charged' => $yes,
                            'payment_method' => $payment_method[$i],
                            'payment_code' => $payment_code[$i],
                            'amount_dr' => $new_amount[$i],
                            'receipt_no' => $check_receipt
                        );
                        $this->db->where('patient_visit_statement_id', $walkin_statement_id[$i]);
                        $this->db->update('patient_visit_statement', $data1);
                    }
                    echo 'walkin patient visit statement sucessfully updated' . '<br>';
                }



                $data2 = array(
                    'payment_status' => $paid_status
                );
                $this->db->where('visit_id', $walkin_id[$i]);
                $this->db->update('procedure_result', $data2);
                echo 'walkin successfully updated.' . '<br>';


                echo 'walkin total patient sucessfully updated' . '</br>';



                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
// generate an error... or use the log_message() function to log your error
                }
            }
        }
    }

    public function walkin_patient_lab_make_payment() {
        $temp = $this->input->post('walkin_patient_visit_statement_id');
        $walkin_id = $this->input->post('walkin_id');
        $walkin_statement_id = $this->input->post('walkin_patient_visit_statement_id');
        $description = $this->input->post('description');
        $amount_charged = $this->input->post('amount_charged');
        $amount_paid = $this->input->post('amount_paid');
        $quantity = $this->input->post('quantity');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');
        $lab_test_id = $this->input->post('lab_test_id');
        $balance_remaining = $this->input->post('balance_remaining');
        $paid_status = 'paid';
        $yes = 'yes';



        for ($i = 0; $i < count($temp); $i++) {

            if (empty($walkin_id)) {
                
            } else {
                $this->db->trans_start();




                $get_amount_dr = $this->get_walkin_patient_amount_paid();
                $walkin_id_r = $walkin_id[$i];
                echo 'amount paid : ' . $amount_paid[$i] . '<br>';
                echo 'Amount previously paid : ' . $get_amount_dr . '<br>';
                $check_receipt = $this->get_walkin_receipt_no();
//                echo 'Check receipt no : ' . $check_receipt . '<br> Amount Dr : ' . $get_amount_dr . '<br> Amount paid currently :  ' . $amount_paid[$i] . '<br>';
//                if (empty($get_amount_dr)) {
//                    if (empty($check_receipt)) {
//                        $new_receipt_no = $this->generate_walkin_receipt();
//                        $data1 = array(
//                            'charged' => $yes,
//                            'payment_method' => $payment_method[$i],
//                            'payment_code' => $payment_code[$i],
//                            'amount_dr' => $amount_paid[$i],
//                            'receipt_no' => $new_receipt_no
//                        );
//                        $this->db->where('patient_visit_statement_id', $walkin_statement_id[$i]);
//                        $this->db->update('patient_visit_statement', $data1);
//                    } else {
//                        $data1 = array(
//                            'charged' => $yes,
//                            'payment_method' => $payment_method[$i],
//                            'payment_code' => $payment_code[$i],
//                            'amount_dr' => $amount_paid[$i],
//                            'receipt_no' => $check_receipt
//                        );
//                        $this->db->where('patient_visit_statement_id', $walkin_statement_id[$i]);
//                        $this->db->update('patient_visit_statement', $data1);
//                    }
//                } else {
                if (empty($check_receipt)) {
                    echo 'NEW RECEIPT NO';
                    echo 'Amount Paid to date : ' . $amount_paid[$i].'<br>';
                    $new_amount = $amount_charged[$i] - $balance_remaining[$i] + $amount_paid[$i];
                    echo 'NEW AMOUNT : ' . $new_amount . '<br>';
                    echo 'Patient visit statement id : '.$walkin_statement_id[$i].'<br>';
                     $new_receipt_no = $this->generate_walkin_receipt();
                    echo 'new amount' . $new_amount . '<br>';
                    $data1 = array(
                        'charged' => $yes,
                        'payment_method' => $payment_method[$i],
                        'payment_code' => $payment_code[$i],
                        'amount_dr' => $new_amount,
                        'receipt_no' => $new_receipt_no
                    );
                    $this->db->where('patient_visit_statement_id', $walkin_statement_id[$i]);
                    $this->db->update('patient_visit_statement', $data1);
                } else {
                    echo 'OLD RECEIPT';
                    echo 'Amount Paid to date : ' . $amount_paid[$i].'<br>';
                    $new_amount = $amount_charged[$i] - $balance_remaining[$i] + $amount_paid[$i];
                    echo 'NEW AMOUNT : ' . $new_amount . '<br>';
                    echo 'Patient visit statement id : '.$walkin_statement_id[$i].'<br>';
                    $data2 = array(
                        'charged' => $yes,
                        'payment_method' => $payment_method[$i],
                        'payment_code' => $payment_code[$i],
                        'amount_dr' => $new_amount,
                        'receipt_no' => $check_receipt
                    );
                    $this->db->where('patient_visit_statement_id', $walkin_statement_id[$i]);
                    $this->db->update('patient_visit_statement', $data2);
                }
                echo 'walkin patient visit statement sucessfully updated' . '<br>';



                $check_full_payment = $amount_charged[$i] - $balance_remaining[$i] + $amount_paid[$i];
                echo 'Check full payment : ' . $check_full_payment . '<br>';
                if ($check_full_payment === $amount_charged[$i]) {


                    $data2 = array(
                        'paid' => $paid_status
                    );
                    $this->db->where('lab_test_id', $lab_test_id[$i]);
                    $this->db->update('lab_test_result', $data2);
                    echo 'WALKIN LAB PAYMENT UPDATED SUCCESFULLY .....' . '<br>';
                } else {
                    
                }

                echo 'walkin total patient sucessfully updated' . '</br>';



                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
// generate an error... or use the log_message() function to log your error
                }
            }
        }
    }

    public function walkin_patient_pharmacy_make_payment() {
        $temp = $this->input->post('walkin_id');
        $walkin_id = $this->input->post('walkin_id');
        $walkin_statement_id = $this->input->post('walkin_statement_id');
        $description = $this->input->post('description');
        $amount_charged = $this->input->post('amount_charged');
        $amount_paid = $this->input->post('amount_paid');
        $quantity = $this->input->post('quantity');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');
        $paid_status = 'paid';
        $yes = 'yes';

        echo 'WALKIN PHARM UPDATE PAYMENT' . '<br>';

        for ($i = 0; $i < count($temp); $i++) {
            echo 'Walkin statement id' . $walkin_statement_id[$i] . '<br>';
            if (empty($walkin_id)) {
                
            } else {
                $this->db->trans_start();


                $query = $this->db->get_where('walkin_pharm', array('patient_id' => $walkin_id[$i]));
                foreach ($query->result() as $value) {
                    $walkin_pharm_id = $value->walkin_pharm_id;
                    echo 'walkin pharm id' . $walkin_pharm_id . '<br>';
                    $data = array(
                        'paid' => $yes
                    );


                    $this->db->where('walkin_pharm_id', $walkin_pharm_id);
                    $this->db->update('walkin_pharm', $data);
                }
                echo 'walkin pharm' . '<br>';

                echo 'walkin patient visit statement id' . $walkin_statement_id[$i] . '<br>';
                $get_amount_dr = $this->get_walkin_patient_amount_paid();
                $walkin_id_r = $walkin_id[$i];
                echo 'amount paid' . $amount_paid[$i] . '<br>';
                echo 'Amount previously paid' . $get_amount_dr . '<br>';
                $check_receipt = $this->get_walkin_receipt_no();
                if (empty($get_amount_dr)) {
                    if (empty($check_receipt)) {
                        $new_receipt_no = $this->generate_walkin_receipt();
                        $data1 = array(
                            'charged' => $yes,
                            'payment_method' => $payment_method[$i],
                            'payment_code' => $payment_code[$i],
                            'amount_dr' => $amount_paid[$i],
                            'receipt_no' => $new_receipt_no
                        );
                        $this->db->where('walkin_patient_visit_statement_id', $walkin_statement_id[$i]);
                        $this->db->update('walkin_patient_visit_statement', $data1);
                    } else {
                        $data1 = array(
                            'charged' => $yes,
                            'payment_method' => $payment_method[$i],
                            'payment_code' => $payment_code[$i],
                            'amount_dr' => $amount_paid[$i],
                            'receipt_no' => $check_receipt
                        );
                        $this->db->where('walkin_patient_visit_statement_id', $walkin_statement_id[$i]);
                        $this->db->update('walkin_patient_visit_statement', $data1);
                    }
                } else {
                    if (empty($check_receipt)) {
                        $new_amount = $get_amount_dr + $amount_paid[$i];
                        echo 'new amount' . $new_amount . '<br>';
                        $new_receipt_no = $this->generate_walkin_receipt();
                        $new_amount = $get_amount_dr + $amount_paid[$i];
                        echo 'new amount' . $new_amount . '<br>';
                        $data1 = array(
                            'charged' => $yes,
                            'payment_method' => $payment_method[$i],
                            'payment_code' => $payment_code[$i],
                            'amount_dr' => $new_amount,
                            'receipt_no' => $new_receipt_no
                        );
                        $this->db->where('walkin_patient_visit_statement_id', $walkin_statement_id[$i]);
                        $this->db->update('walkin_patient_visit_statement', $data1);
                    } else {
                        $new_amount = $get_amount_dr + $amount_paid[$i];
                        $data1 = array(
                            'charged' => $yes,
                            'payment_method' => $payment_method[$i],
                            'payment_code' => $payment_code[$i],
                            'amount_dr' => $new_amount[$i],
                            'receipt_no' => $check_receipt
                        );
                        $this->db->where('walkin_patient_visit_statement_id', $walkin_statement_id[$i]);
                        $this->db->update('walkin_patient_visit_statement', $data1);
                    }
                    echo 'walkin patient visit statement sucessfully updated' . '<br>';
                }



                $data2 = array(
                    'paid' => $paid_status
                );
                $this->db->where('walkin_id', $walkin_id[$i]);
                $this->db->update('walkin', $data2);
                echo 'walkin successfully updated.' . '<br>';
                $query = $this->db->get_where('walkin_total_patient', array('walkin_patient_id' => $walkin_id[$i]));
                foreach ($query->result() as $value) {
                    $walkin_total_patient_id = $value->walkin_total_patient_id;


                    $data3 = array(
                        'paid' => $yes
                    );
                    $this->db->where('	walkin_total_patient_id', $walkin_total_patient_id);
                    $this->db->update('walkin_total_patient', $data3);
                }

                echo 'walkin total patient sucessfully updated' . '</br>';



                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
// generate an error... or use the log_message() function to log your error
                }
            }
        }
    }

    public function see_visit($id) {

        $query = $this->db->get_where('triage', array('visit_id' => $id));
        return $query->result_array();
    }

    public function see_details($visit_id) {

        $query = $this->db->get_where('visit', array('visit_id' => $visit_id));

        foreach ($query->result_array() as $row) {
            $patient_id = $row['patient_id'];
            $sql = $this->db->get_where('patient', array('patient_id' => $patient_id));
            return $sql->result_array();
        }
    }

    public function name($patientid) {
        $query = $this->db->get_where('patient', array('patient_id' => $patientid));
        return $query->result_array();
    }

    public function patient_statements() {
        $sql = "select sum(patient_visit_statement.amount_dr) as total_payments_dr,
            patient_visit_statement.patient_id,patient_visit_statement.visit_id,
            patient_visit_statement.date_added as visit_date,concat(patient.f_name, '',patient.s_name,' ',patient.other_name) as patient_name 
            from patient_visit_statement inner join patient on patient.patient_id = patient_visit_statement.patient_id 
          group by patient_visit_statement.visit_id";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function walkin_patient_statements() {
        $sql = "select * from walkin_patient_visit_statement"
                . " inner join walkin on walkin.walkin_id = walkin_patient_visit_statement.walkin_patient_id"
                . " inner join employee on employee.employee_id = walkin_patient_visit_statement.user_id "
                . "where walkin_patient_visit_statement.charged = 'Yes'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function walkin_patient_visit_statement() {
        $sql = "Select * from walkin_patient_visit_statement_view";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function walkin_print_patient_receipt($visit_id) {
        $sql = "SELECT * FROM `walkin_patient_receipt_details` where walkin_patient_id='$visit_id'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function print_patient_receipt($visit_id) {
        $sql = "SELECT * FROM `patient_receipt_details` where visit_id='$visit_id'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function get_patient_receipt_details($visit_id) {
        $sql = "Select * from patient_receipt_contact_info where visit_id='$visit_id'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function walkin_get_patient_receipt_details($visit_id) {
        $sql = "Select * from walkin_patient_receipt_info where walkin_id='$visit_id'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function check_if_receipt_no_exist($visit_id_1) {

        $query = $this->db->get_where('patient_payments', array('visit_id' => $visit_id_1), 1, 0);
        foreach ($query->result() as $value) {
            $receipt_no = $value->receipt_no;
            return $receipt_no;
        }
    }

    public function get_walkin_receipt_no() {
        $temp = $this->input->post('walkin_id');
        $walkin_id = $this->input->post('walkin_id');


        for ($i = 0; $i < count($temp); $i++) {
            $sql = "Select receipt_no from patient_visit_statement where visit_id = '$walkin_id[$i]'";
            $query = $this->db->query($sql);
            foreach ($query->result() as $value) {
                $receipt_no = $value->receipt_no;
                return $receipt_no;
            }
        }
    }

    public function generate_walkin_receipt() {
        $receipt_no = "RCPT_0";
        $receipt_no_base = "RCPT_";
        $this->db->select_max('receipt_no', 'receipt_numeric_no');
        $query = $this->db->get('patient_visit_statement');
        foreach ($query->result() as $value) {
            $max_receipt_no = $value->receipt_numeric_no;
            $one = 1;
            if (empty($max_receipt_no)) {
                $integer_value = substr($receipt_no, 5);
                $new_receipt_no = $integer_value + $one;
                $new_receipt = $receipt_no_base . $new_receipt_no;
            } else {
                $integer_value = substr($max_receipt_no, 5);
                $new_receipt_no = $integer_value + $one;
                $new_receipt = $receipt_no_base . $new_receipt_no;
            }

            return $new_receipt;
        }
    }

    public function generate_receipt() {
        $receipt_no = "RCPT_0";
        $receipt_no_base = "RCPT_";
        $this->db->select_max('receipt_numeric_no', 'receipt_numeric_no');
        $query = $this->db->get('patient_payments');
        foreach ($query->result() as $value) {
            $max_receipt_no = $value->receipt_numeric_no;
            $one = 1;
            $integer_value = substr($max_receipt_no, 5);
            $new_receipt_no = $max_receipt_no + $one;
            $new_receipt = $receipt_no_base . $new_receipt_no;
            return $new_receipt;
        }
    }

}

?>